import Event from './event';

Echo.join('operation.'+window.Laravel.user.operation_id)
    .here(users => {
        Event.$emit('users.here', users);
    })

    .joining(user => {
        Event.$emit('users.joined', user);
    })
    .leaving(user => {
        Event.$emit('users.left', user);
    });