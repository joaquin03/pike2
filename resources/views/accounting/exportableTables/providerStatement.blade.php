<table class="table table-bordered table-striped display dataTable no-footer" id="table">
    <tr style="background-color: #305c81; color: white;">
        <th>DATE</th>
        <th>BILL. #</th>
        <th>TYPE</th>
        <th>DUE DATE</th>
        <th>AIRCRAFT</th>
        <th>DEBIT</th>
        <th>CREDIT</th>
        <th>BALANCE</th>
        <th>ICAO</th>
        <th>OBSERVATIONS</th>
        <th>OPERATOR</th>
        <th>PIKE NUMBER</th>
    </tr>
    @foreach($accountingStatement['data'] as $accountingItem)
        <tr>
            <td>{{ $accountingItem['date'] }}</td>
            <td>{{ $accountingItem['ref_number'] }}</td>
            <td>
                @if( $accountingItem['type']  == 'Payment')
                    {{ $accountingItem['type'] }}
                @elseif( $accountingItem['type']  == 'CreditNote')
                   {{ $accountingItem['type'] }}
                @else
                    {{ $accountingItem['type'] }}
                @endif
            </td>
            <td>{{ $accountingItem['due_date'] }}</td>
            <td>{{ $accountingItem['aircraft'] }}</td>
            <td>{{ $accountingItem['debit'] }}</td>
            <td>{{ $accountingItem['credit'] }}</td>
            <td><b>{{ $accountingItem['balance'] }}</b></td>
            <td>{{ $accountingItem['icao'] }}</td>
            <td>{{ $accountingItem['observations'] }}</td>
            <td>{{ $accountingItem['operator'] }}</td>
            <td>
                @if(isset($accountingItem['operation_id']))
                    {{ $accountingItem['pike_number'] }}
                @endif
            </td>
        </tr>
    @endforeach

</table>