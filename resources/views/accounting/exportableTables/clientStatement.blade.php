<table class="table table-bordered table-striped display dataTable no-footer" id="table">
    <tr style="background-color: #305c81; color: white;">
        <th>DATE</th>
        <th>TYPE</th>
        <th>REF. #</th>
        <th>STATUS</th>
        <th>DUE DATE</th>
        <th>DEBIT</th>
        <th>CREDIT</th>
        <th>BALANCE</th>
        <th>OBSERVATIONS</th>
        <th>PIKE #</th>
        <th>OPERATOR</th>
    </tr>



    @foreach($accountingStatement['data'] as $accountingItem)
        <tr>
            <td>{{ $accountingItem['date'] }}</td>
            <td>
                @if( $accountingItem['type']  == 'Payment')
                    {{ $accountingItem['type'] }}
                @elseif( $accountingItem['type']  == 'CreditNote')
                    {{ $accountingItem['type'] }}
                @else
                    {{ $accountingItem['type'] }}
                @endif
            </td>
            <td>{{ $accountingItem['ref_number'] }}</td>
            <td>
                @if(isset($accountingItem['operation_id']))
                    {{ $accountingItem['status'] }}
                @endif
            </td>
            <td>{{ $accountingItem['due_date'] }}</td>
            <td>{{ $accountingItem['debit'] }}</td>
            <td>{{ $accountingItem['credit'] }}</td>
            <td><b>{{ $accountingItem['balance'] }}</b></td>
            <td>{{ $accountingItem['observations'] }}</td>
            <td>
                @if(isset($accountingItem['operation_id']))
                    {{ $accountingItem['pike_number'] }}
                @endif
            </td>
            <td>{{ $accountingItem['operator'] }}</td>

        </tr>
    @endforeach

</table>