@extends('backpack::layout')

@section('styles')
    <link href="{{ asset('assets/admin/pages/css/profile.css')}}" rel="stylesheet" type="text/css"/>

@endsection

@section('title')
    Credit note : {{ $creditNote->credit_notes_number }}
@endsection

@section('content')
    <section class="content-header">

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="col-md-12" style="overflow: hidden;">
                    <div class="box " style="box-sizing: border-box;">
                        <div class="box-header with-border">
                            <h3 class="box-title">Show</h3>
                        </div>
                        <div class="box-header with-border">

                            <form action="{{ route('crud.clients.credit-notes.update-assigned-bill', ['credit_note_id'=>$creditNote->id])}}"
                                  method="POST">
                                {{ csrf_field() }}

                                <div class="form-group col-md-4">
                                    <label>Credit Note Number</label>
                                    <input type="text" name="credit_notes_number" value={{$creditNote->payment_number}}
                                            disabled class="form-control">
                                </div>

                                <div class="form-group col-md-3">
                                    <input type="hidden" name="date" disabled>
                                    <label>Date</label>
                                    <div class="input-group date">
                                        <input value={{$creditNote->date->format('d/m/Y')}}
                                                type="text" class="form-control" name="date" disabled>
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group col-md-3">
                                    <label for="amount">Amount</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">USD</div>
                                        <input type="number" name="amount" id="amount"
                                               value={{\App\Models\Accounting\Bill::getTotalCreditNoteAmount($creditNote->billsAndDebitNotes)}}
                                                       step="any" disabled="disabled"
                                               class="form-control">
                                    </div>
                                </div>

                                <div class="form-group col-md-2">
                                    <label for="dollar_change">Dollar Rate</label>
                                    <input type="number" name="dollar_change" id="dollar_change"
                                           value={{$creditNote->dollar_change}} step="any" class="form-control"
                                           disabled>
                                </div>


                                <div class="form-group col-md-12">
                                    <label for="note">Notes</label>
                                    <textarea class="col-md-12" name="note" value=class="form-control" disabled>{{$creditNote->note}}
                                </textarea>
                                </div>

                                <hr class="col-md-12">

                                <h3 class="col-md-12">Credit Note Bills</h3>
                                <table id="bills-table" class="table table-striped table-bordered" style="width:100%">
                                    <tr>
                                        <th style="width: 222px">Bill Number</th>
                                        <th>CFE Type</th>
                                        <th>Reason</th>
                                        <th>Amount</th>
                                        <th>Discount from original bill</th>
                                        <th style="width: 222px">Assign to</th>
                                    </tr>
                                    @foreach($creditNote->billsAndDebitNotes as $index => $bill)

                                        <tr id="item-{{$index}}">
                                            <td>
                                                <select class="bill-input form-control select2 col-md-12"
                                                        id="old-bills[{{$index}}]"
                                                        disabled
                                                >
                                                    {{$selected=null}}
                                                    @foreach (\App\Models\Accounting\Bill::where('billing_company_id', $creditNote->billingCompany->id)
                                                    ->withoutGlobalScopes()->get()  as $connected_entity_entry)
                                                        <option value="{{$connected_entity_entry->id}}"
                                                                @if($connected_entity_entry->bill_number == $bill->bill_number)

                                                                selected {{$selected=$connected_entity_entry->id}} @endif >  {{ $connected_entity_entry->bill_number }}</option>
                                                    @endforeach
                                                </select>
                                                <input type="hidden" value="{{$selected}}"
                                                       id="old-bills[{{$index}}]-input" name="old-bills[{{$index}}]">
                                            </td>


                                            <td>{{$bill->getType($bill->pivot->cfe_type)}}</td>
                                            <td>{{$bill->pivot->reason}}</td>
                                            <td>{{$bill->pivot->amount}}</td>
                                            <td><input type="checkbox" class="discount_from_bill"
                                                       id="discount[{{$index}}][bill_id]"
                                                       name="discount[{{$index}}][bill_id]"
                                                       onchange="changeTickStatus(this)"></td>
                                            <td>
                                                <div id="select2-container-{{$index}}" style="display: none">
                                                    <select class="bill-input form-control select2 col-md-12"
                                                            id="assign-bills[{{$index}}]"
                                                            name="assign-bills[{{$index}}]"
                                                    >
                                                        <option value="-1">-</option>

                                                        {{$assigned_selected=null}}
                                                        @foreach (\App\Models\Accounting\Bill::where('company_id', $creditNote->billingCompany->id)->withoutGlobalScopes()->get() as $connected_entity_entry)
                                                            @if($connected_entity_entry->bill_number != '')
                                                                <option value="{{$connected_entity_entry->id}}"
                                                                        @if($connected_entity_entry->id == $bill->pivot->assigned_bill_id)
                                                                        selected {{$assigned_selected =$connected_entity_entry->id}}@endif >
                                                                    {{$connected_entity_entry->bill_number}}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td><b>TOTAL</b></td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <b>{{\App\Models\Accounting\Bill::getTotalCreditNoteAmount($creditNote->billsAndDebitNotes)}}</b>
                                        </td>
                                        <td></td>
                                        <td></td>

                                    </tr>
                                </table>
                                <div class="box-footer">
                                    <a class="btn btn-info" target="_blank"
                                       href={{$creditNote->bill_link}}>Show
                                        generated bill
                                    </a>

                                    <a href="{{route('crud.client-statement.show', ['client_id'=>$clientId])}}"
                                       class="btn btn-default"><span
                                                class="fa fa-ban"</span> Back
                                    </a>

                                    <input type="hidden" name="_method" value="PUT">
                                    <input class="btn btn-success pull-right" type="submit" value="Save Changes">
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection



@section('after_scripts')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.discount_from_bill').prop('checked', true);
            loadTicks();
            loadAssignBIlls();
            $('.select2').select2();
        });

        function loadTicks() {
            @foreach($creditNote->billsAndDebitNotes as $index => $bill)
            if (document.getElementById('assign-bills[{{$index}}]').value != -1) {
                if (document.getElementById('old-bills[{{$index}}]').value !== document.getElementById('assign-bills[{{$index}}]').value) {
                    document.getElementById('discount[{{$index}}][bill_id]').checked = false;
                }
            }
            @endforeach

        }

        function loadAssignBIlls() {
            @foreach($creditNote->billsAndDebitNotes as $index => $bill)
            if (!document.getElementById('discount[{{$index}}][bill_id]').checked) {
                $("#select2-container-{{$index}}").show();
            }
            @endforeach
        }

        function changeTickStatus(x) {
            closest_bill = parseInt(x.name.split('[')[1].split(']')[0]);
            var selectedBill = $("#select2-container-" + closest_bill);

            if (!selectedBill.is(":visible")) {
                selectedBill.show();
            } else {
                selectedBill.hide();
            }
        }
    </script>
    @stack('script_stack')

@endsection