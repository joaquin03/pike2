<hr class="col-md-12">

<h3 class="col-md-12">Credit Note Invoices</h3>
<table id="bills-table" class="table table-striped table-bordered" style="width:100%">
    <tr>
        <th>Description</th>
        <th>Final price</th>
    </tr>
    @foreach($invoice->accountingServices as $accountingService)
        <tr>
            <td>{{$accountingService->description}}</td>
            <td>{{round($accountingService->billing_final_price,2)}}</td>
        </tr>
    @endforeach
    <tr>
        <td><b>TOTAL</b></td>
        <td>
            <b>{{round($invoice->amount,2)}}</b>
        </td>
    </tr>
</table>