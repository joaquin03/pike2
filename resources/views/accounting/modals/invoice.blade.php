<html>
<head>
    <link href="{{ asset('assets/admin/pages/css/profile.css')}}" rel="stylesheet" type="text/css"/>
</head>

<section class="content-header">
    @if($invoice)
        <div class="row">
            <div class="col-md-12 col-md-offset-2" style="margin:0px">
                <div class="col-md-12" style="overflow: hidden;">
                    <div class="box " style="box-sizing: border-box;">
                        <div class="box-header with-border">
                            <h3 class="box-title">Invoice : #{{ $invoice->bill_number ?? 'No number' }}</h3>
                        </div>
                        <div class="modal-body">
                            <div class="box-header with-border">

                                <div class="form-group col-md-4">
                                    <label for="amount">Amount</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">USD</div>
                                        <input type="number" name="amount" id="amount"
                                               value='{{round($invoice->amount,2)}}'
                                               step="any" disabled="disabled"
                                               class="form-control">
                                    </div>
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="dollar_change">Dollar Rate</label>
                                    <input   type="number" name="dollar_change" id="dollar_change"
                                           value='{{$invoice->dollar_change}}' step="any" class="form-control" disabled>
                                </div>

                                <div class="form-group col-md-4">
                                    <input type="hidden" name="date" disabled>
                                    <label>Invoice date</label>
                                    <div class="input-group date">
                                        <input value='{{$invoice->date->format('d/m/Y')}}'
                                               type="text" class="form-control" disabled>
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group col-md-6">
                                    <input type="hidden" name="date" disabled>
                                    <label>Due date</label>
                                    <div class="input-group date">
                                        @if($invoice->due_date != null)
                                            <input value='{{$invoice->due_date->format('d/m/Y')}}'
                                                   type="text" class="form-control" disabled>
                                        @else
                                            <input value='' type="text" class="form-control" disabled>
                                        @endif
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group col-md-6">
                                    <label>Status</label>
                                    <input type="text" name="" value='{{$invoice->getStatus()}}' disabled
                                           class="form-control">
                                </div>

                                <div class="form-group col-md-12">
                                    <label>Files</label>
                                    <div class="well well-sm file-preview-container">
                                        @foreach($invoice->attachment as $attachment)
                                            <div class="file-preview">
                                                <a target="_blank" href="/storage/{{$attachment}}">{{$attachment}}</a>
                                            </div>
                                        @endforeach
                                    </div>

                                </div>

                                <div class="form-group col-md-12">
                                    <label for="note">Notes</label>
                                    <textarea class="col-md-12" name="note" value=class="form-control" disabled> {{$invoice->note}}
                                </textarea>


                                </div>

                                @include('accounting.modals.invoice_accounting_services')

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
</section>

</html>
