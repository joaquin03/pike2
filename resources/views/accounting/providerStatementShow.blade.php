@extends('backpack::layout')
@section('after_styles')
    <link href="{{ asset('assets/admin/pages/css/profile.css')}}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('/js/handsontable.full.min.css') }}"/>
    <style>
        .handsontable thead th .relative {
            padding: 2px 4px;
            background-color: #2b2b2b;
            font-size: 14px;
            text-transform: uppercase;
            font-weight: bold;
            color: white;
            text-align: center;
        }

        .handsontable tbody td {
            font-size: 13px;
        }

    </style>

@endsection

@section('title')
    Company Statement : {{ $company['name'] }}
@endsection

@section('content')
    <section class="content-header">
        <h1>
            Company: <a href="{{route('crud.company.show', ['company_id'=>$company['id']])}}"> {{$company['name']}} </a>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/admin/dashboard')}}">Admin</a></li>
            <li><a href="{{url('/admin/client-statement')}}" class="text-capitalize">Companies Statement</a></li>
            <li class="active">Show</li>
        </ol>

    </section>
    <div class="content">
        <div class="row">

            <div class="col-md-12">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs pull-right">
                        <li class="edit active">
                            <a href="#tab_statement" data-toggle="tab">Statement</a>
                        </li>
                    </ul>

                    <div class="tab-content" style="font-size:14px">
                        <div class="tab-pane edit active" id="tab_statement">
                            <div class="create-activity-btn btn-group pull-right" style="">


                                <a href="/admin/provider-statement/{{$company['id']}}/export-table" type="button"
                                   class="btn bg-aqua margin btn-outline" style="margin-top: 1px; margin-top: 1px;
                                        color:#fff; background-color: #f0ad4e;">Export Table
                                </a>

                                <button type="button" data-toggle="dropdown"
                                        class="btn bg-olive btn-flat margin btn-outline dropdown-toggle"
                                        style="margin-top: 1px;"
                                        style="margin-top: 1px;     z-index: 9999;">
                                    <i class="fa fa-angle-down"></i>Create Payment
                                </button>


                                <ul class="dropdown-menu pull-right" style="z-index: 9999"  role="menu">
                                    <li>
                                        <a href="{{route('crud.'.$type.'.payments.create', ['company_id'=>$company['id']])}}">
                                            <i class="fa fa-money"></i> Payment
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{route('crud.'.$type.'.overPayments.create', ['company_id'=>$company['id']])}}">
                                            <i class="fa fa-money"></i> Over Payment
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{route('crud.'.$type.'.credit-notes.create', ['company_id'=>$company['id']])}}">
                                            <i class="fa fa-sticky-note"></i> Credit Note
                                        </a>
                                    </li>

                                </ul>
                            </div>
                            {{--<div id="clientStatement" ></div>--}}
                            <table class="table table-bordered table-striped display dataTable no-footer"
                                   style="overflow-y:scroll;height:700px;display:block;" id="table">
                                <thead>
                                <tr style="background-color: #305c81; color: white; position: sticky; display: block;top: 0; z-index: 1000;">
                                    <th width="100">DATE</th>
                                    <th width="80" >BILL. #</th>
                                    <th width="80"  >STATUS</th>
                                    <th width="80" >TYPE</th>
                                    <th width="100" >DUE DATE</th>
                                    <th width="80" >AIRCRAFT</th>
                                    <th width="80" >DEBIT</th>
                                    <th width="80" >CREDIT</th>
                                    <th width="100" >BALANCE</th>
                                    <th width="100" >ICAO</th>
                                    <th width="170" >OBSERVATIONS</th>
                                    <th width="200">OPERATOR</th>
                                    <th width="100" >PIKE #</th>
                                    <th width="100" >FILES</th>
                                    <th width="80" ></th>
                                </tr>
                                </thead>
                                @foreach($accountingStatement['data'] as $accountingItem)
                                    <tr style="display: block">
                                        <td width="100">{{ $accountingItem['date'] }}</td>
                                        <td width="80">{{ $accountingItem['ref_number'] }}</td>
                                        <td width="80" style="position: relative; ">
                                            {!! $accountingItem['html_status'] ?? '' !!}

                                        </td>
                                        <td width="80">
                                            @if( $accountingItem['type']  == 'Payment')
                                                @if ($accountingItem['is_over_payment'])
                                                    <a href="/admin/providers/{{$company['id']}}/over-payments/{{$accountingItem['id']}}">O. {{ $accountingItem['type'] }}</a>
                                                @else
                                                    <a href="/admin/providers/{{$company['id']}}/payments/{{$accountingItem['id']}}">{{ $accountingItem['type'] }}</a>
                                                @endif
                                            @elseif( $accountingItem['type']  == 'CreditNote')
                                                <a href="/admin/providers/{{$company['id']}}/credit-notes/{{$accountingItem['id']}}">{{ $accountingItem['type'] }}</a>
                                            @else
                                                <a href="/admin/invoices/{{$accountingItem['id']}}/edit">{{ $accountingItem['type'] }}</a>
                                            @endif
                                        </td>
                                        <td width="100">{{ $accountingItem['due_date'] }}</td>
                                        <td width="80">{{ $accountingItem['aircraft'] }}</td>
                                        <td width="80">{{ $accountingItem['debit'] }}</td>
                                        <td width="80">{{ $accountingItem['credit'] }}</td>
                                        <td width="100"><b>{{ $accountingItem['balance'] }}</b></td>
                                        <td width="100">{{ $accountingItem['icao'] }}</td>
                                        <td width="170">
                                            <span title="{{ $accountingItem['observations'] }}">
                                            {{ str_limit($accountingItem['observations'], 50, '...')}}
                                            </span>
                                        </td>
                                        <td width="200">{{ $accountingItem['operator'] }}</td>
                                        <td width="100">
                                            @if(isset($accountingItem['operation_id']))
                                                <a href="/admin/procurement/{{$accountingItem['operation_id']}}/edit">
                                                    {{ $accountingItem['pike_number'] }}
                                                </a>
                                            @endif
                                        </td>
                                        <td width="100">
                                            @if(isset($accountingItem['files']))
                                                @foreach( $accountingItem['files'] as $key => $file)
                                                    <div class="file-preview">
                                                        <a target="_blank" href={{$file}}> Link {{$key+1
                                                        }} </a>
                                                    </div>
                                                @endforeach
                                            @endif
                                        </td>
                                        <td width="80">
                                            <a data-toggle="modal" data-target="#invoiceModal"
                                               href="{{route('crud.providers.invoices.show', ['id' => $accountingItem['id']])}}"
                                               class="btn btn-info"
                                               style='padding: 3px 8px;font-size: 10px;'>
                                                <i class='fa fa-info'></i>
                                            </a>
                                            <a href="/admin/invoice/{{ $accountingItem['id'] }}/delete?type={{$accountingItem['type']}}"
                                               onclick="return confirm('Are you sure you want to delete this statement?')"
                                               class='btn btn-sd btn-danger'
                                               style='padding: 3px 7px;font-size: 10px;margin: 2px;'>
                                                <i class='fa fa-trash-o'></i>
                                            </a>

                                        </td>


                                    </tr>
                                @endforeach

                            </table>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>



    <!-- Default bootstrap modal example -->
    <div class="modal fade" id="invoiceModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>

@endsection

{{--@push('stack_after_scripts')--}}
{{--@yield('after_scripts')--}}
{{--<script>--}}
{{--</script>--}}
{{--@endpush--}}