<hr class="col-md-12">

<h3 class="col-md-12">Items</h3>
<table id="bills-table" class="table table-striped table-bordered" style="width:100%">
    <tr>
        <th>Description</th>
        <th>Quantity</th>
        <th>Unit Price</th>
        <th>TOTAL</th>
        <th></th>
    </tr>
    @foreach($bill->accountingServices as $accountingService)
        <tr>
            <td>{{$accountingService->description}}</td>
            <td>{{round($accountingService->procurement_quantity,2)}}</td>
            <td>{{round($accountingService->billing_unit_price,2)}}</td>
            <td>{{round($accountingService->billing_final_price,2)}}</td>
            <td>
                @if($accountingService->getInvoice() != null)
                    <a data-toggle="modal" data-target="#invoiceModal"
                       href="{{route('crud.providers.invoices.show', ['id' => $accountingService->getInvoice()->id])}}" class="btn btn-info"
                       style='padding: 3px 8px;font-size: 10px;'>
                        Show Invoice
                    </a>
                @endif
            </td>
        </tr>
    @endforeach

    <tr>
        <td><b>TOTAL</b></td>
        <td></td><td></td>
        <td>
            <b>{{round($bill->amount,2)}}</b>
        </td>
        <td></td>
    </tr>
</table>


<!-- Default bootstrap modal example -->
<div class="modal fade" id="invoiceModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>