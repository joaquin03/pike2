@extends('backpack::layout')
@section('after_styles')
    <link href="{{ asset('assets/admin/pages/css/profile.css')}}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('/js/handsontable.full.min.css') }}"/>
    <style>
        .handsontable thead th .relative {
            padding: 2px 4px;
            background-color: #2b2b2b;
            font-size: 17px;
            text-transform: uppercase;
            font-weight: bold;
            color: white;
            text-align: center;
        }
    </style>

@endsection

@section('title')
    Company Statement : {{ $company['name'] }}
@endsection

@section('content')
    <section class="content-header">
        <h1>
            Company: <a href="{{route('crud.company.show', ['company_id'=>$company['id']])}}"> {{$company['name']}} </a>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/admin/dashboard')}}">Admin</a></li>
            <li><a href="{{url('/admin/client-statement')}}" class="text-capitalize">Companies Statement</a></li>
            <li class="active">Show</li>
        </ol>

    </section>
    <div class="content">
        <div class="row">

            <div class="col-md-12">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs pull-right">
                        <li class="edit active">
                            <a href="#tab_statement" data-toggle="tab">Statement</a>
                        </li>
                    </ul>

                    <div class="tab-content" style="">
                        <div class="tab-pane edit active" id="tab_statement" style="padding-bottom: 70px;">
                            <div class="create-activity-btn btn-group pull-right" style="">


                                <a href="/admin/client-statement/{{$company['id']}}/export-table" type="button"
                                   class="btn bg-aqua margin btn-outline" style="margin-top: 1px; margin-top: 1px;
                                        color:#fff; background-color: #f0ad4e;">Export Table
                                </a>

                                <button type="button" data-toggle="dropdown"
                                        class="btn bg-olive btn-flat margin btn-outline dropdown-toggle"
                                        style="margin-top: 1px;     z-index: 9999;">
                                    <i class="fa fa-angle-down"></i>Create Payment
                                </button>

                                <ul class="dropdown-menu pull-right" style="z-index: 9999"  role="menu">
                                    <li>
                                        <a href="{{route('crud.'.$type.'.payments.create', ['company_id'=>$company['id']])}}">
                                            <i class="fa fa-money"></i> Payment
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{route('crud.'.$type.'.overPayments.create', ['company_id'=>$company['id']])}}">
                                            <i class="fa fa-money"></i> Over Payment
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{route('crud.'.$type.'.credit-notes.create', ['company_id'=>$company['id']])}}">
                                            <i class="fa fa-sticky-note"></i> Credit Note
                                        </a>
                                    </li>

                                </ul>
                            </div>

                            <table class="table table-bordered table-striped display dataTable no-footer"
                                   style="overflow-y:scroll;height:700px;display:block;" id="table">
                                <tr style="background-color: #305c81; color: white; position: sticky; display: block;top: 0; z-index: 1000;">
                                    <th width="110">DATE</th>
                                    <th width="100">TYPE</th>
                                    <th width="130">REF. #</th>
                                    <th width="80">STATUS</th>
                                    <th width="100">DUE DATE</th>
                                    <th width="80">DEBIT</th>
                                    <th width="80">CREDIT</th>
                                    <th width="100">BALANCE</th>
                                    <th width="430">OBSERVATIONS</th>
                                    <th width="130">PIKE #</th>
                                    <th width="150">OPERATOR</th>
                                    <th width="80">TICKET</th>
                                </tr>


                                @foreach($accountingStatement['data'] as $index => $accountingItem)
                                    <tr style="display: block">
                                        <td width="110">{{ $accountingItem['date'] }}</td>
                                        <td width="100">
                                            @if( $accountingItem['type']  == 'Payment')
                                                @if ($accountingItem['is_over_payment'])
                                                    <a href="/admin/clients/{{$company['id']}}/over-payments/{{$accountingItem['id']}}">O. {{ $accountingItem['type'] }}</a>
                                                @else
                                                    <a href="/admin/clients/{{$company['id']}}/payments/{{$accountingItem['id']}}">{{ $accountingItem['type'] }}</a>
                                                @endif
                                            @elseif( $accountingItem['type']  == 'CreditNote')
                                                <a href="/admin/clients/{{$company['id']}}/credit-notes/{{$accountingItem['id']}}/show">{{ $accountingItem['type'] }}</a>
                                            @elseif( $accountingItem['type']  == 'DebitNote')
                                                <a href="/admin/clients/{{$company['id']}}/debit-notes/{{$accountingItem['id']}}/show">{{ $accountingItem['type'] }}</a>
                                            @else
                                                <a href="/admin/bills/{{$accountingItem['id']}}">{{ $accountingItem['type'] }}</a>
                                            @endif
                                        </td>
                                        <td width="130">{{ $accountingItem['ref_number'] }}</td>
                                        <td width="80" style="position: relative; ">
                                            {!! $accountingItem['html_status'] ?? '' !!}

                                        </td>
                                        <td width="100">{{ $accountingItem['due_date'] }}</td>
                                        <td width="80">{{ $accountingItem['debit'] }}</td>
                                        <td width="80">{{ $accountingItem['credit'] }}</td>
                                        <td width="100"><b>{{ $accountingItem['balance'] }}</b></td>
                                        <td width="430">
                                            <span title="{{ $accountingItem['observations'] }}">
                                            {{ str_limit($accountingItem['observations'], 140, '...')}}
                                            </span>
                                        </td>
                                        <td width="130">
                                            @if(isset($accountingItem['operation_id']))
                                                <a href="/admin/billing/{{$accountingItem['operation_id']}}/edit">
                                                    {{ $accountingItem['pike_number'] }}
                                                </a>
                                            @endif
                                        </td>
                                        <td width="150">{{ $accountingItem['operator'] }}</td>
                                        <td width="80">
                                            @if( $accountingItem['type']  != 'Payment')
                                                <a href="{{$accountingItem['link'] }}" target="_blank"> Show </a>
                                            @endif
                                        </td>

                                    </tr>
                                @endforeach

                            </table>
                        </div>
                    </div>


                </div>
            </div>
        </div>

        @endsection

        @push('stack_after_scripts')
            @yield('after_scripts')

            <script src="{{ asset('/js/handsontable.full.min.js') }}"></script>

            <script>

                var accountingStatement =
                        {!! json_encode($accountingStatement['data'], true) !!}
                var columns = [
                        {data: 'id', readOnly: true},
                        {data: 'date', readOnly: true},
                        {data: 'type', type: 'text', readOnly: true, renderer: renderLink},
                        {data: 'ref_number', type: 'text', readOnly: true},
                        {data: 'status', type: 'text', readOnly: true},
                        {data: 'observations', type: 'text', format: '0.00', readOnly: true},
                        {data: 'debit', type: 'numeric', format: '0.00', readOnly: true},
                        {data: 'credit', type: 'numeric', format: '0.00', readOnly: true},
                        {data: 'balance', type: 'numeric', format: '0.00', readOnly: true},
                        {data: 'pike_number', type: 'text', readOnly: true, renderer: renderPikeLink},
                        {data: 'operator', type: 'text', readOnly: true},
                        {data: 'due_date', type: 'text', readOnly: true},
                        {data: 'link', type: 'text', readOnly: true, renderer: renderTicketLink},
                        {data: 'operation_id', type: 'text', readOnly: true},
                    ];
                var colHeaders = [
                    'ID', 'Date', 'Type', 'Ref. #', 'Status', 'Observations',
                    'Debit', 'Credit', 'Balance', 'Pike#',
                    'Operator', 'Due Date', 'Ticket', ''
                ];
                var colWidths = [
                    2, 80, 80, 80, 80, 100,
                    90, 90, 90, 90,
                    90, 80, 100, 1,
                ];
                var hotElement = document.querySelector('#clientStatement');
                var hotSettings = {
                    data: accountingStatement,
                    columns: columns,
                    width: 1500,
                    autoWrapRow: true,
                    //height: 200 + 24 * accountingStatement.length,
                    colHeaders: colHeaders,
                    colWidths: colWidths,
                    maxRows: accountingStatement.length,
                    cells: function (row, col, prop) {
                        var cellMeta = {};
                        var data = accountingStatement[row];
                        if (data.ref_number == 'Payment' || data.ref_number == 'CreditNote') {
                            cellMeta.readOnly = true;
                            cellMeta.type = 'text';
                            cellMeta.renderer = function (hotInstance, td, row, col, prop, value) {
                                td.style.background = '#b9dfff';
                                if (col == 2) {
                                    renderLink(hotInstance, td, row, col, prop, value, prop);
                                } else {
                                    td.textContent = value;
                                }
                            };
                        }
                        return cellMeta;
                    }
                };

                hot_tables_Statement = new Handsontable(hotElement, hotSettings);


                function renderLink(instance, td, row, col, prop, value, cellProperties) {
                    var item = instance.getData()[row];
                    td.innerHTML = '<a href="' + getEditLink(instance, row, value) + '" >' + value + '</a>';
                }

                function renderTicketLink(instance, td, row, col, prop, value, cellProperties) {
                    var item = instance.getData()[row];
                    if (item[12]) {
                        td.innerHTML = '<a href="' + item[12] + '" > Show </a>';
                    }
                }

                function getEditLink(instance, row, value) {
                    var item = instance.getData()[row];
                    var link = '';
                    var type = 'clients';
                    var billType = 'bills';
                    if ('{{$type}}' != 'clients') {
                        type = 'providers';
                        billType = 'bills-provider';
                    }
                    if (value == 'Payment') {
                        return '/admin/' + type + '/'+{{$company['id']}}+
                        '/payments/' + item[0];
                    }
                    if (value == 'CreditNote') {
                        return '/admin/' + type + '/'+{{$company['id']}}+
                        '/credit-notes/' + item[0] + '/show';
                    } else {
                        return '/admin/' + billType + '/' + item[0] + '/edit';
                    }
                }

                function renderPikeLink(instance, td, row, col, prop, value, cellProperties) {
                    var item = instance.getData()[row];
                    var link = '/admin/billing/' + item[13] + '/edit';
                    td.innerHTML = '';
                    if (value) {
                        if ('{{$type}}' != 'clients') {
                            link = '/admin/procurement/' + item[13] + '/edit';
                        }

                        td.innerHTML = '<a href="' + link + '" >' + value + '</a>';
                    }
                }


            </script>

    @endpush