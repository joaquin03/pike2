@extends('backpack::layout')

@section('styles')
    <link href="{{ asset('assets/admin/pages/css/profile.css')}}" rel="stylesheet" type="text/css"/>

@endsection

@section('title')
    Bill : {{ $bill->bill_number }}
@endsection

@section('content')
    <section class="content-header">

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="col-md-12" style="overflow: hidden;">
                    <div class="box " style="box-sizing: border-box;">
                        <div class="box-header with-border">
                            <h3 class="box-title">Show</h3>
                        </div>
                        <div class="box-header with-border">


                            <div class="form-group col-md-4">
                                <label>Bill number</label>
                                <input type="text" name="" value='{{$bill->bill_number}}'
                                       disabled class="form-control">
                            </div>

                            <div class="form-group col-md-4">
                                <label for="amount">Amount</label>
                                <div class="input-group">
                                    <div class="input-group-addon">USD</div>
                                    <input type="number" name="amount" id="amount"
                                           value='{{round($bill->amount,2)}}'
                                           step="any" disabled="disabled"
                                           class="form-control">
                                </div>
                            </div>

                            <div class="form-group col-md-4">
                                <label for="dollar_change">Dollar Rate</label>
                                <input type="number" name="dollar_change" id="dollar_change"
                                       value='{{$bill->dollar_change}}' step="any" class="form-control" disabled>
                            </div>

                            <div class="form-group col-md-2">
                                <label for="payment_method">Payment Method</label>
                                <input type="text" name="payment_method" id="payment_method"
                                       value='{{$bill->getPaymentMethod()}}' step="any" class="form-control" disabled>
                            </div>
                            @if(false)
                            <div class="form-group col-md-3">
                                <input type="hidden" name="date"  disabled>
                                <label>Bill date</label>
                                <div class="input-group date">
                                    <input value='{{$bill->date->format('d/m/Y')}}'
                                           type="text" class="form-control" disabled>
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </div>
                                </div>
                            </div>
                            @endif

                            <div class="form-group col-md-2">
                                <label>Billing days</label>
                                <input type="text" name="billing_days" value='{{$bill->billing_days_to_pay}}'
                                       disabled class="form-control">
                            </div>

                            @if(false)
                            <div class="form-group col-md-3">
                                <input type="hidden" name="date"  disabled>
                                <label>Due date</label>
                                <div class="input-group date">
                                    <input value='{{$bill->due_date->format('d/m/Y')}}'
                                           type="text" class="form-control" disabled>
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </div>
                                </div>
                            </div>
                            @endif

                            <div class="form-group col-md-2">
                                <label>Status</label>
                                <input type="text" name="" value='{{$bill->getStatus()}}' disabled class="form-control">
                            </div>



                            <div class="form-group col-md-12">
                                <label for="note">Notes</label>
                                <textarea class="col-md-12" name="note" value=class="form-control" disabled> {{$bill->note}}
                                </textarea>
                            </div>

                            @include('accounting.bill_accounting_services')

                            <hr class="col-md-12">
                            <div class="box-footer">
                                <a class="btn btn-info" target="_blank"
                                   href={{$bill->bill_link}}>Show
                                    generated bill
                                </a>

                                <a href="/admin/client-statement/{{$bill->company_id}}" class="btn btn-default"><span
                                            class="fa fa-ban"></span>Back</a>

                                @if($bill->is_active && $bill->status != 'canceled')
                                    <a class="btn btn-danger pull-right" target="_blank"
                                       onclick="return confirm('The bill will be canceled. The amount will be the same. Are you sure?');"
                                       href="{{route('crud.clients.bills.cancel', ['bill_id'=> $bill->id])}}"
                                       data-toggle="tooltip" title="Cancel bill"><i class="fa fa-times"></i> Cancel</a>
                                @endif
                                @if($bill->is_active && $bill->status != 'invalidated')
                                    <a class="btn btn-alert bg-yellow pull-right" target="_blank"
                                       onclick="return confirm('The bill will be invalidated. The amount will be set in 0. Are you sure?');"
                                       href="{{route('crud.clients.bills.invalidate', ['bill_id'=> $bill->id])}}"
                                       data-toggle="tooltip" title="Invalidate bill"><i class="fa fa-times"></i> Invalidate</a>
                                @endif

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection