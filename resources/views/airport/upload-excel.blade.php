@extends('backpack::layout')

@section('styles')
    <link href="{{ asset('assets/admin/pages/css/profile.css')}}" rel="stylesheet" type="text/css"/>

@endsection
@section('title')
    Upload Airports from Excel
@endsection

@section('content')
    <div id="app" class="portlet light">


        <form id="form_upload" method="post" action="/admin/airport/upload" class="formfile" enctype="multipart/form-data">

            <input type="hidden" name="_token" id="_token" value="<?= csrf_token(); ?>">

            <div class="form-group col-xs-12">
                <label>Upload Excel File </label>
                <input name="file" id="file" type="file" class="file form-control" required/>
            </div>

            <button type="submit"  class="btn green">Upload</button>
        </form>
    </div>



@endsection


@section('scripts')
    <script>
        new Vue({
            el: "#app",

            data: {
                airport: {},
            }

        });


    </script>


@endsection