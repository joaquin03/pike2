<form action="{{route('crud.airport.provider.add', ['airport_icao'=>$airport->icao])}}" method="POST">
    {{ csrf_field() }}
    <div class="form-group col-md-3">
        <select name="provider_id" style="width: 100%" class="form-control select2" id="provider_id" required>
            <option disabled value="" selected>Provider*</option>
            @foreach(\App\Models\Provider::all() as $provider)

                <option value="{{$provider->id}}">
                    {{$provider->name.", ".$provider->country}}
                </option>
            @endforeach
        </select>
    </div>

    <div class="control col-md-3">
        <button class="button is-primary btn btn-sm blue" type="submit">
            Add
        </button>
    </div>

</form>



<table class='table'>
    <thead>
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Country</th>
        <th>Business</th>
        <th>Score</th>
        <th></th>
    </tr>
    </thead>
    <tbody>

    @foreach($airport->providers as $provider)
        <tr>
            <td> {{$provider->id}} </td>
            <td> {{$provider->name}} </td>
            <td> {{$provider->country}} </td>
            <td> {{$provider->business}} </td>
            <td>
                <input class="provider-score" type="number" name="score"
                       data-id="{{$provider->id}}" value="{{$provider->pivot->score}}"></td>
            <td>
                <a href="{{route('crud.airport.provider.remove', ['airport_icao'=>$airport->icao, 'provider_id'=>$provider->id])}}"
                   class="btn btn-circle btn-xs" data-toggle="confirmation" data-original-title="" title="">
                    <i class="fa fa-close" style="font-size: 12px"></i>
                </a>
            </td>
        </tr>
    @endforeach

    </tbody>
</table>



