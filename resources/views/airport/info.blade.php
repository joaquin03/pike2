{{ csrf_field() }}

<div class="box-body">
    <div class="col-md-12">
        <h4 class="form-section">Airport info</h4>

        <div class="form-group col-md-4">
            <label class="control-label col-md-4">Icao:</label>
            <span class="col-md-8">{{ $airport->icao }}</span>
        </div>

        <div class="form-group col-md-8">
            <label class="control-label col-md-4">Name:</label>
            <span class="col-md-8">{{ $airport->name }}</span>
        </div>

        <div class="form-group col-md-4">
            <label class="control-label col-md-4">Description:</label>
            <span class="col-md-8">{{ strtoupper($airport->descriptions) }}</span>
        </div>

        <div class="form-group col-md-4">
            <label class="control-label col-md-4">Country:</label>
            <span class="col-md-8">{{ strtoupper($airport->country) }}</span>
        </div>

        <div class="form-group col-md-4">
            <label class="control-label col-md-2">Iata:</label>
            <span class="col-md-10">{{ strtoupper($airport->iata) }}</span>
        </div>

        <div class="form-group col-md-4">
            <label class="control-label col-md-4">OPR HS H24:</label>
            <span class="col-md-8">{{$airport->OPR_HS_H24}}</span>
        </div>

        <div class="form-group col-md-4">
            <label class="control-label col-md-4">From/To:</label>
            <span class="col-md-8">{{$airport->OPR_HS_FROM . ' to '. $airport->OPR_HS_TO  }}</span>
        </div>

        <div class="form-group col-md-4">
            <label class="control-label col-md-4">H24 OR:</label>
            <span class="col-md-8">{{$airport->H24_OR }}</span>
        </div>

    </div>

    <div class="col-md-12">

        <hr>
        <div class="form-group col-md-6">
            <label class="control-label col-md-4">Length width 1 ft:</label>
            <span class="col-md-8">{{$airport->lengthwidth1ft}}</span>
        </div>

        <div class="form-group col-md-6">
            <label class="control-label col-md-4">Lengt hwidth 1 m:</label>
            <span class="col-md-8">{{$airport->lengthwidth1m}}</span>
        </div>

        <div class="form-group col-md-6">
            <label class="control-label col-md-2">RWY1:</label>
            <span class="col-md-10">{{$airport->RWY1}}</span>
        </div>

        <div class="form-group col-md-6">
            <label class="control-label col-md-2">PCN1:</label>
            <span class="col-md-10">{{$airport->PCN1}}</span>
        </div>

    </div>
    <div class="col-md-12">

        <hr>
        <div class="form-group col-md-6">
            <label class="control-label col-md-4">Length width 2 ft:</label>
            <span class="col-md-8">{{$airport->lengthwidth2ft}}</span>
        </div>

        <div class="form-group col-md-6">
            <label class="control-label col-md-4">Lengt hwidth 2 m:</label>
            <span class="col-md-8">{{$airport->lengthwidth2m}}</span>
        </div>

        <div class="form-group col-md-6">
            <label class="control-label col-md-2">RWY2:</label>
            <span class="col-md-10">{{$airport->RWY2}}</span>
        </div>

        <div class="form-group col-md-6">
            <label class="control-label col-md-2">PCN2:</label>
            <span class="col-md-10">{{$airport->PCN2}}</span>
        </div>


    </div>
</div>
