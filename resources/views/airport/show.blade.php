@extends('backpack::layout')

@section('styles')
    <link href="{{ asset('assets/admin/pages/css/profile.css')}}" rel="stylesheet" type="text/css"/>

@endsection

@section('title')
    Airport : {{ $airport->name }}
@endsection

@section('content')
    <section class="content-header">

        <h1>
            Airport: {{$airport->name}}
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/admin/dashboard')}}">Admin</a></li>
            <li><a href="{{url('/admin/airport')}}" class="text-capitalize">Airports</a></li>
            <li class="active">Show</li>
        </ol>
    </section>

    <div class="content" id="app">
        <div class="row">

            <div class="col-md-3">
                <div class="box box-primary ">
                    <div class="box-body box-profile">
                        <h3 class="profile-username text-center">  {{  $airport->icao }} </h3>
                        <h3 class="profile-username text-center"> {{ $airport->name }} </h3>
                        <p class="text-muted text-center"> {{ \App\Models\Utilities\Country::getName($airport->country) }}</p>
                    </div>
                </div>

            </div>

            <div class="col-md-9">
                {{--Providers tab--}}
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs pull-right">

                        <li class="active">
                            <a href="#tab_providers" data-toggle="tab">Providers</a>
                        </li>
                        <li>
                            <a href="#tab_info" data-toggle="tab">Info</a>
                        </li>
                    </ul>
                    <div class="tab-content">

                        <div class="tab-pane active" id="tab_providers" style="padding: 10px;">
                            @include('airport.provider', ['airport'=>$airport])
                        </div>


                        <div class="tab-pane" id="tab_info" style="padding: 10px;">
                            @include('airport.info', ['airport'=>$airport])
                        </div>

                    </div>
                </div>


            </div>
        </div>
    </div>


@endsection

@section('after_scripts')


    <script>
        $( document ).ready(function (){
            updateScoreHook = function () {
                $('.provider-score').on('change', function(){
                    var providerId = $(this).data('id');
                    var score = $(this).val();
                    PNotify.removeAll();
                    $.ajax({
                        type: "PUT",
                        url: '/api/airport/{{$airport->icao}}/update-provider',
                        data: {
                            'provider_id': providerId,
                            'score': score,
                        },
                    }).success(function (data) {
                        new PNotify({
                            text: "Provider Updated",
                            type: "success"
                        });
                    }).error(function (data) {
                        new PNotify({
                            title: "Unknown error",
                            text: "Please repeat the action",
                            type: "error"
                        });
                    });
                });
            };
            updateScoreHook();

            $('.select2').select2();
        });



    </script>
@endsection