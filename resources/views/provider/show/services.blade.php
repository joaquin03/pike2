{{ csrf_field() }}

<div>
    <form action="{{route('crud.providers.services.add', ['company_id'=>$company->id])}}" method="POST">
        {{ csrf_field() }}
        <div class="form-group col-md-3">
            <select name="service_id" style="width: 100%" class="form-control select2" id="service_id">
                <option disabled value="" selected>Service*</option>
                    @foreach(\App\Models\Service::all() as $service)
                        <option value="{{$service->id}}"> {{$service->name}} </option>
                    @endforeach
            </select>
        </div>
        <div class="form-group col-md-3">
            <input type="number" name="price" name="price" placeholder="Price" class="form-control">
        </div>
        <div class="form-group col-md-3">
            <input type="text" name="comments" placeholder="Comments" class="form-control">
        </div>
        <div class="control col-md-3">
            <button class="button is-primary btn btn-sm blue"  type="submit" >
                Add
            </button>
        </div>
    </form>


</div>

<table class='table display dataTable no-footer'>
    <thead>
    <tr>
        <th>Name</th>
        <th>Price</th>
        <th>Comments</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    @foreach($services as $service)
        <tr>
            <td> {{ $service->name }} </td>
            <td> {{ $service->pivot->price }} </td>
            <td> {{ $service->pivot->comments }} </td>
            <td>
                <a class="btn btn-circle btn-xs"
                   href="{{ route('crud.providers.services.remove', ['company_id'=>$company->id, 'services'=>$service->id]) }}"
                   data-toggle="confirmation" data-original-title="" title="">
                    <i class="fa fa-close" style="font-size: 12px"></i>
                </a>
            </td>
        </tr>
    @endforeach

    </tbody>
</table>

@push('scripts_providers')
    <script src="jquery-3.2.1.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>


    <script type="text/javascript">
        $(document).ready(function () {
            if ($('.select2').length > 0) {
                $('.select2').select2();
            }
        });
    </script>
@endpush