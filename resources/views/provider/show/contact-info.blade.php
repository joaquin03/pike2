<!-- Profile Image -->
<div class="box box-primary">
    <div class="box-body box-profile">
        @if ($company->image)
            <img src="/storage/{{$company->image}}" class="profile-user-img img-responsive img-circle"
                 style="max-width: 75%;width: auto;height: auto"/>
        @else
            <img src="http://www.joniskelis.lt/wp-content/themes/annina-pro/images/no-image-box.png"
                 class="profile-user-img img-responsive img-circle"
                 style="max-width: 75%;width: auto;height: auto;border-radius: 10px !important;"/>
        @endif

        <h3 class="profile-username text-center"> {{ $company->name }} </h3>

        <p class="text-muted text-center">{{ $company->business }}</p>
        <p class="text-muted text-center">{{ \App\Models\Utilities\Country::getName($company->country) }}</p>

        <a href="/admin/company/{{$company->id}}#edit"
           class="btn btn-primary btn-block"><b>Edit</b></a>

    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->

<!-- About Me Box -->
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Details</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        @if($company->emails!=null)
            @foreach($company->emails as $detail)
                <div class="text-muted">
                    <i class="fa fa-envelope"></i>
                    {{ $detail['comment'] ?? ''}} ({{ $detail['email'] ?? ''}})
                </div>
            @endforeach
        @endif

        @if($company->address!=null)
            @foreach($company->address as $detail)
                <div class="text-muted">
                    <i class="fa fa-map-marker"></i>
                    {{ $detail['city'] ?? '' }}, {{ $detail['country'] ?? '' }} ({{ $detail['address'] ?? '' }})
                    - {{ $detail['zip_code'] ?? '' }}
                </div>
            @endforeach
        @endif

        @if($company->phones!=null)

            @foreach($company->phones as $detail)
                <div class="text-muted">
                    <i class="fa fa-phone"></i>
                    {{ $detail['comment'] ?? ''}} ({{ $detail['phone'] ?? ''}})
                </div>
            @endforeach
        @endif
        <hr>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->

@push('stack_after_scripts')
    <script>
        function changeTab() {
            $('.activities').removeClass('active');
            $('.edit').addClass('active');
        }
    </script>
@endpush


