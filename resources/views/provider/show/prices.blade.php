<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>
    <!-- Bootstrap Date-Picker Plugin -->
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
</head>
<div class="tab-content row">


    <div class="form-group col-md-3">
        <div class="container">
            <div class="row">
                <div class='col-md-3'>
                    <div class="form-group">
                        <div class='input-group date datepicker' id='date-picker'>
                            <input type='text' class="form-control" id="date"/>
                            <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group col-md-2">
        <select class="form-control select2" name="service_id" id="service_id">
            @foreach(App\Models\Service::all() as $service)
                <option value="{{$service->id}}">
                    {{$service->name}}
                </option>
            @endforeach
        </select>
    </div>
    <div class="form-group col-md-2">
        <select class="form-control select2" name="currency_id" id="currency_id">
            @foreach(App\Models\Currency::all() as $currency)
                <option value="{{$currency->id}}">
                    {{$currency->currency}}
                </option>
            @endforeach
        </select>
    </div>
    <div class="form-group col-md-2">
        <input type="number" class="form-control" id="service_price" placeholder="Service Price">
    </div>
    <div class="form-group col-md-2">
        <input type="text" class="form-control" id="service_observation" placeholder="Service Observation">
    </div>

    <div class="col-md-1">
        <label for=""></label>
        <button id="add-competitor-button" type="submit" class="btn btn-success" onClick="addPrice()">
            Add
        </button>
        <!-- /.table-responsive -->
    </div>

</div>

<table class='table display dataTable no-footer' id="prices-table">
    <thead>
    <tr>
        <th>Date</th>
        <th>Service</th>
        <th>Currency</th>
        <th>Price</th>
        <th>Observations</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($company->prices as $price)
        <tr id="company-price-{{$price->id}}">
            <td>
                <div class='input-group date datepicker'>
                    <input type='text' class="form-control" id="date-{{$price->id}}" value="{{$price->date}}"
                           onchange="updatePrice({{$price->id}})"/>
                    <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                </div>

            </td>
            <td style="padding: 15px;">
                {{$price->service->name}}
            </td>
            <td style="padding: 15px;">
                {{$price->currency->currency}}
            </td>
            <td>
                <input type="number" class="form-control" id="service_price-{{$price->id}}" value="{{$price->price}}"
                       onchange="updatePrice({{$price->id}})">
            </td>
            <td>
                <input type="text" class="form-control" id="service_observation-{{$price->id}}"
                       value="{{$price->observation}}" onchange="updatePrice({{$price->id}})">
            </td>
            <td>
                <button href="" onclick="deletePrice({{$price->id}})" class="btn btn-sd btn-danger"
                        style="padding: 3px 7px;font-size: 10px;margin: 2px;"><i class="fa fa-trash-o"
                                                                                 style="padding: 0;"></i></button>
            </td>
        </tr>
    @endforeach

    </tbody>
</table>


<script>


    function addPrice() {
        var date = $("#date").val();
        var service_id = $("#service_id :selected").val();
        var currency_id = $("#currency_id :selected").val();
        var service_price = $("#service_price").val();
        var service_observation = $("#service_observation").val();

        if (validate(date, service_id, service_price)) {
            $.ajax({
                type: "POST",
                url: '{{url("/admin/provider/")}}' + '/' + {{$company->id}} +'/price/',
                data: {
                    'service_date': date,
                    'service_id': service_id,
                    'currency_id': currency_id,
                    'price': service_price,
                    'observation': service_observation,
                }
            }).success(function (data) {
                $('#prices-table tr:last').after('' +
                    '<tr id="company-price-' + data['price_id'] + '">' + rowData(data) + '</tr>'
                );
                $('.datepicker').datepicker(
                    {
                        format: 'dd/mm/yyyy',
                    }
                );
            })
                .error(function (data) {
                    alert('There has been an error, please contact your provider.')
                });
        }
    }


    function deletePrice(price_id) {
        if (confirm('Are you sure you want to delete this price?')) {
            $.ajax({
                type: "DELETE",
                url: '{{url("/admin/provider/")}}' + '/' + {{$company->id}} +'/price/' + price_id,
            }).success(function (data) {
                $('#company-price-' + data).remove();
            })
                .error(function (data) {
                    alert('There has been an error, please contact your provider.');
                });
        }
    }


    function updatePrice(price_id) {
        var date = $("#date-" + price_id).val();
        var service_price = $("#service_price-" + price_id).val();
        var service_observation = $("#service_observation-" + price_id).val();

        if (validateUpdate(date, service_price)) {
            $.ajax({
                type: "PUT",
                url: '{{url("/admin/provider/")}}' + '/' + {{$company->id}} +'/price/' + price_id,
                data: {
                    'service_date': date,
                    'price': service_price,
                    'observation': service_observation,
                }
            }).success(function (data) {
                $('#company-price-' + price_id).html(rowData(data));
                $('.datepicker').datepicker(
                    {
                        format: 'dd/mm/yyyy',
                    }
                );
            })
                .error(function (data) {
                    alert('There has been an error, please contact your provider.');
                });
        }
    }

    function validate(date, service_id, service_price) {
        var ret = false;
        if (date === '') {
            alert('Please select a date');
        } else if (service_id === '') {
            alert('Please select a service');
        } else if (service_price === '') {
            alert('Please insert price');
        } else {
            ret = true;
        }
        return ret;
    }

    function validateUpdate(date, service_price) {
        var ret = false;
        if (date === '') {
            alert('Please select a date');
        } else if (service_price === '') {
            alert('Please insert price');
        } else {
            ret = true;
        }
        return ret;
    }

    function rowData(data) {
        return ('<td> <div class="input-group date datepicker"> ' +
            '<input type="text" class="form-control" id="date-' + data['price_id'] + '"  value="' + data['date'] + '"' +
            ' onchange="updatePrice(' + data['price_id'] + ')" />' +
            '<span class="input-group-addon">' +
            '<span class="glyphicon glyphicon-calendar"></span>' +
            '</span> </div> </td>' +

            '<td style="padding: 15px;">' + data['service'] + '</td>' +
            '<td style="padding: 15px;">' + data['currency'] + '</td>' +

            '<td>' +
            '<input type="number" class="form-control" id="service_price-' + data['price_id'] + '" value="' + data['price'] + '"' +
            ' onchange="updatePrice(' + data['price_id'] + ')" />' +
            '</td>' +

            '<td>' +
            '<input type="text" class="form-control" id="service_observation-' + data['price_id'] + '" value="' + data['observation'] + '" ' +
            ' onchange="updatePrice(' + data['price_id'] + ')" />' +
            '</td>' +

            '<td><button href="" onclick="deletePrice(' + data['price_id'] + ')" ' +
            'class="btn btn-sd btn-danger" style="padding: 3px 7px;font-size: 10px;margin: 2px;">' +
            '<i class="fa fa-trash-o" style="padding: 0;"></i></button></td>');
    }

    $(document).ready(function () {
        $('.select2').select2();
        $('#service_id').select2();
        $('#currency_id').select2();

        $('.datepicker').datepicker(
            {
                format: 'dd/mm/yyyy',
            }
        );
    });

</script>

<style>
    .select2 {
        width: 100% !important;
        min-height: 34px !important;
    }
</style>
