<link href="{{ asset('vendor/adminlte/plugins/select2/select2.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css"
      rel="stylesheet" type="text/css"/>

<form action="{{route('crud.providers.airport.add', ['company_id'=>$company->id])}}" method="POST">
    {{ csrf_field() }}
    <div class="form-group col-md-3">
        <select name="airport_icao" style="width: 100%" class="form-control select2" id="airport_icao" required>
            <option disabled value="" selected>Airport*</option>
            @foreach(\App\Models\Airport::all() as $airport)
                <option value="{{$airport->icao}}">
                    {{$airport->icao}}
                </option>
            @endforeach
        </select>
    </div>

    <div class="control col-md-3">
        <button class="button is-primary btn btn-sm blue" type="submit">
            Add
        </button>
    </div>

</form>


<table class='table display dataTable no-footer'>
    <thead>
    <tr>
        <th>ICAO</th>
        <th>Name</th>
        <th>Country</th>
        <th>Description</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($company->airports as $airport)
        <tr>
            <td> {{$airport->icao}} </td>
            <td> {{$airport->name}} </td>
            <td> {{$airport->country}} </td>
            <td> {{$airport->description}} </td>
            <td>
                <a href="{{route('crud.providers.airport.remove', ['company_id'=>$company->id, 'airport_icao'=>$airport->icao])}}"
                   class="btn btn-circle btn-xs" data-toggle="confirmation" data-original-title="" title="">
                    <i class="fa fa-close" style="font-size: 12px"></i>
                </a>
            </td>
        </tr>
    @endforeach

    </tbody>
</table>

@push('scripts_providers')
    <script src="jquery-3.2.1.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>


    <script type="text/javascript">
        $(document).ready(function () {
            if ($('.select2').length > 0) {
                $('.select2').select2();
            }
        });
    </script>
@endpush


