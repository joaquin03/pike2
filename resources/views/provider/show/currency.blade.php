<div class="row">
    <!-- THE ACTUAL CONTENT -->
    <div class="col-md-12">
        <div class="box-header with-border">


            <h3 class="box-title">Currency</h3>
            <div class="box-body row">

                <div class="form-group col-md-6">
                    <input type="text" name="currency" id="currency" class="form-control">
                </div>
                <div class="form-group col-md-1">

                    <button id="add-currency" onclick="addCurrency()" class="btn btn-success">
                        <span>Add</span>
                    </button>
                </div>
            </div>
            <div id="datatable_button_stack" class="pull-right text-right"></div>
        </div>


        <div id="crudTable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">

            <div class="row">
                <div class="col-sm-12">
                    <table id="currency-table"
                           class="table  display dataTable no-footer"
                           role="grid" aria-describedby="crudTable_info">
                        <thead>
                        <tr role="row">
                            <th style="display: none"></th>
                            <th class="sorting" tabindex="0" aria-controls="crudTable" rowspan="1" colspan="1"
                                aria-label="Currency: activate to sort column ascending" style="width: 673px;">
                                Currency
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="crudTable" rowspan="1" colspan="1">

                            </th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach(\App\Models\Currency::all() as $index => $currency)
                            <tr id="currency-{{$currency->id}}" role="row">
                                <td>{{$currency->currency}}</td>
                                <td>
                                    <button href="" onclick="deleteCurrency({{$currency->id}})"
                                            class="btn btn-sd btn-danger"
                                            style="padding: 3px 7px;font-size: 10px;margin: 2px;"><i
                                                class="fa fa-trash-o" style="padding: 0;"></i></button>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div><!-- /.box-body -->


    </div>

</div>

<script>

    function addCurrency() {
        var currency = $("#currency").val();

        $.ajax({
            type: "POST",
            url: '{{url("/admin/currency/")}}',
            data: {
                'currency': currency
            }
        }).success(function (data) {
            $('#currency-table tr:last').after('' +
                '<tr id="currency-' + data['id'] + '"><td>' + data['name'] + '</td>' +
                '<td> <button href="" onclick="deleteCurrency(' + data['id'] + ')"' +
                ' class="btn btn-sd btn-danger"' +
                ' style="padding: 3px 7px;font-size: 10px;margin: 2px;"><i' +
                ' class="fa fa-trash-o" style="padding: 0;"></i></button></td>' +
                '</tr>'
            );

            console.log(data);
        })
            .error(function (data) {
                alert('There has been an error, please contact your provider.')
            });

    }


    function deleteCurrency(id) {
        if (confirm('Are you sure you want to delete this currency?')) {
            $.ajax({
                type: "DELETE",
                url: '{{url("/admin/currency/")}}' + '/' + id,
            }).success(function (data) {
                $('#currency-' + id).remove();
            })
                .error(function (data) {
                    alert('There has been an error, please contact your provider.');
                });
        }
    }

</script>