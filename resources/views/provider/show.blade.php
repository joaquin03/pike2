@extends('backpack::layout')


@section('title')
    Company : {{ $company->name }}
@endsection

@section('content')
    <section class="content-header">
        <h1>
            Company: {{$company->name}}
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/admin/dashboard')}}">Admin</a></li>
            <li><a href="{{url('/admin/company')}}" class="text-capitalize">Companies</a></li>
            <li class="active">Show</li>
        </ol>

    </section>
    <div class="content" id="crm-provider">
        <div class="row">

            <div class="col-md-3">
                @include('provider.show.contact-info')
            </div>

            <div class="col-md-9">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs pull-right">
                        <li>
                            <a href="#tab_airports" data-toggle="tab">Airports</a>
                        </li>
                        @if(Auth::user()->can('Procurement'))
                            <li class="active">
                                <a href="#tab_services" data-toggle="tab">Services</a>
                            </li>
                        @endif
                        <li>
                            <a href="#tab_prices" data-toggle="tab">Prices</a>
                        </li>
                        <li>
                            <a href="#tab_currency" data-toggle="tab">Currencies</a>
                        </li>

                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane" id="tab_prices" style="padding: 10px;">
                            @include('provider.show.prices', ['company'=>$company])
                        </div>
                        @if(Auth::user()->can('Procurement'))
                            <div class="tab-pane active" id="tab_services" style="padding: 10px;">
                                @include('provider.show.services', ['services'=> $company->services, 'type' => 'all'])
                            </div>
                        @endif

                        <div class="tab-pane" id="tab_airports" style="padding: 10px;">
                            @include('provider.show.airports', ['company'=>$company])
                        </div>
                        <div class="tab-pane" id="tab_currency" style="padding: 10px;">
                            @include('provider.show.currency')
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
@endsection

@push('stack_after_scripts')
    @include('partials.scripts.delete-scripts')

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>

    <!-- Bootstrap Date-Picker Plugin -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
@endpush