@extends('reports.baseReport')

@section('title')
    Report Operation
@endsection

@section('content')
    <div class="row" xmlns="http://www.w3.org/1999/html">
        <div class="col-md-12">
            @include('reports.filters', ['op_group_services'=>true])
        </div>


        <table id="report-table" class="table">
            <thead>
            <tr>
                <th>PN</th>
                <th>Date</th>
                <th>Aircraft</th>
                <th>Country</th>
                <th>ICAO</th>
                <th>Provider</th>
                <th>Service</th>
                <th>Status</th>
            </tr>
            </thead>

            <tfoot></tfoot>
        </table>


    </div>

@endsection

@push('script_stack')

    <script>
        $(function() {
            var dtButtons = function(buttons){
                var extended = [];
                for(var i = 0; i < buttons.length; i++){
                    var item = {
                        extend: buttons[i],
                        exportOptions: {
                            columns: [':visible']
                        }
                    };
                    switch(buttons[i]){
                        case 'pdfHtml5':
                            item.orientation = 'landscape';
                            break;
                    }
                    extended.push(item);
                }
                return extended;
            }

            var oTable = $('#report-table').DataTable({
                processing: true,
                serverSide: true,
                searching: false,
                pageLength: 25,
                ajax: {
                    url: '/api/reports/operation',
                    data: function (d) {
                        d.pn_start  = $('#pn_start').val();
                        d.pn_end    = $('#pn_end').val();
                        d.aircraft_id = $('#aircraft').val();
                        d.country   = $('#country').val();
                        d.icao      = $('#icao').val();
                        d.provider_id  = $('#provider_id').val();
                        d.service_id   = $('#service_id').val();
                        d.group_services = $('#group_services').val();
                        d.date_start = $('#date_start').val();
                        d.date_end = $('#date_end').val();

                    }
                },
                columns: [
                    { data: 'code', name: 'code'},
                    { data: 'date', name: 'date'},
                    { data: 'aircraft', name: 'aircraft'},
                    { data: 'country', name: 'country'},
                    { data: 'icao_names', name: 'icao_names'},
                    { data: 'provider_name', name: 'provider_name'},
                    { data: 'service_names', name: 'service_names'},
                    { data: 'service_status', name: 'service_status'}
                ],
                dom: '<"p-l-0 col-md-6"l>B<"p-r-0 col-md-6"f>rt<"col-md-6 p-l-0"i><"col-md-6 p-r-0"p>',
                buttons: dtButtons([
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5',
                    'print',
                    'colvis'
                ]),
            });

            $('#search-form').on('change', function(e) {
                oTable.draw();
                e.preventDefault();
            });
            $('#search-form').on('submit', function(e) {
                oTable.draw();
                e.preventDefault();
            });
        });
    </script>

@endpush