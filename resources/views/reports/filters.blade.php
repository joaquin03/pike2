<form method="POST" id="search-form" class="row" role="form">
    <div class="col-md-12">
        <div class="col-md-2">
            <label for="pn_start">PN Start</label>
            <input id='pn_start' type="text" name="pn_start" class="form-control"/>
        </div>
        <div class="col-md-2">
            <label for="pn_end">PN End</label>
            <input id='pn_end' type="text" name="pn_end" class="form-control"/>
        </div>
        <div class="col-md-2">
            <label for="date_start">Start Date</label>
            <input id='date_start' type="input" name="date_start" class="form-control datepicker" data-date-format="dd-mm-yyyy"/>
        </div>
        <div class="col-md-2">
            <label for="date_end">End Date</label>
            <input id='date_end' type="input" name="date_end" class="form-control datepicker" data-date-format="dd-mm-yyyy"/>
        </div>
        <div class="col-md-2">
            <label for="aircraft">Aircraft</label>
            <select id='aircraft' type="text" name="aircraft" class="form-control select2" multiple="multiple">
                <option value=""> - </option>
                @foreach($aircraft as $item)
                    <option value="{{$item->id}}">{{$item->registration}}</option>
                @endforeach()
            </select>
        </div>
        <div class="col-md-2">
            <label for="country">Country</label>
            <select id='country' type="text" name="country" class="form-control select2" multiple="multiple">
                <option value=""> - </option>
                @foreach($countries as $code => $country)
                    <option value="{{$code}}">{{$country}}</option>
                @endforeach()
            </select>
        </div>
    </div>
    <div class="col-md-12">
        <div class="col-md-2">
            <label for="icao">ICAO</label>
            <select id='icao' type="text" name="icao" class="form-control select2" multiple="multiple">
                <option value=""> - </option>
                @foreach($airports as $airport)
                    <option value="{{$airport->icao}}">{{$airport->icao}}</option>
                @endforeach()
            </select>
        </div>
        <div class="col-md-2">
            <label for="provider_id">Provider</label>
            <select id='provider_id' type="text" name="provider_id" class="form-control select2" multiple="multiple">
                <option value=""> - </option>
                @foreach($providers as $provider)
                    <option value="{{$provider->id}}">{{$provider->name}}</option>
                @endforeach()
            </select>
        </div>
        <div class="col-md-2">
            <label for="service_id">Service</label>
            <select id='service_id' type="text" name="service_id" class="form-control select2" multiple="multiple">
                <option value=""> - </option>
                @foreach($services as $service)
                    <option value="{{$service->id}}">{{$service->name}}</option>
                @endforeach()
            </select>
        </div>
        @if($op_group_services??false)
            <div class="col-md-2">
                <label for="group_services">Group Services</label>
                <select id="group_services" name="group_services" class="form-control">
                    <option value=0>No</option>
                    <option value=1>Yes</option>
                </select>
            </div>
        @endif
        <div class="col-md-2">
            <button type="submit" class="btn btn-primary" style="margin-top: 23px;">Filter</button>
        </div>
    </div>
    <hr class="col-md-12">
</form>