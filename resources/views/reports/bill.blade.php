@extends('reports.baseReport')

@section('title')
    BIlls Report
@endsection

@section('content')
    <div class="row" xmlns="http://www.w3.org/1999/html">
        <div class="col-md-12">
            <form method="POST" id="search-form" class="" role="form">
                <div class="col-md-2">
                    <label for="date_start">Start Date</label>
                    <input id='date_start' type="input" name="date_start" class="form-control datepicker" data-date-format="dd/mm/yyyy"
                    value="{{request('date_start')}}"/>
                </div>
                <div class="col-md-2">
                    <label for="date_end">End Date</label>
                    <input id='date_end' type="input" name="date_end" class="form-control datepicker" data-date-format="dd/mm/yyyy"
                           value="{{request('date_end')}}"/>
                </div>
                <div class="col-md-2">
                    <label for="client_id">Client</label>
                    <select id='client_id' type="text" name="client_id" class="form-control select2" multiple="multiple">
                        <option value=""> - </option>
                        @foreach($clients as $client)
                            <option value="{{$client->id}}" @if(request('client_id') == $client->id) selected @endif>
                                {{$client->name}}
                            </option>
                        @endforeach()
                    </select>
                </div>
                <div class="col-md-2">
                    <label for="status">Status</label>
                    <select id='status' type="text" name="status" class="form-control select2" multiple="multiple">
                        <option value=""> - </option>
                        <option value="not_paid" @if(request('status') == "Not Paid") selected @endif>
                            Not Paid
                        </option>
                        <option value="partially_paid" @if(request('status') == "Partially paid") selected @endif>
                            Partially Paid
                        </option>
                        <option value="paid" @if(request('status') == "Paid") selected @endif>
                            Paid
                        </option>
                    </select>
                </div>
                <div class="col-md-2">
                    <button type="submit" class="btn btn-primary" style="margin-top: 23px;">Filter</button>
                </div>
                <hr class="col-md-12">
            </form>
        </div>


        <table id="report-table" class="table">
            <thead>
            <tr>
                <th>Invoice</th>
                <th>PN</th>
                <th>Client</th>
                <th>Date</th>
                <th>Due Date</th>
                <th>Service</th>
                <th>Status</th>
                <th>Total</th>
                <th>Paid</th>
                <th>Sub Total</th>
            </tr>
            </thead>

            <tfoot></tfoot>
        </table>


    </div>

@endsection

@push('script_stack')

    <script>
        function customStatusHtmlRenderer(row, type, val) {
            var newHtml = '';

            if (row.status == 'paid') {
                newHtml = '<small class="label p1 white bg-green" style="position: absolute; color:white"> Paid </small>';
            }
            if (row.status == 'partially_paid') {
                newHtml = '<small class="label p1 white bg-yellow" style="position: absolute; color:white"> Part. Paid </small>';
            }
            if (row.status == 'not_paid') {
                newHtml = '<small class="label p1 white bg-gray" style="position: absolute; color:white"> Not Paid </small>';
            }
            if (row.status == 'canceled') {
                newHtml = '<small class="label p1 white bg-red" style="position: absolute; color:white"> Canceled </small>';
            }
            if (row.status == 'invalidated') {
                newHtml = '<small class="label p1 white bg-gray" style="position: absolute; color:white"> Invalidated </small>';
            }
            return newHtml;
        }

        $(function() {
            var dtButtons = function(buttons){
                var extended = [];
                for(var i = 0; i < buttons.length; i++){
                    var item = {
                        extend: buttons[i],
                        exportOptions: {
                            columns: [':visible']
                        }
                    };
                    switch(buttons[i]){
                        case 'pdfHtml5':
                            item.orientation = 'landscape';
                            break;
                    }
                    extended.push(item);
                }
                return extended;
            };

            var oTable = $('#report-table').DataTable({
                processing: true,
                serverSide: true,
                pageLength: 25,
                searching: false,
                paging: false,
                ordering: false,
                ajax: {
                    url: '/api/reports/bill',
                    data: function (d) {
                        d.pn_start  = $('#pn_start').val();
                        d.pn_end    = $('#pn_end').val();
                        d.country   = $('#country').val();
                        d.icao      = $('#icao').val();
                        d.client_id  = $('#client_id').val();
                        d.service_id   = $('#service_id').val();
                        d.group_services = $('#group_services').val();
                        d.date_start = $('#date_start').val();
                        d.date_end = $('#date_end').val();
                        d.status = $('#status').val();
                    }
                },
                columns: [
                    { data: 'bill_number', name: 'bill_number'},
                    { data: 'code', name: 'code'},
                    { data: 'client', name: 'client'},
                    { data: 'date', name: 'date'},
                    { data: 'due_date', name: 'due_date'},
                    { data: 'service_names', name: 'service_names'},
                    { data: 'status', data: customStatusHtmlRenderer},
                    { data: 'total', name: 'total'},
                    { data: 'paid', name: 'paid'},
                    { data: 'sub_total', name: 'sub_total'},
                ],
                dom: '<"p-l-0 col-md-6"l>B<"p-r-0 col-md-6"f>rt<"col-md-6 p-l-0"i><"col-md-6 p-r-0"p>',
                buttons: dtButtons([
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5',
                    'print',
                    'colvis'
                ]),
            });

            $('#search-form').on('change', function(e) {
                oTable.draw();
                e.preventDefault();
            });
            $('#search-form').on('submit', function(e) {
                oTable.draw();
                e.preventDefault();
            });

        });



    </script>

@endpush