@extends('backpack::layout')
@section('title')
    Report {{$report->place}}
@endsection

<link href="{{ asset('css/table.css') }}" rel="stylesheet">

@section('content')
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="box holder col-md-10 col-md-offset-1">
                <section class="print" style="">

                    <h3 style="margin-top: 10px">Pike Aviation</h3>
                    <table class="table table-bordered table-striped display dataTable no-footer"
                           style="width: 100%; margin: 0">
                        <tr>
                            <td>Date</td>
                            <td>{{$report->date}}</td>

                        </tr>
                        <tr>
                            <td>Place</td>
                            <td>{{$report->place}}</td>
                        </tr>
                        <tr>
                            <td>Objectives</td>
                            <td>{{$report->objectives}}</td>
                        </tr>
                        <tr>
                            <td>Participants</td>
                            <td>{{$report->participants}}</td>
                        </tr>
                    </table>
                    <br>
                    <h3 style="">Background Information</h3>

                    <table class="table table-bordered table-striped display dataTable no-footer"
                           style="width: 100%; margin: 0">

                        <tr>
                            <td>{{$report->background_info}}</td>
                        </tr>

                    </table>
                    <br><br>

                    <h3 style="">Details & Actions</h3>
                    <h4 style=""> {{$report->tag_name}}</h4>
                    <table class="table table-bordered table-striped display dataTable no-footer"
                           style="width: 100%; margin: 0">
                        <tr>
                            <th style="width: 100px">Contact/Provider</th>
                            <th style="width: 200px">Details</th>
                            <th style="width: 300px">Actions</th>
                        </tr>

                        @foreach($report->notes($report->tag) as $note)
                            <tr>
                                <td>
                                    <b>{{$note->getCompanyName()}}</b> <br>
                                    {{$note->getContactsInfo()}}
                                </td>
                                <td>{{$note->title}}</td>
                                <td>{{$note->body}}</td>
                            </tr>
                        @endforeach

                    </table>
                    <br>
                    <br>
                </section>
            </div>

        </div>
    </div>
@endsection


@section('after_scripts')

    @stack('script_stack')
@endsection