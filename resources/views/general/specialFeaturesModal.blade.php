@php
    $company = $operation->aircraftClient ?? \App\Models\Company::findOrFail(config('constants.tba_company_id'));
@endphp
<div class="modal fade" id="specialFeaturesModal"
     tabindex="-1" role="dialog"
     aria-labelledby="specialFeaturesModalLabel">

    <input type="hidden" name="client-input-id" id="client-input-id" value=""/>

    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center"
                    id="favoritesModalLabel">
                    <i class="fa fa-exclamation-triangle" style="color: orange"></i>
                    Company with special features
                    <i class="fa fa-exclamation-triangle" style="color: orange"></i>
                </h4>
            </div>
            <div class="modal-body" style="padding: 0">
                <div class="tab-content row">
                    <div class="col-md-12">
                        <div class="box-body" style="border-top: 1px solid #f4f4f4;">
                            <ul class="products-list product-list-in-box">

                                @foreach($company->activitiesSpecialFeatures as $activity)

                                    <li class="item {{$activity->event->getSlugClassName()}}">
                                        <div class="product-img">
                                            <i class="fa {{ $activity->event->getIcon() }} {{ $activity->event->getColor() }}"
                                               style="padding: 16px;font-size: 18px"></i>
                                        </div>
                                        <div class="product-info">
                                            <a target="_blank" href="{{route('crud.'.$activity->event->getSlugClassName().'.edit',
                                    ['company_id'=>$company, 'event_id'=> $activity->event_id])}}"
                                               class="product-title">
                                                {{ $activity->event->getTitle() }} - {{ $activity->author->name }}

                                                <span class="time pull-right"><i class="fa fa-clock-o"></i> {{ $activity->getDate() }}</span>
                                            </a>
                                            <span class="product-description ">
                                      {{ $activity->event->getDescription() }}
                                     </span>
                                        </div>

                                    </li>
                                @endforeach

                            </ul>
                        </div>
                    </div>
                </div>


            </div>
            <div class="modal-footer">
                <button type="button"
                        class="btn btn-default"
                        data-dismiss="modal">Close
                </button>
                <span class="pull-right">
                    <a href="{{ url('admin/company/'.$company->id) }}" target="_blank">
                          <button type="button" class="btn btn-primary">
                            Go to company
                          </button>
                    </a>
        </span>
            </div>
        </div>
    </div>
</div>
