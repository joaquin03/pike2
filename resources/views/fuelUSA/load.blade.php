@extends('backpack::layout')


@section('title')

@endsection

@section('header')
    <section class="content-header">
        <h1>Upload Fuel USA list</h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/admin/dashboard')}}">Admin</a></li>
            <li><a href="{{url('/admin/billing')}}" class="text-capitalize">Fuel USA</a></li>
            <li class="active">Load</li>
        </ol>

    </section>
@endsection()

@section('content')
    @include('crud::inc.grouped_errors')

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body row">
                    <div class="col-md-12">
                        <div class="form-group col-md-6" id="export-quotation-button">
                            <form action="{{route('fuel-usa.upload')}}" method="POST" enctype="multipart/form-data">

                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input class="col-md-8" type="file" name="fuel_airports" required="required" style="margin-top: 10px; margin-bottom: 10px">

                                <button type="submit" class="col-md-4 btn btn-warning">Import Excel</button>
                            </form>
                        </div>
                        <a class="col-md-12" target="_blank" href="{{url('uploads/example.xlsx')}}">Download Fuel List Example</a>
                    </div>

                    <div class="col-md-12">
                        <h3>Last Values Uploaded - <span style="font-size: 15px;">Date {{$lastUpdate->created_at->format('d-m-Y')}}</span></h3>
                        <table class="table table-hover">
                            <tr>
                                <th>ICAO</th>
                                <th>FBO</th>
                                <th>Client A</th>
                                <th>Client B</th>
                                <th>Client C</th>
                                <th>Min negotiable for pike</th>
                                <th>Retail price:</th>
                            </tr>

                            @foreach($fuelData as $values)
                                <tr>
                                    <td>{{ $values['icao'] }}</td>
                                    <td>{{ $values['fbo'] }}</td>
                                    <td>{{ $values['client_a'] }}</td>
                                    <td>{{ $values['client_b'] }}</td>
                                    <td>{{ $values['client_c'] }}</td>
                                    <td>{{ $values['min_pike'] }}</td>
                                    <td>{{ $values['retail'] }}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection