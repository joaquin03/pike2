<div class="col-md-12">
    <h3>Las Values Uploaded - <span style="font-size: 15px;">Date {{$lastUpdate->created_at->format('d-m-Y')}}</span></h3>
    <table class="table table-hover">
        <tr>
            <th>ICAO</th>
            <th>FBO</th>
            <th>Client A</th>
            <th>Client B</th>
            <th>Client C</th>
            <th>Min negotiable for pike</th>
            <th>Retail price:</th>
        </tr>

        @foreach($fuelData as $values)
            <tr>
                <td>{{ $values['icao'] }}</td>
                <td>{{ $values['fbo'] }}</td>
                <td>{{ $values['client_a'] }}</td>
                <td>{{ $values['client_b'] }}</td>
                <td>{{ $values['client_c'] }}</td>
                <td>{{ $values['min_pike'] }}</td>
                <td>{{ $values['retail'] }}</td>
            </tr>
        @endforeach
    </table>
</div>