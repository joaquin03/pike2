@extends('backpack::layout')


@section('title')

@endsection

@section('header')
    <section class="content-header">
        <h1>Upload Fuel USA list</h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/admin/dashboard')}}">Admin</a></li>
            <li><a href="{{url('/admin/billing')}}" class="text-capitalize">Fuel USA</a></li>
            <li class="active">Load</li>
        </ol>

    </section>
@endsection()

@section('content')
    @include('crud::inc.grouped_errors')

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body row">
                    @include('fuelUSA.partials.showTable')
                </div>
            </div>
        </div>
    </div>

@endsection