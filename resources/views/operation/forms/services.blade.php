<div class="form-group">
    <label class="">Provider *</label>
    <select class="form-control select2" name="provider_id" id="provider_id">
        <option value="" disabled> -</option>
        @foreach(\App\Models\Provider::all() as $provider)
            @if(in_array($itinerary->airport->icao, $provider->airports->pluck('icao')->toArray())
                    || strtolower($provider->country)==='ww' || $provider->is_ww_provider)
                <option value="{{ $provider->id }}">{{ $provider->name }}</option>
            @endif
        @endforeach
    </select>
</div>
<div class="form-group">
    <label>Service *</label>
    <select class="form-control select2" name="service_id" required id="service_id">
        <option value="" disabled> -</option>
    </select>
</div>
<div class="form-group">
    <label for=""></label>
    <button type="button" class="btn btn-success" onClick="addService({{$itinerary->id}})" style="margin-top: 25px;">
        Add
    </button>
</div>
<input type="hidden" name="provider_id" value="" id="provider_id">

<div class="itineraryNote" style="margin-top: 30px">
    @if($itinerary->notes)
        <div class="col-md-12">
            <label class="control-label col-md-4">General Notes:</label>
            <span class="col-md-8">{{$itinerary->notes}}</span>
        </div>
    @endif
    @if($itinerary->quotation_notes)
        <div class="col-md-12">
            <label class="control-label col-md-4">Quotation Notes:</label>
            <span class="col-md-8">{{$itinerary->quotation_notes}}</span>
        </div>
    @endif
    <h4>Notes</h4>
    <textarea style="min-width: 100%;min-height: 80px;" id="note" rows="2" cols="36.5" placeholder=""
              onkeyup="updateNotes()">{{$itinerary->operation_notes}}</textarea>
</div>

<script src="{{ asset('vendor/adminlte/plugins/select2/select2.min.js') }}"></script>

<script>

    function updateNotes() {
        $.ajax({
            type: "PUT",
            url: '/api/itinerary/{{$itinerary->id}}',
            data: {
                'itinerary': [
                    {'operation_notes': $("#note").val()}]
                ,

            },
        }).success(function (data) {
        });
    }

    function addService(itineraryId) {
        addItineraryServicesCount(itineraryId);

        $.ajax({
            type: "POST",
            url: '{{url("/api/itinerary/")}}' + '/' + itineraryId + '/service/',
            data: {
                'service_id': $("#service_id").val(),
                'provider_id': $("#provider_id").val(),
                'operation_status': 'Requested',
                'service_origin': 'Operation',
            }
        }).success(function (data) {
            getItineraryProviderServices(itineraryId);
        })
            .error(function (data) {
                PNotify.removeAll();
                new PNotify({
                    text: data.responseJSON.message,
                    type: "error"
                });
            });
    };

    reloadItineraryServices = function (providerServices) {
        location.reload();
    };

    $(function () {
        function loadServices() {
            var provider_id = $("#provider_id").val();
            $.get('{{url('api/provider/')}}' + '/' + provider_id + '/service?is_for_procurement=1', function (data) {
                $("#service_id").html(formatServices(data));
                $("#provider_id").val(provider_id);
                $('.select2').select2();
            });
        }

        function formatServices(data) {
            var html = '<option value="" disabled> - </option>';

            for (var i = 0; data.length > i; i++) {
                html += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
            }
            return html;
        }

        loadServices();

        $('#provider_id').on('change', function () {
            loadServices()
        });

        $('.select2').select2();
    });

    function addItineraryServicesCount(itineraryId) {
        console.log(parseInt($('#services-count-' + itineraryId).text()));
        if (parseInt($('#services-count-' + itineraryId).text()) === 0) {
            $('#services-count-' + itineraryId).show();
            document.getElementById('itineraryWithServices-'+itineraryId).style.backgroundColor = "#00a65a";
            document.getElementById('itineraryWithServices-'+itineraryId).style.borderColor = "#00a65a";
            document.getElementById('truck-'+itineraryId).style.color = "white";
        }
        $('#services-count-' + itineraryId).text(parseInt($('#services-count-' + itineraryId).text()) + 1);
    }


</script>
