@extends('backpack::layout')

@section('after_styles')

    @stack('styles_stack')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>

@endsection

@section('title')
    Operations
@endsection

@section('header')
    <style>
        .page-link {
            display: none;
        }
    </style>
@endsection()
@section('content')
    @include('crud::inc.grouped_errors')

    <link rel="stylesheet" href="public/css/styles.css">



    <div class="row" style="margin-top: -16px; margin-bottom: -40px;">
        <div class="box">
            <div class="box-body itinerary-table itinerary-table-dark itinerary-table-striped" style="font-size: 14px;">
                <table class="table  itinerary-table itinerary-table-dark itinerary-table-striped ">
                    <thead>
                    <tr>
                        <th width="100px" style="text-align: center;">
                            Operation
                        </th>
                        <th width="100px" style="text-align: center;">
                            Aircraft / Type
                        </th>

                        <th width="120px" style="text-align: center;">
                            Operator / Client
                        </th>

                        <th width="30" style="text-align: center"></th>
                        <th width="80" style="text-align: center">Type</th>
                        <th width="100" style="text-align: center">Route</th>
                        <th width="100" style="text-align: center">Date</th>
                        <th width="150" style="text-align: center">Time</th>
                        <th width="500" style="text-align: center">Services</th>


                    </tr>
                    </thead>
                    <tbody id="procurement-table">

                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop

@section('after_scripts')

    {{--<script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>--}}
    <script src="paginator.js"></script>

    <script>
        var order = "";
        var orderType = "";
        var procurements = {!! json_encode($items['data']) !!};


        var body = document.getElementsByTagName('body')[0];
        body.className = body.className + ' sidebar-collapse';

        renderProcurements = function (operationList) {
            var html = '';
            for (var i = 0; i < operationList.length; i++) {
                var item = operationList[i];
                html += '<tr>';
                html += '<td style="text-align: center"><b><a style="color: white;" href=' + item.id + '/edit target="_blank"> ' + item.code + ' </a></b></td>';
                html += '<td style="text-align: center"><b> ' + item.aircraft.toUpperCase()+'<br> - <br>'+ item.aircraft_type.toUpperCase() + ' </b></td>';
                html += '<td style="text-align: center"><b> ' + item.aircraft_operator.toUpperCase() + '<br> - <br>' + item.aircraft_client.toUpperCase() + ' </b></td>';
                html += '<td colspan="6" width="960" class="sub-table-container">';
                html += '<table class="table itinerary-table sub-table" style="border-top: 0">';
                var itineraries = item.itineraries.data;
                for (var j = 0; j < itineraries.length; j++) {
                    html += renderHtmlItinerary(itineraries[j]);
                }
                html += '</table>';
                var procurement_status = item.operation_status;

            }
            $("#procurement-table").html(html);
        };

        renderHtmlItinerary = function (itinerary) {
            var html = '';
            html += '<tr>';
            if (itinerary.type === 'ETD') {
                html += '<td width="30" style="border-top: 0"> <i class="fa fa-circle-o-notch fa-spin text-yellow" style="font-size: 15px; "></i></td>';
            }
            if (itinerary.type === 'ETA') {
                html += '<td width="30" style="border-top: 0"> <i class="fa fa-circle-o-notch fa-spin text-blue" style="font-size: 15px"></i></td>';
            }
            if (itinerary.type === 'ATD') {
                html += '<td width="30" style="border-top: 0"> <i class="fa  fa-check-circle text-yellow" style="font-size: 15px"></i></td>';
            }
            if (itinerary.type === 'ATA') {
                html += '<td width="30" style="border-top: 0"> <i class="fa  fa-check-circle text-blue" style="font-size: 15px"></i></td>';
            }
                html += '<td width="80" style="border-top: 0"> ' + itinerary.type + '</td>';
            html += '<td width="100" style="border-top: 0">' + itinerary.airport_icao_string + '</td>';
            html += '<td width="100" style="border-top: 0">' + itinerary.date + '</td>';
            html += '<td width="150" style="border-top: 0">' + itinerary.time + '</td>';
            html += '<td width="500" style="border-top: 0"><b>';
            var services = itinerary.services.data;
            for (var j = 0; j < services.length; j++) {
                if (j < services.length - 1) {
                    html += services[j].name + ', ';
                }
                else {
                    html += services[j].name;
                }
            }

            html += '</b></td>';
            html += '</tr>';
            return html;
        };

        renderProcurements(procurements);

        @if($page != null)
            reloadScreen( {{$page ?? '1'}} , {{$paginate ?? '0'}});
        @endif

        function reloadScreen(page, paginate) {
            var timerId = 0,
                timerId = setInterval(function () {
                    $.ajax({
                        url: "/admin/operation/screen?page="+ page +"&paginate="+ paginate,
                        success: function (data) {
                            $("#procurement-table").html(data);
                            clearInterval(timerId);
                        }
                    });
                }, 5 * 1000)
        }





    </script>


@endsection
