<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">Flight Plan</h4>
</div>
<form action="{{ route('crud.flight-plan.itinerary.store', ['itinerary_id'=>$itinerary->id]) }}" method="POST">
    <div class="modal-body row">
        {{ csrf_field() }}

        <div class="form-group col-md-4">
            <label>Flight Type *</label>
            <select name="flight_type" id="" class="form-control" required="">
                @foreach(\App\Models\FlightPlan::$flightLevels as $key => $level)
                    <option value="{{ $key }}" @if($key==$flightPlan->flight_type) selected @endif>{{$level}}</option>
                @endforeach()
            </select>
        </div>
        <div class="form-group col-md-4">
            <label>Speed *</label>
            <input name="speed" type="text" class="form-control" required="" value="{{ $flightPlan->speed }}">
        </div>
        <div class="form-group col-md-4">
            <label>Level *</label>
            <input name="level" type="text" class="form-control" required="" value="{{ $flightPlan->level }}">
        </div>
        <div class="form-group col-md-4">
            <label>EET *</label>
            <input name="eet" type="text" class="form-control" required="" value="{{ $flightPlan->eet }}">
        </div>
        <div class="form-group col-md-4">
            <label>Route *</label>
            <input name="route" type="text" class="form-control" required="" value="{{ $flightPlan->route }}">
        </div>
        <div class="form-group col-md-4">
            <label>Icao Route *</label>
            <input name="icao_route" type="text" class="form-control" required="" value="{{ $flightPlan->icao_route }}">
        </div>
        <div class="form-group col-md-4">
            <label>Provider *</label>
            <select name="provider_id" id="" class="form-control" required="" selected="selected">
                @if(isset($flightPlan->provider))
                    <option value="{{ $flightPlan->provider->id }}">{{$flightPlan->provider->name}}</option>
                @endif
                    @foreach(\App\Models\Provider::all() as $provider)
                    <option value="{{ $provider->id }}">{{$provider->name}}</option>
                @endforeach()
            </select>
        </div>


        <div class="form-group col-md-12" style="text-align: center">
            <label for=""></label>
            <button type="submit" class="btn btn-info" style="margin-top: 25px;">Save Flight Plan</button>
        </div>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
</form>



