<div class="table_row_slider">
    @if(!$itinerary->isDeparture())

        <div class="row">
            <h3 class="col-md-12">
                <b>Services</b>
            </h3>


            <div class="col-md-3 one-line">
                @if(\Illuminate\Support\Facades\Auth::user()->can('Operation'))
                    @include('operation.forms.services')
                @endif
            </div>
            <div class="col-md-9">
                @include('operation.tables.services')
            </div>
        </div>

        <hr class="row separator">
    @endif

</div>


