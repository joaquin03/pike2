


<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">Permanent Permits</h4>
</div>
<div class="modal-body">

    <form method="POST" action="{{ route('crud.operation.permanent-permission.store', ['operation_id'=>$operation->id]) }}"
          accept-charset="UTF-8">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <table class="table" id="table" >
                <tr>
                    <span style = "font-weight: bold;">Code</span>
                </tr>
                @foreach($permanentPermissions as $permanentPermission)
                    <tr>
                        <td style="width: 5%">
                            <input type="checkbox"
                                name = "permanentPermissionId[]"
                                value = "{{$permanentPermission->id}}"
                                @if(in_array($permanentPermission->id, $operationPermanentPermissions))
                                checked ="checked"

                                @endif
                            />
                        </td>
                        <td>Provider: {{$permanentPermission->provider_name}} / Code: {{$permanentPermission->code}} / Type: {{$permanentPermission->type}} / Country: {{\App\Models\Utilities\Country::getName($permanentPermission->country)}} </td>
                    </tr>
                @endforeach
            </table>



        <div class="modal-footer">
            <button type="submit" class="btn btn-success">Save</button>
        </div>
    </form>
</div>

<script src="{{ asset('vendor/adminlte/plugins/select2/select2.min.js') }}"></script>
<script>
    $(function () {
        $('.select2').select2();
    })

</script>


