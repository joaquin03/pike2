

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title" id="myModalLabel">Services</h4>
</div>
<div class="modal-body row">
    <div class="col-md-12">
        @include('operation.forms.services')
    </div>
</div>
        @include('operation.tables.services')
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>





