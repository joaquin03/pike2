<div id="operation_{{$operation->id}}">

</div>
<br>

<!-- Default bootstrap modal example -->
<div class="modal fade" id="permamentPermissionModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>




@push('script_stack')
    <script>

        var deleteFile = false;
        var codeChanged = false;

        function changeCode() {
            codeChanged = true;
        };
        $("#submit").on("click", function () {
            if (codeChanged) {
                if (confirm("Changes have been made to the permit codes. Are you sure you want to proceed? ")) {
                    return true;
                } else {
                    location.reload();
                    return false;
                }
            }

        });

        var permits = {!! json_encode($permits['data']) !!}
        var hot_tables_{{$operation->id}} = [];

        var columns = [
            {data: 'id', type: 'numeric', readOnly: true},
            {data: 'provider_name', type: 'numeric', readOnly: true},
            {data: 'code', type: 'text'},
            {data: 'type', type: 'dropdown', source: ['Departure', 'Over Flight', 'Landing', 'Transit']},
            {data: 'country', type: 'text', readOnly: true},
            {data: 'icao_from', type: 'text', format: '0', readOnly: true},
            {data: 'icao_to', type: 'text', format: '0.00', readOnly: true},
            {data: 'expiration_date', type: 'date', dateFormat: 'DD/MM/YYYY',
                correctFormat: true},
            {data: 'file', type: "text", renderer: customHtmlRenderer,  readOnly: true},
            {data: 'status', type: "text", renderer: permissionStatusRenderer, readOnly: true},
            {data: 'delete_permission', renderer: deletePermissionRenderer,  readOnly: true},

        ];
        var colHeaders = ['Id','Provider', 'Code', 'Type', 'Country',  'From', 'To', 'Exp. Date', 'File', 'Status', ''];

        var colWidths = [
            1,70, 100, 55, 70, 50, 50, 85, 30, 80, 65
        ];

        rendererPermits = function (permits) {
            var hotElement = document.querySelector('#operation_{{$operation->id}}');
            var hotSettings = {
                data: permits,
                columns: columns,
                width: 912,
                autoWrapRow: true,
                height: 130 + 24 * permits.length,
                colWidths: colWidths,
                colHeaders: colHeaders,
                maxRows: permits.length,
                afterChange: function (changes, source) {
                    if (source == 'edit') {
                       var item = this.getData()[changes[0][0]];
                        updatePermission(item);
                    }

                },
            }
            hot_tables_{{$operation->id}} = new Handsontable(hotElement, hotSettings);
        };

        rendererPermits(permits);


        function customHtmlRenderer(instance, td, row, col, prop, value, cellProperties) {
            var instance2 = instance.getData()[row];
            var newHtml = "<form id='upload_form' enctype='multipart/form-data'>";
            if (value != "") {
                newHtml += "<input id='file-input' type='file' class='' style='display: none;' " +
                        " name='permission[" + instance2[1] + "][file]'><a href='" + value + "' target='_blank'>" +
                        "<i class='fa fa-cloud-download'/></a></form>";
            }
            else {
                newHtml += "<input id='file-input-no-photo["+instance2[0]+"]' class='"+instance2[0]+"' onchange='updateFile("+JSON.stringify(instance2)+")'" +
                        "type='file' style='display:none' data-id="+ instance2[0]+" />" +
                "<label for='file-input-no-photo["+instance2[0]+"]'> " +
                        "<i class='fa fa-cloud-download' style='color:darkgray' /></label></form>";
            }

            td.innerHTML = newHtml;
            return td;
        }

        function deletePermissionRenderer(instance, td, row, col, prop, value, cellProperties) {
            var instance2 = instance.getData()[row];
            newHtml = "<button href='' onclick='deletePermission("+JSON.stringify(instance2)+")' class ='btn btn-sd btn-danger' " +
                    "style='padding: 3px 7px;font-size: 10px;margin: 2px;'> <i class='fa fa-trash-o'></i></button>"
            if(instance2[8] != ""){
                newHtml += "<button href='' class ='btn btn-sd btn-warning' onclick='deleteFilePermission("+JSON.stringify(instance2)+")'"+
                        "style='padding: 4px 6px;font-size: 10px;margin: 2px;'><i class='fa fa-file-excel-o'></i></button>";

            }

            td.innerHTML = newHtml;
        }

        function deletePermission(item) {
            if(confirm('Are you sure you want to delete this permission?')){
                $.ajax({
                    type: "GET",
                    url: '/admin/operation/{{$operation->id}}/permission/' + item[0] + '/delete',
                }).success(function (data) {
                    reloadOperationPermits(data.data.permissions);
                });
            }
        }

        function permissionStatusRenderer(instance, td, row, col, prop, value, cellProperties) {
            var instance2 = JSON.stringify(instance.getData()[row]);
            newHtml = "";
            if (value == 'Requested') {
                newHtml += "<button onclick='changePermissionStatus("+instance2+")' class='status label  bg-gray' type='button' data-value='"+instance2+" " +
                        "'style='font-size:11px; color:white' data-status='Requested'>REQUESTED </button>";

            } else if (value == 'Canceled'){
                newHtml += "<button onclick='changePermissionStatus("+instance2+")' class='status label bg-black' type='button' data-value='"+instance2+" " +
                        "'style='font-size:11px; color:white' data-status='Canceled'>CANCELED </button>";
            }
            td.innerHTML = newHtml;
        }

        function changePermissionStatus (item) {
            var status = item[9];
            if(status == "Requested") {
                item[9] = "Canceled";
            } else {
                item[9] = "Requested";
            }
            deleteFile = false;
            updatePermission(item);
        }

        function updatePermission(item) {
            $.ajax({
                type: "POST",
                url: '/api/operation/{{$operation->id}}/permission/update',
                data: {
                    'id': item[0],
                    'code': item[2],
                    'type': getPermissionType(item[3]),
                    'expiration_date': item[7],
                    'status': item[9]  ,
                    'file': null,
                    'delete-file': deleteFile,
                },
            }).success(function (data) {
                reloadOperationPermits(data.data.permissions);
            });
        }

        function getPermissionType(abreviated){
            if(abreviated == 'DPT') {
                return 'Departure';
            }
            if(abreviated == 'OVF') {
                return 'Over Flight';
            }
            if(abreviated == 'LND') {
                return 'Landing';
            }
            return abreviated;
        }

        function reloadOperationPermits(permissions) {
            deleteFile = false;
            permissions = permissions.data;
            var table = hot_tables_{{$operation->id}};
            table.loadData(permissions);
            table.updateSettings({
                height: 130 + 24 * permissions.length,
                maxRows: permissions.length,
            })
        }

        function updateFile(item){
            event.preventDefault();
            var formData = new FormData($('#upload_form')[0]);
            formData.append('file', $("."+item[0])[0].files[0]);
            formData.append('id', item[0]);
            formData.append('expiration_date', '');
            formData.append('delete-file', 0);
            $.ajax({
                type: "POST",
                url: '{{url("/api/operation/")}}'+ '/' + '{{$operation->id}}' + '/permission/update',
                processData: false,
                cache       : false,
                contentType : false,
                data: formData ? formData : form.serialize(),
            }).success(function (data){
                    reloadOperationPermits(data.data.permissions);
            })
                    .error(function (data){
                        PNotify.removeAll();
                        new PNotify({
                            text: "error",
                            type: "error"
                        });
                    });
        };

        function deleteFilePermission(item) {
            if (confirm('Are you sure you want to delete the associated file?')) {
                deleteFile = true;
                updatePermission(item);
            }
        }
    </script>
@endpush