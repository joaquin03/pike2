<div>
    <span class="h3">Permanent permits</span>
</div>
<table class="table" id="table">
    <tr>
        <th>Provider</th>
        <th>Name</th>
        <th>Code</th>
        <th>Type</th>
        <th>File</th>
        <th>Country</th>
    </tr>



    @foreach($operation->permanentPermissions as $permanentPermission)
        <tr>
            <td>{{ $permanentPermission->provider_id }}</td>
            <td>{{ $permanentPermission->file_name }}</td>
            <td>{{ $permanentPermission->code }}</td>
            <td>{{$permanentPermission->type}}</td>
            <td>
                @if(! is_null($permanentPermission->file))
                    <a href="{{ url($permanentPermission->getUrlFile()) }}" target="_blank">Download</a>
                @else

                @endif
            </td>
            <td>{{ $permanentPermission->getCountry() }}</td>
        </tr>
    @endforeach



</table>

<a id="addPermits"
   data-toggle="modal" data-target="#permamentPermissionModal" class="btn bg-maroon ajax-modal" style="float:right;"
   href="/admin/operation/{{$operation->id}}/permanent-permission/create">
    Manage Permanent Permits
</a>


