<div class="row">
    <div class="col-md-12">
        <div id="itinerary_{{$itinerary->id}}"></div>
    </div>
</div>


<script>
    var provider_changed = false;

    var providerServices = {!! json_encode($providerServices, true)  !!};
    var hot_tables_{{$itinerary->id}} = null;

    var columns = [
        {data: 'id', readOnly: true},
        {data: 'provider_name', type: 'text', readOnly: true},
        {data: 'service', type: 'text', readOnly: true},
        {data: 'operation_status', renderer: serviceStatusRenderer, readOnly: true},
        {data: 'operation_quantity', type: 'numeric', format: '0.00'},
        {data: 'delete_service', renderer: deleteServiceRenderer, readOnly: true},
    ];

    var colHeaders = [
        'ID', 'Provider', 'Service', 'Op. Status', 'Qty', ''];

    var colWidths = [
        2, 240, 180, 100, 60, 50,
    ];


    renderServices = function (providerServices) {
        var items = providerServices.data;

        var hotElement = document.querySelector('#itinerary_{{$itinerary->id}}');
        var hotSettings = {
            data: items,
            columns: columns,
            width: 912,
            height: 130 + 24 * items.length,
            colWidths: colWidths,
            colHeaders: colHeaders,
            maxRows: items.length - 1,
            cells: function (row, col, prop) {
                return editCustomCells(row, col, prop, items);
            },
            afterChange: function (changes, source) {
                if (source == 'edit') {
                    var item = this.getData()[changes[0][0]];
                    updateService(item);
                }
            },
        };
        hot_tables_{{$itinerary->id}} = new Handsontable(hotElement, hotSettings);

    };


    function editCustomCells(row, col, prop, items) {
        var cellMeta = {};
        var data = items[row];
        if (col == 6) {
            cellMeta.renderer = function (hotInstance, td, row, col, prop, value) {
                deleteServiceRenderer(hotInstance, td, row, col, prop, value);
            }
        }

        return cellMeta;
    }

    function reloadItineraryServices(providerServices) {
        services = providerServices.data;
        var table = hot_tables_{{$itinerary->id}};
        table.loadData(services);
        table.updateSettings({
            height: 130 + 24 * services.length,
            maxRows: services.length - 1,
            cells: function (row, col, prop) {
                return editCustomCells(row, col, prop, services);
            }
        })
    }

    function getItineraryProviderServices(itineraryId) {
        $.ajax({
            type: "GET",
            url: '/api/itinerary/' + itineraryId + '/service?in_operation=1',
        }).success(function (data) {
            if (hot_tables_{{$itinerary->id}} == null) {
                renderServices(data);
            } else {
                reloadItineraryServices(data);
            }
        })
    }

    $(document).ready(function () {
        providerServices = getItineraryProviderServices({{$itinerary->id}});
    });

    $(".delete_service").on("click", function () {
        return confirm("Do you want to delete this service?");
    });


    function serviceStatusRenderer(instance, td, row, col, prop, value, cellProperties) {
        var instance2 = JSON.stringify(instance.getData()[row]);
        newHtml = "";
        @if(\Illuminate\Support\Facades\Auth::user()->can('Operation'))
            if (value == 'Requested') {
                newHtml += "<button onclick='changeServiceStatus(" + instance2 + ")' class='status label  bg-gray' type='button' data-value='" + instance2 + " " +
                    "' style='font-size:13px; color:white' data-status='Requested'>REQUESTED </button>";

            } else if (value == 'Canceled') {
                newHtml += "<button onclick='changeServiceStatus(" + instance2 + ")' class='status label bg-black' type='button' data-value='" + instance2 + " " +
                    "' style='font-size:13px; color:white' data-status='Canceled'>CANCELED </button>";
            }
        @else
            if (value == 'Requested') {
                newHtml += "<button class='status label  bg-gray' type='button' data-value='" + instance2 + " " +
                    "' style='font-size:13px; color:white' data-status='Requested'>REQUESTED </button>";

            } else if (value == 'Canceled') {
                newHtml += "<button class='status label bg-black' type='button' data-value='" + instance2 + " " +
                    "' style='font-size:13px; color:white' data-status='Canceled'>CANCELED </button>";
            }
        @endif
        td.innerHTML = newHtml;
    }

    function deleteServiceRenderer(instance, td, row, col, prop, value, cellProperties) {

                @if(\Illuminate\Support\Facades\Auth::user()->can('Operation'))
        var instance2 = JSON.stringify(instance.getData()[row]);
        newHtml = "<button href='' onclick='deleteService(" + instance2 + ")' class ='btn btn-sd btn-danger' " +
            "style='padding: 3px 7px;font-size: 10px;margin: 2px;'> <i class='fa fa-trash-o'></i></button>";
        td.innerHTML = newHtml;

        @endif
    }

    function deleteService(item) {
        if (confirm('Are you sure you want to delete this service?')) {
            removeItineraryServicesCount({{$itinerary->id}});

            $.ajax({
                type: "DELETE",
                url: '/api/itinerary/{{$itinerary->id}}/service/' + item[0],
            }).success(function (data) {
                getItineraryProviderServices({{$itinerary->id}});
            }).error(function (data) {
                console.log(data);
                return alert(data.responseJSON.message);
            });
        }
    }

    function changeServiceStatus(item) {
        var status = item[3];
        if (status == "Requested") {
            item[3] = "Canceled";
        } else {
            item[3] = "Requested";
        }
        updateService(item);
    }

    function updateService(item) {
        console.log(item);
        $.ajax({
            type: "PUT",
            url: '/api/itinerary/{{$itinerary->id}}/service/' + item[0],
            data: {
                'operation_status': item[3],
                'operation_quantity': item[4]
            },
        }).success(function (data) {
            getItineraryProviderServices({{ $itinerary->id }});
        }).error(function (data) {
            PNotify.removeAll();
            new PNotify({
                text: data.responseJSON.message,
                type: "error"
            });
        });
    }


    function removeItineraryServicesCount(itineraryId) {
        if (parseInt($('#services-count-' + itineraryId).text()) === 1) {
            $('#services-count-' + itineraryId).hide();
            document.getElementById('itineraryWithServices-' + itineraryId).style.backgroundColor = "#f4f4f4";
            document.getElementById('itineraryWithServices-' + itineraryId).style.borderColor = "#f4f4f4";
            document.getElementById('truck-' + itineraryId).style.color = "black";

        }
        $('#services-count-' + itineraryId).text(parseInt($('#services-count-' + itineraryId).text()) - 1);
    }

</script>

