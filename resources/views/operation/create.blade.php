@extends('backpack::layout')

@stack('styles_stack')

@section('after_styles')
    <link href="{{ asset('vendor/adminlte/plugins/select2/select2.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css"
          rel="stylesheet" type="text/css"/>

    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"/>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>


@endsection

@section('title')
    Operation
@endsection

@section('header')
    <section class="content-header">
        <h1>Operation</h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/admin/dashboard')}}">Admin</a></li>
            <li><a href="{{url('/admin/operation')}}" class="text-capitalize">Operation</a></li>
            <li class="active">Create</li>
        </ol>

    </section>
@endsection()

@section('content')
    @include('crud::inc.grouped_errors')
    <div class="nav">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs pull-right">

                <li class="edit" onclick="">
                    <a href="#tab_documents" data-toggle="tab">Documents</a>
                </li>
                <li class="active" class="edit" onclick="">
                    <a href="#tab_general" data-toggle="tab">General</a>
                </li>
            </ul>

            <form action="{{route('crud.operation.store')}}" method="POST" enctype="multipart/form-data">
                @include('operation._fields')
                <div class="box-footer">

                    <div id="saveActions" class="form-group">
                        <button type="submit" class="btn btn-success">
                            <span class="fa fa-save" role="presentation" aria-hidden="true"></span>
                            Create
                        </button>

                        <a href="{{url('/admin/operation')}}" class="btn btn-default"><span class="fa fa-ban"></span>
                            &nbsp;Cancel</a>
                    </div>
                </div>
            </form>


        </div>
    </div>

@endsection



@section('after_scripts')
    <script src="{{ asset('vendor/adminlte/plugins/select2/select2.min.js') }}"></script>
    <script src="{{ asset('vendor/adminlte/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendor/adminlte/plugins/daterangepicker/moment.min.js') }}"></script>
    <script type="text/javascript"
            src="https://cdn.jsdelivr.net/bootstrap.datetimepicker/4.17.42/js/bootstrap-datetimepicker.min.js"></script>

    <script>

        $(document).ready(function () {
            if ($('.select2').length > 0) {
                $('.select2').select2();
            }
        });

        $(function () {
            var dateNow = new Date();
            $('.datepicker').datetimepicker({
                toolbarPlacement: 'bottom',
                showClose: true,
                viewMode: 'years',
                format: 'MM-YY',
            });
        });


        var data1;
        jQuery(function ($) {


            $("#operation-aircraft-id").on("change", function () {
                var aircraft_registration = $("#operation-aircraft-id option:selected").val();

                $.ajax({
                    url: '{{url('/admin/aircraft/info')}}' + '/' + aircraft_registration,
                    type: 'GET',
                    dataType: "json",
                    success: function (data) {
                        if (data['client_id']) {
                            data1 = data;
                            $("#aircraft_client_id").val(data['client_id']);
                        } else {
                            $("#aircraft_client_id").val("");
                        }
                        if (data['operator_id']) {
                            data1 = data;
                            $("#operator-id").val(data['operator_id']);
                        } else {
                            $("#operator-id").val("");
                        }
                        if (data['mtow']) {
                            data1 = data;
                            $("#mtow").val(data['mtow']);
                        } else {
                            $("#mtow").val("");
                        }
                        if (data['mtow_type']) {
                            data1 = data;
                            $("#mtow_type").val(data['mtow_type']);
                        } else {
                            $("#mtow_type").val("");
                        }
                        if (data['aircraft_type']) {
                            $("#aircraft_type").val(data['aircraft_type']);
                        } else {
                            $("#aircraft_type").val("");
                        }

                    },
                    error: function (result) {
                        alert('error');
                    }
                });


            });
        });

        function showMTOW() {
            jQuery(function ($) {


                var aircraft_registration = $("#operation-aircraft-id option:selected").val();

                $.ajax({
                    url: '{{url('/admin/aircraft/info')}}' + '/' + aircraft_registration,
                    type: 'GET',
                    dataType: "json",
                    success: function (data) {

                        if (data['mtow']) {
                            $("#mtow").val(data['mtow']);
                        } else {
                            $("#mtow").val("");
                        }
                        if (data['mtow_type']) {
                            $("#mtow_type").val(data['mtow_type']);
                        } else {
                            $("#mtow_type").val("");
                        }
                        if (data['aircraft_type']) {
                            $("#aircraft_type").val(data['aircraft_type']);
                        } else {
                            $("#aircraft_type").val("");
                        }

                    },
                });
            });
        }

    </script>


    @stack('script_stack')

@endsection