@extends('backpack::layout')
@stack('styles_stack')
@section('title')

@endsection

@section('after_styles')
    <link rel="stylesheet" href="{{ asset('/js/handsontable.full.min.css') }}"/>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>

@endsection

@section('header')
    @include('operation.partials.operationNavBar', ['operation'=>$operation])
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <section class="content-header">
        <h1>Edit Operation</h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/admin/dashboard')}}">Admin</a></li>
            <li><a href="{{url('/admin/operation')}}" class="text-capitalize">Operation</a></li>
            <li class="active">Edit</li>
        </ol>

    </section>
@endsection()
@php
    $currentUser = \Illuminate\Support\Facades\Auth::user();
@endphp
@section('content')
    @include('crud::inc.grouped_errors')
    <div class="row">
        <div class="col-md-12">
            <div id="app">

                <active-users></active-users>
            </div>

            <div class="nav">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs pull-right">

                        <li class="edit" onclick="">
                            <a href="#tab_documents" data-toggle="tab">Documents</a>
                        </li>
                        <li class="active" class="edit" onclick="">
                            <a href="#tab_general" data-toggle="tab">General</a>
                        </li>
                    </ul>
                    <form action="{{route('crud.operation.update', ['operation'=>$operation->id])}}" method="POST"
                          enctype="multipart/form-data">
                        @include('operation._fields')
                        <div class="box-footer">
                            @if($currentUser->can('Operation'))

                                <div id="saveActions" class="form-group" style="margin-left: 27px">
                                    <button type="submit" class="btn btn-success">
                                        <span class="fa fa-save" role="presentation" aria-hidden="true"></span>
                                        Save
                                    </button>

                                    <a href="{{url('/admin/operation')}}" class="btn btn-default"><span></span>
                                        &nbsp;Back</a>
                                </div>
                            @endif

                        </div>
                    </form>
                </div>
                <div class="box col-md-12">
                    <div class="col-md-12">
                        <div class="box-body">
                            @include('operation.partials.itinerary', ['itineraries'=>$operation->activeItineraries], ['operation'=>$operation])
                            @include('operation.partials.permissions', ['operation'=>$operation])
                        </div>

                    @if($operation->quotation_id!=0)
                        @include('quotation.quotation-modals.quotation-modal', ['operation'=>$operation, 'entity'=>'operation'])
                    @endif


                    <!-- Default bootstrap modal example -->
                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                             aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"
                                                aria-label="Close"><span
                                                    aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                                    </div>
                                    <div class="modal-body">
                                        ...
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                        </button>
                                        <button type="button" class="btn btn-primary">Save changes</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @endsection



        @push('stack_after_scripts')

            <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />
            <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>
            <link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css"
                  rel="stylesheet" type="text/css"/>
            <link rel="stylesheet"
                  href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"/>

            <script type="text/javascript"
                    src="{{ asset('vendor/adminlte/plugins/daterangepicker/moment.min.js') }}"></script>
            <script src="{{ asset('vendor/adminlte/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
            <script type="text/javascript"
                    src="https://cdn.jsdelivr.net/bootstrap.datetimepicker/4.17.42/js/bootstrap-datetimepicker.min.js"></script>

            <script src="{{ asset('/js/handsontable.full.min.js') }}"></script>
            <script>

                window.Laravel = {!! json_encode([
            'csrfToken'=> csrf_token(),
            'user'=> [
                'authenticated' => auth()->check(),
                'id' => auth()->check() ? auth()->user()->id : null,
                'name' => auth()->check() ? auth()->user()->name : null,
                'operation_id' => $operation->id
                ]
            ])
        !!};


                jQuery(function ($) {


                    $(".ajax-modal").on("show.bs.modal", function (e) {
                        var link = $(e.relatedTarget);
                        $(this).find(".modal-body").load(link.attr("href"));
                    });
                    $(".modal").on('hidden.bs.modal', function () {
                        $(this).data('bs.modal', null);
                    });

                });

                $(document).ready(function () {
                    if ($('.select2').length > 0) {
                        $('.select2').select2();
                    }
                    if (document.getElementById('second_operation_captain_id').value != "") {
                        addCrew();
                    }
                    showMTOW();
                    preLoadAircraftType();
                    alertSpecialFeatures();

                });

                $(function () {
                    var dateNow = new Date();
                    $('.datepicker').datetimepicker({
                        toolbarPlacement: 'bottom',
                        showClose: true,
                        viewMode: 'years',
                        format: 'MM-YY',
                        defaultDate: dateNow

                    });
                });

                function addCrew() {
                    document.getElementById('second_crew').style.display = '';
                    document.getElementById('add_crew').style.display = 'none';
                    document.getElementById('hide_crew').style.display = '';
                }

                function hideCrew() {
                    document.getElementById('second_crew').style.display = 'none';
                    document.getElementById('add_crew').style.display = '';
                    document.getElementById('hide_crew').style.display = 'none';
                }

                function showMTOW() {
                    jQuery(function ($) {


                        var aircraft_registration = $("#operation-aircraft-id option:selected").val();

                        $.ajax({
                            url: '{{url('/admin/aircraft/info')}}' + '/' + aircraft_registration,
                            type: 'GET',
                            dataType: "json",
                            success: function (data) {

                                if (data['mtow']) {
                                    $("#mtow").val(data['mtow']);
                                } else {
                                    $("#mtow").val("");
                                }
                                if (data['mtow_type']) {
                                    $("#mtow_type").val(data['mtow_type']);
                                } else {
                                    $("#mtow_type").val("");
                                }
                                if (data['aircraft_type']) {
                                    $("#aircraft_type").val(data['aircraft_type']);
                                } else {
                                    $("#aircraft_type").val("");
                                }

                            },
                        });
                    });
                }

                function preLoadAircraftType() {
                    @if( $operation->aircraftType)
                    $("#aircraft_type").val('{{ $operation->aircraftType->name }}');
                    @endif
                }

                $(".file-clear-button").click(function (e) {
                    e.preventDefault();
                    var container = $(this).parent().parent();
                    var parent = $(this).parent();
                    // remove the filename and button
                    parent.remove();
                    console.log(container);
                    // if the file container is empty, remove it
                    if ($.trim(container.html()) == '') {
                        container.remove();
                    }
                    $("<input type='hidden' name='clear_documents[]' value='" + $(this).data('filename') + "'>").insertAfter("#documents_file_input");
                });

                $("#documents_file_input").change(function () {
                    console.log($(this).val());
                    // remove the hidden input, so that the setXAttribute method is no longer triggered
                    $(this).next("input[type=hidden]").remove();
                });
            </script>
            <script src="{{ asset('js/app.js') }}" defer></script>

    @stack('script_stack')
    @endpush