
<div class="operation-info">
    <section class="content-header" style="padding: 0px 15px 0 15px;">
        <div id="app">
            <span>
                @if($operation->aircraft)
                <h3 style="float:left">{{$operation->code}} - Aircraft {{$operation->aircraft->registration}}</h3>
                <active-users operation-id="{{$operation->id}}"></active-users>
                    @else
                    <h3 style="float:left">{{$operation->code}}
                    <active-users operation-id="{{$operation->id}}"></active-users>
                @endif
            </span>
        </div>
    </section>
</div>
@push('script_stack')
<script>

    $(document).ready(function(){
        $(".operation-info").hide(); //Hide the navigation bar first

        $(window).scroll(function () {  //Listen for the window's scroll event
            if (isScrolledAfterElement("#itineraryModal")) { //if it has scrolled beyond the #content elment
                $('.operation-info').fadeIn(300);  //Show the navigation bar
            } else {
                $('.operation-info').fadeOut(300); //Else hide it
            }
        });

        //Function that returns true if the window has scrolled beyond the given element
        function isScrolledAfterElement(elem) {
            var $elem = $(elem);
            var $window = $(window);

            var docViewTop = $window.scrollTop();
            var docViewBottom = $window.height() - docViewTop ;

            var elemTop = $elem.offset().top;

            return elemTop > docViewBottom;
        }
    });
</script>
@endpush