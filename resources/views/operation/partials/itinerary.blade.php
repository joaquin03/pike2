@php
    $currentUser = \Illuminate\Support\Facades\Auth::user();
@endphp
@if($currentUser->can('Operation'))
    <a href="{{ route('crud.operation.itinerary.create', ['operation'=>$operation->id]) }}"
       data-toggle="modal" data-target="#itineraryModal" class="btn btn-success ajax-modal"
       style="margin-bottom: 10px;">
        Add Itinerary
    </a>
@endif
<table id="crudTable" class="table table-bordered table-striped display" style="width:100%; font-size: 17px">
    <thead>
    <th style="width: 5%"></th>
    <th style="width: 5%">Type</th>
    <th style="width: 5%">Airport</th>
    <th style="width: 7%">Date</th>
    <th style="width: 7%">Time</th>
    <th style="width: 20%; text-align: center;">Handler</th>
    <th style="width: 2%;">Status</th>
    </thead>
    <tbody>
    @foreach($itineraries as $key => $itinerary)

        <tr data-id="{{ $itinerary->id }}">
            @if($itinerary->canHaveServices())
                <td class="details-control text-center cursor-pointer" id="itinerary-{{$itinerary->id}}">
                    <i data-entry-id="{{ $itinerary->id }}"
                       class="fa fa-plus-square-o details-row-button cursor-pointer"></i>
                </td>
                <td>
                    <b><a href="{{ route('crud.operation.itinerary.edit', ['itinerary_id'=>$itinerary->id, 'operation_id'=>$operation->id]) }}"
                          data-toggle="modal" data-target="#myModal" id="estimated">{{ $itinerary->type }}</a></b>
                </td>
            @else
                <td></td>
                <td>{{ $itinerary->type }}</td>
            @endif


            <td>{{ $itinerary->airport->icao }}</td>

            <td>
                @if($itinerary->hasDate()) {{ \Carbon\Carbon::parse($itinerary->date)->format('d-m-Y')  }} @else
                    TBA @endif
            </td>
            <td>
                @if($itinerary->hasTime()){{ $itinerary->time }} UTC @else TBA @endif
            </td>
            <td style="text-align: center;">
                @if($itinerary->handler) {{ $itinerary->handler->name }} @else - @endif
            </td>
            <td style=" position: relative; display: inline-flex; ">

                @if($currentUser->can('Operation'))

                    <a class="btn btn-sd btn-info ajax-modal" style="margin-right: 5px;"
                       href="{{ route('crud.operation.itinerary.edit', ['itinerary_id'=>$itinerary->id, 'operation_id'=>$operation->id]) }}"
                       data-toggle="modal" data-target="#itineraryModal" id="editItinerary">
                        Edit</a>
                    <!-- TODO : DELETE -->
                    <a class="btn btn-sd btn-danger ajax-modal delete_itinerary" id="{{$itinerary->id}}"
                       style=" margin-right:5px;"
                       href="#"
                       data-toggle="cancel"
                       data-btn-ok-label="Cancel" data-btn-ok-icon="glyphicon glyphicon-ban-circle"
                       data-btn-ok-class="btn btn-sm btn-warning"
                       data-btn-cancel-label="Delete"
                       data-btn-cancel-icon="fa fa-remove"
                       data-btn-cancel-class="btn btn-sm btn-danger"
                       data-title=" "
                       data-placement="left" data-singleton="true">
                        <i class="fa fa-trash-o"></i>
                    </a>
                @endif
                @if($itinerary->isDeparture())


                @elseif($itinerary->hasServices())
                    <span class="btn btn-success"
                          id="itineraryWithServices-{{$itinerary->id}}"
                          style="margin-right:5px;">
                    <i class="fa fa-truck" id="truck-{{$itinerary->id}}" style=" display: inline-block; width:12px; ">
                        <span class="label label-warning" id="services-count-{{$itinerary->id}}"
                              style="zoom:1.1; margin-top:2.5px; font-family: Helvetica; position:absolute;">
                            {{ $itinerary->itineraryProviderServicesForOperations()->count()}}
                        </span>
                    </i>
                </span>

                @else

                    <span class="btn btn-default"
                          id="itineraryWithServices-{{$itinerary->id}}" style=" margin-right:5px;">
                    <i class="fa fa-truck" id="truck-{{$itinerary->id}}" style="color:#696969;width:15px;">
                        <span class="label label-warning" id="services-count-{{$itinerary->id}}"
                              style="zoom:1.1; margin-top:2.5px; font-family: Helvetica; position:absolute; display: none">
                            0
                        </span>
                    </i>
                    </span>
                @endif

                <a href="/admin/itinerary/{{$itinerary->id}}/revisions" class="btn"
                   style="background-color: #838384"
                   id="itineraryWithServices-{{$itinerary->id}}">
                    <i class="fa fa-info-circle" style="color:white;width:15px;"></i></a>

            </td>

        </tr>

    @endforeach
    </tbody>

</table>


<div class="box-footer">
</div>

<!-- Default bootstrap modal example -->
<div class="modal fade" id="itineraryModal" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="serviceModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


@push('styles_stack')
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/plugins/datepicker/datepicker3.css') }}">
@endpush

@push('script_stack')
    <script type="text/javascript"
            src="{{ asset('vendor/adminlte/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script type="text/javascript"
            src="{{ asset('vendor/adminlte/plugins/datatables/dataTables.bootstrap.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-confirmation/1.0.5/bootstrap-confirmation.js"></script>
    <script>

        jQuery(document).ready(function ($) {

            $('[data-toggle=cancel]').confirmation({
                rootSelector: '[data-toggle=cancel]',

                //Cancel element
                onConfirm: function (event, element) {
                    var itineraryId = element.attr('id');
                    var cancelMsg = confirm('Do you want to cancel this itinerary?');
                    if (cancelMsg) {
                        url = "{{ url('admin/operation')}}" + "/" + {{$operation->id}} +'/itinerary/' + itineraryId + '/cancel';
                        window.location.href = url;
                    }

                },
                //Delete element
                onCancel: function (event, element) {
                    var itineraryId = element.attr('id');
                    var deleteMsg = confirm('Do you want to delete this itinerary?');
                    if (deleteMsg) {
                        url = "{{ url('admin/operation')}}" + "/" + {{$operation->id}} +'/itinerary/' + itineraryId + '/delete';
                        window.location.href = url;
                    }
                }
            });


            var table = $("#crudTable").DataTable({
                "bPaginate": false,
                "searching": false,
                "aaSorting": [],

            });
            hideItinerary = function () {
                items = $('.details-row-button.fa-minus-square-o');
                for (var i = 0; i < items.length; i++) {
                    $(items[i]).click();
                }
            }

            // var crudTable = $('#crudTable tbody');
            // Remove any previously registered event handlers from draw.dt event callback
            $('#crudTable tbody').off('click', 'td .details-row-button');

            // Make sure the ajaxDatatables rows also have the correct classes
            $('#crudTable tbody td .details-row-button').parent('td')
                .removeClass('details-control').addClass('details-control')
                .removeClass('text-center').addClass('text-center')
                .removeClass('cursor-pointer').addClass('cursor-pointer');

            // Add event listener for opening and closing details
            $('#crudTable tbody td.details-control').on('click', function () {
                hideItinerary();
                var tr = $(this).closest('tr');
                var btn = $(this).find('.details-row-button');
                var row = table.row(tr);

                if (row.child.isShown()) {
                    // This row is already open - close it
                    btn.removeClass('fa-minus-square-o').addClass('fa-plus-square-o');
                    $('div.table_row_slider', row.child()).slideUp(function () {
                        row.child.hide();
                        tr.removeClass('shown');
                    });
                } else {
                    // Open this row
                    btn.removeClass('fa-plus-square-o').addClass('fa-minus-square-o');
                    // Get the details with ajax
                    $.ajax({
                        url: '{{ url("/admin/itinerary")}}/' + btn.data('entry-id') + '/',
                        type: 'GET',
                    })
                        .done(function (data) {
                            // console.log("-- success getting table extra details row with AJAX");
                            row.child("<div class='table_row_slider'>" + data + "</div>", 'no-padding').show();
                            tr.addClass('shown');
                            $('div.table_row_slider', row.child()).slideDown();
                        })
                        .fail(function (data) {
                            // console.log("-- error getting table extra details row with AJAX");
                            row.child("<div class='table_row_slider'>There was an error loading the details. Please retry.</div>").show();
                            tr.addClass('shown');
                            $('div.table_row_slider', row.child()).slideDown();
                        })
                        .always(function (data) {
                            // console.log("-- complete getting table extra details row with AJAX");
                        });
                }

            });


            showItineraryByUrl = function () {

                var url = $(location).attr('href');
                var itinerary = url.split('#')[1];
                if (itinerary != null) {
                    console.log(itinerary);
                    $("#" + itinerary).click();
                }
            };
            showItineraryByUrl();


        });

    </script>

    <script>

        $('#estimated').on('shown', function () {
            $("#time_select").click();
        });
    </script>


@endpush
