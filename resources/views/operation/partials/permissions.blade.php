<div id="permissionsForm" class="row">

    <div class="col-md-12">
        <div class="row-fluid summary">
            <div class="span1">
                <i class="fa fa-minus-square-o details-button" data-toggle="collapse" data-target="#intro"></i>
                <span class="h3">Permits</span>
            </div>


            <div class="row-fluid summary">
                <div id="intro" class="collapse in">
                    @if(\Illuminate\Support\Facades\Auth::user()->can('Operation'))
                        <div class="col-md-4">
                            <h3 style="padding-left:1%">Add new permit</h3>
                            @include('operation.forms.permissions')
                        </div>
                    @endif
                    <div class="col-md-8" style="padding-left: 0;padding-top:20px">
                        @include('operation.tables.permissions')
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>
@push('script_stack')
    <script>

        $('.details-control i').click(function () {
            $(this).text(function (i, old) {
                if (this.classList.contains("fa-minus-square-o")) {
                    $(this).removeClass('fa-minus-square-o').addClass('fa-plus-square-o');
                } else {
                    $(this).removeClass('fa-plus-square-o').addClass('fa-minus-square-o');

                }
            });
        });
    </script>
@endpush