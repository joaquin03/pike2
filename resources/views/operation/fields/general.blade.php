<div class="col-md-12">
    <h3 class="form-section" style="text-align: center;margin-bottom: 20px">GENERAL</h3>

    <div class="col-md-6">
        <div class="form-group col-md-12">
            <label class="control-label col-md-3">Code #</label>
            <div class="col-md-9">
                <input name="code" class="form-control" value="{{ $operation->code }}" disabled/>
            </div>
        </div>
        <div class="form-group col-md-12">
            <label class="control-label col-md-3">State</label>
            <div class="col-md-9">
                <select class="form-control select2" name="state" id="state">
                    @foreach(App\Models\Operation::$states as $state)
                        <option value="{{$state}}"
                                {{(isset($operation) && $operation->state == $state)? 'selected="selected"' : ''}}
                        >
                            {{ $state }}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group col-md-12">
            <label class="control-label col-md-3">Starting date</label>
            <div class="col-md-9">

                @if ($creating ?? false)

                    <div class='input-group date datepicker' id="time">
                        <input name="start_date" type='text' class="form-control"
                               value="{{ $operation->start_date }}"/>
                        <span id="time-button" class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span></span>
                    </div>
                @else
                    <div class='input-group date datepicker form-group col-md-12' id="time">
                        <input name="code" class="form-control" disabled value="{{ $operation->getStartDate() }}"/>
                    </div>
                @endif


            </div>
        </div>

        <div class="form-group col-md-12">
            <label class="control-label col-md-3">Ref #</label>
            <div class="col-md-9">
                <input name="ref_number" type="text" class="form-control" autocomplete="off"
                       value='{{$operation->ref_number}}'>
            </div>
        </div>

        <div class="form-group col-md-12">
            <label class="control-label col-md-3">Quotation Notes:</label>
            <div class="col-md-9">
                <pre class="text-wrap" id="quotation-notes">{{$operation->quotation_notes}}</pre>
                <div class="col-md-12" style="float: right;">
                    <button type="button" class="btn btn-default" data-toggle="collapse"
                            onclick="showMore('quotation-notes')" id="more-quotation-notes"
                            style="float: right; display: none">Show more
                    </button>
                    <button type="button" class="btn btn-default" data-toggle="collapse"
                            onclick="showLess('quotation-notes')" id="less-quotation-notes"
                            style="float: right;display: none;">Show less
                    </button>
                </div>
            </div>

        </div>
        <div class="form-group col-md-12">
            <label class="control-label col-md-3">General Notes:</label>
            <div class="col-md-9">
                <pre class="text-wrap" id="notes">{{$operation->notes}}</pre>
                <div class="col-md-12" style="float: right;">
                    <button type="button" class="btn btn-default" data-toggle="collapse"
                            onclick="showMore('notes')" id="more-notes"
                            style="float: right; display: none">Show more
                    </button>
                    <button type="button" class="btn btn-default" data-toggle="collapse"
                            onclick="showLess('notes')" id="less-notes"
                            style="float: right;display: none;">Show less
                    </button>
                </div>
            </div>
        </div>
        <div class="form-group col-md-12">
            <label class="control-label col-md-3">Operation Notes</label>
            <div class="col-md-9">
                <textarea name="operation_notes" class="form-control round-box"> {{$operation->operation_notes}}</textarea>
            </div>
        </div>


        <div class="col-md-12">
            <label class="control-label col-md-3">Route</label>
            <div class="col-md-9">
                <textarea name="route" class="form-control round-box"> {{$operation->route}}</textarea>
            </div>
        </div>
    </div>

    <div class="col-md-6">

        <div class="form-group col-md-12">
            <label class="control-label col-md-3">Registration</label>
            <div class="col-md-9">
                <select class="form-control select2" name="aircraft_id" id="operation-aircraft-id"
                        onChange="showMTOW()">
                    <option value="" disabled="disabled" selected="selected">Select the Aircraft</option>
                    @foreach(App\Models\Aircraft::all() as $aircraft)
                        <option value="{{$aircraft->id}}"
                                {{(isset($operation) && $operation->aircraft_id == $aircraft->id)? 'selected="selected"' : ''}}
                        >
                            {{$aircraft->registration}}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group col-md-12">
            <label class="control-label col-md-3">Operator</label>
            <div class="col-md-9">
                <select class="form-control select2" name="aircraft_operator_id" id="operator-id">
                    <option value="" disabled="disabled" selected="selected">Select the Operator</option>
                    @foreach(App\Models\Operator::all() as $operator)
                        <option value="{{$operator->id}}"
                                {{(isset($operation) && $operation->aircraft_operator_id == $operator->id)? 'selected="selected"' : ''}}
                        >
                            {{$operator->name}}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group col-md-12">
            <label class="control-label col-md-3">Client</label>
            <div class="col-md-9">
                <select class="form-control select2" name="aircraft_client_id" id="aircraft_client_id"
                        >
                    <option value="" disabled="disabled" selected="selected">Select the Client</option>
                    @foreach(App\Models\Company::all() as $client)
                        <option value="{{$client->id}}"
                                {{(isset($operation) && $operation->aircraft_client_id == $client->id)? 'selected="selected"' : ''}}>
                            {{$client->name}}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group col-md-12">
            <label class="control-label col-md-3">Aircraft Type</label>
            <div class="col-md-9">
                <input id="aircraft_type" name="aircraft_type" type="text" class="form-control" autocomplete="off"
                       disabled
                       value=" ">
            </div>
        </div>

        <div class="form-group col-md-12">
            <label class="control-label col-md-3">MTOW</label>
            <div class="col-md-3">
                <input id="mtow" name="mtow" type="text" class="form-control" autocomplete="off" disabled
                       value=" ">
            </div>
            <label class="control-label col-md-3" style="text-align: center">MTOW Type</label>
            <div class="col-md-3">
                <input id="mtow_type" name="mtow_type" type="text" class="form-control" autocomplete="off" disabled
                       value=" ">
            </div>
        </div>
    </div>

    @include('operation.partials.crewMembers')
</div>

@include('general.specialFeaturesModal')


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript">

    $(document).ready(function () {
        hideButton('notes');
        hideButton('quotation-notes');


        alertSpecialFeatures();
    });

    function hideButton(section) {
        if ($('#' + section).html().split(/\n/).length > 2) {
            $('#more-' + section).show();
        }
    }

    function showMore(section) {
        document.getElementById(section).style.height = 'auto';
        document.getElementById(section).style.maxHeight = '400px';
        $('#more-' + section).hide();
        $('#less-' + section).show();

    }

    function showLess(section) {
        document.getElementById(section).style.height = '35px';
        $('#less-' + section).hide();
        $('#more-' + section).show();
    }

    function alertSpecialFeatures() {
        var company_id = $('#aircraft_client_id').val();
        var current_url = window.location.href;
        if (company_id && current_url.split('#')[1] != 'documents') {
            $.ajax({
                url: '{{url('/admin/company/alert/special-features')}}' + '/' + company_id,
                type: 'GET',
                dataType: "json",
                success: function (data) {
                    if (data['special_features']) {
                        $("#client-input-id").val(company_id);
                        $("#specialFeaturesModal").modal();

                    }
                },
                error: function (result) {
                    alert('error');
                }
            });

        }

    }

</script>

<style>
    .select2 {
        width: 100% !important;
    }
</style>