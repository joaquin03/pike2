@extends("backpack::layout")


@section('header')

  <section class="content-header">
    <h1>
      404 Page not found.
    </h1>
    <ol class="breadcrumb">
      <li>
        <a href="{{ url(config('backpack.base.route_prefix', 'admin')) }}">{{ config('backpack.base.project_name') }}</a>
      </li>
      <li class="active">{{ trans('backpack::base.dashboard') }}</li>
    </ol>
  </section>
  <style>
    a {
      color: #A9A9A9
    }

    a:hover {
      color: #696969;
    }
  </style>

@endsection


@section('content')

  <div class="col-sm-12">

    <div class="box box-body">
      <div class="explanation">
        <br>
        <small>
            <?php
            $default_error_message = "Please return to <a href='".url('')."'>our homepage</a>.";
            ?>
          {!! isset($exception)? ($exception->getMessage()?$exception->getMessage():$default_error_message): $default_error_message !!}
        </small>
      </div>
    </div>
  </div>







@endsection

