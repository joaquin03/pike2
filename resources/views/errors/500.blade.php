<html>
  <head>
    <title>{{ config('backpack.base.project_name') }} Error 500</title>

    <link href='//fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>

    <style>
      body {
        margin: 0;
        padding: 0;
        width: 100%;
        height: 100%;
        color: #B0BEC5;
        display: table;
        font-weight: 100;
        font-family: 'Lato';
      }

      .container {
        text-align: center;
        display: table-cell;
        vertical-align: middle;
      }

      .content {
        text-align: center;
        display: inline-block;
      }

      .title {
        font-size: 156px;
      }

      .quote {
        font-size: 36px;
      }

      .explanation {
        font-size: 24px;
      }
    </style>
  </head>
  <body>
    <div class="container">
      <div class="content">
        <div class="title">500</div>
        <div class="quote">It's not you, it's me.</div>
        <div class="explanation">
          <br>
          <small>
            <?php
              $default_error_message = "An internal server error has occurred. If the error persists please contact the development team.";
            ?>
            {!! isset($exception)? ($exception->getMessage()?$exception->getMessage():$default_error_message): $default_error_message !!}
         </small>
       </div>
      </div>
    </div>
  </body>
</html>



@extends("backpack::layout")


@section('header')

  <section class="content-header">
    <h1>
      500 It's not you, it's me.
    </h1>
    <ol class="breadcrumb">
      <li>
        <a href="{{ url(config('backpack.base.route_prefix', 'admin')) }}">{{ config('backpack.base.project_name') }}</a>
      </li>
      <li class="active">{{ trans('backpack::base.dashboard') }}</li>
    </ol>
  </section>
  <style>
    a {
      color: #A9A9A9
    }

    a:hover {
      color: #696969;
    }
  </style>

@endsection


@section('content')

  <div class="col-sm-12">

    <div class="box box-body">
      <div class="explanation">
        <br>
        <small>
            <?php
            $default_error_message = "An internal server error has occurred. If the error persists please contact the development team.";
            ?>
          {!! isset($exception)? ($exception->getMessage()?$exception->getMessage():$default_error_message): $default_error_message !!}
        </small>
      </div>
    </div>
  </div>







@endsection

