@extends("backpack::layout")


@section('header')

    <section class="content-header">
        <h1>
            {{ trans('backpack::base.dashboard') }}
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url(config('backpack.base.route_prefix', 'admin')) }}">{{ config('backpack.base.project_name') }}</a>
            </li>
            <li class="active">{{ trans('backpack::base.dashboard') }}</li>
        </ol>
    </section>
    <style>
        a {
            color: #A9A9A9
        }

        a:hover {
            color: #696969;
        }
    </style>

@endsection


@section('content')
    <div class="hidden">
        @if($errors->any())
            {{\Alert::error($errors->first())->flash()}}
        @endif
    </div>
    <div class="col-lg-6 col-xs-12 col-sm-12">

        <div class="box box-body">
            <div class="portlet-title" style="font-size: 25px; padding-left: 20px; padding-top: 10px;">
                <div class="caption ">
                    <i class="icon-share font-dark hide"></i>
                    <span class="caption-subject font-dark bold uppercase ">
                            You have
                            <span class="badge badge-default"> {{ count($dashboardReminders) }} </span>
                            Team Recent Reminders</span>
                </div>
            </div>
            <hr>


            <div class="box-body">
            @foreach($dashboardReminders as $reminder)

                <!-- See dist/js/pages/dashboard.js to activate the todoList plugin -->
                    <ul class="todo-list" style="padding: 2px; font-size:18px">
                        <li>
                            <div class="row" style="margin: 0;">
                                <div class="col-lg-1 col-xs-2 col-sm-2">
                                    <i class="fa fa-bell-o icon-reminder {{ $reminder->getPriorityColor() }}"></i>
                                </div>

                                <div class="col-lg-11 col-xs-10 col-sm-10">


                                    <a style="color: #595959;" onmouseout="this.style.color='#595959';"
                                       @if($reminder->operation_link)
                                       onmouseover="this.style.color='blue';"
                                       href={{url(config('backpack.base.route_prefix', 'admin')).$reminder->operation_link }} @endif
                                               class="text">{{ $reminder->title}}
                                    </a>
                                    <br>

                                    <a style="color: #595959;" onmouseout="this.style.color='#595959';"
                                       onmouseover="this.style.color='blue';" href="{{ route('crud.activityReminder.edit',
                                                                ['company'=>$reminder->activity->company_id, 'reminder'=>$reminder]) }}"
                                       style="font-size:15px">
                                        Assigned to: {{ $reminder->user->name}} </a>
                                    <!-- Emphasis label -->
                                    <span class="text {{ $reminder->getPriorityColor() }}"
                                          style="font-size: 13px; padding: 3px"><i class="fa fa-clock-o"
                                                                                   style="padding-right: 2px"></i>
                                        {{ $reminder->getDueDate()->diffForHumans() }}</span>


                                    <!-- General tools such as edit or delete-->
                                    <div class="tools">
                                        <a href="{{ route('crud.activityReminder.finish',
                                                                ['company'=>$reminder->activity->company_id, 'reminder'=>$reminder]) }}">
                                            Mark as Finished <i class="fa fa-share"></i>
                                        </a>
                                    </div>
                                </div>

                            </div>

                        </li>
                    </ul>
                @endforeach
            </div>
        </div>
    </div>

    <div class="col-lg-6 col-xs-12 col-sm-12">

        <div class="box box-body">
            <div class="portlet-title" style="font-size: 25px; padding-left: 20px; padding-top: 10px;">
                <div class="caption ">
                    <i class="icon-share font-dark hide"></i>
                    <span class="caption-subject font-dark bold uppercase ">
                            You have
                            <span class="badge badge-default"> {{ count($contactReminders) }} </span>
                            Recent Reminders</span>
                </div>
            </div>
            <hr>


            <div class="box-body">
            @foreach($contactReminders as $reminder)

                <!-- See dist/js/pages/dashboard.js to activate the todoList plugin -->
                    <ul class="todo-list" style="padding: 2px; font-size:18px">
                        <li>
                            <div class="row" style="margin: 0;">

                                <div class="col-lg-1 col-xs-2 col-sm-2" style="float:right">
                                    {{--<a id="deleteReminder"--}}
                                    {{--href="{{route('crud.activityReminder.remove', ['company_id' => $reminder->getCompany(),'reminder_id' => $reminder->id])}}">--}}
                                    {{--<i class="glyphicon glyphicon-remove"> </i></a>--}}
                                </div>

                                <div class="col-lg-1 col-xs-2 col-sm-2">

                                    <i class="fa fa-bell-o icon-reminder {{ $reminder->getPriorityColor() }}"></i>
                                </div>
                                <!-- todo text -->
                                <div class="col-lg-11 col-xs-10 col-sm-10">



                                <a style="color: #595959;" onmouseout="this.style.color='#595959';"
                                       @if($reminder->operation_link)
                                       onmouseover="this.style.color='blue';"
                                       href={{url(config('backpack.base.route_prefix', 'admin')).$reminder->operation_link }} @endif
                                               class="text">{{ $reminder->title}}
                                    </a> <br>

                                    <a style="color: #595959;" onmouseout="this.style.color='#595959';"
                                       onmouseover="this.style.color='blue';" href="{{ route('crud.activityReminder.edit',
                                                                ['company'=>$reminder->activity->company_id, 'reminder'=>$reminder]) }}"
                                       style="font-size:15px">
                                        Assigned to: {{ $reminder->user->name}} </a>
                                    <!-- Emphasis label -->
                                    <span class="text {{ $reminder->getPriorityColor() }}"
                                          style="font-size: 13px; padding: 3px"><i class="fa fa-clock-o"
                                                                                   style="padding-right: 2px"></i>{{ $reminder->getDueDate()->diffForHumans() }}</span>
                                    <!-- General tools such as edit or delete-->

                                </div>

                            </div>

                        </li>
                    </ul>
                @endforeach
            </div>
        </div>
    </div>







@endsection
