@extends('backpack::layout')

@section('styles')
    <link href="{{ asset('assets/admin/pages/css/profile.css')}}" rel="stylesheet" type="text/css"/>

@endsection

@section('title')
    Crew Member : {{ $crewMember->name }}
@endsection

@section('content')
    <section class="content-header">
        <h1>
            Crew Member : {{$crewMember->name}}
        </h1>

    </section>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-body table-responsive">


                </div>

            </div>







            </div>
        </div>
    </div>

@endsection

@section('after_scripts')
    @include('partials.scripts.delete-scripts')
@endsection