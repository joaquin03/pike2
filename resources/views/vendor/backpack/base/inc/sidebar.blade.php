@if (Auth::check())
    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="https://placehold.it/160x160/00a65a/ffffff/&text={{ mb_substr(Auth::user()->name, 0, 1) }}"
                         class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>{{ Auth::user()->name }}</p>
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/dashboard') }}"><i
                                class="fa fa-dashboard"></i> <span>{{ trans('backpack::base.dashboard') }}</span></a>
                </li>

                {{--<li class = "header">CRM</li>--}}
                <a href="#">
                    <li class="header" style="font-size: 14px">CRM</li>
                </a>
                <li style="background: #2c3b41"><a href="{{ url('admin/company') }}"><i class="fa fa-tag"></i> <span>Companies</span></a>
                </li>
                <li style="background: #2c3b41"><a href="{{ url('admin/operator') }}"><i class="fa fa-suitcase"></i>
                        <span>Operators</span></a></li>
                <li style="background: #2c3b41"><a href="{{ url('admin/provider') }}"><i class="fa fa-share-alt"></i>
                        <span>Providers</span></a></li>
                <li style="background: #2c3b41"><a href="{{ url('admin/client') }}"><i class="fa fa-user"></i> <span>Clients</span></a>
                </li>
                <li style="background: #2c3b41"><a href="{{ url('admin/contact') }}"><i class="fa fa-book"></i> <span>Contacts</span></a>

                @if(Auth::user()->can('Tag-Report'))
                    <li style="background: #2c3b41"><a href="{{ url('admin/company-tag') }}"><i class="fa fa-tag"></i>
                            <span>Company Tags</span></a></li>
                    <li style="background: #2c3b41"><a href="{{ url('admin/report') }}"><i class="fa fa-area-chart"></i>
                            <span>Tag Report</span></a></li>
                @endif

                @if(Auth::user()->can('CRM-Advance'))
                    <li style="background: #2c3b41"><a href="{{ url('admin/report-crm') }}"><i class="fa fa-area-chart"></i>
                            <span>Report CRM - Audit</span></a></li>
                @endif



                @if(Auth::user()->can('Quotation'))
                    <li class="treeview @if(Auth::user()->hasRole('Quotation')) active @endif">
                        <a href="#">
                            <i class="fa fa-expeditedssl"></i> <span>Quotation</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu @if(Auth::user()->hasRole('Quotation')) menu-open @endif">
                            <li>
                                <a href="{{ url('admin/quotation') }}">
                                    <i class="fa  fa-expeditedssl"></i><span>Quotation</span>
                                </a>
                            </li>
                            </li>
                            <li style="background: #2c3b41"><a href="{{ url('admin/sales-manager') }}"><i class="fa fa-suitcase"></i><span>Sales Manager</span></a>
                            </li>
                            <li style="background: #2c3b41"><a href="{{ url('admin/quotation/report/search') }}"><i class="fa fa-area-chart"></i>
                                    <span>Report</span></a></li>
                        </ul>
                    </li>
                @endif

            @if(Auth::user()->can('Billing'))
                <li class="treeview @if(Auth::user()->hasRole('Billing')) active @endif">
                    <a href="#"><i class="fa fa-expeditedssl"></i> <span>Billing</span> <i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu @if(Auth::user()->hasRole('Billing')) menu-open @endif"
                    ">
                <li><a href="{{ url('admin/billing') }}"><i class="fa  fa-expeditedssl"></i> <span>Billing</span></a>
                </li>
                <li><a href="{{ url('admin/company-billing-info') }}"><i class="fa fa-info-circle"></i> <span>Billing Info</span></a>
                </li>
                <li><a href="{{ url('admin/aircraft-default-service-price') }}"><i class="fa fa-money"></i> <span>Aircraft Service Price</span></a>
                </li>
                <li><a href="{{ url('admin/service-pike') }}"><i class="fa fa-truck"></i> <span>Services</span></a></li>
                <li><a href="{{ url('admin/client-statement') }}"><i class="fa fa-money"></i>
                        <span>Client Statement</span></a></li>
                <li><a href="{{ url('admin/bills') }}"><i class="fa fa-money"></i>
                        <span>Bills List</span></a></li>
                <li><a href="{{ url('admin/bill/report') }}"><i class="fa fa-balance-scale"></i> <span>Bills Report</span></a>
                </li>
                <li><a href="{{ url('admin/client-statement-report') }}"><i class="fa fa-area-chart"></i>
                        <span>Statements Report</span></a></li>
                <li><a href="{{ url('admin/clients-balance') }}"><i class="fa fa-dollar"></i>
                        <span>Clients Balance Report</span></a></li>
                </ul>
                </li>
            @endif

            @if(Auth::user()->can('Procurement'))
                <li class="treeview @if(Auth::user()->hasRole('Procurement')) active @endif">
                    <a href="#"><i class="fa fa-expeditedssl"></i> <span>Procurements</span> <i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu @if(Auth::user()->hasRole('Procurement')) menu-open @endif">
                        <li><a href="{{ url('admin/procurement') }}"><i class="fa  fa-expeditedssl"></i>
                                <span>Procurement</span></a></li>
                        <li><a href="{{ url('admin/service-procurement') }}"><i class="fa fa-truck"></i>
                                <span>Services</span></a></li>
                        <li><a href="{{ url('admin/provider-statement') }}"><i class="fa fa-money"></i> <span>Provider Statement</span></a>
                        </li>
                        <li><a href="{{ url('admin/procurement/report') }}"><i class="fa fa-bar-chart-o"></i>
                                <span>Report</span></a></li>
                        <li><a href="{{ url('admin/invoice/report') }}"><i class="fa fa-balance-scale"></i> <span>Invoice Report</span></a>
                        </li>
                        <li><a href="{{ url('admin/providers-payments/report') }}"><i class="fa fa-money"></i> <span>Payment Report</span></a>
                        </li>
                        <li><a href="{{ url('admin/provider-statement-report') }}"><i class="fa fa-bar-chart-o"></i>
                                <span>Providers Statements Report </span></a></li>
                        <li><a href="{{ url('admin/providers-balance') }}"><i class="fa fa-dollar"></i>
                                <span>Provider Balance Report</span></a></li>
                    </ul>
                </li>
            @endif

            @if(Auth::user()->can('Operation'))
                <li class="treeview @if(Auth::user()->hasRole('Operation')) active @endif">
                    <a href="#"><i class="fa fa-globe"></i> <span>Operations</span> <i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu @if(Auth::user()->hasRole('Operation')) menu-open @endif">
                        <li><a href="{{ url('admin/operation') }}"><i class="fa fa-globe"></i>
                                <span>Operations</span></a></li>
                        <li><a href="{{ url('admin/permanent-permission') }}"><i class="fa fa-tag"></i> <span>Permanent Permits</span></a>
                        </li>
                        <li><a href="{{ url('admin/crew-member') }}"><i class="fa fa-group"></i>
                                <span>Crew Members</span></a></li>
                        <li><a href="{{ url('admin/aircraft') }}"><i class="fa fa-fighter-jet"></i>
                                <span>Aircrafts</span></a></li>
                        <li><a href="{{ url('admin/aircraft-type') }}"><i class="fa fa-tag"></i>
                                <span>Aircraft Types</span></a></li>
                        <li><a href="{{ url('admin/airport') }}"><i class="fa fa-location-arrow"></i>
                                <span>Airports</span></a></li>
                        <li><a href="{{ url('admin/operation/report') }}"><i class="fa fa-bar-chart-o"></i>
                                <span>Report</span></a></li>
                        <li><a href="{{ url('admin/operation/screen') }}"><i class="fa fa-tv"></i>
                                <span>Operation List</span></a></li>

                        <li style="color: white;">Screen</li>
                        <li><a href="{{ url('admin/operation/screen?page=1&paginate=5') }}"><i class="fa fa-tv"></i>
                                <span>TV Screen 1</span></a></li>
                        <li><a href="{{ url('admin/operation/screen?page=2&paginate=5') }}"><i class="fa fa-tv"></i>
                                <span>TV2 Screen 2</span></a></li>

                    </ul>
                </li>
            @endif

            <li class="treeview">
                <a href="#"><i class="fa fa-cog"></i> <span>Config</span> <i
                            class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu menu-open">
                    @if(Auth::user()->can('Procurement'))
                        <li><a href="{{ url('admin/credit-card') }}"><i class="fa fa-credit-card"></i> <span>Credit Cards</span></a>
                        </li>
                    @endif
                    @if(Auth::user()->can('Fuel USA'))
                        <li>
                            <a href="{{ url('admin/fuel-usa') }}"><i class="fa fa-upload"></i> <span>Upload Fuel ListUSA</span></a>
                        </li>
                    @endif
                    @if(Auth::user()->can('Fuel Prices'))
                        <li>
                            <a href="{{ url('admin/fuel-price') }}"><i class="fa fa-upload"></i> <span>Upload Fuel WW List</span></a>
                        </li>
                    @endif
                    @if(Auth::user()->can('Quotation'))
                        <li>
                            <a href="{{ url('admin/quotations-credit-card-charges') }}"><i class="fa fa-upload"></i> <span>Quote Credit Note Charges</span></a>
                        </li>
                    @endif
                </ul>
            </li>

            <li class="treeview">
                <a href="#"><i class="fa fa-tint"></i> <span>Fuel</span> <i
                            class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu menu-open">
                    @if(Auth::user()->can('Fuel Upload'))
                        <li>
                            <a href="{{ url('admin/fuel-usa') }}"><i class="fa fa-file-text"></i> <span>Upload Fuel ListUSA</span></a>
                        </li>
                        <li>
                            <a href="{{ url('admin/fuel-price') }}"><i class="fa fa-file-text"></i> <span>Upload Fuel WW List</span></a>
                        </li>
                    @endif
                    @if(Auth::user()->can('Fuel Show'))
                        <li>
                            <a href="{{ url('admin/fuel-usa/show') }}"><i class="fa fa-file-text"></i> <span>Show Fuel ListUSA</span></a>
                        </li>
                        <li>
                            <a href="{{ url('admin/fuel-price/show') }}"><i class="fa fa-file-text"></i> <span>Show Fuel WW List</span></a>
                        </li>
                    @endif
                </ul>
            </li>


        @if(Auth::user()->can('User management'))

            <!-- ======================================= -->
                <li class="treeview">
                    <a href="#"><i class="fa fa-group"></i> <span>Users</span> <i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">

                        <li class="treeview">
                            <a href="#"><i class="fa fa-group"></i> <span>Users, Roles, Permissions</span> <i
                                        class="fa fa-angle-left pull-right"></i></a>
                            <ul class="treeview-menu">
                                <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/role') }}"><i
                                                class="fa fa-group"></i> <span>Roles</span></a></li>
                                <li>
                                    <a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/permission') }}"><i
                                                class="fa fa-file-text"></i> <span>Permissions</span></a></li>
                                <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/user') }}"><i
                                                class="fa fa-user"></i> <span>Users</span></a></li>
                            </ul>
                        </li>
                        @endif


                        <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/elfinder') }}"><i
                                        class="fa fa-files-o"></i> <span>File manager</span></a></li>
                        <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/log') }}"><i
                                        class="fa fa-terminal"></i> <span>Logs</span></a></li>
                        <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/logout') }}"><i
                                        class="fa fa-sign-out"></i>
                                <span>{{ trans('backpack::base.logout') }}</span></a></li>
                    </ul>
                    </ul>
                </li>
        </section>
        <!-- /.sidebar -->
    </aside>
@endif
