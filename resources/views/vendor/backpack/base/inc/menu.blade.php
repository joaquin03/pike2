<div class="navbar-custom-menu pull-left">
    <ul class="nav navbar-nav">
        <!-- =================================================== -->
        <!-- ========== Top menu items (ordered left) ========== -->
        <!-- =================================================== -->

    <!-- <li><a href="{{ url('/') }}"><i class="fa fa-home"></i> <span>Home</span></a></li> -->

        <!-- ========== End of top menu left items ========== -->
    </ul>

</div>
<link rel="stylesheet" href="/css/styles.css">


<div class="navbar-custom-menu">

    <ul class="nav navbar-nav">
        <!-- ========================================================= -->
        <!-- ========== Top menu right items (ordered left) ========== -->
        <!-- ========================================================= -->
        <li style="font-size: 26px; position: relative; right: 30px;margin: 8px; color: white " id="time"></li>

        <li class="dropdown notifications-menu">

            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-bell-o " style="font-size: 22px; position: absolute; right: 10px;"></i>
                <span class="label label-warning"
                      style="zoom:1.5;right: 2px;top: 5px;">{{ count($dashboardReminders) }}</span>
            </a>
            <ul class="dropdown-menu">
                <li class="header">You have {{ count($dashboardReminders) }} notifications</li>
                <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                    @foreach($dashboardReminders as $reminder)


                        <!-- See dist/js/pages/dashboard.js to activate the todoList plugin -->
                            <ul class="todo-list" style="padding: 2px; font-size:18px">
                                <li>
                                    <div class="row" style="margin: 0;">
                                        <div class="col-md-2" style="margin-left: -10px">
                                            <i class="fa fa-bell-o {{ $reminder->getPriorityColor() }}"
                                               style="border-radius: 60%;font-size: 15px;padding: 5px;"></i>
                                        </div>
                                        <div class="col-md-10">
                                            <a style="color: #595959;font-size: 15px; margin-left: -10px"
                                               onmouseout="this.style.color='#595959';"
                                               @if($reminder->operation_link)
                                               onmouseover="this.style.color='blue';"
                                               href={{url(config('backpack.base.route_prefix', 'admin')).$reminder->operation_link }} @endif
                                                       class="text">{{ $reminder->title}}
                                            </a>
                                        </div>
                                        <div class="col-md-12">
                                            <a style="color: #595959; font-size: 15px"
                                               onmouseout="this.style.color='#595959';"
                                               onmouseover="this.style.color='blue';" href="{{ route('crud.activityReminder.edit',
                                                                ['company'=>$reminder->activity->company_id, 'reminder'=>$reminder]) }}"
                                               style="font-size:15px">
                                                Assigned to: {{ $reminder->user->name}} </a>
                                        </div>
                                </li>
                            </ul>
                        @endforeach
                    </ul>
                </li>
                <li class="footer"><a href="/admin/dashboard">View all</a></li>
            </ul>
        </li>

    <!-- <li><a href="{{ url('/') }}"><i class="fa fa-home"></i> <span>Home</span></a></li> -->

        @if (Auth::guest())
            <li>
                <a href="{{ url(config('backpack.base.route_prefix', 'admin').'/login') }}">{{ trans('backpack::base.login') }}</a>
            </li>
            @if (config('backpack.base.registration_open'))
                <li>
                    <a href="{{ url(config('backpack.base.route_prefix', 'admin').'/register') }}">{{ trans('backpack::base.register') }}</a>
                </li>
            @endif
        @else
            <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/logout') }}"><i
                            class="fa fa-btn fa-sign-out" style="zoom: 1.5;"></i> {{ trans('backpack::base.logout') }}
                </a></li>
    @endif


    <!-- ========== End of top menu right items ========== -->

    </ul>

</div>
<script>
    function updateClock() {
        var now = new Date(), // current date
            months = ['January', 'February', '...']; // you get the idea
        time = now.getUTCHours() + ':' + (now.getMinutes() < 10 ? '0' : '') + now.getMinutes(), // again, you get the idea


            // set the content of the element with the ID time to the formatted string
            document.getElementById('time').innerHTML = [time + ' UTC'];

        // call this function again in 1000ms
        setTimeout(updateClock, 1000);

    }

    updateClock(); // initial call

</script>
