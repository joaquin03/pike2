@php
    if(!array_key_exists('value', $field)){
        $field['value'] = null;
    }
    if(isset($crud->clientId)){
        $company = $crud->clientId;
    } else{
        $company = $crud->companyId;
    }
@endphp
<div class="col-md-12" style="overflow: hidden">
    <hr>
    <div id="credit_notes_info"></div>
    <span style="margin-left: 20px"><i>{{$field['options']['text'] ?? ''}}</i></span>
    <h4>Bills</h4>
    <div class="box-body table-responsive">

        <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr>
                <th class="sortable bill_number" data-order="bill_number" data-type="ASC">
                    Bill Number <i class="fa fa-sort-amount-asc pull-right" aria-hidden="true"></i>
                </th>

                @if($field['options']['type'] === 'credit-note')

                    <th class="sortable" data-order="cfe_type" data-type="ASC">
                        CFE Type <i class="fa fa-sort-amount-asc pull-right" aria-hidden="true"></i>
                    </th>
                    <th class="sortable" data-order="reason" data-type="ASC">
                        Reason <i class="fa fa-sort-amount-asc pull-right" aria-hidden="true"></i>
                    </th>
                @endif

                <th class="sortable amount" data-order="amount" data-type="ASC">
                    Amount <i class="fa fa-sort-amount-asc pull-right" aria-hidden="true"></i>
                </th>
                <th class="sortable" data-order="delete" data-type="ASC">
                </th>
            </tr>
            </thead>
            <tbody id="bills-table">

            </tbody>

        </table>
        <div class="form-group col-md-3" style="margin-top: 20px">
            <button type="button" class="btn btn-info" onclick="addBillSelect(null)" id='add-credit-note-bill'>
                Add Bill
            </button>
        </div>
        <hr>

        <table class="table table-striped table-bordered table-hover">
            <tbody id="totals-table">
                <tr>
                    <td>Sub Total:</td><td id="sub-total"></td><td></td>
                </tr>
                <tr>
                    <td>Remaining:</td><td id="remaining-total"></td><td></td>
                </tr>
                <tr>
                    <td>Total:</td><td id="total"></td><td></td>
                </tr>
            </tbody>
        </table>

    </div>
</div>

{{-- FIELD CSS - will be loaded in the after_styles section --}}
@push('crud_fields_styles')
    {{-- @push('crud_fields_styles')
        {{-- YOUR CSS HERE --}}
@endpush
{{-- FIELD JS - will be loaded in the after_scripts section --}}
@push('crud_fields_scripts')
    <script src="{{ asset('vendor/adminlte/plugins/select2/select2.min.js') }}"></script>
    <script>

        var bills = {!! json_encode($field['value']) !!};
        var billsNotPaid = {!! json_encode($field['model']::isNotPaid()->where('billing_company_id', $company)->get()) !!};
        var index = 0;
        $( document ).ready(function() {
            renderBills();

            $('#amount').on('change', function(){
                setTotalValues();
            });
            $('.bill-amount').on('change', function(){
                setTotalValues();
            });

            setTotalValues();
        });

        renderBills = function () {
            for (var i = 0; i < bills.length; i++) {
                addBillSelect(bills[i]);
            }

        };

        addBillSelect = function (bill = null, edit = false) {
            var style = '';
            var selectedEdit ='selected';
            var defaultAmount;
            var billId;
            var html = '';

            if (bill != null) {
                style = 'background-color: #eee; opacity: 1;pointer-events:none;'
                selectedEdit = '';
                billId = bill.id;
                defaultAmount = bill.pivot.amount;
            }

            html += '<tr class="payment-item" id="item-' + index + '">';
            html += '<td><select class="bill-input form-control select2 col-md-12" name="{{$field['name']}}[' + index + '][bill_id]"'+
                'id="{{$field['name']}}[' + index + '][bill_id]" data-index="'+index+'" required style="'+style+'">';
            html += '<option value="" disabled '+selectedEdit+'> Select Bill</option>';

            for (var i=0; i < billsNotPaid.length; i++) {
                var selected = '';
                if (billsNotPaid[i].id == billId) { selected = 'selected';}

                html += '<option id="bill-select-'+billsNotPaid[i].id+'" value="'+billsNotPaid[i].id+'" ' +
                        'data-remaining_amount="'+billsNotPaid[i].remaining_amount+'" '+selected+' >'
                        + billsNotPaid[i].bill_number+'  (USD '+billsNotPaid[i].remaining_amount+') </option>';
            }
            html += '</select> </td>';

            html += '<td>  <input id="bill-amount-'+index+'" class="bill-input bill-amount" style="height: 32px; width: 100%" type="text" ' +
                    'name="{{$field['name']}}[' + index + '][amount]" required value="'+defaultAmount+'">  </td>';
            html += '<td> <button href="" type="button" class ="btn btn-sd btn-danger remove"> <i class="fa fa-trash-o"></i></button>  </td>';

            $("#bills-table").append(html);
            setAmountAmountAction();
            index ++;
        };

        $(document).on('click', 'button.remove', function () {
            $(this).closest('tr').remove();
            setTotalValues();
            return false;
        });


        // Calculations


        function setAmountAmountAction() {
            $('.bill-input').on('change', function(){

                var billId = $(this).val();
                var index = $(this).data('index');
                var BillAmount = parseFloat($('#bill-select-'+billId).data("remaining_amount"));
                $('#bill-amount-'+index).val(0);

                $('#bill-amount-'+index).val(Number(getNewBillAmount(BillAmount)).toFixed(3));

                setTotalValues();
            });
        };

        function calculateBillsAmount() {
            var total = 0;
            var items = $('.bill-amount');
            for (var i = 0; i<items.length; i++ ){
                var value = parseFloat($(items[i]).val());
                if (value > 0) {
                    total += value;
                }
            }
            return Number(parseFloat(total)).toFixed(3);
        }

        function calculateRemainingAmount() {
            var totalAmount = parseFloat($('#amount').val());
            var billsAmount = calculateBillsAmount();
            return totalAmount-billsAmount;
        }

        function getNewBillAmount(billAmount) {
            var remainingAmount = calculateRemainingAmount();

            if (billAmount > remainingAmount) {
                return remainingAmount;
            }
            return billAmount;
        }

        function setTotalValues() {
            var totalAmount = parseFloat($('#amount').val());
            var billsAmount = calculateBillsAmount();
            var remainingAmount = totalAmount-billsAmount;

            $('#sub-total').html(Number(billsAmount).toFixed(3));
            $('#remaining-total').html(Number(remainingAmount).toFixed(3));
            $('#total').html(Number(totalAmount).toFixed(3));
        }








    </script>


@endpush