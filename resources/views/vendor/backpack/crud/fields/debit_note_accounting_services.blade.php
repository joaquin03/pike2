@php
    if(!array_key_exists('value', $field)){
        $field['value'] = null;
    }
    if(isset($crud->clientId)){
        $company = $crud->clientId;
    } else{
        $company = $crud->companyId;
    }
@endphp
<div class="col-md-12" style="overflow: hidden">
    <hr>
    <div id="credit_notes_info"></div>
        <h4>Items</h4>
    <div class="box-body table-responsive">

        <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr>
                <th class=" bill_number">
                    Service
                </th>

                <th class="amount"data-type="ASC">
                    Amount
                </th>
                <th class="amount" data-type="ASC">
                    Quantity
                </th>
                <th class="amount" data-type="ASC">
                    Sub Total
                </th>
                <th class="sortable" data-order="delete" data-type="ASC">
                </th>
            </tr>
            </thead>
            <tbody id="accounting-items-table">


            </tbody>
            <tfoot>
                <td><span class="col-md-6"> Total:</span></td>
                <td></td>
                <td></td>
                <td><span class="col-md-6" id="total" style="font-weight: 600;"></span></td>
                <td></td>
            </tfoot>

        </table>
        <table class="table table-striped table-bordered table-hover">
            <tbody id="totals-table">
            <tr>

            </tr>

            </tbody>

        </table>
        <div class="form-group col-md-3" style="margin-top: 20px">
            <button type="button" class="btn btn-info" onclick="addBillSelect(null)" id='add-credit-note-bill'>
                Add Line
            </button>
        </div>
        <hr>



    </div>
</div>

{{-- FIELD CSS - will be loaded in the after_styles section --}}
@push('crud_fields_styles')
    {{-- @push('crud_fields_styles')
        {{-- YOUR CSS HERE --}}
@endpush
{{-- FIELD JS - will be loaded in the after_scripts section --}}
@push('crud_fields_scripts')
    <script src="{{ asset('vendor/adminlte/plugins/select2/select2.min.js') }}"></script>


    <script>
        addBillSelect = function (bill = null, edit = false) {
            var style = '';
            var selectedEdit ='selected';
            var defaultAmount;
            var billId;
            var html = '';
            var index = $('.accounting-item').length;

            if (bill != null) {
                style = 'background-color: #eee; opacity: 1;pointer-events:none;';
                selectedEdit = '';
                billId = bill.id;
                defaultAmount = bill.pivot.amount;
            }

            html += '<tr class="accounting-item" id="item-' + index + '">';
                html += '<td><input class="form-control bill-input bill-amount" type="text" name="services['+index+'][name]"/></td>';
            html += '<td>  <input id="amount-'+index+'" type="number" class="bill-input service-amount" ' +
                'style="height: 32px; width: 100%" type="text" ' +
                'name="services[' + index + '][amount]" required value="" step="any"></td>';
            html += '<td>  <input id="quantity-'+index+'" type="number" class="bill-input service-quantity" ' +
                'style="height: 32px; width: 100%" type="text" ' +
                'name="services[' + index + '][quantity]" required value="1" step="any"></td>';
            html += '<td>  <input id="sub-total-'+index+'" type="number"  class="sub-total form-control" disabled="disabled" ' +
                'style="height: 32px; width: 100%" type="text"></td>';

            html += '<td> <button href="" type="button" data-item="'+index+'" class="btn btn-sd btn-danger remove"> <i class="fa fa-trash-o"></i></button>  </td>';

            $("#accounting-items-table").append(html);
            setSubTotalEvent(index);
        };

        $(document).on('click', 'button.remove', function () {
            $(this).closest('tr').remove();
            calculateTotal();
            return false;
        });

        function setSubTotalEvent(index)
        {
            var index2 = index;
            $('#amount-'+index).on('change', function(){
                calculateSubTotal(index2);
            });
            $('#quantity-'+index).on('change', function(){
                calculateSubTotal(index2);
            });
        }

        function calculateSubTotal(index)
        {
            var total = parseFloat($('#amount-'+index).val()) * parseFloat($('#quantity-'+index).val());
            $('#sub-total-'+index).val(Number(parseFloat(total)).toFixed(2));

            $('#total').html(calculateTotal());
        }


        function calculateTotal() {
            var total = 0;
            var items = $('.sub-total');
            for (var i = 0; i<items.length; i++ ){
                var value = parseFloat($(items[i]).val());
                if (value > 0) {
                    total += value;
                }
            }
            return Number(parseFloat(total)).toFixed(2);
        }

    </script>


@endpush