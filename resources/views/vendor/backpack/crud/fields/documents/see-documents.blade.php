@if (isset($documents) && count($documents))
    <div class="well well-sm file-preview-container">

        @foreach($documents as $key => $file_path)

            @if(explode('/',$file_path))
                <div class="file-preview">
                    <a target="_blank"
                       href="{{ url('/').'/storage/'.$file_path}}">

                        @if(array_key_exists(2,explode('/',$file_path)))
                            {{ explode('/',$file_path)[2] }}
                        @else
                            {{ $file_path }}
                        @endif
                    </a>
                    <a id="documents_{{ $key }}_clear_button" href="#"
                       class="btn btn-default btn-xs pull-right file-clear-button" title="Clear file"
                       data-filename="{{ $file_path }}"><i class="fa fa-remove"></i></a>
                    <div class="clearfix"></div>
                </div>
            @endif
        @endforeach
    </div>

@endif