<div class="form-group col-md-12">
    <label class="control-label col-md-3">Sales Manager</label>
    <div class="col-md-6">

        @if(isset($entry)){{--editing--}}
        <select class="form-control select2" name="sales_manager_id" id="sales_manager_id">
            <option value="" disabled selected>---Client's country sales managers---</option>


                @foreach(App\Models\SalesManager::all() as $salesManager)
                    @if(in_array($entry->country, $salesManager->getCountriesKeys()))
                        <option value="{{$salesManager->id}}"
                                {{$entry->sales_manager_id == $salesManager->id? 'selected="selected"' : ''}}
                        >
                            {{$salesManager->name}}
                        </option>
                    @endif
                @endforeach

                <option value="" disabled>---All managers---</option>

                @foreach(App\Models\SalesManager::all() as $salesManager)ger)
                <option value="{{$salesManager->id}}"
                        {{$entry->sales_manager_id == $salesManager->id? 'selected="selected"' : ''}}
                >
                    {{$salesManager->name}}
                </option>
                @endforeach
            @else
                <p> Please save the company before selecting sales manager. </p>

            @endif

        </select>
    </div>

</div>
