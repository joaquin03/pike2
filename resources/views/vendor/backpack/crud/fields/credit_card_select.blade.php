<!-- select2 from array -->
<div @include('crud::inc.field_wrapper_attributes') >
    <label>{!! $field['label'] !!}</label>
    <select
            name="{{ $field['name'] }}@if (isset($field['allows_multiple']) && $field['allows_multiple']==true)[]@endif"
            style="width: 100%"
            @include('crud::inc.field_attributes', ['default_class' =>  'form-control select2_from_array'])
            @if (isset($field['allows_multiple']) && $field['allows_multiple']==true)multiple @endif
    >

        @if (isset($field['allows_null']) && $field['allows_null']==true)
            <option value="">-</option>
        @endif

        @if (count($field['options']))
            @foreach ($field['options'] as $title => $array)
                <option value="{{ $title }}" disabled
                        style="font-weight: bold; color: black">{{ strtoupper($title) }}</option>

                @foreach($array as $key => $value)
                    @if((old($field['name']) && ($key == old($field['name']) || is_array(old($field['name'])) &&
                            in_array($key, old($field['name'])))) || (is_null(old($field['name'])) && isset($field['value']) &&
                                ($key == $field['value'] || (is_array($field['value']) && in_array($key, $field['value'])))))
                        <option value="{{ $key }}" selected>{{ $value }}</option>
                    @else
                        <option value="{{ $key }}">{{ $value }}</option>
                    @endif
                @endforeach


            @endforeach

            <option value="CREDIT CARD" disabled style="font-weight: bold; color: black"> CREDIT CARD</option>
            @foreach(\App\Models\CreditCard::all() as $key => $value)

                @if((old($field['name']) && ($key == old($field['name']) || is_array(old($field['name'])) &&
                            in_array($key, old($field['name'])))) || (is_null(old($field['name'])) && isset($field['value']) &&
                                ($key == $field['value'] || (is_array($field['value']) && in_array($key, $field['value'])))))
                    <option value="{{ $value['number'] }}" selected>{{ $value['number'] }}</option>
                @else
                    <option value="{{ $value['number'] }}">{{ $value['number'] }}</option>
                @endif



            @endforeach
        @endif

    </select>

    {{-- HINT --}}
    @if (isset($field['hint']))
        <p class="help-block">{!! $field['hint'] !!}</p>
    @endif
</div>
