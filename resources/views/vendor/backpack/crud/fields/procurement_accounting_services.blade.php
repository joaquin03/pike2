<div class="col-md-12" style="overflow: hidden">
    <hr>
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Description</th>
            <th>Quantity</th>
            <th>Unit Price</th>
            <th>Final Price</th>
        </tr>
        </thead>
        <tbody>
        @foreach($accountingServices['data'] as $item)

            <tr id="{{$item['id']}}" style=" @if($loop->last) font-weight:bolder @endif">
                <td>{{$item['description']}}</td>
                <td>{{$item['procurement_quantity']}}</td>
                <td>{{$item['procurement_unit_final_cost']}}</td>
                <td>{{$item['procurement_final_cost']}}</td>
            </tr>

        @endforeach

        </tbody>
        <tr></tr>


    </table>

</div>




<div class="col-md-12" style="overflow: hidden">
    <hr>
    <div id="accounting-services"></div>
</div>

@push('crud_fields_styles')
    <link rel="stylesheet" href="{{ asset('/js/handsontable.full.min.css') }}"/>
@endpush

@push('crud_fields_scripts')

@endpush