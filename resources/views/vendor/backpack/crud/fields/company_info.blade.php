<div>
    <!-- select2 -->
    <div @include('crud::inc.field_wrapper_attributes') >
        <label>{!! $field['label'] !!}</label>
        @include('crud::inc.field_translatable_icon')
        <?php $entity_model = $crud->model; ?>


        <select
                id="company"
                name="{{ $field['name'] }}"
                style="width: 100%"
                @include('crud::inc.field_attributes', ['default_class' =>  'form-control select2_field'])
        >

            @if ($entity_model::isColumnNullable($field['name']))
                <option value="">-</option>
            @endif

            <option class="col-md-6" value="" disabled="disabled" selected="selected">Select the Company</option>

            @foreach(App\Models\Company::all() as $company)
                <option value="{{$company->id}}"
                        @if($crud->entry && $company->id == $crud->entry->company_id) selected @endif
                        @if(isset($field['default']) && $company->id == $field['default']) selected @endif
                    >{{$company->name}}
                </option>
            @endforeach
        </select>


    </div>
    <div class="form-group col-md-4">
        <label>Company Emails</label>
        <textarea class="form-control" id="info-emails" rows="2" style="resize: none;" readonly>
        </textarea>
    </div>
    <div class="form-group col-md-4">
        <label>Company Address</label>
        <textarea class="form-control" id="info-address" rows="2" style="resize: none;" readonly>
        </textarea>
    </div>
    <div class="form-group col-md-4">
        <label>Company Phones</label>
        <textarea class="form-control" id="info-phones" rows="2" style="resize: none;" readonly>
        </textarea>
    </div>


</div>


{{-- ########################################## --}}
{{-- Extra CSS and JS for this particular field --}}
{{-- If a field type is shown multiple times on a form, the CSS and JS will only be loaded once --}}
@if ($crud->checkIfFieldIsFirstOfItsType($field, $fields))

    {{-- FIELD CSS - will be loaded in the after_styles section --}}
    @push('crud_fields_styles')
        <!-- include select2 css-->
        <link href="{{ asset('vendor/adminlte/plugins/select2/select2.min.css') }}" rel="stylesheet"
              type="text/css"/>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css"
              rel="stylesheet" type="text/css"/>
    @endpush

    {{-- FIELD JS - will be loaded in the after_scripts section --}}
    @push('crud_fields_scripts')
        <!-- include select2 js-->
        <script src="{{ asset('vendor/adminlte/plugins/select2/select2.min.js') }}"></script>
        <script>
            jQuery(document).ready(function ($) {
                    // trigger select2 for each untriggered select2 box
                    $('.select2_field').each(function (i, obj) {
                        if (!$(obj).hasClass("select2-hidden-accessible")) {
                            $(obj).select2({
                                theme: "bootstrap"
                            });
                        }
                    });
                    formatCompany();
                    loadCompanyInfo();

                    function loadCompanyInfo() {
                        $('#company').on('change', function () {
                            formatCompany();
                        });
                    }

                    function formatCompany() {
                        var companyId = $("#company option:selected").val();
                        $.get('{{url('admin/company')}}' + '/' + companyId + '/info', function (data) {
                            $("#info-emails").html(formatEmails(data));
                            $("#info-address").html(formatAddress(data));
                            $("#info-phones").html(formatPhones(data));
                        });

                    }

                    function formatEmails(company) {
                        var html = '';
                        var emails = company['emails'];
                        if (emails) {
                            for (var i = 0; emails.length > i; i++) {
                                if (emails[i]['email']) {
                                    html += emails[i]['email'] += '\n';
                                }
                            }
                        }
                        $("#info-emails").html(html);
                    }

                    function formatAddress(company) {
                        var html = '';
                        var address = company['address'];
                        if (address) {
                            for (var i = 0; address.length > i; i++) {
                                if (address[i]['address']) {
                                    html += address[i]['address'] += ', ';
                                }
                                if (address[i]['city']) {
                                    html += address[i]['city'] += ', ';
                                }
                                if (address[i]['country']) {
                                    html += address[i]['country'] += '\n';
                                }
                            }

                        }

                        $("#info-address").html(html);
                    }


                    function formatPhones(company) {
                        var html = '';
                        var phones = company['phones'];
                        if (phones) {
                            for (var i = 0; phones.length > i; i++) {
                                if (phones[i]['phone']) {
                                    html += phones[i]['phone'] += '\n';
                                }
                            }
                        }
                        $("#info-phones").html(html);
                    }


                }
            )
            ;
        </script>
    @endpush

@endif
{{-- End of Extra CSS and JS --}}
{{-- ########################################## --}}
