@php
    if(!array_key_exists('value', $field)){
        $field['value'] = null;
    }
    if(isset($crud->clientId)){
        $company = $crud->clientId;
    } else{
        $company = $crud->companyId;
    }
@endphp
<div class="col-md-12" style="overflow: hidden">
    <hr>
    <div id="credit_notes_info"></div>
    <span style="margin-left: 20px"><i>{{$field['options']['text']}}</i></span>
    <h4>Bills</h4>
    <div class="box-body table-responsive">

        <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr>
                <th class="sortable bill_number" data-order="bill_number" data-type="ASC">
                    Bill Number <i class="fa fa-sort-amount-asc pull-right" aria-hidden="true"></i>
                </th>

                @if($field['options']['type'] === 'credit-note')

                    <th class="sortable" data-order="cfe_type" data-type="ASC">
                        CFE Type <i class="fa fa-sort-amount-asc pull-right" aria-hidden="true"></i>
                    </th>
                    <th class="sortable" data-order="reason" data-type="ASC">
                        Reason <i class="fa fa-sort-amount-asc pull-right" aria-hidden="true"></i>
                    </th>
                @endif

                <th class="sortable amount" data-order="amount" data-type="ASC">
                    Amount <i class="fa fa-sort-amount-asc pull-right" aria-hidden="true"></i>
                </th>
                <th class="sortable" data-order="delete" data-type="ASC">
                </th>
            </tr>
            </thead>
            <tbody id="bills-table">

            </tbody>

        </table>


        <div class="form-group col-md-3" style="margin-top: 20px">
            <button type="button" class="btn btn-info" onclick="addBill(null, $('#bills-table tr').length )"
                    id='add-credit-note-bill'> Add Bill
            </button>
        </div>
    </div>
</div>

{{-- FIELD CSS - will be loaded in the after_styles section --}}
@push('crud_fields_styles')
    {{-- @push('crud_fields_styles')
        {{-- YOUR CSS HERE --}}
@endpush
{{-- FIELD JS - will be loaded in the after_scripts section --}}
@push('crud_fields_scripts')
    <script src="{{ asset('vendor/adminlte/plugins/select2/select2.min.js') }}"></script>

    <script>
        var bills = {!! json_encode($field['value']) !!};
        renderBills = function (bills) {
            var html = '';
            if (bills) {
                for (var i = 0; i < bills.length; i++) {
                    addBill(bills[i], i, true);
                }
            }
        };

        addBill = function (bill, index, edit = false) {
            var html = '';
            html += '<tr class="payment-item" id="item-' + index + '">';

            if (edit) {
                html += '<td> <select class="bill-input form-control select2 col-md-12" name="{{$field['name']}}[' + index + '][bill_id]"' +
                    'id="{{$field['name']}}[' + index + '][bill_id]" style="    background-color: #eee; opacity: 1;pointer-events:none;">' +
                    '@foreach ($field['model']::where('billing_company_id', $company)->withoutGlobalScope(new \App\Models\Scopes\NotDebitNoteScope())->get() as $connected_entity_entry)' +
                    '@if ($connected_entity_entry->status == 'paid')' +
                    '<option value="{{$connected_entity_entry->id}}" > {{ $connected_entity_entry->bill_number }}</option>' +
                    '@else <option value="{{$connected_entity_entry->id}}"> {{ $connected_entity_entry->bill_number }}</option>' +
                    '@endif @endforeach </select> </td>';
            }
            else {
                html += '<td> <select required class="bill-input form-control select2 col-md-12" name="{{$field['name']}}[' + index + '][bill_id]"' +
                    'id="{{$field['name']}}[' + index + '][bill_id]" onchange="loadBillTotalOnChangeFunction(this)" >' +
                    '<option value="" disabled selected >Select Bill</option>' +
                    '@foreach ($field['model']::where('billing_company_id', $company)->withoutGlobalScope(new \App\Models\Scopes\NotDebitNoteScope())->get() as $connected_entity_entry)' +
                    '@if ($connected_entity_entry->status != 'paid')' +
                    '<option value="{{$connected_entity_entry->id}}">{{ $connected_entity_entry->bill_number }}</option>' +
                    '@endif' +
                    '@endforeach' +
                    '</select> </td>';
            }
            @if($field['options']['type'] === 'credit-note')

                html += '<td> <select class="bill-input form-control select2" name="{{$field['name']}}[' + index + '][cfe_type]">\n' +
                '<option value="102">Credit Rebate</option>\n' +
                '<option value="103">Debit Rebate</option>\n' +
                '</select> </td>';

            html += '<td> <select class="bill-input form-control select2"  name="{{$field['name']}}[' + index + '][reason]">\n' +
                '@foreach($crud->model->reasons as $reason)\n' +
                '<option value="{{$reason}}">{{$reason}}</option>\n' +
                '@endforeach\n' +
                '</select> </td>';
            @endif

                html += '<td>  <input class="bill-input" style="height: 32px; width: 100%"\n' +
                'type="text" name="{{$field['name']}}[' + index + '][amount]" required value=" ">  </td>';

            html += '<td> <button href="" type="button" class ="btn btn-sd btn-danger remove"> <i class="fa fa-trash-o"></i></button>  </td>';

            $("#bills-table").append(html);


            setDefaultData(bill, index, edit);

        };

        setDefaultData = function (bill, index, edit) {


            $('[name="{{$field['name']}}[' + index + '][bill_id]"]').val('');
            $('[name="{{$field['name']}}[' + index + '][cfe_type]"]').val(bill.pivot.cfe_type);
            $('[name="{{$field['name']}}[' + index + '][reason]"]').val(bill.pivot.reason);
            $('[name="{{$field['name']}}[' + index + '][amount]"]').val('');

            if (edit) {
                $('[name="{{$field['name']}}[' + index + '][bill_id]"]').val(bill.id);
                $('[name="{{$field['name']}}[' + index + '][cfe_type]"]').val(bill.pivot.cfe_type);
                $('[name="{{$field['name']}}[' + index + '][reason]"]').val(bill.pivot.reason);
                $('[name="{{$field['name']}}[' + index + '][amount]"]').val(bill.pivot.amount);
            }


        };

        function loadBillTotalOnChangeFunction(x) {

            closest_bill = x.name.split('[')[1].split(']')[0];
            console.log(closest_bill);


                    @if($field['options']['type'] === 'credit-note')
            var t = document.getElementById("bills-table"),
                d = t.getElementsByTagName("tr")[closest_bill],
                r = d.getElementsByTagName("td")[3];

                    @else
            var t = document.getElementById("bills-table"),
                d = t.getElementsByTagName("tr")[closest_bill],
                r = d.getElementsByTagName("td")[1];
            @endif


                bill_id = x.value;

            $.get('{{url('admin/bills/amount')}}' + '/' + bill_id, function (data) {
                $(r).html(amountHTML(data, closest_bill));
            });
        }


        function amountHTML(data, closest_bill) {
            return '<input class="bill-input" style="height: 32px; width: 100%"\n' +
                'type="text" name="{{$field['name']}}[' + closest_bill + '][amount]" required value="' + data + '"> ';
        }

        $(document).ready(function () {
            renderBills(bills);
        });

        $(document).on('click', 'button.remove', function () {
            $(this).closest('tr').remove();
            return false;
        });

    </script>


@endpush