<td>
    <label class="col-md-6">Company</label>
    <select class="form-group select2 col-md-6" name="company" id="company">
        <option class="col-md-6" value="" disabled="disabled" selected="selected">Select the Company</option>
        @foreach(App\Models\Company::all() as $company)
            <option value="{{$company->id}}">{{$company->name}}
            </option>
        @endforeach
    </select>
</td>