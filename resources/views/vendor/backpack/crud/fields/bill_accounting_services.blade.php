<div class="col-md-12" style="overflow: hidden">
    <hr>
    <div id="accounting-services"></div>
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Description</th>
            <th>Quantity</th>
            <th>Unit Price</th>
            <th>Final Price</th>
        </tr>
        </thead>
        <tbody>
        @foreach($accountingServices['data'] as $item)

            <tr id="{{$item['id']}}" style=" @if($loop->last) font-weight:bolder @endif">
                <td>{{$item['description']}}</td>
                <td>{{$item['billing_quantity'] ?? $item['procurement_quantity']}}</td>
                <td>@if($loop->last)  @else {{ round($item['billing_unit_price'],2) ?? round($item['procurement_unit_final_cost'],2) }} @endif</td>
                <td>{{round($item['billing_final_price'],2)}}</td>
            </tr>

        @endforeach

        </tbody>
        <tr></tr>


    </table>
</div>

<div class="col-md-12">
    <a class="btn btn-warning pull-right" target="_blank" id="debit-note-create"
       onclick="return confirm('You are going to create a Debit Note. Are you sure?');"
       data-toggle="tooltip"
       title="Create Debit Note">Create Debit Note</a>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script>
    $(document).ready(function () {

        document.getElementById('debit-note-create').onclick = function () {
            saveChanges();
        };



    });

    function saveChanges() {
        confirm('You are going to create a Debit Note. Are you sure?');
        $.ajax({
            type: "POST",
            url: '/admin/bills/{{$entry->id}}/update-debit-note-data',
            data: {
                'dollar_change': document.getElementById('dollar_change').value,
                'date': document.getElementById('datepicker-date').value,
                'due_date': document.getElementById('date_picker_custom').value,
            },
        }).success(
            location.href ="{{route('crud.clients.bills.debit-note.create', ['bill_id'=> $entry->id])}}",
        );
    }

</script>