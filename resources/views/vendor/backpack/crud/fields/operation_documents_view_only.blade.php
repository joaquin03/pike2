<!-- upload multiple input -->


    {{-- Show the file name and a "Clear" button on EDIT form. --}}

    @if (isset($operation->documents) && count($operation->documents))

        <div class="well well-sm file-preview-container">
            @foreach($operation->documents as $key => $file_path)

                @if(explode('/',$file_path))
                    <div class="file-preview">
                        <a target="_blank"
                           href="{{ url('/').'/storage/'.$file_path}}">

                        @if(array_key_exists(2,explode('/',$file_path)))
                                {{ explode('/',$file_path)[2] }}
                            @else
                                {{ $file_path  }}
                            @endif
                        </a>
                       
                    </div>
                @endif
            @endforeach
        </div>
        @else
        No operation documents
    @endif



{{-- FIELD EXTRA JS --}}
{{-- push things in the after_scripts section --}}

