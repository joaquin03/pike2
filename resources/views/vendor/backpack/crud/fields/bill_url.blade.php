<div class="box-body">
    <div class="form-group col-md-6">
        @if(array_key_exists('value', $field))
            @if($field['value'])
                <a target="_blank"  href="{{ $field['value'] }}">Show generated bill
                </a>
            @endif
        @endif
    </div>
</div>
