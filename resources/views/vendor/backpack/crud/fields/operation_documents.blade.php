<!-- upload multiple input -->
@php
    $currentUser = \Illuminate\Support\Facades\Auth::user();
@endphp

{{-- Show the file name and a "Clear" button on EDIT form. --}}

@if (isset($operation->documents) && count($operation->documents))

    <div class="well well-sm file-preview-container">
        @foreach($operation->documents as $key => $file_path)

            @if(explode('/',$file_path))
                <div class="file-preview">
                    <a target="_blank"
                       href="{{ url('/').'/storage/'.$file_path}}">

                        @if(array_key_exists(2,explode('/',$file_path)))
                            {{ explode('/',$file_path)[2] }}
                        @else
                            {{ $file_path }}
                        @endif
                    </a>
                    @if($currentUser->can('Operation'))

                        <a id="documents_{{ $key }}_clear_button" href="#"
                           class="btn btn-default btn-xs pull-right file-clear-button" title="Clear file"
                           data-filename="{{ $file_path }}"><i class="fa fa-remove"></i></a>
                    @endif
                    <div class="clearfix"></div>
                </div>
            @endif
        @endforeach
    </div>
@endif
{{-- Show the file picker on CREATE form. --}}
@if($currentUser->can('Operation'))

    <input name="documents[]" type="hidden" value="" class="col-md-9">
    <input
            type="file"
            id="documents_file_input"
            name="documents[]"
            value="documents"
            multiple
    >
@endif


{{-- FIELD EXTRA JS --}}
{{-- push things in the after_scripts section --}}

