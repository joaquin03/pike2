<!-- used for heading, separators, etc -->
@if($entry->is_active)
    <div @include('crud::inc.field_wrapper_attributes') >
        <a class="btn btn-danger pull-right" target="_blank"  onclick="return confirm('The bill will be canceled. Are you sure?');"
           href="{{route('crud.clients.bills.cancel', ['bill_id'=> $entry->id])}}" data-toggle="tooltip"
           title="Cancel bill"><i class="fa fa-times"></i> Cancel</a>
    </div>
@endif