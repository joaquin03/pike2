<td>
    @if ($entry->type == "overflight")
        OVF
    @elseif ($entry->type == "landing")
        LND
    @elseif ($entry->type == "departure")
        DEP
    @elseif ($entry->type == "landing diplomatic")
        LND DP
    @elseif ($entry->type == "overflight diplomatic")
        OVF DP
    @endif

</td>
