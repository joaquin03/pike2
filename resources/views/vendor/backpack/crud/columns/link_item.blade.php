<td>
    <a href="{{$column['link'].'/'.$entry->id}}" >
        @if(!array_key_exists('html', $column))
            {{ str_limit(strip_tags($entry->{$column['name']}), 80) }}
        @else
            {{ $column['html'] }}
        @endif
    </a>
</td>