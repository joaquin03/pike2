<td>
    @if ($entry->status == "Pending Data Sheet")
        <small class="label p1 white bg-blue" style='font-size:13px; color:white'>PENDING DATA SHEET</small>

    @elseif ($entry->status == "In Process")
        <small class="label  bg-orange" style='font-size:13px'>IN PROCESS</small>

    @elseif ($entry->status == "Pending Answer")
        <small class="label  bg-black" style='font-size:13px'>PENDING ANSWER</small>

    @elseif ($entry->status == "Canceled")
        <small class="label  bg-red" style='font-size:13px'>CANCELED</small>

    @elseif ($entry->status == "Lost")
        <small class="label  bg-red" style='font-size:13px'>LOST</small>

    @elseif ($entry->status == "Budget")
        <small class="label  bg-fuchsia" style='font-size:13px'>BUDGET</small>

    @elseif ($entry->status == "Confirmed")
        <small class="label  bg-green" style='font-size:13px'>CONFIRMED</small>
    @endif

</td>
