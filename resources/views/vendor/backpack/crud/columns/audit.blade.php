{{-- enumerate the values in an array  --}}
<td>
    @php

        $text = $entry->transformNewValues();
        $max_length = 200;

        if (strlen($text) > $max_length) {
            $offset = ($max_length - 3) - strlen($text);
            $text = substr($text, 0, strrpos($text, ' ', $offset)) . '...';
        }
        echo $text;
    @endphp
</td>

