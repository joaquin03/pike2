<td >
    @if ($entry->status == "label-success")
        <small class="label  bg-green" style='font-size:13px' >SUCCESS</small>
    @elseif ($entry->status == "label-warning")
        <small class="label  bg-yellow" style='font-size:13px' >WARNING</small>
    @elseif ($entry->status == "label-danger")
        <small class="label  bg-red" style='font-size:13px' >DANGER</small>

    @endif

</td>
