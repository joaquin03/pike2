<style>
    #operator_name {
        width: 100px;
        display: inline-block;
        text-overflow: ellipsis;
        white-space: nowrap;
        overflow: hidden;
    }

</style>
<td>
    <span id="operator_name" title="{{isset($entry->airCraftOperator) ? $entry->airCraftOperator->name : ''}}">
        {{isset($entry->airCraftOperator) ? $entry->airCraftOperator->name : ''}}
    </span>
</td>