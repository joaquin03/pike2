<td >
        @if ($entry->status == "Completed")
                <small class="label p1 white bg-gray" style='font-size:13px; color:white' >COMPLETED</small>
        @elseif ($entry->status == "In Process")
                <small class="label  bg-green" style='font-size:13px' >IN PROCESS</small>
        @elseif ($entry->status == "Canceled")
                <small class="label  bg-red" style='font-size:13px' >CANCELED</small>
        @elseif ($entry->status == "Deleted")
                <small class="label  bg-black" style='font-size:13px' >DELETED</small>
        @endif

</td>
