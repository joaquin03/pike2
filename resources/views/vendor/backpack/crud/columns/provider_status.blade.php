    <td >
        @if ($entry->balance == 0)
                <small class="label p1 white bg-gray" style='font-size:13px; color:white' >{{$entry->balance}}</small>
        @elseif ($entry->balance < 0)
            <small class="label p1 white bg-red" style='font-size:13px; color:white' >{{$entry->balance}}</small>
        @elseif ($entry->balance > 0)
            <small class="label p1 white bg-green" style='font-size:13px; color:white' >{{$entry->balance}}</small>
        @endif

</td>
