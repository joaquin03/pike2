<td >
    @if ($entry->status == "active")
        <small class="label  bg-green" style='font-size:13px' >ACTIVE</small>
    @elseif ($entry->status == "pending")
        <small class="label  bg-yellow" style='font-size:13px' >PENDING</small>
    @elseif ($entry->status == "expired")
        <small class="label  bg-red" style='font-size:13px' >EXPIRED</small>

    @endif

</td>
