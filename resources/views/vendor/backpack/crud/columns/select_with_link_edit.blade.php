
<td>
    @if ($entry->{$column['entity']})
    <a href="{{route($column['route'], ['id'=>$entry->{$column['entity']}->id])}}">
            {{ $entry->{$column['entity']}->{$column['attribute']} }}
    </a>
    @endif
</td>