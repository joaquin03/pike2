@if ($items['meta'] ?? false && $items['meta']['pagination'] ?? false)
    @php $pagination = $items['meta']['pagination'] @endphp
    @if ($pagination['total'])
        <ul class="pagination">
            {{-- Previous Page Link --}}
            <li class="page-item" id="firstPage">
                <a href="#"  class="page-link" onClick="changePage(-1)" rel="prev">
                    @lang('pagination.previous')
                </a>
            </li>
            {{-- Next Page Link --}}
            <li class="page-item" id="lastPage">
                <a href="#" class="page-link" onClick="changePage(1)" rel="next">
                    @lang('pagination.next')
                </a>
            </li>
        </ul>
    @endif
@endif

@push('script_stack')
<script src="jquery-3.2.1.min.js"></script>
<script>
    var pageTo = {!! $pagination['current_page']!!};
    var totalPages = {!! $pagination['total_pages'] !!};
    var type = {!! json_encode($type) !!};

    function renderButtons(totalPages) {
        if(pageTo == 1){
            $("#firstPage").attr("disabled", "disabled");
            $("#firstPage" ).addClass( "disabled" );
//        $("#firstPageDisabled").prop('disabled', false);
        } else {
            $("#firstPage").removeAttr("disabled");
            $("#firstPage" ).removeClass( "disabled" );
        }
        if(pageTo >= totalPages) {
            $("#lastPage").attr("disabled", "disabled");
            $("#lastPage").addClass( "disabled" );
        } else {
            $("#lastPage").removeAttr("disabled");
            $("#lastPage").removeClass( "disabled" );

        }
    }
    renderButtons(totalPages);

    function changePage(change){

        pageTo = pageTo + change;
        if (pageTo > totalPages)  {
            pageTo = totalPages;
        }
        if (pageTo < 1) {
            pageTo = 1;
        }
        console.log(type);
        if( type == 'procurement') {
            filterProcurements();
        } else {
            filterBillings();
        }
    }

</script>
@endpush
