
@if ($items['meta'] ?? false && $items['meta']['pagination'] ?? false)
    @php $pagination = $items['meta']['pagination'] @endphp
    @if ($pagination['total'])
        <ul class="pagination">
            {{-- Previous Page Link --}}
            @if ($pagination['current_page'] == 1)
                <li class="page-item disabled"><span class="page-link">@lang('pagination.previous')</span></li>
            @else
                <li class="page-item"><a class="page-link" href="{{ $pagination['links']['previous'] }}" rel="prev">@lang('pagination.previous')</a></li>
            @endif

            {{-- Next Page Link --}}
            @if ($pagination['current_page'] < $pagination['total_pages'])
                <li class="page-item"><a class="page-link" href="{{ $pagination['links']['next'] }}" rel="next">@lang('pagination.next')</a></li>
            @else
                <li class="page-item disabled"><span class="page-link">@lang('pagination.next')</span></li>
            @endif
        </ul>
    @endif
@endif