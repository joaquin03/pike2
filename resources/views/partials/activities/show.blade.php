<div class="modal-header">
    <button class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">
        <i class="{{ $activity->event->getIcon() }}"></i> {{ ucfirst($eventName) }}
    </h4>
</div>

<div class="modal-body">
    <div class="row">
        @include('partials.activities.show.' . $eventName, ['event' => $activity->event])
    </div>

</div>

<div class="modal-footer">
    <button class="btn dark btn-outline" data-dismiss="modal">Close</button>
    <a href="{{ route($company->getType() . '.activity.edit', [$company->getType() => $company, 'activity' => $activity]) }}"
       class="btn green"
    >Edit</a>
</div>