@extends('layouts.app')

@section('title')
    Editing {{ucfirst($eventName)}} for {{ ucfirst($company->getType()) }} {{ $company->name }}
@endsection

@section('content')
    <div id="app" class="portlet light">
        @include('partials.vue.ajax_errors')

        <div class="portlet light">
            <form @submit.prevent="update" class="form form-horizontal form-row-seperated">
                <h3 class="form-section">{{ucfirst($eventName)}} Information</h3>
                @include('partials.activities.fields.'.$eventName)

                <h3 class="form-section">Activity information</h3>
                @include('partials.activities._fields')

                <div style="text-align: right;">
                    <a href="{{ route($company->getType() . '.show', [$company->getType() => $company]) }}" class="btn">Cancel</a>
                    <button type="submit" class="btn green"><i class="fa fa-check"></i> Update</button>

                    <a href="{{ route($company->getType() . '.activity.delete', [$company->getType() => $company, 'activity' => $activity]) }}"
                       class="btn btn-danger pull-left" data-toggle="confirmation"
                    >Delete</a>
                </div>
            </form>
        </div>
    </div>
@endsection


@section('scripts')
    <script>

        $('.timepicker2').timepicker({'showMeridian': false});
        $('.select2').select2({
            theme: "bootstrap"
        });

        Vue.directive('select', {
            twoWay: true,
            priority: 1000,

            params: ['options'],

            bind: function () {
                var self = this;
                $(this.el)
                        .select2({
                            data: this.params.options
                        })
                        .on('change', function () {
                            self.set($(self.el).val())
                        })
            },
            update: function (value) {
                $(this.el).val(value).trigger('change')
            },
            unbind: function () {
                $(this.el).off().select2('destroy')
            }
        });

        new Vue({
            el: "#app",

            data: {
                activity: {{$activity->event}},
                contacts: <?=json_encode($activity->getContactsIds())?>,

                @include('partials.vue.ajax_data')
            },

            ready: function() {
                this.activity.contacts = this.contacts;
            },

            methods: {
                update: function (event) {
                    var button = this.ajaxStart(event);
                    this.$http.put(
                            '{{route($company->getType() . '.activity.update', ['activity' => $activity, $company->getType() => $company])}}',
                            this.activity
                    ).then(function () {
                        this.ajaxBack(button, 'Activity updated succesfully');
                    }.bind(this));
                },

                @include('partials.vue.ajax_methods')

            }
        });
    </script>
@endsection
