<div class="col-md-12 form-group">
    <label for="contacts" class="col-md-1 control-label">Contacts</label>
    @if(false)
    <div class="col-md-11">
        <select class="form-control"
                v-select="activity.contacts"
                v-model="activity.contacts"
                multiple
        >
            @foreach($contacts as $contact)
                <option value="{{$contact->id}}">{{$contact->name}}</option>
            @endforeach
        </select>
    </div>
    @endif
</div>

<div class="col-md-12 form-group">
    <label class="control-label col-md-1">Visibility *</label>
    <div class="col-md-11">
        <div class="mt-radio-inline">
            <label class="mt-radio">
                <input type="radio" name="visibility" id="optionsRadios4" value="public" v-model="activity.visibility" checked> Public
            </label>
            <label class="mt-radio">
                <input type="radio" name="visibility" id="optionsRadios5" value="private" v-model="activity.visibility"> Private
            </label>
        </div>
    </div>
</div>