@section('scripts')

    <script>
        new Vue({
            el: "#app",

            data: {
                contacts: <?=json_encode($activity->contacts)?>,

                @include('partials.vue.ajax_data')
            },

            methods: {
                addEmail: function () {
                    this.client.emails.push({
                        "subtype": "laboral",
                        "data": ""
                    });
                },

                removeEmail: function (email) {
                    var index = this.client.emails.indexOf(email);
                    if (index > -1) {
                        this.client.emails.splice(index, 1);
                    }
                },

                @include('partials.vue.ajax_methods')
            }
        });
    </script>
@endsection()