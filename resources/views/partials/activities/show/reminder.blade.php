<div class="col-md-12">
    <div class="row">
        <div class="col-md-8">
            <div class="form-group">
                <label class="control-label">Title</label>
                <span class="form-control">{{$event->title}}</span>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group">
                <label class="control-label">Due Date</label>
                <span class="form-control">{{$event->due_date ? $event->due_date->toDateString() : ''}}</span>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label">Description</label>
                <span class="form-control">{{$event->description}}</span>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
                <label class="control-label">Assign To</label>
                <span class="form-control">{{$event->assignedTo->name}}</span>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label">Created at</label>
                <span class="form-control">{{$event->getDate()}}</span>
            </div>
        </div>


        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label">Completed At</label>
                <span class="form-control">{{$event->completed_at ? $event->completed_at->toDateTimeString() : 'not yet'}}</span>
            </div>
        </div>
    </div>
</div>
