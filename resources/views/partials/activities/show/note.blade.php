<div class="col-md-12">
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
                <label class="control-label">Title</label>
                <span class="form-control">{{$event->title}}</span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
                <label class="control-label">Date</label>
                <span class="form-control">{{$event->getDate()}}</span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label">Body</label>
                <span class="form-control">{{$event->body}}</span>
            </div>
        </div>
    </div>
</div>
