<div class="col-md-12">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label">Name</label>
                <span class="form-control">{{$event->name}}</span>
            </div>
        </div>
    </div>
    <div class="row">
        <!-- Start -->
        <div class="col-md-12">
            <div class="form-group">
                <label for="start" class="control-label">Start*</label>
                <div class="row">
                    <div class="col-md-6">
                        <span id="start" type="text" name="from" class="form-control datetimepicker3">
                            {{$event->start_date}}
                        </span>
                    </div>
                    <div class="col-md-5 input-group">
                        <div class="input-group bootstrap-timepicker timepicker">
                            <span class="timepicker2 form-control" type="text" v-model="activity.start_time">
                                {{$event->start_time}}
                            </span>
                            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End -->
        <div class="col-md-12">
            <div class="form-group">
                <label for="end" class="">End*</label>
                <div class="row">
                    <div class="col-md-6">
                        <span id="start" type="text" name="from" class="form-control datetimepicker3">
                            {{$event->end_date}}
                        </span>
                    </div>
                    <div class="col-md-5 input-group">
                        <div class="input-group bootstrap-timepicker timepicker">
                            <span class="timepicker2 form-control">{{$event->end_time}}</span>
                            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label">Summary</label>
                <span class="form-control" style="word-wrap: break-word;">{{$event->summary}}</span>
            </div>
        </div>
    </div>
</div>
