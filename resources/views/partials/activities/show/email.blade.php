<div class="col-md-12">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">From</label>
                <span class="form-control">{{$event->from}}</span>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">To</label>
                <span class="form-control">{{$event->to}}</span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">CC</label>
                <span class="form-control">{{$event->cc}}</span>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">CCO</label>
                <span class="form-control">{{$event->cco}}</span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">Date</label>
                <span class="form-control">{{$event->getDate()}}</span>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">Proposal</label>
                <span class="form-control">{{$event->getProposal()}}</span>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label">Subject</label>
                <span class="form-control">{{$event->subject}}</span>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label">Body</label>
                <span class="form-control">{{$event->body}}</span>
            </div>
        </div>
    </div>
</div>
