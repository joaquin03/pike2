<div class="row">
    <!-- Name -->
    <div class="col-md-6">
        <div class="form-group">
            <label for="name" class="col-md-2 control-label">Name*</label>
            <div class="col-md-10">
                <input id="name" type="text" name="name" class="form-control"
                       placeholder="Important Email" v-model="activity.name" required="required">
            </div>
        </div>
    </div>
    <!-- Location -->
    <div class="col-md-6">
        <div class="form-group">
            <label for="location" class="col-md-2 control-label">Location*</label>
            <div class="col-md-10">
                <input id="location" type="text" name="location" class="form-control" required="required"
                       placeholder="WTC Tower 1 office 401"  v-model="activity.location">
            </div>
        </div>
    </div>
</div>

<div class="row">
    <!-- Start -->
    <div class="col-md-6">
        <div class="form-group">
            <label for="start" class="col-md-2 control-label">Start*</label>
            <div class="col-md-4">
                <input id="start" type="text" name="from"  v-model="activity.start_date" required="required"
                       class="form-control datetimepicker3" placeholder="02/11/2018" >
            </div>
            <div class="col-md-3 input-group">
                <div class="input-group bootstrap-timepicker timepicker">
                    <input class="timepicker2 form-control" type="text" v-model="activity.start_time" >
                    <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                </div>

            </div>
        </div>
    </div>
    <!-- End -->
    <div class="col-md-6">
        <div class="form-group">
            <label for="end" class="col-md-2 control-label">End*</label>
            <div class="col-md-4">
                <input id="end" type="text" name="from"  v-model="activity.end_date" required="required"
                       class="form-control datetimepicker3" placeholder="02/11/2017" >
            </div>
            <div class="col-md-3 input-group">
                <div class="input-group bootstrap-timepicker timepicker">
                    <input class="form-control timepicker2" type="text" v-model="activity.end_time">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                </div>

            </div>
        </div>
    </div>
</div>
<div class="row">
    <!-- Summary -->
    <div class="col-md-12">
        <div class="form-group">
            <label for="summary" class="col-md-1 control-label">Summary*</label>
            <div class="col-md-11">
            <textarea name="summary" id="summary" cols="30" rows="10" class="form-control"
                      v-model="activity.summary" required="required"></textarea>
            </div>
        </div>
    </div>

</div>


