<div class="col-md-6">
    <div class="form-group">
        <label for="title" class="col-md-2 control-label">Title*</label>
        <div class="col-md-10">
            <input id="title"
                   name="title"
                   class="form-control"
                   v-model="activity.title"
                   required="required"
            />
        </div>
    </div>
</div>
<div class="col-md-3">
    <div class="form-group">
        <label for="due_date" class="col-md-5 control-label">Due Date*</label>
        <div class="col-md-7">
            <input id="due_date" type="text" name="due_date"  v-model="activity.due_date" required="required"
                   class="form-control datetimepicker3" placeholder="02/11/2018" >
        </div>
    </div>
</div>

<div class="col-md-3">
    <div class="form-group">
        <label for="end" class="col-md-8 control-label">Is Finished</label>
        <div class="col-md-4">
            <input type="checkbox" v-model="activity.is_finish" name="is_finish" title="is finished?" />
        </div>
    </div>
</div>
<div class="col-md-12">
    <div class="form-group">
        <label for="description" class="col-md-1 control-label">Description*</label>
        <div class="col-md-11">
            <textarea name="description"
                      id="description"
                      cols="30" rows="10"
                      class="form-control"
                      v-model="activity.description"
                      required="required"
            ></textarea>
        </div>
    </div>
</div>
<div class="col-md-12">
    <div class="form-group">
        <label for="assign_to_user_id" class="col-md-1 control-label">Assigned to*</label>
        <div class="col-md-11">
            <select name="assign_to_user_id"
                    id="assign_to_user_id"
                    class="form-control"
                    v-model="activity.assign_to_user_id"
                    required="required"
            >
                @foreach($users as $user)
                    <option value="{{ $user->id }}">{{ $user->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>
