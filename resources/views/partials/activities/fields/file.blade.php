<div class="col-md-6">
    <div class="form-group">
        <label for="title" class="col-md-2 control-label">Name*</label>
        <div class="col-md-10">
            <input id="name" type="text" name="name" class="form-control"
                   placeholder="Name file" v-model="file.name" required="required">
        </div>
    </div>
</div>

<div class="col-md-6">
    <div class="form-group">
        <label for="file" class="col-md-1 control-label">File*</label>
        <div class="col-md-11">
            <input type="file" name="file" id="file" class="form-control"
                   v-model="file.file" required="required"/>
        </div>
    </div>
</div>

<div class="col-md-12">
    <div class="form-group">
        <label for="notes" class="col-md-1 control-label">Notes</label>
        <div class="col-md-11">
            <textarea id="notes" name="notes" cols="30" rows="5" v-model="file.notes" class="form-control"></textarea>
        </div>
    </div>
</div>