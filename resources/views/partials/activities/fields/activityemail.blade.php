<div class="col-md-6">
    <div class="form-group">
        <label for="from" class="col-md-2 control-label">From*</label>
        <div class="col-md-10">
            <input id="from"
                   type="email"
                   name="from"
                   v-model="activity.from"
                   required="required"
                   class="form-control"
            />
        </div>
    </div>
</div>
<div class="col-md-6">
    <div class="form-group">
        <label for="to" class="col-md-2 control-label">To*</label>
        <div class="col-md-10">
            <input id="to"
                   type="email"
                   name="to"
                   class="form-control"
                   required="required"
                   v-model="activity.to"
            />
        </div>
    </div>
</div>
<div class="col-md-6">
    <div class="form-group">
        <label for="cc" class="col-md-2 control-label">CC</label>
        <div class="col-md-10">
            <input id="cc"
                   type="email"
                   name="cc"
                   v-model="activity.cc"
                   class="form-control"
            />
        </div>
    </div>
</div>
<div class="col-md-6">
    <div class="form-group">
        <label for="cco" class="col-md-2 control-label">CCO</label>
        <div class="col-md-10">
            <input id="cco"
                   type="email"
                   name="cco"
                   v-model="activity.cco"
                   class="form-control"
            />
        </div>
    </div>
</div>
<div class="col-md-6">
    <div class="form-group">
        <label for="date" class="col-md-2 control-label">Date*</label>
        <div class="col-md-10">
            <input id="date"
                   name="date"
                   v-model="activity.date"
                   class="form-control datetimepicker3"
            />
        </div>
    </div>
</div>
<div class="col-md-6">
    <div class="form-group">
        <label for="Proposal" class="col-md-2 control-label">Proposal*</label>
        <div class="col-md-10">
            <select name="proposal"
                    id="proposal"
                    class="form-control select2"
                    v-model="activity.proposal"
            >
                <option value="">&lt;no proposal&gt;</option>
                <option value="waiting_response">Waiting Response</option>
                <option value="accepted">Accepted</option>
                <option value="waiting_response">Declined</option>
            </select>
        </div>
    </div>
</div>
<div class="col-md-12">
    <div class="form-group">
        <label for="subject" class="col-md-1 control-label">Subject*</label>
        <div class="col-md-11">
            <input id="subject"
                   name="subject"
                   class="form-control"
                   v-model="activity.subject"
                   required="required"
            />
        </div>
    </div>
</div>
<div class="col-md-12">
    <div class="form-group">
        <label for="body" class="col-md-1 control-label">Body*</label>
        <div class="col-md-11">
            <textarea name="body"
                      id="body"
                      cols="30"
                      rows="10"
                      class="form-control"
                      v-model="activity.body"
                      required="required"
            ></textarea>
        </div>
    </div>
</div>
