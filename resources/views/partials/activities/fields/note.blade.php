
<!-- Title -->
<div class="col-md-12">
    <div class="form-group">
        <label for="title" class="col-md-1 control-label">Title*</label>
        <div class="col-md-11">
            <input id="title" type="text" name="title" class="form-control"
                   placeholder="Important Note" v-model="activity.title" required="required">
        </div>
    </div>
</div>

<!-- Body -->
<div class="col-md-12">
    <div class="form-group">
        <label for="body" class="col-md-1 control-label">Body*</label>
        <div class="col-md-11">
            <textarea name="body" id="body" cols="30" rows="10" class="form-control"
                      v-model="activity.body" required="required"></textarea>
        </div>
    </div>
</div>
