<div class="create-activity-btn btn-group pull-right">
    <button type="button"
            data-toggle="dropdown"
            class="btn green btn-sm btn-outline dropdown-toggle"
    >
        <i class="fa fa-angle-down"></i>Create Activity
    </button>
    <ul class="dropdown-menu pull-right" role="menu">
        <li>
            <a href="{{route($company->getType() . '.activity.create', ['activity' => 'Email', $company->getType() => $company])}}">
                <i class="icon-envelope-open"></i> Email
            </a>
        </li>
        <li>
            <a href="{{route($company->getType() . '.activity.create', ['activity' => 'Meeting', $company->getType() => $company])}}">
                <i class="icon-users"></i> Meeting
            </a>
        </li>
        <li>
            <a href="{{route($company->getType() . '.activity.create', ['activity' => 'Call', $company->getType() => $company])}}">
                <i class="icon-call-in"></i> Calls
            </a>
        </li>
        <li>
            <a href="{{route($company->getType() . '.activity.create', ['activity' => 'Note', $company->getType() => $company])}}">
                <i class="icon-note"></i> Note
            </a>
        </li>
        <li>
            <a href="{{route($company->getType() . '.activity.create', ['activity' => 'Reminder', $company->getType() => $company])}}">
                <i class="icon-bell"></i> Reminder
            </a>
        </li>
        <hr/>
        <li>
            <a href="{{route($company->getType() . '.file.create', [$company->getType() => $company])}}">
                <i class="icon-paper-clip"></i> Files
            </a>
        </li>
    </ul>
</div>