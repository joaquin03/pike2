@extends('backpack::layout')

@section('title')
    Creating {{ucfirst($eventName)}} for {{ ucfirst($company->getType()) }} {{ $company->name }}
@endsection

@section('tabs')
    @include('company.activity-combo')
@endsection

@section('content')
    <section class="content-header">
        <h1>
            Create <span>{{ $eventName }}</span>
        </h1>
    </section>

    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <form class="form form-horizontal form-row-seperated">
                        <div class="box-header with-border">
                            <h3 class="form-section">{{ucfirst($eventName)}} Information</h3>
                        </div>
                        <div class="box-body row">
                            @include('partials.activities.fields.'.$eventName)

                            <h3 class="form-section">Activity information</h3>
                            @include('partials.activities._fields')

                            <div style="text-align: right;">
                                @if(false)
                                    <a href="{{ route($company->getType() . '.show', [$company->getType() => $company]) }}" class="btn">Cancel</a>
                                @endif
                                <button type="submit" class="btn green"><i class="fa fa-check"></i> Create</button>
                            </div>
                        </div>

                </form>
                </div>
            </div>
        </div>
    </div>
@endsection


