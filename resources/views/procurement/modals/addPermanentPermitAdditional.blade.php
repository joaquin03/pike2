<div id="permanent-permit-add-{{$provider['id']}}" class="col-md-12" style="margin-left: -15px;">
    <div class="form-group col-md-2">
        <select class="form-control select2" id="permanent-permit_provider_id-{{$provider['id']}}">
            <option value=""> Additional </option>
        @forelse($provider['permits'] as $permit)
            @if(strpos($permit['code'], 'Total')===false
                        && strpos($permit['type'], 'ADD')===false)
                    <option value="{{$permit['id']}}"> {{  $permit['code'].'-'.$permit['type'] }}</option>
                @endif
            @empty
                <option disabled selected>No Permits</option>
            @endforelse
        </select>
    </div>
    <div class="form-group col-md-1">
        <input type="text" id="permanent-permit_tax-{{$provider['id']}}" class="form-control" placeholder="Tax">
    </div>
    <div class="form-group col-md-1">
        <input type="text" id="permanent-cost-{{$provider['id']}}" class="form-control" placeholder="Cost">
    </div>
    <div class="form-group col-md-2">
        <button id='add-permanent-permit-additional' onClick="createPermanentAdditional({{$provider['id']}})" class="btn btn-default add-button">
            Add Additional
        </button>
    </div>
</div>

@push('script_stack')
    <script>
        createPermanentAdditional = function(id) {
            var code = $('#permanent-permit_provider_id-'+id).val();
            var tax =  $('#permanent-permit_tax-'+id).val();
            var cost = $('#permanent-cost-'+id).val();
            var item = [code, tax, cost];

            return createPermanentPermitAdditional(item, {{$operation->id}});
        }
    </script>
@endpush
