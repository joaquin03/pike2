//TODO: eliminar vista?


<div class="col-lg-12">
    <div class="row-fluid summary">
        <h3>Additionals</h3>
        <div id='itinerary-additionals-{{$itinerary}}'></div>
    </div>
</div>
<script>
    var columns = [
        {data: 'id', readOnly: true, className: "htRight"},
        {data: 'provider_id', readOnly: true, className: "htRight"},
        {data: 'service', type: 'text', readOnly: true},
        {data: 'tax', type: 'numeric', format: '0.0%'},
        {data: 'amount', type: 'numeric', format: '0.000'},
        {data: 'price', type: 'numeric', format: '0.000', readOnly: true},
        {data: 'procurement_status', renderer: statusRenderer, readOnly: true},
        {data: 'delete_additional', renderer: AdditionalsServiceRendererExtras, readOnly: true, className: "htRight"}


    ];
    var colHeaders = ['', '', 'Service', 'Tax', 'Cost', 'Total', 'Invoiced', ''
    ];
    var colWidths = [
        1,1, 300, 100, 100, 100, 80, 70
    ];

    var hot_tables_services_additionals = [];
            @foreach($servicesAdditionals ?? [] as $service)
    var hotElement = document.querySelector('#itinerary-additionals-{{$itinerary}}');

    var hotSettings = {
        data: {!! json_encode($service['additionals'], true) !!} ,
        columns: columns,
        rowHeaders: false,
        width: 800,
        autoWrapRow: true,
        height: 80 + 35 * {{count($service['additionals'])}},
        colHeaders: colHeaders,
        colWidths: colWidths,
        afterChange: function (changes, source) {
            if (source == 'edit') {
                var item = this.getData()[changes[0][0]];
                updateServiceAdditional(item);
            }

        },
    };
    hot_tables_services_additionals[{{$itinerary}}] = new Handsontable(hotElement, hotSettings);

    @endforeach
        createServiceAdditional = function (item) {

        if (item[1] == 'TOTAL') {
            return
        }
        $.ajax({
            type: "POST",
            url: '/api/additional/service/' + {{$itinerary}},
            data: {
                'iti_pro_ser_id': item[0],
                'provider_id': item[1],
                'procurement_tax': item[2],
                'procurement_cost': item[3],
                'is_additional': 1,
                'service_origin': "Procurement",

            }
        }).success(function (data) {
            data = data.data;
            getItineraryProviderServices({{$itinerary}});
            renderItineraries(providers);
        });
    };

    updateServiceAdditional = function (item) {
        if (item[1] == 'TOTAL') {
            return
        }
        $.ajax({
            type: "PUT",
            url: '/api/additional/service/' + item[0],
            data: {
                'permit_id': item[0],
                'tax': item[2],
                'billing_unit_price': item[3],
                'billing_price': item[4],
                'type': 'invoice'
            }
        }).success(function (data) {
            data = data.data;
            reloadServiceAdditionals(data.additionals);
        }).error(function (data) {
            data = data.data;
            reloadServiceAdditionals(data.additionals);
        });
    };


    reloadServiceAdditionals = function (providers) {
        for (var i = 0; i < providers.length; i++) {
            var provider = providers[i];
            var items = provider['additionals'];
            var table = hot_tables_services_additionals[provider['id']];
            table.loadData(items);
            table.updateSettings({
                height: 35 + 24 * items.length,
                maxRows: items.length,
            })

        }
    };


    function renderServicesBillItem(instance, td, row, col, prop, value, cellProperties) {
        var item = instance.getData()[row];
        if (value != null) {
            td.innerHTML = value + ' - <input type="checkbox" class="bill_additionals" ' +
                'name="bill_permits_administration_fees[]" value="' + item[0] + '">';
        } else {
            td.innerHTML = '';
        }
    }

    addServiceAdditionalInvoice = function (item) {

        var itinerary = {{$itinerary}};
        var data = hot_tables_services_additionals[itinerary].getData();
        var service = [];
        service.push(item[0]);
        var provider = item[1];
        $.ajax({
            type: "POST",
            url: '/api/procurement/' + itinerary + '/additional/add-invoice/',
            data: {
                'itinerary_id': itinerary,
                'provider_id': provider,
                'service': service,
                'note': "Service Additional"
            }
        }).success(function (data) {
            $(location).attr('href', '/admin/invoices/' + data.id + '/edit');
        });
    };

    function addServiceAdditionalInvoiceRenderer(instance, td, row, col, prop, value, cellProperties) {
        var instance2 = JSON.stringify(instance.getData()[row]);
        td.innerHTML = '';
        if (instance.getData()[row][1] != "Total" && instance.getData()[row][5] != 1) {
            newHtml = "<button href='' class ='btn btn-sd btn-info' title='Add Invoice' onclick='addServiceAdditionalInvoice(" + instance2 + ")' " +
                "style='padding: 3px 7px;font-size: 10px;margin: 2px;'> <i class='fa  fa-plus-square'></i></button>";
            td.innerHTML += newHtml;
        }
    }

    function statusRenderer(instance, td, row, col, prop, value, cellProperties) {
        var instance2 = JSON.stringify(instance.getData()[row]);
        var instanceArray = instance.getData()[row];
        var url = "/admin/provider-statement/additional/" + instanceArray[0];
        td.innerHTML = "";

        if (instance.getData()[row][1] == 'Total') {
            return;
        }
        if (instance.getData()[row][1] != "Total" && instance.getData()[row][6] != 1) {
            newHtml = "<i class='fa fa-check-square' style='color:darkgray'></i>";
        }
        else if (instance.getData()[row][1] != "Total" && instance.getData()[row][6] == 1) {
            newHtml =
                "<a href='" + url + "' type='button' class ='btn btn-sd btn-success' title='Show Invoice' " +
                "style='padding: 3px 7px;font-size: 10px;margin: 2px;'><i class='fa fa-check' style='color:white; font-size: 11px'></i> SHOW</a>";
        }
        td.innerHTML = newHtml;
    }

    function AdditionalsServiceRendererExtras(instance, td, row, col, prop, value, cellProperties) {
        var instance2 = JSON.stringify(instance.getData()[row]);
        newHtml = "";

        if (instance.getData()[row][1] == 'Total') {
            return;
        }

        if (instance.getData()[row][1] != "Total" && instance.getData()[row][6] != 1) {
            newHtml += "<button href='' class ='btn btn-sd btn-info' title='Add Invoice' onclick='addServiceAdditionalInvoice(" + instance2 + ")' " +
                "style='padding: 3px 7px;font-size: 10px;margin: 2px;'> <i class='fa  fa-plus-square'></i></button>";
        }
        newHtml += "<button href='' onclick='deleteAdditionalService(" + instance2 + ")' class ='btn btn-sd btn-danger' " +
            "style='padding: 3px 7px;font-size: 10px;margin: 2px; float: right;'> <i class='fa fa-trash-o'></i></button>";
        td.innerHTML = newHtml;
    }

    function deleteAdditionalService(item) {
        if (confirm('Are you sure you want to delete this permit?')) {
            $.ajax({
                type: "DELETE",
                url: '/api/additional/service/' + item[0] + '?itinerary_id=' +{{$itinerary}},
            }).success(function (data) {
                if (data.data == false) {
                    return alert(data.message);
                }
                data = data.data;
                reloadServiceAdditionals(data.additionals);
            }).error(function(data) {
                alert(data.responseJSON.message);
            });
        }
    }

</script>
