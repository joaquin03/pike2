<div id="permanent_permission-add-{{$provider['id']}}" class="col-md-12" style="margin-left: -15px;">
    <div class="form-group col-md-2">
        <select class="form-control select2" name="permanent_permission_provider_id">
            <option value=""> Over Flight </option>
        @forelse($provider['permits'] as $permanentPermission)
                @if(strpos($permanentPermission['type'], 'OVF')===false && strpos($permanentPermission['code'], 'Total')===false)
                    && )
                    <option value="{{$permanentPermission['id']}}"> {{  $permanentPermission['code'].'-'.$permanentPermission['type'] }}</option>
                @endif
            @empty
                <option disabled selected>No Permissions</option>
            @endforelse
        </select>
    </div>
    <div class="form-group col-md-1">
        <input type="text" name="permanent_permission_tax" class="form-control" placeholder="Tax">
    </div>
    <div class="form-group col-md-1">
        <input type="text" name="permanent_permission_cost" class="form-control" placeholder="Cost">
    </div>
    <div class="form-group col-md-2">
        <button onClick="addPermanentPermission('#permanent_permission-add-{{$provider['id']}}')" class="btn btn-default add-button">
            Add Over Flight
        </button>
    </div>
</div>

