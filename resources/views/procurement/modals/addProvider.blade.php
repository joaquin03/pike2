<div id="service-add-{{$provider['id']}}" class="form-inline col-md-8">

    <input type="hidden" name="provider_id" value="{{$provider['id']}}">
    <div class="form-group col-md-3">
        <label for="">Service</label>
        <select class="form-control" name="service_id">
            @forelse($provider['provider_services']['data'] as $service)
                <option value="{{$service['id']}}"> {{ $service['service'] }}</option>
            @empty
                <option disabled selected>No Services</option>
            @endforelse
        </select>
    </div>

    <div class="form-group col-md-2">
        <label for="" class="">Cost</label>
        <input type="text" name="procurement_cost" class="form-control">
    </div>
    <div class="form-group col-md-2">
        <label for="">Quantity</label>
        <input type="text" name="procurement_quantity" class="form-control">
    </div>

    <div class="form-group col-md-3">
        <label for="">Status</label>
        <select name="procurement_status" class="form-control">
            <option value="Requested"> Requested </option>
            <option value="Canceled"> Canceled </option>
            <option value="Done"> Done </option>
        </select>
    </div>
    <div>
    </div>
    <button onClick="addService('#service-add-{{$provider['id']}}')" class="btn btn-default col-md-2 add-button"
            style="margin-top: 30px;">
        Add Service
    </button>
</div>