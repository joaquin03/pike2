<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title" id="myModalLabel" style="margin-left: 43%">Total Cost</h4>
</div>
<div class="modal-body row">
    <table id="crudTable" class="table table-bordered table-striped display" style="width:100%;">
        <tr>
            <td><h4>Services:</h4></td>
            <td><h4>$ {{round($total['service'],3)}}</h4></td>
        </tr>
        <tr>
            <td><h4>Permits:</h4></td>
            <td><h4>$ {{round($total['aircraft_permissions'],3)}}</h4></td>
        </tr>
        <tr>
            <td><h4>Permanent Permits: </h4></td>
            <td><h4>$ {{round($total['permanent_permissions'],3)}}</h4></td>
        </tr>
        <tr>
            <td><h4>Additionals: </h4></td>
            <td><h4>$ {{round($total['additionals'],3)}}</h4></td>
        </tr>
        <tr >
            <td><h4 style="font-weight:bold">Total: </h4></td>
            <td><h4 style="font-weight:bold">$ {{round($total['total'],3)}}</h4></td>
        </tr>
   </table>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
