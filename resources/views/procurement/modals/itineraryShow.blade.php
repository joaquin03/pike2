<div>
    @php
        $providers = $providers['data'];
    @endphp
    <div class="table_row_slider">
        <div class="row">
            <div class="col-lg-12">
                <div class="row-fluid summary" id="itineraries-billing">
                    <h3 class="col-md-6">
                        <p id="itinerary_total"> Total: USD {{$itinerary->getProviderServicesTotal()}}</p>
                    </h3>

                    @foreach($providers as $provider)
                        <div class="row">
                            <h3 class="col-md-12">{{ $provider['name'] }}</h3>
                            <div class="col-md-10" style="overflow: hidden">
                                <div id='itinerary_{{$itinerary->id}}-provider_{{$provider['id'] }}'
                                     class="provider-services hot-{{$provider['id']}}"></div>
                            </div>
                            <div class="col-md-2">
                                @if ($provider['bill_id'] == null)
                                    <button class="btn btn-info" onClick="addServiceInvoice({{$provider['id'] }})">
                                        Add Invoice
                                    </button>
                                @else
                                    <a href="{{route('crud.bills-provider.edit', ['bill_id'=>$provider['bill_id'] ])}}"
                                       class="btn btn-info">Show Invoice</a>
                                @endif
                            </div>
                            @include('procurement.modals.addService', ['provider'=>$provider])
                        </div>
                    @endforeach

                </div>
            </div>
        </div>


        <hr>
        <div class="col-lg-12 itineraryNote">
            <div class="col-md-4">
                @if($itinerary->notes)
                    <div class="col-md-12">
                        <label class="control-label col-md-4">General Notes:</label>
                        <span class="col-md-8">{{$itinerary->notes}}</span>
                    </div>
                @endif
                @if($itinerary->quotation_notes)
                    <div class="col-md-12">
                        <label class="control-label col-md-4">Quotation Notes:</label>
                        <span class="col-md-8">{{$itinerary->quotation_notes}}</span>
                    </div>
                @endif
                @if($itinerary->operation_notes)
                    <div class="col-md-12">
                        <label class="control-label col-md-4" style="text-align: left">Op. Notes:</label>
                        <span class="col-md-8">{{$itinerary->operation_notes}}</span>
                    </div>
                @endif
                <div class="col-md-12">
                    <label for="note"> Procurement Notes</label>
                    <br>
                    <textarea id="note" style="min-width: 100%;" rows="2" cols="36.5" placeholder=""
                              onkeyup="updateNotes()">{{$itinerary->procurement_notes}}</textarea>
                </div>
            </div>
            <div class="col-md-6">
                <div class="col-md-6">
                    <label for="procurement_service_status">Service Status</label>
                    <br>
                    <select class="form-control select2" style="margin-left: 0px; width: 300px"
                            id="procurement_service_status" onchange="updateProcurementServicesStatus()">
                        <option value=''>-</option>
                        <option value="Done"
                                @if($itinerary->getProcurementServicesStatus()=='Done')
                                selected
                                @endif>Done
                        </option>
                        <option value="Requested"
                                @if($itinerary->getProcurementServicesStatus()=='Requested')
                                selected
                                @endif>Requested
                        </option>
                        <option value="Not Required"
                                @if($itinerary->getProcurementServicesStatus()=='Not Required')
                                selected
                                @endif>Not Required
                        </option>
                    </select>
                </div>
                <div class="col-md-6">
                    <label for="procurement_fuel_status">Fuel Status</label>
                    <br>
                    <select class="form-control select2" style="margin-left: 0px; width: 300px"
                            id="procurement_fuel_status" onchange="updateProcurementServicesStatus()">
                        <option value>-</option>
                        <option value="Done"
                                @if($itinerary->getProcurementFuelStatus()=='Done') selected @endif>Done
                        </option>
                        <option value="Requested"
                                @if($itinerary->getProcurementFuelStatus()=='Requested') selected @endif>Requested
                        </option>
                        <option value="Not Required"
                                @if($itinerary->getProcurementFuelStatus()=='Not Required') selected @endif>Not Required
                        </option>
                    </select>
                </div>

            </div>
        </div>
        <hr class="row separator">


        <script>

            function updateNotes() {
                $.ajax({
                    type: "PUT",
                    url: '/api/itinerary/{{$itinerary->id}}',
                    data: {
                        'itinerary': [
                            {'procurement_notes': $("#note").val()}]
                        ,

                    },
                }).success(function (data) {
                });
            }

            showError = function (data) {
                PNotify.removeAll();
                new PNotify({
                    text: data.responseJSON.message,
                    type: "error"
                });
            };

            var columns = [
                {data: 'id', readOnly: true},
                {data: 'parent_name', type: 'text', readOnly: true},
                {data: 'service', type: 'text', readOnly: true},
                {data: 'operation_status', type: 'text', readOnly: true},
                {data: 'procurement_quantity', type: 'numeric', format: '0.000'},
                {data: 'procurement_cost', type: 'numeric', format: '0.000'},
                {data: 'procurement_tax', type: 'numeric', format: '0.0%'},
                {data: 'procurement_adm_fee', type: 'numeric', format: '0.0%'},
                {data: 'final_cost', type: 'numeric', format: '0.000', readOnly: true},
                {data: 'procurement_status', renderer: itineraryStatusRender, readOnly: true},
                {data: 'is_additional', renderer: isAdditionalRender, readOnly: true},
                {data: 'delete_service', renderer: extrasRenderer, readOnly: true},
            ];
            var colHeaders = [
                'ID', 'Parent', 'Service', 'Op. Status', 'Quantity', 'Cost',
                'Tax %', 'Adm Fee%', 'Final Cost', 'Invoiced', 'Additional', ''];

            var hot_tables_{{$itinerary->id}} = [];
            var colWidths = [
                2, 125, 150, 90, 80,
                70, 70, 85, 90, 80, 80, 40
            ];
            var providers = {!! json_encode($providers, true) !!};
            var lastParentService = null;


            renderItineraries = function (providers) {
                for (var i = 0; i < providers.length; i++) {
                    var provider = providers[i];
                    var items = provider['services']['data'];
                    var hotElement = document.querySelector('#itinerary_{{$itinerary->id}}-provider_' + provider['id']);
                    var hotSettings = {
                        data: items,
                        columns: columns,
                        rowHeaders: false,
                        width: 1300,
                        height: 50 + 30 * items.length,
                        colWidths: colWidths,
                        colHeaders: colHeaders,
                        maxRows: items.length,
                        afterChange: function (changes, source) {
                            if (source == 'edit') {
                                var item = this.getData()[changes[0][0]];
                                updateItinerary(item);
                            }
                        },
                        cells: function (row, col, prop) {
                            var cellProperties = {};
                            var data = items[row];
                            if (data && data.parent_name != null) {

                                cellProperties.renderer = function (hotInstance, td, row, col, prop, value) {
                                    td.style.backgroundColor = '#e6f5f9';
                                    if (col == 11) {
                                        extrasRenderer(hotInstance, td, row, col, prop, value, prop);
                                    } else {
                                        td.textContent = value;
                                    }
                                };
                                return cellProperties;
                            }

                        }
                    };
                    hot_tables_{{$itinerary->id}}[provider['id']] = new Handsontable(hotElement, hotSettings);
                }
            };

            renderItineraries(providers);

            getItineraryProviderServices = function (itineraryId) {
                $.ajax({
                    type: "GET",
                    url: '/api/itinerary/' + itineraryId + '/service?in_procurement=1',
                }).success(function (data) {
                    reloadProviderServices(data);
                    updateProviderServicesTotal();

                })
            };

            //todo: DEPRECATED, esperar por fórmula de procurement
            //            function renderAdmFeeByCost(instance, td, row, col, prop, value, cellProperties) {
            //                td.innerHTML = '';
            //                var item = instance.getData()[row];// if it has bills, checkbox is disabled
            //                td.innerHTML = '<input type="checkbox" data-parent="' + lastParentService + '" class="adm_fee_by_cost" name="adm_fee_by_cost[]" value="' + item[0] + '">';
            //
            //                if (item[1] != null) {
            //                    lastParentService = item[0];
            //                    td.innerHTML = '';
            //                }
            //            }

            updateItinerary = function (item) {
                if (item[2] == 'TOTAL' || item[1] != null || item[9] == 1) { //total, padre o invoiced
                    getItineraryProviderServices({{$itinerary->id}});
                    return;
                }
                $.ajax({
                    type: "PUT",
                    url: '/api/itinerary/{{$itinerary->id}}/service/' + item[0],
                    data: {
                        'procurement_quantity': item[4],
                        'procurement_cost': item[5],
                        'procurement_tax': item[6],
                        'procurement_adm_fee': item[7],
                        'procurement_status': item[9],
                        'billing-type': 'invoice',
                    }
                }).success(function (data) {
                    getItineraryProviderServices({{$itinerary->id}});
                }).error(function (data) {
                    showError(data);
                    getItineraryProviderServices({{$itinerary->id}});
                });
            };

            uploadBillingProvider = function (provider) {
                $('.upload-billing').on('change', function () {
                });
            };


            reloadProviderServices = function (providers) {
                providers = providers.data;
                for (var i = 0; i < providers.length; i++) {
                    var provider = providers[i];
                    var items = provider['services']['data'];
                    var table = hot_tables_{{$itinerary->id}}[provider['id']];
                    table.loadData(items);
                    table.updateSettings({
                        height: 40 + 30 * items.length,
                        maxRows: items.length,
                        cells: function (row, col, prop) {
                            var cellProperties = {};
                            var data = items[row];

                            if (data && data.parent_name != null) {

                                cellProperties.renderer = function (hotInstance, td, row, col, prop, value) {
                                    td.style.backgroundColor = '#e6f5f9';
                                    if (col == 11) {
                                        extrasRenderer(hotInstance, td, row, col, prop, value, prop);
                                    } else {
                                        td.textContent = value;
                                    }
                                };
                                return cellProperties;
                            }
                        }
                    });
                }
            };


            addService = function (provider) {
                $.ajax({
                    type: "POST",
                    url: '/api/itinerary/{{$itinerary->id}}/service',
                    data: {
                        'service_id': $(provider + ' [name=service_id]').val(),
                        'provider_id': $(provider + ' [name=provider_id]').val(),
                        'procurement_quantity': $(provider + ' [name=procurement_quantity]').val(),
                        'procurement_cost': $(provider + ' [name=procurement_cost]').val(),
                        'status': $(provider + ' [name=status]').val(),
                        'service_origin': "Procurement"
                    },
                }).success(function (data) {
                    getItineraryProviderServices({{$itinerary->id}});
                }).error(function (data) {
                    showError(data)
                });
            };

            $(".delete_service").on("click", function () {
                return confirm("Do you want to delete this service?");
            });


            function extrasRenderer(instance, td, row, col, prop, value, cellProperties) {
                if (instance.getData()[row][1] == 'TOTAL') {
                    return;
                }
                var instance2 = JSON.stringify(instance.getData()[row]);
                newHtml = "<button href='' onclick='deleteService(" + instance2 + ")' class ='btn btn-sd btn-danger' " +
                    "style='padding: 3px 7px;font-size: 10px;margin: 2px;'> <i class='fa fa-trash-o'></i></button>";
                td.innerHTML = newHtml;
            }


            function deleteService(item) {
                if (confirm('Are you sure you want to delete this service?')) {
                    $.ajax({
                        type: "DELETE",
                        url: '/api/itinerary/{{$itinerary->id}}/service/' + item[0],
                    }).success(function (data) {
                        getItineraryProviderServices({{$itinerary->id}});
                    }).error(function (data) {
                        return alert(data.responseJSON.message);
                    });
                }
            }


            function createBill(data) {
                var providerId = data.getAttribute('data');
                $('#form_upload-' + providerId).submit();
            };

            function itineraryStatusRender(instance, td, row, col, prop, value, cellProperties) {
                var instance2 = JSON.stringify(instance.getData()[row]);
                var instanceArray = instance.getData()[row];
                var url = "/admin/provider-statement/service/" + instanceArray[0];
                newHtml = "";
                if (value != '1') {
                    newHtml += "<i class='fa fa-check-square' style='color:darkgray'></i>";

                } else if (value == '1') {
                    newHtml += "<a href='" + url + "' type='button' class ='btn btn-sd btn-success' title='Show Invoice' " +
                        "style='padding: 3px 7px;font-size: 10px;margin: 2px;'><i class='fa fa-check' style='color:white; font-size: 11px'></i> SHOW</a>";
                }
                td.innerHTML = newHtml;
            }

            function isAdditionalRender(instance, td, row, col, prop, value, cellProperties) {
                var instance2 = JSON.stringify(instance.getData()[row]);
                var instanceArray = instance.getData()[row];
                newHtml = "";
                if (value != '1') {
                    newHtml += "<i class='fa fa-check-square' style='color:darkgray'></i>";

                } else if (value == '1') {
                    newHtml += "<i class='fa fa-check-square' style='color:green'></i>";
                }
                td.innerHTML = newHtml;
            }


            function customHtmlRenderer(instance, td, row, col, prop, value, cellProperties) {
                if (value) {
                    newHtml = "<a href='/storage/" + value + "'>" +
                        "<i class='fa fa-cloud-download'/></a>";
                    td.innerHTML = newHtml;

                    return td;
                }
            }

            uploadBillingProvider();


            addServiceInvoice = function (providerId) {
                var data = hot_tables_{{$itinerary->id}}[providerId].getData();
                var services = [];
                for (var i = 0; i < data.length; i++) {
                    if (data[i][0] != null) {
                        if (data[i][1] == null) { // si no es parent
                            services.push(data[i][0]);
                        }
                    }
                }
                $.ajax({
                    type: "POST",
                    url: '/api/procurement/{{$itinerary->operation_id}}/add-invoice',
                    data: {
                        'itinerary_id': {{$itinerary->id}},
                        'provider_id': providerId,
                        'services': services,
                        'note': "Service"
                    },
                }).success(function (data) {
                    if (data.accounting_services.length == 0) {
                        alert('No services')
                    } else {
                        $(location).attr('href', '/admin/invoices/' + data.id + '/edit');
                    }
                });
            };

            function updateProcurementServicesStatus() {
                $.ajax({
                    type: "POST",
                    url: '/admin/itinerary-procurement/{{$itinerary->id}}/edit',
                    data: {
                        'procurement_fuel_status': $("#procurement_fuel_status").find(":selected").text(),
                        'procurement_service_status': $("#procurement_service_status").find(":selected").text(),
                    }
                }).success(function (data) {
                });
            }

            function updateProviderServicesTotal() {
                //todo
                document.getElementById("itinerary_total").innerHTML = 'Total: USD ' + '{{$itinerary->getProviderServicesTotal()}}';
            }

        </script>
    </div>