<div id="permit-add-{{$provider['id']}}" class="col-md-12" style="margin-left: -15px;">
    <div class="form-group col-md-2">
        <select class="form-control select2" name="permit_provider_id">
            <option value=""> Over Flight </option>
        @forelse($provider['permits'] as $permit)
            @if(strpos($permit['code'], 'Total')===false)
                    <option value="{{$permit['id']}}"> {{  $permit['code'].' - '.$permit['type'] }}</option>
            @endif
            @empty
                <option disabled selected>No Permits</option>
            @endforelse
        </select>
    </div>
    <div class="form-group col-md-1">
        <input type="text" name="permit_tax" class="form-control" placeholder="Tax">
    </div>
    <div class="form-group col-md-1">
        <input type="text" name="cost" class="form-control" placeholder="Cost">
    </div>
    <div class="form-group col-md-2">
        <button onClick="addPermit('#permit-add-{{$provider['id']}}')" class="btn btn-default add-button">
            Add Over Flight
        </button>
    </div>
</div>