{{ csrf_field() }}

<div class="col-md-12">
    <h4 class="form-section">GENERAL</h4>


    <div class="form-group col-md-4">
        <label class="control-label col-md-4">Code:</label>
        <span class="col-md-8">{{ $operation->code }}</span>
    </div>

    <div class="form-group col-md-4">
        <label class="control-label col-md-4">In Charge:</label>
        <span class="col-md-8">{{ $operation->user->name }}</span>
    </div>

    <div class="form-group col-md-4">
        <label class="control-label col-md-4">Operation status:</label>
        <span class="col-md-8">{{ $operation->state }}</span>
    </div>


    <div class="form-group col-md-12">
        <label class="control-label col-md-2">Quotation Notes:</label>
        <pre class="col-md-9 text-wrap" id="quotation-notes">{{$operation->quotation_notes}}</pre>
        <div class="col-md-1">
            <button type="button" class="btn btn-default" data-toggle="collapse"
                    onclick="showMore('quotation-notes')" id="more-quotation-notes"
                    style="float: right; display: none">Show more
            </button>
            <button type="button" class="btn btn-default" data-toggle="collapse"
                    onclick="showLess('quotation-notes')" id="less-quotation-notes"
                    style="float: right;display: none;">Show less
            </button>
        </div>
    </div>

    <div class="form-group col-md-12">
        <label class="control-label col-md-2">Operation Notes:</label>
        <pre class="col-md-9 text-wrap" id="operation-notes">{{$operation->operation_notes}}</pre>
        <div class="col-md-1">
            <button type="button" class="btn btn-default hide-button-align-right" data-toggle="collapse"
                    onclick="showMore('operation-notes')" id="more-operation-notes">Show more
            </button>
            <button type="button" class="btn btn-default hide-button-align-right" data-toggle="collapse"
                    onclick="showLess('operation-notes')" id="less-operation-notes"
            >Show less
            </button>
        </div>


        <label class="control-label col-md-2">General Notes:</label>
        <pre class="col-md-9 text-wrap" id="notes">{{$operation->notes}}</pre>
        <div class="col-md-1">
            <button type="button" class="btn btn-default hide-button-align-right" data-toggle="collapse"
                    onclick="showMore('notes')" id="more-notes">Show more
            </button>
            <button type="button" class="btn btn-default hide-button-align-right" data-toggle="collapse"
                    onclick="showLess('notes')" id="less-notes">Show less
            </button>
        </div>
        <label class="control-label col-md-2">Notes:</label>
        <textarea class="control-label col-md-9 round-box" name="procurement_notes"
                  class="form-control"
                  style="resize: vertical;padding: 0;"> {{$operation->procurement_notes}}</textarea>
    </div>
</div>

<div class="col-md-12">
    <h4 class="form-section">AIRCRAFT</h4>


    <div class="form-group col-md-4">
        <label class="control-label col-md-4">Registration</label>
        <span id="registration"
              class="col-md-8">{{$operation->aircraft ? $operation->aircraft->registration : ''}}</span>
        <span id="operation-aircraft-id"
              class="hidden">{{$operation->aircraft ? $operation->aircraft->id : ''}}</span>
    </div>

    <div class="form-group col-md-4">
        <label class="control-label col-md-4">Operator</label>
        <span class="col-md-8">@if($operation->aircraftOperator) {{  $operation->aircraftOperator->name }} @endif</span>
    </div>

    <div class="form-group col-md-4">
        <label class="control-label col-md-4">Client</label>
        <input id="client_id" value="{{ $operation->aircraftClient->id }}" hidden/>
        <span class="col-md-8">{{ $operation->aircraftClient->name }}</span>

    </div>
    <div class="form-group col-md-4">
        <label class="control-label col-md-4">MTOW</label>
        <div class="col-md-8">
            <span id="mtow" name="mtow" type="text"></span>
        </div>
    </div>
    <div class="form-group col-md-4">
        <label class="control-label col-md-4">Aircraft Type</label>
        <div class="col-md-8">
            <span id="aircraft_type" name="aircraft_type" type="text"></span>
        </div>
    </div>
    <div class="form-group col-md-4">
        <label class="control-label col-md-4">MTOW Type</label>
        <div class="col-md-8">
            <span id="mtow_type" name="mtow_type" type="text"></span>
        </div>
    </div>
    <div class="form-group col-md-12">
        <label class="control-label col-md-1">Route</label>
        <span class="col-md-11"
              style="text-align: left; padding-left: 50px;">{{ $operation->route }}</span>

    </div>
</div>

<div class="form-group col-md-12">
    <h4 class="form-section">PROCUREMENT STATUS</h4>

    <div class="col-md-2">
        <select class="form-control select2" name="procurement_status" id="procurement_status">
            @foreach(App\Models\Operation::$states as $state)
                @if($state != "Deleted")
                    <option value="{{$state}}"
                            {{(isset($operation) && $operation->procurement_status == $state)? 'selected="selected"' : ''}}
                    >
                        {{ $state }}
                    </option>
                @endif
            @endforeach
        </select>
    </div>
</div>

@include('general.specialFeaturesModal')


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript">

    $(document).ready(function () {
        showMTOWProcurement();
        hideButton('quotation-notes');
        hideButton('operation-notes');
        hideButton('notes');
        alertSpecialFeatures();

    });

    function showMTOWProcurement() {
        jQuery(function ($) {
            var aircraft_registration = $("#operation-aircraft-id").text();

            $.ajax({
                url: '{{url('/admin/aircraft/info')}}' + '/' + aircraft_registration,
                type: 'GET',
                dataType: "json",
                success: function (data) {

                    if (data['mtow']) {
                        $("#mtow").text(data['mtow']);
                    } else {
                        $("#mtow").text("");
                    }
                    if (data['mtow_type']) {
                        $("#mtow_type").text(data['mtow_type']);
                    } else {
                        $("#mtow_type").text("");
                    }
                    if (data['aircraft_type']) {
                        $("#aircraft_type").text(data['aircraft_type']);
                    } else {
                        $("#aircraft_type").text("");
                    }

                },
            });
        });
    }

    function hideButton(section) {
        if ($('#' + section).html().split(/\n/).length > 2) {
            $('#more-' + section).show();
        }
    }

    function showMore(section) {
        document.getElementById(section).style.height = 'auto';
        document.getElementById(section).style.maxHeight = '400px';
        $('#more-' + section).hide();
        $('#less-' + section).show();

    }

    function showLess(section) {
        document.getElementById(section).style.height = '35px';
        $('#less-' + section).hide();
        $('#more-' + section).show();
    }

    function alertSpecialFeatures() {
        var company_id = $('#client_id').val();
        var current_url = window.location.href;
        if (company_id && current_url.split('#')[1] != 'documents') {
            $.ajax({
                url: '{{url('/admin/company/alert/special-features')}}' + '/' + company_id,
                type: 'GET',
                dataType: "json",
                success: function (data) {
                    if (data['special_features']) {
                        console.log(data['special_features']);
                        $("#client-input-id").val(company_id);
                        $("#specialFeaturesModal").modal();

                    }
                },
                error: function (result) {
                    alert('error');
                }
            });

        }

    }

</script>


