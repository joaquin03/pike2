{{ csrf_field() }}

<div class="box-body">
    <h3 class="form-section" style="text-align: center; margin-top: 20px">DOCUMENTS</h3>

    <div class="col-md-3"></div>
    <div class="col-md-6">

        @include('vendor.backpack.crud.fields.operation_documents_view_only')
        
    </div>
</div>




