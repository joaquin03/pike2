@php
    $providersAdditionals = $permissionsAdditionals;
@endphp

<div class="col-lg-12">
    <div class="row-fluid summary" >
        <h3>Permissions Additionals</h3>
        <div id='permission-additionals-{{$provider['id']}}'></div>
    </div>
</div>

@push('script_stack')
    <script>
        var columns = [
            {data: 'id', readOnly: true, className: "htRight"},
            {data: 'service', type: 'text', readOnly: true},
            {data: 'tax', type: 'numeric', format: '0.0%'},
            {data: 'amount', type: 'numeric', format: '0.000'},
            {data: 'price', type: 'numeric', format: '0.000', readOnly: true},
            {data: 'procurement_status', renderer: statusRenderer, readOnly: true},
            {data: 'delete_additional', renderer: AdditionalsPermitRendererExtras, readOnly: true},

        ];
        var colHeaders = ['ID', 'Service', 'Tax', 'Cost', 'Price', 'Invoiced', ''
        ];
        var colWidths = [
            2, 300, 100, 100, 100, 70, 70
        ];

        var hot_tables_permits_additionals = [];


        @foreach($providersAdditionals ?? [] as $provider)
        var hotElement = document.querySelector('#permission-additionals-{{$provider['id']}}');

        var hotSettings = {
            data: {!! json_encode($provider['additionals'], true) !!} ,
            columns: columns,
            rowHeaders: false,
            width: 900,
            autoWrapRow: true,
            height: 50 + 30 * {{count($provider['additionals'])}},
            colHeaders: colHeaders,
            colWidths: colWidths,
            afterChange: function (changes, source) {
                if (source == 'edit') {
                    var item = this.getData()[changes[0][0]];
                    updatePermitAdditional(item);
                }

            },
        };
        hot_tables_permits_additionals[{{$provider['id']}}] = new Handsontable(hotElement, hotSettings);

        @endforeach


            createPermitAdditional = function (item) {
            if (item[1] == 'Total') {
                return
            }
            $.ajax({
                type: "POST",
                url: '/api/additional/permit/' + item[0],
                data: {
                    'permit_id': item[0],
                    'tax': item[1],
                    'billing_unit_price': item[2],
                }
            }).success(function (data) {
                data = data.data;
                reloadPermitAdditionals(data.providers);
            });
        };

        updatePermitAdditional = function (item) {
            if (item[1] == 'Total') {
                return
            }
            $.ajax({
                type: "PUT",
                url: '/api/additional/permit/' + item[0],
                data: {
                    'permit_id': item[0],
                    'tax': item[2],
                    'billing_unit_price': item[3],
                    'billing_price': item[4],
                    'type': 'invoice',
                }
            }).success(function (data) {
                data = data.data;
                reloadPermitAdditionals(data.providers);
            }).error(function (data) {
                data = data.data;
                reloadServiceAdditionals(data.additionals);
            });
        };

        refreshPermitAdditional = function () {
            $.ajax({
                type: "GET",
                url: '/api/additional/' + {{$provider['id']}} +'/permit/'
            }).success(function (data) {
                data = data.data;
                reloadPermitAdditionals(data.providers);
            });
        };

        reloadPermitAdditionals = function (providers) {
            for (var i = 0; i < providers.length; i++) {
                var provider = providers[i];
                var items = provider['additionals'];
                var table = hot_tables_permits_additionals[provider['id']];
                table.loadData(items);
                table.updateSettings({
                    height: 35 + 24 * items.length,
                    maxRows: items.length,
                })

            }
        };


        function renderPermitsBillItem(instance, td, row, col, prop, value, cellProperties) {
            var item = instance.getData()[row];
            if (value != null) {
                td.innerHTML = value + ' - <input type="checkbox" class="bill_additionals" ' +
                    'name="bill_permits_administration_fees[]" value="' + item[0] + '">';
            } else {
                td.innerHTML = '';
            }
        }

        addPermitAdditionalInvoice = function (item) {

            var data = hot_tables_permits_additionals[{{$provider['id']}}].getData();
            var service = [];
            service.push(item[0]);
            $.ajax({
                type: "POST",
                url: '/api/procurement/' + service + '/permit/additional/add-invoice/',
                data: {
                    'provider_id': [{{$provider['id']}}],
                    'service': service,
                    'note': "Permit Additional"
                }
            }).success(function (data) {
                $(location).attr('href', '/admin/invoices/' + data.id + '/edit');
            });
        };

        function statusRenderer(instance, td, row, col, prop, value, cellProperties) {
            var instance2 = JSON.stringify(instance.getData()[row]);
            var instanceArray = instance.getData()[row];
            var url = "/admin/provider-statement/additional/" + instanceArray[0];
            td.innerHTML = "";

            if (instance.getData()[row][1] == 'Total') {
                return;
            }
            if (instance.getData()[row][1] != "Total" && instance.getData()[row][5] != 1) {
                newHtml = "<i class='fa fa-check-square' style='color:darkgray'></i>";
            }
            else if (instance.getData()[row][1] != "Total" && instance.getData()[row][5] == 1) {
                newHtml =
                    "<a href='" + url + "' type='button' class ='btn btn-sd btn-success' title='Show Invoice' " +
                    "style='padding: 3px 7px;font-size: 10px;margin: 2px;'><i class='fa fa-check' style='color:white; font-size: 11px'></i> SHOW</a>";
            }
            td.innerHTML = newHtml;
        }


        function AdditionalsPermitRendererExtras(instance, td, row, col, prop, value, cellProperties) {
            var instance2 = JSON.stringify(instance.getData()[row]);
            newHtml = "";

            if (instance.getData()[row][1] == 'Total') {
                return;
            }

            if (instance.getData()[row][1] != "Total" && instance.getData()[row][5] != 1) {
                newHtml += "<button href='' class ='btn btn-sd btn-info' title='Add Invoice' onclick='addPermitAdditionalInvoice(" + instance2 + ")' " +
                    "style='padding: 3px 7px;font-size: 10px;margin: 2px;'> <i class='fa fa-plus-square'></i></button>";

            }
            newHtml += "<button href='' onclick='deleteAdditionalPermit(" + instance2 + ")' class ='btn btn-sd btn-danger' " +
                "style='padding: 3px 7px;font-size: 10px;margin: 2px; float: right;'> <i class='fa fa-trash-o'></i></button>";
            td.innerHTML = newHtml;
        }

        function deleteAdditionalPermit(item) {
            if (confirm('Are you sure you want to delete this permit?')) {
                $.ajax({
                    type: "GET",
                    url: '/api/additional/permit/' + item[0] + '?operation_id=' +{{$operation->id}},
                }).success(function (data) {
                    if (data.data == false) {
                        return alert(data.message);
                    }
                    data = data.data;
                    reloadPermitAdditionals(data.providers);
                }).error(function(data) {
                    alert(data.responseJSON.message);
                });
            }
        }
    </script>

@endpush