@php
    $providers = $providerPermits;
@endphp

<div class="row">
    <div class="col-lg-12">
        <div class="row-fluid summary">
            <span class="h2 col-md-2">Permits</span>
            <div class="col-md-6">
                <div class="col-md-12">
                    <label for="procurement_permits_status">Permits Status</label>
                    <br>
                    <select class="form-control select2 " style="margin-left: 0px; width: 300px"
                            id="procurement_permits_status" onchange="updateProcurementPermitsStatus()">
                        <option value="">-</option>
                        <option value="Done"
                                @if($operation->getProcurementPermitsStatus()=='Done')
                                selected
                                @endif>Done</option>
                        <option value="Requested"
                                @if($operation->getProcurementPermitsStatus()=='Requested')
                                selected
                                @endif>Requested</option>
                        <option value="Not Required"
                                @if($operation->getProcurementPermitsStatus()=='Not Required')
                                selected
                                @endif>Not Required
                        </option>
                    </select>
                </div>
            </div>
            <div class="row-fluid summary" id="aircraft-permits">
                <div id="" class="collapse in row">
                    @foreach($providers ?? [] as $provider)
                        <h3 class="col-md-12">{{ $provider['name'] }}</h3>
                        <div class="container col-md-10" style="overflow: hidden">
                            <div style="overflow: auto; width: 100%"
                                 id='permissions-provider-{{ $provider['id'] }}'></div>
                        </div>
                        <div class="col-md-1">

                            <div class="col-md-2">
                                <button class="btn btn-info" onClick="addPermitsInvoice({{$provider['id'] }})">
                                    Add Invoice
                                </button>
                            </div>
                        </div>
                        @include('procurement.modals.addOverFlight', ['provider'=>$provider])

                        <hr class="col-md-12">
                    @endforeach
                </div>
            </div>
        </div>
        <hr>
    </div>


</div>

@push('script_stack')
    <script>
        var columns = [
            {data: 'id', type: 'numeric', readOnly: true},
            {data: 'code', type: 'numeric', readOnly: true, className: "htLeft"},
            {data: 'type', type: 'numeric', readOnly: true},
            {data: 'country', type: 'text', readOnly: true},
            {data: 'icao_from', type: 'text', readOnly: true},
            {data: 'icao_to', type: 'text', readOnly: true},
            {data: 'expiration_date', type: 'text', readOnly: true},
            {data: 'procurement_tax', type: 'numeric', format: '0.0%'},
            {data: 'procurement_cost', type: 'numeric', format: '0.00'},
            {data: 'final_cost', type: 'numeric', format: '0.00', readOnly: true},
            {data: 'procurement_status', type: 'text', renderer: permissionStatusRender, readOnly: true},
            {data: 'delete_permit', renderer: deletePermitRenderer, readOnly: true},

        ];
        var colHeaders = ['ID', 'Code', 'Type', 'Country', 'From', 'To', 'Exp. Date',
            'Tax %', 'Cost', 'Final Cost', 'Invoiced',  ' '];
        var hotProviderPermits = [];
        var colWidths = [
            2, 150, 50, 90, 55, 55, 80, 80, 50, 100, 80, 40
        ];

                @foreach($providers ?? [] as $provider)
        var hotElement = document.querySelector('#permissions-provider-{{$provider['id']}}');

        var hotSettings = {
            data: {!! json_encode($provider['permits'], true) !!} ,
            columns: columns,
            autoWrapRow: true,
            colWidths: colWidths,
            height: 30 + 35 * {{count($provider['permits'])}},
            colHeaders: colHeaders,
            afterChange: function (changes, source) {
                if (source == 'edit') {
                    var item = this.getData()[changes[0][0]];
                    updateRow(item, {{$provider['id']}});
                }

            },
        };
        hotProviderPermits[{{$provider['id']}}] = new Handsontable(hotElement, hotSettings);
        @endforeach


        reloadProviderPermits = function (providers) {
            if(providers.length == 0){
                clearProviderTable();
            }
            for (var i = 0; i < providers.length; i++) {
                var provider = providers[i];
                var items = provider['permits'];
//                console.log(items);
                var table = hotProviderPermits[provider['id']];
                table.loadData(items);
                table.updateSettings({
                    height: 50 + 24 * items.length,
                    maxRows: items.length,
                })
            }
        };

        clearProviderTable = function() {
            hotProviderPermits.forEach(function(element){
                element.loadData([]);
                element.updateSettings({
                    height: 50 + 24 * items.length,
                    maxRows: items.length,
                })
            });
        }

        updateRow = function (row, providerId) {
            $.ajax({
                type: "PUT",
                url: '/api/procurement/{{$operation->id}}/aircraft-permission/' + row[0] + '?billing-type=invoice',
                data: {
                    'id': row[0],
                    'tax': row[7],
                    'cost': row[8],
                    'procurement_status': row[10]
                },
            }).success(function (response) {
                response = response.data;
                reloadProviderPermits(response);
            }).error(function (data) {
                console.log(data);
                var response = data.responseJSON.data;
                alert(data.responseJSON.message);
                reloadProviderPermits(response);
            });
        };

        function uploadFile(data) {
            var providerId = data.getAttribute('data');
            $('#form_upload-' + providerId).submit();
        }


        function fileHtmlRender(instance, td, row, col, prop, value, cellProperties) {
            if (value) {
                newHtml = "<a href='" + value + "' target='_blank'>" +
                    "<i class='fa fa-cloud-download'/></a>";
                td.innerHTML = newHtml;

                return td;
            }
        }


        function permissionStatusRender(instance, td, row, col, prop, value, cellProperties) {
            var instance2 = JSON.stringify(instance.getData()[row]);
            var instanceArray = instance.getData()[row];
            var url = "/admin/provider-statement/permission/" + instanceArray[0];
            if (value != '1') {
                newHtml = "<span><i class='fa fa-check-square' style='color:darkgray'></i></span>";

            } else if (value == '1') {
                newHtml = "<a href='" + url + "' type='button' class ='btn btn-sd btn-success' title='Show Invoice' " +
                    "style='padding: 3px 7px;font-size: 10px;margin: 2px;'><i class='fa fa-check' style='color:white; font-size: 11px'></i> SHOW</a>";
            }
            td.innerHTML = newHtml;
        }


        addPermit = function (provider) {
            $.ajax({
                type: "POST",
                url: '/api/operation/{{$operation->id}}/OVF-permit/',
                data: {
                    'permit_id': $(provider + ' [name=permit_provider_id]').val(),
                    'tax': $(provider + ' [name=permit_tax]').val(),
                    'cost': $(provider + ' [name=cost]').val(),
                },
            }).success(function (data) {
                data = data.data;
                reloadProviderPermits(data.providers);
            });
        };

        function deletePermitRenderer(instance, td, row, col, prop, value, cellProperties) {
            if (instance.getData()[row][1] == 'Total') {
                return;
            }
            var instance2 = JSON.stringify(instance.getData()[row]);
            newHtml = "<button href='' onclick='deletePermit(" + instance2 + ")' class ='btn btn-sd btn-danger' " +
                "style='padding: 3px 7px;font-size: 10px;margin: 2px;'> <i class='fa fa-trash-o'></i></button>";
            td.innerHTML = newHtml;
        }

        function deletePermit(item) {
            if (confirm('Are you sure you want to delete this permit?')) {
                $.ajax({
                    type: "DELETE",
                    url: '/api/procurement/{{$operation->id}}/permit/' + item[0],
                }).success(function (data) {
                    if (data.data == false) {
                        return alert(data.message);
                    }
                    data = data.data;
                    reloadProviderPermits(data.providers);
                });
            }
        }

        addPermitsInvoice = function (providerId) {
            var data = hotProviderPermits[providerId].getData();
            var permits = [];
            for (var i = 0; i < data.length; i++) {
                if (data[i][0] != null) {
                    permits.push(data[i][0]);
                }
            }

            $.ajax({
                type: "POST",
                url: '/api/procurement/{{$operation->id}}/add-invoice',
                data: {
                    'provider_id': providerId,
                    'permits': permits,
                    'note': "Permit",
                },
            }).success(function (data) {
                if(data.accounting_services.length == 0){
                    alert('No permits')
                } else {
                    $(location).attr('href', '/admin/invoices/' + data.id + '/edit');
                }
            });
        };

        function updateProcurementPermitsStatus() {
            $.ajax({
                type: "POST",
                url: '/admin/operation/{{$operation->id}}/edit',
                data: {
                    'procurement_permits_status': $("#procurement_permits_status").find(":selected").text(),
                }
            }).success(function (data) {
            });
        }

    </script>



@endpush