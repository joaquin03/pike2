<div class="col-md-12">
    <h3 class="form-section">Crew members</h3>

    <div class="form-group col-md-4">
        <label class="control-label col-md-5">Captain</label>
        <div class="col-md-7">
            <select class="form-control" name="crew_captain_id" id="operation_captain_id" >
            <option value="" selected="selected" class="select2" >Select the Captain</option>
            @foreach(App\Models\CrewMember::all() as $crewMember)
                <option value="{{$crewMember->id}}"
                        {{(isset($operation) && $operation->crew_captain_id == $crewMember->id)? 'selected="selected"' : ''}}
                >
                    {{$crewMember->name}}
                </option>
                @endforeach
                </select>
        </div>
    </div>

    <div class="form-group col-md-4">
        <label class="control-label col-md-5">First Official</label>
        <div class="col-md-7">
            <select class="form-control" name="crew_first_official_id" id="operation_first_official_id" >
            <option value="" selected="selected" class="select2" >Select the First Official</option>
            @foreach(App\Models\CrewMember::all() as $crewMember)
                <option value="{{$crewMember->id}}"
                        {{(isset($operation) && $operation->crew_first_official_id == $crewMember->id)? 'selected="selected"' : ''}}
                >
                    {{$crewMember->name}}
                </option>
                @endforeach
                </select>
        </div>
    </div>

    <div class="form-group col-md-4">
        <label class="control-label col-md-5">Auxiliary</label>
        <div class="col-md-7">
            <select class="form-control" name="crew_auxiliary_id" id="auxiliary_id" >
            <option value="" selected="selected" class="select2" >Select the Auxiliary</option>
            @foreach(App\Models\CrewMember::all() as $crewMember)
                <option value="{{$crewMember->id}}"
                        {{(isset($operation) && $operation->crew_auxiliary_id == $crewMember->id)? 'selected="selected"' : ''}}
                >
                    {{$crewMember->name}}
                </option>
                @endforeach
                </select>
        </div>
    </div>

    <div id="second_crew" style="display: none">
        <div class="form-group col-md-4" >
            <label class="control-label col-md-5">Captain</label>
            <div class="col-md-7">
                <select class="form-control" name="second_captain_id" id="second_operation_captain_id" />
                <option value=""  selected="selected" class="select2" >Select the second Captain</option>
                @foreach(App\Models\CrewMember::all() as $crewMember)
                    <option value="{{$crewMember->id}}"
                            {{(isset($operation) && $operation->second_crew_captain_id == $crewMember->id)? 'selected="selected"' : ''}}
                    >
                        {{$crewMember->name}}
                    </option>
                    @endforeach
                    </select>
            </div>
        </div>

        <div class="form-group col-md-4">
            <label class="control-label col-md-5">First Official</label>
            <div class="col-md-7">
                <select class="form-control" name="second_first_official_id" id="second_operation_first_official_id" />
                <option value=""  selected="selected" class="select2" >Select the second First Official</option>
                @foreach(App\Models\CrewMember::all() as $crewMember)
                    <option value="{{$crewMember->id}}"
                            {{(isset($operation) && $operation->second_crew_first_official_id == $crewMember->id)? 'selected="selected"' : ''}}
                    >
                        {{$crewMember->name}}
                    </option>
                    @endforeach
                    </select>
            </div>
        </div>

        <div class="form-group col-md-4" >
            <label class="control-label col-md-5">Auxiliary</label>
            <div class="col-md-7">
                <select class="form-control" name="second_auxiliary_id" id="second_auxiliary_id" />
                <option value="" selected="selected" class="select2" >Select the second Auxiliary</option>
                @foreach(App\Models\CrewMember::all() as $crewMember)
                    <option value="{{$crewMember->id}}"
                            {{(isset($operation) && $operation->second_crew_auxiliary_id == $crewMember->id)? 'selected="selected"' : ''}}
                    >
                        {{$crewMember->name}}
                    </option>
                    @endforeach
                    </select>
            </div>
        </div>
    </div>

    <div class="form-group" id="add_crew">
        <label class="control-label col-s-3"></label>
        <div class="col-md-9">
            <a href="#" type="button" onclick="addCrew()">Add other crew (optional)</a>
        </div>
    </div>

    <div class="form-group" id="hide_crew" style="display: none; margin-left:2%">
        <label class="control-label col-s-3"></label>
        <div class="col-md-9">
            <a href="#" type="button" onclick="hideCrew()">Hide</a>
        </div>
    </div>

</div>