@php
    $providersAdditionals = $permanentPermissionsAdditionals;
@endphp

<div class="col-lg-12">
    <div class="row-fluid summary" >
        <h3>Permanent permissions Additionals</h3>
        <div id='permanent-permission-additionals-{{$provider['id']}}'></div>
    </div>
</div>

@push('script_stack')
    <script>
        var columns = [
            {data: 'id', readOnly: true, className: "htRight"},
            {data: 'service', type: 'text', readOnly: true},
            {data: 'tax', type: 'numeric', format: '0.0%'},
            {data: 'amount', type: 'numeric', format: '0.000'},
            {data: 'price', type: 'numeric', format: '0.000', readOnly: true},
            {data: 'procurement_status', renderer: statusRenderer, readOnly: true},
            {data: 'delete_additional', renderer: AdditionalsPermanentPermitRendererExtras, readOnly: true},

        ];
        var colHeaders = ['ID', 'Service', 'Tax', 'Cost', 'Price', 'Status', ' ', ''
        ];
        var colWidths = [
            2, 300, 100, 100, 100, 80, 70
        ];

        var hot_tables_permanent_permits_additionals = [];

        @foreach($providersAdditionals ?? [] as $provider)
        var hotElement = document.querySelector('#permanent-permission-additionals-{{$provider['id']}}');

        var hotSettings = {
            data: {!! json_encode($provider['additionals'], true) !!} ,
            columns: columns,
            rowHeaders: false,
            width: 1500,
            autoWrapRow: true,
            height: 50 + 20 * {{count($provider['additionals'])}},
            colHeaders: colHeaders,
            colWidths: colWidths,
            afterChange: function (changes, source) {
                if (source == 'edit') {
                    var item = this.getData()[changes[0][0]];
                    updatePermanentPermitAdditional(item, this.provider);
                }
            },
        };
        hot_tables_permanent_permits_additionals[{{$provider['id']}}] = new Handsontable(hotElement, hotSettings);

        @endforeach


        createPermanentPermitAdditional = function (item, operation_id) {
            if (item[1] == 'TOTAL') {
                return
            }
            $.ajax({
                type: "POST",
                url: '/api/additional/permanent-permit/' + item[0] + '/operation/' + operation_id,
                data: {
                    'permit_id': item[0],
                    'tax': item[2],
                    'billing_unit_price': item[3],
                    'billing_price': item[4],
                }
            }).success(function (data) {
                data = data.data;
                reloadPermanentPermitAdditionals(data.providers);
            });
        };

        updatePermanentPermitAdditional = function (item, provider) {
            if (item[1] == 'TOTAL') {
                return
            }
            $.ajax({
                type: "PUT",
                url: '/api/additional/permanent-permit/' + item[0] + '/operation/' + {{$operation->id}},
                data: {
                    'permit_id': item[0],
                    'tax': item[2],
                    'billing_unit_price': item[3],
                    'billing_price': item[4],
                }
            }).success(function (data) {
                data = data.data;
                reloadPermanentPermitAdditionals(data.providers);
            });
        };

        refreshPermanentPermitAdditional = function (provider) {
            $.ajax({
                type: "GET",
                url: '/api/additional/permanent-permit/' + provider
            }).success(function (data) {
                data = data.data;
                reloadPermanentPermitAdditionals(data.providers);
            });
        };

        reloadPermanentPermitAdditionals = function (providers) {

            for (var i = 0; i < providers.length; i++) {
                var provider = providers[i];
                var items = provider['additionals'];
                console.log(provider);
                var table = hot_tables_permanent_permits_additionals[provider['id']];
                table.loadData(items);
                table.updateSettings({
                    height: 35 + 24 * items.length,
                    maxRows: items.length,
                })

            }
        };


        function renderPermitsBillItem(instance, td, row, col, prop, value, cellProperties) {
            var item = instance.getData()[row];
            if (value != null) {
                td.innerHTML = value + ' - <input type="checkbox" class="bill_additionals" ' +
                    'name="bill_permits_administration_fees[]" value="' + item[0] + '">';
            } else {
                td.innerHTML = '';
            }
        }

        addPermanentPermitAdditionalInvoice = function (item) {

            var service = [];
            service.push(item[0]);
            $.ajax({
                type: "POST",
                url: '/api/procurement/' + service + '/permit/additional/add-invoice/',
                data: {
                    'provider_id': [{{$provider['id']}}],
                    'service': service,
                    'note': "Permanent Permit Additional"
                }
            }).success(function (data) {
                $(location).attr('href', '/admin/bills-provider/' + data.id + '/edit');
            });
        };

        function addPermanentPermitAdditionalInvoiceRenderer(instance, td, row, col, prop, value, cellProperties) {
            var instance2 = JSON.stringify(instance.getData()[row]);
            td.innerHTML = '';
            if(instance.getData()[row][1]!="Total" && instance.getData()[row][5]!=1) {
                newHtml = "<button href='' class ='btn btn-sd btn-info' title='Add Invoice' onclick='addPermanentPermitAdditionalInvoice(" + instance2 + ")' " +
                    "style='padding: 3px 7px;font-size: 10px;margin: 2px;'> <i class='fa fa-info-circle'></i></button>";
                td.innerHTML = newHtml;
            }
        }
        function statusRenderer(instance, td, row, col, prop, value, cellProperties) {
            var instance2 = JSON.stringify(instance.getData()[row]);
            var instanceArray = instance.getData()[row];
            var url = "/admin/provider-statement/additional/" + instanceArray[0];
            td.innerHTML = "";

            if (instance.getData()[row][1] == 'Total') {
                return;
            }
            if (instance.getData()[row][1] != "Total" && instance.getData()[row][5] != 1) {
                newHtml = "<i class='fa fa-check-square' style='color:darkgray'></i>";
            }
            else if (instance.getData()[row][1] != "Total" && instance.getData()[row][5] == 1) {
                newHtml =
                    "<a href='" + url + "' type='button' class ='btn btn-sd btn-success' title='Show Invoice' " +
                    "style='padding: 3px 7px;font-size: 10px;margin: 2px;'><i class='fa fa-check' style='color:white; font-size: 11px'></i> SHOW</a>";
            }
            td.innerHTML = newHtml;
        }

        function AdditionalsPermanentPermitRendererExtras(instance, td, row, col, prop, value, cellProperties) {
            var instance2 = JSON.stringify(instance.getData()[row]);
            newHtml = "";
            if (instance.getData()[row][1] == 'Total') {
                return;
            }

            if (instance.getData()[row][1] != "Total" && instance.getData()[row][5] != 1) {
                newHtml += "<button href='' class ='btn btn-sd btn-info' title='Add Invoice' onclick='addPermanentPermitAdditionalInvoice(" + instance2 + ")' " +
                    "style='padding: 3px 7px;font-size: 10px;margin: 2px;'> <i class='fa fa-plus-square'></i></button>";

            }
            newHtml += "<button href='' onclick='deleteAdditionalPermanentPermit(" + instance2 + ")' class ='btn btn-sd btn-danger' " +
                "style='padding: 3px 7px;font-size: 10px;margin: 2px; float: right;'> <i class='fa fa-trash-o'></i></button>";
            td.innerHTML = newHtml;


        }

        function deleteAdditionalPermanentPermit(item) {
            if (confirm('Are you sure you want to delete this permit?')) {
                $.ajax({
                    type: "DELETE",
                    url: '/api/additional/permanent-permit/' + item[0] + '?operation_id=' + {{$operation->id}},
                }).success(function (data) {
                    if (data.data == false) {
                        return alert(data.message);
                    }
                    data = data.data;
                    reloadPermitAdditionals(data.providers);
                });
            }
        }

    </script>

@endpush