@php
    $providers = $providerPermanentPermits;
@endphp

<div class="row">
    <div class="col-lg-12">
        <div class="row-fluid summary">
            <span class="h3">Permanent Permits</span>

            <div class="row-fluid summary" id="permanent-permits">
                <div class="collapse in row">
                    @foreach($providers ?? [] as $provider)
                        <h3 class="col-md-12">{{ $provider['name'] }}</h3>
                        <div class="container col-md-10 " style="overflow: hidden">

                            <div id='permanent-permissions-provider-{{ $provider['id'] }}'
                                 data-providerId="{{$provider['id']}}"></div>

                        </div>
                        <div class="col-md-2">
                                <button class="btn btn-info" onClick="addPermanentPermitsInvoice({{$provider['id'] }})">
                                Add Invoice
                            </button>
                        </div>
                        @include('procurement.modals.addPermanentPermitOverFlight', ['provider'=>$provider])
                        @include('procurement.partials.permanentPermissions.permanentPermissionAdditionals', ['provider'=>$provider])
                        @include('procurement.modals.addPermanentPermitAdditional', ['provider'=>$provider])
                        <hr class="col-md-12">
                    @endforeach
                </div>
            </div>

        </div>
    </div>

</div>
@push('script_stack')
    <script>

        var columnsPermanentP = [
            {data: 'id', type: 'numeric', readOnly: true},
            {data: 'code', type: 'numeric', readOnly: true, className: "htLeft"},
            {data: 'type', type: 'numeric', readOnly: true},
            {data: 'country', type: 'text', readOnly: true},
            {data: 'icao_from', type: 'text', readOnly: true},
            {data: 'icao_to', type: 'text', readOnly: true},
            {data: 'expiration_date', type: 'text', readOnly: true},
            {data: 'operation_status', type: 'text'},
            {data: 'procurement_tax', type: 'numeric', format: '0.0%'},
            {data: 'procurement_cost', type: 'numeric'},
            {data: 'final_price', type: 'numeric', format: '0.00', readOnly: true},
            {data: 'procurement_status', type: 'text', renderer: permanentPermissionStatusRender},
            {data: 'delete_permit', renderer: deletePermanentPermitRenderer,  readOnly: true}
        ];
        var colHeadersPermanentP = ['ID', 'Code', 'Type', 'Country', 'From', 'To', 'Expiration Date', 'Ops Status',
            'Tax %', 'Cost', 'Final Price', 'Invoiced', ''];
        var permanentPermission = [];
        @foreach($providers ?? [] as $provider)
        var hotElement2 = document.querySelector('#permanent-permissions-provider-{{$provider['id']}}');
        var colWidths = [
            2, 110, 100, 90, 55, 55, 80, 80, 60, 50, 100, 80, 40
        ];
        var hotSettings2 = {
            data: {!! json_encode($provider['permits']) !!},
            columns: columnsPermanentP,
            colWidths: colWidths,
            width: 1280,
            height: 30 + 30 * {{count($provider['permits'])}},
            colHeaders: colHeadersPermanentP,
            afterChange: function (changes, source) {
                if (source == 'edit') {
                    var item = this.getData()[changes[0][0]];
                    updatePermanentPermissionRow(item, {{ $provider['id'] }});
                }

            },
        };
        permanentPermission[{{$provider['id']}}] = new Handsontable(hotElement2, hotSettings2);
        @endforeach


            updatePermanentPermissionRow = function (row, providerId) {
            $.ajax({
                type: "PUT",
                url: '/api/procurement/{{$operation->id}}/permanent-permission/' + row[0] + '/',
                data: {
                    'id': row[0],
                    'operation_status': row[7],
                    'cost': row[9],
                    'tax': row[8],
                    'procurement_status': row[10],
                },
            }).success(function (data) {
                reloadProviderPermanentPermits(data.data);
            });
        }

        reloadProviderPermanentPermits = function (providers) {
            if(providers.length == 0){
                clearPermitsTable();
            }
            for (var i = 0; i < providers.length; i++) {
                var provider = providers[i];
                var items = provider['permits'];
                var table = permanentPermission[provider['id']];
                table.loadData(items);
                table.updateSettings({
                    height: 50 + 24 * items.length,
                    maxRows: items.length,
                })
            }
        };

        clearPermitsTable = function() {
            permanentPermission.forEach(function(element){
                element.loadData([]);
                element.updateSettings({
                    height: 50 + 24 * items.length,
                    maxRows: items.length,
                })
            });

        };

        function permanentPermissionStatusRender(instance, td, row, col, prop, value, cellProperties) {
            var instance2 = JSON.stringify(instance.getData()[row]);
            var instanceArray = instance.getData()[row];
            var provider = instance.rootElement.id.split("-")[3];
            var url = "/admin/provider-statement/permanent-permission/" + instanceArray[0];

            newHtml = "<span onclick='changePermanentPermissionStatus(" + instance2 + ", " + provider + ")'>";
            if (value != '1') {
                newHtml = "<span><i class='fa fa-check-square' style='color:darkgray'></i></span>";

            } else if (value == '1') {
                newHtml = "<a href='" + url + "' type='button' class ='btn btn-sd btn-success' title='Show Invoice' " +
                    "style='padding: 3px 7px;font-size: 10px;margin: 2px;'><i class='fa fa-check' style='color:white; font-size: 11px'></i> SHOW</a>";
            }
            td.innerHTML = newHtml;
        }

        addPermanentPermission = function (provider) {
            $.ajax({
                type: "POST",
                url: '/api/operation/{{$operation->id}}/OVF-permanent-permit/',
                data: {
                    'permit_id': $(provider + ' [name=permanent_permission_provider_id]').val(),
                    'tax': $(provider + ' [name=permanent_permission_tax]').val(),
                    'cost': $(provider + ' [name=permanent_permission_cost]').val(),
                },
            }).success(function (data) {
                data = data.data;
                reloadProviderPermanentPermits(data.providers);
            });
        };

        function deletePermanentPermitRenderer(instance, td, row, col, prop, value, cellProperties) {
            var instance2 = JSON.stringify(instance.getData()[row]);
            newHtml = "<button href='' onclick='deletePermanentPermit("+instance2+")' class ='btn btn-sd btn-danger' " +
                "style='padding: 3px 7px;font-size: 10px;margin: 2px;'> <i class='fa fa-trash-o'></i></button>";
            td.innerHTML = newHtml;
        }

        function deletePermanentPermit(item) {
            if(confirm('Are you sure you want to delete this permanent permit?')){
                $.ajax({
                    type: "DELETE",
                    url: '/api/procurement/{{$operation->id}}/permanentPermit/' + item[0],
                }).success(function (data) {
                    if(data.data==false){
                        return alert(data.message);
                    }
                    data = data.data;
                    reloadProviderPermanentPermits(data.providers);
                });
            }
        }

        addPermanentPermitsInvoice = function (providerId) {
            var data = permanentPermission[providerId].getData();
            var permits = [];
            for (var i = 0; i < data.length; i++) {
                if (data[i][0] != null) {
                    permits.push(data[i][0]);
                }
            }

            $.ajax({
                type: "POST",
                url: '/api/procurement/{{$operation->id}}/permanent-permit/add-invoice',
                data: {
                    'provider_id': providerId,
                    'permanentPermits': permits,
                    'note': "Permanent Permit",
                },
            }).success(function (data) {
                $(location).attr('href', '/admin/invoices/' + data.id + '/edit');
            });
        };


    </script>



@endpush