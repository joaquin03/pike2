<form action="{{ route('crud.permission.update', ['operation_id' => $operation->id]) }}" method="POST">
    {{ csrf_field() }}
    <table class="table" id="table">
        <tr>
            <th>Provider</th>
            <th>Code</th>
            <th>Type</th>
            <th>From</th>
            <th>To</th>
            <th>File</th>
            <th>Country</th>
            <th>Status</th>
            <th>Actions</th>
        </tr>



        @foreach($operation->aircraftPermission as $permission)
            <tr>
                <td>{{ $permission->provider_id }}</td>
                <td><input type="text" class="form-control" name="permission[{{$permission->id}}][code]" required value="{{ $permission->code }}" size="4"></td>
                <td>{{$permission->getType()}}</td>
                <td>{{$permission->icao_from}}</td>
                <td>{{$permission->icao_to}}</td>
                <td>
                    @if(! is_null($permission->file))
                        <a href="{{ $permission->getUrlFile() }}" target="_blank">Download</a>
                    @else
                        -
                    @endif
                </td>
                <td>{{ $permission->country }}</td>
                <td>
                    <select name="permission[{{$permission->id}}][status]">
                        <option value="Active" @if($permission->status == 'Active') selected @endif >Active</option>
                        <option value="Canceled" @if($permission->status == 'Canceled') selected @endif >Canceled
                        </option>
                    </select>
                </td>
                <td>
                    <a class="btn btn-sd btn-danger ajax-modal"
                        href="{{route('crud.permission.delete', ['operation_id'=>$operation->id, 'permission_id'=>$permission->id]) }}">
                        <i class="fa fa-trash-o"></i></a></td>
            </tr>
        @endforeach


    </table>

    <button type="submit" class="btn bg-maroon pull-right">Save Permissions Changes</button>


</form>