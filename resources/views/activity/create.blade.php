@extends('backpack::layout')


@section('header')
    <section class="content-header">
        <h1>
            {{ trans('backpack::crud.add') }} <span>{{ $crud->entity_name }}</span>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url(config('backpack.base.route_prefix'),'dashboard') }}">{{ trans('backpack::crud.admin') }}</a>
            </li>
            <li><a href="/admin/company" class="text-capitalize">Companies</a></li>
        </ol>
    </section>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <!-- Default box -->
            @if ($crud->hasAccess('list'))
                <a href={{"/admin/company/".$crud->companyId}}><i class="fa fa-angle-double-left"></i>Back to company
                </a><br><br>
            @endif

            @include('crud::inc.grouped_errors')

            {!! Form::open(array('url' => $crud->route, 'method' => 'post', 'files'=>$crud->hasUploadFields('create'))) !!}
            <div class="box">

                <div class="box-header with-border">
                    <h3 class="box-title">{{ trans('backpack::crud.add_a_new') }} {{ $crud->entity_name }}</h3>
                </div>
                <div class="box-body row">
                    <!-- load the view from the application if it exists, otherwise load the one in the package -->
                    @if(view()->exists('vendor.backpack.crud.form_content'))
                        @include('vendor.backpack.crud.form_content', [ 'fields' => $crud->getFields('create'), 'action' => 'create' ])
                    @else
                        @include('crud::form_content', [ 'fields' => $crud->getFields('create'), 'action' => 'create' ])
                    @endif
                </div><!-- /.box-body -->

                <div class="box-footer">
                    {{--@include('crud::inc.form_save_buttons')--}}
                    <div id="saveActions" class="form-group">
                        <button type="submit" class="btn btn-success">
                            <span class="fa fa-save" role="presentation" aria-hidden="true"></span>
                            Save
                        </button>

                        <a href="{{url('/admin/company')}}" class="btn btn-default"><span></span> &nbsp;Back to all
                            companies</a>
                    </div>

                </div><!-- /.box-footer-->

            </div><!-- /.box -->
            {!! Form::close() !!}
        </div>
    </div>

@endsection