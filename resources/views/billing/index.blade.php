@extends('backpack::layout')

@section('after_styles')

    @stack('styles_stack')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>

@endsection

@section('title')
    Operations
@endsection


@section('header')
    <section class="content-header">
        <h1>Billing List</h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/admin/dashboard')}}">Admin</a></li>
            <li class="active">Procurement</li>
        </ol>

    </section>
@endsection()

@section('content')
    @include('crud::inc.grouped_errors')
    <div class="row">

        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                </div>
                <div class="box-body table-responsive">

                    <div class="form-inline">
                        <div class="form-group">
                            <label for="serch-q">Text:</label>
                            <input class="form-control" type="text" name="q" id="serch-q">
                        </div>
                        <div class="form-group">
                            <label for="procurement_status">Procurement Status:</label>
                            <select name="procurement_status[]" class="searchByStatus form-control select2"
                                    multiple="multiple"
                                    id="procurement_status" style="text-align:right; width:200px">
                                <option value="Canceled">Canceled</option>
                                <option value="Completed">Completed</option>
                                <option value="In Process">In Process</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="billing_status">Billing Status:</label>
                            <select name="billing_status[]" class="searchByStatus form-control select2"
                                    multiple="multiple"
                                    id="billing_status" style="text-align:right; width:200px">
                                <option value="In Process">In Process</option>
                                <option value="Pending">Pending</option>
                                <option value="Completed">Completed</option>
                                <option value="Canceled">Canceled</option>
                            </select>
                        </div>
                    </div>

                    <div class="box-body table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th class="sortable" data-order="code" data-type="ASC">
                                    Code <i class="fa fa-sort-amount-asc pull-right" aria-hidden="true"></i>
                                </th>
                                <th class="sortable" data-order="start_date" data-type="ASC">
                                    Date <i class="fa fa-sort-amount-asc pull-right" aria-hidden="true"></i>
                                </th>
                                <th class="sortable" data-order="users.name" data-type="ASC">
                                    In Charge <i class="fa fa-sort-amount-asc pull-right" aria-hidden="true"></i>
                                </th>
                                <th class="sortable" data-order="operatorCompanies.name" data-type="ASC">
                                    A. Operator <i class="fa fa-sort-amount-asc pull-right" aria-hidden="true"></i>
                                </th>
                                <th class="sortable" data-order="clientCompanies.name" data-type="ASC">
                                    A. Client <i class="fa fa-sort-amount-asc pull-right" aria-hidden="true"></i>
                                </th>
                                <th class="sortable" data-order="aircraft.registration" data-type="ASC">
                                    Aircraft <i class="fa fa-sort-amount-asc pull-right" aria-hidden="true"></i>
                                </th>
                                <th width="10">Type</th>
                                <th width="10">Route</th>
                                <th width="5">Fuel Status</th>
                                <th width="5">Services Status</th>
                                <th>Perm. Status</th>
                                <th class="sortable" data-order="procurement_status" data-type="ASC">
                                    Proc. Status <i class="fa fa-sort-amount-asc pull-right" aria-hidden="true"></i>
                                </th>
                                <th class="sortable" data-order="billing_status" data-type="ASC">
                                    Billing Status <i class="fa fa-sort-amount-asc pull-right" aria-hidden="true"></i>
                                </th>

                            </tr>
                            </thead>
                            <tbody id="billing-table">
                            </tbody>
                            @include('vendor.pagination.JsonPagination', ['items'=>$items, 'type' => 'billing'])
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop


@section('after_scripts')
    @stack('script_stack')

    <script src="{{ asset('vendor/adminlte/plugins/select2/select2.min.js') }}"></script>

    <script>
        var order = "";
        var orderType = "";
        $('.select2').select2();
        var billings = {!! json_encode($items['data']) !!};


        renderBillings = function (operationList) {
            var html = '';
            for (var i = 0; i < operationList.length; i++) {
                var item = operationList[i];
                html += '<tr>';
                html += '<td> <a href="/admin/billing/' + item.id + '/edit">' + item.code + '</a> </td>';
                html += '<td> ' + item.date + ' </td>';
                html += '<td> ' + item.in_charge_user + ' </td>';
                html += '<td> ' + item.aircraft_operator + ' </td>';
                html += '<td> ' + item.aircraft_client + ' </td>';
                html += '<td> ' + item.aircraft + ' </td>';
                html += '<td colspan="4" width="30" class="sub-table-container">';
                html += '<table class="table sub-table">';
                var itineraries = item.itineraries.data;
                for (var j = 0; j < itineraries.length; j++) {
                    html += renderHtmlItinerary(itineraries[j]);
                }
                html += '</table>';
                html += '</td>';
                html += '<th style="font-weight: normal">';
                var permit_status = item.procurement_permits_status;
                console.log(item);
                if (permit_status === null) {
                    html += '';
                }
                else {
                    if (permit_status == 'Done') {
                        html += '<small class="label bg-green" style=" color:white">' +
                            '<i class="fa fa-file-text text-success" style="color:white; font-size: 11px"></i></small>';
                    } else {
                        html += '<i class="fa fa-file-text" aria-hidden="true"></i>';
                    }
                }
                html += '<td>' + item.procurement_status + '</td>';
                html += '<td>' + item.billing_status + '</td>';
                html += '</tr>';

            }
            $("#billing-table").html(html);
        };

        renderHtmlItinerary = function (itinerary) {
            var html = '';
            html += '<tr>';
            html += '<td>' + itinerary.type + '</td>';
            html += '<td>' + itinerary.airport_icao_string + '</td>';
            html += '<td style="min-width: 50px;">';
            if (itinerary.procurement_fuel_status === null) {
                html += '';
            } else {
                if (itinerary.procurement_fuel_status == 'Done') {
                    html += '<small class="label bg-green" style=" color:white" title="Done">' +
                        '<i class="fa fa-tint" style="color:white; font-size: 11px"> </i>';
                } else if (itinerary.procurement_fuel_status == 'Requested') {
                    html += '<i class="fa fa-tint" aria-hidden="true" title="Requested"></i>';
                }
            }
            html += '</td>';
            html += '<td style="min-width: 50px;">';
            if (itinerary.procurement_service_status === null) {
                html += '';
            } else {
                if (itinerary.procurement_service_status == 'Done') {
                    html += '<small class="label bg-green" style=" color:white" title="Done">' +
                        '<i class="fa fa-truck" style="color:white; font-size: 11px"> </i>';
                } else if (itinerary.procurement_service_status == 'Requested') {
                    html += '<i class="fa fa-truck" aria-hidden="true" title="Requested"></i>';
                }
            }
            html += '</td>';
            html += '</tr>';
            return html;
        }

        renderBillings(billings);

        $("#serch-q").on('change keyup paste', function () {
            pageTo = 1;
            filterBillings();
        });

        $(".searchByStatus").on('select2:select', function () {
            pageTo = 1;
            filterBillings();
        });
        $(".searchByStatus").on('select2:unselect', function () {
            pageTo = 1;
            filterBillings();
        });

        $(".sortable").on('click', function () {
            $(".sortable").css({"color": "black"});
            $(this).css({"color": "gray"});
            var icon = $(this).children();
            if (icon.hasClass("fa-sort-amount-asc")) {
                $(this).children().removeClass("fa-sort-amount-asc");
                $(this).children().addClass("fa-sort-amount-desc");
                $(this).data("order_type", 'DESC')

            } else {
                $(this).children().removeClass("fa-sort-amount-desc");
                $(this).children().addClass("fa-sort-amount-asc");
                $(this).data("order_type", 'ASC')

            }
            order = $(this).data("order");
            orderType = $(this).data("order_type");

            filterBillings();
        });


        function filterBillings() {

            $.ajax({
                type: "GET",
                url: '/admin/billing/search',
                data: {
                    q: $("#serch-q").val(),
                    procurement_status: $("#procurement_status").val(),
                    billing_status: $("#billing_status").val(),
                    order: order,
                    order_type: orderType,
                    page: pageTo
                },
                success: (function (data) {
                    totalPages = data.items.meta.pagination.total_pages;
                    renderBillings(data.items.data);
                    renderButtons(totalPages);
                })
            });
        }

    </script>
@endsection

