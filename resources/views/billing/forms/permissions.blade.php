
<form action="{{ route('crud.permission.store', ['operation_id'=>$operation->id]) }}" method="POST" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="form-group col-md-6">
        <label class="">Code *</label>
        <input type="text" class="form-control" name="code" required>
    </div>
    <div class="form-group col-md-6">
        <label class="">Type *</label>
        <select name="type" class="form-control select2" id="">
            @foreach(\App\Models\AircraftPermission::$types as $code => $type)
                <option value="{{ $code }}">{{ $code }}</option>
            @endforeach
            </select>
        {{--<input type="text" class="form-control" name="name">--}}
    </div>
    <div class="form-group col-md-12">
        <label class="">Country *</label>
        <select name="country" class="form-control select2" id="">
            @foreach(\App\Models\Utilities\Country::all() as $code => $country)
                <option value="{{$country}}">{{ $country }}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group col-md-12">
        <label class="">Provider *</label>
        <select name="provider_id" class="form-control select2" id="provider_id">
            <option value="" disabled> - </option>
            @foreach($providers as $provider)
                <option value="{{ $provider->id }}">{{ $provider->name }}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group col-md-6">
        <label class="">To *</label>
        <select name="icao_to" class="form-control" id="to">
            <option value="" disabled> -</option>
            @foreach($airports as $airport)
                <option value={{$airport->icao}}>{{ $airport->icao }}</option>
            @endforeach
        </select>

    </div>
    <div class="form-group col-md-6">
        <label class="">From *</label>
        <select name="icao_from" class="form-control" id="from">
            <option value="" disabled> -</option>
            @foreach($airports as $airport)
                <option value={{$airport->icao}}>{{ $airport->icao }}</option>
            @endforeach
        </select>

    </div>
    <div class="form-group col-md-6">
        <label class="">File *</label>
        <input type="file" class="" name="file">
    </div>
    <div class="form-group col-md-12">
        <label for=""></label>
        <button type="submit" class="btn bg-maroon" style="margin-top: 25px;">Save</button>
    </div>



</form>

