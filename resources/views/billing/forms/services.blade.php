<form action="{{ route('crud.service.itinerary.store', ['itinerary_id'=>$itinerary->id]) }}" method="POST" class="service-form">
    {{ csrf_field() }}
    <div class="form-group">
        <label class="">Provider *</label>
        <select name="provider_id" class="form-control" id="provider_id">
            <option value="" disabled> - </option>
            @foreach($providers as $provider)
                <option value="{{ $provider->id }}">{{ $provider->name }}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label>Service *</label>
        <select class="form-control" name="service_provider_id" required id="service_provider_id">
            <option value="" disabled> - </option>
        </select>
    </div>
    <div class="form-group">
        <label for=""></label>
        <button type="submit" class="btn btn-success" style="margin-top: 25px;">Add</button>
    </div>


</form>

<script src="{{ asset('vendor/adminlte/plugins/select2/select2.min.js') }}"></script>
<script>
    $(function () {
        function loadServices() {
            var provider_id = $("#provider_id").val();
            $.get('{{url('admin/provider/')}}'+'/'+provider_id+'/services', function(data){
                $("#service_provider_id").html(formatServices(data));
                $('.select2').select2();
            });
        }

        function formatServices(data) {
            var html = '<option value="" disabled> - </option>';

            for(var i=0; data.length > i; i++) {
                html += '<option value="'+data[i].id+'">'+data[i].service_name+'</option>';
            }
            return html;
        }
        loadServices();

        $('#provider_id').on('change', function() {
            loadServices()
        });

        $('.select2').select2();
    });



</script>
