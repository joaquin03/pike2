{{ csrf_field() }}

@section('after_styles')

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

    <!-- Datepicker -->
    <link href='bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css' rel='stylesheet' type='text/css'>
    <script src='bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js' type='text/javascript'></script>
    <link rel="stylesheet" href="public/css/styles.css">

@endsection

<div class="tab-content" style="">
    <div class="tab-pane edit active" id="tab_general">
        @include('billing.fields.general')
    </div>
    <div class="tab-pane edit" id="tab_documents">
        @include('procurement.fields.documents')
    </div>
</div>
