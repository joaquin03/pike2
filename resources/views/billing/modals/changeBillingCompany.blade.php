

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Change Billing Company</h4>
</div>


<form action="{{route('crud.billing.changeBillingCompany', ['id'=>$operation->id])}}" method="POST">

    <div class="box">
        {{ csrf_field() }}
        <div class="row" style="margin-top: 20px">
           <div class="form-group col-md-6 col-md-offset-3">
                <select class="form-control select2 col-md-12"  name="billing_company_id" style="width: 100%">
                    <option value=""> Company </option>
                    @foreach($companies as $company)
                        <option value="{{$company->id}}"
                        @if($operation->billing_company_id == $company->id) selected @endif >{{$company->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="box-footer">
            <button type="submit" id="submit" class="btn btn-success" style=" margin-left: 90%">Change</button>
        </div>
    </div>

</form>

<script src="{{ asset('vendor/adminlte/plugins/select2/select2.min.js') }}"></script>
<script>
    $('.select2').select2();
</script>

