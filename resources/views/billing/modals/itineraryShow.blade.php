@php
    $itineraryServices = $itineraryServices['data'];
@endphp
<div>

    <div class="table_row_slider">
        <div class="row">
            <div class="col-md-12">
                <div class="row-fluid summary" id="itineraries-billing" style="margin-bottom: 10px">
                    <h3>Services</h3>
                    <div id='itinerary_billing_{{$itinerary->id}}'></div>
                </div>
                @include('billing.modals.addService', ['itinerary_id'=>$itinerary->id, 'provider'=>$provider])
            </div>

            <br>
        </div>

        <div class="col-md-6">
            <div class="row">
                <div class="col-md-6">
                    <label for="adm_fee">Adm. Fee %</label>
                    <br>
                    <input id="{{$itinerary->id}}_adm_fee" type="number" name="adm_fee"
                           value={{$itinerary->billingCompany->aircraftClient->adm_fee ?? 12}}
                                   class="form-control">

                    <button id="create-bill" class="btn btn-success" style="margin-right: 30px;">Create Bill</button>
                    <hr>
                </div>

            </div>

            <div class="row">
                <div class="col-md-6">
                    <label for="billing_service_status">Service Status</label>
                    <br>
                    <select class="form-control" style="width:100%" id="billing_service_status"
                            onchange="updateServicesStatus()">
                        <option value="">-</option>
                        <option value="Done"
                                @if($itinerary->billing_service_status =='Done') selected @endif>Done
                        </option>
                        <option value="Requested"
                                @if($itinerary->billing_service_status =='Requested') selected @endif>Requested
                        </option>
                    </select>
                </div>
                <div class="col-md-6">
                    <label for="billing_fuel_status">Fuel Status</label>
                    <br>
                    <select class="form-control" style="width:100%" id="billing_fuel_status"
                            onchange="updateServicesStatus()">
                        <option value="">-</option>
                        <option value="Done" @if($itinerary->getBillingFuelStatus()=='Done') selected @endif>
                            Done
                        </option>
                        <option value="Requested" @if($itinerary->getBillingFuelStatus()=='Requested') selected @endif>
                            Requested
                        </option>
                    </select>
                </div>
            </div>

        </div>
        <div class="col-md-6">
            @if($itinerary->notes)
                <div class="col-md-12">
                    <label class="control-label col-md-4">General Notes:</label>
                    <span class="col-md-8">{{$itinerary->notes}}</span>
                </div>
            @endif
            @if($itinerary->quotation_notes)
                <div class="col-md-12">
                    <label class="control-label col-md-4">Quotation Notes:</label>
                    <span class="col-md-8">{{$itinerary->quotation_notes}}</span>
                </div>
            @endif
            @if($itinerary->operation_notes)
                <div class="col-md-12">
                    <label class="control-label col-md-4">Op. Notes:</label>
                    <span class="col-md-8">{{$itinerary->operation_notes}}</span>
                </div>
            @endif
            @if($itinerary->procurement_notes)
                <div class="col-md-12">
                    <label class="control-label col-md-4">Proc. Notes:</label>
                    <span class="col-md-8">{{$itinerary->procurement_notes}}</span>
                </div>
            @endif

        </div>
        <hr class="col-md-12">
    </div>

</div>

<hr class="row separator">


<script>

    function updateNotes() {
        $.ajax({
            type: "PUT",
            url: '/api/itinerary/{{$itinerary->id}}',
            data: {
                'itinerary': [
                    {'notes': $("#note").val()}]
                ,

            },
        }).success(function (data) {
        });
    }

    function updateServicesStatus() {
        $.ajax({
            type: "PUT",
            url: '/api/itinerary/{{$itinerary->id}}',
            data: {
                'itinerary': [
                    {
                        'billing_fuel_status': $("#billing_fuel_status").find(":selected").text(),
                        'billing_service_status': $("#billing_service_status").find(":selected").text(),
                    }
                ]
            }
        }).success(function (data) {
        });
    }


    var columns = [
        {data: 'id', readOnly: true}, //0
        {data: 'parent_name', type: 'text', readOnly: true}, //1
        {data: 'service', renderer: renderServices, readOnly: true}, //2
        {data: 'procurement_quantity', type: 'numeric', readOnly: true}, //3
        {data: 'final_cost', type: 'numeric', format: '0.00', readOnly: true}, //4
        {data: 'procurement_status_text', type: 'text', readOnly: true}, //5
        {data: 'billing_quantity', type: 'numeric', format: '0.00'}, //6
        {data: 'billing_percentage', type: 'numeric', format: '0.0%'}, //7
        {data: 'billing_unit_price', type: 'numeric', format: '0.00'}, //8
        {data: 'price', type: 'numeric', format: '0.00', readOnly: true}, //9
        {data: 'adm_fee_checked', type: 'checkbox', checkedTemplate: '1', uncheckedTemplate: '0'}, //10
        {data: 'billing_checked', type: 'checkbox', checkedTemplate: '1', uncheckedTemplate: '0'}, //11
        {data: 'has_bills', type: 'numeric', readOnly: true}, //12
        {data: 'has_bills', renderer: renderBilledCheck, readOnly: true}, //13
        {data: 'profit', type: 'numeric', format: '0.00', readOnly: true}, //14
    ];

    var colHeaders = ['ID', 'Parent', 'Service', 'Quant.', 'Final Cost', 'P. Status', 'B. Quant.', 'Percent.',
        'Unit Price', 'TOTAL', 'A. Fee', 'Bill', 'has_bills', 'Billed', 'Profit',
    ];

    var services = {!! json_encode($itineraryServices, true)  !!};

    var hot_tables_biling{{$itinerary->id}} = [];
    var colWidths = [
        2, 125, 180, 70, 80, 80, 85, 80,
        80, 80, 80, 50, 1, 50, 70
    ];
    var lastParentService = null;


    renderItineraries = function (services) {
        var items = services;
        var hotElement = document.querySelector('#itinerary_billing_{{$itinerary->id}}');
        var hotSettings = {
            data: items,
            columns: columns,
            rowHeaders: false,
            width: 1200,
            autoWrapRow: true,
            height: 50 + 24 * items.length,
            colHeaders: colHeaders,
            colWidths: colWidths,
            maxRows: items.length,
            cells: function (row, col, prop) {
                var cellProperties = {};
                var data = items[row];
                if (data.parent_name != null) {
                    cellProperties.renderer = parentServicesRender;
                }
                return cellProperties;
            },
            afterChange: function (changes, source) {
                if (source == 'edit') {
                    var item = this.getData()[changes[0][0]];
                    updateItinerary(item);
                }

            },
        };
        hot_tables_biling{{$itinerary->id}} = new Handsontable(hotElement, hotSettings);
        activeCheckboxFunctionality();
    };

    reloadItineraryServices = function (data) {
        var items = data.data;
        var table = hot_tables_biling{{$itinerary->id}};
        table.loadData(items);
        table.updateSettings({
            height: 50 + 24 * items.length,
            maxRows: items.length,
            cells: function (row, col, prop) {
                var cellProperties = {};
                var data = items[row];
                if (data.parent_name != null) {
                    cellProperties.renderer = parentServicesRender;
                }
                return cellProperties;
            },
        });
        activeCheckboxFunctionality();
    };


    function parentServicesRender(instance, td, row, col, prop, value, cellProperties) {
        numericColRender = [3, 4, 6, 7, 8, 9, 12];
        checkboxColRender = [10, 11];
        billingCheckedColRender = [13];
        cellProperties.readOnly = true;

        Handsontable.renderers.TextRenderer.apply(this, arguments);

        if (numericColRender.includes(col)) {
            Handsontable.renderers.NumericRenderer.apply(this, arguments);
        } else if (checkboxColRender.includes(col)) {
            renderCheckBox(instance, td, row, col, prop, value, cellProperties);
        } else if (billingCheckedColRender.includes(col)) {
            td.innerHTML = "";
            //renderBilledCheck(instance, td, row, col, prop, value, cellProperties);
        }

        td.style.background = '#e6f5f9';
    }

    function renderCheckBox(instance, td, row, col, prop, value, cellProperties) {
        Handsontable.renderers.CheckboxRenderer.apply(this, arguments);
        cellProperties.readOnly = false;
    }

    function renderBilledCheck(instance, td, row, col, prop, value, cellProperties) {
        var instance2 = instance.getData()[row];
        td.innerHTML = "<i class='fa fa-check-square' style='color:darkgray'></i>";
        if (value != null && value > 0) {
            td.innerHTML = "<a href='{{url('admin/billing/provider-service-bill/')}}/" + instance2[0] + "' target='_blank'><i class='fa fa-check-square' style='color:green'></i></a>";
        }
        return td;
    }


    showError = function (data) {
        PNotify.removeAll();
        new PNotify({
            text: data.responseJSON.message,
            type: "error"
        });
    };

    renderItineraries(services);


    setItineraryTotals = function (data) {
        var itemId = '#itinerary_{{$itinerary->id}}';
        $(itemId + '_pike_services').val(data['billing_pike_service_cost']);
        $(itemId + '_discount').val(data['billing_discount']);
        $(itemId + '_status').val(data['billing_status']);
        $(itemId + '_notes').html(data['billing_notes']);
        $(itemId + '_total').html(data['totals']);
    };

    updateItinerary = function (item) {
        if (item[2] == 'TOTAL' || item[12] == 1) { //total or billed
            getItineraryProviderServices({{$itinerary->id}});
            return;
        }
        $.ajax({
            type: "PUT",
            url: '/api/itinerary/{{$itinerary->id}}/service/' + item[0],
            data: {
                'service_parent': item[2] == '' ? 1 : 0,
                'billing_quantity': item[6],
                'billing_percentage': item[7],
                'billing_unit_price': item[8],
                'procurement_quantity': item[3],
                'adm_fee_checked': item[10],
                'billing_checked': item[11],
                'billing-type': 'billing',
            },
        }).success(function (data) {
            getItineraryProviderServices({{$itinerary->id}});
        }).error(function (data) {
            getItineraryProviderServices({{$itinerary->id}});
        });
    };

    getItineraryProviderServices = function (itineraryId) {
        $.ajax({
            type: "GET",
            url: '/api/itinerary/' + itineraryId + '/service?in_billing=1',
        }).success(function (data) {
            reloadItineraryServices(data);
        }).error(function (data) {
            showError(data);
        })
    };


    function renderAdmFee(instance, td, row, col, prop, value, cellProperties) {
        td.innerHTML = '';
        var item = instance.getData()[row];
        var billValue = item[11]; // if it has bills, checkbox is disabled

        td.innerHTML = '';
        if (billValue != null && billValue < 1) {
            td.innerHTML = '<input type="checkbox" data-parent="' + lastParentService + '" class="bill_adm_fee" name="bill_adm_fee[]" value="' + item[0] + '">';
        }
        if (item[1] != null) {
            lastParentService = item[0];
            td.innerHTML = '';
        }
    }

    function renderServices(instance, td, row, col, prop, value, cellProperties) {
        td.innerHTML = '';
        var item = instance.getData()[row];
        var isAdditional = item[12];
        td.innerHTML = item[2];
        if (isAdditional === 1) {
            td.innerHTML += '(A)';
        }
    }

    function addBillCheckedOnAdmFill() {
        $('.bill_adm_fee').on('change', function () {
            if ($(this).is(':checked') && !parentIsCheck($(this))) {
                $(':input[value="' + $(this).val() + '"]').prop("checked", true);
            }
        });
    }

    function parentIsCheck(item) {
        return $('.bill_parent_services[data-value="' + item.data('parent') + '"').is(':checked');
    }

    function BillCheckParentUncheckChildren() {
        $('.bill_parent_services').on('change', function () {
            if ($(this).is(':checked')) {
                $('.bill_services:input[data-parent="' + $(this).data('value') + '"]').prop("checked", false);
            }
        });
    }

    function BillCheckChildUncheckParent() {
        $('.bill_services').on('change', function () {
            if ($(this).is(':checked')) {
                $(':input[data-value="' + $(this).data('parent') + '"]').prop("checked", false);
            }
        });
    }

    function activeCheckboxFunctionality() {
        addBillCheckedOnAdmFill();
        BillCheckParentUncheckChildren();
        BillCheckChildUncheckParent();
    }


    transformCheckBoxInList = function (id) {
        var itemsAux = $(id);
        var ret = [];
        for (var i = 0; i < itemsAux.length; i++) {
            ret.push($(itemsAux[i]).val());
        }
        return ret;
    };

    createBill = function () {
        var subServices = transformCheckBoxInList('.bill_services:checked');
        var subServicesAdmFeeAux = transformCheckBoxInList('.bill_adm_fee:checked');
        var serviceParent = transformCheckBoxInList('.bill_parent_services:checked');
        var parentServicesAdmFee = transformCheckBoxInList('.bill_parent_adm_fee:checked');
        var itineraryId = {{$itinerary->id}};

        $.ajax({
            type: "POST",
            url: '/api/billing/{{$itinerary->operation_id}}/create-bill',
            data: {
                'itinerary_id': {{$itinerary->id}},
                'services': subServices,
                'administrationFees': subServicesAdmFeeAux,
                'serviceParent': serviceParent,
                'parentServicesAdmFee': parentServicesAdmFee,
                'admFeePercentage': $('#{{$itinerary->id}}_adm_fee').val() / 100,
            },
        }).success(function (data) {
            if (data.accounting_services.length == 0) {
                alert('No services selected')
            } else {
                $(location).attr('href', '/admin/bills/' + data.id + '/edit?itineraryId=' + itineraryId);
            }
        }).error(function (data) {
            showError(data);
        })
    };

    $('#create-bill').on('click', function () {
        createBill();
    });

    addService = function (provider) {
        $.ajax({
            type: "POST",
            url: '/api/itinerary/{{$itinerary->id}}/service',
            data: {
                'service_id': $(provider + ' [name=service_id]').val(),
                'provider_id': $(provider + ' [name=provider_id]').val(),
                'billing_unit_price': $(provider + ' [name=billing_unit_price]').val(),
                'procurement_quantity': $(provider + ' [name=procurement_quantity]').val(),
                'status': $(provider + ' [name=status]').val(),
                'service_origin': 'Billing'
            },
        }).success(function (data) {
            getItineraryProviderServices({{$itinerary->id}});
        }).error(function (data) {
            showError(data)
        });
    };

</script>
