
<div id="service-add-{{$itinerary_id}}" class="col-md-12" style="margin-left: -18px; margin-bottom: 30px; width: 1100px">
    <input type="hidden" name="provider_id" value="{{Config::get('constants.pikeProviderId')}}">
    <input type="hidden" name="in_procurement" value="0">

    <div class="form-group col-md-4" style="margin-right:100px">
        <select class="form-control select2 col-md-12"  name="service_id">
            <option value=""> Service </option>
            @forelse($provider->serviceChild as $service)
                <option value="{{$service->id}}">{{$service->parent->name}} - {{$service->name}}</option>
            @empty
                <option disabled selected>No Services</option>
            @endforelse
        </select>
    </div>

    <div class="form-group col-md-3">
        <input type="text" name="procurement_quantity" class="form-control" placeholder="Quantity">
    </div>

    <div class="form-group col-md-3">
        <input type="text" name="billing_unit_price" class="form-control" placeholder="Amount">
    </div>



    <button onClick="addService('#service-add-{{$itinerary_id}}')" class="btn btn-default add-button col-md-2">
        Add Service
    </button>
</div>


<script src="{{ asset('vendor/adminlte/plugins/select2/select2.min.js') }}"></script>
<script>
    $('.select2').select2();
</script>
