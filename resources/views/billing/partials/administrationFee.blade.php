@php
    $administrationFees = $administrationFees['data'] ?? [] ;

@endphp

<div class="col-lg-12">
    <div class="row-fluid summary" id="itineraries-billing">
        <h3>Administration Fees</h3>
        <div id='itinerary_{{$itinerary->id}}'></div>
    </div>
</div>


<script>
    var columns = [
        {data: 'id', readOnly: true},
        {data: 'service', type: 'text', readOnly: true},
        {data: 'service_price', type: 'text', readOnly: true},
        {data: 'percentage', type: 'numeric', format: '0.0%'},
        {data: 'amount', type: 'numeric', format: '0.00'},
        {data: 'profit', type: 'numeric', format: '0.00', readOnly: true},
        {data: 'price', type: 'numeric', format: '0.00', readOnly: true},
        {data: 'has_bills', renderer: renderBillItem, readOnly: true},
        {data: 'delete_permit', renderer: deleteAdmFee, readOnly: true},
    ];
    var colHeaders = ['ID', 'Service', 'Service Price', 'Percentage %', 'Amount', 'Profit', 'Price', 'Bill', ''
    ];


    var administrationFees = {!! json_encode($administrationFees, true)  !!};
    var hot_tables_administration_fees{{$itinerary->id}} = [];


    renderAdministrationFees = function (administrationFees) {
        var hotElement = document.querySelector('#itinerary_{{$itinerary->id}}');
        var hotSettings = {
            data: administrationFees,
            columns: columns,
            rowHeaders: false,
            width: 900,
            autoWrapRow: true,
            height: 70 + 24 * administrationFees.length,
            colHeaders: colHeaders,
            maxRows: administrationFees.length,
            afterChange: function (changes, source) {
                if (source == 'edit') {
                    var item = this.getData()[changes[0][0]];
                    updateAdministrationFee(item);
                }

            },
        };
        hot_tables_administration_fees{{$itinerary->id}} = new Handsontable(hotElement, hotSettings);

    };

    renderAdministrationFees(administrationFees);

    updateAdministrationFee = function (item) {
        if (item[1] == 'TOTAL') {
            return
        }
        $.ajax({
            type: "PUT",
            url: '/api/itinerary/' + item[0] + '/service/fee',
            data: {
                'id': item[0],
                'billing_percentage': item[3],
                'billing_unit_price': item[4],
            }
        }).success(function (data) {
            data = data.data;
            reloadAdministrationFees(data.administrationFees);
        });
    };

    refreshAdministrationFee = function () {
        $.ajax({
            type: "GET",
            url: '/api/itinerary/' + {{$itinerary->id}} +'/service/fee'
        }).success(function (data) {
            data = data.data;
            reloadAdministrationFees(data.administrationFees);
        });
    };

    reloadAdministrationFees = function (administrationFees) {
        administrationFees = administrationFees.data;
        var table = hot_tables_administration_fees{{$itinerary->id}};
        table.loadData(administrationFees);
        table.updateSettings({
            height: 70 + 24 * administrationFees.length,
            maxRows: administrationFees.length
        })

    };

    updateAdministrationFeesFromService = function (item) {
        renderAdministrationFees(administrationFees);
    };

    function deleteAdmFee(instance, td, row, col, prop, value, cellProperties) {
        var instance2 = JSON.stringify(instance.getData()[row]);
        newHtml = '';
        if (instance.getData()[row][1] != 'TOTAL'){
                newHtml = "<button href='' onclick='deleteFee(" + instance2 + ")' class ='btn btn-sd btn-danger' " +
                        "style='padding: 3px 7px;font-size: 10px;margin: 2px;'> <i class='fa fa-trash-o'></i></button>";
            }
        td.innerHTML = newHtml;

    }

    function deleteFee(item) {
        if (confirm('Are you sure you want to delete this fee?')) {
            $.ajax({
                type: "DELETE",
                url: '/api/administration_fee/' + item[0],
            }).success(function (data) {
                if (data.data == false) {
                    return alert(data.message);
                }
                data = data.data;
                reloadAdministrationFees(data.administrationFees);
            });
        }
    }

    function renderBillItem (instance, td, row, col, prop, value, cellProperties) {
        var item = instance.getData()[row];
        if (value != null) {
            td.innerHTML = value+' - <input type="checkbox" class="bill_administration_fees" name="bill_administration_fees[]" value="'+item[0]+'">';
        } else {
            td.innerHTML = '';
        }
    }

</script>
