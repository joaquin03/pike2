@php
    $providers = $providerPermanentPermits;
@endphp

<div class="row">

    <div class="col-md-12">
        <div class="row-fluid summary">
            <span class="h3">Permanent Permits</span>


            <div class="row-fluid summary" id="permanent-permits">
                <div class="collapse in row">
                    @foreach($providers ?? [] as $provider)
                        <h3 class="col-md-12">{{ $provider['name'] }}</h3>
                        <div class="container col-md-10 " style="overflow: hidden">
                            <div id='permanent-permissions-provider-{{ $provider['id'] }}'
                                 data-providerId="{{$provider['id']}}"></div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <button id="create-bill-permanent-permits" class="btn btn-success">Create Bill</button>

    </div>


</div>
@push('script_stack')
    <script>

        var columnsPermanentP = [
            {data: 'id', type: 'numeric', readOnly: true},
            {data: 'code', type: 'numeric', readOnly: true},
            {data: 'type', type: 'numeric', readOnly: true},
            {data: 'country', type: 'text', readOnly: true},
            {data: 'icao_from', type: 'text', readOnly: true},
            {data: 'icao_to', type: 'text', readOnly: true},
            {data: 'expiration_date', type: 'date', readOnly: true},
            {data: 'operation_status', type: 'text'},
            {data: 'procurement_tax', type: 'numeric', format: '0.0%'},
            {data: 'procurement_cost', type: 'text'},
            {data: 'final_price', type: 'numeric', format: '0.00', readOnly: true},
            {data: 'procurement_status', type: 'text', renderer: permanentPermissionStatusRender},
            {data: 'file', type: 'text', readOnly: true, renderer: fileHtmlRender},
            {data: 'invoice', type: 'text', readOnly: true, renderer: fileHtmlRender},
            {data: 'administration_fee', renderer: renderAdmFee, readOnly: true},
            {data: 'has_bills', renderer: renderBillPermPermits, readOnly: true},
        ];
        var colHeadersPermanentP = [
            'ID', 'Code', 'Type', 'Country', 'From', 'To', 'Expiration Date', 'Ops Status', 'Tax %', 'Cost',
            'Final Price', 'Proc Status', 'File', 'Invoice', 'Adm Fee', 'Bill'];
        var permanentPermission = [];
                @foreach($providers ?? [] as $provider)
        var hotElement2 = document.querySelector('#permanent-permissions-provider-{{$provider['id']}}');

        var hotSettings2 = {
            data: {!! json_encode($provider['permits']) !!},
            columns: columnsPermanentP,
            width: 1500,
            autoWrapRow: true,
            height: 50 + 20 *{{count($provider['permits'])}},
            colHeaders: colHeadersPermanentP,
            afterChange: function (changes, source) {
                if (source == 'edit') {
                    var item = this.getData()[changes[0][0]];
                    updatePermanentPermissionRow(item, {{ $provider['id'] }});
                }

            },
        };
        permanentPermission[{{$provider['id']}}] = new Handsontable(hotElement2, hotSettings2);
        activeCheckboxFunctionality();

        @endforeach


            updatePermanentPermissionRow = function (row, providerId) {
            $.ajax({
                type: "PUT",
                url: '/api/procurement/{{$operation->id}}/permanent-permission/' + row[0] + '/',
                data: {
                    'id': row[0],
                    'operation_status': row[7],
                    'tax': row[8],
                    'cost': row[9],
                    'procurement_status': row[10],
                    'administration_fee': row[14]
                },
            }).success(function (data) {
                data = data.data;
                reloadProviderPermanentPermits(data);
            }).error(function(response) {
                response = response.responseJSON.data;
                reloadProviderPermanentPermits(response);
            });;
        };

        reloadProviderPermanentPermits = function (providers) {
            for (var i = 0; i < providers.length; i++) {
                var provider = providers[i];
                var items = provider['permits'];
                var table = permanentPermission[provider['id']];
                table.loadData(items);
                table.updateSettings({
                    height: 50 + 24 * items.length,
                    maxRows: items.length,
                })
            }
        };

        function permanentPermissionStatusRender(instance, td, row, col, prop, value, cellProperties) {
            var instance2 = JSON.stringify(instance.getData()[row]);
            var provider = instance.rootElement.id.split("-")[3];
            newHtml = "<span onclick='changePermanentPermissionStatus(" + instance2 + ", " + provider + ")'>";
            if (value != 'Done') {
                newHtml += "<i class='fa fa-check-square' style='color:darkgray'></i></span>";

            } else {
                newHtml += "<i class='fa fa-check-square' style='color:green'/></span>";
            }
            td.innerHTML = newHtml;
        }

        function changePermanentPermissionStatus(item, provider) {
            var status = item[10];
            if (status == "Process") {
                item[10] = "Done";
            } else {
                item[10] = "Process";
            }
            updatePermanentPermissionRow(item, provider);
        }

        function renderBillPermPermits(instance, td, row, col, prop, value, cellProperties) {
            td.innerHTML = '';
            var item = instance.getData()[row];
            if (value != null && value < 1) {
                td.innerHTML = '<input type="checkbox" class="bill_permanent_permits" name="bill_permanent_permits[]" value="' + item[0] + '">';
            }
        }

        createPermanentPermitsBill = function() {
            var items = $('.bill_permanent_permits:checked');
            var items2 = $('.bill_permanent_permits_administration_fees:checked');
            var permanentPermits = [];
            var permanentPermitsAdministrationFees = []
            for (var i=0; i<items.length; i++) {
                permanentPermits.push($(items[i]).val());
            }
            for (var i=0; i<items2.length; i++) {
                permanentPermitsAdministrationFees.push($(items2[i]).val());
            }

            $.ajax({
                type: "POST",
                url: '/api/billing/{{$operation->id}}/create-bill',
                data: {
                    'permanentPermits': permanentPermits,
                    'permanentPermitsAdministrationFees': permanentPermitsAdministrationFees
                },
            }).success(function (data) {
                $(location).attr('href', '/admin/bills/'+data.id+'/edit');
            });
        };

        $('#create-bill-permanent-permits').on('click', function () {
                createPermanentPermitsBill();
        });

        function activeCheckboxFunctionality() {
            addBillCheckedOnAdmFill();
        }

        function addBillCheckedOnAdmFill() {
            $('.bill_permanent_permits_administration_fees').on('change', function () {
                if ($(this).is(':checked')) {
                    $(':input[value="' + $(this).val() + '"]').prop("checked", true);
                }
            });
        }

        function renderAdmFee(instance, td, row, col, prop, value, cellProperties) {
            td.innerHTML = '';
            var item = instance.getData()[row];
            var billValue = item[15];
            if (billValue != null && billValue < 1) {
                td.innerHTML = '<input type="checkbox" class="bill_permanent_permits_administration_fees" name="bill_adm_fee[]" value="' + item[0] + '">';
            }

        }

    </script>



@endpush