@php
    $administrationFees = $permanentPermitsAdministrationFees ['data'] ?? [] ;
@endphp

<div class="col-lg-12">
    <div class="row-fluid summary" id="permanent-permissions-billing">
        <h3>Permanent Permission Administration Fees</h3>
        <div id='permanentPermitOperation_{{$operation->id}}'></div>
    </div>
</div>

@push('script_stack')

    <script>
        var columns = [
            {data: 'id', readOnly: true, className:"htRight"},
            {data: 'service', type: 'text', readOnly: true},
            {data: 'service_price', type: 'text', readOnly: true},
            {data: 'percentage', type: 'numeric', format: '0.0%'},
            {data: 'amount', type: 'numeric', format: '0.00'},
            {data: 'profit', type: 'numeric', format: '0.00', readOnly: true},
            {data: 'price', type: 'numeric', format: '0.00', readOnly: true},
            {data: 'has_bills', renderer: renderPermanentPermitsBillItem, readOnly: true},
            {data: 'delete_permit', renderer: deleteAdmFee, readOnly: true},



        ];
        var colHeaders = ['ID', 'Service', 'Service Price', 'Percentage %', 'Amount', 'Profit', 'Price', 'Bill', ''
        ];

        var permanentPermitsAdministrationFees = {!! json_encode($administrationFees, true)  !!};
        var hot_tables_permanent_permits_administration_fees{{$operation->id}} = [];


        renderPermanentPermitsAdministrationFees = function (permanentPermitsAdministrationFees) {
            var hotElement = document.querySelector('#permanentPermitOperation_{{$operation->id}}');
            var hotSettings = {
                data: permanentPermitsAdministrationFees,
                columns: columns,
                rowHeaders: false,
                width: 1500,
                autoWrapRow: true,
                height: 50 + 20 * permanentPermitsAdministrationFees.length,
                colHeaders: colHeaders,
                maxRows: permanentPermitsAdministrationFees.length,
                afterChange: function (changes, source) {
                    if (source == 'edit') {
                        var item = this.getData()[changes[0][0]];
                        updatePermanentPermitAdministrationFee(item);
                    }

                },
            };
            hot_tables_permanent_permits_administration_fees{{$operation->id}} = new Handsontable(hotElement, hotSettings);

        };

        renderPermanentPermitsAdministrationFees(permanentPermitsAdministrationFees);


        updatePermanentPermitAdministrationFee = function (item) {
            if (item[1] == 'TOTAL') {
                return
            }
            $.ajax({
                type: "PUT",
                url: '/api/administration_fee/' + item[0] + '/permanent_permit/fee',
                data: {
                    'id': item[0],
                    'billing_percentage': item[3],
                    'billing_unit_price': item[4],
                }
            }).success(function (data) {
                data = data.data;
                reloadPermanentPermitAdministrationFees(data.administrationFees);
            });
        };

        refreshPermanentPermitAdministrationFee = function () {
            $.ajax({
                type: "GET",
                url: '/api/administration_fee/' + {{$operation->id}} +'/permanent_permit/fee'
            }).success(function (data) {
                data = data.data;
                reloadPermanentPermitAdministrationFees(data.administrationFees);
            });
        };


        reloadPermanentPermitAdministrationFees = function (permanentPermitsAdministrationFees) {
            permanentPermitsAdministrationFees = permanentPermitsAdministrationFees.data;
            var table = hot_tables_permanent_permits_administration_fees{{$operation->id}};
            table.loadData(permanentPermitsAdministrationFees);
            table.updateSettings({
                height: 35 + 24 * permanentPermitsAdministrationFees.length,
                maxRows: permanentPermitsAdministrationFees.length
            })
        };

        updatePermanentPermitAdministrationFeesFromService = function (item) {
            renderPermanentPermitsAdministrationFees(permanentPermitsAdministrationFees);

        };

        function renderPermanentPermitsBillItem(instance, td, row, col, prop, value, cellProperties) {
            var item = instance.getData()[row];
            if (value != null) {
                td.innerHTML = value + ' - <input type="checkbox" class="bill_permanent_permits_administration_fees" name="bill_permanentPermits_administration_fees[]" value="' + item[0] + '">';
            } else {
                td.innerHTML = '';
            }
        }

        function deleteAdmFee(instance, td, row, col, prop, value, cellProperties) {
            var instance2 = JSON.stringify(instance.getData()[row]);
            newHtml = '';
            if (instance.getData()[row][1] != 'TOTAL'){
                newHtml = "<button href='' onclick='deletePermissionFee(" + instance2 + ")' class ='btn btn-sd btn-danger' " +
                        "style='padding: 3px 7px;font-size: 10px;margin: 2px;'> <i class='fa fa-trash-o'></i></button>";
            }
            td.innerHTML = newHtml;

        }

        function deletePermissionFee(item) {
            if (confirm('Are you sure you want to delete this fee?')) {
                $.ajax({
                    type: "DELETE",
                    url: '/api/administration_fee/' + item[0],
                }).success(function (data) {
                    if (data.data == false) {
                        return alert(data.message);
                    }
                    data = data.data;
                    reloadPermanentPermitAdministrationFees(data.administrationFees);
                });
            }
        }

    </script>

@endpush