@php
    $providers = $providerPermits;
@endphp

<div class="row">
    <div class="col-lg-12">
        <div class="row-fluid summary">
            <span class="h2 col-md-2">Permits</span>
            <div class="col-md-6">
                <div class="col-md-10">
                    <label for="billing_permits_status">Permits Status</label>
                    <br>
                    <select class="form-control select2 " style="margin-left: 0px; width: 300px"
                            id="billing_permits_status" onchange="updateBillingPermitsStatus()">
                        <option value="">-</option>
                        <option value="Done"
                                @if($operation->getBillingPermitsStatus()=='Done')
                                selected
                                @endif>Done
                        </option>
                        <option value="Not Required"
                                @if($operation->getBillingPermitsStatus()=='Not Required')
                                selected
                                @endif>Not Required
                        </option>
                    </select>


                </div>

            </div>

            <div class="row-fluid summary col-md-12" id="aircraft-permits">
                <div id="" class="collapse in row">
                    @foreach($providers ?? [] as $provider)
                        <h3 class="col-md-12">{{ $provider['name'] }}</h3>
                        <div class="container col-md-12 " style="overflow: hidden">
                            <div id='permissions-provider-{{ $provider['id'] }}'></div>
                        </div>

                    @endforeach
                </div>
            </div>
            <div>
                <div class="col-md-2" style="margin: 20px">
            <button id="create-bill-permits" class="btn btn-success">Create Bill</button>
                </div>
            <div class="col-md-2">
                <label for="adm_fee">Adm. Fee %</label>
                <br>
                <input id="{{$operation->id}}_adm_fee" type="number" name="adm_fee" value="12"
                       class="form-control">
                <br><br>
            </div>

            </div>
        </div>
    </div>
    <hr>


</div>
@push('script_stack')
    <script>


        var columns = [
            {data: 'id', type: 'numeric', readOnly: true},
            {data: 'code', type: 'numeric', readOnly: true},
            {data: 'type', type: 'numeric', readOnly: true},
            {data: 'country', type: 'text', readOnly: true},
            {data: 'icao_from', type: 'text', readOnly: true},
            {data: 'icao_to', type: 'text', readOnly: true},
            {data: 'expiration_date', type: 'date', readOnly: true},
            {data: 'operation_status', type: 'text', readOnly: true},
            {data: 'final_cost', type: 'numeric', format: '0.00', readOnly: true},
            {data: 'billing_percentage', type: 'numeric', format: '0.00%'},
            {data: 'billing_unit_price', type: 'numeric', format: '0.00'},
            {data: 'profit', type: 'numeric', format: '0.00', readOnly: true},
            {data: 'final_price', type: 'numeric', format: '0.00', readOnly: true},
            {data: 'procurement_status', type: 'text', renderer: permissionStatusRender, readOnly: true},
            {data: 'administration_fee', renderer: renderAdmFee, readOnly: true},
            {data: 'has_bills', renderer: renderBillPermits, readOnly: true},
        ];
        var colHeaders = [
             '','Code', 'Type', 'Country', 'From', 'To', 'Exp. Date', 'Ops Status', 'Final Cost', 'Perc. %', 'Amount',
            'Profit', 'Final Price', 'Proc.', 'A. Fee', 'Bill'
        ];
        var hotProviderPermits = [];
        var colWidths = [
            2, 100, 50, 80, 80, 60, 110,
            70, 80, 80, 80, 80, 80,  40, 40, 40
        ];

                @foreach($providers ?? [] as $provider)
        var hotElement = document.querySelector('#permissions-provider-{{$provider['id']}}');
        var hotSettings = {
            data: {!! json_encode($provider['permits'], true) !!} ,
            columns: columns,
            width: 1200,
            autoWrapRow: true,
            height: 50 + 40 *{{count($provider['permits'])}},
            colHeaders: colHeaders,
            colWidths: colWidths,
            afterChange: function (changes, source) {
                if (source == 'edit') {
                    var item = this.getData()[changes[0][0]];
                    updateRow(item, {{$provider['id']}});
                }
            },
        };
        hotProviderPermits[{{$provider['id']}}] = new Handsontable(hotElement, hotSettings);
        activeCheckboxFunctionality();
        @endforeach

            updateRow = function (row, providerId) {
            $.ajax({
                type: "PUT",
                url: '/api/procurement/{{$operation->id}}/aircraft-permission/' + row[0] + '?billing-type=bill',
                data: {
                    'id': row[0],
                    'type': row[2],
                    'billing_percentage': row[9],
                    'billing_unit_price': row[10],
                    'procurement_status': row[11],
                    'administration_fee': row[13],
                },
            }).success(function (response) {
                response = response.data;
                reloadProviderPermits(response);
            }).error(function(response) {
                alert('Permission already contained in statement');
                response = response.responseJSON.data;
                reloadProviderPermits(response);
            });
        };

        reloadProviderPermits = function (providers) {
            for (var i = 0; i < providers.length; i++) {
                var provider = providers[i];
                var items = provider['permits'];
                var table = hotProviderPermits[provider['id']];
                table.loadData(items);
                table.updateSettings({
                    height: 50 + 24 * items.length,
                    maxRows: items.length,
                })
            }
        };


        function uploadFile(data) {
            var providerId = data.getAttribute('data');
            $('#form_upload-' + providerId).submit();
        }


        function fileHtmlRender(instance, td, row, col, prop, value, cellProperties) {
            if (value) {
                newHtml = "<a href=" + value + "'/storage' target='_blank'>" +
                    "<i class='fa fa-cloud-download'/></a>";
                td.innerHTML = newHtml;

                return td;
            }
        }

        function permissionStatusRender(instance, td, row, col, prop, value, cellProperties) {
            var instance2 = JSON.stringify(instance.getData()[row]);
            newHtml = "<span onclick='changePermissionStatus(" + instance2 + ")'>";
            if (value != 'Done') {
                newHtml += "<i class='fa fa-check-square' style='color:darkgray'></i></span>";

            } else {
                newHtml += "<i class='fa fa-check-square' style='color:green'/></span>";
            }
            td.innerHTML = newHtml;
        }

        function changePermissionStatus(item) {
            var status = item[10];
            if (status == "Process") {
                item[10] = "Done";
            } else {
                item[10] = "Process";
            }
            updateRow(item);
        }


        function renderBillPermits(instance, td, row, col, prop, value, cellProperties) {
            td.innerHTML = '';
            var item = instance.getData()[row];
            if (value != null && value < 1) {
                td.innerHTML = '<input type="checkbox" class="bill_permits" name="bill_permits[]" value="' + item[0] + '">';
            }
        }

        createPermitsBill = function () {
            var items = $('.bill_permits:checked');
            var items2 = $('.bill_permits_administration_fees:checked');
            var permits = [];
            var permitsAdministrationFees = []
            for (var i = 0; i < items.length; i++) {
                permits.push($(items[i]).val());
            }
            for (var i = 0; i < items2.length; i++) {
                permitsAdministrationFees.push($(items2[i]).val());
            }

            $.ajax({
                type: "POST",
                url: '/api/billing/{{$operation->id}}/create-bill',
                data: {
                    'permits': permits,
                    'permitsAdministrationFees': permitsAdministrationFees,
                    'admFeePercentage': $('#{{$operation->id}}_adm_fee').val() / 100,
                },
            }).success(function (data) {
                if(data.accounting_services.length == 0){
                    alert('No permits selected')
                } else {
                    $(location).attr('href', '/admin/bills/' + data.id + '/edit');
                }
            });
        };


        $('#create-bill-permits').on('click', function () {
            createPermitsBill();
        });

        function activeCheckboxFunctionality() {
            addBillCheckedOnAdmFill();
        }

        function addBillCheckedOnAdmFill() {
            $('.bill_permits_administration_fees').on('change', function () {
                if ($(this).is(':checked')) {
                    $(':input[value="' + $(this).val() + '"]').prop("checked", true);
                }
            });
        }

        function renderAdmFee(instance, td, row, col, prop, value, cellProperties) {
            td.innerHTML = '';
            var item = instance.getData()[row];
            var billValue = item[15];
            if (billValue != null && billValue < 1) {
                td.innerHTML = '<input type="checkbox" class="bill_permits_administration_fees" name="bill_adm_fee[]" value="' + item[0] + '">';
            }

        }

        function updateBillingPermitsStatus() {
            $.ajax({
                type: "POST",
                url: '/admin/operation/{{$operation->id}}/edit',
                data: {
                    'billing_permits_status': $("#billing_permits_status").find(":selected").text(),
                }
            }).success(function (data) {
            });
        }


    </script>
@endpush