@php
    $administrationFees = $administrationFees ['data'] ?? [] ;
@endphp

<div class="col-lg-12">
    <div class="row-fluid summary" id="permissions-billing">
        <h3>Permission Administration Fees</h3>
        <div id='operation_{{$operation->id}}'></div>
    </div>
</div>

@push('script_stack')

    <script>
        var columns = [
            {data: 'id', readOnly: true, className:"htRight"},
            {data: 'service', type: 'text', readOnly: true},
            {data: 'service_price', type: 'text', readOnly: true},
            {data: 'percentage', type: 'numeric', format: '0.0%'},
            {data: 'amount', type: 'numeric', format: '0.00'},
            {data: 'profit', type: 'numeric', format: '0.00', readOnly: true},
            {data: 'price', type: 'numeric', format: '0.00', readOnly: true},
            {data: 'has_bills', renderer: renderPermitsBillItem, readOnly: true},
            {data: 'delete_permit', renderer: deleteAdmFee, readOnly: true},

        ];
        var colHeaders = ['ID', 'Service', 'Service Price', 'Percentage %', 'Amount', 'Profit', 'Price', 'Bill', ''
        ];

        var permitsAdministrationFees = {!! json_encode($administrationFees, true)  !!};
        var hot_tables_permits_administration_fees{{$operation->id}} = [];


        renderPermitsAdministrationFees = function (permitsAdministrationFees) {
            var hotElement = document.querySelector('#operation_{{$operation->id}}');
            var hotSettings = {
                data: permitsAdministrationFees,
                columns: columns,
                rowHeaders: false,
                width: 1500,
                autoWrapRow: true,
                height: 50 + 20 * permitsAdministrationFees.length,
                colHeaders: colHeaders,
                maxRows: permitsAdministrationFees.length,
                afterChange: function (changes, source) {
                    if (source == 'edit') {
                        var item = this.getData()[changes[0][0]];
                        updatePermitAdministrationFee(item);
                    }

                },
            };
            hot_tables_permits_administration_fees{{$operation->id}} = new Handsontable(hotElement, hotSettings);

        };

        renderPermitsAdministrationFees(permitsAdministrationFees);

        updatePermitAdministrationFee = function (item) {
            if (item[1] == 'TOTAL') {
                return
            }
            $.ajax({
                type: "PUT",
                url: '/api/administration_fee/' + item[0] + '/permit/fee',
                data: {
                    'id': item[0],
                    'billing_percentage': item[3],
                    'billing_unit_price': item[4],
                }
            }).success(function (data) {
                data = data.data;
                reloadPermitAdministrationFees(data.administrationFees);
            });
        };

        refreshPermitAdministrationFee = function () {
            $.ajax({
                type: "GET",
                url: '/api/administration_fee/' + {{$operation->id}} +'/permit/fee'
            }).success(function (data) {
                data = data.data;
                reloadPermitAdministrationFees(data.administrationFees);
            });
        };


        reloadPermitAdministrationFees = function (permitsAdministrationFees) {
            permitsAdministrationFees = permitsAdministrationFees.data;
            var table = hot_tables_permits_administration_fees{{$operation->id}};
            table.loadData(permitsAdministrationFees);
            table.updateSettings({
                height: 35 + 24 * permitsAdministrationFees.length,
                maxRows: permitsAdministrationFees.length
            })
        };

        updatePermitAdministrationFeesFromService = function (item) {
            renderPermitsAdministrationFees(permitsAdministrationFees);
        };

        function renderPermitsBillItem(instance, td, row, col, prop, value, cellProperties) {
            var item = instance.getData()[row];
            if (value != null) {
                td.innerHTML = value + ' - <input type="checkbox" class="bill_permits_administration_fees" name="bill_permits_administration_fees[]" value="' + item[0] + '">';
            } else {
                td.innerHTML = '';
            }
        }

        function deleteAdmFee(instance, td, row, col, prop, value, cellProperties) {
            var instance2 = JSON.stringify(instance.getData()[row]);
            newHtml = '';
            if (instance.getData()[row][1] != 'TOTAL'){
                newHtml = "<button href='' onclick='deletePermanentPermissionFee(" + instance2 + ")' class ='btn btn-sd btn-danger' " +
                        "style='padding: 3px 7px;font-size: 10px;margin: 2px;'> <i class='fa fa-trash-o'></i></button>";
            }
            td.innerHTML = newHtml;

        }

        function deletePermanentPermissionFee(item) {
            if (confirm('Are you sure you want to delete this fee?')) {
                $.ajax({
                    type: "DELETE",
                    url: '/api/administration_fee/' + item[0],
                }).success(function (data) {
                    if (data.data == false) {
                        return alert(data.message);
                    }
                    data = data.data;
                    reloadPermitAdministrationFees(data.administrationFees);
                });
            }
        }


    </script>

@endpush