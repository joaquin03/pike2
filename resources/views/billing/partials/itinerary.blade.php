
    <table id="crudTable" class="table table-bordered table-striped display" style="width:100%; font-size: 17px">
        <thead>
        <tr>
            <th></th>
            <th style="width: 20%">Type</th>
            <th style="width: 20%">Airport</th>
            <th style="width: 20%">Date</th>
            <th style="width: 20%">Time</th>
            <th style="width: 20%">Handler</th>
            <th style="width: 20%">Status</th>
        </tr>


        </thead>
        <tbody>
        @foreach($itineraries as $itinerary)
            <tr data-id="{{ $itinerary->id }}">
                @if($itinerary->type == 'ATA' || $itinerary->type == 'ETA')
                    <td class="details-control text-center cursor-pointer" id="itinerary-{{$itinerary->id}}" style="width: 10px;">
                        <i data-entry-id="{{ $itinerary->id }}"
                           class="fa fa-plus-square-o details-row-button cursor-pointer"></i>
                    </td>
                @else
                    <td></td>
                @endif
                <td><b>{{ $itinerary->type }}</b></td>
                <td>{{ $itinerary->airport->icao }}</td>
                <td>@if($itinerary->hasDate()) {{ $itinerary->date}}@else TBA @endif</td>
                <td>@if($itinerary->hasTime()){{ $itinerary->time }}@else TBA @endif</td>
                <td>
                    @if($itinerary->handler)
                        {{ $itinerary->handler->name }}
                    @else
                        -
                    @endif
                </td>
                <td>
                    @if($itinerary->is_canceled)
                        <button class="btn btn-danger" style="margin-right:5px; pointer-events: none">
                            Canceled
                        </button>
                    @else
                        <button class="btn btn-success" style="margin-right:5px; pointer-events: none">
                            Active
                        </button>
                    @endif
                </td>

            </tr>
        @endforeach
        </tbody>


    </table>

<div class="box-footer">

</div>



@push('styles_stack')
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/plugins/datepicker/datepicker3.css') }}">
@endpush

@push('script_stack')
    <script type="text/javascript"
            src="{{ asset('vendor/adminlte/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script type="text/javascript"
            src="{{ asset('vendor/adminlte/plugins/datatables/dataTables.bootstrap.js') }}"></script>
    <script>

        jQuery(document).ready(function ($) {

            var table = $("#crudTable").DataTable({
                "bPaginate": false,
                /* Disable initial sort */
                "aaSorting": [],

            });

            // var crudTable = $('#crudTable tbody');
            // Remove any previously registered event handlers from draw.dt event callback
            $('#crudTable tbody').off('click', 'td .details-row-button');

            // Make sure the ajaxDatatables rows also have the correct classes
            $('#crudTable tbody td .details-row-button').parent('td')
                .removeClass('details-control').addClass('details-control')
                .removeClass('text-center').addClass('text-center')
                .removeClass('cursor-pointer').addClass('cursor-pointer');

            // Add event listener for opening and closing details
            $('#crudTable tbody td.details-control').on('click', function () {
                hideItinerary();
                var tr = $(this).closest('tr');
                var btn = $(this).find('.details-row-button');
                var row = table.row(tr);
                if (row.child.isShown()) {
                    // This row is already open - close it
                    btn.removeClass('fa-minus-square-o').addClass('fa-plus-square-o');
                    $('div.table_row_slider', row.child()).slideUp(function () {
                        row.child.hide();
                        tr.removeClass('shown');
                    });
                }
                else {
                    // Open this row
                    btn.removeClass('fa-plus-square-o').addClass('fa-minus-square-o');
                    // Get the details with ajax
                    $.ajax({
                        url: '{{ url("/admin/itinerary-billing")}}/' + btn.data('entry-id') + '/',
                        type: 'GET',
                    })
                        .done(function (data) {
                            row.child("<div class='table_row_slider'>" + data + "</div>", 'no-padding').show();
                            tr.addClass('shown');
                            $('div.table_row_slider', row.child()).slideDown();
                        })
                        .fail(function (data) {
                            row.child("<div class='table_row_slider'>There was an error loading the details. Please retry.</div>").show();
                            tr.addClass('shown');
                            $('div.table_row_slider', row.child()).slideDown();
                        })
                        .always(function (data) {

                        });
                }

            });

            hideItinerary = function() {
                items = $('.details-row-button.fa-minus-square-o');
                for(var i=0; i<items.length; i++) {
                    $(items[i]).click();
                }
            }
            showItineraryByUrl();

        });

        showItineraryByUrl = function () {
            var url = $(location).attr('href');
            var itinerary = url.split('=')[1];
            console.log(itinerary);

            if (itinerary != null) {
                console.log(itinerary);
                $('[data-entry-id='+itinerary+']').click();
            }
        };



    </script>

    <script>

        $('#estimated').on('shown', function () {
            $("#time_select").click();
        });
    </script>

     <script>
         $(".delete_itinerary").on("click", function(){
             return confirm("Do you want to delete this itinerary?");
         });
     </script>

@endpush