{{ csrf_field() }}

<div class="box-body">
    <div class="col-md-12">
        <h4 class="form-section">General</h4>

        <div class="form-group col-md-4">
            <label class="control-label col-md-4">Code:</label>
            <span class="col-md-8">{{ $operation->code }}</span>
        </div>

        <div class="form-group col-md-4">
            <label class="control-label col-md-4">In Charge:</label>
            <span class="col-md-8">{{ $operation->user->name }}</span>
        </div>

        <div class="form-group col-md-4">
            <label class="control-label col-md-4">State:</label>
            <span class="col-md-8">{{ $operation->state }}</span>
        </div>

        <div class="form-group col-md-4">
            <label class="control-label col-md-4">Quotation Notes:</label>
            <span class="col-md-8">{{$operation->quotation_notes}}</span>
        </div>
        <div class="form-group col-md-4">
            <label class="control-label col-md-4">Operation Notes:</label>
            <span class="col-md-8">{{$operation->operation_notes}}</span>
        </div>
        <div class="form-group col-md-4">
            <label class="control-label col-md-4">Procurement Notes:</label>
            <span class="col-md-8">{{$operation->procurement_notes}}</span>
        </div>
        <div class="form-group col-md-12">
            <label class="control-label col-md-2">General Notes:</label>
            <span class="col-md-10">{{$operation->notes}}</span>
        </div>



        @if ($operation->ref_number != '')
        <div class="form-group col-md-4">
            <label class="control-label col-md-4">Ref. Number:</label>
            <span class="col-md-8">{{$operation->ref_number}}</span>
        </div>
        @endif
    </div>

    <div class="col-md-12">
        <h4 class="form-section">Aircraft</h4>

        <div class="form-group col-md-4">
            <label class="control-label col-md-4">Registration</label>
            <span id="registration" class="col-md-8">{{$operation->aircraft ? $operation->aircraft->registration : ''}}</span>
            <span id="operation-aircraft-id" class="hidden">{{$operation->aircraft ? $operation->aircraft->id : ''}}</span>

        </div>

        <div class="form-group col-md-4">
            <label class="control-label col-md-4">Operator</label>
            <span class="col-md-8">@if($operation->aircraftOperator) {{  $operation->aircraftOperator->name }} @endif</span>
        </div>

        <div class="form-group col-md-4">
            <label class="control-label col-md-4">Client</label>
            <span class="col-md-8">{{ $operation->aircraftClient->name }}</span>
        </div>
        <div class="form-group col-md-4">
            <label class="control-label col-md-4">Billing Client</label>
            <span class="col-md-8">{{ $operation->billingCompany->name }}
                <a href="{{route('crud.billing.showChangeBillingCompany', ['id'=> $operation->id])}}" data-toggle="modal" data-target="#myModal"
                   class="ajax-modal"> (Change)</a>
            </span>

        </div>

        <div class="form-group col-md-4">
            <label class="control-label col-md-4">Aircraft Type</label>
            <div class="col-md-8">
                <span id="aircraft_type" name="aircraft_type" type="text"></span>
            </div>
        </div>
        <div class="form-group col-md-4">
            <label class="control-label col-md-4">MTOW</label>
            <div class="col-md-8">
                <span id="mtow" name="mtow" type="text"></span>
            </div>
        </div>
        <div class="form-group col-md-4">
            <label class="control-label col-md-4">MTOW Type</label>
            <div class="col-md-8">
                <span id="mtow_type" name="mtow_type" type="text"></span>
            </div>
        </div>
        <div class="form-group col-md-4">
            <label class="control-label col-md-4">MTOW Hand. Serv.</label>
            <div class="col-md-8">
                <span id="handling_service" name="handling_service" type="text">$ {{ $operation->aircraft ? $operation->aircraft->type->getHandlingPrice() : '' }}</span>
            </div>
        </div>
        <div class="form-group col-md-12">
            <label class="control-label col-md-1">Route</label>
            <span class="col-md-11" style="text-align: left; padding-left: 50px;">{{ $operation->route }}</span>
        </div>
    </div>

    <div class="col-md-12" style="font-size: 16px">
        <h4 class="form-section">Total Operation</h4>
        <div class="col-md-3">
            <p><b>Service:</b> <span id="total-service">{{ round($total['service'],2) }}</span></p>
        </div>
        <div class="col-md-3">
            <p><b>Permits:</b> <span id="total-aircraft_permissions">{{ round($total['aircraft_permissions'],2) }}</span></p>
        </div>
        <div class="col-md-3">
            <p><b>Permanent Permits:</b> <span id="total-permanent_permissions">{{ round($total['permanent_permissions'],2) }}</span></p>
        </div>
        <div class="col-md-3">
            <p><b>Total:</b> <span id="total-total">{{ round($total['total'],2) }}</span></p>
        </div>
    </div>

    <div class="form-group col-md-6">
        <h4 class="form-section">Billing Status</h4>

        <div class="col-md-4">
            <select class="form-control select2" name="billing_status" id="billing_status">
                @foreach(App\Models\Billing::$states as $state)
                    @if($state != "Deleted")
                        <option value="{{$state}}"
                                {{(isset($operation) && $operation->billing_status == $state)? 'selected="selected"' : ''}}>
                            {{ $state }}
                        </option>
                    @endif
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <h4 class="form-section">Documents</h4>

        <div class="col-md-9">
            @include('vendor.backpack.crud.fields.operation_documents_view_only')
        </div>

    </div>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript">

    $(document).ready(function () {
        showMTOWBilling();
    });

    function showMTOWBilling() {
        jQuery(function ($) {
            var aircraft_registration = $("#operation-aircraft-id").text();

            $.ajax({
                url: '{{url('/admin/aircraft/info')}}' + '/' + aircraft_registration,
                type: 'GET',
                dataType: "json",
                success: function (data) {

                    if (data['mtow']) {
                        data1 = data;
                        $("#mtow").text(data['mtow']);
                    } else {
                        $("#mtow").text("");
                    }
                    if (data['mtow_type']) {
                        data1 = data;
                        $("#mtow_type").text(data['mtow_type']);
                    } else {
                        $("#mtow_type").text("");
                    }
                    if (data['aircraft_type']) {
                        $("#aircraft_type").text(data['aircraft_type']);
                    } else {
                        $("#aircraft_type").text("");
                    }

                },
            });
        });
    }


</script>





