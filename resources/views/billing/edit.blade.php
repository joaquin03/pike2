@extends('backpack::layout')

@section('after_styles')
    <link href="{{ asset('vendor/adminlte/plugins/select2/select2.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css"
          rel="stylesheet" type="text/css"/>

    <link rel="stylesheet"
          href="https://cdn.jsdelivr.net/bootstrap.datetimepicker/4.17.42/css/bootstrap-datetimepicker.min.css"/>

    <link rel="stylesheet" href="{{ asset('/js/handsontable.full.min.css') }}"/>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    @stack('styles_stack')

@endsection

@section('title')

@endsection

@section('header')
    <section class="content-header">
        <h1>Edit Billing</h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/admin/dashboard')}}">Admin</a></li>
            <li><a href="{{url('/admin/billing')}}" class="text-capitalize">Billing</a></li>
            <li class="active">Edit</li>
        </ol>

    </section>
@endsection()

@section('content')
    @include('crud::inc.grouped_errors')

    <div class="row">
        <div class="col-md-12">
            <div class="box">

                <div class="nav">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs pull-right">

                            <li class="edit" onclick="">
                                <a href="#tab_documents" data-toggle="tab">Documents</a>
                            </li>
                            <li class="active" class="edit" onclick="">
                                <a href="#tab_general" data-toggle="tab">General</a>
                            </li>
                        </ul>
                    <form action="{{route('crud.billing.update', ['operation'=>$operation->id])}}" method="POST">
                        @include('billing._fields')
                        <div class="box-footer">

                            <div id="saveActions" class="form-group">
                                <button type="submit" class="btn btn-success">
                                    <span class="fa fa-save" role="presentation" aria-hidden="true"></span>
                                    Save
                                </button>

                                <a href="{{url('/admin/billing')}}" class="btn btn-default"><span></span> &nbsp;Back</a>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="box-body">
                    @include('billing.partials.itinerary', ['itineraries'=>$operation->itineraries], ['operation'=>$operation])
                    @include('billing.partials.Permissions.permissions')
                </div>
            </div>
        </div>
    </div>
    @if($operation->quotation_id!=0)
        @include('quotation.quotation-modals.quotation-modal', ['operation'=>$operation, 'entity'=>'billing'])
    @endif


    <!-- Default bootstrap modal example -->
    <div class="modal fade" id="myModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('after_scripts')
    <script src="{{ asset('vendor/adminlte/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendor/adminlte/plugins/daterangepicker/moment.min.js') }}"></script>
    <script type="text/javascript"
            src="https://cdn.jsdelivr.net/bootstrap.datetimepicker/4.17.42/js/bootstrap-datetimepicker.min.js"></script>


    <script type="text/javascript"
            src="https://cdn.jsdelivr.net/bootstrap.datetimepicker/4.17.42/js/bootstrap-datetimepicker.min.js"></script>
    <script src="{{ asset('/js/handsontable.full.min.js') }}"></script>

    <script>
        $('i').click(function () {
            $(this).text(function (i, old) {
                if (this.classList.contains("fa-minus-square-o")) {
                    $(this).removeClass('fa-minus-square-o').addClass('fa-plus-square-o');
                } else {
                    $(this).removeClass('fa-plus-square-o').addClass('fa-minus-square-o');
                }
                $($(this).data('target')).toggle();
            });
        });

        createPermitsBill = function () {
            var items = $('.bill_permits:checked');
            var items2 = $('.bill_permanent_permits:checked');
            var permits = [];
            var permanentPermits = [];
            for (var i = 0; i < items.length; i++) {
                permits.push($(items[i]).val());
            }
            for (var i = 0; i < items2.length; i++) {
                permanentPermits.push($(items2[i]).val());
            }

            $.ajax({
                type: "POST",
                url: '/api/billing/{{$operation->id}}/create-bill',
                data: {
                    'permits': permits,
                    'permanentPermits': permanentPermits,
                },
            }).success(function (data) {
                $(location).attr('href', '/admin/bills/' + data.id + '/edit');
            });
        };



    </script>
    @stack('script_stack')
@endsection