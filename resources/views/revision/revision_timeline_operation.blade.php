<style>
    .override {padding-bottom:0px !important;
                padding-top:0px !important;}​
</style>
<ul class="timeline">
    @foreach($revisions as $revisionDate => $dateRevisions)
        <li class="time-label" data-date="{{ date('Y-m-d', strtotime($revisionDate)) }}">
      <span class="bg-red">
        {{ Date::parse($revisionDate)->format(config('backpack.base.default_date_format')) }}
      </span>
        </li>

        <li class="timeline-item-wrap">
            <i class="fa fa-calendar bg-default"></i>
            <div class="timeline-item">
                <span class="time"><i class="fa fa-clock-o"></i> {{ date('h:ia', strtotime($revisionDate)) }}</span>
                @foreach($dateRevisions as $history)
                    @if ($loop->first)
                        <h3 class="timeline-header" style="margin-bottom:15px">
                            {{ $history->userResponsible()?$history->userResponsible()->name:trans('backpack::crud.guest_user') }}
                        </h3>

                        <table class="table table-striped table-condensed">
                            <thead>
                            <tr>
                                <th scope="col">Field name</th>
                                <th scope="col">{{ ucfirst(trans('backpack::crud.from')) }}</th>
                                <th scope="col">{{ ucfirst(trans('backpack::crud.to')) }}</th>
                            </tr>
                            </thead>
                            <tbody>
                    @endif

                    @if($history->key == 'created_at' && !$history->old_value)
                        @else
                            <tr>
                                <td>{{ $history->fieldName()}}</td>
                                <td>{{ $history->oldValue() }}</td>
                                <td>{{ $history->newValue() }}</td>
                           </tr>
                        @endif

                    @if ($loop->last)

                            </tbody>
                        </table>

                    @endif
                @endforeach
            </div>
        </li>
    @endforeach
</ul>

@section('after_scripts')
    <script type="text/javascript">
        $.ajaxPrefilter(function(options, originalOptions, xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');

            if (token) {
                return xhr.setRequestHeader('X-XSRF-TOKEN', token);
            }
        });
        function onRestoreClick(e) {
            e.preventDefault();
            var entryId = $(e.target).attr('data-entry-id');
            var revisionId = $(e.target).attr('data-revision-id');
            $.ajax('{{ \Request::url().'/' }}' +  revisionId + '/restore', {
                method: 'POST',
                data: {
                    revision_id: revisionId
                },
                success: function(revisionTimeline) {
                    // Replace the revision list with the updated revision list
                    $('.timeline').replaceWith(revisionTimeline);

                    // Animate the new revision in (by sliding)
                    $('.timeline-item-wrap').first().addClass('fadein');
                    new PNotify({
                        text: '{{ trans('backpack::crud.revision_restored') }}',
                        type: 'success'
                    });
                }
            });
        }
    </script>
@endsection

@section('after_styles')
    {{-- Animations for new revisions after ajax calls --}}
    <style>
        .timeline-item-wrap.fadein {
            -webkit-animation: restore-fade-in 3s;
            animation: restore-fade-in 3s;
        }
        @-webkit-keyframes restore-fade-in {
            from {opacity: 0}
            to {opacity: 1}
        }
        @keyframes restore-fade-in {
            from {opacity: 0}
            to {opacity: 1}
        }
    </style>
@endsection
