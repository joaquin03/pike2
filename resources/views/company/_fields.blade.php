<h3 class="form-section">General</h3>
<div class="form-group @{{ companyNameError===true ? ' has-error' : '' }} @{{ companyNameError===false ? ' has-success' : '' }}">


    <div class="form-group">
        <label class="control-label col-md-3">Company Type</label>
        <div class="col-md-9">
            <div class="mt-checkbox-list">
                <label class="mt-checkbox">
                    <input type="checkbox"
                           v-model="company.is_client"
                           id="is_client"
                           value="1"
                    />client
                </label>
                <label class="mt-checkbox">
                    <input type="checkbox"
                           v-model="company.is_operator"
                           id="is_operator"
                           value="1"
                    />operator
                </label>
                <label class="mt-checkbox">
                    <input type="checkbox"
                           v-model="company.is_provider"
                           id="is_provider"
                           value="1"
                    />provider
                </label>

            </div>
        </div>
    </div>

    <label class="control-label col-md-3">Name *</label>
    <div class="col-md-9">
        <div class="input-icon right">
            <i v-show="companyNameError === true" class="fa fa-warning tooltips" data-original-title="There is a company with the same Name"></i>
            <i v-show="companyNameError === false" class="fa fa-check tooltips" data-original-title="The company name is unique"></i>

            <input v-model="company.name"
                   id="company-name"
                   class="form-control"
                   placeholder="Pike Handling"
                   required="required"
                    @blur="checkUniqueName"/>
        </div>


    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3">Country *</label>
    <div class="col-md-9">
        <select id="company-country" class="form-control" v-selecttwo="company.country" v-model="company.country">
            <option value="" disabled="disabled" selected="selected">Select the country</option>
            @foreach(\App\Models\Utilities\Country::all() as $country => $code)
                <option value="{{$code}}"
                        {{(isset($company) && $company->country == $code)? 'selected="selected"' : ''}}
                >{{$country}}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group @{{ companyWebsiteError===true ? ' has-error' : '' }} @{{ companyWebsiteError===false ? ' has-success' : '' }}">
    <label class="control-label col-md-3">Website</label>
    <div class="col-md-9">

        <div class="input-icon right">
            <i v-show="companyWebsiteError === true" class="fa fa-warning tooltips" data-original-title="There is a company with the same Website"></i>
            <i v-show="companyWebsiteError === false" class="fa fa-check tooltips" data-original-title="The Website is unique"></i>

            <input id="company-website"
                   v-model="company.website"
                   class="form-control"
                   placeholder="http://pikehandling.com"
                    @blur="checkUniqueWebsite"
            />
        </div>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3">Business</label>
    <div class="col-md-9">
        <input id="company-business"
               v-model="company.business"
               class="form-control"
               placeholder="FBO"
        />
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3">Payment Methods *</label>
    <div class="col-md-9">
        <div class="mt-checkbox-list">
            @foreach(\App\Models\Client::$paymentMethods as $paymentMethod)
                <label class="mt-checkbox">
                    <input type="checkbox"
                           value="{{$paymentMethod}}"
                           id="company-payment_methods"
                           v-model="company.payment_methods"
                    /> {{$paymentMethod}}
                </label>
            @endforeach
        </div>
    </div>
</div>



<div class="form-group">
    <label class="col-sm-3 control-label">Image</label>
    <div class="col-sm-5">
        <input id="file" type="file" class="form-control" v-el="fileInput">
        @if (isset($company) && isset($company->image))
            <small>Let it blank to keep current image</small>
        @endif
    </div>
    <div class="col-sm-4">
        @if (isset($company) && isset($company->image))
            Current image: <img id="logo-preview" src="/storage/{{$company->image}}" style="max-height: 100px"/>
        @endif
    </div>
</div>

<h3 class="form-section">Contact details</h3>
<div class="form-group" v-for="email in company.emails">
    <div class="col-md-3">
        <select class="form-control" v-model="email.subtype" title="email">
            <option value="laboral">Email (laboral)</option>
            <option value="personal">Email (personal)</option>
            <option value="others">Email (others)</option>
        </select>
    </div>
    <div class="col-md-8">
        <input v-model="email.data" class="form-control" placeholder="Email address"/>
    </div>
    <button class="col-md-1 btn btn-default" @click.prevent="removeEmail(email)">
        <i class="icon-trash"></i> Remove
    </button>
</div>
<div class="form-group">
    <label class="control-label col-md-3"></label>
    <div class="col-md-9">
        <a href="#" @click.prevent="addEmail">Add email address</a>
    </div>
</div>

<div class="form-group" v-for="phone in company.phones">
    <div class="col-md-3">
        <select class="form-control" v-model="phone.subtype" title="phone">
            <option value="laboral">Phone (laboral)</option>
            <option value="personal">Phone (personal)</option>
            <option value="others">Phone (others)</option>
        </select>
    </div>
    <div class="col-md-8">
        <input v-model="phone.data" class="form-control" placeholder="Phone number"/>
    </div>
    <button class="col-md-1 btn btn-default" @click.prevent="removePhone(phone)">
        <i class="icon-trash"></i> Remove
    </button>
</div>
<div class="form-group">
    <label class="control-label col-md-3"></label>
    <div class="col-md-9">
        <a href="#" @click.prevent="addPhone">Add phone number</a>
    </div>
</div>

<div class="form-group" v-for="address in company.addresses">
    <div class="col-md-3">
        <select class="form-control" v-model="address.subtype" title="address">
            <option value="laboral">Address (laboral)</option>
            <option value="personal">Address (personal)</option>
            <option value="others">Address (others)</option>
        </select>
    </div>
    <div class="col-md-8">
        <input v-model="address.data" class="form-control" placeholder="Address"/>
    </div>
    <button class="col-md-1 btn btn-default" @click.prevent="removeAddress(address)">
        <i class="icon-trash"></i> Remove
    </button>
</div>
<div class="form-group">
    <label class="control-label col-md-3"></label>
    <div class="col-md-9">
        <a href="#" @click.prevent="addAddress">Add address</a>
    </div>
</div>