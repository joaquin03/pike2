<div>
    {!! Form::open(array('url' => $crud->route.'/'.$entry->getKey(), 'method' => 'put', 'files'=>$crud->hasUploadFields('update', $entry->getKey()))) !!}
    <div class="tab-content row">
        @include('crud::form_content', ['fields' => $fields, 'action' => 'edit'])
    </div>

        <button
                type="submit" class="btn btn-success"> Save
        </button>


    <a href="{{ url(route('crud.company.show', ['company'=>$company->id])) }}#edit" class="btn btn-default"><span
                class="fa fa-ban"></span>
        &nbsp;{{ trans('backpack::crud.cancel') }}</a>

    {!! Form::close() !!}
</div>
