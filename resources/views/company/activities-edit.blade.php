@extends('layouts.app')

@section('title')
    Client: {{ $client->name }} - Edit {{ucfirst($eventName)}}
@endsection

@section('content')
    <div id="app">

        @include('partials.vue.ajax_errors')

        <div class="col-md-12">
            <form @submit.prevent="update" class="form-horizontal form-row-seperated">
                <div class="portlet">
                    <div class="portlet">
                        <div class="portlet-title">

                            <div class="actions btn-set">
                                <a href="{{ route('client.show', ['client'=>$client]) }}" type="button" name="back" class="btn default">
                                    <i class="fa fa-angle-left"></i> Back</a>
                                <a href="{{ Request::url() }}" class="btn default">
                                    <i class="fa fa-reply"></i> Reset</a>
                                <button type="submit" class="btn green">
                                    <i class="fa fa-check"></i> Save
                                </button>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="tabbable">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#tab_general" data-toggle="tab">
                                            {{ucfirst($eventName)}} </a>
                                    </li>
                                </ul>

                                <div class="tab-content no-space">
                                    <div class="tab-pane active" id="tab_general">
                                        <div class="form-body">

                                            @include('partials.activities.fields.'.$eventName)

                                            <div class="row">
                                                <!-- Contacts -->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="contacts" class="col-md-2 control-label">Contacts</label>
                                                        <div class="col-md-10">

                                                            <select name="contacts" v-select="contacts" multiple
                                                                    v-model="activity.contacts" class="form-control select2">
                                                                @foreach($contacts as $contact)
                                                                    <option value="{{$contact->id}}">{{$contact->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection


@section('scripts')
    <script>

        $('.timepicker2').timepicker({'showMeridian': false});
        $('.select2').select2({
            theme: "bootstrap"
        });
        Vue.http.headers.common['X-CSRF-TOKEN'] = '{{ csrf_token() }}';

        Vue.transition('fade', {
            css: false,
            enter: function (el, done) {
                $(el).css('opacity', 0).animate({opacity: 1}, 1000, done)
            },
            enterCancelled: function (el) {
                $(el).stop()
            },
            leave: function (el, done) {
                $(el).animate({opacity: 0}, 1000, done)
            },
            leaveCancelled: function (el) {
                $(el).stop()
            }
        });
        Vue.directive('select', {
            twoWay: true,
            priority: 1000,

            params: ['options'],

            bind: function () {
                var self = this;
                $(this.el)
                        .select2({
                            data: this.params.options
                        })
                        .on('change', function () {
                            self.set($(self.el).val())
                        })
            },
            update: function (value) {
                $(this.el).val(value).trigger('change')
            },
            unbind: function () {
                $(this.el).off().select2('destroy')
            }
        });

        new Vue({
            el: "#app",

            data: {
                activity: {{$activity->event}},
                contacts: <?=json_encode($activity->getContactsIds())?>,

                @include('partials.vue.ajax_data')
            },

            methods: {
                update: function (event) {
                    var button = this.ajaxStart(event);
                    this.activity.contacts = this.contacts;
                    this.$http.put(
                            '{{route('client.activity.update', ['activity'=>$activity, 'client'=>$client])}}',
                            this.activity
                    ).then(function () {
                        this.ajaxBack(button, 'Activity updated succesfully');
                    }.bind(this));
                },

                addContact: function () {
                    this.client.emails.push({
                        "subtype": "laboral",
                        "data": ""
                    });
                },

                removeContact: function (email) {
                    var index = this.client.emails.indexOf(email);
                    if (index > -1) {
                        this.client.emails.splice(index, 1);
                    }
                },

                @include('partials.vue.ajax_methods')

            }
        });
    </script>
@endsection
