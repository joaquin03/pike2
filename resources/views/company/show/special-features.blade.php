<div class="tab-content row">
    <div class="col-md-12">
        <div class="box-body" style="border-top: 1px solid #f4f4f4;">
            <ul class="products-list product-list-in-box">
                @foreach($activitiesSpecialFeatures as $activity)
                    <li class="item {{$activity->event->getSlugClassName()}}">
                        <div class="product-img">
                            <i class="fa {{ $activity->event->getIcon() }} {{ $activity->event->getColor() }}"
                               style="padding: 16px;font-size: 18px"></i>
                        </div>
                        <div class="product-info">
                            <a href="{{route('crud.'.$activity->event->getSlugClassName().'.edit',
                                    ['company_id'=>$company, 'event_id'=> $activity->event_id])}}" class="product-title">
                                {{ $activity->event->getTitle() }} - {{ $activity->author->name }}

                                <span class="time pull-right"><i class="fa fa-clock-o"></i> {{ $activity->getDate() }}</span>
                            </a>
                            <span class="product-description ">
                                      {{ $activity->event->getDescription() }}
                                </span>
                        </div>
                        <div class="product-footer  pull-right">
                            <a href="{{route('crud.'.$activity->event->getSlugClassName().'.edit', ['company_id'=>$company, 'event_id'=> $activity->event_id])}}"
                               class="btn btn-xs btn-default"> <i class="fa fa fa-pencil-square-o"></i> Edit</a>
                            <a href="{{route('crud.'.$activity->event->getSlugClassName().'.destroy', ['company_id'=>$company, 'event_id'=> $activity->event_id])}}"
                               method="DELETE" class="btn btn-xs btn-danger" data-button-type="delete">
                                <i class="fa fa-trash"></i> Delete
                            </a>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>

