<!-- Profile Image -->
<div class="box box-primary">
    <div class="box-body box-profile">
        @if ($company->image)
            <img src="/storage/{{$company->image}}" class="profile-user-img img-responsive img-circle"
                 style="max-width: 75%;width: auto;height: auto"/>
        @else
            <img src="http://www.joniskelis.lt/wp-content/themes/annina-pro/images/no-image-box.png"
                 class="profile-user-img img-responsive img-circle"
                 style="max-width: 75%;width: auto;height: auto;border-radius: 10px !important;"/>
        @endif

        <h3 class="profile-username text-center"> {{ $company->name }} - {{ $company->getCountry() }}</h3>

        <div class="row text-muted text-center" style="margin-bottom: 15px">
            @if($company->is_client)
                <span class="col-md-4">CLIENT</span>
            @endif
            @if($company->is_operator)
                <span class="col-md-4">OPERATOR</span>
            @endif
            @if($company->is_provider)
                <span class="col-md-4">PROVIDER</span>
            @endif
        </div>
        @if($company->branchCompany != null)
            <div class="row text-muted text-center" style="margin-bottom: 10px;font-weight: bolder;">
                Branch: {{ $company->branchCompany->name }}
            </div>
        @endif

        <a href="#tab_edit" data-toggle="tab" onclick="changeTab()"
           class="btn btn-primary btn-block"><b>Edit</b></a>

    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->

<!-- About Me Box -->
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Details</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">

        @if($company->has_special_features)
            <p class="text-muted  text-center" style="font-weight: bolder;">Company with special features</p>
        @endif

        @if($company->type_of_client!=null && $company->is_client)

            @if($company->type_of_client == 'Bad')
                <div style="color: red " class="text-muted">
                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                    {{  \App\Models\Company::$typeOfClient[$company->type_of_client] }}
                </div>
            @endif

            @if($company->type_of_client == 'Medium')
                <div style="color: orange " class="text-muted">
                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                    {{  \App\Models\Company::$typeOfClient[$company->type_of_client] }}
                </div>
            @endif

            @if($company->type_of_client == 'Good')
                <div style="color: green " class="text-muted">
                    <i class="fa fa-check-square-o" aria-hidden="true"></i>
                    {{  \App\Models\Company::$typeOfClient[$company->type_of_client] }}
                </div>
            @endif

        @endif

        @if($company->salesManager)
            <div class="text-muted">
                <i class="fa fa-suitcase"></i>
                Sales Manager: {{$company->salesManager->name}}
            </div>
        @endif
        @if($company->emails!=null)
            @foreach($company->emails as $detail)
                <div class="text-muted">
                    <i class="fa fa-envelope"></i>
                    {{ $detail['comment'] ?? ''}} ({{ $detail['email'] ?? ''}})
                </div>
            @endforeach
        @endif

        @if($company->address!=null)
            @foreach($company->address as $detail)
                <div class="text-muted">
                    <i class="fa fa-map-marker"></i>
                    {{ $detail['city'] ?? '' }}, {{ $detail['country'] ?? '' }} ({{ $detail['address'] ?? '' }})
                    - {{ $detail['zip_code'] ?? '' }}
                </div>
            @endforeach
        @endif

        @if($company->phones!=null)

            @foreach($company->phones as $detail)
                <div class="text-muted">
                    <i class="fa fa-phone"></i>
                    {{ $detail['comment'] ?? ''}} ({{ $detail['phone'] ?? ''}})
                </div>
            @endforeach
        @endif
        <hr>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->

@push('stack_after_scripts')
    <script>
        function changeTab() {
            $('.activities').removeClass('active');
            $('.edit').addClass('active');
        }
    </script>
@endpush
