
<div class="tab-content row">
    <div class="col-md-12">
        @if($company->is_client)
        @php $clientOperations = $company->clientOperations; @endphp

        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Client Operations</h3>


                <div class="box-tools pull-right">
                    <small class="label label-default"># {{ $clientOperations->count() }} Operations</small>
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    @include('company.partials.operationTable', ['operations'=>$clientOperations])
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <!-- /.box-footer -->
        </div>
        @endif


    </div>
</div>