<head>
    @yield('scripts')
</head>

<div class="tab-content row">
    <div class="col-md-12">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Competitors</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                    </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="col-md-10">
                    <select class="form-control select2" name="competitor_id" id="competitor">
                        <option value="" disabled selected>---Select a Competitor---</option>

                        @foreach(App\Models\Provider::all() as $provider)
                            @if($provider->id != $company->id && !in_array($provider->id, $company->competitors->pluck('id')->toArray()))
                                <option value="{{$provider->id}}">
                                    {{$provider->name}}
                                </option>
                            @endif
                        @endforeach

                    </select>
                </div>

                <div class="col-md-2">
                    <label for=""></label>
                    <button id="add-competitor-button" type="button" class="btn btn-success"
                            onClick="addCompetitor()">
                        Add Competitor
                    </button>
                    <!-- /.table-responsive -->
                </div>
                <div class="col-md-12">

                    <div class="table-responsive" style="margin-top: 20px">
                        <table class="table no-margin" id="competitors-table">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Country</th>
                                <th>Business</th>
                                <th></th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($company->competitors as $competitor)
                                @if($competitor->id != $company->id )
                                    <tr id="competitor-{{$competitor->id}}">
                                        <td>{{ $competitor->name }}</td>
                                        <td>{{ $competitor->getCountry() }}</td>
                                        <td>{{ $competitor->business }}</td>
                                        <td>
                                            <button href=""
                                                    onclick="deleteCompetitor({{$competitor->id}})"
                                                    class="btn btn-sd btn-danger"
                                                    style="padding: 3px 7px;font-size: 10px;margin: 2px;"><i
                                                        class="fa fa-trash-o" style="padding: 0 ;"></i></button>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <!-- /.box-footer -->
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
            $('#competitor').select2();
    });

    function addCompetitor() {
        var competitor_id = $("#competitor :selected").val();
        if (competitor_id === '') {
            alert('Please select a competitor');
        } else {
            $.ajax({
                type: "POST",
                url: '{{url("/admin/company/")}}' + '/' + {{$company->id}} +'/add-competitor/' + competitor_id,
            }).success(function (data) {
                $('#competitors-table tr:last').after('<tr id="competitor-' + competitor_id + '"><td>' + data['name'] + '</td><td>' + data['country'] + '</td><td>' + data['business'] + '</td>' +
                    '</td><td><button href="" onclick="deleteCompetitor(' + competitor_id + ')" class="btn btn-sd btn-danger" style="padding: 3px 7px;font-size: 10px;margin: 2px;"><i class="fa fa-trash-o" style="padding: 0;"></i></button></td></tr>');
            })
                .error(function (data) {
                    alert('There has been an error, please contact your provider.')
                });
        }
    }

    function deleteCompetitor(competitor_id) {
        if (confirm('Are you sure you want to delete this competitor?')) {
            $.ajax({
                type: "DELETE",
                url: '{{url("/admin/company/")}}' + '/' + {{$company->id}} +'/delete-competitor/' + competitor_id,
            }).success(function (data) {
                $('#competitor-' + competitor_id).remove();
            })
                .error(function (data) {
                    alert('There has been an error, please contact your provider.')
                });
        }
    }


</script>
<style>
    .select2 {
        width: 100% !important;
    }
</style>