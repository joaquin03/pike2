
@php $quotations = $company->quotations; @endphp
<div class="tab-content row">
    <div class="col-md-12">
        <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Client Quotations</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table no-margin">
                            <thead>
                            <tr>
                                <th>Operation</th>
                                <th>Date</th>
                                <th>Aircraft / Type</th>
                                <th>Route</th>
                                <th>In Charge</th>
                                <th>Operator / Client</th>
                                <th>Status</th>
                                <th>Quote pdf</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($quotations as $operation)
                                <tr>
                                    <td><a href="{{route('crud.quotation.edit', ['operation_id'=> $operation->id])}}"
                                           target="_blank">{{$operation->code}}</a></td>
                                    <td>{{ $operation->getStartDateForList() }}</td>
                                    <td>{{ $operation->getOperatorName() }} / {{ $operation->getAircraftClientName() }}</td>
                                    <td>{!! $operation->operationAirports()  !!} </td>
                                    <td>{{ $operation->user->name }}</td>
                                    <td>{{ $operation->getOperatorName() }} / {{ $operation->aircraftClient->name }}</td>
                                    <td>{!! $operation->getHtmlQuotationStatus($operation->status)  !!} </td>
                                    <td><a target="blank" href="{{"http://$_SERVER[HTTP_HOST]/admin/quotation/".$operation->id.'/document-show'}}">
                                            @if($operation->extraData->quotation_exportable_file) Download  @endif
                                        </a></td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.box-body -->
                <!-- /.box-footer -->
            </div>
    </div>
</div>
