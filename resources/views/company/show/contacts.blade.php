<div class="box-header with-border col-md-3">
    <a class="btn btn-primary btn-block"  href={{route('crud.contact.create')}} data-toggle="tooltip" >
        <i class="fa fa-plus" ></i>Add Contact</a>
</div>
@if($contacts->isEmpty())
    <div style="text-align: center;line-height: 50px;">No contacts</div>
@else
    <table class="table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Role</th>
            <th style="width: 140px;">Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($contacts as $contact)
            <tr>
                <td>{{ $contact->id }}</td>
                <td><a  href="/admin/contact/{{$contact->id}}/edit" target="_blank">{{ $contact->name . ' ' . $contact->surname }}</a></td>
                <td>{{ $contact->role }}</td>
                <td style="text-align: center;">
                    <!--<a href="{}}//route('contact.edit', ['id' => $contact->id])}}" class="btn btn-circle btn-xs">
                        <i class="fa fa-edit" style="font-size: 12px"></i>
                    </a>-->
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif
