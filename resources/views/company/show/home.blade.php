

<div class="tab-content row">
        <div class="col-md-6">
            <div class="info-box">
                <span class="info-box-icon bg-green"><i class="fa fa-globe"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text" style="font-weight: 600;">OPERATIONS CLIENT</span>
                    <span class="info-box-text">
                        In Process <span class="pull-right badge bg-blue">{{$company->clientOperations()->inProcess()->count()}}</span>
                    </span>
                    <span class="info-box-text">
                        Completed <span class="pull-right badge bg-green">{{$company->clientOperations()->completed()->count()}}</span>
                    </span>
                    <span class="info-box-text">
                        Canceled <span class="pull-right badge bg-red">{{$company->clientOperations()->canceled()->count()}}</span>
                    </span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-expeditedssl"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text" style="font-weight: 600;">QUOTATIONS</span>
                    <span class="info-box-text">
                        CONFIRMED <span class="pull-right badge bg-green">{{$company->quotations()->confirmed()->count()}}</span>
                    </span>
                    <span class="info-box-text">
                        BUDGET <span class="pull-right badge bg-fuchsia">{{$company->quotations()->budget()->count()}}</span>
                    </span>
                    <span class="info-box-text">
                        CANCELED/LOST <span class="pull-right badge bg-red">{{$company->quotations()->canceled()->count()}}</span>
                    </span>
                </div>
                <!-- /.info-box-content -->
            </div>


            {{-- SPECIAL FEATURES --}}
            @if($company->hasActivitiesSpecialFeatures())
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Last Special Features</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <ul class="products-list product-list-in-box">
                            @foreach($activitiesSpecialFeatures->take(2) as $activity)
                                <li class="item">
                                    <div class="product-img">
                                        <i class="fa {{ $activity->event->getIcon() }} {{ $activity->event->getColor() }}"
                                           style="padding: 16px;font-size: 18px"></i>
                                    </div>
                                    <div class="product-info">
                                        <a href="{{route('crud.'.$activity->event->getSlugClassName().'.edit',
                                            ['company_id'=>$company, 'event_id'=> $activity->event_id])}}" class="product-title">
                                            {{ $activity->event->getTitle() }}

                                            <span class="time pull-right"><i class="fa fa-clock-o"></i> {{ $activity->getDate() }}</span>
                                        </a>
                                        <span class="product-description">
                                              {{ str_limit($activity->event->getDescription(), 230) }}
                                            </span>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-center">
                        <a href="#tab_special_features" onclick="changeTab()"  data-toggle="tab" class="uppercase">View All Special Features</a>
                    </div>
                    <!-- /.box-footer -->
                </div>
            @endif
        </div>

        {{-- LAST ACTIVITES --}}
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Last Activities</h3>
                    <span id="add-activity">
                         @include('company.activity-combo')
                    </span>


                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <ul class="products-list product-list-in-box">
                        @foreach($activities->take(5) as $activity)
                            <li class="item">
                                <div class="product-img">
                                    <i class="fa {{ $activity->event->getIcon() }} {{ $activity->event->getColor() }}"
                                       style="padding: 16px;font-size: 18px"></i>
                                </div>
                                <div class="product-info">
                                    <a href="{{route('crud.'.$activity->event->getSlugClassName().'.edit',
                                    ['company_id'=>$company, 'event_id'=> $activity->event_id])}}" class="product-title">
                                        {{ $activity->event->getTitle() }}

                                        <span class="time pull-right"><i class="fa fa-clock-o"></i> {{ $activity->getDate() }}</span>
                                    </a>
                                    <span class="product-description">
                                      {{ str_limit($activity->event->getDescription(), 159) }}
                                    </span>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <!-- /.box-body -->

                <div class="box-footer text-center">
                    @if($activities->count() == 0)
                        <h4>No activities were found</h4>
                    @else
                        <a href="#tab_activities" onclick="changeTab()" data-toggle="tab">View All Activities</a>
                    @endif
                </div>
                <!-- /.box-footer -->
            </div>
        </div>

        {{-- CONTACTS --}}
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Contacts</h3>
                    <a class="btn btn-primary btn-block pull-right" style="width: 120px;padding: 5px;" target="_blank"
                       href={{route('crud.contact.create', ['company_id'=>$company->id])}} data-toggle="tooltip" >
                        <i class="fa fa-plus" ></i> Add</a>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table no-margin">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Role</th>
                                    <th>Main Email</th>
                                    <th>Main Phone</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($contacts as $contact)
                                <tr>
                                    <td><a href="/admin/contact/{{$contact->id}}/edit" target="_blank">
                                            {{ $contact->name . ' ' . $contact->surname }}</a></td>
                                    <td>{{ $contact->role }}</td>
                                    <td>{{ $contact->getMainEmail() }}</td>
                                    <td>{{ $contact->getMainPhone() }}</td>
                                </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer text-center">
                    @if($contacts->count() == 0)
                        <h4>No contacts were found</h4>
                    @endif

                </div>
                <!-- /.box-footer -->
            </div>
        </div>
</div>