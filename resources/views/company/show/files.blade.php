<div class="tab-content row">
    <div class="col-md-12">
        <div class="box-body" style="border-top: 1px solid #f4f4f4;">
            <ul class="products-list product-list-in-box">
                @foreach($activitiesFile??[] as $activity)
                    <li class="timeline-item-container {{$activity->event->getSlugClassName()}}">

                        <div class="product-img">
                            <i class="fa {{ $activity->event->getIcon() }} {{ $activity->event->getColor() }}"
                               style="padding: 16px;font-size: 18px"></i>
                        </div>

                        <div class="product-info">
                            <a href="{{route('crud.'.$activity->event->getSlugClassName().'.edit', ['company_id'=>$company, 'event_id'=> $activity->event_id])}}">
                                {{ $activity->event->getTitle() }} - {{ $activity->author->name }}

                                <span class="time pull-right"><i
                                            class="fa fa-clock-o"></i> {{ $activity->getDate() }}</span>
                            </a>
                            <span class="product-description ">
                                @if($description = $activity->event->getDescription())
                                    <div class="timeline-body">
                                {!! $description !!}
                                    </div>
                                @endif
                            </span>
                        </div>
                        <div class="product-footer  pull-right">

                            <a href="{{route('crud.'.$activity->event->getSlugClassName().'.edit', ['company_id'=>$company, 'event_id'=> $activity->event_id])}}"
                               class="btn btn-xs btn-default"
                               style="color: black"
                            > <i class="fa fa fa-pencil-square-o"></i> Edit</a>
                            <a href="{{route('crud.'.$activity->event->getSlugClassName().'.destroy', ['company_id'=>$company, 'event_id'=> $activity->event_id])}}"
                               method="DELETE"
                               class="btn btn-xs btn-danger" data-button-type="delete"><i
                                        class="fa fa-trash"></i> Delete
                            </a>
                        </div>
                        <br><br>

                    </li>
                @endforeach


                <div class="box-footer text-center">
                    @if($activities->count() == 0)
                        <h4>No activities were found</h4>
                    @endif
                </div>

            </ul>
        </div>
    </div>
</div>


<div class="modal" id="showActivity" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true" style="display:none">
    <div class="modal-dialog">
        <div class="modal-content">

        </div>
    </div>
</div>