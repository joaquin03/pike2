@extends('layouts.app')

@section('title')
    Companies
@endsection

@section('content')
    <div class="portlet light">
        <table id="companies" class="table table-striped table-bordered table-hover datatable">
            <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Is Client</th>
                <th>Is Operator</th>
                <th>Is Provider</th>
            </tr>
            </thead>
            <tbody>
            @foreach($companies as $company)
                <tr>
                    <td>{{ $company->id }}</td>
                    <td>{{ $company->name }}</td>
                    <td>
                        <input type="checkbox"
                               title="is client"
                        {{ $company->is_client ? 'checked="checked"' : ''  }}
                        @change="updateCheckbox('is_client', {{$company->id}})"
                        /></td>
                    <td>
                        <input type="checkbox"
                               title="is operator"
                        {{ $company->is_operator ? 'checked="checked"' : ''  }}
                        @change="updateCheckbox('is_operator', {{$company->id}})"
                        /></td>
                    <td>
                        <input type="checkbox"
                               title="is provider"
                        {{ $company->is_provider ? 'checked="checked"' : ''  }}
                        @change="updateCheckbox('is_provider', {{$company->id}})"
                        /></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection

@section('scripts')
    <script>
        new Vue({
            el: "#companies",

            data: {
                @include('partials.vue.ajax_data')
            },

            methods: {
                updateCheckbox: function (field, id) {
                    var button = this.ajaxStart(event);

                    this.$http.put(`/companies/${id}`, {
                        toggleField: field
                    })
                            .catch(this.handleAjaxError.bind(this))
                            .finally(function () {
                                this.ajaxBack(button);
                            }.bind(this));
                },

                @include('partials.vue.ajax_methods')
            }
        });
    </script>
@endsection


