@extends('layouts.app')

@section('title')
    Create Company
@endsection

@section('content')
    <div id="app" class="portlet light">
        @include('partials.vue.ajax_errors')

        <form @submit.prevent="create" class="form-horizontal form">
            @include('company._fields')

            <div style="text-align: right;">
                <a href="{{ route( 'company.index') }}" class="btn">Cancel</a>
                <button type="submit" class="btn green"><i class="fa fa-check"></i> Create</button>
            </div>
        </form>
    </div>
@endsection

@section('scripts')
    <script>
        new Vue({
            el: "#app",

            data: {

                company: {
                    country: null,
                    emails: [],
                    phones: [],
                    addresses: [],
                    payment_methods: [],
                    is_client:0,
                    is_operator:0,
                    is_provider:0

                },

                companyNameError : null,
                companyWebsiteError: null,

                @include('partials.vue.ajax_data')
            },

            methods: {
                create: function (event) {
                    var button = this.ajaxStart(event);
                        this.$http.post('{{ route('company.store') }}', this.company)
                            .then(function (result) {

                                if ($('#file')[0].files.length) {
                                    var formData = new FormData();
                                    formData.append('image', $('#file')[0].files[0]);
                                    formData.append('_method', 'PUT');

                                    this.$http.post(result.body.id + '/image', formData)
                                        .then(function () {
                                            this.showAlert('Company created succesfully', 3000, function () {
                                                location.href = '/companies/' ;
                                            });
                                        }.bind(this))
                                } else {
                                    this.showAlert('Company created succesfully', 3000, function () {
                                        location.href = '/companies/';
                                    });
                                }

                            }.bind(this))
                            .catch(this.handleAjaxError.bind(this))
                            .finally(function () {
                                this.ajaxBack(button);
                            }.bind(this));

                },


                addEmail: function () {
                    this.company.emails.push({"subtype": "laboral", "data": ""});
                },

                addPhone: function () {
                    this.company.phones.push({"subtype": "laboral", "data": ""});
                },

                addAddress: function () {
                    this.company.addresses.push({"subtype": "laboral", "data": ""});
                },

                removeEmail: function (email) {
                    var index = this.company.emails.indexOf(email);
                    (index > -1) && this.company.emails.splice(index, 1);
                },

                removePhone: function (phone) {
                    var index = this.company.phones.indexOf(phone);
                    (index > -1) && this.company.phones.splice(index, 1);
                },

                removeAddress: function (address) {
                    var index = this.company.addresses.indexOf(address);
                    (index > -1) && this.company.addresses.splice(index, 1);
                },

                checkUniqueName: function() {
                    this.$http.get('{{route('api.company.checkUnique')}}'+'?field=name&value='+this.company.name)
                            .then(function (result) {
                                if(this.company.name != '') {
                                    this.companyNameError = (result.body.length>0);
                                } else {
                                    this.companyNameError = null;
                                }
                            })
                },


                @include('partials.vue.ajax_methods')
            }
        });
    </script>
@endsection