<table class="table no-margin">
    <thead>
    <tr>
        <th>Operation</th>
        <th width="101px">Date</th>
        <th>Aircraft / Type</th>
        <th>Route</th>
        <th>In Charge</th>
        <th>Operator / Client</th>
        <th>Status Op.</th>
        <th>Status Pr.</th>
        <th>Status Billing.</th>
    </tr>
    </thead>
    <tbody>
    @foreach($operations as $operation)
        <tr>
            <td><a href="{{route('crud.operation.edit', ['operation_id'=> $operation->id])}}"
                   target="_blank">{{$operation->code}}</a></td>
            <td>{{ $operation->getStartDateForList() }}</td>
            <td>{{ $operation->aircraft ? $operation->aircraft->registration : '' }} / {{ $operation->aircraft ? $operation->aircraft->type->name : ''}}</td>
            <td>{!! $operation->operationAirports()  !!} </td>
            <td>{{ $operation->user->name }}</td>
            <td>{{ $operation->getOperatorName() }} / {{ $operation->aircraftClient->name }}</td>
            <td>{!! $operation->getHtmlOperationStatus($operation->operation_status)  !!} </td>
            <td>{!! $operation->getHtmlProcurementStatus($operation->procurement_status)  !!} </td>
            <td>{!! $operation->getHtmlProcurementStatus($operation->billing_status) !!}</td>
        </tr>
    @endforeach
    </tbody>
</table>
