@extends('layouts.app')

@section('title')
    {{ ucfirst($type . 's') }}
@endsection

@section('content')
    <div class="portlet light">
        <table class="table table-striped table-bordered table-hover datatable">
            <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Country</th>
                <th>Website</th>
                <th>Business</th>
                <th>Latest activity</th>
                <th style="width: 100px !important;">Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($companies as $company)

                <tr>
                    <td>{{ $company->id }}</td>
                    <td>
                        <a href="{{route($company->getType() . '.show', ['id' => $company->id])}}">{{ $company->name }}</a>
                    </td>
                    <td>{{ \App\Http\Utilities\Country::getName($company->country) }}</td>
                    <td><a href="{{ $company->website }}" target="_blank">{{ $company->website }}</a></td>
                    <td>{{ $company->business }}</td>
                    <td>
                        @if (!empty($company->latestActivity))
                            {{$company->lastActivityDate() . ' '}}
                            <span class="small">({{array_values(array_slice(explode('\\', $company->latestActivity->event_type), -1))[0]}})</span>
                        @endif
                    </td>
                    <td style="text-align: center;">
                        <a href="{{route($company->getType() . '.show', ['id' => $company->id])}}" class="btn btn-circle btn-xs">
                            <i class="fa fa-info" style="font-size: 12px"></i>
                        </a>
                        <a href="{{route($company->getType() . '.edit', ['id' => $company->id])}}" class="btn btn-circle btn-xs">
                            <i class="fa fa-edit" style="font-size: 12px"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop


