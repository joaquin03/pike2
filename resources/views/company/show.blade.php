@extends('backpack::layout')

@section('scripts')
    <link href="{{ asset('assets/admin/pages/css/profile.css')}}" rel="stylesheet" type="text/css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>
@endsection

@section('title')
    Company : {{ $company->name }}
@endsection

@section('content')
    <section class="content-header">
        <h1>
            Company: {{$company->name}}
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/admin/dashboard')}}">Admin</a></li>
            <li><a href="{{url('/admin/company')}}" class="text-capitalize">Companies</a></li>
            <li class="active">Show</li>
        </ol>

    </section>
    <div class="content" id="crm-company">
        <div class="row">

            <div class="col-md-3">
                @include('company.show.contact-info')
            </div>

            @php $quotations = $company->quotations; @endphp


            <div class="col-md-9">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs pull-right">
                        <li class="edit" onclick="changeContentSize()">
                            <a href="#tab_edit" data-toggle="tab">Edit</a>
                        </li>
                        @if($company->getType() === 'client')
                            <li>
                                <a href="#tab_competitors" data-toggle="tab">Competitors</a>
                            </li>
                        @endif
                        <li>
                            <a href="#tab_quotations" data-toggle="tab">Quotations</a>
                        </li>
                        <li onclick="">
                            <a href="#tab_operations" data-toggle="tab">Operations</a>
                        </li>

                        <li onclick="">
                            <a href="#tab_files" data-toggle="tab">Files</a>
                        </li>
                        <li class="activities">
                            <a href="#tab_activities" data-toggle="tab">Activities</a>
                        </li>
                        <li onclick="">
                            <a href="#tab_special_features" data-toggle="tab">Special Features</a>
                        </li>
                        <li class="active" onclick="">
                            <a href="#tab_home" data-toggle="tab">Home</a>
                        </li>
                    </ul>
                    <div class="tab-content" style="">
                        <div class="tab-pane edit" id="tab_edit" style="padding: 10px;">
                            @include('company.edit')
                        </div>

                        <div class="tab-pane tab_activities" id="tab_activities"
                             style="padding: 10px;">
                            @include('company.activity-combo')
                            @include('company.show.activities')
                        </div>
                        <div class="tab-pane" id="tab_files" style="padding: 10px;">
                            @include('company.activity-combo')
                            @include('company.show.files')
                        </div>
                        <div class="tab-pane operations" id="tab_operations" style="padding: 10px;">
                            @include('company.show.operations')
                        </div>

                        <div class="tab-pane quotations" id="tab_quotations" style="padding: 10px;">
                            @include('company.show.quotations')
                        </div>

                        <div class="tab-pane competitors" id="tab_competitors" style="padding: 10px;">
                            @include('company.show.competitors')
                        </div>

                        <div class="tab-pane tab_special_features" id="tab_special_features" style="padding: 10px;">
                            @include('company.activity-combo')
                            @include('company.show.special-features')
                        </div>
                        <div class="tab-pane edit active" id="tab_home" style="padding: 10px;">
                            @include('company.show.home')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('stack_after_scripts')
    @yield('after_scripts')
    @include('partials.scripts.delete-scripts')

    <script>
        function changeContentSize() {
            $('.tab-content').resize();
        }
    </script>
@endpush