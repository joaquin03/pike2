<div class="create-activity-btn btn-group pull-right" style="">
    <button type="button"
            data-toggle="dropdown"
            class="btn bg-olive btn-flat margin btn-outline dropdown-toggle" style="margin-top: 1px;"
    >
        <i class="fa fa-angle-down"></i>Create Activity
    </button>

    <ul class="dropdown-menu pull-right" role="menu">
        <li>
            <a href="{{route('crud.activity-note.create', ['company_id'=>$company->id])}}">
                <i class="fa fa-sticky-note"></i> Note
            </a>
        </li>
        <li>
            <a href="{{route('crud.activity-email.create', ['company_id'=>$company->id])}}">
                <i class="fa fa-envelope"></i> Email
            </a>
        </li>
        <li>
            <a href="{{route('crud.activity-meeting.create', ['company_id'=>$company->id])}}">
                <i class="fa fa-users"></i> Meeting
            </a>
        </li>
        <li>
            <a href="{{route('crud.activity-call.create', ['company_id'=>$company->id])}}">
                <i class="fa fa-phone"></i> Call
            </a>
        </li>

        <li>
            <a href="{{route('crud.activity-reminder.create', ['company_id'=>$company->id])}}">
                <i class="fa fa-bell"></i> Reminder
            </a>
        </li>
        <hr/>
        <li>
            <a href="{{route('crud.activity-file.create', ['company_id'=>$company->id])}}">
                <i class="fa fa-link"></i> Files
            </a>
        </li>

        <li>
            <a href="{{route('crud.activity-special-features.create', ['company_id'=>$company->id])}}">
                <i class="fa fa-pencil-square-o"></i> Special Features
            </a>
        </li>

        @if(false)
        <li>
            <a href="{{route('crud.activity-call.create', ['activity' => 'ActivityCall', $company->getType() => $company])}}">
                <i class="icon-call-in"></i> Calls
            </a>
        </li>
        <li>
            <a href="{{route('activity.create', ['activity' => 'ActivityNote', $company->getType() => $company])}}">
                <i class="icon-note"></i> Note
            </a>
        </li>

        <hr/>

        <li>
            <a href="{{route('crud.activity-files.create', [$company->getType() => $company])}}">
                <i class="icon-paper-clip"></i> Files
            </a>
        </li>
        @endif

    </ul>

</div>