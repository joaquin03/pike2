@extends("backpack::layout")


@section('header')

    <section class="content-header">
        <h1 style="margin-bottom: 20px;">
            <span class="pull-left">
             {{ trans('backpack::base.dashboard') }}
            </span>

            <div class="col-md-4">
                <div style="color: #6d6d6d;font-size: 19px;margin-bottom: 21px;font-weight: normal;margin-top: 3px;">
                    <span class="col-md-4">Date Range:</span>
                    <span class="col-md-8">
                    <input type="text" name="daterange" class="form-control"
                           value="{{$request->date_start}} - {{$request->date_end}}" style="margin-top: -8px;"/>
                </span>

                </div>
            </div>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url(config('backpack.base.route_prefix', 'admin')) }}">{{ config('backpack.base.project_name') }}</a>
            </li>
            <li class="active">{{ trans('backpack::base.dashboard') }}</li>
        </ol>
    </section>
    <style>
        a {
            color: #A9A9A9
        }

        a:hover {
            color: #696969;
        }
    </style>

@endsection


@section('content')

    <div class="content row">
        <div class="col-md-12">

        </div>
        <div class="col-lg-4 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3>@money($profitsTotal)</h3>
                    <h4>Profit</h4>
                </div>
                <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h3>@money($expensesTotal)</h3>
                    <h4>Expenses</h4>
                </div>
                <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>@money($total)</h3>
                    <h4>Total</h4>
                </div>
                <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Profit Chart</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="chart">
                        <canvas class="col-lg-12" id="totalByMonth" style="height: 400px"></canvas>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>

@endsection


@section('after_styles')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@endsection

@section('after_scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>

    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>




    <script>
        jQuery(document).ready(function ($) {
            var ctx = document.getElementById('totalByMonth').getContext('2d');
            var chart = new Chart(ctx, {
                // The type of chart we want to create
                type: 'line',

                // The data for our dataset
                data: {
                    labels: {!! json_encode($totalByMonth['months'])!!},
                    datasets: [{
                        label: "Expenses",
                        borderColor: '#00c0ef',
                        data: {!! json_encode($totalByMonth['expenses'])!!},
                    },
                    {
                        label: "Profits",

                        borderColor: '#dd4b39',
                        data: {!! json_encode($totalByMonth['profits'])!!},
                    },
                    {
                        label: "Total",

                        borderColor: '#00a65a',
                        data: {!! json_encode($totalByMonth['total'])!!},
                    }
                    ]
                },

                // Configuration options go here
                options: {}
            });

            $('input[name="daterange"]').daterangepicker({
                opens: 'left',
                ranges: {
                    'This Month': [moment().subtract(1, 'month').startOf('month'), moment().endOf('month')],
                    'Last 3 Month': [moment().subtract(3, 'month'), moment()],
                    'Last 6 Month': [moment().subtract(6, 'month'), moment()],
                    'This Year': [moment().startOf('year'), moment().endOf('year')],
                    'Last Year': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')]
                },
                locale: {
                    format: 'DD/MM/YYYY'
                }
            }, function(start, end, label) {
                window.location.href = "/admin/reports/total?date_start="+start.format('DD/MM/YYYY')+"&date_end="+end.format('DD/MM/YYYY');
                //console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
            });

            });
    </script>

        });

    </script>
@endsection