@extends("backpack::layout")
@section('header')
    <section class="content-header">

        <h1>
            Quotation Data Generation
        </h1>
        <h4><i> Please insert the itinerary values in order to auto generate the services.</i></h4>


    </section>
    <style>
        a {
            color: #A9A9A9
        }

        a:hover {
            color: #696969;
        }
        input{
            border: 0;
            width: 200px !important;
            height: 30px;

        }
    </style>

@endsection
@section('content')

    @yield('variables')
    @php
        $ssID = '1hoLOnOMcehR7Kj932J5UqOyMiIlbsjKTvfmRtd0inCE';


        $apiKey = 'AIzaSyBJIjRLC7T4QFv71B_GZ8rz3MiaS3dc40I';
        $client_id = '901856530835-4ou6vlrpurkqq2461q7q06vljb8rke47.apps.googleusercontent.com';
        $scope = 'https://www.googleapis.com/auth/spreadsheets';
    @endphp


    <script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>

    <!--
    BEFORE RUNNING:
    ---------------
    1. If not already done, enable the Google Sheets API
       and check the quota for your project at
       https://console.developers.google.com/apis/api/sheets
    2. Get access keys for your application. See
       https://developers.google.com/api-client-library/javascript/start/start-js#get-access-keys-for-your-application
    3. For additional information on authentication, see
       https://developers.google.com/sheets/api/quickstart/js#step_2_set_up_the_sample
    -->


    @include('quotation.sheetIntegration.scripts')
    <div class="row">
        <div class="col-md-12">
            <div class="box" style="padding: 30px; ">

                <h3 style="margin-top: 0px !important;">{{$itinerary->getCountryName()}}</h3>

                <div class="col-md-3">
                    <div class="col-md-6">
                        <label for="operation_data">Operation Info</label>
                    </div>
                    <div class="col-md-6">

                        <select id='operation_data' class="form-control col-md-6">
                            <option value="Handling">Handling</option>
                            <option value="Flight Support">Flight Support</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-12" style="text-align: center; margin: 15px">
                    <?php
                    //inputs table striped, with first column disabled
                    for ($row = $readConfigRowStart; $row < $readConfigRowEnd; $row++) {
                        if ($row % 2 == 0) {
                            echo "<div style = 'clear:both;background-color: #edfdff'>";
                        } else {
                            echo "<div style = 'clear:both;background-color: #e0f7fa'>";
                        }

                        for ($col = $readConfigColStart; $col < $readConfigColEnd; $col++) {
                            if ($row % 2 == 0) {
                                if ($col == 1) {
                                    echo "<input type='text' style='float:left;background-color: #edfdff; pa' name='$row:$col' id='$row:$col' disabled>";
                                } else {
                                    echo "<input type='text' style='float:left;background-color: #edfdff' name='$row:$col' id='$row:$col' >";
                                }
                            }
                            else{
                                if ($col == 1) {
                                    echo "<input type='text' style='float:left;background-color: #e0f7fa' name='$row:$col' id='$row:$col' disabled>";
                                } else {
                                    echo "<input type='text' style='float:left;background-color: #e0f7fa' name='$row:$col' id='$row:$col' >";
                                }
                            }
                        }
                        echo "</div>";

                    }

                    ?>

                </div>
                <br><br>

                <button id="update-button" class="btn btn-warning" onclick="handleSaveClick()">Save Data</button>
                <button id="generate-button" class="btn bg-maroon" onclick="readSheetAndGenerate()">Generate Services
                </button>
            </div>
        </div>
    </div>


@endsection
