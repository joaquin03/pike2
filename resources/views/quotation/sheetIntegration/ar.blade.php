@extends('quotation.sheetIntegration.base')
@section('variables')
    @php
        $readConfigRowStart = 3;
        $readConfigRowEnd = 22;
        $readConfigColStart = 1;
        $readConfigColEnd = 3;

        $writeConfigRowStart = 3;
        $writeConfigRowEnd = 22;
        $writeConfigColStart = 1;
        $writeConfigColEnd = 2;

        $range = 'ARGENTINA';
    @endphp
@endsection