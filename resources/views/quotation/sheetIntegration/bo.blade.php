@extends('quotation.sheetIntegration.base')
@section('variables')
    @php
        $readConfigRowStart = 3;
        $readConfigRowEnd = 18;
        $readConfigColStart = 1;
        $readConfigColEnd = 3;

        $writeConfigRowStart = 3;
        $writeConfigRowEnd = 19;
        $writeConfigColStart = 1;
        $writeConfigColEnd = 2;

        $range = 'BOLIVIA';
    @endphp
@endsection
