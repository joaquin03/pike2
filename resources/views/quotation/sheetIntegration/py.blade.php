@extends('quotation.sheetIntegration.base')
@section('variables')
    @php
        $readConfigRowStart = 3;
        $readConfigRowEnd = 15;
        $readConfigColStart = 1;
        $readConfigColEnd = 3;

        $writeConfigRowStart = 3;
        $writeConfigRowEnd = 16;
        $writeConfigColStart = 1;
        $writeConfigColEnd = 2;

        $range = 'PARAGUAY';
    @endphp
@endsection