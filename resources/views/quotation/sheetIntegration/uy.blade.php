@extends('quotation.sheetIntegration.base')
@section('variables')
    @php
        $readConfigRowStart = 4;
        $readConfigRowEnd   = 16;
        $readConfigColStart = 1;
        $readConfigColEnd   = 3;

        $writeConfigRowStart = 4;
        $writeConfigRowEnd   = 19;
        $writeConfigColStart = 1;
        $writeConfigColEnd   = 2;

        $range = 'URUGUAY';
    @endphp
@endsection