<script>

    var ssID = '{{ $ssID  }}';
    var rng= '{{ $range }}';

    var readConfigRowStart    =  {{ $readConfigRowStart }};
    var readConfigRowEnd      =  {{ $readConfigRowEnd  }};
    var readConfigColStart    =  {{ $readConfigColStart }};
    var readConfigColEnd      =  {{ $readConfigColEnd  }};

    var writeConfigRowStart   =  {{ $writeConfigRowStart }};
    var writeConfigRowEnd     =  {{ $writeConfigRowEnd  }};
    var writeConfigColStart   =  {{ $writeConfigColStart }};
    var writeConfigColEnd     =  {{ $writeConfigColEnd }};

    var operationDataRow      = 2;
    var quoteProviderServiceNames = 4;
    var quoteProviderServiceAmount= 5;


    function readSheetApi() {
        var params = {
            // The ID of the spreadsheet to re
            // ieve data from.
            spreadsheetId: ssID,

            // The A1 notation of the values to retrieve.
            range: rng,
        };

        var request = gapi.client.sheets.spreadsheets.values.get(params);
        request.then(function(response) {
            // TODO: Change code below to process the `response` object:
            console.log(response.result);
            populateSheet(response.result);
        }, function(reason) {
            console.error('error: ' + reason.result.error.message);
        });
    }

    function updateSheetApi(){
        var vals = [];
        var col = operationDataRow;

        for(var row = 0; row < readConfigRowEnd ; row++){

            vals[row] = [];

            if(row == operationDataRow){
                vals[row][col] = document.getElementById('operation_data').value;
            }

            var aux = (document.getElementById(row+":"+col)) ;
            
            if(aux) {
                console.log(aux.value);
            }
            console.log('------------------------');


            if(aux){
                vals[row][col] = aux.value.replace('.','');;
            }
        }

        var params = {
            // The ID of the spreadsheet to retrieve data from.
            spreadsheetId: ssID,

            // The A1 notation of the values to retrieve.
            range: rng,

            valueInputOption: 'RAW',
        };

        var valueRangeBody = { "values" : vals };

        var request = gapi.client.sheets.spreadsheets.values.update(params, valueRangeBody);
        request.then(function(response) {
            // TODO: Change code below to process the `response` object:
            console.log(response.result);
        }, function(reason) {
            console.error('error: ' + reason.result.error.message);
        });

    }

    function initClient() {
        var API_KEY = '{{ $apiKey }}';

        var CLIENT_ID = '{{ $client_id }}';

        var SCOPE =  '{{ $scope }}';

        gapi.client.init({
            'apiKey': API_KEY,
            'clientId': CLIENT_ID,
            'scope': SCOPE,
            'discoveryDocs': ['https://sheets.googleapis.com/$discovery/rest?version=v4'],
        }).then(function() {
            gapi.auth2.getAuthInstance().isSignedIn.listen(updateSignInStatus);
            updateSignInStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
        });
    }

    function handleClientLoad() {
        gapi.load('client:auth2', initClient);
    }

    function updateSignInStatus(isSignedIn) {
        if (isSignedIn) {
            readSheetApi();
        }
    }


    function handleSignOutClick(event) {
        gapi.auth2.getAuthInstance().signOut();
    }

    function handleSaveClick() {
        updateSheetApi();
    }

    function populateSheet(result){
        for (var row = readConfigRowStart; row<readConfigRowEnd; row++){
            for (var col = readConfigColStart; col<readConfigColEnd; col++){
                document.getElementById(row+":"+col).value = result.values[row][col];
            }
        }
    }



    function readSheetAndGenerate() {
        var params = {
            // The ID of the spreadsheet to retrieve data from.
            spreadsheetId: ssID,

            // The A1 notation of the values to retrieve.
            range: rng,
        };

        var request = gapi.client.sheets.spreadsheets.values.get(params);
        request.then(function(response) {
            obtainServicesData(response.result);
        }, function(reason) {
            console.error('error: ' + reason.result.error.message);
        });
    }

    function obtainServicesData(data){
        var values = data.values;
        var services =[];
        for (var i = writeConfigRowStart ; i<writeConfigRowEnd ; i++) //'Tarifario' quote dimensions
        {
            var id = values[i][quoteProviderServiceNames].split('-')[0].trim();
            var amount =   values[i][quoteProviderServiceAmount].replace('.','');

            if ( _isNumeric(id) && validAmount(amount)){
                services.push({
                    'service_provider_id': id,
                    'amount': amount,
                    'service_origin': 'Quotation',
                })
            }
        }


        generateServices( {{$itinerary->id}}, services);
    }

    function _isNumeric(value){
        return !isNaN(parseFloat(value)) && isFinite(value);
    }

    function validAmount(value){
        return !(value=="N/A" || value=="TBA");
    }

    function generateServices(itineraryId, services) {
        $.post({
            url: '{{url("/api/itinerary/")}}' + '/' + itineraryId + '/generate-quotation-services/',
            data: {'services': services, },
        }).success(function (data) {
            url = "{{ url('admin/quotation')}}" + "/" + {{$itinerary->operation_id}} +'/edit/';
            window.location.href = url;
        })
            .error(function (data) {
                PNotify.removeAll();
                new PNotify({
                    text: data.responseJSON.message,
                    type: "error"
                });
            });
    };

    function setDataFromQuotation(){
        var MTOW = {{$itinerary->quotation->aircraft ? $itinerary->quotation->aircraft->type ? $itinerary->quotation->aircraft->type->MTOW : 0 : 0}};
        var MTOWType = '{{$itinerary->quotation->aircraft ? $itinerary->quotation->aircraft->type ? $itinerary->quotation->aircraft->type->MTOW_Type : 0 : 0}}' ;

        var PAX = '{{$itinerary->quotation->extraData->pax_in_out ?? 'Not Defined'}}';

        var parsedMTOW = convertMTOWToTons(MTOW, MTOWType)



        document.getElementById(readConfigRowStart+":"+operationDataRow).value = parsedMTOW ;
        document.getElementById((readConfigRowStart+1)+":"+operationDataRow).value= PAX;
    }

    function convertMTOWToTons(mtow, mtowType)
    {
        if(mtow) {
            if (mtowType == 'kg') {
                return (mtow * 0.00110231).toFixed(2).replace('.',',');
            }

            if (mtowType == 'libras') {
                return (mtow * 0.0005).toFixed(2).replace('.',',');
            }
        }
        return 0;
    }


    $( document ).ready(function() {
        setTimeout(function(){
            if(!gapi.auth2.getAuthInstance().isSignedIn.get()) {
                gapi.auth2.getAuthInstance().signIn();
            }
            setDataFromQuotation();

        }, 2000);
    });

</script>
<script async defer src="https://apis.google.com/js/api.js"
        onload="this.onload=function(){};handleClientLoad()"
        onreadystatechange="if (this.readyState === 'complete') this.onload()">
</script>

