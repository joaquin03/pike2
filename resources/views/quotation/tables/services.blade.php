<div class="row">
    <div class="col-md-12" style="margin-top: 30px;">
        <div id="itinerary_{{$itinerary->id}}">
        </div>
        @include('quotation.tables.servicesAdditionals')
    </div>


    <div class="itineraryNote col-md-6" style="margin-top: 10px">
        <h4>Notes</h4>
        <textarea id="note" style="width: 100%;min-height: 80px;" placeholder="" onkeyup="updateNotes()">{{$itinerary->quotation_notes}}</textarea>
    </div>

    <div class="providerServices col-md-6" style="margin-top: 10px">
        <h4>Adm Fee Info</h4>
        @include('quotation.tables.providersAdmFee', ['providers' => $providers])

    </div>
</div>
<script>
    var provider_changed = false;
    var providerServices = {!! json_encode($providers, true)  !!};
    var hot_tables_{{$itinerary->id}} = null;

    var columns = [
        {data: 'id', readOnly: true},
        {data: 'parent_name', type: 'text', readOnly: true},
        {data: 'service', type: 'text', readOnly: true},
        {data: 'provider_name', type: 'text', readOnly: true},
        {data: 'service_unit', type: 'text'},
        {data: 'procurement_quantity', type: 'numeric', format: '0.00'},
        {data: 'procurement_cost', type: 'numeric', format: '0.00'},
        {data: 'procurement_tax', type: 'numeric', format: '0.0%'},
        {data: 'procurement_adm_fee', type: 'numeric', format: '0.0%'},
        {data: 'final_cost', type: 'numeric', format: '0.000', readOnly: true},
        {data: 'administration_fee', type: 'numeric', format: '0.0%'},
        {data: 'billing_unit_price', type: 'numeric', format: '0.00'},
        {data: 'quotation_final_cost', type: 'numeric', format: '0.00', readOnly: true},
        {data: 'delete_service', renderer: deleteServiceRenderer, readOnly: true},
    ];


    var colHeaders = [
        'ID', 'Parent', 'Service', 'Provider', 'Unit', 'Quantity', 'Unit Cost', 'Tax', 'P. Adm. Fee', 'Final Cost', 'Adm Fee', 'Unit Price', 'TOTAL', ''];

    var colWidths = [
        2, 180, 150, 150, 150, 80, 80, 80, 100, 80, 80, 80, 80
    ];


    renderServices = function (providerServices) {
        var items = providerServices.data;
        var hotElement = document.querySelector('#itinerary_{{$itinerary->id}}');
        var hotSettings = {
            data: items,
            columns: columns,
            width: 1400,
            colWidths: colWidths,
            colHeaders: colHeaders,
            maxRows: items.length,
            cells: function (row, col, prop) {
                return editCustomCells(row, col, prop, items);
            },
            afterChange: function (changes, source) {
                if (source == 'edit') {
                    var item = this.getData()[changes[0][0]];
                    updateService(item);
                }
            },
            cells: function (row, col, prop) {
                var cellProperties = {};
                var data = items[row];
                if (data.children == null) {
                    cellProperties.readOnly = true;
                    cellProperties.renderer = function (hotInstance, td, row, col, prop, value) {
                        td.style.backgroundColor = '#e6f5f9';
                        td.textContent = value;

                    };
                    return cellProperties;
                }
            }
        };
        hot_tables_{{$itinerary->id}} = new Handsontable(hotElement, hotSettings);

    };


    function editCustomCells(row, col, prop, items) {
        var cellMeta = {};
        var data = items[row];
        if (col == 13) {
            cellMeta.renderer = function (hotInstance, td, row, col, prop, value) {
                deleteServiceRenderer(hotInstance, td, row, col, prop, value);
            }
        }

        return cellMeta;
    }

    function reloadItineraryServices(data) {

        var items = data.data;
        var table = hot_tables_{{$itinerary->id}};
        table.loadData(items);
        table.updateSettings({
            maxRows: items.length,
            cells: function (row, col, prop) {
                var cellProperties = {};
                var data = items[row];
                if (data.children == null) {
                    cellProperties.readOnly = true;
                    cellProperties.renderer = function (hotInstance, td, row, col, prop, value) {
                        td.style.backgroundColor = '#e6f5f9';
                        td.textContent = value;

                    };
                    return cellProperties;
                }
                return editCustomCells(row, col, prop, items);
            }
        })
    }

    function getItineraryProviderServices(itineraryId) {
        $.ajax({
            type: "GET",
            url: '/api/itinerary/' + itineraryId + '/service?in_quotation=1',
        }).success(function (data) {
            if (hot_tables_{{$itinerary->id}} == null) {
                renderServices(data);
            } else {
                reloadItineraryServices(data);
            }
        })
    }

    $(document).ready(function () {

        providerServices = getItineraryProviderServices({{$itinerary->id}});
    });

    $(".delete_service").on("click", function () {
        return confirm("Do you want to delete this service?");
    });


    function serviceStatusRenderer(instance, td, row, col, prop, value, cellProperties) {
        var instance2 = JSON.stringify(instance.getData()[row]);
        newHtml = "";
        if (value == 'Requested') {
            newHtml += "<button onclick='changeServiceStatus(" + instance2 + ")' class='status label  bg-gray' type='button' data-value='" + instance2 + " " +
                "'style='font-size:13px; color:white' data-status='Requested'>REQUESTED </button>";

        } else if (value == 'Canceled') {
            newHtml += "<button onclick='changeServiceStatus(" + instance2 + ")' class='status label bg-black' type='button' data-value='" + instance2 + " " +
                "'style='font-size:13px; color:white' data-status='Canceled'>CANCELED </button>";
        }
        td.innerHTML = newHtml;
    }

    function deleteServiceRenderer(instance, td, row, col, prop, value, cellProperties) {
        var instance2 = JSON.stringify(instance.getData()[row]);
        newHtml = "<button href='' onclick='deleteService(" + instance2 + ")' class ='btn btn-sd btn-danger' " +
            "style='padding: 3px 7px;font-size: 10px;margin: 2px;'> <i class='fa fa-trash-o'></i></button>";
        td.innerHTML = newHtml;
    }

    function deleteService(item) {
        if (confirm('Are you sure you want to delete this service?')) {
            removeItineraryServicesCount({{$itinerary->id}});

            $.ajax({
                type: "DELETE",
                url: '/api/itinerary/{{$itinerary->id}}/service/' + item[0],
            }).success(function (data) {
                getItineraryProviderServices({{$itinerary->id}});
                getItineraryproviderServicesAdditionals({{ $itinerary->id }});

            }).error(function (data) {
                return alert(data.responseJSON.message);
            });
        }
    }

    function changeServiceStatus(item) {
        var status = item[3];
        if (status == "Requested") {
            item[3] = "Canceled";
        } else {
            item[3] = "Requested";
        }
        updateService(item);
    }

    function updateService(item) {
        $.ajax({
            type: "PUT",
            url: '/api/itinerary/{{$itinerary->id}}/service/quotation/' + item[0],
            data: {
                'service_unit': item[4],
                'procurement_quantity': item[5],
                'procurement_cost': item[6],
                'procurement_tax': item[7],
                'procurement_adm_fee': item[8],
                'administration_fee': item [10],
                'billing_unit_price': item[11],

            },
        }).success(function (data) {
            getItineraryProviderServices({{ $itinerary->id }});
            getItineraryproviderServicesAdditionals({{ $itinerary->id }});
        }).error(function (data) {
            PNotify.removeAll();
            new PNotify({
                text: data.responseJSON.message,
                type: "error"
            });
        });
    }


    function updateNotes() {
        $.ajax({
            type: "PUT",
            url: '/api/itinerary/{{$itinerary->id}}',
            data: {
                'itinerary': [
                    {'quotation_notes': $("#note").val()}]
                ,

            },
        }).success(function (data) {
        });
    }

    function removeItineraryServicesCount(itineraryId) {
        console.log(parseInt($('#services-count-' + itineraryId).text()));
        if (parseInt($('#services-count-' + itineraryId).text()) === 1) {
            $('#services-count-' + itineraryId).hide();
            document.getElementById('itineraryWithServices-'+itineraryId).style.backgroundColor = "#f4f4f4";
            document.getElementById('itineraryWithServices-'+itineraryId).style.borderColor = "#f4f4f4";
            document.getElementById('truck-'+itineraryId).style.color = "black";

        }
        $('#services-count-' + itineraryId).text(parseInt($('#services-count-' + itineraryId).text()) - 1);
    }

</script>

