

<table id="adm-fees-table">
    <tr>
        <th>Provider</th>
        <th>Adm Fee</th>
    </tr>
    @foreach($providers as $provider)
        <tr>
            <td >{{$provider['name']}}</td>
            <td style="padding-left: 10px">{{$provider['adm_fee']}}</td>
        </tr>
    @endforeach
</table>

<script>

    $(document).ready(function() {

        var table = document.getElementById ("myTable");

        $("#add-service-button").on("click", function () {
            table.refresh ();
        });
    });

</script>