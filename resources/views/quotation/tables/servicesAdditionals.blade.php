<h3> Additionals </h3>
<div id="additional_{{$itinerary->id}}">
</div>

<script>
    var provider_changed = false;
    var providerServicesAdditionals = {!! json_encode($providers, true)  !!};
    var hot_tables_additional_{{$itinerary->id}} = null;

    var columns = [
        {data: 'id', readOnly: true},
        {data: 'parent_name', type: 'text', readOnly: true},
        {data: 'service', type: 'text', readOnly: true},
        {data: 'provider_name', type: 'text', readOnly: true},
        {data: 'service_unit', type: 'text'},
        {data: 'procurement_quantity', type: 'numeric', format: '0.00'},
        {data: 'procurement_cost', type: 'numeric', format: '0.00'},
        {data: 'procurement_tax', type: 'numeric', format: '0.0%'},
        {data: 'procurement_adm_fee', type: 'numeric', format: '0.0%'},
        {data: 'final_cost', type: 'numeric', format: '0.000', readOnly: true},
        {data: 'administration_fee', type: 'numeric', format: '0.0%'},
        {data: 'billing_unit_price', type: 'numeric', format: '0.00'},
        {data: 'quotation_final_cost', type: 'numeric', format: '0.00', readOnly: true},
        {data: 'delete_service', readOnly: true},
    ];


    var colHeaders = [
        'ID', 'Parent', 'Service', 'Provider', 'Unit', 'Quantity', 'Unit Cost', 'Tax', 'P. Adm. Fee', 'Final Cost', 'Adm Fee', 'Unit Price', 'TOTAL', ''];

    var colWidths = [
        2, 180, 150, 150, 150, 80, 80, 80, 100, 80, 80, 80, 80
    ];


    renderAdditionalServices = function (providerServicesAdditionals) {
        var items = providerServicesAdditionals.data;
        var hotElement = document.querySelector('#additional_{{$itinerary->id}}');
        var hotSettings = {
            data: items,
            columns: columns,
            width: 1400,
            colWidths: colWidths,
            colHeaders: colHeaders,
            maxRows: items.length,
            cells: function (row, col, prop) {
                return editCustomCellsAdditionals(row, col, prop, items);
            },
            afterChange: function (changes, source) {
                if (source == 'edit') {
                    var item = this.getData()[changes[0][0]];
                    updateService(item);
                }
            },
            cells: function (row, col, prop) {
                var cellProperties = {};
                var data = items[row];
                if (data.children == null) {
                    cellProperties.readOnly = true;
                    cellProperties.renderer = function (hotInstance, td, row, col, prop, value) {
                        td.style.backgroundColor = '#e6f5f9';
                        td.textContent = value;

                    };
                    return cellProperties;
                }
            }
        };
        hot_tables_additional_{{$itinerary->id}} = new Handsontable(hotElement, hotSettings);
    };


    function reloadItineraryAdditionalServices(data) {

        var items = data.data;
        var table = hot_tables_additional_{{$itinerary->id}};
        table.loadData(items);
        table.updateSettings({
            maxRows: items.length,
            cells: function (row, col, prop) {
                var cellProperties = {};
                var data = items[row];
                if (data.children == null) {
                    cellProperties.readOnly = true;
                    cellProperties.renderer = function (hotInstance, td, row, col, prop, value) {
                        td.style.backgroundColor = '#e6f5f9';
                        td.textContent = value;

                    };
                    return cellProperties;
                }
                return editCustomCellsAdditionals(row, col, prop, items);
            }
        })
    }

    function getItineraryproviderServicesAdditionals(itineraryId) {
        $.ajax({
            type: "GET",
            url: '/api/itinerary/' + itineraryId + '/service?in_quotation=1&is_additional=1',
        }).success(function (data) {
            if (hot_tables_additional_{{$itinerary->id}} == null) {
                renderAdditionalServices(data);
            } else {
                reloadItineraryAdditionalServices(data);
            }
        })
    }




    function editCustomCellsAdditionals(row, col, prop, items) {
        var cellMeta = {};
        var data = items[row];
        if (col == 13) {
            cellMeta.renderer = function (hotInstance, td, row, col, prop, value) {
                deleteServiceRenderer(hotInstance, td, row, col, prop, value);
            }
        }

        return cellMeta;
    }


    $(document).ready(function () {
        providerServicesAdditionals = getItineraryproviderServicesAdditionals({{$itinerary->id}});
    });


</script>

