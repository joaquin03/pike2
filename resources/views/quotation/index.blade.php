@extends('backpack::layout')

@section('after_styles')
    @stack('styles_stack')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
@endsection



@section('header')
    <section class="content-header">
        <h1>Quotations List</h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/admin/dashboard')}}">Admin</a></li>
            <li class="active">Quotation</li>
        </ol>

    </section>
@endsection()

@section('content')
    @include('crud::inc.grouped_errors')
    <div class="row">


        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                </div>
                <div class="box-body table-responsive">
                    <div class="form-inline">
                        <div class="form-group">
                            <label for="serch-q">Text:</label>
                            <input class="form-control" type="text" name="q" id="serch-q">
                        </div>
                    </div>
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th class="sortable" data-order="code" data-type="ASC">
                                Code <i class="fa fa-sort-amount-asc pull-right" aria-hidden="true"></i>
                            </th>
                            <th class="sortable" data-order="start_date" data-type="ASC">
                                Date <i class="fa fa-sort-amount-asc pull-right" aria-hidden="true"></i>
                            </th>
                            <th class="sortable" data-order="users.name" data-type="ASC">
                                In Charge <i class="fa fa-sort-amount-asc pull-right" aria-hidden="true"></i>
                            </th>
                            <th class="sortable" data-order="service.name" data-type="ASC">
                                Service <i class="fa fa-sort-amount-asc pull-right" aria-hidden="true"></i>
                            </th>
                            <th class="sortable" data-order="clientCompanies.name" data-type="ASC">
                                A. Client <i class="fa fa-sort-amount-asc pull-right" aria-hidden="true"></i>
                            </th>
                            <th class="sortable" data-order="aircraft.registration" data-type="ASC">
                                Aircraft <i class="fa fa-sort-amount-asc pull-right" aria-hidden="true"></i>
                            </th>

                        </tr>
                        </thead>
                        <tbody id="quotation-table">

                        </tbody>
                        </table>
                </div>
            </div>
        </div>
    </div>
@stop

@section('after_scripts')
    @stack('script_stack')
    <script src="{{ asset('vendor/adminlte/plugins/select2/select2.min.js') }}"></script>

    <script>
        var order = "";
        var orderType = "";
        $('.select2').select2();
        var quotations = {!! json_encode($items['data']) !!};


        renderQuotations = function (operationList) {
            var html = '';
            for (var i = 0; i < operationList.length; i++) {
                var item = operationList[i];
                html += '<tr>';
                html += '<td> <a href="/admin/quotation/' + item.id + '/edit">' + item.code + '</a> </td>';
                html += '<td> ' + item.date + ' </td>';
                html += '<td> ' + item.in_charge_user + ' </td>';
                html += '<td> ' + item.service + ' </td>';
                html += '<td> ' + item.aircraft_client + + item.country + ' </td>';
                html += '<td> ' + item.aircraft + ' </td>';
                html += '</tr>';
            }
            $("#quotation-table").html(html);
        };


        renderQuotations(quotations);



        $(".sortable").on('click', function () {
            $(".sortable").css({"color": "black"});
            $(this).css({"color": "gray"});
            var icon = $(this).children();
            if (icon.hasClass("fa-sort-amount-asc")) {
                $(this).children().removeClass("fa-sort-amount-asc");
                $(this).children().addClass("fa-sort-amount-desc");
                $(this).data("order_type", 'DESC')

            } else {
                $(this).children().removeClass("fa-sort-amount-desc");
                $(this).children().addClass("fa-sort-amount-asc");
                $(this).data("order_type", 'ASC')

            }
            order = $(this).data("order");
            orderType = $(this).data("order_type");

        });


    </script>


@endsection

