{{ csrf_field() }}

@php
    $quotation = '';
    $itineraries = [];
    if ($operation->quotation_id!=0){
        $quotation = \App\Models\Quotation::findOrFail($operation->quotation_id);
    }

    if($quotation !=''){
        $itineraries = $quotation->activeItineraries()->get();
        $permits = $quotation->aircraftPermission()->get()->where('is_additional_quotation', '!=', 1);
        //todo: consultar Joaco, filtro en la query no funcionó
   }
@endphp

<div class="full-width" id="quotatoins-container">
    @if($quotation !='')
        @include('quotation.quotation-modals.tables.services')
        <div class="col-md-2">
            <a class="btn-success quotationButton showPDFButton" id="showExportable" onclick="showPDF()"

               @if($quotation->extraData->quotation_exportable_file)
               href="{{"http://$_SERVER[HTTP_HOST]/admin/quotation/".$operation->quotation_id.'/document-show'}}"
               target="blank"
               @else
               title="Please ask Quotations department to save the pdf first."
               style="background-color: gray"
                    @endif
            >
                Show Quotations PDF
            </a>
        </div>
        <div class="col-md-2">
            <button onclick="showQuotation()" class="btn-info quotationButton" id="quotationButton"
                    title="Show Quotation for This Operation">Show Quotation
            </button>

        </div>
    @endif
</div>



@push('script_stack')

    {{--<script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>--}}
    {{--<script src="paginator.js"></script>--}}

    <script>

        $(document).ready(function () {

            $("#quotation").hide();
            if ("{{$quotation==''}}") {
                $("#quotationButton").hide();
            }
            //$(".sticky-header").floatThead({top:50});
        });

        function showPDF(){
            if ("{{!$quotation->extraData->quotation_exportable_file}}") {
                alert('Please ask Quotations department to save the pdf.');
            }

        }
        function showQuotation() {
            if ($("#quotation").is(":hidden")) {
                $("#quotation").show();
                document.getElementById('quotationButton').innerText = "Hide Quotation";
                document.getElementById('quotationButton').style.bottom = '310px';
                document.getElementById('showExportable').style.bottom = '310px';


            } else {
                $("#quotation").hide();
                document.getElementById('quotationButton').innerText = "Show Quotation";
                document.getElementById('quotationButton').style.bottom = '20px';
                document.getElementById('showExportable').style.bottom = '20px';

            }
        }

    </script>

@endpush
