<div class="col-md-12" style="text-align: center; margin: 10px; color: #797553;"> <b>PERMITS</b> </div>
<table class="table fixed_header itinerary-table  itinerary-table-striped ">
    <thead>
    <tr class="center-text" style="z-index: 999;">

        <th width="150">Provider</th>
        <th width="100">Code / Type</th>
        <th width="150">Country</th>
        <th width="100">From / To</th>
        <th width="150">Distance (NM)</th>
        <th width="150">Exp. Date</th>

        <th width="130">P. Cost</th>
        <th width="130">P. Adm. Fee</th>
        <th width="100">Final Cost</th>

        <th width="100">Unit Price</th>
        <th width="100">B. Adm. Fee</th>
        <th width="100" class="center-text">Total</th>


    </tr>
    </thead>

    @foreach($permits as $key => $permit)
        <tr class="center-text" id="quotation-permit-"{{$key}}>

            <td> {{$permit->provider->name  }} </td>

            <td> {{$permit->code}}
                <br> {{$permit->type}}
            </td>

            <td> {{$permit->country}} </td>

            <td> {{$permit->icao_from}}
                <br> {{$permit->icao_to}}
            </td>

            <td> {{$permit->country_distance}} </td>
            <td> {{$permit->expiration_date}} </td>


            <td width="100">
                {{$permit->cost}}
            </td>
            <td width="100">
                {{$permit->tax}}
            </td>

            <td width="100">
                {{$permit->getProcurementFinalCost()}}
            </td>
            <td width="100">
                {{$permit->billing_unit_price}}
            </td>

            <td width="100">
                {{$permit->billing_percentage}}
            </td>

            <td width="100">
                {{$permit->getFinalCostQuotation()}}
            </td>


        </tr>

    @endforeach


    <tbody id="quotation-permits-table">
    </tbody>
</table>

