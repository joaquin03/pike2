<div class="services-table">

    <div class="quotation-box col-md-10" style="overflow-y: scroll;" id="quotation" hidden>
        <div class=" sticky-header itinerary-table itinerary-table-dark itinerary-table-striped"
             style="font-size: 14px;">

            <div class="col-md-12 center-text form-group" style="margin: 15px;">
                <div style="color: #797553;">
                    @if(\Illuminate\Support\Facades\Auth::user()->can('Operation'))

                        <b>QUOTATION <a  href={{"http://$_SERVER[HTTP_HOST]/admin/quotation/".$quotation->id.'/edit'}}> {{$quotation->code}} </a>
                            GENERAL INFORMATION</b>
                    @else
                        <b>QUOTATION {{$quotation->code}} GENERAL INFORMATION</b>
                    @endif
                </div>
                <div class="col-md-12" style="text-align: center">
                    <div class="col-md-4">
                        Contact
                        name: {{$quotation->extraData->contact ? $quotation->extraData->contact->getFullName() : 'No Contact Assigned' }}
                    </div>
                    <div class="col-md-4">
                        Contact
                        e-mail: {{$quotation->extraData->contact ? $quotation->extraData->contact->getMainEmail() : 'No Contact Assigned'}}
                    </div>
                    <div class="col-md-4">
                        Quotation date: {{$quotation->getStartDate()}}
                    </div>
                </div>
                <div class="col-md-12" style="text-align: center">
                    <b>
                        Total Trip: USD {{$quotation->getFormattedQuotationTotalCost()}}
                    </b>
                </div>
            </div>
            <div class="form-group" id="itinerary-0">
                <table class="table fixed_header itinerary-table  itinerary-table-striped ">
                    <thead>
                    <tr class="center-text">
                        <th width="150">Type / Airport</th>
                        <th width="250">Handler</th>
                        <th width="250">Provider / Service</th>
                        @if($entity == 'procurement' || $entity == 'billing')
                            <th width="100">Quantity</th>
                            <th width="100">Unit Cost</th>
                            <th width="100" class="center-text">Tax</th>
                            <th width="130">P. Adm. Fee</th>
                            <th width="100">Final Cost</th>

                            @if($entity == 'billing')
                                <th width="100">Percentage</th>
                                <th width="100">Adm. Fee</th>
                                <th width="100">Unit Price</th>
                                <th width="100" class="center-text">Total</th>
                            @endif
                        @endif

                    </tr>
                    </thead>

                    @foreach($itineraries as $key => $itinerary)
                        <tr class="center-text" id="quotation-itinerary-"{{$key}}>
                            <td colspan="2" class="sub-table-container">

                                <table class="table itinerary-table sub-table" style="border-top: 0">
                                    <tr>
                                        <td width="150"> {{$itinerary->type}} - {{$itinerary->airport_icao_string}}
                                            <br> {{$itinerary->date}} {{$itinerary->time}}
                                        </td>
                                        <td width="200"> @if($itinerary->handler){{$itinerary->handler->name}}@endif
                                        </td>

                                    </tr>
                                    <tr>
                                        @if($itinerary->isArrival())
                                            <td colspan="2">
                                                @if($itinerary->quotation_notes)
                                                    <div class="col-md-12" style="color: #797553;">
                                                        <b>ITINERARY NOTES</b>
                                                    </div>
                                                    <div class="col-md-12">
                                                        {{ $itinerary->quotation_notes  }}
                                                    </div>
                                                @endif
                                                @if($itinerary->getItinieraryServicesAdmFees()>0)

                                                    <div class="col-md-12" style="color: #797553; margin-top: 10px;">
                                                        <b>ADMINISTRATION FEE</b>
                                                    </div>
                                                    <div class="col-md-12">
                                                        USD {{ number_format($itinerary->getItinieraryServicesAdmFees(),2) }}
                                                    </div>
                                                @endif
                                                @if($itinerary->getQuotationCreditNoteSurcharge($itinerary->itineraryCostWithoutCharges())>0)

                                                    <div class="col-md-12" style="color: #797553;">
                                                        <b>CONVENIENCE FEE</b>
                                                    </div>
                                                    <div class="col-md-12">
                                                        USD {{number_format($itinerary->getQuotationCreditNoteSurcharge($itinerary->itineraryCostWithoutCharges()), 2)}}
                                                    </div>
                                                @endif
                                            </td>

                                        @endif
                                    </tr>
                                </table>
                            </td>

                            <td colspan="10" class="sub-table-container">
                                <table class="table itinerary-table sub-table" style="border-top: 0">
                                    @foreach($itinerary->itineraryProviderServices()->get() as $ips)
                                        @if($ips->service && $ips->provider)
                                            <tr>

                                                <td width="250"
                                                    @if(!$ips->parent) style="background-color: #797553" @endif>
                                                    @if($ips->isAdditional())
                                                        <div style="font-weight: bolder;color: red;font-size: 17px;">
                                                            ADDITIONAL
                                                        </div>@endif {{$ips->provider->name}}
                                                    - {{$ips->service->name}}

                                                </td>

                                                @if($entity == 'procurement' || $entity == 'billing')

                                                    <td width="100"
                                                        @if(!$ips->parent) style="background-color: #797553" @endif>
                                                        {{$ips->procurement_quantity}}
                                                    </td>
                                                    <td width="100"
                                                        @if(!$ips->parent) style="background-color: #797553" @endif>
                                                        {{$ips->procurement_cost}}
                                                    </td>
                                                    <td width="100"
                                                        @if(!$ips->parent) style="background-color: #797553" @endif>
                                                        {{$ips->procurement_tax}}
                                                    </td>
                                                    <td width="130"
                                                        @if(!$ips->parent) style="background-color: #797553" @endif>
                                                        {{$ips->procurement_adm_fee}}
                                                    </td>
                                                    <td width="100"
                                                        @if(!$ips->parent) style="background-color: #797553" @endif>
                                                        {{$ips->getProcurementFinalCost()}}
                                                    </td>
                                                    @if($entity == 'billing')

                                                        <td width="100"
                                                            @if(!$ips->parent) style="background-color: #797553" @endif>
                                                            {{$ips->billing_percentage}}
                                                        </td>
                                                        <td width="100"
                                                            @if(!$ips->parent) style="background-color: #797553" @endif>
                                                            {{$ips->administration_fee}}
                                                        </td>
                                                        <td width="100"
                                                            @if(!$ips->parent) style="background-color: #797553" @endif>
                                                            {{$ips->billing_unit_price}}
                                                        </td>
                                                        <td width="100"
                                                            @if(!$ips->parent) style="background-color: #797553" @endif>
                                                            {{$ips->getFinalCostQuotation()}}
                                                        </td>

                                                    @endif
                                                @endif
                                            </tr>
                                        @endif

                                    @endforeach
                                </table>
                            </td>
                        </tr>
                    @endforeach


                    <tbody id="quotation-services-table">
                    </tbody>
                </table>
                @include('quotation.quotation-modals.tables.permits')


            </div>
        </div>
    </div>

</div>
