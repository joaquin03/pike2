<div class=" form-group col-md-12">
    <h3 class="form-section" style="text-align: center">GENERAL</h3>

    <div class="col-md-6">
        <h4 class="form-section" style="text-align: center; margin-bottom: 20px">FLIGHT INFORMATION</h4>


        <div class="form-group col-md-12">
            <label class="control-label col-md-3">Code #</label>
            <div class="col-md-9">
                <input name="code" class="form-control" value="{{ $operation->code }}" disabled/>
            </div>
        </div>

        <div class="form-group col-md-12">
            <label class="control-label col-md-3">State</label>
            <div class="col-md-9">
                <select class="form-control " name="state" id="state">
                    @foreach(App\Models\Quotation::$quotationStates as $key => $state)
                        @if($state != "Deleted")
                            <option style="color: {{$key}}" value="{{$state}}"
                                    {{(isset($operation) && $operation->state == $state)? 'selected="selected"' : ''}}
                            >
                                {{ $state }}
                            </option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group col-md-12">
            <label class="control-label col-md-3">Starting date</label>
            <div class="col-md-9">

                @if ($creating ?? false)

                    <div class='input-group date datepicker' id="time">
                        <input name="start_date" type='text' class="form-control"
                               value="{{ $operation->start_date }}"/>
                        <span id="time-button" class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span></span>
                    </div>
                @else
                    <div class='input-group date datepicker form-group col-md-12' id="time">
                        <input name="code" class="form-control" disabled value="{{ $operation->getStartDate() }}"/>


                    </div>
                @endif


            </div>
        </div>

        <div class="form-group col-md-12">
            <label class="control-label col-md-3">Operation starting date</label>
            <div class="col-md-9">

                @if (!$operation->isConfirmed())

                    <div class='input-group date datepicker' id="custom_time">
                        <input name="operation_custom_start_date" type='text' class="form-control"
                               value="{{ $operation->getCustomStartDate()  ?? null }}"/>
                        <span id="time-button" class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span></span>
                    </div>
                @else
                    <div class='input-group date datepicker form-group col-md-12' id="custom_time_edit">
                        <input name="custom_code" class="form-control" disabled
                               value="{{ $operation->getCustomStartDate() ?? $operation->getStartDate()}}"/>
                    </div>
                @endif


            </div>
        </div>


        <div class="form-group col-md-12">
            <label class="control-label col-md-3">Type of Flight</label>
            <div class="col-md-9">
                <select class="form-control select2" name="type_of_flight" id="type_of_flight">
                    @foreach(App\Models\Quotation::$typeOfFlight as  $type)
                        <option value="{{$type}}"
                                {{(isset($operation) &&  isset($operation->extraData) &&  $operation->extraData->type_of_flight == $type)? 'selected="selected"' : ''}}
                        >
                            {{$type}}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group col-md-12">
            <label class="control-label col-md-3">Aircraft Type</label>
            <div class="col-md-9">
                <select class="form-control select2" onchange="showByType()" name="aircraft_type_id"
                        id="aircraft_type_id">
                    <option value="" disabled="disabled" selected="selected">Select the Aircraft Type</option>
                    @foreach(App\Models\AircraftType::all() as $type)
                        <option value="{{$type->id}}"
                                {{(isset($operation) && $operation->aircraft_type_id == $type->id)? 'selected="selected"' : ''}}
                        >
                            {{$type->name}}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group col-md-12">
            <label class="control-label col-md-3">MTOW</label>
            <div class="col-md-3">
                <input id="mtow" name="mtow" type="text" class="form-control" autocomplete="off" disabled
                       value=" ">
            </div>
            <label class="control-label col-md-3" style="text-align: center">MTOW Type</label>
            <div class="col-md-3">
                <input id="mtow_type" name="mtow_type" type="text" class="form-control" autocomplete="off" disabled
                       value=" ">
            </div>
        </div>

    </div>


    <div class="col-md-6">

        <h4 class="form-section" style="text-align: center; margin-bottom: 20px">COMMERCIAL INFORMATION</h4>

        <div class="form-group col-md-12">
            <label class="control-label col-md-3">Company</label>
            <div class="col-md-9">
                <select class="form-control select2" name="aircraft_client_id" id="client-id">
                    <option value="" disabled="disabled" selected="selected">Select the Client</option>
                    @foreach(App\Models\Company::all() as $client)
                        <option value="{{$client->id}}"
                                {{(isset($operation) && $operation->aircraft_client_id == $client->id)? 'selected="selected"' : ''}}>
                            {{$client->name}} - {{$client->getCountry()}}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group col-md-12">
            <label class="control-label col-md-3">Sales manager</label>
            <div class="col-md-9">
                <input name="sales_manager" type="text" class="form-control" autocomplete="off"
                       value='{{$operation->aircraftClient ? $operation->aircraftClient->salesManager ? $operation->aircraftClient->salesManager->name : '' : ''}}'
                       disabled>
            </div>
        </div>


        <div class="form-group col-md-12">
            <label class="control-label col-md-3">Contact</label>
            <div class="col-md-9">
                <select class="form-control select2" name="contact_id" id="contact-id">
                    <option value="" disabled="disabled" selected="selected">Select the Contact</option>

                    @if($operation->aircraft_client_id)
                        @foreach(App\Models\Contact::where('company_id', $operation->aircraft_client_id)->get() as $contact)
                            <option value="{{$contact->id}}"
                                    {{(isset($operation) && $operation->extraData ? $operation->extraData->contact_id == $contact->id:false)?  'selected="selected"' : ''}}>
                                {{$contact->name}} {{$contact->surname}}
                            </option>
                        @endforeach
                    @else
                        @foreach(App\Models\Contact::all() as $contact)
                            <option value="{{$contact->id}}"
                                    {{(isset($operation) && $operation->extraData ? $operation->extraData->contact_id == $contact->id:false)?  'selected="selected"' : ''}}>
                                {{$contact->name}} {{$contact->surname}}
                            </option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>

        <div class="form-group col-md-12">
            <label class="control-label col-md-3">Service Type</label>
            <div class="col-md-9">
                <select class="form-control select2" name="quotation_services_type"
                        id="quotation_services_type" onchange="showVIPAssistanceTypeOfFlight()">
                    @foreach(App\Models\Quotation::$serviceTypes as $type)
                        @if($operation->state != "Deleted")
                            <option value="{{$type}}"
                                    {{(isset($operation) && $operation->quotation_services_type == $type)? 'selected="selected"' : ''}}
                            >
                                {{ $type }}
                            </option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group col-md-12">
            <label class="control-label col-md-3">Ref #</label>
            <div class="col-md-9">
                <input name="ref_number" type="text" class="form-control" autocomplete="off"
                       value='{{$operation->ref_number}}'>
            </div>
        </div>

        <div class="form-group col-md-12">
            <label class="control-label col-md-3">Via</label>
            <div class="col-md-9">
                <input name="quotation_via" type="text" class="form-control" autocomplete="off"
                       value='{{$operation->quotation_via}}'>
            </div>
        </div>


        @if(! ($creating?? false))
            <div class="form-group col-md-12">
                <label class="control-label col-md-3">Payment</label>
                <div class="col-md-9">
                    <select class="form-control select2" name="payment" id="payment">
                        @foreach(App\Models\Quotation::$paymentTypes as $key => $paymentType)
                            <option value="{{$paymentType}}"
                                    {{($operation->extraData->payment == $paymentType)? 'selected="selected"' : ''}}
                            >
                                {{$paymentType}}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group col-md-12">
                <label class="control-label col-md-3">Payment Method</label>
                <div class="col-md-9">
                    <select class="form-control select2" name="payment_method" id="payment_method">
                        @foreach(App\Models\Quotation::$paymentMethods as $key => $paymentMethod)
                            <option value="{{$paymentMethod}}"
                                    {{($operation->extraData->payment_method == $paymentMethod)? 'selected="selected"' : ''}}
                            >
                                {{$paymentMethod}}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>


        @endif
    </div>


    <div class="form-group col-md-12">
        <div class="form-group col-md-12">
            <label class="control-label col-md-12">Notes</label>
            <div class="col-md-12">
                <textarea name="quotation_notes" class="form-control"> {{$operation->quotation_notes}}</textarea>
            </div>
        </div>
    </div>
</div>


@include('general.specialFeaturesModal')

<script type="text/javascript">
    $(document).ready(function () {
        if ($('.select2').length > 0) {
            $('.select2').select2();
            alertSpecialFeatures();
        }
    });

    function alertSpecialFeatures() {
        var company_id = $('#client-id').val();
        var current_url = window.location.href;

        if (company_id && current_url.split('#')[1] != 'documents') {
            $.ajax({
                url: '{{url('/admin/company/alert/special-features')}}' + '/' + company_id,
                type: 'GET',
                dataType: "json",
                success: function (data) {
                    if (data['special_features']) {
                        console.log(data['special_features']);
                        $("#client-input-id").val(company_id);
                        $("#specialFeaturesModal").modal();

                    }
                },
                error: function (result) {
                    alert('error');
                }
            });

        }

    }
</script>
