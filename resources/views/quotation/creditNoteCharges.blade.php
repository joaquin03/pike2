@extends('backpack::layout')


@section('title')

@endsection

@section('header')
    <section class="content-header">
        <h1>Credit Note Quotation Charges</h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/admin/dashboard')}}">Admin</a></li>
            <li><a href="{{url('/admin/quotation')}}" class="text-capitalize">Quotations</a></li>
            <li class="active">Load</li>
        </ol>

    </section>
@endsection()

@section('content')
    @include('crud::inc.grouped_errors')

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body row">
                    <div class="col-md-12">
                        <div class="form-group col-md-6" id="export-quotation-button">
                            <form action="{{route('credit-card-charges.upload')}}" method="POST"
                                  enctype="multipart/form-data">

                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input class="col-md-8" type="file" name="charges" required="required"
                                       style="margin-top: 10px; margin-bottom: 10px">

                                <button type="submit" class="col-md-4 btn btn-warning">Import Excel</button>
                            </form>
                        </div>
                    </div>

                    <div class="col-md-12">
                        @if($lastUpdate)
                            <h3>Last Values Uploaded - <span
                                        style="font-size: 15px;">Date {{$lastUpdate->created_at->format('d-m-Y')}}</span>
                            </h3>
                        @else
                            <h3> No data, please upload values. </h3>
                        @endif                            @if($creditCardsData)

                            <table class="table table-hover">
                                <tr>
                                    <th>FROM</th>
                                    <th>TO</th>
                                    <th>VALUE</th>
                                </tr>

                                @foreach($creditCardsData as $values)
                                    <tr>
                                        <td>{{ $values['from'] }}</td>
                                        <td>{{ $values['to'] }}</td>
                                        <td>{{ $values['value'] }}</td>
                                    </tr>
                                @endforeach
                            </table>
                        @endif

                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection