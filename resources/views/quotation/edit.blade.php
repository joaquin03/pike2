@extends('backpack::layout')
@stack('styles_stack')
@section('title')

@endsection

@section('after_styles')

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

    <link rel="stylesheet" href="{{ asset('/js/handsontable.full.min.css') }}"/>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/adminlte/plugins/select2/select2.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css"
          rel="stylesheet" type="text/css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"/>


    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>



@endsection

@section('header')
    @include('operation.partials.operationNavBar', ['operation'=>$operation])
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <section class="content-header">
        <h1>Edit Quotation</h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/admin/dashboard')}}">Admin</a></li>
            <li><a href="{{url('/admin/quotation')}}" class="text-capitalize">Quotation</a></li>
            <li class="active">Edit</li>
        </ol>

    </section>
@endsection()

@section('content')
    @include('crud::inc.grouped_errors')
    <div class="row">
        <div class="col-md-12">
            <div class="nav">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs pull-right">

                        <li class="edit" onclick="">
                            <a href="#tab_documents" data-toggle="tab">Documents</a>
                        </li>
                        <li class="active" class="edit" onclick="">
                            <a href="#tab_general" data-toggle="tab">General</a>
                        </li>
                    </ul>


                    <form action="{{route('crud.quotation.update', ['operation'=>$operation->id])}}" method="POST"
                          enctype="multipart/form-data">


                        @include('quotation._fields')


                        <div style="margin: 27px" id="saveActions" class="form-group">
                            <button type="submit" class="btn btn-success">
                                <span class="fa fa-save" role="presentation" aria-hidden="true"></span>
                                Save
                            </button>

                            <a href="{{url('/admin/quotation')}}" class="btn btn-default"><span></span>
                                &nbsp;Back</a>
                        </div>
                    </form>
                    <hr>
                </div>

            </div>
            <div>
                <div class="box">
                    <div class="col-md-12" style="margin-top: 15px">

                        {{--Confirm Quotation Button--}}
                        <div class="form-group col-md-9" id="confirm-quotation-operation-button">
                            <div class="col-md-12">
                                <form onsubmit="return confirm('Do you really want to confirm the Quotation?')"
                                      action="{{route('crud.quotation.operation.create', ['quotation_id'=>$operation->id])}}"
                                      method="POST">
                                    {{ csrf_field() }}

                                    <div class="col-md-3">
                                        <button type="submit" class="btn btn-info" style="margin-left: -34px;">
                                            Confirm Quotation
                                        </button>
                                    </div>
                                    <div class="col-md-9">
                                        <label class="control-label col-md-4">Select operation to assign
                                            quotation</label>
                                        <div class="col-md-8">
                                            <select class="form-control select2 col-md-9" name="operation_id"
                                                    id="operation_id">
                                                <option value=''> Select operation (optional)</option>
                                                @foreach(App\Models\Operation::all()->where('operation_id', null)->sortByDesc('code') as  $op)
                                                    <option value="{{ $op->id }}"
                                                            {{(isset($operation) && $operation->id == $op->id) ? 'selected="selected"' : ''}}
                                                    >
                                                        {{$op->code}}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                </form>


                            </div>

                            {{--Add Itinerary Button --}}
                            <div class="col-md-12">
                                <div class="col-md-3">
                                    <a href="{{ route('crud.quotation.itinerary.create', ['operation'=>$operation->id]) }}"
                                       data-toggle="modal" data-target="#itineraryModal"
                                       class="btn btn-success ajax-modal"
                                       style="margin-top: 10px; margin-left: -34px;">
                                        Add Itinerary
                                    </a>
                                </div>
                            </div>


                        </div>


                        {{--Show Quotation Generated Operation Button--}}
                        <div class="form-group col-md-3" id="show-quotation-operation-button">
                            <a href="{{url('/admin/operation/' . $operation->operation_id . '/edit')}}"
                               class="btn btn-info"><span></span>Show Operation</a>
                        </div>


                        {{--Export Quotation Button--}}
                        <div class="form-group col-md-3" id="export-quotation-button">

                            <form id="uploadbanner" class="pull-right" enctype="multipart/form-data" method="post"
                                  action="{{ route('quotation.route-map.upload', ['quotation_id'=>$operation->id]) }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="submit" class="btn btn-warning" value="Export Quotation"/>
                            </form>
                        </div>

                    </div>
                    <div class="box-body">
                        @include('quotation.partials.itinerary', ['itineraries'=>$operation->activeItineraries], ['operation'=>$operation])
                    </div>
                </div>

            </div>

        </div>
    </div>
    <!-- Default bootstrap modal example -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>

@endsection




@push('stack_after_scripts')


    <script src="{{ asset('vendor/adminlte/plugins/select2/select2.js') }}"></script>
    <script type="text/javascript"
            src="{{ asset('vendor/adminlte/plugins/daterangepicker/moment.min.js') }}"></script>
    <script src="{{ asset('vendor/adminlte/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    <script type="text/javascript"
            src="https://cdn.jsdelivr.net/bootstrap.datetimepicker/4.17.42/js/bootstrap-datetimepicker.min.js"></script>
    <script src="{{ asset('/js/handsontable.full.min.js') }}"></script>
    <script>

        function loadSelect2(){
            $(".select2").select2();
        }
        window.Laravel = {!! json_encode([
            'csrfToken'=> csrf_token(),
            'user'=> [
                'authenticated' => auth()->check(),
                'id' => auth()->check() ? auth()->user()->id : null,
                'name' => auth()->check() ? auth()->user()->name : null,
                'operation_id' => $operation->id
                ]
            ])
        !!};


        jQuery(function ($) {

            var operation = {!! json_encode($operation->operation, true)  !!};

            $(".ajax-modal").on("show.bs.modal", function (e) {
                var link = $(e.relatedTarget);
                $(this).find(".modal-body").load(link.attr("href"));
            });
            $(".modal").on('hidden.bs.modal', function () {
                $(this).data('bs.modal', null);
            });
            $("#confirm-quotation-operation-button").hide();
            $("#show-quotation-operation-button").hide();

            if (operation) {
                $("#show-quotation-operation-button").show();
            } else {
                $("#confirm-quotation-operation-button").show();
            }

        });


    </script>
    <script src="{{ asset('js/app.js') }}" defer></script>


    <script type="text/javascript">
        $(document).ready(function () {
            if ($('.select2').length > 0) {
                $('.select2').select2();
            }
            showMTOW();
            showByType();
            showVIPAssistanceTypeOfFlight();

        });

        $(function () {
            var dateNow = new Date();
            $('.datepicker').datetimepicker({
                toolbarPlacement: 'bottom',
                showClose: true,
                viewMode: 'years',
                format: 'MM-YY',
                defaultDate: dateNow
            });
        });

        function showMTOW() {
            jQuery(function ($) {
                var aircraft_registration = $("#operation-aircraft-id option:selected").val();

                $.ajax({
                    url: '{{url('/admin/aircraft/info')}}' + '/' + aircraft_registration,
                    type: 'GET',
                    dataType: "json",
                    success: function (data) {

                        if (data['mtow']) {
                            $("#mtow").val(data['mtow']);
                        } else {
                            $("#mtow").val("");
                        }
                        if (data['mtow_type']) {
                            $("#mtow_type").val(data['mtow_type']);
                        } else {
                            $("#mtow_type").val("");
                        }
                        if (data['aircraft_type']) {
                            $("#aircraft_type").val(data['aircraft_type']).change();
                        } else {
                            $("#aircraft_type").val("");
                        }

                    },
                });
            });
        }

        function showByType() {
            var type_selected_id = $("#aircraft_type_id option:selected").val();
            if (type_selected_id) {
                $.ajax({
                    url: '{{url('/admin/aircraft/type/info')}}' + '/' + type_selected_id,
                    type: 'GET',
                    dataType: "json",
                    success: function (data) {

                        if (data['mtow']) {
                            $("#mtow").val(data['mtow']);
                        } else {
                            $("#mtow").val("");
                        }
                        if (data['mtow_type']) {
                            $("#mtow_type").val(data['mtow_type']);
                        } else {
                            $("#mtow_type").val("");
                        }
                    },
                    error: function (result) {
                        alert('error');
                    }
                });

            }
        }

        function changeContentSize() {
            $('.tab-content').resize();
        }


        function showVIPAssistanceTypeOfFlight() {
            var x = $('#quotation_services_type').find(":selected").val();

            if (x === "Vip Assistance") {
                $('#type_of_vip_flight_class').show();
            } else {
                $('#type_of_vip_flight_class').hide();
            }
        }
    </script>

    <script>
        $(".file-clear-button").click(function (e) {
            e.preventDefault();
            var container = $(this).parent().parent();
            var parent = $(this).parent();
            // remove the filename and button
            parent.remove();
            console.log(container);
            // if the file container is empty, remove it
            if ($.trim(container.html()) == '') {
                container.remove();
            }
            $("<input type='hidden' name='clear_documents[]' value='" + $(this).data('filename') + "'>").insertAfter("#documents_file_input");
        });

        $("#documents_file_input").change(function () {
            console.log($(this).val());
            // remove the hidden input, so that the setXAttribute method is no longer triggered
            $(this).next("input[type=hidden]").remove();
        });


        }
    </script>


    @stack('script_stack')
@endpush

<style>
    span.select2 {
        display         : table;
        table-layout    : fixed;
        width           : 100% !important;
    }
</style>