@extends('backpack::layout')

@stack('styles_stack')

@section('after_styles')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <link href="{{ asset('vendor/adminlte/plugins/select2/select2.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css"
          rel="stylesheet" type="text/css"/>

    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"/>
@endsection

@section('title')
Quotation
@endsection

@section('header')
    <section class="content-header">
        <h1>Quotation</h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/admin/dashboard')}}">Admin</a></li>
            <li><a href="{{url('/admin/quotation')}}" class="text-capitalize">Quotation</a></li>
            <li class="active">Create</li>
        </ol>

    </section>
@endsection()

@section('content')
    @include('crud::inc.grouped_errors')
    <div class="col-md-12">
        <div class="box">
            <form action="{{route('crud.quotation.store')}}" method="POST" enctype="multipart/form-data">
                @include('quotation._fields')
                <div class="box-footer">

                    <div id="saveActions" class="form-group">
                        <button type="submit" class="btn btn-success">
                            <span class="fa fa-save" role="presentation" aria-hidden="true"></span>
                            Create
                        </button>

                        <a href="{{url('/admin/quotation')}}" class="btn btn-default"><span class="fa fa-ban"></span>Cancel</a>
                    </div>
                </div>
            </form>


        </div>
    </div>

@endsection



@section('after_scripts')
    <script src="{{ asset('vendor/adminlte/plugins/select2/select2.min.js') }}"></script>
    <script src="{{ asset('vendor/adminlte/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendor/adminlte/plugins/daterangepicker/moment.min.js') }}"></script>
    <script type="text/javascript"
            src="https://cdn.jsdelivr.net/bootstrap.datetimepicker/4.17.42/js/bootstrap-datetimepicker.min.js"></script>

    <script>
        var data1;
        jQuery(function ($) {


            $("#operation-aircraft-id").on("change", function () {
                var aircraft_registration = $("#operation-aircraft-id option:selected").val();
                $.ajax({
                    url: '{{url('/admin/aircraft/info')}}' + '/' + aircraft_registration,
                    type: 'GET',
                    dataType: "json",
                    success: function (data) {
                        if(data['client_id']){
                            $("#client-id").val(data['client_id']);
                        } else{
                            $("#client-id").val("");
                        }
                        if(data['operator_id']){
                            $("#operator-id").val(data['operator_id']);
                        } else {
                            $("#operator-id").val("");
                        }
                        if(data['mtow']){
                            $("#mtow").val(data['mtow']);
                        } else {
                            $("#mtow").val("");
                        }
                        if (data['mtow_type']) {
                            $("#mtow_type").val(data['mtow_type']);
                        } else {
                            $("#mtow_type").val("");
                        }
                        if (data['aircraft_type']) {
                            $("#aircraft_type").val(data['aircraft_type']);
                        } else {
                            $("#aircraft_type").val("");
                        }
                    },
                    error: function (result) {
                        alert('error');
                    }
                });


            });
        });

    </script>


    <script type="text/javascript">
        $(document).ready(function () {
            if ($('.select2').length > 0) {
                $('.select2').select2();
            }
            showMTOW();
            showByType();
        });

        $(function () {
            var dateNow = new Date();
            $('.datepicker').datetimepicker({
                toolbarPlacement: 'bottom',
                showClose: true,
                viewMode: 'years',
                format: 'MM-YY',
                defaultDate: dateNow
            });
        });

        function showMTOW() {
            jQuery(function ($) {
                var aircraft_registration = $("#operation-aircraft-id option:selected").val();

                $.ajax({
                    url: '{{url('/admin/aircraft/info')}}' + '/' + aircraft_registration,
                    type: 'GET',
                    dataType: "json",
                    success: function (data) {

                        if (data['mtow']) {
                            $("#mtow").val(data['mtow']);
                        } else {
                            $("#mtow").val("");
                        }
                        if (data['mtow_type']) {
                            $("#mtow_type").val(data['mtow_type']);
                        } else {
                            $("#mtow_type").val("");
                        }
                        if (data['aircraft_type']) {
                            $("#aircraft_type").val(data['aircraft_type']).change();
                        } else {
                            $("#aircraft_type").val("");
                        }

                    },
                });
            });
        }

        function showByType() {
            var type_selected_id = $("#aircraft_type_id option:selected").val();
            if (type_selected_id) {
                $.ajax({
                    url: '{{url('/admin/aircraft/type/info')}}' + '/' + type_selected_id,
                    type: 'GET',
                    dataType: "json",
                    success: function (data) {

                        if (data['mtow']) {
                            $("#mtow").val(data['mtow']);
                        } else {
                            $("#mtow").val("");
                        }
                        if (data['mtow_type']) {
                            $("#mtow_type").val(data['mtow_type']);
                        } else {
                            $("#mtow_type").val("");
                        }
                    },
                    error: function (result) {
                        alert('error');
                    }
                });

            }
        }
        </script>

    @stack('script_stack')

@endsection
