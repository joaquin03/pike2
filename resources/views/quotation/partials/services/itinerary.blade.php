<a href="{{ route('crud.procurement.total', ['id'=>$operation->id]) }}"
   data-toggle="modal" data-target="#totalCostModal" class="btn btn-info ajax-modal">
    Total Cost
</a>
<table id="crudTable" class="table table-bordered table-striped display" style="width:100%; font-size: 17px">
    <thead>
    <tr>
        <th></th>
        <th style="width: 20%">Type</th>
        <th style="width: 20%">Airport</th>
        <th style="width: 20%">Date</th>
        <th style="width: 20%">Time</th>
        <th style="width: 20%">Handler</th>
        <th style="width: 20%">Status</th>
    </tr>


    </thead>
    <tbody>
    @foreach($itineraries as $itinerary)
        <tr data-id="{{ $itinerary->id }}">
            @if($itinerary->canHaveServices())
                <td class="details-control text-center cursor-pointer" id="itinerary-{{$itinerary->id}}">
                    <i data-entry-id="{{ $itinerary->id }}"
                       class="fa fa-plus-square-o details-row-button cursor-pointer"></i>
                </td>
            @else
                <td>  </td>
            @endif
            <td><b>{{ $itinerary->type }}</b></td>
            <td>{{ $itinerary->airport->icao }}</td>
            <td>@if($itinerary->hasDate()) {{ $itinerary->date}} @else TBA @endif</td>
            <td>@if($itinerary->hasTime()){{ $itinerary->time }}@else TBA @endif</td>
            <td>
                @if($itinerary->handler)
                    {{ $itinerary->handler->name }}
                @else
                    -
                @endif
            </td>
            <td>
                @if($itinerary->is_canceled)
                    <button class="btn btn-danger" style="margin-right:5px; pointer-events: none">
                        Canceled
                    </button>
                @elseif($itinerary->hasServices())
                    <button class="btn btn-success" style="margin-right:5px; pointer-events: none">
                        <i class="fa fa-truck" style="  display: inline-block; width:12px; ">
                                <span class="label label-warning" style="zoom: 1.1; top: -17px;">
                                    {{ $itinerary->itineraryProviderServices()->count()}}
                                </span>
                        </i>
                    </button>

                @else
                    <button class="btn btn-default ajax-modal "style=" margin-right:5px; pointer-events: none">
                        <i class="fa fa-truck" style="color:#696969;width:15px;"></i></button>
                @endif
            </td>
        </tr>

    @endforeach
    </tbody>


</table>

<div class="box-footer">

</div>


<!-- Default bootstrap modal example -->
<div class="modal fade" id="totalCostModal" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-xs">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>




@push('styles_stack')
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/plugins/datepicker/datepicker3.css') }}">
@endpush

@push('script_stack')
    <script type="text/javascript"
            src="{{ asset('vendor/adminlte/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script type="text/javascript"
            src="{{ asset('vendor/adminlte/plugins/datatables/dataTables.bootstrap.js') }}"></script>
    <script>

        $("#totalCostModal").on('hidden.bs.modal', function () {
            $(this).data('bs.modal', null);
        });

        jQuery(document).ready(function ($) {

            var table = $("#crudTable").DataTable({
                "bPaginate": false,
                /* Disable initial sort */
                "aaSorting": [],

            });

            $(table.table().container() ).removeClass( 'form-inline' );

            // var crudTable = $('#crudTable tbody');
            // Remove any previously registered event handlers from draw.dt event callback
            $('#crudTable tbody').off('click', 'td .details-row-button');

            // Make sure the ajaxDatatables rows also have the correct classes
            $('#crudTable tbody td .details-row-button').parent('td')
                .removeClass('details-control').addClass('details-control')
                .removeClass('text-center').addClass('text-center')
                .removeClass('cursor-pointer').addClass('cursor-pointer');

            // Add event listener for opening and closing details
            $('#crudTable tbody td.details-control').on('click', function () {
                hideItinerary();
                var tr = $(this).closest('tr');
                var btn = $(this).find('.details-row-button');
                var row = table.row(tr);
                if (row.child.isShown()) {
                    // This row is already open - close it
                    btn.removeClass('fa-minus-square-o').addClass('fa-plus-square-o');
                    $('div.table_row_slider', row.child()).slideUp(function () {
                        row.child.hide();
                        tr.removeClass('shown');
                    });
                }
                else {
                    // Open this row
                    btn.removeClass('fa-plus-square-o').addClass('fa-minus-square-o');
                    // Get the details with ajax
                    $.ajax({
                        url: '{{ url("/admin/itinerary-procurement")}}/' + btn.data('entry-id') + '/',
                        type: 'GET',
                    })
                        .done(function (data) {
                            // console.log("-- success getting table extra details row with AJAX");
                            row.child("<div class='table_row_slider'>" + data + "</div>", 'no-padding').show();
                            tr.addClass('shown');
                            $('div.table_row_slider', row.child()).slideDown();
                        })
                        .fail(function (data) {
                            // console.log("-- error getting table extra details row with AJAX");
                            row.child("<div class='table_row_slider'>There was an error loading the details. Please retry.</div>").show();
                            tr.addClass('shown');
                            $('div.table_row_slider', row.child()).slideDown();
                        })
                        .always(function (data) {
                            // console.log("-- complete getting table extra details row with AJAX");
                        });
                }

            });


            showItineraryByUrl = function () {

                var url = $(location).attr('href');
                var itinerary = url.split('#')[1];
                if (itinerary != null) {
                    $("#" + itinerary).click();
                }
            };
            showItineraryByUrl();
            hideItinerary = function() {
                items = $('.details-row-button.fa-minus-square-o');
                for(var i=0; i<items.length; i++) {
                    $(items[i]).click();
                }
            }
        });

    </script>

    <script>

        $('#estimated').on('shown', function () {
            $("#time_select").click();
        });
    </script>

     <script>
         $(".delete_itinerary").on("click", function(){
             return confirm("Do you want to delete this itinerary?");
         });
     </script>

@endpush