

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">Permission</h4>
</div>
<div class="modal-body row">
    @include('quotation.forms.permissions')
</div>
<div class="modal-body row">
    @include('quotation.tables.permissions')
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>

<script src="{{ asset('vendor/adminlte/plugins/select2/select2.min.js') }}"></script>
<script>
    $(function () {
        $('.select2').select2();
    })

</script>



