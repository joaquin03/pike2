<div class="table_row_slider">
    @php
        $providers = $providers['data'];
    @endphp
    @if(!$itinerary->isDeparture())

        <div class="row">
            <h3 class="col-md-12">
                <b>Services</b>
                {{--Sheets integration Button--}}
            </h3>
            <div class="row" style="margin-bottom: 10px">
                <div id="integrate-with-google-sheets">
                    <a href="{{url('/admin/quotation/integrate-sheet', ['$itineraryId'=>$itinerary->id])}}"
                       class="btn bg-maroon pull-left" style="margin-right: 20px;"><span></span>Generate Quotation Services</a>
                </div>
                <a href="{{ route('fuel.itinerary.values', ['itinerary'=>$itinerary->id]) }}"
                   data-toggle="modal" data-target="#itineraryModal" class="btn btn-success ajax-modal pull-left"
                    onclick="alert('If the handling provider is the same as the fuel provider, please choose that option (the cheaper one).')">
                    Show Fuel Values
                </a>
            </div>





            <div class="col-md-12 one-line">
                @include('quotation.forms.services')
            </div>
            <div class="col-md-12">
                @include('quotation.tables.services')
            </div>

        </div>

        <hr class="row separator">
    @endif

</div>


