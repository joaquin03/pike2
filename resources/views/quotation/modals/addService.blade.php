<div id="service-add-{{$provider['id']}}" class="col-md-12" style="margin-left: -15px;">
    <input type="hidden" name="provider_id" value="{{$provider['id']}}">
    <input type="hidden" name="in_procurement" value="1">
    <div class="form-group col-md-3" onKeyPress="checkSubmit(event)">
        <select class="form-control select2" style="margin-left: 0px" name="service_id">
            <option value=""> Service</option>
            @forelse($provider['provider_services']['data'] as $service)
                <option value="{{$service['id']}}">{{ $service['service'] }}</option>
            @empty
                <option disabled selected>No Services</option>
            @endforelse
        </select>
    </div>
    <div class="form-group col-md-2" onKeyPress="checkSubmit(event)">
        <input type="text" name="procurement_quantity" class="form-control" placeholder="Quantity">
    </div>
    <div class="form-group col-md-2" onKeyPress="checkSubmit(event)">
        <input type="text" name="procurement_cost" class="form-control" placeholder="Cost">
    </div>

    <div class="form-group col-md-2">
        <button type="submit" id="addService" onClick="addService('#service-add-{{$provider['id']}}')"
                class="btn btn-default add-button">
            Add Service
        </button>
    </div>
</div>


<script src="{{ asset('vendor/adminlte/plugins/select2/select2.min.js') }}"></script>
<script>
    $('.select2').select2();

    $('select').on(
        'select2:select', (
            function () {
                $(this).focus();
            }
        )
    );

    function checkSubmit(e) {
        if (e && e.keyCode == 13) {
            document.getElementById("addService").click();
        }
    }
</script>
