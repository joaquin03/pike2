
@if ($creating ?? false)
    <form method="POST" action="{{ route('crud.quotation.itinerary.store', ['operation_id'=>$crud->operationId]) }}"
     accept-charset="UTF-8">
@else
    <form method="POST"
      action="{{ route('crud.quotation.itinerary.update', ['itinerary_id'=>$itinerary->id]) }}"
      accept-charset="UTF-8">
@endif
        {{ csrf_field() }}
        <div class="box">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin: 1%"><span
                        aria-hidden="true">&times;</span>
            </button>

            @if ($creating ?? false)
                <h3 style="margin-left: 10px;">Add Itinerary</h3>
            @else
                <h3 style="margin-left: 10px;">Edit Itinerary</h3>
            @endif
            <div class="">

                <div class="itinerary-box">
                    <div class="itinerary-item form-group col-md-12" id="itinerary-0">
                        <div class="form-group col-md-1">
                            <label>Type</label>
                            <select id="type" name="itinerary[0][type]" style="width: 100%"
                                    class="itinerary-input form-control type">
                                <option value="ETD" @if ($itinerary->type == 'ETD') selected @endif>ETD</option>
                                <option value="ATD" @if ($itinerary->type == 'ATD') selected @endif>ATD</option>
                                <option value="ETA" @if ($itinerary->type == 'ETA') selected @endif>ETA</option>
                                <option value="ATA" @if ($itinerary->type == 'ATA') selected @endif>ATA</option>
                            </select>
                        </div>

                        <div class="form-group col-md-2">
                            <label>Airport</label>
                            <select id="airport"
                                    name="itinerary[0][airport_icao_string]"
                                    style="width: 100%"
                                    class=" itinerary-input airport form-control">
                                <option value="" disabled>-</option>
                                @foreach(\App\Models\Airport::all() as $airport)
                                    <option value="{{ $airport->icao }}"
                                            @if($airport->icao == $itinerary->airport_icao_string) selected @endif>
                                        {{$airport->icao}}
                                    </option>
                                @endforeach
                            </select>
                        </div>


                        <div class="form-group col-md-2">
                            <label>Date</label>
                            <div class='input-group date datetimepicker' id="time">
                                <input name="itinerary[0][date]" type='text' class="itinerary-input form-control"
                                       value="{{ $itinerary->screenDate() }}"/>
                                <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                            </div>
                        </div>

                        <div class="form-group col-md-2">
                            <label>Time UTC</label>
                            <div class='input-group date onlyTime'>
                                <input name="itinerary[0][time]" type='text' class="itinerary-input form-control"
                                       value="{{ $itinerary->time }}"/>
                                <span class="input-group-addon">
                                <span class="glyphicon  glyphicon-time"></span>
                            </span>
                            </div>
                        </div>


                        <div class="form-group col-md-2">
                            <label>Handler</label>
                            <select id="handler" name="itinerary[0][operator_id]" style="width: 100%"
                                    class="itinerary-input handler form-control">
                                <option value="">-</option>
                                @foreach(\App\Models\Company::all() as $company)
                                    @if($company->name != "default")
                                        <option value="{{ $company->id }}"
                                                @if($itinerary->operator_id == $company->id) selected @endif>
                                            {{ $company->name }}
                                        </option>
                                    @endif
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-2">
                            <label>FBO</label>
                            <select id="fbo_id" name="itinerary[0][fbo_id]" style="width: 100%"
                                    class="itinerary-input form-control">
                                <option value="">-</option>
                                @foreach(\App\Models\Provider::all() as $company)
                                    @if($company->name != "default")
                                        <option value="{{ $company->id }}"
                                                @if($itinerary->fbo_id == $company->id) selected @endif>
                                            {{ $company->name }}
                                        </option>
                                    @endif
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-1">
                            <label>PAX</label>
                            <input type="number" id="pax" name="itinerary[0][pax]" class="itinerary-input form-control" >
                        </div>


                        <div class="form-group" style="margin-top: 29px;">
                            <a href="#" class="remove-block" style="position: absolute;margin: -4px;">X</a>
                        </div>

                    </div>
                </div>

                <div class="box-footer">
                    @if ($creating ?? false)
                        <a href="#" id="add-itinerary" class="btn btn-info">Add Itinerary</a>
                    @endif
                    <button type="submit" id="submit" class="btn btn-success" style=" margin-left: 90%">Save
                    </button>
                </div>
            </div>
        </div>
    </form>
    </form>

    <!-- datetimepicker -->
    <script type="text/javascript">

        var itinerary = {!! json_encode($itinerary) !!}

        $(function () {
            var dateNow = new Date();
            var timezone = dateNow.getTimezoneOffset();
            var serverdate = new Date(dateNow.setMinutes(dateNow.getMinutes() + parseInt(timezone)));
            $('.datetimepicker').datetimepicker({
                toolbarPlacement: 'bottom',
                showClose: true,
                sideBySide: true,
                format: 'DD/MM/YYYY',
            });
            $('.onlyTime').datetimepicker({
                toolbarPlacement: 'bottom',
                showClose: true,
                sideBySide: true,
                format: 'H:ss',
            });


            $('#add-itinerary').on('click', function () {

                var item = $("#itinerary-0").clone();
                var itemsNumber = $('.itinerary-item').length;

                var id = 'itinerary-' + itemsNumber;
                var item = item.attr('id', id);

                item.appendTo(".itinerary-box");

                remplaceId(itemsNumber);

                setItinereryItem();

                loadHandlerOnAirportChange();
            });


            $('.select2').select2();
            loadHandlerOnAirportChange();

            if (itinerary == []) { //Editing
                loadHandlersOfAirpot('itinerary-0', 'MBPV');
            } else {
                loadHandlersOfAirpot('itinerary-0', itinerary.airport_icao_string);
            }

        });

        function loadHandlerOnAirportChange() {
            $('.airport').on('change', function (input) {
                itineraryId = $(this).closest('.itinerary-item').attr('id');

                loadHandlersOfAirpot(itineraryId, $(this).val());
            });
        }

        function loadHandlersOfAirpot(itineraryId, airport_icao)
        {
            
            $.get('{{url('admin/airport/handlers')}}' + '/' + airport_icao, function (data) {
                $("#" + itineraryId + " .handler").html(formatHandlers(data));
                $('.select').select();
            });
        }

        function formatHandlers(data) {
            handlerId = null;
            if (itinerary != []){
                handlerId = itinerary.operator_id;
            }
            var html = '<option value="" > - </option>';

            for (var i = 0; data.length > i; i++) {
                var select = '';
                if (handlerId == data[i].provider.id) {
                    select = 'selected';
                }
                html += '<option value="' + data[i].provider.id + '" '+select+'>' + data[i].provider.name + '</option>';
            }

            return html;
        };

        function setItinereryItem() {
            $('.remove-block').on('click', function () {
                var item = $(this).closest(".itinerary-item");

                if (item.attr('id') != 'itinerary-0') {
                    item.remove();
                }
            });

            $('.datetimepicker').datetimepicker({
                toolbarPlacement: 'bottom',
                showClose: true,
                sideBySide: true,
                format: 'DD/MM/YYYY',
            });
            $('.onlyTime').datetimepicker({
                toolbarPlacement: 'bottom',
                showClose: true,
                sideBySide: true,
                format: 'H:ss',
            });
        };

        function remplaceId(id) {
            $('#itinerary-' + id + ' .itinerary-input').each(function () {
                this.name = this.name.replace(0, id);
            });
        };

        function loadSelect2() {
            if (!$('.select2').select2()) {
                $('.select2').select2();
            }
        };

        function setDefaultItinerary()
        {

        }









    </script>
