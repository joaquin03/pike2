<div id="permit-add-{{$provider['id']}}" class="col-md-12" style="margin-left: -15px;">
    <div class="form-group col-md-2">
        <select class="form-control select2" id="permit_provider_id-{{$provider['id']}}">
            <option value=""> Additional </option>
        @forelse($provider['permits'] as $permit)
            @if(strpos($permit['code'], 'Total')===false
                        && strpos($permit['type'], 'ADD')===false)
                    <option value="{{$permit['id']}}"> {{  $permit['code'].'-'.$permit['type'] }}</option>
                @endif
            @empty
                <option disabled selected>No Permits</option>
            @endforelse
        </select>
    </div>
    <div class="form-group col-md-1">
        <input type="text" id="permit_tax-{{$provider['id']}}" class="form-control" placeholder="Tax">
    </div>
    <div class="form-group col-md-1">
        <input type="text" id="cost-{{$provider['id']}}" class="form-control" placeholder="Cost">
    </div>
    <div class="form-group col-md-2">
        <button  id='add-permanent-permit-additional' onClick="createPermAdditional({{$provider['id']}})" class="btn btn-default add-button">
            Add Additional
        </button>
    </div>
</div>

@push('script_stack')
    <script>
        createPermAdditional = function(id) {
            var code = $('#permit_provider_id-'+id).val();
            var tax = $('#permit_tax-'+id).val();
            var cost = $('#cost-'+id).val();
            var item = [code, tax, cost];

            return createPermitAdditional(item);
        }
    </script>
    <script src="{{ asset('vendor/adminlte/plugins/select2/select2.min.js') }}"></script>
    <script>
        $('.select2').select2();
    </script>
@endpush
