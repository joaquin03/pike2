{{--<form action="{{ route('crud.permission.store', ['operation_id'=>$operation->id]) }}" method="POST" enctype="multipart/form-data">--}}
{{--{{ csrf_field() }}--}}
<form id="form">
    <div class="form-group col-md-6">
        <label class="">Code *</label>
        <input type="text" class="form-control" name="code" id="code" required autocomplete="off">
    </div>
    <div class="form-group col-md-6">
        <label class="">Type *</label>
        <select name="type" class="form-control select2" id="permission_type">
            @foreach(\App\Models\AircraftPermission::$types as $code => $type)
                <option value="{{ $code }}">{{ $code }}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group col-md-12 providers">
        <label class="">Country *</label>
        <div id="AirportProviders" style="display: block;">
            <select name="provider_id" class="form-control select2" id="all_providers_id" required style="width: 100%">
                <option value="" disabled> -</option>
                @foreach(\App\Models\Provider::isCountryEntity()->get() as $provider)
                    @if($provider->name != 'default')
                        <option value="{{ $provider->id }}">{{ $provider->getCountry()}}</option>
                    @endif
                @endforeach
            </select>
        </div>


    </div>


    <div class="form-group col-md-6">
        <label class="">From *</label>
        <select name="icao_from" class="form-control" id="icao_from">
            <option value="" disabled> -</option>
            @foreach($airports as $airport)
                <option value={{$airport->icao}}>{{ $airport->icao }}</option>
            @endforeach
        </select>

    </div>

    <div class="form-group col-md-6">
        <label class="">To *</label>
        <select name="icao_to" class="form-control" id="icao_to">
            <option value="" disabled> -</option>
            @foreach($airports as $airport)
                <option value={{$airport->icao}}>{{ $airport->icao }}</option>
            @endforeach
        </select>

    </div>
    <div class="form-group col-md-12">
        <label class="">Expiration Date </label>
        <div class='input-group date datetimepicker' id="time">
            <input name="expiration_date" id="expiration_date" type='text' class="form-control"
                   value=""/>
            <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
        </div>
    </div>
    <div class="form-group col-md-12">
        <label class="">File *</label>
        <input id="icao_file" type="file" class="" name="file">
    </div>

    <div class="form-group col-md-12">
        <label for=""></label>
        <button type="submit" class="btn bg-maroon" style="margin-top: 25px;">Save</button>

    </div>
</form>

@push('script_stack')
    <script src="{{ asset('vendor/adminlte/plugins/select2/select2.min.js') }}"></script>

    <script type="text/javascript">
        $(function () {
            var dateNow = new Date();
            $('.datetimepicker').datetimepicker({
                toolbarPlacement: 'bottom',
                showClose: true,
                sideBySide: true,
                format: 'DD-MM-YYYY',
                defaultDate: dateNow
            });
        });


        $('#form').submit(function (event) {
            event.preventDefault();
            var form = $(this);
            var formData = false;
            if (window.FormData) {
                formData = new FormData(form[0]);
            }

            $.ajax({
                type: "POST",
                url: '{{url("/admin/quotation/")}}' + '/' + '{{$operation->id}}' + '/permission/create',
                processData: false,
                cache: false,
                contentType: false,
                data: formData ? formData : form.serialize(),
            }).success(function (data) {
                console.log(data.data.permissions);
                reloadOperationPermits(data.data.permissions);
            })
                .error(function (data) {
                    PNotify.removeAll();
                    new PNotify({
                        text: "error",
                        type: "error"
                    });
                });
        });


    </script>
@endpush
