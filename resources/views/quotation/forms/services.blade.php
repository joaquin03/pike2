<div class="row">
    <div class="col-md-4">
        <label class="">Provider *</label>
        <select class="form-control select2" name="provider_id" id="provider_id">
            <option value="" disabled>-</option>
            @foreach(\App\Models\Provider::all() as $provider)
                @if(in_array($itinerary->airport->icao, $provider->airports->pluck('icao')->toArray())
                || strtolower($provider->country)==='ww'
                 || $provider->is_ww_provider)
                    <option value="{{ $provider->id }}">{{ $provider->name }}</option>
                @endif
            @endforeach
        </select>
    </div>
    <div class="col-md-4">
        <label>Service *</label>
        <select class="form-control select2 " name="service_id" required id="service_id">
            <option value="" disabled> -</option>
        </select>
    </div>
    <div class="col-md-4">
        <label for=""></label>
        <button id="add-service-button" type="button" class="btn btn-success" onClick="addService({{$itinerary->id}})"
                style="margin-top: 25px;">
            Add Service
        </button>

        <button id="add-service-button" type="button" class="btn btn-warning"
                onClick="addServiceAdditional({{$itinerary->id}})"
                style="margin-top: 25px;">
            Add additional
        </button>
    </div>

</div>

<input type="hidden" name="provider_id" value="" id="provider_id">


<script src="{{ asset('vendor/adminlte/plugins/select2/select2.min.js') }}"></script>

<script>


    function addService(itineraryId) {
        addItineraryServicesCount(itineraryId);


        $.ajax({
            type: "POST",
            url: '{{url("/api/itinerary/")}}' + '/' + itineraryId + '/service/',
            data: {
                'service_id': $("#service_id").val(),
                'provider_id': $("#provider_id").val(),
                'service_origin': 'Quotation',
            }
        }).success(function (data) {
            getItineraryProviderServices(itineraryId);
        })
            .error(function (data) {
                PNotify.removeAll();
                new PNotify({
                    text: data.responseJSON.message,
                    type: "error"
                });
            });
    };


    function addServiceAdditional(itineraryId) {
        addItineraryServicesCount(itineraryId);

        $.ajax({
            type: "POST",
            url: '{{url("/api/itinerary/")}}' + '/' + itineraryId + '/service/',
            data: {
                'service_id': $("#service_id").val(),
                'provider_id': $("#provider_id").val(),
                'is_additional': 1,
                'service_origin': 'Quotation',
            }
        }).success(function (data) {
            getItineraryproviderServicesAdditionals(itineraryId);
        })
            .error(function (data) {
                PNotify.removeAll();
                new PNotify({
                    text: data.responseJSON.message,
                    type: "error"
                });
            });
    };


    reloadItineraryServices = function (providerServices) {
        location.reload();
    };

    $(function () {
        function loadServices() {
            var provider_id = $("#provider_id").val();
                $.get('{{url('api/provider/')}}' + '/' + provider_id + '/quotation-service?', function (data) {
                $("#service_id").html(formatServices(data));
                $("#provider_id").val(provider_id);
                $('.select2').select2();
            });
        }

        function formatServices(data) {
            var html = '<option value="" disabled> - </option>';

            for (var i = 0; data.length > i; i++) {
                html += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
            }
            return html;
        }

        loadServices();

        $('#provider_id').on('change', function () {
            loadServices()
        });

        $('.select2').select2();
    });

    function addItineraryServicesCount(itineraryId) {
        console.log(parseInt($('#services-count-' + itineraryId).text()));
        if (parseInt($('#services-count-' + itineraryId).text()) === 0) {
            $('#services-count-' + itineraryId).show();
            document.getElementById('itineraryWithServices-' + itineraryId).style.backgroundColor = "#00a65a";
            document.getElementById('itineraryWithServices-' + itineraryId).style.borderColor = "#00a65a";
            document.getElementById('truck-' + itineraryId).style.color = "white";
        }
        $('#services-count-' + itineraryId).text(parseInt($('#services-count-' + itineraryId).text()) + 1);
    }

</script>
