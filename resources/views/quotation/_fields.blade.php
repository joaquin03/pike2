{{ csrf_field() }}

@section('after_styles')
    <script src="{{ asset('vendor/adminlte/plugins/select2/select2.js') }}"></script>
    <script type="text/javascript"
            src="{{ asset('vendor/adminlte/plugins/daterangepicker/moment.min.js') }}"></script>
    <script src="{{ asset('vendor/adminlte/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    <script type="text/javascript"
            src="https://cdn.jsdelivr.net/bootstrap.datetimepicker/4.17.42/js/bootstrap-datetimepicker.min.js"></script>
    <script src="{{ asset('/js/handsontable.full.min.js') }}"></script>

@endsection


<div class="tab-content" style="">
    <div class="tab-pane edit active" id="tab_general" style="padding: 10px;">
        @include('quotation.fields.general')
    </div>
    <div class="tab-pane edit" id="tab_documents" style="padding: 10px;">
        @include('quotation.fields.documents')
    </div>
</div>





