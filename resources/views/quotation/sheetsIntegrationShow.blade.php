@extends("backpack::layout")
@section('header')

    <section class="content-header">
        <h1>
            Quotation Data Generation
        </h1>

        <br><br>

    </section>
    <style>
        a {
            color: #A9A9A9
        }

        a:hover {
            color: #696969;
        }
    </style>

@endsection
@section('content')


    <div class="col-md-2">
        <label for="operation_data">Operation Data</label>
        <select id='operation_data' class="form-control col-md-6">
            <option value="Handling">Handling</option>
            <option value="Flight Support">Flight Support</option>
        </select>

    </div>

    <div class="col-md-12" style="text-align: center; margin: 30px">


        <?php

        for ($row = 4; $row < 16; $row++) {
            echo "<div style = 'clear:both'>";
            for ($col = 1; $col < 3; $col++) {
                echo "<input type='text' style='float:left;' name='$row:$col' id='$row:$col'>";
            }
            echo "</div>";
        }

        ?>
    </div>

    <br><br>

    <button id="update-button" class="btn btn-warning" onclick="handleSaveClick()">Save Data</button>
    <button id="generate-button" class="btn btn-danger" onclick="readSheetAndGenerate()">Generate Services</button>
@endsection
