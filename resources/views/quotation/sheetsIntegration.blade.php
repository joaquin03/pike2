<script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>

<!--
BEFORE RUNNING:
---------------
1. If not already done, enable the Google Sheets API
   and check the quota for your project at
   https://console.developers.google.com/apis/api/sheets
2. Get access keys for your application. See
   https://developers.google.com/api-client-library/javascript/start/start-js#get-access-keys-for-your-application
3. For additional information on authentication, see
   https://developers.google.com/sheets/api/quickstart/js#step_2_set_up_the_sample
-->
<script>

    var ssID = '1k-8SxGcumZnESt2vYhwYQcZZr0cZpHVI_iM99PTvePA';
    var rng= 'URUGUAY' ;



    function readSheetApi() {
        var params = {
            // The ID of the spreadsheet to re
            // ieve data from.
            spreadsheetId: ssID,

            // The A1 notation of the values to retrieve.
            range: rng,
        };

        var request = gapi.client.sheets.spreadsheets.values.get(params);
        request.then(function(response) {
            // TODO: Change code below to process the `response` object:
            console.log(response.result);
             populateSheet(response.result);
        }, function(reason) {
            console.error('error: ' + reason.result.error.message);
        });
    }

       function updateSheetApi(){
           var vals = new Array(16);
           var col = 2;

           for(var row = 0; row < 16 ; row++){

               vals[row] = new Array(1);

               if(row == 2){
                   vals[row][col] = document.getElementById('operation_data').value;
               }

               var aux = (document.getElementById(row+":"+col)) ;
               if(aux){
                   vals[row][col] = aux.value;
               }

           }


           var params = {
               // The ID of the spreadsheet to retrieve data from.
               spreadsheetId: ssID,

               // The A1 notation of the values to retrieve.
               range: rng,

               valueInputOption: 'RAW',
           };

           var valueRangeBody = { "values" : vals };

           var request = gapi.client.sheets.spreadsheets.values.update(params, valueRangeBody);
           request.then(function(response) {
               // TODO: Change code below to process the `response` object:
               console.log(response.result);
           }, function(reason) {
               console.error('error: ' + reason.result.error.message);
           });

       }

    function initClient() {
        var API_KEY = 'AIzaSyBJIjRLC7T4QFv71B_GZ8rz3MiaS3dc40I';  // TODO: Update placeholder with desired API key.

        var CLIENT_ID = '901856530835-4ou6vlrpurkqq2461q7q06vljb8rke47.apps.googleusercontent.com';  // TODO: Update placeholder with desired client ID.

        // TODO: Authorize using one of the following scopes:
        //   'https://www.googleapis.com/auth/drive'
        //   'https://www.googleapis.com/auth/drive.file'
        //   'https://www.googleapis.com/auth/drive.readonly'
        //   'https://www.googleapis.com/auth/spreadsheets'
        //   'https://www.googleapis.com/auth/spreadsheets.readonly'
        var SCOPE = 'https://www.googleapis.com/auth/spreadsheets';

        gapi.client.init({
            'apiKey': API_KEY,
            'clientId': CLIENT_ID,
            'scope': SCOPE,
            'discoveryDocs': ['https://sheets.googleapis.com/$discovery/rest?version=v4'],
        }).then(function() {
            gapi.auth2.getAuthInstance().isSignedIn.listen(updateSignInStatus);
            updateSignInStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
        });
    }

    function handleClientLoad() {
        gapi.load('client:auth2', initClient);
    }

    function updateSignInStatus(isSignedIn) {
        if (isSignedIn) {
            readSheetApi();
        }
    }


    function handleSignOutClick(event) {
        gapi.auth2.getAuthInstance().signOut();
    }

    function handleSaveClick() {
        updateSheetApi();
    }

    function populateSheet(result){
        for (var row = 4; row<16; row++){
            for (var col = 1; col<3; col++){
                document.getElementById(row+":"+col).value = result.values[row][col];
            }
        }
    }



    function readSheetAndGenerate() {
        var params = {
            // The ID of the spreadsheet to retrieve data from.
            spreadsheetId: ssID,

            // The A1 notation of the values to retrieve.
            range: rng,
        };

        var request = gapi.client.sheets.spreadsheets.values.get(params);
        request.then(function(response) {
            obtainServicesData(response.result);
        }, function(reason) {
            console.error('error: ' + reason.result.error.message);
        });
    }

    function obtainServicesData(data){
        var values = data.values;
        var services =[];

        for (var i = 3; i<19 ; i++) //'Tarifario' quote dimensions
        {
            var id = values[i][4].split('-')[0].trim();
            var amount =   values[i][5].replace('.','');

            if ( _isNumeric(id) && validAmount(amount)){
                services.push({
                    'service_provider_id': id,
                    'amount': amount,
                    'service_origin': 'Quotation',
                })
            }
        }

        generateServices( {{$itinerary->id}}, services);
    }

    function _isNumeric(value){
        return !isNaN(parseFloat(value)) && isFinite(value);
    }

    function validAmount(value){
        return !(value=="N/A" || value=="TBA");
    }

    function generateServices(itineraryId, services) {
        $.ajax({
            type: "POST",
            url: '{{url("/api/itinerary/")}}' + '/' + itineraryId + '/generate-quotation-services/',
            data: {'services': services, },
        }).success(function (data) {
            url = "{{ url('admin/quotation')}}" + "/" + {{$itinerary->operation_id}} +'/edit/';
            window.location.href = url;
        })
        .error(function (data) {
            PNotify.removeAll();
            new PNotify({
                text: data.responseJSON.message,
                type: "error"
            });
        });
    };

    $( document ).ready(function() {
        setTimeout(function(){
            gapi.auth2.getAuthInstance().signIn();
        }, 2000);
    });

</script>
<script async defer src="https://apis.google.com/js/api.js"
        onload="this.onload=function(){};handleClientLoad()"
        onreadystatechange="if (this.readyState === 'complete') this.onload()">
</script>

@include('quotation.sheetsIntegrationShow')
