{{-------------------------------------}}
{{--         SERVICES TABLE          --}}
{{-------------------------------------}}

<div class="row">
    <div class="col-12">


        <table class="table table-sm  table-borderless services-table-{{$itineraryCount}}">
            <thead>
            <tr>
                <th style="width: 500px;color: white;-webkit-print-color-adjust:exact;background-color: #507d98 !important">
                    {{$itinerary->airport->icao}}@if($itinerary->fbo) - FBO: {{$itinerary->fbo->name}} @endif</th>
                <th style="width: 300px;color: white;-webkit-print-color-adjust:exact;background-color: #507d98 !important">
                    Unit
                </th>
                <th style="width: 100px;color: white;-webkit-print-color-adjust:exact;background-color: #507d98 !important">
                    Price
                </th>
            </tr>
            </thead>

            <tbody>

            {{--3rd party services--}}
            @foreach($itinerary->itineraryProviderServicesChildsNotByPikeWithoutFuel()->get() as $ips)
                @if($itinerary->isHandlingQuotation())
                    @if($ips->service_id != Config::get('constants.handling_services_id'))
                        {{-- Busyness rule => if the service type is handling -> show handling service as a pike service --}}
                        <tr class="service-from-itinerary-{{$itineraryCount}}">
                            <td> {{$ips->service->name}} </td>
                            <td> {{$ips->service_unit}} </td>
                            <td style="text-align: right"> {{ number_format($ips->getFinalCostQuotationWithoutAdmFee(),2) }} </td>
                        </tr>
                    @endif
                @else
                    <tr class="service-from-itinerary-{{$itineraryCount}}">
                        <td> {{$ips->service->name}} </td>
                        <td> {{$ips->service_unit}} </td>
                        <td style="text-align: right"> {{ number_format($ips->getFinalCostQuotationWithoutAdmFee(),2) }} </td>
                    </tr>
                @endif
            @endforeach

            {{--Services After Sub Total--}}
            @if( !$itinerary->itineraryProviderServicesChildsByPike()->get()->isEmpty()
                || $itinerary->getItinieraryServicesAdmFees() != 0
                || $quotation->payedWithCreditCard())

                {{--SubTotal--}}
                <tr class="service-from-itinerary-{{$itineraryCount}}">
                    <th colspan="2"
                        style="text-align: right;-webkit-print-color-adjust:exact;background-color: #d9d9d9 !important">
                        <b> Subtotal </b></th>
                    <th style="text-align: right;-webkit-print-color-adjust:exact;background-color: #d9d9d9 !important">
                        <b> {{ number_format($itinerary->getItinerarySubtotalCostWithoutAdmFee(),2) }} </b></th>
                </tr>


                {{--Adm Fee--}}
                @if($itinerary->getItinieraryServicesAdmFees() != 0)
                    <tr class="service-from-itinerary-{{$itineraryCount}}">
                        <td> Administration Fee</td>
                        <td> Per Operation</td>
                        <td style="text-align: right"> {{ number_format($itinerary->getItinieraryServicesAdmFees(),2) }} </td>
                    </tr>

                @endif

                {{--  For Handling Service Type  --}}
                @if($quotation->quotation_services_type == 'Handling' )
                    {{-- Busyness rule => if the service type is handling -> show basic handling service as a third party --}}
                    <tr class="service-from-itinerary-{{$itineraryCount}}">
                        <td> Handling Services</td>
                        <td> Per Operation</td>
                        <td style="text-align: right"> {{ number_format($itinerary->getItineraryHandlingService(),2) }} </td>
                    </tr>
                @endif

                {{--Pike Aviation services--}}
                @foreach($itinerary->itineraryProviderServicesChildsByPike()->get() as $ips)
                    <tr class="service-from-itinerary-{{$itineraryCount}}">
                        <td> {{$ips->service->name}} </td>
                        <td> {{$ips->service_unit}} </td>
                        <td style="text-align: right"> {{ number_format($ips->getFinalCostQuotationWithoutAdmFee(),2) }} </td>
                    </tr>
                @endforeach

                {{--Convenience Fee--}}
                @if($quotation->extraData->payment == 'Credit Card' )
                    <tr class="service-from-itinerary-{{$itineraryCount}}">
                        <td> Convenience FEE</td>
                        <td> Per Opertaion</td>
                        <td style="text-align: right"> {{number_format($itinerary->getQuotationCreditNoteSurcharge($itinerary->itineraryCostWithoutChargesWithoutFuel()), 2)}} </td>
                    </tr>

                @endif
            @endif

            <tr>
                <th colspan="2"
                    style="text-align: right;-webkit-print-color-adjust:exact;background-color: #d7e5f5 !important">
                    <b> Total </b></th>
                <th style="text-align: right;-webkit-print-color-adjust:exact;background-color: #d7e5f5 !important">
                    <b> {{ number_format($itinerary->getItineraryTotalCost(),2) }} </b></th>
            </tr>


            </tbody>

        </table>

        <br>
        <br>
<h

        @include('quotation.exportable.repetitive.fuel-info')


        <ul class="extra-info-{{$itineraryCount}} right-custom" style="float: right; font-size: 12px;">
        </ul>

        <div style="margin-top: 30px; margin-bottom: 30px;" class="noPrintPageButton">
            <button id="editExtras" class="btn btn-info" onclick="editExtras({{$itineraryCount}})"> Add Extra
                Info
            </button>
            <button id="saveExtras" class="btn btn-success" onclick="saveExtras({{$itineraryCount}})">Save
            </button>
        </div>

        <div class="services-ending-{{$itineraryCount}}"></div>

        {{--@if($itinerary->servicesTotalCount() > 18)--}}
        {{--<div class="pagebreak"></div>--}}
        {{--@endif--}}

        @include('quotation.exportable.repetitive.additionals-info')


    </div>
</div>

<style>
    @media print {
        .noPrintPageButton {
            display: none;
        }
    }

</style>


<script>

    function editExtras(x) {
        var list = document.querySelector('.extra-info-' + x);
        list.innerHTML = '<li class="right-custom right-custom-notes-' + x + '"></li>';
        list.contentEditable = true;
        list.focus();

    }

    function saveExtras(x) {
        var pageHeight = 200;

        var list = document.querySelector('.extra-info-' + x);
        list.contentEditable = false;
    }

    function calculateItineraryHeight(x) {
        console.log($('.right-custom-notes-'.x).length);
    }


</script>


