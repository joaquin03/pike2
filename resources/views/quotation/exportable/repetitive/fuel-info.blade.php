{{-------------------------------------}}
{{--         SERVICES TABLE          --}}
{{-------------------------------------}}

<div class="row">
    <div class="col-12">


        <table class="table table-sm  table-borderless fuel-table-{{$itineraryCount}}">
            <thead>
            <tr>
                <th style="width: 500px;color: white;-webkit-print-color-adjust:exact;background-color: #507d98 !important">
                    FUEL </th>
                <th style="width: 300px;color: white;-webkit-print-color-adjust:exact;background-color: #507d98 !important">
                    Unit
                </th>
                <th style="width: 100px;color: white;-webkit-print-color-adjust:exact;background-color: #507d98 !important">
                    Price
                </th>
            </tr>
            </thead>

            <tbody>

            {{--3rd party services--}}
            @foreach($itinerary->itineraryFuelServices()->get() as $ips)
                <tr class="service-from-itinerary-{{$itineraryCount}}">
                    <td> {{$ips->service->name}} </td>
                    <td> {{$ips->service_unit}} </td>
                    <td style="text-align: right"> {{ number_format($ips->getFinalCostQuotation(),2) }} </td>
                </tr>
            @endforeach


            <tr>
                <th colspan="2"
                    style="text-align: right;-webkit-print-color-adjust:exact;background-color: #d7e5f5 !important">
                    <b> Total </b></th>
                <th style="text-align: right;-webkit-print-color-adjust:exact;background-color: #d7e5f5 !important">
                    <b> {{ number_format($itinerary->getItineraryFuelCost(),2) }} </b></th>
            </tr>


            </tbody>

        </table>




    </div>
</div>

<style>
    @media print {
        .noPrintPageButton {
            display: none;
        }
    }

</style>



