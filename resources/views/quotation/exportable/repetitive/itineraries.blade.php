<div class="container" style="padding-top: 180px; padding-bottom: 100px">
    <div class="row">
        <div class="col-12">
            <table class="table table-sm table-borderless" style="page-break-inside: avoid">
                <tbody>
                @if($itinerary->isArrival())
                    {{---------------INFO--------------}}
                    @include('quotation.exportable.general.general-info')
                    {{---------------LEG--------------}}
                    @include('quotation.exportable.general.icao-leg')
                    {{---------------SERVICES--------------}}
                    @include('quotation.exportable.repetitive.services-info')
                @endif
                </tbody>
                <br>
            </table>
        </div>
    </div>
</div>
