{{--------------------------------------------}}
{{--ITINERARY ADDITIONALS & SERVICES BY PIKE--}}
{{--------------------------------------------}}

@if(!$quotation->itinerariesProviderServicesAdditionalsQuotation()->get()->isEmpty())
    @if(!$itinerary->itineraryProviderServicesAdditionalsQuotation()->get()->isEmpty())

                <table class="table additionals-table-{{$itineraryCount}} table-sm table-borderless overflow-table" style="
                   margin-bottom: 0;
                   {{--@if($itinerary->servicesTotalCount() > 18) margin-top:200px @else margin-top:50px @endif !important;--}}
                        ">

                    <thead>
                    <tr>
                        <div class="col-6">
                            <th style="width: 500px;-webkit-print-color-adjust:exact;background-color: #d9d9d9 !important" >Additional services (on request)</th>
                        </div>
                        <div class="col-3">
                            <th style="width: 300px;-webkit-print-color-adjust:exact;background-color: #d9d9d9 !important" >Unit</th>
                        </div>
                        <div class="col-3">
                            <th style="width: 100px;-webkit-print-color-adjust:exact;background-color: #d9d9d9 !important" >Price</th>
                        </div>
                    </tr>
                    </thead>

                    <tbody>
                    {{--3rd party services--}}
                    @foreach($itinerary->itineraryProviderServicesAdditionalsQuotation()->get() as $ips)
                        <tr class="service-from-itinerary-{{$itineraryCount}}">
                            <td> {{$ips->service->name}} </td>
                            <td> {{$ips->service_unit}} </td>
                            <td style="text-align: right"> {{ number_format($ips->getQuotationFinalPrice(),2)    }} </td>
                        </tr>
                    @endforeach

                    {{--3rd party permits--}}
                    @foreach($quotation->permitsAdditionalsQuotation()->get() as $permit)
                        <tr class="service-from-itinerary-{{$itineraryCount}}">
                            <td> Permit: {{$permit->code}} - {{$permit->type}}</td>
                            <td> {{$permit->icao_from}} - {{$permit->icao_to}} </td>
                            <td style="text-align: right"> {{ number_format($permit->getQuotationFinalPrice(),2) }} </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
    @endif
@endif
