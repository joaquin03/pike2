<div style="margin-top: 0;">
    <img src="{{"http://$_SERVER[HTTP_HOST]/" . "encabezado.jpg"}}" class="img-fluid" alt="header-img">
</div>
<div class="container" style="padding-top: 0">
    @include('quotation.exportable.general.general-info')
    <div class="col-md-12" style=" font-size: 12px;">
        <div class="col-md-12">
            <b>Notes</b>
        </div>
        <div class="col-md-12">

            <ul class="itinerary-notes">
                <li><span class="edit-list"> Administration fee - 15% over third parties - already included  </span>
                </li>
                <li><span class="edit-list"> Local taxes already included</span></li>
                <li>
                    <span class="edit-list"> Payment method. Wire transfer or credit card (100% in advance is reuqired)</span>
                </li>
                <li><span class="edit-list">  Fuel price is subject to change at any time. Valid thru 26/05/2019. Please check before operation</span>
                </li>

            </ul>
        </div>
    </div>
    <div style="margin-top: 30px">
        <button id="editNote" class="btn btn-info noPrintPageButton" onclick="editNotes()"> Edit Notes</button>
        <button id="saveNote" class="btn btn-success noPrintPageButton" onclick="saveNotes()">Save</button>
    </div>


    <div class="row">
        <div class="col-md-12 " style="text-align: right">
            <div class="darkblue-container-high" style="padding-right: 30px;">
                <h4><b>Total trip: USD {{$quotation->getFormattedQuotationTotalCost()}}</b></h4>
            </div>
        </div>
    </div>

    <br>

    @include('quotation.exportable.banks.info')
</div>


<script>

    var list = document.querySelector('.itinerary-notes');

    function editNotes() {
        list.contentEditable = true;
        list.focus();
    }

    function saveNotes() {
        list.contentEditable = false;
    }

</script>
