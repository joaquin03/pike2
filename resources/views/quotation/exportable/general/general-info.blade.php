<div class="">
    <h5 style="text-align: right">
        <br><i>{{\Carbon\Carbon::now()->format('M d, Y')}}
        </i><br>
    </h5>
    <div class="gray-container"><b> Customer & Flight details</b></div>
    <br>


    {{-------------------------------------}}
    {{--Customer & Flight Details Section--}}
    {{-------------------------------------}}


    <div class="row">

        <div class="col-md-6">
            <div class="row">

                <div class="col-md-2">
                    <div class="col-md-12">
                        <b>Company:</b>
                    </div>

                    <div class="col-md-12">
                        <b>Operator:</b>
                    </div>

                    <div class="col-md-12">
                        <b>Aircraft:</b>
                    </div>

                    <div class="col-md-12">
                        <b>MTOW:</b>
                    </div>

                    <div class="col-md-12">
                        <b>Type:</b>
                    </div>

                    @if($quotation->quotation_services_type === "Vip Assistance")
                        <div class="col-md-12">
                            <b> Type of Vip Assistance: </b>
                        </div>
                    @endif
                </div>

                <div class="col-md-10">
                    <div class="col-md-12">
                        {{$quotation->billingCompany->name ?? '-'}}
                    </div>

                    <div class="col-md-12">
                        {{$quotation->aircraftOperator ?  $quotation->aircraftOperator->name : '-'}}
                    </div>

                    <div class="col-md-12">
                        {{ $quotation->aircraftType ? $quotation->aircraftType->name : '-' }}
                    </div>

                    <div class="col-md-12">
                        {{$quotation->aircraftType ? $quotation->aircraftType->MTOW. ' ' .$quotation->aircraftType->MTOW_Type : '-'}}
                    </div>

                    <div class="col-md-12">
                        {{$quotation->extraData ? $quotation->extraData->type_of_flight : '-'}}
                    </div>

                    @if($quotation->quotation_services_type === "Vip Assistance")
                        <div class="col-md-12">
                            {{$quotation->extraData ? $quotation->extraData->type_of_vip_assistance_flight : ''}}
                        </div>
                    @endif
                </div>

            </div>

        </div>


        <div class="col-md-6">
            <div class="row">

                <div class="col-md-5" style="text-align: right; margin-left: -70px;">
                    <div class="col-md-12">
                        <b>Route:</b>
                    </div>
                    <div class="col-md-12">
                        <b>Quote number:</b>
                    </div>
                    <div class="col-md-12">
                        <b>Contact Name:</b>
                    </div>
                    <div class="col-md-12">
                        <b>E-mail:</b>
                    </div>

                    @if($quotation->extraData->fbo)
                        <div class="col-md-12">
                            <b>FBO:</b>
                        </div>
                    @endif
                    <div class="col-md-12">
                        <b>Phone number:</b>
                    </div>

                </div>

                <div class="col-md-7">
                    <div class="col-md-12">
                        {{$quotation->operationAirportsList() ?? '-'}}
                    </div>
                    <div class="col-md-12">
                        {{$quotation->code}}
                    </div>

                    <div class="col-md-12">
                        {{ $extended->contact ? $extended->contact->name . ' ' . $extended->contact->surname : '-' }}
                    </div>

                    <div class="col-md-12">
                        {{ $extended->contact ? $extended->contact->emails ?
                        array_key_exists('email', $extended->contact->emails[0]) ? $extended->contact->emails[0]['email'] : ' ' : ' ' : '-' }}
                    </div>

                    @if($quotation->extraData->fbo)
                        <div class="col-md-12">
                            {{$quotation->extraData->fbo->name . ' - ' . $quotation->extraData->fbo->getCountry() ?? '-'}}
                        </div>
                    @endif

                    <div class="col-md-12">
                        {{ $extended->contact ? $extended->contact->phones ?
                         array_key_exists('phone', $extended->contact->phones[0]) ? $extended->contact->phones[0]['phone'] : '' : '' : '-' }}
                    </div>

                </div>
            </div>

        </div>
    </div>

    <br><br>

    {{-------------------------------------}}
    {{--           SEPARATOR             --}}
    {{-------------------------------------}}

    <div class="row">
        <div class="col-md-12 " style="text-align: center">
            <div class="skyblue-container">
                <h4><b>QUOTE SHEET</b></h4>
            </div>
        </div>
    </div>
    <br>
</div>