
    @if ($quotation->quotation_services_type == 'Vip Assistance')

        @if ($quotation->extraData->type_of_flight === 'Arrival' )
            <div class="col-12">
                <b>ARRIVAL:</b>
                <ul>
                    <li> Dedicated agent will meet the passengers at their gate holding a name-sign.</li>
                    <li> Passengers will be escorted through immigration, baggage reclaim and customs.</li>
                    <li> Passengers will then be escorted through to their onward mode of transportation.</li>
                </ul>
            </div>

        @elseif ($quotation->extraData->type_of_flight === 'Departure' )
            <div class="col-12">
                <b>DEPARTURE:</b>
                <ul>
                    <li> Dedicated agent will as we meet the passengers at their vehicle.</li>
                    <li> Passengers will be assisted with check in.</li>
                    <li> Passengers will then be escorted through the departure security point.</li>
                </ul>
            </div>

        @elseif ($quotation->extraData->type_of_flight === 'Transit' )
            <div class="col-12">
                <b>TRANSIT:</b>
                <ul>
                    <li> Dedicated agent will meet the passengers at their gate holding a name-sign.</li>
                    <li> Passengers will be escorted through the airport.</li>
                    <li> Agent will then escort passengers to their lounge (if applicable) and then to boarding gate.
                    </li>
                </ul>
            </div>
        @endif

    @endif