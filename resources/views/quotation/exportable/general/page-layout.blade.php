@foreach($quotation->itineraries as $count => $itinerary)
    @if($count != $quotation->itineraries->count())
        @if($itinerary->isArrival() && !$itinerary->isEmpty())

            @php $itineraryCount++ @endphp

            <div class="pagebreak"></div>
            {{-- Header ----------------------------------------------------------------------------------}}
            <div class="" style="position: fixed;top: 0;">
                <img src="{{"http://$_SERVER[HTTP_HOST]/" . "encabezado.jpg"}}" class="img-fluid" alt="header-img">
            </div>
            {{-- End Header ------------------------------------------------------------------------------}}

            {{-- Itineraries ----------------------------------------------------------------------------------}}
            @include('quotation.exportable.repetitive.itineraries')
            {{--- End Itineraries------------------------------------------------------------------------------}}



            {{-- Footer ----------------------------------------------------------------------------------}}
            <div class="" style="position: fixed;bottom: 0;">
                <img src="{{"http://$_SERVER[HTTP_HOST]/" ."/pie.jpg"}}" style="width: 1903px;" class="img-fluid" alt="header-img">
            </div>
            {{-- End Footer ------------------------------------------------------------------------------}}

        @endif
    @endif
@endforeach
<div class="pagebreak"></div>
@include('quotation.exportable.general.permits')
@include('quotation.exportable.general.notes')
<div class="" style="position: fixed;bottom: 0;">
    <img src="{{"http://$_SERVER[HTTP_HOST]/" ."/pie.jpg"}}" style="width: 1903px;" class="img-fluid" alt="header-img">
</div>

{{--@include('quotation.exportable.page2.vip-assistance')--}}