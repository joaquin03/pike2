{{-------------------------------------}}
{{--         ICAO LEG TABLE          --}}
{{-------------------------------------}}

<table class="table table-sm  table-bordered">
    <tbody>
    <tr>
        <th style="color: white;-webkit-print-color-adjust:exact;background-color: #507d98 !important; width: 500px;">
            LEG
        </th>

        <td class="text-center">  {{$itinerary->itineraryAirportsIcaos() ?? '-'}} </td>
    </tr>
    <tr>
        <th style="color: white;-webkit-print-color-adjust:exact;background-color: #507d98 !important; width: 500px;">
            ICAO assistance
        </th>
        <td class="text-center">
            <b>{{$itinerary->airport_icao_string ?? ''}}</b>
        </td>
        {{--ICAO Assistance = icao del primer arrival--}}
    </tr>
    <tr>
        <th style="color: white;-webkit-print-color-adjust:exact;background-color: #507d98 !important; width: 500px;">
            Passengers
        </th>
        <td class="text-center">

            <input type="hidden" {{ $paxIn = $quotation->getArrivalOriginDeparturePax($itinerary)}}
            {{ $paxOut =$itinerary->pax ?? null }}/>

            @if((!$paxIn && !$paxOut) || ($paxIn == 0 && $paxOut == 0))
                -
            @else
                IN: {{$paxIn ?? '-'}}
                /
                OUT: {{$paxOut ?? '-'}}

        @endif
    </tr>
    </tbody>
</table>
<br>
