@if(!$quotation->aircraftPermission()->get()->isEmpty())
    <div style="margin-top: 0;">
        <img src="{{"http://$_SERVER[HTTP_HOST]/" . "encabezado.jpg"}}" class="img-fluid" alt="header-img">
    </div>
    <div style="margin-top: 0;">
        <img src="{{"http://$_SERVER[HTTP_HOST]/" . "encabezado.jpg"}}" class="img-fluid" alt="header-img">
    </div>
    <div class="container" style="padding-top: 0">
        @include('quotation.exportable.page1.general-info')
        <div>
            <div style="text-align: center">
                <b>Permits</b>
            </div>
            <div>
                <table class="table table-sm  table-borderless permits-table">
                    <thead>
                    <tr>
                        <th style="width: 500px;color: white;-webkit-print-color-adjust:exact;background-color: #507d98 !important">
                            Provider
                        </th>
                        <th style="width: 300px;color: white;-webkit-print-color-adjust:exact;background-color: #507d98 !important">
                            Code
                        </th>
                        <th style="width: 300px;color: white;-webkit-print-color-adjust:exact;background-color: #507d98 !important">
                            From
                        </th>
                        <th style="width: 300px;color: white;-webkit-print-color-adjust:exact;background-color: #507d98 !important">
                            To
                        </th>
                        <th style="width: 100px;color: white;-webkit-print-color-adjust:exact;background-color: #507d98 !important">
                            Price
                        </th>
                    </tr>
                    </thead>

                    <tbody>

                    {{--3rd party permits--}}
                    @foreach($quotation->permitsChildsNotByPike()->get() as $permit)
                        <tr>
                            <td> {{$permit->provider->name}} </td>
                            <td> {{$permit->code}} </td>
                            <td> {{$permit->icao_from}} </td>
                            <td> {{$permit->icao_to}} </td>
                            <td style="text-align: right"> {{ number_format($permit->getQuotationFinalPrice(),2) }} </td>
                        </tr>
                    @endforeach
                    @if(!$quotation->permitsChildsByPike()->get()->isEmpty())
                        <tr>
                            <th colspan="3"
                                style="text-align: right;-webkit-print-color-adjust:exact;background-color: #d9d9d9 !important">
                                Permits by Pike Aviation
                            </th>
                        </tr>
                        @foreach($quotation->permitsChildsByPike()->get() as $permit)
                            <tr>
                                <td> {{$permit->provider->name}} </td>
                                <td> {{$permit->code}} </td>
                                <td style="text-align: right"> {{ number_format($permit->getQuotationFinalPrice(),2) }} </td>
                            </tr>
                        @endforeach
                    @endif
                    <tr>
                        <th colspan="4"
                            style="text-align: right;-webkit-print-color-adjust:exact;background-color: #d7e5f5 !important">
                            <b> Total </b></th>
                        <th style="text-align: right;-webkit-print-color-adjust:exact;background-color: #d7e5f5 !important">
                            <b> {{ number_format($quotation->getQuotationTotalPermitsCost(),2) }} </b></th>
                    </tr>


                    </tbody>

                </table>


            </div>
        </div>
    </div>
    <div class="pagebreak"></div>


    <script>

        var list = document.querySelector('.itinerary-notes');

        function editNotes() {
            list.contentEditable = true;
            list.focus();

        }

        function saveNotes() {
            list.contentEditable = false;
        }

    </script>

@endif