<div class="lightgray-container text-center" style="color: #989898;">
    <br>
    @include('quotation.exportable.banks.'.  str_replace(' ', '', $quotation->extraData->payment_method ?? 'Santander') )
    <br>
</div><br>

<div style="font-size:10px; ">
    <b>Proposal conditions:</b>
    <div class="tab">
        1. The above estimation is a best guess total price to complete the work stated, and not a bill. Should prices
        change or additional services are required, this will be accordantly
        informed.<br>
        2. This proposal is valid for acceptance by client during 15 days, after which date it shall be null and void,
        and may be accepted by returning a confirmation of acceptance. Such
        acceptance is deemed to be an acceptance and constitutes a binding agreement between the parties for the matters
        set forth herein.
        <br>
        3. For prepay accounts Pike Aviation will charge the estimate.<br>
        <div class="tab">
            a. In case of actual costs are higher, Pike Aviation shall charge the difference to the customer’s card on
            file.<br>
            b. In case of overpayment such amount will be credited to Customer’s card on file or any other outstanding
            balance owed by the customer.<br>
        </div>
        4. It is the client's responsibility to ensure that all passengers and crew have valid passports, visas,
        vaccinations certificates or any other immigration documentation required to enter a
        foreign country.<br>
        5 The information contained in this proposal comprises confidential information. If this proposal is not
        accepted by client, client agrees to either destroy this document; or continue to
        hold this information in strict confidence.<br>
        6. Each party shall be liable for damage to its own property and related third party claims in connection with
        liability or responsibility for loss or damage resulting from the violation of
        any authorities having jurisdiction with which the action may conflict.<br>
        7. Unpaid invoices that exceed the due date for reason not attributed to Pike Aviation are subject to a late payment interest.<br>
    </div>
</div>


