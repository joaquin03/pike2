<div>
    <h5><u>Wire transfer information</u></h5>
    <p> (currency: US DOLLARS) </p>
</div>

<div class="col-md-12 row">
    <div class="col-md-6" style="text-align: right" >
        <div class="col-md-12">Beneficiary Bank:</div>
        <div class="col-md-12">Beneficiary Bank Address:</div>
        <div class="col-md-12">Beneficiary Bank Swift Code:</div>
        <br>
        <br>
        <div class="col-md-12">Beneficiary Name:</div>
        <br>
        <div class="col-md-12">Address:</div>
        <br>
        <div class="col-md-12">Account Number:</div>
        <br>
        <br>
        <div class="col-md-12">Intermediary Bank Name:</div>
        <div class="col-md-12">ABA:</div>
        <div class="col-md-12">Swift Code:</div>
    </div>

    <div class="col-md-6 "  style="text-align: left" >
        <div class="col-md-12">BANCO SANTANDER S.A. </div>
        <div class="col-md-12">Cerrito 449, CP 11000 (Montevideo, Uruguay)</div>
        <div class="col-md-12">BSCHUYMM</div>
        <br>
        <br>
        <div class="col-md-12">PIKE AVIATION S.A</div>
        <br>
        <div class="col-md-12">Paraguay 2141, Aguada Park, Piso 10<br>CP11800 (Montevideo, Uruguay)</div>
        <div class="col-md-12">5100338698</div>
        <br>
        <br>
        <div class="col-md-12">WELLS FARGO</div>
        <div class="col-md-12">#026005092</div>
        <div class="col-md-12">PNBPUS3NNYC</div>
    </div>
</div>