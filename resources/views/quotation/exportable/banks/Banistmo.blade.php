<div>
    <h5><u>Wire transfer information</u></h5>
    <p> (currency: US DOLLARS) </p>
</div>

<div class="col-md-12 row">
    <div class="col-md-6" style="text-align: right" >
        <div class="col-md-12">Beneficiary Bank:</div>
        <div class="col-md-12">Beneficiary Bank Address:</div>
        <div class="col-md-12">Beneficiary Bank Swift Code:</div>
        <div class="col-md-12">Beneficiary Bank Account:</div>
        <br>
        <br>
        <div class="col-md-12">Beneficiary Name:</div>
        <br>
        <div class="col-md-12">Address:</div>
        <br>
        <div class="col-md-12">Account Number:</div>
        <br>
        <br>
        <div class="col-md-12">Intermediary Bank Name:</div>
        <div class="col-md-12">Intermediary Bank Address:</div>
        <div class="col-md-12">Swift Code:</div>
        <div class="col-md-12">ABA:</div>
    </div>

    <div class="col-md-6 "  style="text-align: left" >
        <div class="col-md-12">Banistmo S.A. </div>
        <div class="col-md-12">Torre Banistmo, Calle 50.</div>
        <div class="col-md-12">MIDLPAPA</div>
        <div class="col-md-12">36322415 </div>
        <br>
        <br>
        <div class="col-md-12">PIKE AVIATION S.A</div>
        <br>
        <div class="col-md-12">Paraguay 2141, Aguada Park, Piso 10<br>CP11800 (Montevideo, Uruguay)</div>
        <div class="col-md-12">01-1393662-0</div>
        <br>
        <br>
        <div class="col-md-12">Citibank New York</div>
        <div class="col-md-12">111 Wall Street, New York - NY 10043, USA</div>
        <div class="col-md-12">CITIUS33 </div>
        <div class="col-md-12">021000089 </div>
    </div>
</div>