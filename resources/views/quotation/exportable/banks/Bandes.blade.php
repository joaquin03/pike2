<div>
    <h5><u>Wire transfer information</u></h5>
    <p> (currency: EUROS) </p>
</div>

<div class="col-md-12 row">
    <div class="col-md-6" style="text-align: right" >
        <div class="col-md-12">Beneficiary Bank:</div>
        <div class="col-md-12">Account number:</div>
        <div class="col-md-12"> IBAN code:</div>
        <div class="col-md-12"> Bank Swift code:</div>
        <br>
        <br>
        <div class="col-md-12">Beneficiary Name:</div>
        <br>
        <div class="col-md-12">Address:</div>
        <br>
        <div class="col-md-12">Account Number:</div>
        <br>
        <br>
        <div class="col-md-12">Intermediary Bank Name:</div>
        <div class="col-md-12">Swift Code:</div>
    </div>

    <div class="col-md-6 "  style="text-align: left" >
        <div class="col-md-12">BANCO BANDES URUGUAY S.A. </div>
        <div class="col-md-12">903095068718</div>
        <div class="col-md-12">PT50000709030009506871897</div>
        <div class="col-md-12">CFACUYMM</div>
        <br>
        <br>
        <div class="col-md-12">PIKE AVIATION S.A</div>
        <br>
        <div class="col-md-12">Paraguay 2141, Aguada Park, Piso 10<br>CP11800 (Montevideo, Uruguay)</div>
        <div class="col-md-12">40016886-0</div>
        <br>
        <br>
        <div class="col-md-12">NOVO BANCO - SUCURSAL FINANCIERA EXTERIOR</div>
        <div class="col-md-12">BESCPTPL</div>
    </div>
</div>