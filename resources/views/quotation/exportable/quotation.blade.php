<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <link rel="stylesheet" type="text/css" href="/css/print.css" media="all"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>

@php
    $itineraryCount = 0;
    $extended = $quotation->getOrCreateQuotationExtended();
@endphp

<body>
{{------------------------------------------------------------------------------------}}
{{-- The page                                                                       --}}
{{------------------------------------------------------------------------------------}}
@include('quotation.exportable.general.page-layout')
</body>


{{----------------------------------------------------------------------------------}}
{{-- Save Quotation Document Button                                               --}}
{{----------------------------------------------------------------------------------}}

<button style="bottom: 15px;left: 15px; position:sticky" id="saveQuotationDocument" class="btn btn-info saveQuotationDocument noPrintPageButton">
    Save Quotation Document
</button>


<button style="bottom: 15px;left: 230px;  position:sticky" id="downloadQuotation" class="btn btn-success downloadQuotationDocument noPrintPageButton">
    Download Quotation PDF
</button>

</html>

<script>
    document.getElementById('saveQuotationDocument').onclick = function () {

        var documentHTML = document.documentElement.innerHTML.toString();


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "POST",
            url: {{$quotation->id}} +'/document-save',
            data: {htmlToSave: documentHTML},
            success: function () {
                alert('Quotation saved successfully to ' + '{{$quotation->getAircraftClientName() }} ');
            },
            error: function () {
                alert('Could not save quotation. Please contact your provider.');
            }
        });



    };

    document.getElementById('downloadQuotation').onclick = function ()
    {
        var maxPageItems = 21;
        for(var i = 1; i <= {{$quotation->arrivalsQuantity()}} ; i ++ ) {
            var services = $('.service-from-itinerary-'+i).length;
            var notes = $('.right-custom-notes-'+i).length;

            if (services + notes > maxPageItems) {
                $('.services-ending-'+i).append('<div class="pagebreak"></div>');
                if($('.additionals-table-'+i)[0]){
                    $('.additionals-table-'+i)[0].style.marginTop = '200px';
                }
            }
        }
         window.print();
    };

</script>




