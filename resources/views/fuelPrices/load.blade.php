@extends('backpack::layout')



@section('title')
    Upload Fuel WW list
@endsection

@section('header')
    <section class="content-header">
        <h1>Upload Fuel WW list</h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/admin/dashboard')}}">Admin</a></li>
            <li><a href="{{url('/admin/billing')}}" class="text-capitalize">Fuel Prices</a></li>
            <li class="active">Load</li>
        </ol>

    </section>
@endsection()

@section('content')
    @include('crud::inc.grouped_errors')

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body row">
                    <div class="col-md-12">
                        <h2>Upload information</h2>
                        <div class="form-group col-md-6" id="export-quotation-button">
                            <form action="{{route('fuel-prices.upload')}}" method="POST" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <div class="col-md-4">
                                    <select name="provider" id="" class="form-control">
                                        @foreach(\App\Models\FuelPrices::getProviders() as $provider)
                                            <option value="{{$provider}}">{{ $provider }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <input class="col-md-6" type="file" name="fuel_airports" required="required">
                                <div class="col-md-12" style="margin-top: 10px">
                                    <button type="submit" class="col-md-4 btn btn-warning">Import Excel</button>
                                </div>

                            </form>
                        </div>
                        <div class="col-md-6">
                            <a class="col-md-12" target="_blank" href="{{url('uploads/fuel/AEG.xlsx')}}">Example AEG</a>
                            <a class="col-md-12" target="_blank" href="{{url('uploads/fuel/Jetex.xlsx')}}">Example Jetex</a>
                            <a class="col-md-12" target="_blank" href="{{url('uploads/fuel/Epic_fuel.xlsx')}}">Example EpicFuel</a>
                        </div>

                    </div>

                    @include('fuelPrices.partials.showTable')
                </div>
            </div>
        </div>
    </div>

@endsection

@section('after_styles')
    <link href="{{ asset('vendor/adminlte/plugins/select2/select2.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css"
          rel="stylesheet" type="text/css"/>
@endsection

@section('after_scripts')
    <script src="{{ asset('vendor/adminlte/plugins/select2/select2.min.js') }}"></script>
    <script>
        $(document).ready(function (){
            $('.select2').select2();
        });
    </script>

@endsection