

<div class="col-md-12">

    <div class="col-md-12">
        <table class="table">
            <tr>
                <th>Provider</th>
                <th>Last Update</th>
            </tr>
            @foreach(\App\Models\FuelPrices::getProvidersItems() as $provider)
                <tr>
                    <td>{{$provider->provider}}</td>
                    <td>{{$provider->updated_at->format('d-m-Y')}}</td>
                </tr>
            @endforeach
        </table>
    </div>
    <hr class="col-md-12">

    <h2>Cotizar</h2>
    <form action="" method="get" enctype="multipart/form-data">
        <div class="col-md-3">
            <select name="icao" id="" class="select2 form-control">
                @foreach(\App\Models\FuelPrices::getIcaosItems()->get() as $provider)
                    <option value="{{$provider->icao}}" @if($icao == $provider->icao) selected @endif>
                        {{$provider->icao}}
                    </option>
                @endforeach
            </select>
        </div>

        <div class="col-md-3">
            <button type="submit" class="col-md-3 btn btn-success">Cotizar</button>
        </div>
    </form>
</div>
<div class="col-md-12">
    <table class="table">
        <tr>
            <th>ICAO</th>
            <th>Provider</th>
            <th>Fuel Provider</th>
            <th>Min Quantity</th>
            <th>Max Quantity</th>
            <th>Price</th>
            <th>Notes</th>
            <th>Date Vaid</th>
        </tr>
        @foreach($providerValues as $provider)
            <tr>
                <td>{{$provider->icao}}</td>
                <td>{{$provider->provider}}</td>
                <td>{{$provider->fuel_provider}}</td>
                <td>{{$provider->min_quantity}}</td>
                <td>{{$provider->max_quantity}}</td>
                <td>{{$provider->price}}</td>
                <td>{{$provider->notes}}</td>
                <td>{{$provider->date_valid}}</td>
            </tr>
        @endforeach
    </table>
</div>