<div class="col-md-12 box">
    <h3>Fuel Prices - Company:
        <a target="_blank" href="{{route('crud.company.show', ['id'=>$client->id])}}#edit">
            {{$client->name}}</a>
    </h3>


    <table class="table">
        <tr>
            <th>ICAO</th>
            <th>Provider</th>
            <th>Fuel Provider</th>
            <th>Min Quantity</th>
            <th>Max Quantity</th>
            <th>Price</th>
            <th>Notes</th>
            <th>Date Vaid</th>
        </tr>
        @foreach($providers as $provider)
            <tr>
                <td>{{$provider->icao}}</td>
                <td>{{$provider->provider}}</td>
                <td>{{$provider->fuel_provider}}</td>
                <td>{{$provider->min_quantity}}</td>
                <td>{{$provider->max_quantity}}</td>
                <td>{{$provider->price}}</td>
                <td>{{$provider->notes}}</td>
                <td>{{$provider->date_valid}}</td>
            </tr>
        @endforeach
    </table>
</div>