<div class="col-md-12 box">
    <h3>Fuel Prices - Company:
        <a target="_blank" href="{{route('crud.company.show', ['id'=>$client->id])}}#edit">
            {{$client->name}}</a> -
        Type: {{$client->US_fuel_client_type ?? 'Client A (default)'}}</h3>

    @if($fuelData == [])
        <h4 style="margin-bottom: 20px">At the moment this functionality is only for ICAOs in US</h4>
    @else

        <table class="table">
            <tr>
                <th>FBO</th>
                <th>Recommended for the client</th>
                <th>Min negotiable for pike</th>
                <th>Retail price:</th>
            </tr>

            @foreach($fuelData as $values)
                <tr>
                    <td>{{ $values['fbo'] }}</td>
                    <td><b>{{ $values[$client->US_fuel_client_type?? 'client_a'] }}</b></td>
                    <td>{{ $values['min_pike'] }}</td>
                    <td>{{ $values['retail'] }}</td>
                </tr>
            @endforeach
        </table>
    @endif
</div>