@extends('backpack::layout')



@section('title')
    List Fuel WW
@endsection

@section('header')
    <section class="content-header">
        <h1>List Fuel WW</h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/admin/dashboard')}}">Admin</a></li>
            <li><a href="{{url('/admin/billing')}}" class="text-capitalize">Fuel Prices</a></li>
            <li class="active">Load</li>
        </ol>

    </section>
@endsection()

@section('content')
    @include('crud::inc.grouped_errors')

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body row">

                    @include('fuelPrices.partials.showTable')
                </div>
            </div>
        </div>
    </div>

@endsection

@section('after_styles')
    <link href="{{ asset('vendor/adminlte/plugins/select2/select2.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css"
          rel="stylesheet" type="text/css"/>
@endsection

@section('after_scripts')
    <script src="{{ asset('vendor/adminlte/plugins/select2/select2.min.js') }}"></script>
    <script>
        $(document).ready(function (){
            $('.select2').select2();
        });
    </script>

@endsection