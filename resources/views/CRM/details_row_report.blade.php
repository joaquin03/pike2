<div class="m-t-10 m-b-10 p-l-10 p-r-10 p-t-10 p-b-10">
    <div class="row">
        <div class="col-md-12" style="font-size: 15px;">
            <div class="col-md-6">
                <h3>Old Values</h3>
                <span>
					@php
                        echo str_replace(' ,', '<br>',$entry->transformOldValues());
                    @endphp
				</span>


            </div>

            <div class="col-md-6">
                <h3>New Values</h3>
                <span>
					@php
                        echo str_replace(', ', '<br>',$entry->transformNewValues());
                    @endphp
				</span>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>