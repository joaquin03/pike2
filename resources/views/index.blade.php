<html lang="en"><!--<![endif]--><!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8">
    <title>Pike - Aviation | Coming Soon</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="Preview page of Metronic Admin Theme #1 for " name="description">
    <meta content="" name="author">
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=all" rel="stylesheet"
          type="text/css">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="tex/css">
    <link href="{{ asset('/assets/global/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet"
          type="text/css">
    <link href="{{ asset('/assets/global/plugins/simple-line-icons/simple-line-icons.min.css')}}" rel="stylesheet"
          type="text/css">
    <link href="{{ asset('/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet')}}" type="text/css">
    <link href="{{ asset('/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')}}" rel="stylesheet"
          type="text/css">
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="{{ asset('/assets/global/css/components.min.css') }}" rel="stylesheet" id="style_components"
          type="text/css">
    <link href="{{ asset('/assets/global/css/plugins.min.css') }}" rel="stylesheet" type="text/css">
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="{{ asset('/css/coming-soon.min.css') }}" rel="stylesheet" type="text/css">
    <!-- END PAGE LEVEL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="favicon.png">
</head>
<!-- END HEAD -->

<body style="background-image: url(/media/1.jpg);background-size: cover;">
<div class="container" style="margin-left: 20%">
    <div class="row">
        <div class="col-md-12 coming-soon-header">
            <a class="brand"
               href="index.html">
                <img src="{{ asset('/media/logo-blanco.png') }}" alt="logo" style="width: 270px;margin-left: -20px;">
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 coming-soon-content"
             style="font-size: 62px;line-height: 51px;margin-bottom: 25px;font-weight: 300;">
            <h1 style="font-size: 60px;margin-top: -20px;">Coming Soon!</h1>
            <p style="font-size: 20px;">
                We are a leading company in airport services providing ground support to all types of aircrafts at
                the major airports in Latin America
            </p>
            <br>
                <div class="form-group">
                    <div class="col-md-6">
                        <a class="btn btn-default"  href="{{ url("/admin/dashboard") }}">Login</a>
                    </div>
                </div>


        </div>

    </div>
    <!--/end row-->
    <div class="row">
        <div class="col-md-12 coming-soon-footer"> {{\Carbon\Carbon::now()->year}} Pike - Aviation</div>
    </div>
</div>
<!--[if lt IE 9]>
<script src="../assets/global/plugins/respond.min.js"></script>
<script src="../assets/global/plugins/excanvas.min.js"></script>
<script src="../assets/global/plugins/ie8.fix.min.js"></script>
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<script async="" src="//www.googletagmanager.com/gtm.js?id=GTM-W276BJ"></script>
<script async="" src="https://www.google-analytics.com/analytics.js"></script>
<script src="../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/js.cookie.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}"
        type="text/javascript"></script>


<script>
    $(document).ready(function () {
        $('#clickmewow').click(function () {
            $('#radio1003').attr('checked', 'checked');
        });
    })
</script>

</body>

</html>