<?php

define('LARAVEL_START', microtime(true));

if(!function_exists('_dd')) {
    function  _dd($args) {
        header('HTTP/1.0 500 Internal Server Error');
        die(var_dump($args));
    }
}
/*
|--------------------------------------------------------------------------
| Register The Composer Auto Loader
|--------------------------------------------------------------------------
|
| Composer provides a convenient, automatically generated class loader
| for our application. We just need to utilize it! We'll require it
| into the script here so we do not have to manually load any of
| our application's PHP classes. It just feels great to relax.
|
*/

require __DIR__.'/../vendor/autoload.php';
