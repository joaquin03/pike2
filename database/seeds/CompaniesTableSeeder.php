<?php

use App\Models\Activity;
use App\Models\Call;
use App\Models\Client;
use App\Models\Comment;
use App\Models\Company;
use App\Models\Contact;
use App\Models\Details;
use App\Models\Email;
use App\Models\File;
use App\Models\Meeting;
use App\Models\Note;
use App\Models\Reminder;
use Illuminate\Database\Seeder;

class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory(Company::class, 50)->create()->each(function (Company $company) {

            $details_qty = random_int(1, 3);
            for ($j = 0; $j <= $details_qty; $j++) {
				$company->details()->save(factory(Details::class)->make());
            }

            $contacts_qty = random_int(1, 4);
            for ($i = 0; $i <= $contacts_qty; $i++) {
                $contact = factory(Contact::class)->make();

				$company->contacts()->save($contact);

                $details_qty = random_int(1, 3);
                for ($j = 0; $j <= $details_qty; $j++) {
                    $contact->details()->save(factory(Details::class)->make());
                }
            }

            $activities_qty = random_int(8, 15);
            for ($i = 0; $i <= $activities_qty; $i++) {
                $activity = factory(Activity::class)->make();

                $type_of_activity = random_int(1, 5);
                switch ($type_of_activity) {
                    case 1:
                        $event_name = Email::class;
                        break;
                    case 2:
                        $event_name = Meeting::class;
                        break;
                    case 3:
                        $event_name = Call::class;
                        break;
                    case 4:
                        $event_name = Reminder::class;
                        break;
                    case 5:
                        $event_name = Note::class;
                        break;
                    case 6:
                        $event_name = File::class;
                        break;
                }

                $activity->event()->associate(factory($event_name)->create());

				$company->activities()->save($activity);

                $comments_qty = random_int(0, 5);
                for ($j = 0; $j <= $comments_qty; $j++) {
                    $activity->comments()->save(factory(Comment::class)->make());
                }
            }

        });
    }
}
