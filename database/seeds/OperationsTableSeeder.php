<?php

use App\Models\Airport;
use App\Models\FlightPlan;
use App\Models\Itinerary;
use App\Models\Operation;
use App\Models\Provider;
use App\Models\ProviderService;
use App\Models\Service;
use Illuminate\Database\Seeder;

class OperationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		factory(Operation::class, 30)->create()->each(function (Operation $operation){

			$this->createItinerary($operation);

			$operation->flightPlan()->save(factory(FlightPlan::class)->make());
		});
    }

	private function createItinerary(Operation $operation)
	{
		$itinerariesQty = random_int(1, 3);
		for ($j = 0; $j <= $itinerariesQty; $j++) {
			$itinerary = factory(Itinerary::class)->make();
			$operation->itineraries()->save($itinerary);

			$this->createItineraryServices($itinerary);
		}
	}

	private function createItineraryServices(Itinerary $itinerary)
	{
		$itineraryServicesQty = random_int(1, 3);

		for ($j = 0; $j <= $itineraryServicesQty; $j++) {
			$provider = new App\Models\Provider;

			$provider = $provider->inRandomOrder()->first();
			$providerService = $provider->providerServices()->inRandomOrder()->first();

			$itinerary->providerServices()->attach($providerService, ['operation_cost'=>$providerService->price ?? null]);
		}
	}


}
