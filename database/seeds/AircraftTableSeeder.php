<?php

use App\Models\Aircraft;
use App\Models\AircraftType;
use Illuminate\Database\Seeder;

class AircraftTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		factory(AircraftType::class)->times(10)->create();

		factory(Aircraft::class)->times(30)->create();
    }
}
