<?php

use App\Models\Provider;
use App\Models\ProviderService;
use App\Models\Service;
use Illuminate\Database\Seeder;

class ServicesTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		factory(Service::class, 8)->create()->each(function (Service $service) {

			for ($i = 0; $i <= random_int(1, 5); $i++) {
				$provider = Provider::inRandomOrder()->first();
				$data = ['price'=>random_int(1, 12)*1000];

				$service->addServiceToProvider($provider, $data);
			}

		});
	}
}