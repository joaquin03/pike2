<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'           => "Ivan L'olivier",
            'email'          => 'ivan.lolivier@gmail.com',
            'password'       => bcrypt('secret'),
            'remember_token' => str_random(10),
			'id'			 => 1,
        ]);

        DB::table('users')->insert([
            'name'           => "Joaquin Anduano",
            'email'          => 'janduano@gmail.com',
            'password'       => bcrypt('123456'),
            'remember_token' => str_random(10),
			'id'			 => 2,
        ]);

        DB::table('users')->insert([
            'name'           => "Rafa De Marco",
            'email'          => 'rafademarcoo@gmail.com',
            'password'       => bcrypt('123456'),
            'remember_token' => str_random(10),
            'id'			 => 15,
        ]);
    
        DB::table('users')->insert([
            'name'           => "Andres Haskel",
            'email'          => 'andres.haskel@gmail.com',
            'password'       => bcrypt('123456'),
            'remember_token' => str_random(10),
            'id'			 => 6,
        ]);
        

		factory(User::class, 3)->create();
    }
}
