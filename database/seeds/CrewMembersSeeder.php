<?php

use App\Models\CrewMember;
use Illuminate\Database\Seeder;

class CrewMembersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		factory(CrewMember::class, 30)->create();
    }
}
