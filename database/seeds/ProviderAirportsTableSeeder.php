<?php

use Illuminate\Database\Seeder;
use App\Models\ProviderAirport;


class ProviderAirportsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('provider_airports')->insert([
            'provider_id'           => 3,
            'airport_icao'          => 'AMmouth4oj',
        ]);

        DB::table('provider_airports')->insert([
            'provider_id'           => 3,
            'airport_icao'          => 'AOmouthmpb',
        ]);

        DB::table('provider_airports')->insert([
            'provider_id'           => 3,
            'airport_icao'          => 'AXmouthysT',
        ]);

        DB::table('provider_airports')->insert([
            'provider_id'           => 3,
            'airport_icao'          => 'AXmouthysTk',
        ]);

        DB::table('provider_airports')->insert([
            'provider_id'           => 16,
            'airport_icao'          => 'GEshire1L9',
        ]);
    }
}

