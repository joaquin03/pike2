<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterItineraryProvServiceAddBillingQuantity extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('itinerary_provider_services', function(Blueprint $table) {
             $table->double('billing_quantity')->nullable();
        });
        
        Schema::table('accounting_services', function(Blueprint $table) {
            $table->double('billing_quantity')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('itinerary_provider_services', function(Blueprint $table) {
            $table->dropColumn('billing_quantity');
        });
        Schema::table('accounting_services', function(Blueprint $table) {
            $table->dropColumn('billing_quantity');
        });
    }
}
