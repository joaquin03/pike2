<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterItineraryAndOperationBilling extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('itinerary_provider_services', function (Blueprint $table) {
            $table->decimal('procurement_cost')->nullable();
            $table->decimal('billing_percentage')->nullable();
        });
        Schema::table('itineraries', function (Blueprint $table) {
            $table->integer('billing_status')->default(0);
            $table->decimal('billing_discount')->nullable();
            $table->decimal('billing_pike_service_cost')->nullable();
            $table->text('billing_notes')->nullable();
        });

        Schema::table('operations', function (Blueprint $table) {
            $table->integer('billing_status')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('itinerary_provider_services', function (Blueprint $table) {
            $table->dropColumn('procurement_cost');
            $table->dropColumn('billing_percentage');
        });
        Schema::table('itineraries', function (Blueprint $table) {
            $table->dropColumn('billing_status');
            $table->dropColumn('billing_discount');
            $table->dropColumn('billing_notes');
            $table->dropColumn('billing_pike_service_cost');
        });
        Schema::table('operations', function (Blueprint $table) {
            $table->dropColumn('billing_status');
            
            
        });
    }
}
