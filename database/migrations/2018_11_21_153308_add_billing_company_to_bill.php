<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBillingCompanyToBill extends Migration
{
    public function up()
    {
        Schema::table('bills', function (Blueprint $table) {
            $table->unsignedInteger('billing_company_id')->nullable();
            $table->foreign('billing_company_id')->references('id')->on('companies');
        });
        \DB::connection(env('DB_CONNECTION'))->select(\DB::raw('
            UPDATE bills
            SET billing_company_id = company_id
            '));
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bills', function (Blueprint $table) {
            $table->dropForeign('bills_billing_company_id_foreign');
            $table->dropColumn('billing_company_id');
        });
    }
}
