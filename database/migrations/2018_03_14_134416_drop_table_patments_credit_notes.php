<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropTablePatmentsCreditNotes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('payments_credit_notes');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('payments_credit_notes', function (Blueprint $table) {
            $table->increments('id');
        
            $table->unsignedInteger('credit_note_id');
            $table->foreign('credit_note_id')->references('id')->on('credit_notes');
        
            $table->unsignedInteger('payment_id');
            $table->foreign('payment_id')->references('id')->on('payments');
        
            $table->timestamps();
        });
    }
}
