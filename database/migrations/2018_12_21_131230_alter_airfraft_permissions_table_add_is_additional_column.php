<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAirfraftPermissionsTableAddIsAdditionalColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::table('aircraft_permissions', function(Blueprint $table){
            $table->boolean('is_additional_quotation')->nullable();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('aircraft_permissions', function(Blueprint $table){
            $table->dropColumn('is_additional_quotation');
        });
    }
}
