<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AlterItineraryProviderServicesNewLogic extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Drops
        Schema::table('itinerary_provider_services', function (Blueprint $table) {
            $table->dropForeign('itinerary_provider_services_itin_prov_service_id_foreign');
            $table->dropColumn('itin_prov_service_id');
    
            $table->dropForeign('itinerary_provider_services_service_provider_id_foreign');
            $table->dropColumn('service_provider_id');
            
            $table->dropColumn('in_procurement');
            $table->dropColumn('file');
            $table->dropColumn('procurement_cost');
            $table->dropColumn('status');
            $table->dropColumn('done_at');
            $table->dropColumn('requested_at');
        });
        
        //Change Names
        Schema::table('itinerary_provider_services', function (Blueprint $table) {
            $table->renameColumn('tax', 'procurement_tax');
            $table->renameColumn('operation_cost', 'procurement_cost');
            $table->renameColumn('service_quantity', 'procurement_quantity');
            
            $table->renameColumn('billing_price', 'billing_final_price');
        });

        //Add Column
        Schema::table('itinerary_provider_services', function (Blueprint $table) {
            $table->unsignedInteger('service_id')->default(1);
            $table->unsignedInteger('provider_id')->default(1);
            $table->boolean('is_service_parent')->default(0);
        });
        
        Schema::table('itinerary_provider_services', function (Blueprint $table) {
            
            //$table->foreign('service_id')->references('id')->on('services'); //did't work at all...
            //$table->foreign('provider_id')->references('id')->on('companies');
        });
        //Change Order Column
        //Op
        DB::statement('ALTER TABLE itinerary_provider_services MODIFY COLUMN service_id INT(10) AFTER itinerary_id;');
        DB::statement('ALTER TABLE itinerary_provider_services MODIFY COLUMN provider_id INT(10) AFTER service_id;');
        DB::statement('ALTER TABLE itinerary_provider_services MODIFY COLUMN is_service_parent INT(10) AFTER provider_id;');
        DB::statement('ALTER TABLE itinerary_provider_services MODIFY COLUMN operation_status VARCHAR(10) AFTER is_service_parent;');
        //Proc
        DB::statement('ALTER TABLE itinerary_provider_services MODIFY COLUMN procurement_cost DOUBLE AFTER operation_status;');
        DB::statement('ALTER TABLE itinerary_provider_services MODIFY COLUMN procurement_quantity INT(10) AFTER procurement_cost;');
        DB::statement('ALTER TABLE itinerary_provider_services MODIFY COLUMN procurement_tax DOUBLE AFTER procurement_quantity;');
        DB::statement('ALTER TABLE itinerary_provider_services MODIFY COLUMN procurement_status DOUBLE AFTER procurement_tax;');
        //Bill
        DB::statement('ALTER TABLE itinerary_provider_services MODIFY COLUMN billing_quote_price DOUBLE AFTER procurement_status;');
        DB::statement('ALTER TABLE itinerary_provider_services MODIFY COLUMN billing_amount INT(10) AFTER billing_percentage;');
    
        DB::statement('ALTER TABLE itinerary_provider_services MODIFY COLUMN billing_percentage DOUBLE AFTER billing_final_price;');
        DB::statement('ALTER TABLE itinerary_provider_services MODIFY COLUMN billing_final_price DOUBLE AFTER administration_fee;');
        DB::statement('ALTER TABLE itinerary_provider_services MODIFY COLUMN administration_fee DOUBLE AFTER has_bills;');
        DB::statement('ALTER TABLE itinerary_provider_services MODIFY COLUMN has_bills DOUBLE AFTER administration_fee;');
    
    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        throw new Exception('I don\'t wanna let you down');
        
    }
}
