<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AlterAccountingServicesTableRefactoring extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accounting_services', function(Blueprint $table){
            $table->double('procurement_unit_final_cost')->nullable();
            $table->double('procurement_final_cost')->nullable();
        });

        DB::statement("ALTER TABLE `accounting_services` MODIFY COLUMN procurement_quantity DOUBLE AFTER notes");
        DB::statement("ALTER TABLE `accounting_services` MODIFY COLUMN procurement_cost DOUBLE AFTER procurement_quantity");
        DB::statement("ALTER TABLE `accounting_services` MODIFY COLUMN procurement_tax DOUBLE AFTER procurement_cost");
        DB::statement("ALTER TABLE `accounting_services` MODIFY COLUMN procurement_adm_fee DOUBLE AFTER procurement_tax");
        DB::statement("ALTER TABLE `accounting_services` MODIFY COLUMN procurement_unit_final_cost DOUBLE AFTER procurement_adm_fee");
        DB::statement("ALTER TABLE `accounting_services` MODIFY COLUMN procurement_final_cost DOUBLE AFTER procurement_unit_final_cost");

        DB::statement("ALTER TABLE `accounting_services` MODIFY COLUMN billing_quantity DOUBLE AFTER procurement_final_cost");
        DB::statement("ALTER TABLE `accounting_services` MODIFY COLUMN billing_percentage DOUBLE AFTER billing_quantity");
        DB::statement("ALTER TABLE `accounting_services` MODIFY COLUMN billing_unit_price DOUBLE AFTER billing_percentage");
        DB::statement("ALTER TABLE `accounting_services` MODIFY COLUMN billing_final_price DOUBLE AFTER billing_unit_price");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accounting_services', function(Blueprint $table){
            $table->dropColumn('procurement_unit_final_cost');
            $table->dropColumn('procurement_final_cost');
        });
    }


}
