<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFuelUSATable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fuel_USA', function (Blueprint $table) {
            $table->increments('id');
            $table->string('icao');
            
            $table->string('FBO');
            $table->string('provider');
            $table->double('provider_value')->nullable();
            
            $table->integer('bat_number')->index();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fuel_USA');
    }
}
