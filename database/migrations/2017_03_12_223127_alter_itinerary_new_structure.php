<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterItineraryNewStructure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::table('itineraries', function (Blueprint $table)
        {
            $table->unsignedInteger('crew_captain_id')->nullable();
            $table->foreign('crew_captain_id')->references('id')->on('crew_members');
        
            $table->unsignedInteger('crew_first_official_id')->nullable();
            $table->foreign('crew_first_official_id')->references('id')->on('crew_members');
        
            $table->unsignedInteger('crew_auxiliary_id')->nullable();
            $table->foreign('crew_auxiliary_id')->references('id')->on('crew_members');
    
    
            $table->dropForeign('itineraries_airport_icao_foreign');
            $table->dropColumn("airport_icao");
            
            $table->string('airport_icao_string')->nullable();
            
        });
    
        Schema::table('flight_plans', function (Blueprint $table)
        {
            $table->dropForeign('flight_plans_operation_id_foreign');
            $table->dropColumn("operation_id");
            
            $table->unsignedInteger('itinerary_id');
            $table->foreign('itinerary_id')->references('id')->on('itineraries');
        });
    
        
        Schema::table('operations', function (Blueprint $table)
        {
            //$table->dropForeign('operations_plane_id_foreign');
            //$table->dropColumn('plane_id');
    
            //$table->dropForeign('operations_plane_operator_id_foreign');
            //$table->dropColumn('plane_operator_id');
    
            //$table->dropForeign('operations_plane_client_id_foreign');
            //$table->dropColumn('plane_client_id');
        
            //$table->dropForeign('operations_plane_captain_id_foreign');
            //$table->dropColumn('plane_captain_id');
            
  
            $table->unsignedInteger('aircraft_id');
            $table->foreign('aircraft_id')->references('id')->on('aircraft');
    
            $table->unsignedInteger('aircraft_operator_id');
            $table->foreign('aircraft_operator_id')->references('id')->on('companies');
    
            $table->unsignedInteger('aircraft_client_id');
            $table->foreign('aircraft_client_id')->references('id')->on('companies');
            
        });
    
        //Schema::rename('plane_permissions', 'aircraft_permissions');
        
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    
        Schema::rename('aircraft_permissions', 'plane_permissions');
        
        Schema::table('itineraries', function (Blueprint $table)
        {
            $table->dropForeign('operations_captain_crew_id_foreign');
            $table->dropColumn('captain_crew_id');
        
            $table->dropForeign('operations_first_official_crew_id_foreign');
            $table->dropColumn('first_official_crew_id');
        
            $table->dropForeign('operations_auxiliary_crew_id_foreign');
            $table->dropColumn('auxiliary_crew_id');
        });
    }
}
