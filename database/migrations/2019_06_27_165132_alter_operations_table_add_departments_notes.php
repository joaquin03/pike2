<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOperationsTableAddDepartmentsNotes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('operations', function (Blueprint $table) {
            $table->text('quotation_notes')->nullable();
            $table->text('operation_notes')->nullable();
            $table->text('procurement_notes')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('operations', function (Blueprint $table) {
            $table->dropColumn('quotation_notes');
            $table->dropColumn('operation_notes');
            $table->dropColumn('procurement_notes');
        });
    }
}
