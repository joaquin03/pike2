<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAdministrationFeeAddItineraryId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('administration_fees', function (Blueprint $table) {
            $table->integer('itinerary_id')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('administration_fees', function (Blueprint $table) {
            $table->dropColumn('itinerary_id');
        });
    }
}
