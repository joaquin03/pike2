<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPermanentPermissionAddAdministrationFee extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('permanent_permissions', function (Blueprint $table) {
            $table->integer('administration_fee')->nullable();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permanent_permissions', function (Blueprint $table) {
            $table->dropColumn('administration_fee');
        });
    }
}
