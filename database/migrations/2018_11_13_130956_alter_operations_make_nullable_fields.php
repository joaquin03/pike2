<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOperationsMakeNullableFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::table('operations', function(Blueprint $table){
            
            $table->integer('aircraft_client_id')->nullable()->unsigned()->change();
            $table->integer('aircraft_id')->nullable()->unsigned()->change();
            $table->integer('aircraft_operator_id')->nullable()->unsigned()->change();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('operations', function(Blueprint $table){
            $table->integer('aircraft_id')->change();
            $table->integer('aircraft_operator_id')->change();
            $table->integer('aircraft_client_id')->change();
        });
    }
}
