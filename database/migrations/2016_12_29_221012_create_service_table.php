<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('services', function (Blueprint $table) {
			$table->increments('id');

			$table->string('name');//->unique(); for developer
			$table->text('description')->nullable();

			$table->timestamps();
			$table->softDeletes();
		});

		Schema::create('services_providers', function (Blueprint $table) {
			$table->increments('id');

			$table->unsignedInteger('service_id');
			$table->foreign('service_id')->references('id')->on('services');
			$table->unsignedInteger('provider_id');
			$table->foreign('provider_id')->references('id')->on('providers');

			$table->decimal('price')->nullable();
			$table->text('comments')->nullable();

			$table->timestamps();
			$table->softDeletes();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::dropIfExists('services_providers');
		Schema::dropIfExists('services');
    }
}
