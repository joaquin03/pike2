<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotesTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('note_note_tags', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('activity_note_id');
            $table->foreign('activity_note_id')->references('id')->on('notes');
        
            $table->unsignedInteger('company_tag_id');
            $table->foreign('company_tag_id')->references('id')->on('company_tags');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('note_note_tags');
    }
}
