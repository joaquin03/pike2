<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBillsPaymentsTableAddAssignedBillId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bills_payments', function (Blueprint $table) {
            $table->integer('assigned_bill_id')->nullable();
        });

        foreach (\App\Models\Accounting\CreditNote::all() as $debitNote){
            foreach ($debitNote->bills as $bill){
                $bill->pivot->assigned_bill_id = $bill->pivot->bill_id;
                $bill->pivot->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bills_payments', function (Blueprint $table) {
            $table->dropColumn('assigned_bill_id');
        });
    }
}
