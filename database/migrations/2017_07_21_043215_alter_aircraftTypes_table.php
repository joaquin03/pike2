<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAircraftTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('aircraft_types', function (Blueprint $table)
        {
            $table->enum('MTOW_Type', ['libras', 'kg'])->default('libras');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('aircraft_types', function (Blueprint $table)
        {
            $table->dropColumn('MTOW_Type');
        });
    }
}
