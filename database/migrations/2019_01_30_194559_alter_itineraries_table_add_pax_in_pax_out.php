<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterItinerariesTableAddPaxInPaxOut extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('itineraries', function ($table) {
            $table->integer('pax_in')->nullable();
            $table->integer('pax_out')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('itineraries', function ($table) {
            $table->dropColumn('pax_in');
            $table->dropColumn('pax_out');
        });
    }
}
