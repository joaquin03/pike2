<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterActiviyTableAddSoftdelete extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notes', function (Blueprint $table){
            $table->softDeletes();
        });
        Schema::table('meetings', function (Blueprint $table){
            $table->softDeletes();
        });
        Schema::table('emails', function (Blueprint $table){
            $table->softDeletes();
        });
        Schema::table('calls', function (Blueprint $table){
            $table->softDeletes();
        });
        Schema::table('reminders', function (Blueprint $table){
            $table->softDeletes();
        });
        Schema::table('files', function (Blueprint $table){
            $table->softDeletes();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notes', function (Blueprint $table){
            $table->dropSoftDeletes();
        });
        Schema::table('meetings', function (Blueprint $table){
            $table->dropSoftDeletes();
        });
        Schema::table('emails', function (Blueprint $table){
            $table->dropSoftDeletes();
        });
        Schema::table('calls', function (Blueprint $table){
            $table->dropSoftDeletes();
        });
        Schema::table('reminders', function (Blueprint $table){
            $table->dropSoftDeletes();
        });
        Schema::table('files', function (Blueprint $table){
            $table->dropSoftDeletes();
        });
    }
}
