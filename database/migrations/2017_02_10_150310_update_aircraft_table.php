<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAircraftTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::table('aircraft', function (Blueprint $table)
		{
			$table->string('equipment')->nullable();
			$table->string('pbn')->nullable();
			$table->string('nav')->nullable();
			$table->string('color')->nullable();
			$table->integer('dinghies_number')->nullable();
			$table->char('dinghies_capacity')->nullable();
			$table->char('dinghies_cover')->nullable();
			$table->string('dinghies_colour')->nullable();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('aircraft', function (Blueprint $table)
		{
			$table->dropColumn('equipment');
			$table->dropColumn('pbn');
			$table->dropColumn('nav');
			$table->dropColumn('color');
			$table->dropColumn('dinghies_number');
			$table->dropColumn('dinghies_capacity');
			$table->dropColumn('dinghies_cover');
			$table->dropColumn('dinghies_colour');
		});
    }
}
