<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubItineraryProviderServices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    
        Schema::table('services', function (Blueprint $table)
        {
            $table->unsignedInteger('service_id')->nullable();
            $table->foreign('service_id')->references('id')->on('services');
        });
        
        
        Schema::table('itinerary_provider_services', function (Blueprint $table)
        {
            $table->unsignedInteger('service_id')->nullable();
            //$table->foreign('service_id')->references('id')->on('services');
    
            $table->unsignedInteger('provider_id')->nullable();
            //$table->foreign('provider_id')->references('id')->on('providers');
            
            $table->unsignedInteger('itin_prov_service_id')->nullable();
            $table->foreign('itin_prov_service_id')->references('id')->on('itinerary_provider_services');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
