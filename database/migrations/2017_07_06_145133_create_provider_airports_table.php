<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProviderAirportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('provider_airports');
        Schema::create('provider_airports', function (Blueprint $table) {

            $table->unsignedInteger('provider_id')->index();
            $table->foreign('provider_id')->references('id')->on('companies');

            $table->string('airport_icao');
            $table->foreign('airport_icao')->references('icao')->on('airports');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('provider_airports');
    }
}
