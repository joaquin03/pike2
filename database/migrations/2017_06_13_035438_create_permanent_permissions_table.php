<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermanentPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permanent_permissions', function (Blueprint $table) {
            $table->increments('id');

            $table->enum('type', ['landing', 'overflight', 'landing diplomatic', 'overflight diplomatic'])->nullable();
            $table->string('code')->nullable();

//            $table->string('file')->nullable();

            $table->timestamp('start_at')->nullable();
            $table->timestamp('expire_at')->nullable();

            $table->char('country', 2);

            $table->unsignedInteger('provider_id');
            $table->foreign('provider_id')
                ->references('id')->on('companies')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permanent_permissions');
    }
}
