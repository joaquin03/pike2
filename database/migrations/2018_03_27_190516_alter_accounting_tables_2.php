<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
class AlterAccountingTables2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE bills MODIFY COLUMN amount DOUBLE NULL;');
        DB::statement(DB::raw('ALTER TABLE payments MODIFY COLUMN amount DOUBLE NULL;'));
        DB::statement(DB::raw('ALTER TABLE credit_notes MODIFY COLUMN amount DOUBLE NULL;'));
        
        Schema::table('accounting_services', function (Blueprint $table) {
            $table->text('description')->nullable()->change();
        });
        DB::statement(DB::raw('ALTER TABLE accounting_services MODIFY COLUMN billing_percentage DOUBLE NULL;'));
        DB::statement(DB::raw('ALTER TABLE accounting_services MODIFY COLUMN billing_price DOUBLE NULL;'));
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
