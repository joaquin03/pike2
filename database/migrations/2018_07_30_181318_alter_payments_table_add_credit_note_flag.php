<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPaymentsTableAddCreditNoteFlag extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->boolean('is_credit_note')->default(0);
            $table->double('dollar_change')->nullable();
            $table->string('bill_link')->nullable();
        });
        Schema::dropIfExists('credit_notes_bills');
        Schema::dropIfExists('credit_notes');
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->dropColumn('is_credit_note');
            $table->dropColumn('dollar_change');
            $table->dropColumn('bill_link');
    
        });
    }
}
