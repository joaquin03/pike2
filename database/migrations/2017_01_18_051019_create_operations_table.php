<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOperationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
	{
		Schema::create('operations', function (Blueprint $table) {
			$table->increments('id');

            $table->unsignedInteger('creator_user_id');
			$table->foreign('creator_user_id')->references('id')->on('users');

            //General
            $table->string('code')->nullable();
            $table->unsignedInteger('in_charge_user_id');
			$table->foreign('in_charge_user_id')->references('id')->on('users');

            $table->enum('state', ['Requested', 'In Process', 'Completed', 'Canceled']); //Todo: review this
            $table->text('notes')->nullable();

            //Plane
            

            $table->timestamps();
			$table->softDeletes();
		});

	}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::dropIfExists('operations');
    }
}
