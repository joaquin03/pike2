<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePermitsTablesAddCoste extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('aircraft_permissions', function (Blueprint $table) {
            $table->double('cost')->nullable();
        });
        Schema::table('permanent_permissions', function (Blueprint $table) {
            $table->double('cost')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('aircraft_permissions', function (Blueprint $table) {
            $table->dropColumn('cost');
        });
        Schema::table('permanent_permissions', function (Blueprint $table) {
            $table->dropColumn('cost');
        });
    }
}
