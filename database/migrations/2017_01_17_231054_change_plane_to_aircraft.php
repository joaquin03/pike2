<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangePlaneToAircraft extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		Schema::rename('plane_types', 'aircraft_types');

		Schema::rename('planes', 'aircraft');

		Schema::table('aircraft', function (Blueprint $table) {
			$table->dropForeign('planes_plane_type_id_foreign');
			$table->dropColumn("plane_type_id");
			$table->unsignedInteger('aircraft_type_id');
			$table->foreign('aircraft_type_id')->references('id')->on('aircraft_types')->onDelete('cascade');
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{

		Schema::rename('aircraft_types', 'plane_types');

		Schema::rename('aircraft', 'planes');

		Schema::table('planes', function (Blueprint $table) {
			$table->dropForeign('aircraft_aircraft_type_id_foreign');
			$table->dropColumn("aircraft_type_id");
			$table->unsignedInteger('plane_type_id');
			//$table->foreign('plane_type_id')->references('id')->on('plane_types')->onDelete('cascade');
		});
	}
}
