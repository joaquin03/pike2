<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUsersAddCountriesAndDropSalesManagersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {




        Schema::table('quotation_extended', function ($table) {
            $table->dropForeign('quotation_extended_sales_manager_id_foreign');
            $table->dropColumn('sales_manager_id');
        });

        Schema::table('companies', function ($table) {
            $table->dropForeign('companies_sales_manager_id_foreign');

        });

        Schema::dropIfExists('sales_managers');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('countries');
        });
    }
}
