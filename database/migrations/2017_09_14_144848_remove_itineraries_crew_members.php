<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveItinerariesCrewMembers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('itineraries', function (Blueprint $table) {
            $table->dropForeign('itineraries_crew_auxiliary_id_foreign');
            $table->dropForeign('itineraries_crew_captain_id_foreign');
            $table->dropForeign('itineraries_crew_first_official_id_foreign');

            $table->dropColumn('crew_captain_id');
            $table->dropColumn('crew_first_official_id');
            $table->dropColumn('crew_auxiliary_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
