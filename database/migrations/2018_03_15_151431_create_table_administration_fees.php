<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAdministrationFees extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('administration_fees', function (Blueprint $table) {
            $table->increments('id');
            
            //Reference
            $table->unsignedInteger('service_id');
            $table->foreign('service_id')->references('id')->on('services');
            
            $table->unsignedInteger('servicesable_id');
            $table->string('servicesable_type');
            
            
            //Duplicate Info
            $table->double('tax')->nullable();
            $table->double('billing_percentage')->nullable();
            $table->double('billing_amount')->nullable();
            
            $table->timestamps();
            $table->softDeletes();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('administration_fees');
        
    }
    
}
