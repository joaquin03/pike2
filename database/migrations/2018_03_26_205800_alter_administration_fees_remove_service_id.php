<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAdministrationFeesRemoveServiceId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('administration_fees', function (Blueprint $table) {
            $table->dropForeign('administration_fees_service_id_foreign');
            $table->dropUnique('administration_fees_service_id_foreign');
            $table->dropColumn('service_id');
    
    
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('administration_fees', function (Blueprint $table) {
            $table->primary('administration_fees_service_id_foreign');
            $table->index('administration_fees_service_id_foreign');
            $table->integer('service_id');
    
        });
    }
}
