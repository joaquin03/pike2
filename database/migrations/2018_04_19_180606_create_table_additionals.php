<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAdditionals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('additionals', function (Blueprint $table) {
            $table->increments('id');
            
            $table->unsignedInteger('servicesable_id');
            $table->string('servicesable_type');
    
            
            //Duplicate Info
            $table->double('tax')->nullable();
            $table->double('billing_amount')->nullable()->default(0);
            $table->integer('billing_price')->nullable();
    
            $table->integer('administration_fee')->nullable();
            $table->integer('has_bills')->default(0);
            $table->integer('operation_id')->default(0);
            $table->integer('provider_id')->nullable();
        
            $table->timestamps();
            $table->softDeletes();
        });
   
    }
    
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    
    public function down()
    {
        Schema::dropIfExists('additionals');
       
    }
    
}
