<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivitySpecialFeaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('special_features', function (Blueprint $table) {
            $table->increments('id');
    
            $table->unsignedInteger('user_id');
    
            $table->foreign('user_id')->references('id')->on('users');
    
            $table->string('name');
    
            $table->text('notes')->nullable();
    
            $table->morphs('company');
    
            $table->morphs('event');
    
            $table->timestamps();
            $table->softDeletes();
    
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('special_features');
    
    }
}
