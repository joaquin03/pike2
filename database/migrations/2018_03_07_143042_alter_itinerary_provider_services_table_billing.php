<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterItineraryProviderServicesTableBilling extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('itinerary_provider_services', function (Blueprint $table) {
            $table->double('billing_quote_price')->nullable();
            $table->double('billing_amount')->nullable();
            $table->double('billing_price')->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('itinerary_provider_services', function (Blueprint $table) {
            $table->dropColumn('billing_quote_price');
            $table->dropColumn('billing_amount');
            $table->dropColumn('billing_price');
        });
    }
}
