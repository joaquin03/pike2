a<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewStructure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('clients', 'companies');

        Schema::table('companies', function(Blueprint $table){
           $table->boolean('is_client')->default(false);
           $table->boolean('is_operator')->default(false);
           $table->boolean('is_provider')->default(false);

            $table->dropColumn("type");
        });

        Schema::table('activities', function (Blueprint $table) {
            $table->dropIndex('activities_company_id_company_type_index');
            $table->dropColumn("company_type");
//            $table->dropColumn("company_id");
        });

        Schema::table('activities', function (Blueprint $table) {
//            $table->unsignedInteger('company_id');
            $table->foreign('company_id')
                ->references('id')->on('companies')
                ->onDelete('cascade');
        });


        Schema::table('contacts', function (Blueprint $table) {
            $table->dropIndex('contacts_company_id_company_type_index');
            $table->dropColumn("company_type");
        });

        Schema::table('contacts', function (Blueprint $table) {
            $table->foreign('company_id')
                ->references('id')->on('companies')
                ->onDelete('cascade');
        });

        Schema::table('services_providers', function (Blueprint $table) {
            $table->dropForeign('services_providers_provider_id_foreign');
//            $table->dropColumn("provider_id");
        });

        Schema::table('services_providers', function (Blueprint $table) {
//            $table->unsignedInteger('company_id');
            $table->foreign('provider_id')
                ->references('id')->on('companies')
                ->onDelete('cascade');
        });

        Schema::drop('providers');

        DB::update('update companies set is_client = 1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('providers', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->char('country', 2)->nullable();
            $table->string('website')->nullable();
            $table->string('business')->nullable();
            $table->string('payment_methods')->nullable();
            $table->string('image')->nullable();

            $table->string('origin');

            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('services_providers', function (Blueprint $table) {
            $table->dropForeign('services_providers_company_id_foreign');
            $table->dropColumn('company_id');
        });

        Schema::table('services_providers', function (Blueprint $table) {
            $table->unsignedInteger('provider_id');
            $table->foreign('provider_id')->references('id')->on('providers');
        });

        Schema::table('contacts', function (Blueprint $table) {
            $table->dropForeign('contacts_company_id_foreign');
            $table->dropColumn('company_id');
        });

        Schema::table('contacts', function (Blueprint $table) {
            $table->morphs('company');
        });

        Schema::table('activities', function (Blueprint $table) {
//            $table->morphs('company');
            $table->string("company_type");
            $table->index(["company_id", "company_type"]);

        });

        Schema::table('activities', function (Blueprint $table) {
            $table->dropForeign('activities_company_id_foreign');
//            $table->dropColumn('company_id');
        });

        Schema::table('companies', function(Blueprint $table){
            $table->dropColumn('is_provider');
            $table->dropColumn('is_operator');
            $table->dropColumn('is_client');
        });

        Schema::rename('companies', 'clients');
    }
}
