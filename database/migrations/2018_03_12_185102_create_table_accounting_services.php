<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAccountingServices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounting_services', function (Blueprint $table)
        {
            $table->increments('id');
            
            //Reference
            $table->unsignedInteger('bill_id');
            $table->foreign('bill_id')->references('id')->on('bills');
            
            $table->unsignedInteger('servicesable_id');
            $table->string('servicesable_type');
            
            
            $table->text('description');
            //Duplicate Info
            $table->text('notes')->nullable();
            $table->double('operation_cost');
            $table->double('service_quantity')->nullable();
            $table->double('tax')->nullable();
            $table->double('billing_percentage');
            $table->double('billing_quote_price')->nullable();
            $table->double('billing_amount')->nullable();
            $table->double('billing_price');
            
           
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounting_services');
    }
}
