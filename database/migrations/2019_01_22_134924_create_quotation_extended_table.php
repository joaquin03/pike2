<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotationExtendedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('sales_managers', function (Blueprint $table) {
            $table->increments('id');


            $table->string('name');
            $table->text('comments')->nullable();

            $table->json('countries')->nullable();
            $table->timestamps();
        });

        Schema::create('quotation_extended', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('quotation_id');
            $table->foreign('quotation_id')->references('id')->on('operations');

            $table->unsignedInteger('contact_id')->nullable();
            $table->foreign('contact_id')->references('id')->on('contacts');

            $table->unsignedInteger('sales_manager_id')->nullable();
            $table->foreign('sales_manager_id')->references('id')->on('sales_managers');

            $table->string('payment')->nullable();
            $table->string('payment_method')->nullable();

            $table->double('pax_in_out')->nullable();

            $table->timestamps();
        });

        $q = App\Models\Quotation::all();
        foreach($q as $item) {$item->getOrCreateQuotationExtended(); }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotation_extended');
        Schema::dropIfExists('sales_managers');

    }
}
