<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProviderPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {



        Schema::create('provider_prices', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('provider_id');
            $table->timestamp('service_date')->nullable();
            $table->unsignedInteger('service_id');
            $table->integer('price')->nullable();
            $table->text('observation')->nullable();

            $table->timestamps();
            $table->time('deleted_at')->nullable();

            $table->foreign('provider_id')->references('id')->on('companies')->onDelete('cascade');
            $table->foreign('service_id')->references('id')->on('services')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('provider_prices');
    }
}
