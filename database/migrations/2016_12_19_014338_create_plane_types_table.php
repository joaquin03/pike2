<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlaneTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plane_types', function (Blueprint $table) {
            $table->increments('id');

            $table->string('manufacturer');
            $table->string('model');
            $table->string('code');

            $table->integer('crew_seats')->nullable();
            $table->integer('min_seats')->nullable();
            $table->integer('max_seats')->nullable();

            $table->integer('MTOW')->nullable();

            $table->enum('needs_stairs', ['YES', 'NO', 'ON_REQUEST'])->nullable();
            $table->enum('needs_toilette', ['YES', 'NO', 'ON_REQUEST'])->nullable();
            $table->enum('needs_bus', ['YES', 'NO', 'ON_REQUEST'])->nullable();
            $table->enum('needs_conveyor_belt', ['YES', 'NO', 'ON_REQUEST'])->nullable();
            $table->enum('needs_chocks', ['YES', 'NO', 'ON_REQUEST'])->nullable();
            $table->enum('needs_asu', ['YES', 'NO', 'ON_REQUEST'])->nullable();
            $table->enum('needs_gpu', ['YES', 'NO', 'ON_REQUEST'])->nullable();

            $table->text('characteristic')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plane_types');
    }
}
