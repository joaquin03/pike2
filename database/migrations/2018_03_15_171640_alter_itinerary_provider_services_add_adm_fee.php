<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterItineraryProviderServicesAddAdmFee extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('itinerary_provider_services', function (Blueprint $table) {
            $table->boolean('administration_fee')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('itinerary_provider_services', function (Blueprint $table) {
            $table->dropColumn('administration_fee');
        });
    }
}
