<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWakeTurbulenceCatToAircraftTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('aircraft_types', function (Blueprint $table) {
            $table->enum('wake_turbulence_cat', ['L', 'M', 'H'])->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('aircraft_types', function (Blueprint $table) {
            $table->dropColumn('wake_turbulence_cat');
        });
    }
}
