<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToCrewMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crew_members', function (Blueprint $table) {
//            $table->string('passport')->nullable()->default(null);
//            $table->dateTime('birthdate')->nullable()->default(null);
//            $table->string('licence_number')->nullable()->default(null);
//            $table->date('licence_number_expire_at')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crew_members', function (Blueprint $table) {
//            $table->dropColumn('passport');
//            $table->dropColumn('birthdate');
//            $table->dropColumn('licence_number');
//            $table->dropColumn('licence_number_expire_at');
        });
    }
}
