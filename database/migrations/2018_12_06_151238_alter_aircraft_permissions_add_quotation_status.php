<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAircraftPermissionsAddQuotationStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::table('aircraft_permissions', function(Blueprint $table){
            $table->text('quotation_status')->nullable();
            $table->double('country_distance')->nullable();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('aircraft_permissions', function(Blueprint $table){
            $table->dropColumn('quotation_status');
            $table->dropColumn('country_distance');
        });
    }
}
