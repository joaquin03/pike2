<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCompaniesAddSalesManagerAlterQuotationExtendedDeleteSm extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function ($table) {
            $table->unsignedInteger('sales_manager_id')->nullable();
            $table->foreign('sales_manager_id')->references('id')->on('sales_managers');
        });
//        Schema::table('quotation_extended', function ($table) {
//            $table->dropForeign('quotation_extended_sales_manager_id_foreign');
//            $table->dropColumn('sales_manager_id');
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('companies', function ($table) {
//            $table->dropForeign('companies_sales_manager_id_foreign');
//            $table->dropColumn('sales_manager_id');
        });
//        Schema::table('quotation_extended', function ($table) {
//            $table->unsignedInteger('sales_manager_id')->nullable();
//            $table->foreign('sales_manager_id')->references('id')->on('sales_managers');
//        });
    }
}
