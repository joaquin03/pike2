<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPermissionsTableMoveToOperations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('aircraft_permissions', function (Blueprint $table) {
            $table->dropForeign('aircraft_permissions_itinerary_id_foreign');
            $table->dropColumn('itinerary_id');
            $table->unsignedInteger('operation_id')->references('id')->on('operations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('aircraft_permissions', function (Blueprint $table) {
            $table->unsignedInteger('itinerary_id')->references('id')->on('itineraries');
            $table->dropForeign('aircraft_permissions_operation_id_foreign');
            $table->dropColumn('operation_id');
        });
    }
}
