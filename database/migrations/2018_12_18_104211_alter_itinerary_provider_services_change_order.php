<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterItineraryProviderServicesChangeOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE `itinerary_provider_services` MODIFY COLUMN service_origin varchar(191) AFTER provider_id");
        DB::statement("ALTER TABLE `itinerary_provider_services` MODIFY COLUMN parent_id int(10) unsigned DEFAULT NULL AFTER service_origin");
        DB::statement("ALTER TABLE `itinerary_provider_services` MODIFY COLUMN parent_service_id  int(10) unsigned DEFAULT NULL AFTER parent_id");
        DB::statement("ALTER TABLE `itinerary_provider_services` MODIFY COLUMN is_additional tinyint(1) DEFAULT NULL AFTER parent_service_id");

        DB::statement("ALTER TABLE `itinerary_provider_services` MODIFY COLUMN administration_fee double DEFAULT NULL AFTER notes");

        DB::statement("ALTER TABLE `itinerary_provider_services` MODIFY COLUMN procurement_adm_fee DOUBLE AFTER procurement_tax");
        DB::statement("ALTER TABLE `itinerary_provider_services` MODIFY COLUMN procurement_final_price DOUBLE AFTER procurement_adm_fee");

        DB::statement("ALTER TABLE `itinerary_provider_services` MODIFY COLUMN billing_quantity DOUBLE AFTER notes");
        DB::statement("ALTER TABLE `itinerary_provider_services` MODIFY COLUMN billing_percentage DOUBLE AFTER billing_quantity");
        DB::statement("ALTER TABLE `itinerary_provider_services` MODIFY COLUMN billing_unit_price DOUBLE AFTER billing_percentage");
        DB::statement("ALTER TABLE `itinerary_provider_services` MODIFY COLUMN billing_final_price DOUBLE AFTER billing_unit_price");
        DB::statement("ALTER TABLE `itinerary_provider_services` MODIFY COLUMN has_bills DOUBLE AFTER billing_final_price");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
