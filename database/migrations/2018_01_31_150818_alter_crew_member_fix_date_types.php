<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCrewMemberFixDateTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crew_members', function (Blueprint $table) {
            $table->dateTime('expire_date')->change();
            $table->dateTime('birthdate')->change();
            $table->dateTime('licence_number_expire_at')->change();
            $table->dateTime('medical_certificate_expire_date')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
