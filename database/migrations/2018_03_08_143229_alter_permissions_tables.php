<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPermissionsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement('ALTER TABLE aircraft_permissions MODIFY tax  DOUBLE;');
        
        Schema::table('aircraft_permissions', function (Blueprint $table) {
            
            $table->double('billing_percentage')->nullable();
            $table->double('billing_quote_price')->nullable();
            $table->double('billing_amount')->nullable();
            $table->double('billing_price')->nullable();
            
        });
        
        Schema::table('permanent_permissions', function (Blueprint $table)
        {
            $table->double('tax')->nullable();
            $table->double('billing_percentage')->nullable();
            $table->double('billing_quote_price')->nullable();
            $table->double('billing_amount')->nullable();
            $table->double('billing_price')->nullable();
    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('aircraft_permissions', function (Blueprint $table)
        {
            $table->dropColumn('billing_percentage');
            $table->dropColumn('billing_quote_price');
            $table->dropColumn('billing_amount');
            $table->dropColumn('billing_price');
        });
    
        Schema::table('permanent_permissions', function (Blueprint $table)
        {
            $table->dropColumn('tax');
            $table->dropColumn('billing_percentage');
            $table->dropColumn('billing_quote_price');
            $table->dropColumn('billing_amount');
            $table->dropColumn('billing_price');
        });
    }
}
