<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCompaniesAddNewFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function(Blueprint $table){
            $table->text('zip_code')->nullable();
            $table->text('city')->nullable();

            $table->text('procurement_bank_info')->nullable();
            $table->double('billing_adm_fee')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('companies', function(Blueprint $table){
            $table->dropColumn('zip_code');
            $table->dropColumn('city');

            $table->dropColumn('procurement_bank_info');
            $table->dropColumn('billing_adm_fee');
        });

    }
}
