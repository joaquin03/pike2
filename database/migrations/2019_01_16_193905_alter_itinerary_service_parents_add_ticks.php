<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterItineraryServiceParentsAddTicks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('itinerary_service_parents', function (Blueprint $table) {
            $table->boolean('billing_checked')->default(0);
            $table->boolean('adm_fee_checked')->default(0);
            $table->boolean('has_bills')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('itinerary_service_parents', function (Blueprint $table) {
            $table->dropColumn('billing_checked');
            $table->dropColumn('adm_fee_checked');
            $table->dropColumn('has_bills');
        });
    }
}
