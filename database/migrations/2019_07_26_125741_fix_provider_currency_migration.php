<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixProviderCurrencyMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('currency', function (Blueprint $table) {
            $table->increments('id');
            $table->text('currency');
            $table->timestamps();
        });

        Schema::table('provider_prices', function (Blueprint $table) {

            $table->unsignedInteger('currency_id');
            $table->foreign('currency_id')->references('id')->on('currency')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('currency');
        Schema::table('provider_prices', function (Blueprint $table) {

            $table->dropForeign('currency_id')->references('id')->on('currency')->onDelete('cascade');
            $table->dropColumn('currency_id');
        });
    }
}
