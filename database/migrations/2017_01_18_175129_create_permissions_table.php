<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('aircraft_permissions', function (Blueprint $table)
		{
			$table->increments('id');

			$table->unsignedInteger('itinerary_id');
			$table->foreign('itinerary_id')->references('id')->on('itineraries');

			$table->string('code')->nullable();
			$table->string('name');

			$table->timestamps();
			$table->softDeletes();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::dropIfExists('aircraft_permissions');
    }
}
