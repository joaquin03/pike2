<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFuelPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fuel_prices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('icao', 20)->index();
            $table->string('provider', 20)->index();
            
            $table->double('min_quantity')->default(0);
            $table->double('max_quantity')->default(99999);
            $table->double('price')->nullable();
            
            $table->string('date_effective')->nullable();
            $table->string('date_valid')->nullable();
            
            $table->text('notes')->nullable();
        
            $table->integer('bat_number')->index();
        
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fuel_prices');
    }
}
