<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAccountingTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::table('bills', function (Blueprint $table) {
            $table->dropColumn('type');
        });
        Schema::table('bills', function (Blueprint $table) {
            $table->enum('type', ['credit', 'debit'])->nullable();
            
            $table->timestamp('start_date')->nullable();
            
            $table->unsignedInteger('aircraft_id')->nullable();
            $table->foreign('aircraft_id')->references('id')->on('aircraft');
            
            $table->string('airport_icao_string')->nullable();
            $table->string('reference_number')->nullable();
        });
    
        Schema::table('payments', function (Blueprint $table) {
            $table->dropColumn('type');
        });
        
        Schema::table('payments', function (Blueprint $table) {
           $table->enum('type', ['credit', 'debit'])->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('credit_notes', function (Blueprint $table) {
            $table->dropColumn('type');
        });
    
        Schema::table('bills', function (Blueprint $table) {
            $table->dropColumn('start_date');
            $table->dropColumn('aircraft_id');
            $table->dropColumn('aircraft_id');
            $table->dropColumn('airport_icao_string');
            $table->dropColumn('reference_number');
        });
        
        
    }
}
