<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPermissionTableAddProcFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('aircraft_permissions', function (Blueprint $table) {
            $table->integer('tax')->nullable();
            $table->string('operation_status')->nullable();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('aircraft_permissions', function (Blueprint $table) {
            $table->dropColumn('tax')->nullable();
            $table->dropColumn('operation_status')->nullable();
        });
    }
}
