<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableCompaniesReceptorFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->integer('doc_type_receptor')->nullable();
            $table->string('country_code_receptor')->nullable();
            $table->string('document_receptor')->nullable();
            $table->string('business_name_receptor')->nullable();
            $table->string('address_receptor')->nullable();
            $table->string('city_receptor')->nullable();
            $table->string('state_receptor')->nullable();
            $table->string('postal_code_receptor')->nullable();
            $table->string('billing_days_receptor')->nullable();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->dropColumn('doc_type_receptor');
            $table->dropColumn('country_code_receptor');
            $table->dropColumn('document_receptor');
            $table->dropColumn('business_name_receptor');
            $table->dropColumn('address_receptor');
            $table->dropColumn('city_receptor');
            $table->dropColumn('state_receptor');
            $table->dropColumn('postal_code_receptor');

        });
    }
}
