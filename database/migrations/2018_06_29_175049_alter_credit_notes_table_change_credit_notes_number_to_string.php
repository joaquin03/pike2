<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCreditNotesTableChangeCreditNotesNumberToString extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('credit_notes', function(Blueprint $table)
        {
            $table->dropColumn('credit_notes_number');
        });
        
        Schema::table('credit_notes', function(Blueprint $table)
        {
            $table->text('credit_notes_number');
        });
        
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
