<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOperationsNullableCrewMembers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('operations', function (Blueprint $table) {
            $table->dropColumn('crew_auxiliary_id');
            $table->dropColumn('crew_captain_id');
            $table->dropColumn('crew_first_official_id');
            $table->dropColumn('second_crew_auxiliary_id');
            $table->dropColumn('second_crew_captain_id');
            $table->dropColumn('second_crew_first_official_id');

        });

        Schema::table('operations', function (Blueprint $table) {
            $table->integer('crew_auxiliary_id')->nullable()->default(null);
            $table->integer('crew_captain_id')->nullable()->default(null);
            $table->integer('crew_first_official_id')->nullable()->default(null);
            $table->integer('second_crew_auxiliary_id')->nullable()->default(null);
            $table->integer('second_crew_captain_id')->nullable()->default(null);
            $table->integer('second_crew_first_official_id')->nullable()->default(null);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
