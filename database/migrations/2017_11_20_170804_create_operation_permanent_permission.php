<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOperationPermanentPermission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('operation_permanent_permission', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('operation_id');
            $table->foreign('operation_id')
                ->references('id')->on('operations')
                ->onDelete('cascade');

            $table->unsignedInteger('aircraft_pm_id');
            $table->foreign('aircraft_pm_id')
                ->references('id')->on('aircraft_permanent_permission')
                ->onDelete('cascade');

            $table->unsignedInteger('permanent_permission_id');
            $table->foreign('permanent_permission_id')
                ->references('id')->on('permanent_permissions')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
