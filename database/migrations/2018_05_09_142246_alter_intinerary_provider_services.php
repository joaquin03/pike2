<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterIntineraryProviderServices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('itinerary_provider_services', function (Blueprint $table) {
            $table->unsignedInteger('parent_service_id')->nullable();
            //$table->foreign('parent_service_id')->references('id')->on('service_id');
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('itinerary_provider_services', function (Blueprint $table) {
           // $table->dropForeign('itinerary_provider_services_parent_service_id');
            $table->dropColumn('parent_service_id');
        });
    }
}
