<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPermanentPermitChangeTypeType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('permanent_permissions', function (Blueprint $table) {
            DB::statement('ALTER TABLE permanent_permissions MODIFY type TEXT NULL;');
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permanent_permissions', function (Blueprint $table) {
            $table->enum('type', ['landing', 'overflight', 'landing diplomatic', 'overflight diplomatic'])->nullable();
            });
    }
}
