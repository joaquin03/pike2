<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPermanentPermissionsTableAddTableItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('permanent_permissions', function (Blueprint $table) {
            $table->string('file')->nullable();
            $table->string('file_name')->nullable();
            $table->text('file_notes')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permanent_permissions', function (Blueprint $table)
        {
            $table->dropColumn('file');
            $table->dropColumn('file_name');
            $table->dropColumn('file_notes');
        });
    }
}
