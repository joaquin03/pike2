<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAicraftPermissionsRenameAmountToUnitPrice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('aircraft_permissions', function(Blueprint $table) {
            $table->renameColumn('billing_amount', 'billing_unit_price');
        });
       
    }
    
    
    public function down()
    {
        Schema::table('aircraft_permissions', function(Blueprint $table) {
            $table->renameColumn('billing_unit_price', 'billing_amount');
        });
      
    }
}
