<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAircraftPermanentPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aircraft_permanent_permission', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('aircraft_id');
            $table->foreign('aircraft_id')
                ->references('id')->on('aircraft')
                ->onDelete('cascade');

            $table->unsignedInteger('permanent_permission_id');
            $table->foreign('permanent_permission_id')
                ->references('id')->on('permanent_permissions')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aircraft_permanent_permission');
    }
}
