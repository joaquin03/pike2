<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAccountingServicesTablesAddHasBills extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('itinerary_provider_services', function (Blueprint $table) {
            $table->integer('has_bills')->default(0);
        });
        Schema::table('aircraft_permissions', function (Blueprint $table) {
            $table->integer('has_bills')->default(0);
        });
        Schema::table('permanent_permissions', function (Blueprint $table) {
            $table->integer('has_bills')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('itinerary_provider_services', function (Blueprint $table) {
            $table->dropColumn('has_bills');
        });
    
        Schema::table('aircraft_permissions', function (Blueprint $table) {
            $table->dropColumn('has_bills');
        });
        Schema::table('permanent_permissions', function (Blueprint $table) {
            $table->dropColumn('has_bills');
        });
    }
}
