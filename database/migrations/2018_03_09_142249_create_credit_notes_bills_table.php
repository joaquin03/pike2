<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreditNotesBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credit_notes_bills', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('bill_id');
            $table->unsignedInteger('credit_note_id');

            $table->timestamps();

            $table->foreign('bill_id')->references('id')->on('bills');
            $table->foreign('credit_note_id')->references('id')->on('credit_notes');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('credit_notes_bills');
    }
}
