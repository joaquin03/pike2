<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterItineraryProviderServiceRenameAmountToUnitPrice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('itinerary_provider_services', function(Blueprint $table) {
            $table->renameColumn('billing_amount', 'billing_unit_price');
        });
    
        Schema::table('accounting_services', function(Blueprint $table) {
            $table->renameColumn('billing_amount', 'billing_unit_price');
        });
    
        Schema::table('additionals', function(Blueprint $table) {
            $table->renameColumn('billing_amount', 'billing_unit_price');
        });
    }
    
    
    public function down()
    {
        Schema::table('itinerary_provider_services', function(Blueprint $table) {
            $table->renameColumn('billing_unit_price', 'billing_amount');
        });
        Schema::table('accounting_services', function(Blueprint $table) {
            $table->renameColumn('billing_unit_price', 'billing_amount');
        });
        Schema::table('additionals', function(Blueprint $table) {
            $table->renameColumn('billing_unit_price', 'billing_amount');
        });
    }
}
