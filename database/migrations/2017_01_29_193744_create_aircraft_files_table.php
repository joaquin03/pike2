<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAircraftFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aircraft_files', function (Blueprint $table)
        {
            $table->increments('id');

            $table->unsignedInteger('aircraft_id');
            $table->foreign('aircraft_id')->references('id')->on('aircraft');

            $table->string('name');
            $table->string('file');
            $table->text('notes')->nullable();

            $table->timestamps();
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aircraft_files');
    }
}
