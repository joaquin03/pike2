<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItinerariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('itineraries', function (Blueprint $table)
		{
			$table->increments('id');
			$table->string('code')->nullable();
			$table->unsignedInteger('operation_id');
            $table->foreign('operation_id')->references('id')->on('operations');

            $table->string('airport_icao');
            $table->foreign('airport_icao')->references('icao')->on('airports');

			$table->enum('type', ['Arrive', 'Departure']);
			$table->timestamp('estimated')->nullable();
			$table->timestamp('actual')->nullable();

			$table->unsignedInteger('operator_id');
			$table->foreign('operator_id')->references('id')->on('companies');

            $table->timestamp('flight_plan_sent_at')->nullable();

            $table->unsignedInteger('fuel_provider_id')->nullable();
            $table->foreign('fuel_provider_id')->references('id')->on('companies');
            $table->integer('fuel_gallons')->nullable();
            $table->decimal('fuel_price')->nullable();

            $table->timestamps();
			$table->softDeletes();
		});

		Schema::create('itinerary_provider_services', function (Blueprint $table)
		{
			$table->increments('id');

			$table->unsignedInteger('itinerary_id');
			$table->foreign('itinerary_id')->references('id')->on('itineraries');

			$table->unsignedInteger('service_provider_id');
			$table->foreign('service_provider_id')->references('id')->on('services_providers');

            $table->timestamp('requested_at')->nullable();
            $table->timestamp('done_at')->nullable();

			$table->decimal('service_price')->nullable();

            $table->text('notes')->nullable();

            $table->timestamps();
			$table->softDeletes();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('itinerary_provider_services');
        Schema::dropIfExists('itineraries');
    }
}
