<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOperationsTableAddNewQuotationsFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('operations', function(Blueprint $table){
            $table->text('quotation_via')->nullable();
            $table->integer('quotation_contact_id');
            $table->integer('quotation_sales_manager_id');
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('operations', function(Blueprint $table){
            $table->dropColumn('quotation_via');
            $table->dropColumn('quotation_contact_id');
            $table->dropColumn('quotation_sales_manager_id');
        });
    }
}
