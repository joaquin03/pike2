<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAirportsTableAddFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('airports', function (Blueprint $table) {
            $table->string('iata')->nullable();
            $table->string('OPR_HS_H24')->nullable();
            $table->string('OPR_HS_TO')->nullable();
            $table->string('OPR_HS_FROM')->nullable();
            $table->string('H24_OR')->nullable();
            $table->string('RWY1')->nullable();
            $table->string('lengthwidth1ft')->nullable();
            $table->string('lengthwidth1m')->nullable();
            $table->string('PCN1')->nullable();
            $table->string('RWY2')->nullable();
            $table->string('lengthwidth2ft')->nullable();
            $table->string('lengthwidth2m')->nullable();
            $table->string('PCN2')->nullable();
    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
