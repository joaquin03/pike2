<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCrewMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('crew_members', function (Blueprint $table)
		{
			$table->increments('id');

			$table->string('name');
			$table->char('country', 2)->nullable();

			$table->boolean('is_captain')->default(0);
			$table->boolean('is_first_official')->default(0);
			$table->boolean('is_auxiliary')->default(0);

			$table->timestamps();
			$table->softDeletes();

		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::dropIfExists('crew_members');
    }
}
