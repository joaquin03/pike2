<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterServiceProviderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('itinerary_provider_services', function (Blueprint $table) {
            $table->dropColumn('service_id');
            $table->dropColumn('provider_id');
            $table->dropColumn('status');
            
            $table->string('operation_status')->nullable();
            $table->string('procurement_status')->nullable();
            $table->double('tax')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('itinerary_provider_services', function (Blueprint $table) {
            $table->integer('service_id')->nullable();
            $table->integer('provider_id')->nullable();
            $table->string('status')->nullable();
            
            $table->dropColumn('operation_status');
            $table->dropColumn('procurement_status');
            $table->dropColumn('tax');
        });
    }
}
