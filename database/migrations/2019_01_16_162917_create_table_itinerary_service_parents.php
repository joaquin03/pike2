<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableItineraryServiceParents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('itinerary_service_parents', function (Blueprint $table) {
            $table->increments('id');
    
            $table->unsignedInteger('itinerary_id');
            $table->foreign('itinerary_id')->references('id')->on('itineraries');
            
            $table->unsignedInteger('service_id');
            $table->foreign('service_id')->references('id')->on('services');
            
            $table->text('itinerary_provider_services');
            
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('itinerary_service_parents');
    }
}
