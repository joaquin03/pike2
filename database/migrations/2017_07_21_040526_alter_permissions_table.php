<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('aircraft_permissions', function (Blueprint $table)
        {
            $table->char('country', 2)->nullable();
            $table->timestamp('canceled_at')->nullable();
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('aircraft_permissions', function (Blueprint $table)
        {
            $table->dropColumn('country', 2)->nullable();
            $table->dropColumn('canceled_at')->nullable();
        
        });
    }
}
