<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterItineraryProviderServicesTableAddProcurementAdmFee extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('itinerary_provider_services', function (Blueprint $table) {
            $table->double('procurement_adm_fee')->nullable();
        });
        Schema::table('accounting_services', function (Blueprint $table) {
            $table->double('procurement_adm_fee')->nullable();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('itinerary_provider_services', function (Blueprint $table) {
            $table->dropColumn('procurement_adm_fee');
        });
        Schema::table('accounting_services', function (Blueprint $table) {
            $table->dropColumn('procurement_adm_fee');
        });
    }
}
