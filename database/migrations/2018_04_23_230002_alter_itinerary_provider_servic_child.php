<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterItineraryProviderServicChild extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('itinerary_provider_services', function (Blueprint $table) {
            $table->unsignedInteger('parent_id')->nullable();
            $table->foreign('parent_id', 'itinerary_provider_services_parent_id_foreign')
                ->references('id')->on('itinerary_provider_services');
            $table->dropColumn('is_service_parent');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('itinerary_provider_services', function (Blueprint $table) {
            $table->dropForeign('itinerary_provider_services_parent_id_foreign');
            $table->dropColumn('parent_id');
            $table->boolean('is_service_parent')->default(0);
        });
    }
}
