<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlightPlanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('flight_plans', function (Blueprint $table)
		{
			$table->increments('id');

			$table->unsignedInteger('operation_id');
			$table->foreign('operation_id')->references('id')->on('operations');

			$table->string('flight_type')->nullable();
			$table->string('speed')->nullable();
			$table->string('level')->nullable();
			$table->string('route')->nullable();

			$table->string('icao_route')->nullable();

			$table->string('eet')->nullable(0);

			$table->timestamps();
			$table->softDeletes();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::dropIfExists('flight_plans');
    }
}
