<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCreditNotesBillsAddInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('credit_notes_bills', function (Blueprint $table) {
            $table->string('cfe_type')->nullable();
            $table->string('reason')->nullable();
            $table->double('amount')->nullable();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('credit_notes_bills', function (Blueprint $table) {
            $table->dropColumn('cfe_type');
            $table->dropColumn('reason');
            $table->dropColumn('amount');
        });
    }
}
