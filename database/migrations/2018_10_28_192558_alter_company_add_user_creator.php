<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCompanyAddUserCreator extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function(Blueprint $table){
            $table->unsignedInteger('creator_user_id')->nullable();
            $table->foreign('creator_user_id')->references('id')->on('users');
        });
    
        Schema::table('contacts', function(Blueprint $table){
            $table->unsignedInteger('creator_user_id')->nullable();
            $table->foreign('creator_user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('companies', function(Blueprint $table){
            $table->dropForeign('companies_creator_user_id_foreign');
            $table->dropColumn("creator_user_id");
        });
    
        Schema::table('contacts', function(Blueprint $table){
            $table->dropForeign('contacts_creator_user_id_foreign');
            $table->dropColumn("creator_user_id");
        });
    }
}
