<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAccountingServicesTableNewLogic extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accounting_services', function (Blueprint $table) {
            $table->renameColumn('tax', 'procurement_tax');
            $table->renameColumn('operation_cost', 'procurement_cost');
            $table->renameColumn('service_quantity', 'procurement_quantity');
            $table->renameColumn('billing_price', 'billing_final_price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accounting_services', function (Blueprint $table) {
            $table->renameColumn('procurement_tax', 'tax');
            $table->renameColumn('procurement_cost', 'operation_cost');
            $table->renameColumn('procurement_quantity', 'service_quantity');
            $table->renameColumn('billing_final_price', 'billing_price');
        });
    }
}
