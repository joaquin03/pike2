<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivityContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('activities_contacts', function (Blueprint $table) {
			$table->increments('id');

			$table->unsignedInteger('contact_id');
			$table->unsignedInteger('activity_id');

            $table->foreign('contact_id')->references('id')->on('contacts');
            $table->foreign('activity_id')->references('id')->on('activities');

			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::dropIfExists('activities_contacts');
    }
}
