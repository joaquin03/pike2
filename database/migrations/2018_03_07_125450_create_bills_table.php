<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bills', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('bill_number')->nullable();
            $table->unsignedInteger('itinerary_id')->nullable();
            $table->unsignedInteger('operation_id')->nullable();
            $table->unsignedInteger('company_id');
            $table->unsignedInteger('user_id');
            $table->integer('amount');
            $table->enum('type', ['income', 'outcome']);
            $table->dateTime('date');
            $table->dateTime('due_date')->nullable();
            $table->string('status');
            $table->string('note')->nullable();
            $table->json('attachment')->nullable();

            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('itinerary_id')->references('id')->on('itineraries');
            $table->foreign('operation_id')->references('id')->on('operations');
            $table->foreign('company_id')->references('id')->on('companies');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bills');
    }
}
