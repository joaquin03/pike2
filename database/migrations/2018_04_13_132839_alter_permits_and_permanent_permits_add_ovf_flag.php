<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPermitsAndPermanentPermitsAddOvfFlag extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('aircraft_permissions', function (Blueprint $table) {
            $table->boolean('is_ovf')->nullable()->default(0);
        });
    
        Schema::table('permanent_permissions', function (Blueprint $table) {
            $table->boolean('is_ovf')->nullable()->default(0);
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('aircraft_permissions', function (Blueprint $table) {
            $table->dropColumn('is_ovf');
        });
    
        Schema::table('permanent_permissions', function (Blueprint $table) {
            $table->dropColumn('is_ovf');
        });
    }
    
    
    
}
