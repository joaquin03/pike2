<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use App\Models\Airport;
use App\Models\Client;
use App\Models\CrewMember;
use App\Models\Aircraft;
use App\Models\AircraftType;
use App\Models\Operator;
use App\Models\Provider;
use App\Models\User;


$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    return [
        'name'           => $faker->name,
        'email'          => $faker->unique()->safeEmail,
        'password'       => 'secret',
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Models\Company::class, function (Faker\Generator $faker) {
    return [
        'name'            => $faker->company,
        'country'         => $faker->countryCode,
        'website'         => 'http://' . $faker->domainName,
        'payment_methods' => $faker->randomElements(['credit card', 'wire transfer', 'cash']),
        'is_client'		  => $faker->boolean(),
        'is_operator'	  => $faker->boolean(),
        'is_provider'	  => $faker->boolean(),
        'business'        => $faker->randomElement([
            'Aerospace, Defense and Security',
            'Airline industry',
            'Banking',
            'Chemistry & Pharma',
            'Consumer goods',
            'Health',
            'Insurance',
            'Manufacturing',
            'Mining industry',
            'Public sector',
            'Telecom',
            'Tourism',
            'Transport',
            'Utilities & Energy']),
    ];
});

$factory->define(App\Models\Contact::class, function (Faker\Generator $faker) {
    return [
        'name'        => $faker->firstName,
        'surname'     => $faker->lastName,
        'role'        => $faker->jobTitle,
        'description' => $faker->paragraphs(3, true),
    ];
});

$factory->define(App\Models\Details::class, function (Faker\Generator $faker) {
    $type = $faker->randomElement(['phone', 'email', 'address']);
    $subtype = $faker->randomElement(['laboral', 'personal', 'others']);
    
    $data = '';
    switch ($type) {
        case 'phone':
            $data = $faker->phoneNumber;
            break;
        case 'email':
            if ($subtype == 'laboral') {
                $data = $faker->companyEmail;
            } else {
                $data = $faker->email;
            }
            break;
        case 'address':
            $data = $faker->address;
            break;
    }
    
    
    return [
        'type'    => $type,
        'subtype' => $subtype,
        'data'    => $data,
    ];
});

$factory->define(App\Models\Activity::class, function (Faker\Generator $faker) {
    return [
        'user_id'    => User::inRandomOrder()->first()->id,
        'visibility' => $faker->randomElement(['public', 'private']),
        'date'     => $faker->dateTimeBetween('-8 months'),
    ];
});

$factory->define(App\Models\Email::class, function (Faker\Generator $faker) {
    return [
        'subject'  => $faker->sentence,
        'from'     => $faker->email,
        'to'       => $faker->email,
        'body'     => $faker->paragraphs(2, true),
        'proposal' => $faker->randomElement(['waiting_response', 'accepted', 'declined']),
    ];
});

$factory->define(App\Models\Meeting::class, function (Faker\Generator $faker) {
    $data = new \Carbon\Carbon($faker->dateTimeBetween('-8 months')->format('Y-m-d H:i:s'));
    $time = $faker->dateTimeBetween->format('h:i');
    
    return [
        'name'     => $faker->sentence,
        'start_date' => $data,
        'start_time' => $time,
        'end_date' => $data,
        'end_time' => $time,
        'location' => $faker->address,
        'summary'  => $faker->paragraphs(3, true),
    ];
});

$factory->define(App\Models\Call::class, function (Faker\Generator $faker) {
    $data = new \Carbon\Carbon($faker->dateTimeBetween('-8 months')->format('Y-m-d H:i:s'));
    $time = $faker->dateTimeBetween->format('H:i:s');
    
    return [
        'name'    => $faker->sentence,
        'start_date' => $data,
        'start_time' => $time,
        'end_date' => $data,
        'end_time' => $time,
        
        'summary' => $faker->paragraphs(3, true),
    ];
});

$factory->define(App\Models\Note::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->sentence,
        'body'  => $faker->paragraphs(3, true),
    ];
});

$factory->define(App\Models\Reminder::class, function (Faker\Generator $faker) {
    return [
        'title'             => $faker->sentence,
        'description'       => $faker->paragraphs(2, true),
        'due_date'			=> $faker->dateTimeBetween('now' ,'30 days')->format('Y-m-d H:i:s'),
        'assign_to_user_id' => random_int(1, 2),
    ];
});

$factory->define(App\Models\Comment::class, function (Faker\Generator $faker) {
    return [
        'body'       => $faker->paragraphs(2, true),
        'visibility' => $faker->randomElement(['public', 'private']),
        'user_id'    => random_int(1, 2),
    ];
});

$factory->define(App\Models\AircraftType::class, function (Faker\Generator $faker) {
    $code = $faker->swiftBicNumber;
    return [
        'manufacturer'	=> $faker->company,
        'model'			=> $faker->word ." ". $code,
        'code'			=> $code,
    ];
});

$factory->define(App\Models\Aircraft::class, function (Faker\Generator $faker) {
    return [
        'client_id'		=> Client::inRandomOrder()->first()->id,
        'operator_id'	=> Operator::inRandomOrder()->first()->id,
        'aircraft_type_id'	=> AircraftType::inRandomOrder()->first()->id,
        'registration' 	=> $faker->swiftBicNumber,
    
    ];
});

$factory->define(App\Models\Service::class, function (Faker\Generator $faker) {
    return [
        'name'      	=> $faker->word,
    ];
});

$factory->define(App\Models\Operation::class, function (Faker\Generator $faker) {
    $aircraft = Aircraft::inRandomOrder()->first();
    return [
        'creator_user_id'	=> User::inRandomOrder()->first()->id,
        'in_charge_user_id'	=> User::inRandomOrder()->first()->id,
        'state'				=> $faker->randomElement(['Requested', 'In Process', 'Completed', 'Canceled']),
        'notes'				=> $faker->text(),
        'aircraft_id'			=> $aircraft->id,
        'aircraft_operator_id'	=> $aircraft->operator_id,
        'aircraft_client_id' 	=> $aircraft->client_id,
        
        'crew_captain_id'		=> CrewMember::inRandomOrder()->IsCaptain()->first()->id,
        'crew_first_official_id'=> CrewMember::inRandomOrder()->IsFirstOfficial()->first()->id,
        'crew_auxiliary_id'		=> CrewMember::inRandomOrder()->IsAuxiliary()->first()->id,
    ];
});

$factory->define(App\Models\Itinerary::class, function (Faker\Generator $faker) {
    $date = $faker->dateTimeBetween('now' ,'60 days')->format('Y-m-d H:i:s');
    return [
        'airport_icao'	=> Airport::inRandomOrder()->first()->icao,
        'operator_id' 	=> Operator::inRandomOrder()->first()->id,
        'type'			=> $faker->randomElement(['Arrive', 'Departure']),
        'estimated'		=> $date,
        'actual'		=> $date,
        
        'fuel_provider_id' 	=> Provider::inRandomOrder()->first()->id,
        'fuel_gallons' 		=> $faker->numberBetween(200, 400) *100,
        'fuel_price' 		=> $faker->numberBetween(18, 22) / 10,
    ];
});

$factory->define(App\Models\Airport::class, function (Faker\Generator $faker) {
    
    return [
        'name'			=> $faker->name,
        'icao'			=> $faker->countryCode.$faker->citySuffix.str_random(3),
        'country'       => $faker->countryCode,
        'descriptions'	=> $faker->text(),
    ];
});

$factory->define(App\Models\CrewMember::class, function (Faker\Generator $faker) {
    
    return [
        'name'			=> $faker->name,
        'country'       => $faker->countryCode,
        'is_captain'	=> $faker->boolean(),
        'is_first_official'	=> $faker->boolean(),
        'is_auxiliary'		=> $faker->boolean()
    ];
});

$factory->define(App\Models\FlightPlan::class, function (Faker\Generator $faker) {
    
    return [
        'flight_type'	=> $faker->name,
        'speed'			=> $faker->numberBetween(100, 300),
        'level'			=> $faker->numberBetween(100, 300),
        'route'			=> $faker->address,
        'icao_route'	=> $faker->shuffleString(),
        'eet'			=> $faker->shuffleString(),
    ];
});




