<?php

namespace App\Listeners;

use App\Events\PaymentDone;
use App\Models\Accounting\Bill;
use App\Models\Accounting\Invoice;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class PayBills
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PaymentDone  $event
     * @return void
     */
    public function handle(PaymentDone $event)
    {
        $invoices = $event->payment->invoices()->get();
        $bills = $event->payment->billsAndDebitNotes()->get();

        foreach ($invoices as $invoice) {
            $invoice->status = $invoice->getStatus();
            $invoice->save();
        }
        foreach ($bills as $bill) {
            $bill->status = $bill->getStatus();
            $bill->save();
        }

    }
}
