<?php

namespace App\Listeners;

use App\Events\RegisterProcurementService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Provider;

class AddProvidersToService
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RegisterProcurementService  $event
     * @return void
     */
    public function handle(RegisterProcurementService $event)
    {
        $providers = Provider::all();
        foreach($providers as $provider)
        {
            $provider->services()->syncWithoutDetaching($event->service);

        }

    }
}
