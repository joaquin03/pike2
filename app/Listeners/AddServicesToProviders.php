<?php

namespace App\Listeners;

use App\Events\RegisterProvider;
use App\Models\Service;
use App\Models\ServiceParent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;


class AddServicesToProviders
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RegisterProvider  $event
     * @return void
     */
    public function handle(RegisterProvider $event)
    {
        $services = Service::all();
        foreach($services as $service)
        {
            $service->providers()->syncWithoutDetaching($event->provider);
        }
    }
}
