<?php

namespace App\Listeners;

use App\Events\PaymentDeleted;
use App\Models\Accounting\Bill;
use App\Models\Accounting\Invoice;
use App\Models\Accounting\Payment;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RemovePaymentBills
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    /**
     * Handle the event.
     *
     * @param  PaymentDeleted $event
     * @return void
     */
    
    public function handle(PaymentDeleted $event)
    {
        $invoices = $event->payment->invoices;
        $bills = $event->payment->bills;
        
        
        foreach ($invoices as $invoice) {
            $invoice->payments()->detach($event->payment);
            $invoice->status = $invoice->getStatus();
            $invoice->save();
        }
        foreach ($bills as $bill) {
            $bill->payments()->detach($event->payment);
            $bill->status = $bill->getStatus();
            $bill->save();
        }
    }
}
