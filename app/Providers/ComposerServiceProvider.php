<?php
/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 11/28/16
 * Time: 3:22 PM
 */

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
	/**
	 * Register bindings in the container.
	 *
	 * @return void
	 */
	public function boot()
	{
		// Using Closure based composers...
		View::composer(
            ['backpack::inc.menu', 'dashboard'],
			'App\Http\ViewComposers\HeaderComposer'
		);
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}
}
