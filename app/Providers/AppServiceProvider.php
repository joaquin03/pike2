<?php

namespace App\Providers;

use App\Models\Itinerary;
use App\Observer\ItineraryObserver;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        $this->setObservers();
    
        Blade::directive('money', function ($amount) {
            
            return "<?php echo '$' . number_format($amount, 2, ',', '.' ); ?>";
        });
        
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->alias('bugsnag.logger', \Illuminate\Contracts\Logging\Log::class);
        $this->app->alias('bugsnag.logger', \Psr\Log\LoggerInterface::class);
    }
    
    private function setObservers()
    {
        Itinerary::observe(ItineraryObserver::class);
    }
}
