<?php

namespace App\Notifications;

use App\Models\PermanentPermission;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class PermanentPermissionNotification extends Notification
{
    use Queueable;
    protected $permanentPermission;
    protected $days;
    
    
    public function __construct(PermanentPermission $permanentPermission, $days)
    {
        $this->permanentPermission = $permanentPermission;
        $this->days = $days;
    }
    
    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }
    
    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if ($this->days == 1) {
            return (new MailMessage)
                ->line('The permanent permission: code  ' . $this->permanentPermission->code . ' is going to expire in ' . $this->days . 'day!')
                ->action('See reminders', url('/admin/dashboard'))
                ->line('Thank you for using pike!');
        } else {
            return (new MailMessage)
                ->line('The permanent permission: code  ' . $this->permanentPermission->code . ' is going to expire in ' . $this->days . 'days!')
                ->action('See reminders', url('/admin/dashboard'))
                ->line('Thank you for using pike!');
            
        }
    }
    
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
