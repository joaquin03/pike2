<?php

namespace App\Console\Commands;

use App\Models\Operation;
use Illuminate\Console\Command;

class AlertProcurementReminderCompletedOperations extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notifications:reminderCompletedOperations';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notificate procurement about old completed operations';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Operation::reminderCompletedOperations(5);
        //Deprecated as no one uses it
    }
}
