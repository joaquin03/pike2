<?php

namespace App\Console\Commands;

use App\Models\User;
use App\Models\PermanentPermission;
use App\Notifications\PermanentPermissionNotification;
use Illuminate\Console\Command;

class PermanentPermissionNotificationsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notifications:permanentPermissionsExpiration';
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notificate 10 days and 1 day before expires';
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    
    protected $days;
    protected $status;
    
    public function __construct()
    {
        parent::__construct();
    }
    
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //obtner permisos por expirar y notificar
        $users = User::whereIn('id', \Config::get('constants.notificationUsers'))->get();
        $permissions = PermanentPermission::getAboutToExpire(10, 0);
        $permissions2 = PermanentPermission::getAboutToExpire(1, 1);
        
        
        foreach ($permissions as $permission) {
            foreach ($users as $user) {
                $user->notify(new PermanentPermissionNotification($permission, 10));
            }
            $permission->flag_notified=1;
            $permission->save();
        }
    
        foreach ($permissions2 as $permission) {
            foreach ($users as $user) {
                $user->notify(new PermanentPermissionNotification($permission, 1));
            }
            $permission->flag_notified=2;
            $permission->save();
        }
        
    }
}
