<?php

namespace App\Console\Commands;

use App\Models\Accounting\Bill;
use App\Models\Accounting\Invoice;
use Illuminate\Console\Command;


class CleanBills extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bills:clean';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'remove bills not saved';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $bills = Bill::where('is_active', 0)->get();

        foreach($bills as $bill) {
            $bill->deleteAccountingServices();
            if ($bill->delete() == false) {
//                dd($bill);
            };
        }

        $invoices = Invoice::where('is_active', 0)->get();
        foreach($invoices as $invoice) {
            $invoice->deleteAccountingServices();
            if ($invoice->delete() == false) {
//                dd($invoice);
            };
        }
    }
}