<?php

namespace App\Policies;

use App\Models\User;
use App\Operation;
use Illuminate\Auth\Access\HandlesAuthorization;

class PostPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the operation.
     *
     * @param \App\Models\User $user
     * @param \App\Operation $operation
     * @return mixed
     */
    public function view(User $user, \App\Models\Operation $operation)
    {
        //
    }

    /**
     * Determine whether the user can create operations.
     *
     * @param \App\Models\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the operation.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Operation $operation
     * @return mixed
     */
    public function update(User $user, \App\Models\Operation $operation)
    {
        return $user->can('Operation');
    }

    /**
     * Determine whether the user can delete the operation.
     *
     * @param \App\Models\User $user
     * @param \App\Operation $operation
     * @return mixed
     */
    public function delete(User $user, Operation $operation)
    {
        //
    }
}
