<?php

namespace App\Observer;

use App\Models\Itinerary;

class ItineraryObserver
{
    
    public function creating(Itinerary $itinerary)
    {
        $itinerary->setCode();
    }
    
    public function created(Itinerary $itinerary)
    {
        $itinerary->save();
        $itinerary->createFlightPlan();
    }
    public function editing(Itinerary $itinerary)
    {
    }
    
    public function edited(Itinerary $itinerary)
    {
        
    }
    
}