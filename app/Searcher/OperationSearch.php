<?php
namespace App\Searcher;
use App\Models\Operation;
use Illuminate\Http\Request;

class OperationSearch
{
    private $query;
    /**
     * OperationSearch constructor.
     */
    public function __construct(Operation $class)
    {
        $this->query = $class->newQuery();
    }
    
    public function apply(Request $filters)
    {
        $this->addSelects();
        
        $this->addJoins();
    
        $this->applyWheres($filters);
    
        $this->filterByStatus($filters);
        
        $this->applyOrder($filters);
        
        return $this->query->paginate(50);
    }
    
    private function addSelects()
    {
        $this->query->select('operations.*', 'clientCompanies.name', 'operatorCompanies.name', 'users.name',
            'aircraft.registration');
    }

    private function addJoins() {
        $this->query->leftJoin('users', 'operations.in_charge_user_id', '=', 'users.id');
        $this->query->leftJoin('companies as operatorCompanies', 'operations.aircraft_operator_id', '=', 'operatorCompanies.id');
        $this->query->leftJoin('companies as clientCompanies', 'operations.aircraft_client_id', '=', 'clientCompanies.id');
        $this->query->leftJoin('aircraft', 'operations.aircraft_id', '=', 'aircraft.id');

    }
    
    private function applyWheres(Request $filters)
    {
        $this->query->where(function ($q) use($filters){
            $input = $filters->get('q');
    
            $q->where('users.name', 'LIKE', '%'.$input.'%');
            $q->orwhere('code', 'LIKE', '%'.$input.'%');
            $q->orwhere('operatorCompanies.name', 'LIKE', '%'.$input.'%');
            $q->orwhere('clientCompanies.name', 'LIKE', '%'.$input.'%');
            $q->orwhere('aircraft.registration', 'LIKE', '%'.$input.'%');
            $q->orwhere(function($query) use ($input) {
                $query->whereHas('itineraries', function ($query2) use ($input) {
                    $query2->where('itineraries.airport_icao_string', 'LIKE', '%'.$input.'%');
                });
            });
        });
    }

    private function applyOrder(Request $filters)
    {
        if($filters->get('order') != null){
            $this->query->orderBy($filters->get('order'), $filters->get('order_type', 'DESC'));
        }
    }
    
    private function filterByStatus(Request $filters)
    {
        if ($filters->has('operation_status')) {
            $inputStatus = $filters->get('operation_status');
            $this->query->whereIn('state', $inputStatus);
        }
        if ($filters->has('procurement_status')) {
            $inputStatus = $filters->get('procurement_status');
            $this->query->whereIn('procurement_status', $inputStatus);
        }
        if ($filters->has('billing_status')) {
            $inputStatus = $filters->get('billing_status');
            $this->query->whereIn('billing_status', $inputStatus);
        }
    }
    
    
}