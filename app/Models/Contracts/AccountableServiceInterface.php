<?php
/**
 * Created by PhpStorm.
 * User: Joaquin
 * Date: 14-Dec-18
 * Time: 12:29 PM
 */

namespace App\Models\Contracts;


use App\Models\Accounting\AccountingService;
use Illuminate\Database\Eloquent\Model;

interface AccountableServiceInterface
{
    
    public function createAccountingService(AccountingItem $bill);

   
    public function getFinalCostQuotation();
    /// Procurement
    public function getProcurementDescription();
    public function getProcurementQuantity();
    public function getProcurementCost();
    public function getProcurementAdmFee();
    public function getProcurementTax();
    public function getProcurementUnitFinalCost();
    public function getProcurementFinalCost();

    /// Billing
    public function getBillingDescription();
    public function getBillingQuantity();
    public function getBillingPercentage();
    public function getBillingUnitPrice();
    public function getBillingFinalPrice();

    //Quotation
    public function getQuotationFinalPrice();

    public function getProfitPrice();

    // Helpers
    public function addBill($billId);

    public function addInvoice();

    public function isContainedInStatement($type);
    
    public function removeBill();
}