<?php
/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 3/30/18
 * Time: 4:41 PM
 */

namespace App\Models\Contracts;


interface CompanyStatementInterface
{
    public function getAccountingItems();
    
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function bills();
    public function payments();
    public function creditNotes();
}