<?php
/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 5/10/18
 * Time: 2:25 AM
 */

namespace App\Models\Contracts;


use App\Models\Accounting\Bill;

interface AbstractOperationServiceInterface
{
    public function getProfitPrice();
    
    public function getFinalPrice();
    
    public function getServiceFinalPrice();
    
    public function createAccountingService(AccountingItem $bill);
    
    public function addBill($billId);
    
    public function addInvoice();
    
    public function getProcurementDescription();
    
    public function getBillingDescription();
    
    public function getFinalCost();
    
    public function getFinalCostQuotation();
    
}