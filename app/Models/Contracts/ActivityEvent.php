<?php

namespace App\Models\Contracts;

use App\Models\Activity;
use App\Models\CompanyTag;
use App\Models\Contact;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use OwenIt\Auditing\Contracts\Auditable;

abstract class  ActivityEvent extends Model implements Auditable
{
    use SoftDeletes;
    use \OwenIt\Auditing\Auditable;
    
    const DEFAULT_COLOR = 'bg-blue';
    
    private $rules = [];
    
    public abstract function getTitle();
    
    public abstract function getDescription();
    
    public abstract function getIcon();
    
    public function activity()
    {
        return $this->morphOne(Activity::class, 'event');
    }
    
    public function delete()
    {
        if (parent::delete()) {
            return $this->activity->delete();
        };
        return false;
    }
    
    
    public function getColor()
    {
        return self::DEFAULT_COLOR;
    }
    
    public function getClassName()
    {
        $path = explode('\\', get_class($this));
        
        return array_pop($path);
    }
    
    public function updateEvent($data)
    {
        $this->validate($data);
        
        $this->fill($data);
        
        if (!$this->save()) {
            throw new \Exception('The event could not be saved');
        }
        
        return $this;
    }
    
    public function validate($data)
    {
        // make a new validator object
        $validator = Validator::make($data, $this->rules);
        
        // return the result
        if ($validator->fails()) {
            throw new ValidationException($validator);
        }
        
        return true;
    }
    
    public function getDate()
    {
        return $this->activity->date;
    }
    
    public function setActivityDate($date)
    {
        $activity = $this->activity;
        if ($activity != null) {
            $activity->date = $date;
            return $activity->save();
        }
        return true;
    }
    
    public function getSlugClassName()
    {
        return strtolower(preg_replace('/(?<!\ )[A-Z]/', '-$0', lcfirst($this->getClassName())));
    }
    
    /*public function delete()
    {
        if (parent::delete()){
            return $this->activity->delete();
        };
        return false;
    }*/
    
    
    public function contacts()
    {
        return $this->morphToMany(Contact::class, 'event', 'contactables', 'event_id', 'contact_id');
    }
    
    public function getContactCompanyName(){
        if($this->contacts->first()) {
            return $this->contacts->first()->company->name;
        }
        return '';
    }
    
    public function getContactsInfo()
    {
        $contactsInfo = '';
        
        if($this->contacts) {
            foreach ($this->contacts as $contact) {
                $contactsInfo = $contactsInfo . " " . $contact->name . ' ' . $contact->surname . ', ' . $contact->getEmails() . '; ';
            }
        }
        return rtrim($contactsInfo, "; ");
    
    }
    
    public function tags()
    {
        return $this->belongsToMany(CompanyTag::class, 'note_note_tags');
    }
    
    public function getAuditableDescription()
    {
        return 'Company: '.$this->getCompanyName().' - '.$this->getTitle();
    }
    
    private function getCompanyName()
    {
        if ($this->activity == null) {
            return '';
        }
        $company = $this->activity->company;
        if ($company == null) {
            return '';
        }
        return $company->name;
    }
}