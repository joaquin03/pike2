<?php


namespace App\Models\Contracts;


use App\Models\Accounting\AccountingService;
use App\Models\Accounting\AdministrationFee;
use App\Models\Accounting\Bill;
use App\Models\Accounting\Invoice;
use App\Models\Service;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

abstract class AbstractOperationService extends Model implements AbstractOperationServiceInterface, Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    public function getProfitPrice()
    {
        return 0;
    }
    
    public function getUnitPrice()
    {
        return 0;
    }
    
    public function getFinalPrice()
    {
        return 0;
    }
    
    public function getFinalCost()
    {
        return 0;
    }
    
    public function getFinalCostQuotation()
    {
        return 0;
    }
    
    public function getServiceFinalPrice()
    {
        return 0;
    }
    
    public function getServiceFinalCost()
    {
        return 0;
    }
    
    
    public function getQuantity(){
        return 1;
    }
    
    public function getBillingQuantity()
    {
        return 1;
    }
    
    public function getAdmFee(){
        return 0;
    }
    
    public function getTax(){
        return 0;
    }
    
    public function getCost(){
        return 0;
    }
    
    public function createAccountingService(AccountingItem $bill)
    {

        if ($this->canAddAccountingService($bill)) {
            $accountingService = new AccountingService();
            $accountingService->servicesable()->associate($this);
            $accountingService->fill($this->toArray());
            $accountingService->description = $this->getDescription($bill);
            $accountingService->procurement_adm_fee = $this->getAdmFee();
            $accountingService->procurement_tax = $this->getTax();
            $accountingService->procurement_cost = $this->getCost();
            $accountingService->procurement_quantity = $this->getQuantity();
            $accountingService->bill_id = $bill->id;
            $accountingService->billing_quantity = $this->getBillingQuantity();
            $accountingService->billing_final_price = $this->getBillingFinalPrice($bill);
            $accountingService->billing_unit_price = $this->getUnitPrice();
            $accountingService->save();
        }
    }
    
    
    private function canAddAccountingService(AccountingItem $bill)
    {
        if ($bill->isFromProcurement() && $this->procurement_status != 1) {
            return true;
        }
        if ($bill->isFromBilling() && $this->has_bills == 0) {
            return true;
        }
        return false;
    }
    private function getBillingFinalPrice(AccountingItem $bill)
    {
        if ($bill->isFromProcurement()) {
            return $this->getFinalCost();
        }
        return $this->getFinalPrice();
    }
    
    public function addBill($billId)
    {
        $this->has_bills = $billId;
        $this->save();
    }
    
    public function addInvoice()
    {
        $this->procurement_status = 1;
        $this->save();
    }
    
    private function getDescription(AccountingItem $bill)
    {
        if ($bill->isFromProcurement()) {
                return $this->getProcurementDescription();
        }
        return $this->getBillingDescription();
    }
    
    public function getProcurementDescription()
    {
        return '';
    }
    
    public function getBillingDescription()
    {
        return $this->getProcurementDescription();
    }

    public function isContainedInStatement($type)
    {
        return $type == 'invoice' ? $this->procurement_status : $this->has_bills;
    }


    
}