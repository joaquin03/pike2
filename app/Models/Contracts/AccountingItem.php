<?php
/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 3/22/18
 * Time: 3:09 PM
 */

namespace App\Models\Contracts;


use App\Models\Accounting\Invoice;
use App\Models\Accounting\Payment;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use mysql_xdevapi\Exception;
use OwenIt\Auditing\Contracts\Auditable;

abstract class AccountingItem extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    public static $types = [102 => 'Credit e-Ticket Rebate', 103 => 'Debit e-Ticket Rebate'];

    public $balance = null;

    protected $casts = [
        'attachment' => 'array'
    ];
    
    
    public function getDescription()
    {
        return (new \ReflectionClass($this))->getShortName();
    }
    
    public function setDateAttribute($value)
    {
        $date = Carbon::parse($value)->startOfDay();

        $this->attributes['date'] = $date;
    }
    
    public function getDateAttribute($value)
    {
        return Carbon::parse($value);
    }

    
    public function scopeIsActive(Builder $q)
    {
        return $q->where('is_active', 1);
    }
    
    public function getDocumentoSFL()
    {
        return;
    }
    
    
    
    
    /*** Report ****/
    
    public function getAccountableNumber()
    {
        return '';
    }
    
    public function company()
    {
        return '';
    }
    
    public function getStatus()
    {
        return '';
    }
    public function getDebit()
    {
        return '';
    }
    public function getCredit()
    {
        return '';
    }
    public function getAccountableStatus()
    {
        return '';
    }

    public function setBalance($value, $request)
    {
        $this->balance = $value;
    }

    public abstract function getBalance();


}