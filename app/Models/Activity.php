<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class Activity extends Model
{
    use CrudTrait, SoftDeletes;

    /*
   |--------------------------------------------------------------------------
   | GLOBAL VARIABLES
   |--------------------------------------------------------------------------
   */

    protected $table = 'activities';
    protected $fillable = [
        'visibility', 'user_id', 'date', 'company_id'
    ];
    protected $dates = ['date', 'created_at'];


    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function event()
    {
        return $this->morphTo('event');
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getDate()
    {
        return $this->date->format('d/m/Y');
    }

    public function contacts()
    {
        return $this->belongsToMany(Contact::class, 'activities_contacts', 'activity_id', 'contact_id');
    }


    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    public function scopeOrderByDate($query)
    {
        return $query->orderBy('date', 'desc');
    }

    public function scopePublicOrMine(Builder $query)
    {
        return $query->where(function ($q) {
            $q->where('visibility', 'public')
                ->orWhere('user_id', \Auth::id());
        });
    }

    public function scopeWithOutSpecialFeatures(Builder $query)
    {
        return $query->where('event_type','<>', ActivitySpecialFeatures::class);
    }
    

    
    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
