<?php

namespace App\Models\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class PaymentScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        $builder->where('is_credit_note', '0');
    }
}