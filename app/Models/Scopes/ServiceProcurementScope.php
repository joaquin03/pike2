<?php
/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 12/6/17
 * Time: 1:21 AM
 */

namespace App\Models\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;


class ServiceProcurementScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        $builder->where('is_for_procurement', true);
    }
}