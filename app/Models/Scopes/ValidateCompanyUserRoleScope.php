<?php
/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 10/28/18
 * Time: 4:17 PM
 */

namespace App\Models\Scopes;


use App\Models\Company;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Support\Facades\Auth;

class ValidateCompanyUserRoleScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        $user = Auth::user();
        if ($user != null && Auth::user()->hasRole('Basic-CRM')) {
            $builder->where(function($q) use($user) {
                $companiesId = $user->availableCompanies()->withoutGlobalScope(ValidateCompanyUserRoleScope::class)->pluck('companies.id');
                
                $q->where('companies.creator_user_id', $user->id)
                    ->orWhereIn('companies.id', $companiesId);
            });
        }
    }
    
}