<?php

namespace App\Models;

use App\Models\Contracts\ActivityEvent;
use Backpack\CRUD\CrudTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class ActivityNote extends ActivityEvent
{
    use CrudTrait, SoftDeletes;

     /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'notes';
    
    protected $fillable = [
        'title',
        'body',
        'date'
    ];
    
    protected $dates = ['date'];
    
    
    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function getIcon()
    {
        return 'fa fa-sticky-note';
    }
    
    public function getTitle()
    {
        return "Note: " . $this->title;
    }
    public function getDescription()
    {
        return $this->body;
    }
    
    public function getColor()
    {
        return 'bg-yellow';
    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    
    
    
    public function getCompanyName(){
        if ($this->activity->company) {
            return $this->activity->company->name;
        }
        return '-';
    }
    
    
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */
    public function getDate()
    {
        return $this->date;
    }
    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    
    public function setDateAttribute($value)
    {
        $date = Carbon::createFromFormat('Y-m-d', $value);
        
        $this->attributes['date'] = $date;
        $this->setActivityDate($date);
    }
    
}
