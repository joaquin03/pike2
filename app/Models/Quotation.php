<?php

namespace App\Models;

use App\Models\Scopes\OperationScope;
use App\Models\Scopes\ProcurementScopes;
use App\Notifications\NewOperationCreatedFromQuotationNotification;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use App\Models\Accounting\Bill;
use App\Models\Scopes\BillingScopes;
use App\Models\Scopes\QuotationScope;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class Quotation extends Procurement
{
    use CrudTrait, SoftDeletes;

    public static function boot()
    {
        static::addGlobalScope(function(Builder $query) {
                $query->withoutGlobalScope(ProcurementScopes::class)->withoutGlobalScope(OperationScope::class);
        });

        parent::boot();
        static::addGlobalScope(new QuotationScope());

        static::created(function (Quotation $model) {
            $model->getOrCreateQuotationExtended();

        }, 0);
    }
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    public static $quotationStates = [ 'blue' => 'Pending Data Sheet', 'orange' => 'In Process', 'black' => 'Pending Answer',
        'red' => 'Canceled', '#FF3336' => 'Lost', 'pink' => 'Budget', 'green' => 'Confirmed'];

    public static $serviceTypes = [ 'Flight Support', 'Handling', 'Fuel', 'Permits', 'Vip Assistance', 'Other'];

    public static $paymentMethods = ['Santander', 'Bank Of America','Bandes', 'Banistmo'];

    public static $paymentTypes= ['Wire Transfer', 'Credit Card'];

    public static $typeOfFlight = ['Ambulance', 'Medevac', 'Private', 'Military', 'Diplomatic', 'Charter', 'Commercial', 'Cargo', 'Other'];

    public static $typeOfVIPAssitance = ['Arrival', 'Departure', 'Transit'];

    protected $casts = ['documents' => 'array'];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function createItinerary($itineraryData)
    {
        $itinerary = new Itinerary();
        $itinerary->fill($itineraryData);
        $itinerary->operation_id = $this->id;


        if ($itinerary->save()) {
            \Alert::success('Itinerary created.')->flash();
            $this->setOldestStartDate();
            return $itinerary;
        }
        return false;
    }

    public function setCode()
    {
        $date = explode('-', $this->start_date);
        $month = $date[0];
        $year = $date[1];
        $id = '001';
        $latest_pn = static::where('code', 'like', 'PQ' . $year . $month . '%')
            ->latest('id')
            ->pluck('code')
            ->all();

        if ($latest_pn != null) {
            $latest_pn = current($latest_pn);
            $pn = substr($latest_pn, 7);
            $id = str_pad(intval($pn) + 1, 3, '0', STR_PAD_LEFT);
        }
        $this->code = "PQ" . $year . $month . $id;
    }


    public static function sortByPNDescending()
    {
        return Quotation::orderBy('code', 'DESC')->get();
    }

    public function getQuotationSubtotalCost(){
        return round($this->itinerariesProviderServicesFinalCostsQuotation(
            [$this->itinerariesProviderServicesChildsNotByPike()->get(),
             $this->permitsChildsNotByPike()->get()]
        ),2);
    }

    public function getQuotationPikeServicesCost(){
        return round($this->itinerariesProviderServicesFinalCostsQuotation(
            [$this->itinerariesProviderServicesChildsByPike()->get(),
                $this->permitsChildsByPike()->get()]
        ),2);
    }

    public function getQuotationTotalPermitsCost(){
        $total = 0;
        foreach ($this->aircraftPermission as $permit){
            $total += $permit->getQuotationFinalPrice();
        }
        return $total;
    }

    public function getQuotationTotalCost(){

        $total = $this->getQuotationSubtotalCost() + $this->getQuotationPikeServicesCost();
        $surcharge = $this->extraData->payment === 'Credit Card' ? $this->getQuotationCreditNoteSurcharge($total) : 0;

        return round( $total + $surcharge ,2);
    }

    public function getQuotationCreditNoteSurcharge($quotationTotal)
    {
        $lastUpdate = QuotationCreditNoteCharges::orderBy('updated_at', 'desc')->first();
        $surcharge = 0;
        $lastUpdate = $lastUpdate->updated_at;

        foreach ($this->itineraries as $itinerary) {
            if($itinerary->isArrival()){
                $itineraryCost =  $itinerary->itineraryCostWithoutCharges();

                if ($lastUpdate) {

                    $surcharges = QuotationCreditNoteCharges::where('updated_at', $lastUpdate)->where('from', '<', $itineraryCost)
                        ->where('to', '>', $itineraryCost)->get();

                    if ($surcharges && !$surcharges->isEmpty()) {
                        $surcharge += $surcharges[0]['value'];
                }
            }}
        }

        return $surcharge;
    }

    public function getFormattedQuotationTotalCost(){
        return number_format($this->getQuotationTotalCost(), 2, ',', '.');
    }

    public function getOrCreateQuotationExtended()
    {
        if ($this->extraData == null) {
            $extraData = new QuotationExtended();
            $extraData->quotation_id = $this->id;
            $extraData->save();
            $this->extraData = $extraData;
        }
        return $this->extraData;
    }

    public function fill(array $attributes)
    {
        if ($this->id != null) {
            $extraData = $this->getOrCreateQuotationExtended();
            $extraData->fill($attributes);
            $extraData->save();
        }
        return parent::fill($attributes);

    }


    public function getIcaoAssistance()
    {
        return "TODO";
    }


    public function createOperation()
    {
        $operation = new Operation();
        $operation->creator_user_id = $this->creator->id;

        $operation->fill($this->toArray());


        $operation->quotation_id = $this->id;
        $operation->state = 'In Process';
        $operation->procurement_status = 'In Process';
        $operation->notes = $operation->notes . '    .- Quotation based operation.';
        $operation->aircraft_client_id = $this->aircraft_client_id;
        $operation->aircraft_operator_id = $this->aircraft_operator_id;
        $operation->aircraft_type_id = $this->aircraft_type_id;
        $operation->start_date = $this->getCustomStartDate() ?? $this->getStartDate();

        $operation->save();
        $this->operation_id = $operation->id;
        $this->save();

        $this->notifyOperationCreated($operation);

        return $operation;
    }

    public function assignOperation($operation_id)
    {
        $this->operation_id = $operation_id;

        $operation = Operation::withoutGlobalScopes()->findOrFail($operation_id);
        $operation->associateQuotation($this->id);

        $this->save();
    }

    private function notifyOperationCreated($operation)
    {
        $operationUser = User::find(config('constants.operation_user_id'));
        foreach (SalesManager::all() as $salesManager){
            self::OperationCreatedFromQuotationReminder($operation, $salesManager);
            //$salesManager->notify(new NewOperationCreatedFromQuotationNotification($operation));
            //comentado para No enviar mails
        }
        $operationUser->notify(new NewOperationCreatedFromQuotationNotification($operation));
    }

    public static function OperationCreatedFromQuotationReminder($operation, $salesManager)
    {
        $reminder = new ActivityReminder();
        $reminder->title = 'Operation ' . $operation->code . ' was created from a Quotation.';
        $reminder->operation_link = '/operation/'.$operation->id.'/edit';
        $reminder->assign_to_user_id = $salesManager->id;

        $reminder->save();

        self::assignReminderToActivityWithUser($reminder, $salesManager);
    }

    public static function assignReminderToActivityQuotation($reminder, $user)
    {
        $activity = new Activity([
            'visibility' => 'public',
            'user_id' => $user,
            'company_id' => env('CENTRAL_PIKE', 51),
            'date' => Carbon::now('UTC')
        ]);
        $activity->event()->associate($reminder);
        $activity->save();
    }

    public function saveDocument($document)
    {
        if ($document) {

            $file_name = 'document-' . $this->id;
            $destination_path = 'storage/quotation-extended/';

            File::put($destination_path.$file_name ,$document);

            $this->extraData->quotation_exportable_file = $destination_path . $file_name;
            $this->extraData->save();
            return $destination_path . $file_name;
        }

        return;

    }

    public function isConfirmed()
    {
        return $this->operation_id != null;
    }

    public function getArrivalOriginDepartureIcao($itinerary)
    {
        $itineraries = $this->itineraries()->where('id', '<', $itinerary->id);

        if ($itineraries){
            $departure = $itineraries->orderByDesc('id')->first();
            if ($departure){
                return $departure->airport_icao_string . '-' . $itinerary->airport_icao_string;
            }
        }
        return $itinerary->airport_icao_string;

    }

    public function getArrivalOriginDeparturePax($itinerary)
    {
        $itineraries = $this->itineraries()->where('id', '<', $itinerary->id);

        if ($itineraries){
            $departure = $itineraries->orderByDesc('id')->first();
            if ($departure){
                return $departure->pax;
            }
        }
        return 0;
    }

    public function operationAirportsList()
    {


        $airportsIcaos = Collection::make($this->itineraryAirports())->pluck('icao');
        if ($airportsIcaos->count() === 0){
            return null;
        }
        if ($airportsIcaos->count() > 3) {
            return $airportsIcaos->implode(' - ') . '…';
        }
        return $airportsIcaos->implode(' - ');
    }

    public function payedWithCreditCard()
    {
        if ($this->extraData) {
            return $this->extraData->payment == 'Credit Card';
        }
        return false;
    }



    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function operation()
    {
        return $this->belongsTo('App\Models\Operation');
    }

    public function extraData()
    {
        return $this->hasOne(QuotationExtended::class, 'quotation_id', 'id');
    }



    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    public function scopeInProcess(Builder $query)
    {
        return $query->where('state', 'Process');
    }
    public function scopeCompleted(Builder $query)
    {
        return $query->where('state', 'Completed');
    }
    public function scopeCanceled(Builder $query)
    {
        return $query->whereIn('state', ['Canceled', 'Lost']);
    }

    public function scopeConfirmed(Builder $query)
    {
        return $query->where('state', 'Confirmed');
    }

    public function scopeBudget(Builder $query)
    {
        return $query->where('state', 'Budget');
    }

    public function addPermission($request)
    {
        $expirationDate = Carbon::parse($request->get('expiration_date'))->format('d/m/Y');
        $requestData = $request->all();
        $requestData['expiration_date'] = $expirationDate;
        $requestData['is_from_quotation'] = 1;
        return AircraftPermission::createItem($requestData, $this);
    }

    public function arrivalsQuantity()
    {
        $count = 0 ;
        foreach ($this->arrivalItineraries as $itinerary) {
            if ($itinerary->hasServices())
                $count++;
        }
        return $count;
    }
    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */
    public static function getQuotationsStates()
    {
        $ret = [];
        foreach (array_values(self::$quotationStates) as $value) {
            $ret[$value] = $value;
        }
        return $ret;
    }


    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
