<?php

namespace App\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class ProviderAirport extends Model
{
    protected $primaryKey = 'provider_id';
    protected $keyType = 'string';

    protected $table = 'provider_airports';

    protected $fillable = ['provider_id', 'airport_icao', 'score'];

    public function provider()
    {
        return $this->belongsTo(Provider::class);
    }

    public function airport()
    {
        return $this->belongsTo(Airport::class);
    }


    public function itineraries()
    {
        return $this->belongsToMany(Itinerary::class, 'itinerary_provider_airports', 'airport_provider_id',
            'itinerary_id');
    }

    //Scopes

    public function scopeExtraData(Builder $builder)
    {
        return $builder
            ->join('companies', 'provider_airports.provider_id', '=', 'companies.id')
            ->join('airports', 'provider_airports.airport_icao', '=', 'airport.icao')
            ->select('provider_airports.*', 'companies.name as provider_name', 'airport.name as airport_name');
    }

    public function scopeProviders(Builder $builder)
    {
        return $builder
            ->join('companies', 'provider_airports.provider_id', '=', 'companies.id')
            ->where('companies.is_provider', true)
            ->select('companies.*')
            ->groupBy('companies.id');
    }
    public function transformer()
    {
        $data = $this->toArray();
        $data['provider_name'] = $this->provider->name;
        $data['airport_name'] = $this->airport->name;
        $data['done'] = (bool) $this->pivot->done;
        return $data;
    }


}
