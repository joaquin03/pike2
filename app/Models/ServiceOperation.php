<?php

namespace App\Models;

use App\Models\Scopes\ServiceOperationScope;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceOperation extends Service
{
    use CrudTrait, SoftDeletes;
    
    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(new ServiceOperationScope());
    }
    /*
   |--------------------------------------------------------------------------
   | GLOBAL VARIABLES
   |--------------------------------------------------------------------------
   */
    protected $fillable = [
        'name', 'description', 'category_id'
    ];
    
    
}
