<?php

namespace App\Models;

use App\Models\Contracts\ActivityEvent;
use Backpack\CRUD\CrudTrait;
use Illuminate\Support\Facades\Storage;

class ActivitySpecialFeatures extends ActivityEvent
{
    use CrudTrait;

     /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'special_features';
    
    protected $fillable = [
        'name',
        'notes',
        'user_id',
    ];
    
    

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function getIcon()
    {
        return 'fa fa-pencil-square-o';
    }
    
    public function getTitle()
    {
        return "Special Feature: ".$this->name;
    }
    public function getDescription()
    {
       
        return $this->notes;
    }
    
    public function getColor()
    {
        return 'bg-blue';
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    
    
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    
}
