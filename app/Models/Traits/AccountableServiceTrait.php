<?php

namespace App\Models\Traits;
use App\Models\Accounting\AccountingService;
use App\Models\Accounting\AdministrationFee;
use App\Models\Contracts\AccountingItem;
use \Venturecraft\Revisionable\Revision;

trait AccountableServiceTrait
{

    public function createAccountingService(AccountingItem $bill)
    {
        if (! $this->canAddAccountingService($bill)) {
            return NULL;
        }
        $accountingService = new AccountingService();
        $accountingService->servicesable()->associate($this);
        $accountingService->fill($this->toArray());
        $accountingService->description = $this->getAccountingDescription($bill);
        $accountingService->bill_id = $bill->id; // colud be a invoice

        $accountingService = $this->setProcurementInvoiceData($accountingService);
        $accountingService = $this->setBillingData($accountingService);

        $accountingService->save();
    }

    public function createAdministrationFee($admFeePercentage)
    {
        $administrationFee = $this->administrationFee;

        if ($administrationFee == null) {
            $administrationFee = new AdministrationFee();
            $administrationFee->servicesable()->associate($this);
            $administrationFee->itinerary_id = $this->itinerary->id;
            $administrationFee->operation_id = $this->itinerary->operation->id;
        }
        $administrationFee->billing_percentage = $admFeePercentage;
        $administrationFee->save();

        return $administrationFee;
    }


    private function setProcurementInvoiceData(AccountingService $accountingService)
    {
        $accountingService->procurement_quantity = $this->getProcurementQuantity();
        $accountingService->procurement_cost = $this->getProcurementCost();
        $accountingService->procurement_adm_fee = $this->getProcurementAdmFee();
        $accountingService->procurement_tax = $this->getProcurementTax();
        $accountingService->procurement_unit_final_cost = $this->getProcurementUnitFinalCost();
        $accountingService->procurement_final_cost = $this->getProcurementFinalCost();

        return $accountingService;
    }
    private function setBillingData(AccountingService $accountingService)
    {
        $accountingService->billing_quantity = $this->getBillingQuantity();
        $accountingService->billing_percentage = $this->getBillingPercentage();
        $accountingService->billing_unit_price = $this->getBillingUnitPrice();
        $accountingService->billing_final_price = $this->getBillingFinalPrice();

        return $accountingService;
    }

    // Helpers
    private function canAddAccountingService(AccountingItem $bill)
    {
        if ($bill->isFromProcurement() && $this->procurement_status != 1) {
            return true;
        }
        if ($bill->isFromBilling() && $this->has_bills == 0) {
            return true;
        }
        return false;
    }

    private function getAccountingDescription($bill)
    {
        if ($bill->isFromBilling()) {
            return $this->getBillingDescription();
        }
        return $this->getProcurementDescription();
    }

    public function addBill($billId)
    {
        $this->has_bills = $billId;
        $this->save();
    }
    
    public function removeBill()
    {
        $this->has_bills = null;
        $this->billing_checked = null;
        $this->adm_fee_checked = null;
        $this->save();
    }

    public function addInvoice()
    {

        $this->procurement_status = 1;
        $this->save();
    }

    public function isContainedInStatement($type)
    {
        return $type == 'invoice' ? $this->procurement_status : $this->has_bills;
    }

}