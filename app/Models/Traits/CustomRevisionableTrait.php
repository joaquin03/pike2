<?php

namespace App\Models\Traits;
use \Venturecraft\Revisionable\Revision;

trait CustomRevisionableTrait
{
    protected $revisionCreationsEnabled = true;

    public function getRevisions($class)
    {
        $revisions = Revision::where('revisionable_type',  get_class($class))
            ->where('revisionable_id', $this->id)->orderBy('created_at', 'DESC')->get()
            ->groupBy(function($revision) {
                return $revision->created_at->format('d.m.Y h:i:s');
            });
        return $revisions;
    }
}