<?php

namespace App\Models;

use App\Models\Accounting\Bill;
use App\Models\Scopes\BillingScopes;
use App\Models\Scopes\OperationScope;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;


class Billing extends Procurement
{
    use SoftDeletes;
    
    public static $states = ['Pending', 'In Process', 'Completed', 'Canceled', 'Deleted'];
    
    
    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(new OperationScope());
        
        //static::addGlobalScope(new BillingScopes());
    }
    
    public static function scopeSortByState(Builder $query)
    {
        return $query->orderByRaw('FIELD(billing_status,"Pending","Canceled", "Completed") ASC')
            ->orderByRaw('FIELD(procurement_status,"Completed","Canceled", "In Process", "Deleted") ASC')
            ->orderByRaw('DATEDIFF(NOW(), start_date) ASC');
        
    }
    
    
    public function createBill($accountingServices = [], $itineraryId = null, $note = null, $admFeePercentage = 0)
    {

        $bill = new Bill();
        //$bill->user_id = Auth::user()->id;
        $bill->user_id = 1;
        $bill->itinerary_id = $itineraryId;
        $bill->operation_id = $this->id;
        $bill->company_id = $this->aircraftClient->id;
        $bill->billing_company_id = $this->billingCompany->id;
        $bill->ref_number = $this->ref_number;
        $bill->date = Carbon::now('UTC');
        $bill->due_date = Carbon::now()->addDays($bill->billing_days_to_pay);
       
        if($itineraryId) { //Si no, es una bill de permisos
            $bill->airport_icao_string = Itinerary::findOrFail($itineraryId)->airport_icao_string;
        }
       
        $bill->type = 'debit';
        $bill->note = $note;
        
        $bill->save();
        $bill->addAccountingServices($accountingServices, $admFeePercentage, $itineraryId);
        $bill->setCalculateAmount();
        
        return $bill;
    }


    
    
}
