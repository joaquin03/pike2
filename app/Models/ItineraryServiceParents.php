<?php

namespace App\Models;

use App\Models\Accounting\AdministrationFee;
use App\Models\Contracts\AccountableServiceInterface;
use App\Models\Traits\AccountableServiceTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Config;

class ItineraryServiceParents extends Model implements AccountableServiceInterface
{
    use AccountableServiceTrait;
 
    
    protected $casts = [
        'itinerary_provider_services' => 'array',
    ];
    
    protected $fillable = [
        'adm_fee_checked', 'billing_checked',
    ];
    
    public function service()
    {
        return $this->hasOne(Service::class, 'id', 'service_id');
    }
    
    public function itinerary()
    {
        return $this->hasOne(Itinerary::class, 'id', 'itinerary_id');
    }

    public function administrationFee()
    {
        return $this->hasOne(AdministrationFee::class, 'id', 'servicesable_id');
    }
    
    public static function createItem(ItineraryProviderService $itineraryProviderService) : bool
    {
        $item = self::findItem($itineraryProviderService);
        
        if ($item == null) {
            return self::newItem($itineraryProviderService);
        }
        return self::editItem($item, $itineraryProviderService);
    }
    
    public static function removeItem(ItineraryProviderService $itineraryProviderService)
    {
        $item = self::findItem($itineraryProviderService);
        
        if ($item == null) {
            return true;
        }
        $collection = $item->removeItineraryProviderService($itineraryProviderService->id);
        if ($collection->count() > 0) {
            $item->itinerary_provider_services = $collection->toArray();
            return $item->save();
        }
        
        return $item->delete();
    }
    
    public static function findItem(ItineraryProviderService $itineraryProviderService)
    {
        return ItineraryServiceParents::where('itinerary_id', $itineraryProviderService->itinerary_id)
            ->where('service_id', $itineraryProviderService->service_id)->first();
    }
    private static function newItem(ItineraryProviderService $itineraryProviderService) :bool
    {
        $item = new ItineraryServiceParents();
        $item->itinerary_id = $itineraryProviderService->itinerary_id;
        $item->service_id = $itineraryProviderService->service_id;
        $item->itinerary_provider_services = [$itineraryProviderService->id];
        $item->has_bills = $itineraryProviderService->has_bills ?? 0;
        return $item->save();
    }
    
    private static function editItem(ItineraryServiceParents $item, ItineraryProviderService $itineraryProviderService) :bool
    {
        $collection = $item->getCollectionItineraryProviderService();
        
        if (! $collection->contains($itineraryProviderService->id)) {
            $collection->push($itineraryProviderService->id);
            $item->itinerary_provider_services = $collection->toArray();
            return $item->save();
        }
        
        return true;
    }
    
    
    //helper
    private function getCollectionItineraryProviderService()
    {
        return collect($this->itinerary_provider_services);
    }
    
    private function removeItineraryProviderService($value)
    {
        $collection = $this->getCollectionItineraryProviderService();
        $newCollection = collect();
        foreach ($collection as $index => $item) {
            if ($item != $value) {
                $newCollection->push($item);
            }
        }
        return $newCollection;
    }
    
    
    public function getItineraryProviderServiceParent()
    {
        return ItineraryProviderService::whereIn('id', $this->itinerary_provider_services)
            ->with('children')->get();
    }
    
    public function getItineraryProviderServiceChild()
    {
        $itineraryProviderServiceParent = $this->getItineraryProviderServiceParent();
        $ret = collect();

        foreach ($itineraryProviderServiceParent as $parent) {
            $ret = $ret->merge($parent->children);

        }
        return $ret;
    }
    
    
    
    
    /***************
     * Accountable
     ****************/
    public function getFinalCostQuotation()
    {
        return null;
    }
    
    //Procurement
    public function getProcurementDescription()
    {
        return $this->service->name;
    }
    public function getProcurementQuantity()
    {
        return 1;
    }
    public function getProcurementCost()
    {
        return $this->transferCalculationToChildren('getProcurementCost');
    }
    public function getProcurementAdmFee()
    {
        return null;
    }
    public function getProcurementTax()
    {
        return null;
    }
    public function getProcurementUnitFinalCost()
    {
        return $this->getProcurementFinalCost();
        
    }
    public function getProcurementFinalCost()
    {
        return $this->transferCalculationToChildren('getProcurementFinalCost');
    }
    
    //Billing
    public function getBillingDescription()
    {
        return $this->service->name;
    }
    public function getBillingQuantity()
    {
        return 1;
    }
    public function getBillingPercentage()
    {
        return null;
    }
    public function getBillingUnitPrice()
    {
        return $this->getBillingFinalPrice();
    }
    public function getBillingFinalPrice()
    {
        return $this->transferCalculationToChildren('getBillingFinalPrice');
    }
    public function getQuotationFinalPrice()
    {
        return $this->transferCalculationToChildren('getQuotationFinalPrice');
    }
    
    public function getProfitPrice()
    {
        return $this->transferCalculationToChildren('getProfitPrice');
    }
    
    //Helper
    private function transferCalculationToChildren($functionName)
    {
        $total = 0;
        foreach ($this->getItineraryProviderServiceChild() as $item) {
            $total += $item->$functionName();
        }
        return $total;
    }
    
    public function isFromPike()
    {
        return $this->provider_id == Config::get('constants.pikeProviderId');
    }
    
    
    private function hasChildrenBilled()
    {
        return $this->getItineraryProviderServiceChild()->where('has_bills', 1)->count() > 0;
    }
    
    /*
     |--------------------------------------------------------------------------
     | MUTATORS
     |--------------------------------------------------------------------------
     */
    
    
    public function getBillingCheckedAttribute($val)
    {
        if ($this->has_bills) {
            return 0;
        }
        return $val;
    }
    
    public function getAdmFeeCheckedAttribute($val)
    {
        if ($this->has_bills) {
            return 0;
        }
        return $val;
    }
    
    
    public function setAdmFeeCheckedAttribute($val)
    {
        $this->attributes['adm_fee_checked'] = 0;
        
        if ($this->original['adm_fee_checked'] == 0 && $val == 1 && $this->has_bills == null) {
            if (!$this->hasChildrenBilled()) {
                $this->attributes['adm_fee_checked'] = 1;
                $this->attributes['billing_checked'] = 1;
            }
            
        }
    }
    
    
    public function setBillingCheckedAttribute($val)
    {
        if ($this->has_bills != null) {
            return;
        }
        if ($this->original['billing_checked'] == 1 && $val == 0) { //Un check
            $this->attributes['adm_fee_checked'] = 0;
            $this->attributes['billing_checked'] = 0;
        }
        // Checked or Checked Adm Fee
        if (($this->original['billing_checked'] == 0 && $val == 1) || $this->attributes['adm_fee_checked'] == 1) {
            if (!$this->hasChildrenBilled()) {
                $this->attributes['billing_checked'] = 1;
                $this->onParentCheckBillingUnCheckChild();
            }
        }
    }
    
    private function onParentCheckBillingUnCheckChild()
    {
        foreach ($this->getItineraryProviderServiceChild() as $child) {
            $child->billing_checked = 0;
            $child->save();
        }
    }
    
    
    
    public function addBill($billId)
    {
        $this->has_bills = $billId;
        
        foreach ($this->getItineraryProviderServiceParent() as $item) {
            $item->addBill($billId);
        }
        
        $this->save();
    }
    
    public function removeBill()
    {
        $this->has_bills = null;
        
        foreach ($this->getItineraryProviderServiceParent() as $item) {
            $item->removeBill();
        }
        
        $this->save();
    }
    
    
    public function addInvoice()
    {
        $this->procurement_status = 1;
        
        foreach ($this->getItineraryProviderServiceParent() as $item) {
            $item->addInvoice();
        }
        
        $this->save();
    }
    
    
}
