<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Aircraft extends Model
{
    use CrudTrait, SoftDeletes;

    /*
   |--------------------------------------------------------------------------
   | GLOBAL VARIABLES
   |--------------------------------------------------------------------------
   */

    protected $table = 'aircraft';

    protected $fillable = [
        'registration', 'client_id', 'operator_id', 'aircraft_type_id', 'equipment', 'pbn', 'nav',
        'color', 'dinghies_number', 'dinghies_capacity', 'dinghies_cover', 'dinghies_colour', 'documents'
    ];

    protected $casts = ['documents' => 'array'];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function permanentPermissions()
    {
        return DB::table('aircraft_permanent_permission')
            ->join('aircraft', 'aircraft_permanent_permission.aircraft_id', '=', 'aircraft.id')
            ->where('aircraft_permanent_permission.aircraft_id', '=', $this->id)
            ->join('permanent_permissions', 'aircraft_permanent_permission.permanent_permission_id', 'permanent_permissions.id')
            ->join('companies', 'permanent_permissions.provider_id', 'companies.id')
            ->select(['permanent_permissions.*', 'companies.name as provider_name'])
            ->get();
    }

    public function delete()
    {
        if ($this->operations != null) {
            return null;
        }
        return 'Aircraft deleted';
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function type()
    {
        return $this->belongsTo(AircraftType::class, 'aircraft_type_id', 'id');
    }

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id', 'id');
    }

    public function operator()
    {
        return $this->belongsTo(Operator::class, 'operator_id', 'id');
    }

    public function operations()
    {
        return $this->belongsTo(Operation::class, 'id', 'aircraft_id');
    }




    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */
    //Name used for revisions
    public function identifiableName()
    {
        return $this->registration;
    }

    //Type name used for quotations
    public function typeName()
    {
        return $this->type->name;
    }

    /*c
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    public function setDocumentsAttribute($value)
    {
        $attribute_name = "documents";
        $disk = "public";

        foreach ($value as $index => $file) {
            if ($file) {
                $file_name = $file->getClientOriginalName();
                $destination_path = 'aircraft_documents/'.md5($file->getClientOriginalName() . time());

                $this->uploadFilesToDisk($file, $attribute_name, $disk, $destination_path, $file_name, $index, false);
            }
        }

        //for only Deletion
        $this->uploadFilesToDisk('', $attribute_name, $disk, '', '', 0, true);

    }


    public function uploadFilesToDisk($value, $attribute_name, $disk, $destination_path, $file_name, $index, $to_delete)
    {
        $request = \Request::instance();
        $attribute_value = (array)$this->{$attribute_name};
        $files_to_clear = $request->get('clear_' . $attribute_name);


        // if a file has been marked for removal,
        // delete it from the disk and from the db
        if ($files_to_clear) {
            $attribute_value = (array)$this->{$attribute_name};

            foreach ($files_to_clear as $key => $filename) {
                \Storage::disk($disk)->delete($filename);
                $attribute_value = array_where($attribute_value, function ($value, $key) use ($filename) {
                    return $value != $filename;
                });
            }
        }
        if (!$to_delete) {
            // if a new file is uploaded, store it on disk and its filename in the database
            if ($request->hasFile($attribute_name)) {

                $file = ($request->file($attribute_name)[$index]);

                if ($file->isValid()) {
                    // 1. Generate a new file name
                    $new_file_name = $file_name;

                    // 2. Move the new file to the correct path
                    $file_path = $file->storeAs($destination_path, $new_file_name, $disk);

                    // 3. Add the public path to the database
                    $attribute_value[] = $file_path;
                }
            }
        }
        $this->attributes[$attribute_name] = json_encode($attribute_value);
    }


}
