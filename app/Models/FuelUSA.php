<?php

namespace App\Models;

use Backpack\CRUD\Exception\AccessDeniedException;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Maatwebsite\Excel\Facades\Excel;

class FuelUSA extends Model
{

    protected $table = 'fuel_USA';

    public function airport()
    {
        return $this->belongsTo(Airport::class, 'icao', 'icao');
    }

    protected static $classADiscount = 1;
    protected static $classBDiscount = 1;
    protected static $classCDiscount = 0.2;
    protected static $minPikeValue = 1.12;

    protected static $namePikeFBO = 'pikefbo';
    protected static $nameRetail ='retail';

    //Class A REATIL - 1
    //Class B REATIL - 1.2
    //Class C Prom Prov - 0.2
    //Min Pike = pike*1.1;
    //All has to be > Min Pike;


    //Load
    public static function loadExcel(UploadedFile $file)
    {
        //dd($file);

        Excel::load($file, function ($reader) {

            $headers = $reader->first()->keys()->toArray();
            unset($headers[0], $headers[1]); //ICAO - FBO
            $batNumber = self::generateBatNumber();
            foreach ($reader->all() as $row) {
                $items = $row->all();
                if ($items['icao'] != null) {
                    foreach ($headers as $key => $value) {

                        $fuelUSA = new FuelUSA();
                        $fuelUSA->icao = $items['icao'];
                        $fuelUSA->FBO = $items['fbo'];
                        $fuelUSA->provider = $value;
                        $fuelUSA->provider_value = $items[$value];
                        $fuelUSA->bat_number = $batNumber;
                        $fuelUSA->save();
                    }
                }
            }
        });
    }

    private static function generateBatNumber()
    {
        $last = FuelUSA::orderBy('updated_at', 'desc')->first();
        if ($last == null) {
            return 1;
        }
        return $last->bat_number + 1;
    }



    //Get

    public static function getLastFuelValues()
    {
        return self::calculateFuelValues( self::getLastList() );
    }

    public static function getFuelValuesForIcao($icao)
    {
        $items = self::getLastList();
        $fbos = self::getFBOFromIcao($items, $icao);
        $ret = [];

        foreach ($fbos as $fbo) {
            $ret[] = self::generateValuesForIcaoFBO($items, $icao, $fbo);
        }

        return $ret;
    }


    private static function getLastList()
    {
        $batNumber = self::generateBatNumber() - 1;
        if ($batNumber == 0) {
            throw new AccessDeniedException('You need to load the USA airports fuel list');
        }

        return self::where('bat_number', $batNumber)->get();
    }

    private static function calculateFuelValues(Collection $items)
    {
        $icaos = self::getIcaosFromItems($items);

        $ret = [];
        foreach ($icaos as $icao) {
            $fbos = self::getFBOFromIcao($items, $icao);
            foreach ($fbos as $fbo) {
                $ret[] = self::generateValuesForIcaoFBO($items, $icao, $fbo);
            }
        }
        return $ret;
    }

    private static function generateValuesForIcaoFBO($items, $icao, $fbo)
    {
        $minPike = self::minPikeFormula($items, $icao, $fbo);

        return [
            'icao'      => $icao,
            'fbo'       => $fbo,
            'client_a'  => round(self::clientAFormula($items, $icao, $fbo, $minPike), 2) ,
            'client_b'  => round(self::clientBFormula($items, $icao, $fbo, $minPike), 2),
            'client_c'  => round(self::clientCFormula($items, $icao, $fbo, $minPike), 2),
            'min_pike'  => round($minPike, 2),
            'retail'    => round(self::retailFormula($items, $icao, $fbo), 2),
        ];
    }



    private static function getIcaosFromItems($items)
    {
        return array_unique($items->pluck('icao')->sortBy('icao')->toArray());
    }
    private static function getFBOFromIcao($items, $icao)
    {
        return array_unique($items->where('icao', $icao)->pluck('FBO')->toArray());
    }

    //Formula
    private static function minPikeFormula(Collection $items, $icao, $fbo)
    {

        $pikeMin = $items->where('icao', $icao)->where('FBO', $fbo)
            ->where('provider_value', '<>', null)
            ->where('provider', '<>', self::$nameRetail)->min('provider_value');

        return $pikeMin * self::$minPikeValue;
    }

    private static function clientAFormula(Collection $items, $icao, $fbo, $minPike)
    {

        $item = $items->where('icao', $icao)->where('FBO', $fbo)
                    ->where('provider', self::$nameRetail)->first();

        $value = $item->provider_value - self::$classADiscount;

        return $value > $minPike ? $value : $minPike;

    }

    private static function clientBFormula(Collection $items, $icao, $fbo, $minPike)
    {
        $item = $items->where('icao', $icao)->where('FBO', $fbo)
            ->where('provider', self::$nameRetail)->first();

        $value = $item->provider_value - self::$classBDiscount;
        return $value > $minPike ? $value : $minPike;
    }

    private static function clientCFormula(Collection $items, $icao, $fbo, $minPike)
    {
       $cValue = $items->where('icao', $icao)->where('FBO', $fbo)
                ->where('provider_value', '<>', null)
                ->where('provider', '<>', self::$namePikeFBO)
                ->where('provider', '<>', self::$nameRetail)->avg('provider_value');
       
       $value =  $cValue - self::$classCDiscount;
       return $value > $minPike ? $value : $minPike;
    }

    private static function retailFormula(Collection $items, $icao, $fbo)
    {
        return $items->where('icao', $icao)->where('FBO', $fbo)
            ->where('provider', self::$nameRetail)->first()->provider_value;
    }



}
