<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Backpack\Base\app\Notifications\ResetPasswordNotification as ResetPasswordNotification;
use Backpack\CRUD\CrudTrait;
use OwenIt\Auditing\Contracts\Auditable;
use Spatie\Permission\Traits\HasRoles;
use App\Models\Utilities\Country;

class User extends Authenticatable implements Auditable
{
    use Notifiable, CrudTrait, HasRoles;
    use \OwenIt\Auditing\Auditable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'countries'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'countries' => 'array'
    ];

    //Name used for revisions
    public function identifiableName()
    {
        return $this->name;
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }
    
    public function reminders()
    {
        return $this->hasMany(ActivityReminder::class, 'assign_to_user_id', 'id');
    }
    
    public function getThisWeekReminders()
    {
        //dd($this->reminders()->thisWeek()->toSql());
        return $this->reminders()->thisWeek()->get();
    }
    
    
    public function availableCompanies()
    {
        return $this->belongsToMany(Company::class, 'available_companies_users');
    }


    public function getCountries()
    {
        $aux = [];
        foreach ($this->countries ?? [] as $country) {
            array_push($aux, Country::getName($country));
        }
        return implode($aux, ', ');
    }
}
