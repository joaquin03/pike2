<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\Scopes\ServiceChildPikeScope;
use Illuminate\Database\Eloquent\SoftDeletes;
use Config;


class ServiceChildPike extends ServiceChild
{
    use CrudTrait, SoftDeletes;


    protected $fillable = [
        'name', 'description', 'is_for_procurement', 'service_id', 'is_from_pike'
    ];

    public static function boot()
    {
        static::bootTraits();
        static::addGlobalScope(new ServiceChildPikeScope());
        static::created(function($model) {
            $provider = Provider::findOrFail(Config::get('constants.pikeProviderId'));
            $model->addServiceToProvider($provider);
        });


    }





}
