<?php

namespace App\Models;

use App\Models\Scopes\OperatorScope;
use Illuminate\Database\Eloquent\Model;

class Operator extends Company
{
    public static function boot()
    {
        parent::boot();
        
        static::addGlobalScope(new OperatorScope());
    }
    
    public function tags()
    {
        return $this->belongsToMany(CompanyTag::class, 'company_company_tags', 'company_id');
    }


}
