<?php

namespace App\Models;

use App\Models\Scopes\ServiceChildScope;
use App\Models\Scopes\ServiceOperationScope;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceQuotation extends Service
{
    use CrudTrait, SoftDeletes;
    
    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(new ServiceChildScope());
    }
    /*
   |--------------------------------------------------------------------------
   | GLOBAL VARIABLES
   |--------------------------------------------------------------------------
   */
    protected $fillable = [
        'name', 'description', 'category_id'
    ];
    
    
}
