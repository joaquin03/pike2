<?php

namespace App\Models;

use App\Models\Contracts\ActivityEvent;
use Backpack\CRUD\CrudTrait;
use Illuminate\Support\Facades\Storage;

class ActivityFile extends ActivityEvent
{
    use CrudTrait;

     /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'files';
    
    protected $fillable = [
        'name',
        'notes',
        'user_id',
        'files'
    ];
    
    protected $casts = [
        'files' => 'array'
    ];
    

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function getIcon()
    {
        return 'fa fa-link';
    }
    
    public function getTitle()
    {
        return "Files: ".$this->name;
    }
    public function getDescription()
    {

        $fileText ='';
        foreach($this->files ?? [] as $index => $file) {
            $fileText .= "<a target='_blank' href='/storage/".$file."'>File ".$index."</a>";

        }
        if ($fileText!=''){
            return "Files:".$fileText;
        }
        return '';
    }
    
    public function getColor()
    {
        return 'bg-green';
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    
    
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    
    public function setFilesAttribute($value)
    {
        $attribute_name = "files";
        $disk = "public";
        $destination_path = "files";
        
        $this->uploadMultipleFilesToDisk($value, $attribute_name, $disk, $destination_path);
    }
}
