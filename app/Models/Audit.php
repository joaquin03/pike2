<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;

class Audit extends \OwenIt\Auditing\Models\Audit
{
    use CrudTrait;
    
    
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
    
    public function getElementString()
    {
        
        $type = explode("\\", $this->auditable_type);
        $end = end($type);
        return str_replace('Activity', '', $end);
    }
    
    public function scopeInCrm($query)
    {
        return $query->whereIn('auditable_type',
            [Company::class, ActivityEmail::class, ActivityCall::class, ActivityFile::class, ActivityMeeting::class,
            ActivityNote::class, ActivitySpecialFeatures::class, ActivityReminder::class]);
    }
    
    public function transformNewValues()
    {
        return $this->transformArrayToString($this->new_values);
    }
    
    public function transformOldValues()
    {
        return $this->transformArrayToString($this->old_values);
    }
    
    private function transformArrayToString($array)
    {
        $text = '';
        if (is_array($array)) {
            foreach ($array as $key => $val) {
                $text .=  str_replace('_', ' ',ucfirst($key))." :";
                $text .= $this->transformArrayToString($val).', ';
            }
        } else {
            $text = $array;
        }
       
        return $text;
    }
    
    
    public function getElementValue()
    {
        if (method_exists($this->auditable, 'getAuditableDescription')) {
           return $this->auditable->getAuditableDescription();
        }
        return $this->transformNewValues();
    }
    
}