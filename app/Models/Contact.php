<?php

namespace App\Models;

use App\Models\Contracts\ActivityEvent;
use App\Models\Scopes\ValidateContactUserRoleScope;
use App\Models\Utilities\Country;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Contact extends Model
{
    use CrudTrait;
    
    /*
   |--------------------------------------------------------------------------
   | GLOBAL VARIABLES
   |--------------------------------------------------------------------------
   */
    
    protected $table = 'contacts';
    
    protected $fillable = [
        'id', 'name', 'surname', 'company_id', 'role', 'description', 'emails', 'phones', 'address', 'creator_user_id'
    ];
    
    protected $casts = [
        'emails' => 'array',
        'phones' => 'array',
        'address' => 'array',
    ];
    
    public static function boot()
    {
        parent::boot();
        
        static::addGlobalScope(new ValidateContactUserRoleScope());
        
    }
    
    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function getFullName()
    {
        return $this->name . ' ' . $this->surname;
    }
    public function getEmails()
    {
        $ret = '';
        if($this->emails) {
            foreach ($this->emails as $email) {
                if (array_key_exists("email", $email)) {
                    return $email["email"];
                }
            }
        }
        return $ret;
    }

    public function getMainEmail()
    {
        if ($this->emails) {
            return $this->emails[0]['email'] ?? '';
        }
        return '';
    }
    public function getMainPhone()
    {
        if ($this->phones) {
            return $this->phones[0]['phone'] ?? '';
        }
        return '';
    }
    
    
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function company()
    {
        return $this->belongsTo(Company::class);
    }
    
    public function activitiesContacts()
    {
        return $this->belongsToMany(Activity::class, 'activities_contacts');
    }


    
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    
    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */
    
    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
