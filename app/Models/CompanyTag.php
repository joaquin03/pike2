<?php

namespace App\Models;

use App\Models\Contracts\ActivityEvent;
use App\Models\Utilities\Country;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class CompanyTag extends Model
{
    use CrudTrait;

     /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    
    protected $table = 'company_tags';
    
    protected $fillable = [
        'name', 'description',
    ];
    
    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function company()
    {
        return $this->belongsToMany(Company::class, 'company_company_tags');
    }
    
    public function note()
    {
        return $this->belongsToMany(ActivityNote::class, 'note_note_tags');
    }
    
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
