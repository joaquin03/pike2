<?php

namespace App\Models;

use App\Models\Scopes\ClientScope;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;


class Client extends Company
{
    use SoftDeletes;
    
    public static function boot()
    {
        parent::boot();
        
        static::addGlobalScope(new ClientScope());
    }
    
    public function tags()
    {
        return $this->belongsToMany(CompanyTag::class, 'company_company_tags', 'company_id');
    }


     /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    *

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */


    public function setBalanceToDate($date)
    {
        $date = Carbon::createFromFormat('d-m-Y', $date ?? Carbon::now()->format('d-m-Y'));


        $this->balance = round($this->getPositiveAmountToDate($date) - $this->getNegativeAmountToDate($date),2);
    }

    public function showBalance()
    {
        return $this->balance;
    }

    public function getPositiveAmountToDate($date)
    {

        return $this->clientBills()->where('date','<=',$date)->sum('amount')
            + $this->clientDebitNotes()->where('date','<=',$date)->sum('amount');
    }

    public function getNegativeAmountToDate($date)
    {
        return $this->clientPayments()->where('date','<=',$date)->sum('amount') +
                     $this->clientCreditNotes()->where('date','<=',$date)->sum('amount');

    }



    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
