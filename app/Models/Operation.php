<?php

namespace App\Models;

use App\Models\Accounting\Additional;
use App\Models\Accounting\AdministrationFee;
use App\Models\Scopes\OperationScope;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Scopes\OVFScope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use DB;
use DateTime;
use MongoDB\BSON\Timestamp;
use OwenIt\Auditing\Contracts\Auditable;
use Venturecraft\Revisionable\RevisionableTrait;
use App\Models\Traits\CustomRevisionableTrait;

use DateTimeZone;

class Operation extends Model implements Auditable
{
    use SoftDeletes, CrudTrait, RevisionableTrait, CustomRevisionableTrait;
    use \OwenIt\Auditing\Auditable;

    protected $dates = ['created_at'];

    protected $table = 'operations';

    protected $fillable = [
        'code', 'in_charge_user_id', 'state', 'route', 'notes', 'aircraft_id', 'aircraft_operator_id',
        'aircraft_client_id', 'crew_captain_id', 'crew_first_official_id', 'crew_auxiliary_id',
        'is_active', 'second_crew_captain_id', 'second_crew_first_official_id', 'second_crew_auxiliary_id',
        'start_date', 'procurement_status', 'billing_status', 'ref_number', 'procurement_permits_status',
        'billing_permits_status', 'completed_at', 'route', 'mtow', 'quotation_id', 'quotation_services_type',
        'aircraft_type_id', 'operation_id', 'quotation_via', 'quotation_contact_id',
        'quotation_exportable_route_map', 'documents', 'operation_custom_start_date', 'quotation_notes',
        'operation_notes', 'procurement_notes'
    ];

    public static $states = ['In Process', 'Completed', 'Canceled', 'Deleted'];

    protected $casts = ['documents' => 'array'];


    protected $revisionFormattedFieldNames = array(
        'code' => 'Code',
        'in_charge_user_id' => 'User in charge',
        'state' => 'Operation Status',
        'route' => 'Route',
        'notes' => 'Notes',
        'aircraft_id' => 'Aircraft',
        'aircraft_operator_id' => 'Operator',
        'aircraft_client_id' => 'Client',
        'crew_captain_id' => 'Captain',
        'crew_first_official_id' => 'First Official',
        'crew_auxiliary_id' => 'Auxiliary',
        'second_crew_captain_id' => 'Second Captain',
        'second_crew_first_official_id' => 'Second Official',
        'second_crew_auxiliary_id' => 'Second Auxiliary',
        'start_date' => 'Start Date',
        'ref_number' => 'Ref Number',
        'creator_user' => 'Creator'
    );

    protected $dontKeepRevisionOf = array(
        'is_active', 'procurement_status', 'billing_status', 'procurement_permits_status', 'billing_permits_status', 'updated_at',
        'created_at'
    );


    public static function boot()
    {

        parent::boot();

        static::addGlobalScope(new OperationScope());
        static::creating(function (Operation $model) {
            $model->preSave();
            $model->billing_status = 'Pending';
            $model->setCode();
            $model->setStartDate(false);
            $model->setOperationCustomStartDate(false);
        }, 0);
    }


    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public static function sortByStateAndDate()
    {

        $closestDates = Operation::where('start_date', '>=', Carbon::now())
            ->orderByRaw('FIELD(state,"IN PROCESS","COMPLETED", "CANCELED", "DELETED")')
            ->orderByRaw('DATEDIFF(NOW(), start_date) DESC');
        $pastDates = Operation::where('start_date', '<', Carbon::now())
            ->orderByRaw('FIELD(state,"IN PROCESS","COMPLETED", "CANCELED", "DELETED")')
            ->orderByRaw('DATEDIFF(NOW(), start_date) ');
        $withoutDate = Operation::where('start_date', null); //If all the itineraries dates are TBA

        $actual_status_filter = app('request')->input('status');
        if ($actual_status_filter != null) {
            $closestDates = $closestDates->where('state', '=', $actual_status_filter);
            $pastDates = $pastDates->where('state', '=', $actual_status_filter);
            $withoutDate = $withoutDate->where('state', '=', $actual_status_filter);
        }
        $pastDates = $pastDates->get()->merge($withoutDate->get());

        $operations = $closestDates->get()->merge($pastDates);

        return $operations;
    }


    public function getStatusAttribute()
    {
        return $this->state;
    }

    // TODO: improve this
    public function logicalDeletion()
    {
        $url = route('crud.operation.delete', ['id' => $this->id]);

        if ($this->state != "Deleted") {
            return '<a href="' . $url . '" class="btn btn-xs btn-default"
                    onclick="return confirm(\'¿Está seguro que desea eliminar la operación?\');">
                    <span  class=" glyphicon glyphicon glyphicon-remove"></span></a>';
        }
    }


    public function addPermanentPermissionList($permanentPermissions)
    {
        $aircraftPermanentPermissions = DB::table('aircraft_permanent_permission')
            ->whereIn('permanent_permission_id', $permanentPermissions)
            ->where('aircraft_id', '=', $this->aircraft->id)
            ->get()
            ->pluck('permanent_permission_id', 'id');

        $items = null;
        foreach ($aircraftPermanentPermissions as $aircraftPmId => $permanentPermissionId) {

            $items[$permanentPermissionId] = [
                'aircraft_pm_id' => $aircraftPmId,
            ];
        }
        $this->permanentPermissions()->sync($items, true);
    }

    public function operationAirports()
    {
        $airports = $this->itineraryAirports();

        $visible_icaos = '';
        $all_icaos = '';
        $cont = 0;
        $overLength = false;
        $airportsLength = count($airports);

        foreach ($airports as $airport) {
            if ($cont + 1 != $airportsLength) {
                if ($cont < 3 && ($cont < $airportsLength)) {
                    $visible_icaos = $visible_icaos . " " . $airport->icao . "-";
                }
                if ($cont == 3 && $airportsLength > 3) {
                    $visible_icaos = $visible_icaos . " " . $airport->icao . "...";
                    $overLength = true;
                }
                $all_icaos = $all_icaos . " " . $airport->icao . "-";
            }
            if ($airportsLength == $cont + 1) {
                if (!$overLength) {
                    $visible_icaos = $visible_icaos . " " . $airport->icao;
                }
                $all_icaos = $all_icaos . " " . $airport->icao;
            }
            $cont++;
        }

        return '<span title="' . $all_icaos . '">' . $visible_icaos . '</span>';
    }

    public function operationAirportsList()
    {


        $airportsIcaos = Collection::make($this->itineraryAirports())->pluck('icao');

        if ($airportsIcaos->count() > 3) {
            return $airportsIcaos->implode(', ') . '…';
        }
        return $airportsIcaos->implode(', ');
    }

    public function updateOperationPermission($permissionUpdated)
    {
        $permission = AircraftPermission::findOrFail($permissionUpdated['id']);
        if ($permissionUpdated['expiration_date']) {
            $permission->attributes['expiration_date'] = Carbon::createFromFormat('d/m/Y', $permissionUpdated['expiration_date']);
            $permissionUpdated['expiration_date'] = Carbon::createFromFormat('d/m/Y', $permissionUpdated['expiration_date']);
        }

        if ($permissionUpdated['delete-file'] == "false") {
            unset($permissionUpdated['file']);
            $permission->fill($permissionUpdated);

        } else {
            $permission->fill($permissionUpdated);
            $attribute_name = "file";
            $disk = "public";
            $destination_path = "aircraft-permission";

            $permission->uploadFileToDisk('file', $attribute_name, $disk, $destination_path);

        }


        $permission->save();

    }


    public function itineraryAirports()
    {
        $airports = [];
        foreach ($this->itineraries as $itinerary) {
            $newAirport = $itinerary->airport;
            if (!in_array($newAirport, $airports) & $newAirport != null) {
                $airports[] = $newAirport;
            }
        }
        return $airports;
    }

    public static function itineraryIcaos($itineraries, $id)
    {
        $airports = [];
        foreach ($itineraries as $itinerary) {
            if ($itinerary->operation_id == $id) {
                if (!in_array($itinerary->airport, $airports)) {
                    array_push($airports, $itinerary->airport);
                }
            }
        }
        return $airports;
    }

    public function activeItineraries()
    {
        return $this->itineraries()->where('is_canceled', 0);
    }

    public function getOperatorName()
    {
        if ($this->aircraftOperator) {
            return $this->aircraftOperator->name;
        }
        return '';
    }

    public function getAircraftClientName()
    {
        if ($this->aircraftClient) {
            return $this->aircraftClient->name;
        }
        return '';

    }

    public function getFirstItieraryDate()
    {
        return $this->itineraries()->isActive()->where('type', 'ETD')
            ->where('operation_id', $this->id)->orderBy('date')->first();
    }

    public function setOldestStartDate()
    {
        $firstETD = $this->itineraries()->isActive()->where('type', 'ETD')->whereNotNull('date')
            ->where('operation_id', $this->id)->where('is_canceled', false)->orderBy('date')->first();

        if ($firstETD != null && $this->start_date != $firstETD->date) {
            $this->start_date = $firstETD->date;
            $this->save();
        }
    }

    public function resetStartDate()
    {
        $this->setOldestStartDate();
    }

    public function getDefaultAircraftServicePrice()
    {
        try {
            return AircraftDefaultServicePrice::getValue($this->aircraft->MTOW);
        } catch (\Exception $e) {
            return null;
        }
    }

    public static function reminderCompletedOperations($days)
    {
        $expireTime = Carbon::now()->subDays($days);

        $operations = Operation::where('state', 'Completed')
            ->where('procurement_status', '!=', 'Completed')->get();

        foreach ($operations as $operation) {

            $diffDays = Carbon::now()->diffInDays($operation->completed_at);

            if ($diffDays === $days)
                self::remindUserToCompleteProcurement($operation, $diffDays, 3);

            if ($diffDays === $days * 2)
                self::remindUserToCompleteProcurement($operation, $diffDays, -1);


        }
    }

    public static function remindUserToCompleteProcurement($operation, $days, $dueDateModify)
    {

        $reminder = new ActivityReminder();
        $reminder->title = 'Operation ' . $operation->code . ' was completed ' . $days . ' days ago.';
        $reminder->description = 'Please complete the procurement for this operation';
        $reminder->assign_to_user_id = env('PROCUREMENT_IN_CHARGE', 32);
        $reminder->due_date = Carbon::now()->addDays($dueDateModify);
        $reminder->operation_link = '/operation/' . $operation->id . '/edit';
        $reminder->save();

        self::assignReminderToActivity($reminder);
    }

    public static function assignReminderToActivity($reminder)
    {
        $activity = new Activity([
            'visibility' => 'public',
            'user_id' => env('PROCUREMENT_IN_CHARGE', 32),
            'company_id' => env('CENTRAL_PIKE', 51),
            'date' => Carbon::now('UTC')
        ]);
        $activity->event()->associate($reminder);
        $activity->save();
    }

    public static function assignReminderToActivityWithUser($reminder, $salesManager)
    {
        $activity = new Activity([
            'visibility' => 'public',
            'user_id' => $salesManager->id,
            'company_id' => env('CENTRAL_PIKE', 51),
            'date' => Carbon::now('UTC')
        ]);
        $activity->event()->associate($reminder);
        $activity->save();
    }

    public function associateQuotation($quotation_id)
    {
        $this->quotation_id = $quotation_id;
        $this->save();
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'in_charge_user_id');
    }

    public function inChargeUser()
    {
        return $this->user();
    }

    public function creatorUser()
    {
        return $this->user();
    }

    public function creator()
    {
        return $this->hasOne(User::class, 'id', 'creator_user_id');
    }

    public function operator()
    {
        return $this->belongsTo(Client::class, 'operator_id', 'id');
    }

    public function aircraft()
    {
        return $this->hasOne(Aircraft::class, 'id', 'aircraft_id');
    }

    public function aircraftType()
    {
        return $this->hasOne(AircraftType::class, 'id', 'aircraft_type_id');
    }


    public function aircraftOperator()
    {
        return $this->hasOne(Company::class, 'id', 'aircraft_operator_id');
    }

    public function billingCompany()
    {
        if ($this->billing_company_id == null) {
            return $this->aircraftClient();
        }
        return $this->hasOne(Company::class, 'id', 'billing_company_id');
    }

    public function aircraftClient()
    {
        return $this->hasOne(Company::class, 'id', 'aircraft_client_id');
    }

    public function aircraftCaptain()
    {
        return $this->hasOne(CrewMember::class, 'id', 'aircraft_captain_id');
    }

    public function itineraries()
    {
        return $this->hasMany(Itinerary::class, 'operation_id', 'id');
    }

    public function arrivalItineraries()
    {
        return $this->hasMany(Itinerary::class, 'operation_id', 'id')->whereIn('type', ['ETA', 'ATA']);
    }

    public function aircraftPermission()
    {
        return $this->hasMany(AircraftPermission::class, 'operation_id', 'id');
    }

    public function aircraftPermanentPermission()
    {
        return $this->hasMany(PermanentPermission::class);
    }

    public function crewCaptain()
    {
        return $this->hasOne(CrewMember::class, 'id', 'crew_captain_id');
    }

    public function secondCrewCaptain()
    {
        return $this->hasOne(CrewMember::class, 'id', 'second_crew_captain_id');
    }

    public function crewFirstOfficial()
    {
        return $this->hasOne(CrewMember::class, 'id', 'crew_first_official_id');
    }

    public function secondCrewFirstOfficial()
    {
        return $this->hasOne(CrewMember::class, 'id', 'second_crew_first_official_id');
    }

    public function crewAuxiliary()
    {
        return $this->hasOne(CrewMember::class, 'id', 'crew_auxiliary_id');
    }

    public function secondCrewAuxiliary()
    {
        return $this->hasOne(CrewMember::class, 'id', 'second_crew_auxiliary_id');
    }

    public function permanentPermissions()
    {
        return $this->belongsToMany(PermanentPermission::class, 'operation_permanent_permission',
            'operation_id',
            'permanent_permission_id'

        )->withoutGlobalScope(OVFScope::class)->withPivot('aircraft_pm_id', 'created_at', 'updated_at');
    }

    public function permitsAdministrationFees()
    {
        return $this->hasMany(AdministrationFee::class, 'operation_id', 'id')
            ->where('servicesable_type', get_class(new AircraftPermission()));
    }

    public function servicesAdditionals()
    {
        return $this->hasMany(Additional::class, 'operation_id', 'id')
            ->where('servicesable_type', get_class(new ItineraryProviderService()));
    }

    public function permitsAdditionals()
    {
        return $this->hasMany(Additional::class, 'operation_id', 'id')
            ->where('servicesable_type', get_class(new AircraftPermission()));
    }

    public function permanentPermitsAdministrationFees()
    {
        return $this->hasMany(AdministrationFee::class, 'operation_id', 'id')
            ->where('servicesable_type', get_class(new PermanentPermission()));
    }

    public function providerOfPermits()
    {
        return Provider::whereIn('id', $this->providerServices()->pluck('services_providers.provider_id'));
    }

    public function quotation()
    {
        return $this->hasOne('App\Models\Quotation');
    }

    public function getTotal()
    {
        return $this->aircraftPermission->sum('cost');
    }


    /** Itineraries  */
    public function createItinerary($itineraryData)
    {
        $itinerary = new Itinerary();
        $itinerary->fill($itineraryData);
        $itinerary->operation_id = $this->id;


        if ($itinerary->save()) {
            if ($itinerary->canHaveHandler()) {
                $itinerary->addDefaultHandlerOnCreate();
            }
            \Alert::success('Itinerary created.')->flash();
            $this->setOldestStartDate();
            return $itinerary;
        }
        return false;
    }

    public function itineraryByAirport()
    {
        return $this->itineraries();
    }


    public function updateItineraries(array $itineraries)
    {
        $itineraries = collect($itineraries);

        $this->deleteItineraries($itineraries);

        $this->createAndUpdateItineraries($itineraries);
    }

    private function createAndUpdateItineraries(Collection $itineraries)
    {
        $itineraries->each(function ($itinerary) {
            if (isset($itinerary['id'])) {
                return Itinerary::updateItem($itinerary['id'], $itinerary);
            }

            return Itinerary::createItem($itinerary, $this);
        });
    }

    private function deleteItineraries(Collection $itineraries)
    {
        $this->itineraries()->whereNotIn('id', $itineraries->pluck('id')->filter()->all())->delete();
    }


    public function transformer()
    {
        $data = $this->toArray();
        $data['itineraries'] = [];
        foreach ($this->itineraries as $itinerary) {
            $data['itineraries'][] = $itinerary->transformer();
        }

        return $data;
    }

    public function addPermission($request)
    {
        $expirationDate = Carbon::parse($request->get('expiration_date'))->format('d/m/Y');
        $requestData = $request->all();
        $requestData['expiration_date'] = $expirationDate;
        return AircraftPermission::createItem($requestData, $this);
    }


    /* Itinerary - Services */
    public function itinerariesProviderServices()
    {
        $itinerariesIds = $this->itineraries()->pluck('id');

        return ItineraryProviderService::whereIn('itinerary_id', $itinerariesIds);
    }


    // Ips for quotations export
    public function itinerariesProviderServicesChildsNotByPike()
    {
        $itinerariesIds = $this->itineraries()->pluck('id');

        return ItineraryProviderService::whereIn('itinerary_id', $itinerariesIds)
            ->where('parent_id', '!=', null)
            ->where('is_additional', null)
            ->where('provider_id', '!=', env('CENTRAL_PIKE', 51));
    }

    public function itinerariesProviderServicesChildsByPike()
    {
        $itinerariesIds = $this->itineraries()->pluck('id');

        return ItineraryProviderService::whereIn('itinerary_id', $itinerariesIds)
            ->where('parent_id', '!=', null)
            ->where('is_additional', null)
            ->where('provider_id', env('CENTRAL_PIKE', 51));
    }

    public function itinerariesProviderServicesAdditionalsQuotation()
    {
        $itinerariesIds = $this->itineraries()->pluck('id');

        return ItineraryProviderService::whereIn('itinerary_id', $itinerariesIds)
            ->where('is_additional', 1)
            ->where('parent_id', '!=', null);
    }
    // End Ips for quotations export

    // Aircraft permissions for quotation export


    public function permitsChildsNotByPike()
    {
        return $this->aircraftPermission()
            ->where('is_additional_quotation', null)
            ->where('provider_id', '!=', env('CENTRAL_PIKE', 51));
    }

    public function permitsChildsByPike()
    {
        return $this->aircraftPermission()
            ->where('is_additional_quotation', null)
            ->where('provider_id', env('CENTRAL_PIKE', 51));
    }

    public function permitsAdditionalsQuotation()
    {

        return $this->aircraftPermission()
            ->where('is_additional_quotation', 1);
    }

    // End aircraft permissions for quotation export


    public function itinerariesProviderServicesFinalCosts($services)
    {
        $finalCost = 0;
        foreach ($services as $service) {
            $finalCost += $service->getProcurementFinalCost();
        }
        return $finalCost;
    }

//    todo: permits no aditionals y aditionals para quotation


    public function itinerariesProviderServicesFinalCostsQuotation($servicesArray)
    {
        $finalCost = 0;
        foreach ($servicesArray as $services) {
            foreach ($services as $key => $service) {
                if ($service instanceof ItineraryProviderService || $service instanceof AircraftPermission) {
                    $finalCost += $service->getFinalCostQuotation();
                }
            }
        }
        return $finalCost;
    }

    public function getHtmlOperationStatus()
    {
        $class = '';
        if ($this->status == "Completed") {
            $class = "p1 white bg-gray";
        }
        if ($this->status == "In Process") {
            $class = "bg-green";
        }
        if ($this->status == "Canceled") {
            $class = "bg-red";
        }
        if ($this->status == "Deleted") {
            $class = 'bg-black';
        }
        return "<small class='label " . $class . "' style='font-size:10px; color:white'>" . strtoupper($this->status) . "</small>";
    }


    public function getHtmlStatus($status)
    {
        $class = "";
        if ($status == "Completed") {
            $class = "p1 white bg-green";
        }
        if ($status == "In Process" || $status == "Pending") {
            $class = "bg-gray";
        }
        if ($status == "Canceled") {
            $class = "bg-red";
        }
        if ($status == "Deleted") {
            $class = 'bg-black';
        }
        return "<small class='label " . $class . "' style='font-size:10px; color:white'>" . strtoupper($status) . "</small>";
    }

    public function getHtmlProcurementStatus($status)
    {
        $class = "";
        if ($status == "Completed") {
            $class = "p1 white bg-green";
        }
        if ($status == "In Process") {
            $class = "bg-gray";
        }
        if ($status == "Canceled") {
            $class = "bg-red";
        }

        return "<small class='label " . $class . "' style='font-size:10px; color:white'>" . strtoupper($status) . "</small>";
    }

    public function getHtmlQuotationStatus($status)
    {
        $color = "";
        if ($status == "Pending Data Sheet") {
            $color = "blue";
        }
        if ($status == "In Process") {
            $color = "orange";
        }
        if ($status == "Pending Answer") {
            $color = "black";
        }
        if ($status == "Canceled") {
            $color = 'red';
        }
        if ($status == "Lost") {
            $color = '#FF3336';
        }
        if ($status == "Budget") {
            $color = 'fuchsia';
        }
        if ($status == "Confirmed") {
            $color = 'green';
        }

        return "<small class='label' style='font-size:10px; color:white; background-color: $color'>" . strtoupper($status) . "</small>";
    }


    public function getProcurementPermitsStatus()
    {
        if ($this->procurement_permits_status == null) {
            return null;
        }
        return $this->procurement_permits_status;
    }

    public function getBillingPermitsStatus()
    {
        if ($this->billing_permits_status == null) {
            return null;
        }
        return $this->billing_permits_status;
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    public function scopeInProcess(Builder $query)
    {
        return $query->where('state', '<>', 'Deleted')
            ->where('state', 'In Process')
            ->orWhere('procurement_status', 'In Process')
            ->orWhere('billing_status', 'In Process');
    }

    public function scopeCompleted(Builder $query)
    {
        return $query->where('billing_status', 'Completed');
    }

    public function scopeCanceled(Builder $query)
    {
        return $query->where('state', 'Canceled');
    }


    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */


    public function getCreatedAt()
    {
        if ($this->start_date) {
            return Carbon::parse($this->start_date, 'UTC')->format('d/m/Y H:i');
        }
        return $this->created_at->format('d/m/Y H:i');
    }

    public function getStartDateAttribute()
    {
        if (array_key_exists('start_date', $this->attributes)) {
            return $this->attributes['start_date'];
        }
        return $this->attributes['created_at'] ?? null;
    }

    public function getOperationCustomStartDateAttribute()
    {

        if (array_key_exists('operation_custom_start_date', $this->attributes)) {
            return $this->attributes['operation_custom_start_date'];
        }
        return $this->attributes['created_at'] ?? null;
    }

    public function getStartDate()
    {

        if ($this->start_date) {
            $date = Carbon::parse($this->start_date, 'UTC');
            $year = $date->format('y');
            $month = $date->format('m');
            return $month . '-' . $year;
        }
        return null;
    }

    public function getCustomStartDate()
    {

        if ($this->operation_custom_start_date) {
            $date = Carbon::parse($this->operation_custom_start_date, 'UTC');
            $year = $date->format('y');
            $month = $date->format('m');
            return $month . '-' . $year;
        }
        return null;
    }


    public function getStartDateForList()
    {
        if ($this->start_date) {
            $date = Carbon::parse($this->start_date, 'UTC');
            return $date->format('d-m-y H:i');
        }
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

    public function setStateAttribute($value)
    {
        $this->attributes['state'] = $value;

        if ($value === 'Completed' && array_key_exists('completed_at', $this->attributes) && $this->attributes['completed_at'] === null)
            $this->attributes['completed_at'] = Carbon::now()->format('Y-m-d');

        if ($value != 'Completed')
            $this->attributes['completed_at'] = null;

    }

    public function setStartDate($save = true)
    {
        if (!$this->isSetDateTime($this->start_date)) {
            $date = '01-' . $this->start_date;
            $newDate = date("d-m-y H:i:s", strtotime($date));
            $d = new DateTime($newDate);
            $this->start_date = $d;
            if ($save) {
                $this->save;
            }
        }
    }

    public function setOperationCustomStartDate($save = true)
    {
        if ($this->operation_custom_start_date && !$this->isSetDateTime($this->operation_custom_start_date)) {
            $date = '01-' . $this->operation_custom_start_date;
            $newDate = date("d-m-y H:i:s", strtotime($date));
            $d = new DateTime($newDate);
            $this->operation_custom_start_date = $d;
            if ($save) {
                $this->save;
            }
        }
    }


    public function setCode()
    {
        $date = explode('-', $this->start_date);
        $month = $date[0];
        $year = $date[1];
        $id = '001';
        $latest_pn = static::where('code', 'like', 'PN' . $year . $month . '%')
            ->latest('id')
            ->pluck('code')
            ->all();

        if ($latest_pn != null) {
            $latest_pn = current($latest_pn);
            $pn = substr($latest_pn, 7);
            $id = str_pad(intval($pn) + 1, 3, '0', STR_PAD_LEFT);
        }

        $this->code = "PN" . $year . $month . $id;
    }


    public function setQuotationDocumentsAttribute($value)
    {
        $this->genericDocumentsMutator($value, 'quotation_');

    }

    public function setDocumentsAttribute($value)
    {
        if ($value) {
            $attribute_name = "documents";
            $disk = "public";

            foreach ($value as $index => $file) {
                if ($file && !is_string($file)) {
                    $file_name = $file->getClientOriginalName();
                    $destination_path = 'operation_documents/' . md5($file->getClientOriginalName() . time());
                    $this->uploadFilesToDisk($file, $attribute_name, $disk, $destination_path, $file_name, $index, false);

                }
            }
            //for only Deletion
            $this->uploadFilesToDisk('', $attribute_name, $disk, '', '', 0, true);
        }
    }


    public function uploadFilesToDisk($value, $attribute_name, $disk, $destination_path, $file_name, $index, $to_delete)
    {
        $request = \Request::instance();
        $attribute_value = (array)$this->{$attribute_name};
        $files_to_clear = $request->get('clear_' . $attribute_name);

        // if a file has been marked for removal,
        // delete it from the disk and from the db
        if ($files_to_clear) {
            $attribute_value = (array)$this->{$attribute_name};

            foreach ($files_to_clear as $key => $filename) {
                \Storage::disk($disk)->delete($filename);
                $attribute_value = array_where($attribute_value, function ($value, $key) use ($filename) {
                    return $value != $filename;
                });
            }
        }
        if (!$to_delete) {
            // if a new file is uploaded, store it on disk and its filename in the database
            if ($request->hasFile($attribute_name)) {

                $file = ($request->file($attribute_name)[$index]);

                if ($file->isValid()) {

                    // 2. Move the new file to the correct path

                    $file_path = $file->storeAs($destination_path, $file_name, $disk);

                    // 3. Add the public path to the database
                    $attribute_value[] = $file_path;

                }
            }
        }


        $this->attributes[$attribute_name] = json_encode($attribute_value);
        $this->save();

    }

    public function isSetDateTime($date)
    {
        $dateTimeLen = 19;
        return strlen($date) === $dateTimeLen;
    }

}
