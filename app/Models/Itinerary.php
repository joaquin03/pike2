<?php

namespace App\Models;

use App\Models\Accounting\Additional;
use App\Models\Accounting\AdministrationFee;
use App\Models\Accounting\Bill;
use App\Models\Scopes\OperationScope;
use App\Notifications\NewServiceToBillCompletedNotification;
use App\Notifications\NewServiceToProcurementCompletedNotification;
use Backpack\CRUD\CrudTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Config;
use Illuminate\Support\Facades\Log;
use OwenIt\Auditing\Auditable;
use Venturecraft\Revisionable\RevisionableTrait;
use App\Models\Traits\CustomRevisionableTrait;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Itinerary extends Model implements AuditableContract
{
    use SoftDeletes, CrudTrait, Auditable;

    protected $fillable = ['airport_icao', 'type', 'time', 'fuel_provider_id', 'fuel_gallons', 'fuel_price',
        'flight_plan_sent_at', 'operator_id', 'crew_captain_id', 'crew_first_official_id', 'crew_auxiliary_id',
        'airport_icao_string', 'procurement_fuel_status', 'procurement_service_status', 'billing_fuel_status',
        'billing_service_status', 'notes', 'date', 'pax', 'fbo_id', 'quotation_notes',
        'operation_notes', 'procurement_notes'
    ];

    public static $type = ['ATA', 'ATD', 'ETA', 'ETD'];

    protected $dates = [
        'date',
        'created_at',
        'updated_at',
    ];


    public static function boot()
    {
        parent::boot();
        static::creating(function (Itinerary $model) {
            $model->setCode();
        }, 0);

        static::created(function (Itinerary $model) {
            $model->save();
            $model->createFlightPlan();
        }, 0);
    }


    /*************
     * Relations *
     *************/

    public function operation()
    {
        return $this->belongsTo(Operation::class, 'operation_id', 'id')
            ->withoutGlobalScopes([OperationScope::class]);
    }

    public function quotation()
    {
        return $this->belongsTo(Quotation::class, 'operation_id', 'id');
    }

    public function airport()
    {
        return $this->hasOne(Airport::class, 'icao', 'airport_icao_string');
    }

    public function handler()
    {
        return $this->hasOne(Company::class, 'id', 'operator_id');
    }

    public function fbo()
    {
        return $this->hasOne(Provider::class, 'id', 'fbo_id');
    }

    public function fuelProvider()
    {
        return $this->hasOne(Company::class, 'id', 'fuel_provider_id');
    }

    public function crewCaptain()
    {
        return $this->hasOne(CrewMember::class, 'id', 'crew_captain_id');
    }

    public function crewFirstOfficial()
    {
        return $this->hasOne(CrewMember::class, 'id', 'crew_first_official_id');
    }

    public function crewAuxiliaryOfficial()
    {
        return $this->hasOne(CrewMember::class, 'id', 'crew_auxiliary_id');
    }

    public function itineraryProviderServices()
    {
        return $this->hasMany(ItineraryProviderService::class, 'itinerary_id', 'id');
    }

    public function itineraryProviderServicesParent()
    {
        return $this->itineraryProviderServices()->whereNull('parent_id')->with('children');
    }

    public function itineraryProviderServicesChild()
    {
        return $this->itineraryProviderServices()->whereNotNull('parent_id');
    }

    public function itineraryProviderServicesWithoutAdditionals()
    {
        return $this->itineraryProviderServices()->whereNull('is_additional')->whereNotNull('parent_id')
            ->where('service_id', '!=', Config::get('constants.service_fuel_child'));
    }

    //unification the way to obtain services
    public function itineraryProviderServicesForOperations()
    {
        return $this->itineraryProviderServicesParent()->serviceOperation();
    }

    public function itineraryProviderServicesForProcurement()
    {
        return $this->itineraryProviderServicesChild()->serviceProcurement();
    }

    public function itineraryProviderServicesForQuotation()
    {
        return $this->itineraryProviderServicesChild()->serviceQuotation();
    }


    public function flightPlan()
    {
        return $this->hasOne(FlightPlan::class);
    }

    public function bills()
    {
        return $this->hasMany(Bill::class, 'itinerary_id', 'id');
    }

    public function servicesChildOrderByParent()
    {
        return $this->hasMany(ItineraryProviderService::class, 'itinerary_id', 'id')
            ->groupBy('parent_service_id')->whereNotNull('parent_id');
    }


    //Todo Remove this
    public function serviceParents()
    {
        return $this->belongsToMany(ServiceParent::class, 'itinerary_provider_services', 'itinerary_id', 'service_id')
            ->groupBy('services.id');
    }

    //new staff
    public function itineraryServiceParents()
    {
        return $this->hasMany(ItineraryServiceParents::class, 'itinerary_id', 'id');
    }


    //Scopes
    public function scopeIsActive($query)
    {
        return $query->where('is_canceled', 0);
    }


    //Functions
    public function getItineraryServicesOrderByParent()
    {
        $data = collect();
        foreach ($this->itineraryProviderServicesParent as $parent) {
            $data->push($parent);
            $data = $data->merge($parent->children);
        }
        return $data;
    }

    public function existsItineraryProviderServices($provider_id, $service_id, $is_additional = null)
    {


        return $this->itineraryProviderServices()
            ->where('service_id', $service_id)
            ->where('provider_id', $provider_id)
            ->where(function ($q) { //Not invoiced ipss
                $q->whereNull('procurement_status')->orWhere('procurement_status', 0);
            })
            ->whereNull('has_bills')
            ->exists();

    }

    private function existsItineraryProviderServicesBilled(ItineraryProviderService $service)
    {
        return $this->itineraryProviderServices()
            ->where('service_id', $service->service_id)
            ->where('provider_id', $service->provider_id)
            ->where('procurement_status', 1)
            ->exists();
    }

    public function associateProviderServices(array $itineraryProviderServices)
    {
        foreach ($itineraryProviderServices as $key => $itineraryProviderService) {
            $providerService = ItineraryProviderService::find($itineraryProviderService['pivot_id']);
            $providerService->fill($itineraryProviderService);
            $providerService->save();
        }
    }

    public function updateProviderServiceStatus($service_id, $status)
    {
        $providerService = $this->providerServices()->where('service_provider_id', $service_id)->first()->pivot;
        $providerService->operation_status = $status;
        $providerService->save();

        return $providerService;

    }


    public function addService($data)
    {

        $service = new ItineraryProviderService();
        $service->fill($data);
        $service->itinerary_id = $this->id;

        $itineraryServiceParent = $this->findOrCreateParentService($service);

        if ($itineraryServiceParent) {
            $service = $this->createItineraryProviderServiceChild($service, $itineraryServiceParent);
        } else {
            $service->save();
            ItineraryServiceParents::createItem($service);
        }

        $this->billingOpeCompletedNotifyBilling($service);
//        $this->procurementOpeCompletedNotifyProcurement($service);
//          por ahora no lo quieren


        return $service;
    }

    public function findOrCreateParentService(ItineraryProviderService $itineraryService)
    {
        $service = $itineraryService->service;

        if ($service == null || $service->service_id == null) {
            return null;
        }

        $serviceParentId = $service->service_id;
        $parentItineraryService = $this->getParentService($serviceParentId, $itineraryService->provider_id);

        if ($parentItineraryService != null) {
            return $parentItineraryService;
        }

        //Creating Parent
        return $this->addService(
            ['provider_id' => $itineraryService->provider_id, 'service_id' => $serviceParentId,
                'service_origin' => $itineraryService->service_origin]);
    }

    public function addParentService($service)
    {

        $itineraryServiceParent = $this->findOrCreateParentService($service);

        if ($itineraryServiceParent) {
            return $this->createItineraryProviderServiceChild($service, $itineraryServiceParent);
        }

        $service->save();
        ItineraryServiceParents::createItem($service);

        return $service;
    }


    private function createItineraryProviderServiceChild(ItineraryProviderService $service,
                                                         ItineraryProviderService $itineraryServiceParent)
    {
        $service->parent_id = $itineraryServiceParent->id;
        $service->parent_service_id = $itineraryServiceParent->service_id;

        if ($this->existsItineraryProviderServicesBilled($service)) {
            $service->is_additional = 1;
        }

        $service->setDefaultUnit();

        $service->save();

        return $service;
    }

    private function getParentService($service_parent_id, $provider_id)
    {

        return $this->itineraryProviderServicesParent()->where('service_id', $service_parent_id)
            ->where('provider_id', $provider_id)
            ->first();
    }


    private function billingOpeCompletedNotifyBilling(ItineraryProviderService $service)
    {
        if ($this->operation == null) {
            return;
        }
        if ($this->operation->billing_status != 'Completed') {
            return;
        }

        if ($service->service_origin === 'Billing') {
            return;
        }
        $users = User::whereIn('id', Config::get('constants.billing_user_id'))->get();

        foreach ($users as $user) {
            $user->notify(new NewServiceToBillCompletedNotification($this->operation));
            $this->operation->billing_status = 'Pending';
            $this->operation->completed_at = null;
            $this->operation->save();
        }
    }


    private function procurementOpeCompletedNotifyProcurement(ItineraryProviderService $service)
    {
        if ($this->operation == null) {
            return;
        }
        if ($this->operation->procurement_status != 'Completed') {
            return;
        }
        $users = User::where('id', Config::get('constants.procurement_user_id'))->get();

        foreach ($users as $user) {
            $user->notify(new NewServiceToProcurementCompletedNotification($this->operation));
            $this->operation->procurement_status = 'In Process';
            $this->operation->save();
        }


    }


    public function addCrewMembers($data)
    {
        $this->crew_captain_id = $data['crew_captain_id'];
        $this->crew_first_official_id = $data['crew_first_official_id'];
        $this->crew_auxiliary_id = $data['crew_auxiliary_id'];

        $this->save();

        return $this;
    }

    public function editFlightPlan($data)
    {
        $flightPlan = $this->createFlightPlan($data);

        $flightPlan->fill($data);
        $flightPlan->save();

        return $flightPlan;
    }

    public function createFlightPlan()
    {
        if ($this->flightPlan != null) {
            return $this->flightPlan;
        } else {
            $flightPlan = new FlightPlan();
            $flightPlan->itinerary_id = $this->id;
            $flightPlan->save();
        }
    }


    //Helpers
    public function setCode()
    {
        if (!$this->canHaveServices()) {
            $this->code = null;
            return;
        }

        $lastItinerary = static::where('operation_id', $this->operation_id)->where('code', '!=', null)->latest('id')
            ->first();

        $this->code = 'A';
        if ($lastItinerary != null) {
            $this->code = ++$lastItinerary->code;
        }
    }

    public function canHaveServices()
    {
        return $this->type == 'ATA' || $this->type == 'ETA';
    }

    public function getDateAttribute()
    {
        if (array_key_exists('date', $this->attributes)) {
            if (!$this->attributes['date']) {
                return null;
            }
            return Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['date'])->toDateString();
        }
        return Carbon::now()->toDateString();
    }

    public function screenDate()
    {
        if (array_key_exists('date', $this->attributes)) {
            if (!$this->attributes['date']) {
                return null;
            }
            return Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['date'])->format('d-m-Y');
        }
        return Carbon::now()->toDateString();
    }
//
//    public function getTimeAttribute()
//    {
//        if ($this->attributes['time']) {
//            return Carbon::createFromFormat('g:ia', $this->attributes['time'])->toTimeString();
//        }
//        return null;
//
//    }
//
//
    public function setDateAttribute($value)
    {
        if ($value) {
            $this->attributes['date'] = Carbon::createFromFormat('d/m/Y', $value)->toDateTimeString();
        } else {
            $this->attributes['date'] = null;
        }

    }

    public function hasServices()
    {
        return $this->itineraryProviderServices()->count() > 0;
    }

    public function servicesTotalCount()
    {
        return $this->itineraryProviderServicesChild()->count() + $this->getAdditionals()->count();
    }

    //All the FP attributes are required, if the speed is null means that doesn't has a flight plan.
    public function hasFlightPlan()
    {
        return $this->flightPlan()->first()->speed;
    }

    public function isDeparture()
    {
        return ($this->type == 'ETD' || $this->type == 'ATD');
    }

    public function isArrival()
    {
        return !$this->isDeparture();
    }


    public function getCrewCaptain()
    {
        if ($this->crewCaptain != null) {
            return $this->crewCaptain->name;
        }
        return '';
    }

    public function getCrewFirstOfficial()
    {
        if ($this->crewFirstOfficial != null) {
            return $this->crewFirstOfficial->name;
        }
        return '';
    }

    public function getCrewAuxiliaryOfficial()
    {
        if ($this->crewAuxiliaryOfficial != null) {
            return $this->crewAuxiliaryOfficial->name;
        }
        return '';
    }


    public function getProviderOfServices()
    {
        return Provider::whereIn('id', $this->itineraryProviderServices()->pluck('provider_id'))->get();
    }

    public function getProviderOfServiceChilds()
    {
        return Provider::whereIn('id', $this->itineraryProviderServicesChild()->pluck('provider_id'))->get();
    }

    public function getServicesOfProvider($providerId)
    {
        return $this->itineraryProviderServicesParent()->where('provider_id', $providerId)->get();
    }

    public function getChildServicesOfProvider($providerId)
    {
        return $this->itineraryProviderServicesChild()->where('provider_id', $providerId)->get();
    }


    public function getProviderServicesTotal()
    {
        $total = 0;
        $services = $this->itineraryProviderServicesParent;
        foreach ($services as $service) {
            $total += $service->getProcurementFinalCost();
        }
        $additionals = $this->getAdditionals();
        foreach ($additionals as $additional) {
            $total += $additional->getFinalPrice();
        }
        return $total;

    }

    public function getProviderServiceFinalPrice($provider)
    {
        $operation = Operation::withoutGlobalScopes()->findOrFail($this->operation_id);
        $services = ItineraryProviderService::whereNotNull('parent_id')
            ->where('provider_id', $provider->id)->where('itinerary_id', $this->id)->get();

        return (double)$operation->itinerariesProviderServicesFinalCosts($services);
    }

    public function getQuotationProviderServiceFinalPrice($provider)
    {
        $quotation = Quotation::findOrFail($this->operation_id);
        $services = ItineraryProviderService::whereNotNull('parent_id')
            ->where('provider_id', $provider->id)->where('itinerary_id', $this->id)->get();
        return (double)$quotation->itinerariesProviderServicesFinalCosts($services);
    }


    private function serviceCategoriesQuery()
    {
        return ServiceCategory::join('services', 'category_services.id', 'services.category_id')
            ->join('services_providers', 'services.id', 'services_providers.service_id')
            ->join('itinerary_provider_services', 'services_providers.id',
                'itinerary_provider_services.service_provider_id')
            ->where('itinerary_provider_services.itinerary_id', $this->id);

    }

    public function getServiceCategories()
    {
        return $this->serviceCategoriesQuery()->distinct()->select('category_services.*')->get();
    }


    public function getAdministrationFees()
    {
        return $this->hasMany(AdministrationFee::class, 'itinerary_id', 'id')
            ->where('servicesable_type', get_class(new ItineraryProviderService()))->get();
    }

    public function getAdditionals()
    {
        return $this->hasMany(Additional::class, 'itinerary_id', 'id')
            ->where('servicesable_type', get_class(new ItineraryProviderService()))->get();
    }

    public function isAdditional()
    {
        return $this->is_additional === true;
    }

    public function getServiceCategoriesTotals()
    {
        return $this->serviceCategoriesQuery()
            ->select('category_services.*', 'itinerary_provider_services.billing_percentage as billing_percentage',
                DB::raw('ROUND(SUM(itinerary_provider_services.procurement_cost), 2) as category_cost'),
                DB::raw('ROUND(SUM(itinerary_provider_services.procurement_cost) * ((100 + billing_percentage)/100), 2) as category_total'),
                DB::raw('ROUND(SUM(itinerary_provider_services.procurement_cost) * billing_percentage/100, 2) as category_sub_total'))
            ->groupBy('category_services.id')->get()->toArray();
    }

    public function updateServiceCategoryBillingPercentage($categoryId, $billing_percentage)
    {
        $category = ServiceCategory::find($categoryId);

        $providerServicesIds = $category->getServicesOfItinerary($this)->pluck('id');

        return ItineraryProviderService::where('itinerary_id', $this->id)
            ->whereIn('service_provider_id', $providerServicesIds)
            ->update(['billing_percentage' => $billing_percentage]);
    }

    public function getCategoryTotal()
    {
        $categories = $this->getServiceCategoriesTotals();
        $categoriesTotal = 0;
        foreach ($categories as $category) {
            $categoriesTotal += $category['category_total'];
        }

        return $categoriesTotal;
    }

    public function getBillingTotals()
    {
        return $this->getCategoryTotal() + $this->billing_pike_service_cost - $this->billing_discount;
    }

    public function getBillingInfo()
    {
        return [
            'totals' => $this->getBillingTotals(),
            'billing_pike_service_cost' => $this->billing_pike_service_cost,
            'billing_discount' => $this->billing_discount,
            'billing_status' => $this->billing_status,
            'billing_notes' => $this->billing_notes,
        ];
    }

    public function updateBillingInfo($data)
    {
        $this->billing_status = $data['billing_status'] ?? 0;
        $this->billing_discount = $data['billing_discount'] ?? 0;
        $this->billing_pike_service_cost = $data['billing_pike_service_cost'] ?? null;
        $this->billing_notes = $data['billing_notes'] ?? null;
        $this->save();
    }

    public function getBillingPikeServiceCostAttribute()
    {
        return null; // Todo: If this dont brake nothing remove
        if ($this->attributes['billing_pike_service_cost'] == null) {
            return $this->operation->getDefaultAircraftServicePrice();
        }
        return $this->attributes['billing_pike_service_cost'];
    }

    public function procurementTransformServiceCategoriesTotals()
    {
        $items = $this->getServiceCategoriesTotals();
        $data = [];
        foreach ($items as $item) {
            $data[] = [
                'id' => $item['id'],
                'service' => $item['name'],
                'provider' => 'Pike',
                'quantity' => '',
                'procurement_cost' => $item['category_cost'],
                'percentage' => $item['billing_percentage'],
                'amount' => '',
                'profit' => $item['category_sub_total'],
                'price' => $item['category_total'],
            ];
        }
        $data[] = $this->procurementCalculateTotalServices($data);
        return $data;
    }

    private function procurementCalculateTotalServices($items)
    {
        $total = [
            'price' => null,
            'profit' => null,
            'procurement_cost' => null
        ];
        foreach ($items as $item) {
            $total['price'] += $item['price'];
            $total['profit'] += $item['profit'];
            $total['procurement_cost'] += $item['procurement_cost'];
        }
        return [
            'id' => '',
            'service' => 'TOTAL',
            'provider' => '',
            'quantity' => '',
            'procurement_cost' => $total['procurement_cost'] ?? '',
            'percentage' => '',
            'amount' => '',
            'profit' => $total['profit'] ?? '',
            'price' => $total['price'] ?? '',
        ];
    }

    public function getProcurementFuelStatus()
    {
        if ($this->procurement_fuel_status == null) {
            return null;
        }
        return $this->procurement_fuel_status;
    }

    public function getProcurementServicesStatus()
    {
        if ($this->procurement_service_status == null) {
            return null;
        }
        return $this->procurement_service_status;
    }

    public function getBillingFuelStatus()
    {
        if ($this->billing_fuel_status == null) {
            return null;
        }
        return $this->billing_fuel_status;
    }


    public function getBillingServicesStatus()
    {
        if ($this->billing_service_status) {
            return;
        }
        return $this->billing_service_status;
    }

    public function hasTime()
    {
        return $this->time != null;
    }

    public function hasDate()
    {
        return $this->date != null;
    }

    public function canHaveHandler()
    {
        return $this->type === 'ETA' || $this->type === 'ATA';
    }

    public function addDefaultHandlerOnCreate()
    {
        $provider = Provider::find($this->operator_id);
        if ($provider && $provider->handlingService()) {
            $service = $provider->handlingService();
            $data = [
                'service_id' => $service->id,
                'provider_id' => $provider->id,
                'operation_status' => 'Requested',
                'service_origin' => 'Operation',
            ];

            $this->addService($data);
        }
        return $this;
    }


    //Procurement status
    public function setProcurementStatus($save = false)
    {
        $change = ($this->setProcurementServicesStatus() || $this->setProcurementFuelStatus());

        if ($save && $change) {
            $this->save();
        }
        return $this;
    }

    private function setProcurementFuelStatus()
    {
        $items = $this->serviceCategoriesQuery()->where('services.category_id',
            Config::get('constants.serviceCategoryFuel'))->get();

        return $this->defineProcurementStatus('procurement_fuel_status', $items);

    }

    private function setProcurementServicesStatus()
    {
        $items = $this->serviceCategoriesQuery()->where('services.category_id', '<>',
            Config::get('constants.serviceCategoryFuel'))->get();

        return $this->defineProcurementStatus('procurement_service_status', $items);

    }

    //Billing status
    public function setBillingStatus($save = false)
    {
        $change = ($this->setBillingFuelStatus() || $this->setBillingServicesStatus());

        if ($save && $change) {
            $this->save();
        }
        return $this;
    }

    private function setBillingFuelStatus()
    {
        $items = $this->serviceCategoriesQuery()->where('services.category_id',
            Config::get('constants.serviceCategoryFuel'))->get();

        return $this->defineStatus('billing_fuel_status', $items);

    }

    private function setBillingServicesStatus()
    {
        $items = $this->serviceCategoriesQuery()->where('services.category_id', '<>',
            Config::get('constants.serviceCategoryFuel'))->get();

        return $this->defineStatus('billing_service_status', $items);

    }


    private function defineStatus($status_attribute, Collection $items)
    {
        if ($items == null || $items->all() == []) {
            $newStatus = null;
        } else {
            $numberItemsDone = $items->whereIn('procurement_status', ['Done'])->count();

            $newStatus = 'Process';

            if ($items->count() == $numberItemsDone) {
                $newStatus = 'Done';
            };
        }
        //Edited?
        if ($this->{$status_attribute} == $newStatus) {
            return false;
        }
        $this->{$status_attribute} = $newStatus;
        return true;
    }

    public function getProviderServicesOfProvider($providerId)
    {
        return $this->itineraryProviderServices()->whereHas('ProviderService',
            function ($query) use ($providerId) {
                $query->where('provider_id', $providerId);
            })->get();

    }

    public function getServicesBill(Provider $provider)
    {
        return $this->bills()->where('company_id', $provider->id)->first();
    }


    public function getBillingCheckedServices()
    {
        return $this->itineraryProviderServices()->where('billing_checked', 1);
    }

    public function getBillingCheckedServicesParents()
    {
        return $this->itineraryServiceParents()->where('billing_checked', 1);
    }

    public function getCountryCode()
    {
        return $this->airport->country;
    }

    public function getCountryName()
    {
        return $this->airport->getCountry();
    }

    public function isEmpty()
    {
        return $this->itineraryProviderServicesChild()->get()->isEmpty();
    }

    public function isHandlingQuotation()
    {
        return $this->quotation ? $this->quotation->quotation_services_type == 'Handling' : false;
    }

    // Ips for quotations export
    public function itineraryProviderServicesChildsNotByPike()
    {
        return ItineraryProviderService::where('itinerary_id', $this->id)
            ->where('parent_id', '!=', null)
            ->where('is_additional', null)
            ->where('provider_id', '!=', env('CENTRAL_PIKE', 51));
    }

    public function itineraryProviderServicesChildsNotByPikeWithoutFuel()
    {
        return ItineraryProviderService::where('itinerary_id', $this->id)
            ->where('parent_id', '!=', null)
            ->where('is_additional', null)
            ->where('provider_id', '!=', env('CENTRAL_PIKE', 51))
            ->where('service_id', '!=', Config::get('constants.service_fuel_child'));
    }

    public function itineraryFuelServices()
    {
        return ItineraryProviderService::where('itinerary_id', $this->id)
            ->where('parent_id', '!=', null)
            ->where('is_additional', null)
            ->where('service_id', Config::get('constants.service_fuel_child'));
    }

    public function itineraryProviderServicesChildsNotByPikeWithoutHandling()
    {
        return ItineraryProviderService::where('itinerary_id', $this->id)
            ->where('parent_id', '!=', null)
            ->where('is_additional', null)
            ->where('provider_id', '!=', env('CENTRAL_PIKE', 51))
            ->where('service_id', '!=', Config::get('constants.handling_services_id'))
            ->where('service_id', '!=', Config::get('constants.service_fuel_child'));

    }

    public function itineraryProviderServicesChildsByPike()
    {
        return ItineraryProviderService::where('itinerary_id', $this->id)
            ->where('parent_id', '!=', null)
            ->where('is_additional', null)
            ->where('provider_id', env('CENTRAL_PIKE', 51));
    }

    public function itineraryProviderServicesAdditionalsQuotation()
    {
        return ItineraryProviderService::where('itinerary_id', $this->id)
            ->where('is_additional', 1)
            ->where('parent_id', '!=', null);
    }

    public function getHandlingService()
    {
        return ItineraryProviderService::where('itinerary_id', $this->id)
            ->where('parent_id', '!=', null)
            ->where(function ($query) {
                $query->where('service_id', Config::get('constants.handling_services_id'));
            });
    }


    public function getItinerarySubtotalCostWithoutAdmFee()
    {
        if ($this->isHandlingQuotation()) {
            return round($this->itineraryProviderServicesFinalCostsQuotationWithoutAdmFee(
                $this->itineraryProviderServicesChildsNotByPikeWithoutHandling()->get()
            ), 2);
        } else {
            return round($this->itineraryProviderServicesFinalCostsQuotationWithoutAdmFee(
                $this->itineraryProviderServicesChildsNotByPikeWithoutFuel()->get()
            ), 2);
        }
    }

    private function getItineraryPikeServicesCostWithoutAdmFee()
    {

        return round($this->itineraryProviderServicesFinalCostsQuotationWithoutAdmFee(
            $this->itineraryProviderServicesChildsByPike()->get()
        ), 2);
    }

    public function getItinerarySubtotalCostWithAdmFee()
    {
        return round($this->itineraryProviderServicesFinalCostsQuotationWithAdmFee(
            $this->itineraryProviderServicesChildsNotByPike()->get()
        ), 2);
    }

    public function getItinerarySubtotalCostWithAdmFeeWithoutFuel()
    {
        return round($this->itineraryProviderServicesFinalCostsQuotationWithAdmFee(
            $this->itineraryProviderServicesChildsNotByPikeWithoutFuel()->get()
        ), 2);
    }

    private function getItineraryPikeServicesCostWithAdmFee()
    {
        return round($this->itineraryProviderServicesFinalCostsQuotationWithAdmFee(
            $this->itineraryProviderServicesChildsByPike()->get()
        ), 2);
    }

    public function getItinieraryServicesAdmFees()
    {
        return round($this->itineraryProviderServicesFinalAdmFees(
            $this->itineraryProviderServicesWithoutAdditionals()->get()
        ), 2);
    }

    public function getItineraryHandlingService()
    {
        return round($this->itineraryProviderServicesFinalCostsQuotationWithoutAdmFee($this->getHandlingService()->get())
            , 2);
    }


    public function itineraryCostWithoutCharges()
    {
        return round((($this->getItinerarySubtotalCostWithAdmFeeWithoutFuel() + $this->getItineraryPikeServicesCostWithAdmFee())), 2);
    }

    public function itineraryCostWithoutChargesWithoutFuel()
    {
        return round((($this->getItinerarySubtotalCostWithAdmFeeWithoutFuel() + $this->getItineraryPikeServicesCostWithAdmFee())), 2);
    }

    public function getItineraryTotalCost()
    {
        $chargeApplicable = $this->quotation->extraData && $this->quotation->extraData->payment === 'Credit Card';
        $charge = 0;

        if ($chargeApplicable) {
            $charge = $this->getQuotationCreditNoteSurcharge($this->itineraryCostWithoutCharges());
        }

        return $this->itineraryCostWithoutCharges() + $charge;

    }

    public function getItineraryFuelCost()
    {
        $fuels = $this->itineraryFuelServices()->get();

        $fuelCost = 0;
        foreach ($fuels as $fuel) {
            $fuelCost += $fuel->getFinalCostQuotation();
        }
        return $fuelCost;
    }


    public function getQuotationCreditNoteSurcharge($itineraryTotal)
    {
        $lastUpdate = QuotationCreditNoteCharges::orderBy('updated_at', 'desc')->first();
        $surcharge = 0;

        if ($lastUpdate) {
            $lastUpdate = $lastUpdate->updated_at;
            $surcharges = QuotationCreditNoteCharges::where('from', '<', $itineraryTotal)
                ->where('to', '>', $itineraryTotal)->where('updated_at', $lastUpdate)->get();
            if ($surcharges && !$surcharges->isEmpty()) {
                $surcharge = $surcharges[0]['value'];
            }

        }

        return $surcharge;
    }


    private function itineraryProviderServicesFinalCostsQuotationWithoutAdmFee($services)
    {
        $finalCost = 0;
        foreach ($services as $service) {
            $finalCost += $service->getFinalCostQuotationWithoutAdmFee();
        }
        return $finalCost;
    }

    private function itineraryProviderServicesFinalCostsQuotationWithAdmFee($services)
    {
        $finalCost = 0;
        foreach ($services as $service) {
            $finalCost += $service->getFinalCostQuotation();
        }
        return $finalCost;
    }

    private function itineraryProviderServicesFinalAdmFees($services)
    {
        $admFee = 0;
        foreach ($services as $service) {
            $admFee += $service->getAdministrationFee();
        }
        return $admFee;
    }

    public function itineraryETDETAAirport()
    {
        $airports = [];
        $newAirport = $this->airport;
        if (!in_array($newAirport, $airports) & $newAirport != null) {
            $airports[] = $newAirport;
        }

        $airportsIcaos = Collection::make($this->itineraryAirports())->pluck('icao');


        return $airportsIcaos->implode(' - ');
    }

    public function itineraryAirportsIcaos()
    {
        if ($this->isArrival()) {
            return $this->quotation->getArrivalOriginDepartureIcao($this);
        }
        return '-';
    }


}

