<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class HandlingServiceQuotation extends Model
{
    use CrudTrait;
    
    protected $fillable = [
        'tons_from', 'tons_to', 'price',
    ];
    protected $table ='handling_services_quotation';
    
    
    
    
    public static function getPrice($tons)
    {
        if (is_null($tons)) {
            return "-";
        }
        
        return self::findHandlingServiceQuotation($tons)->price;
    }
    
    
    public static function findHandlingServiceQuotation($tons)
    {
        if (is_null($tons)) {
            return "-";
        }
        
        return self::where('tons_from', '<', $tons)
                            ->where('tons_to', '>', $tons)->first();
    }
}
