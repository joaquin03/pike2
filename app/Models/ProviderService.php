<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ProviderService extends Model
{
    protected $table = 'services_providers';
    
    protected $fillable = [
        'price', 'comments',
    ];


    public function provider()
    {
        return $this->belongsTo(Provider::class);
    }
    
    public function service()
    {
        return $this->belongsTo(Service::class)->withTrashed();
    }
    
    public function serviceOperation()
    {
        return $this->belongsTo(ServiceOperation::class, 'service_id');
    }
    
    public function serviceProcurement()
    {
        return $this->belongsTo(ServiceProcurement::class, 'service_id');
    }
    
    public function serviceQuotation()
    {
        return $this->belongsTo(ServiceQuotation::class, 'service_id');
    }
    
    
    public function itineraries()
    {
        return $this->belongsToMany(Itinerary::class, 'itinerary_provider_services', 'service_provider_id', 'itinerary_id')
            ->withPivot('procurement_cost', 'comments', 'procurement_quantity', 'billing_percentage','billing_quote_price', 'operation_status',
                'procurement_status', 'procurement_adm_fee', 'procurement_tax', 'procurement_adm_fee', 'administration_fee');
    }
    
    //Scopes
    public function scopeExtraData(Builder $builder)
    {
        return $builder
            ->join('companies', 'services_providers.provider_id', '=', 'companies.id')
            ->join('services', 'services_providers.service_id', '=', 'services.id')
            ->select('services_providers.*', 'companies.name as provider_name', 'services.name as service_name');
    }
    
    public function scopeProviders(Builder $builder)
    {
        return $builder
            ->join('companies', 'services_providers.provider_id', '=', 'companies.id')
            ->where('companies.is_provider', true)
            ->select('companies.*')
            ->groupBy('companies.id');
    }
    
    public function getFinalCost()
    {
        return ($this->pivot->procurement_quantity * $this->pivot->procurement_cost) * (1 + ($this->pivot->tax))
            * (1 + ($this->pivot->procurement_adm_fee));
    }
    
    
    public function getProfitPrice()
    {
        return $this->getFinalPrice() - $this->getFinalCost();
    }
    
    public function getFinalPrice()
    {
        if($this->pivot->billing_quote_price != null){
            return $this->pivot->procurement_quantity * $this->pivot->billing_quote_price;
        }
        $finalCost = $this->getFinalCost();
        return $finalCost * (1 + ($this->pivot->billing_percentage)) + $this->pivot->billing_unit_price ;
    }
    
    
}
