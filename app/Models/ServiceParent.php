<?php

namespace App\Models;

use App\Models\Accounting\AccountingService;
use App\Models\Accounting\Bill;
use App\Models\Contracts\AbstractOperationServiceInterface;
use App\Models\Contracts\AccountableServiceInterface;
use App\Models\Contracts\AccountingItem;
use App\Models\Scopes\ServiceParentScope;
use App\Models\Traits\AccountableServiceTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceParent extends Service
{
    use AccountableServiceTrait;
    
    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(new ServiceParentScope());
    }
    
    private $itineraryChildServices = null;
    
     /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
   
    public $table = 'services';
    
    /*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
    */
    public function itineraryServices()
    {
        return $this->hasMany(ItineraryProviderService::class, 'service_id', 'id');
    }
    
    public function childItineraryServices()
    {
        return $this->hasMany(ItineraryProviderService::class, 'parent_service_id', 'id');
    }
    
    
    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function getItineraryParentService($itineraryId)
    {
        return $this->itineraryServices()
            ->where('itinerary_id', $itineraryId)
            ->whereNull('parent_id')->first();
    }
    
    public function getItineraryChildServices($itineraryId)
    {
        if ($this->itineraryChildServices == null) {
            $this->itineraryChildServices = $this->childItineraryServices()
                ->where('itinerary_id', $itineraryId)
                ->where('is_additional', '=',null)
                ->get();
        }
        return $this->itineraryChildServices;
    }
    
    public function getItineraryChildServicesAdditionals($itineraryId)
    {
        if ($this->itineraryChildServices == null) {
            $this->itineraryChildServices = $this->childItineraryServices()
                ->where('itinerary_id', $itineraryId)
                ->where('is_additional', '=',1)
                ->get();
        }
        return $this->itineraryChildServices;
    }
    
    public function services()
    {
        return $this->hasMany(Service::class, 'service_id', 'id')->orderBy('name');
    }
    
    
    //Billing
    public function getBillingFinalCost($itineraryId)
    {
        $total = 0;
        $items = $this->getItineraryChildServices($itineraryId);
        foreach ($items as $item) {
            $total += $item->getProcurementFinalCost();
        }
        return round($total,2);
    }
    public function getBillingFinalPrice($itineraryId)
    {
        $total = 0;
        $items = $this->getItineraryChildServices($itineraryId);
        foreach ($items as $item) {

            $total += $item->getBillingFinalPrice();
        }
        return round($total,2);
    }

    //getQuotationFinalPrice
    public function getQuotationFinalPrice($itineraryId)
    {
        $total = 0;
        $items = $this->getItineraryChildServices($itineraryId);
        foreach ($items as $item) {
            $total += $item->getQuotationFinalPrice();
        }
        return round($total,2);
    }


    public function getBillingProfit($itineraryId)
    {

        $total = 0;
        $items = $this->getItineraryChildServices($itineraryId);
        foreach ($items as $item) {
            $total += $item->getProfitPrice();
        }
        return round($total,2);
    }
    
    public function getHasBill($itineraryId)
    {
        return $this->childItineraryServices()
            ->where('itinerary_id', $itineraryId)->whereNotNull('has_bills')->exists();
    }
    public function getHasAdmFee($itineraryId)
    {
        return $this->itineraryServices()
            ->where('itinerary_id', $itineraryId)->whereNotNull('administration_fee')->exists();
    }
    
    public function createAccountingService(AccountingItem $bill)
    {

        $accountingService = new AccountingService();
        $accountingService->servicesable()->associate($this);
        $accountingService->fill($this->toArray());
        $accountingService->description = $this->getProcurementDescription();
        $accountingService->bill_id = $bill->id;
        
        $accountingService->billing_final_price += $this->getBillingFinalPrice(1);
        $accountingService->procurement_cost += $this->getBillingFinalCost(1);
    
        $accountingService->itinerary_services = $this->getItineraryChildServices(1)->pluck('id');
        
        $accountingService->save();
    }
    
    public function addBill($billId){
        $this->has_bills = $billId;
        $this->save();
    }
    public function addInvoice()
    {
        $this->procurement_status = 1;
        $this->save();
    }
    
    
    public function getProcurementDescription()
    {
        return $this->name;
    }
    
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    
    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
