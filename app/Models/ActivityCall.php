<?php

namespace App\Models;

use App\Models\Contracts\ActivityEvent;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class ActivityCall extends ActivityEvent
{
    use CrudTrait;

     /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    
    protected $table = 'calls';
    protected $fillable = [
        'name', 'start_date', 'end_date', 'summary'
    ];
    
    public function getTitle()
    {
        return "Call with ".$this->name;
    }
    public function getDescription()
    {
        return "";
    }
    public function getIcon()
    {
        return 'fa fa-phone';
    }
    
    public function getColor()
    {
        return 'bg-teal';
    }
    
    public function getProposal()
    {
        return str_replace("_", " ",$this->proposal) ?? " ";
    }

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

   
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
