<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Report extends Model
{
    use CrudTrait;
    
    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    
    public function getParticipantsNames($participantsIds)
    {
        $participantsNames = '';
        foreach ($participantsIds as $x => $participantId) {
            $contact = User::findOrFail($participantId);
            $participantsNames = $participantsNames . $contact->name . ', ';
        };
        return rtrim($participantsNames, ", ");
    }
    
    public function getTagName($tagId)
    {
        return CompanyTag::findOrFail($tagId)->name;
    }
    
    public function notes($tagId)
    {
        $tag = CompanyTag::findOrFail($tagId);
        return $tag->note;
    }
    
    
    
    
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    
    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */
    
    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
