<?php

namespace App\Models\Accounting;

use App\Models\Contracts\AbstractOperationService;
use App\Models\Contracts\AccountingItem;
use App\Models\ItineraryProviderService;
use App\Models\ProviderService;
use App\Models\Scopes\OVFScope;
use phpDocumentor\Reflection\Types\Parent_;

class Additional extends AbstractOperationService
{
    /*
  |--------------------------------------------------------------------------
  | GLOBAL VARIABLES
  |--------------------------------------------------------------------------
  */
    
    
    protected $fillable = [
        'itinerary_id', 'operation_id', 'tax', 'billing_percentage', 'billing_unit_price', 'billing_price', 'procurement_status'
    ];
    
    
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    
    public function servicesable()
    {
        return $this->morphTo()->withoutGlobalScope(OVFScope::class);
    }
    
    public function getServicesableId(){
        return $this->servicesable_id;
    }
    
    
    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    
    /***************
     * Accountable
     ****************/
    //Procurement
    public function getProcurementDescription()
    {
        return 'Additional: '.$this->servicesable->getProcurementDescription();
    }
    public function getProcurementQuantity()
    {
        return 1;
    }
    public function getProcurementCost()
    {
        return $this->billing_unit_price;
    }
    public function getProcurementAdmFee()
    {
        return $this->billing_unit_price;
    }
    public function getProcurementTax()
    {
        return $this->procurement_tax;
    }
    public function getProcurementUnitFinalCost()
    {
        if ($this->parent_id == null) {
            return $this->getProcurementFinalUnitParent();
        }
        return $this->getProcurementFinalUnitChild();
    }
    public function getProcurementFinalCost()
    {
        return round($this->getProcurementUnitFinalCost() * $this->getProcurementQuantity(), 3);
    }
    
    
    private function getProcurementFinalUnitChild()
    {
        return $this->procurement_cost *(1 + ($this->procurement_adm_fee)) * (1 + $this->procurement_tax);
    }
    private function getProcurementFinalUnitParent()
    {
        $total = 0;
        foreach ($this->children as $child) {
            $total += $child->getProcurementFinalCost();
        }
        return $total;
    }
    
    //Billing
    public function getBillingDescription()
    {
        $description = $this->getService()->name;
        if ($this->isAdditional()) {
            $description .= ' Additional';
        }
        
        return $description;
    }
    public function getBillingQuantity()
    {
        return $this->billing_quantity ?? 1;
    }
    public function getBillingPercentage()
    {
        return $this->billing_percentage;
    }
    public function getBillingUnitPrice()
    {
        if (is_null($this->parent_id)) {
            return $this->getBillingFinalUnitPriceParent();
        }
        return $this->getBillingFinalUnitPriceChild();
    }
    public function getBillingFinalPrice()
    {
        return $this->getBillingUnitPrice() * $this->getBillingQuantity();
    }
    
    
    private function getBillingFinalUnitPriceParent()
    {
        $total = 0;
        foreach ($this->children as $child) {
            $total += $child->getBillingFinalPrice();
        }
        return $total;
    }
    private function getBillingFinalUnitPriceChild()
    {
        if ($this->billing_unit_price != 0) {
            return $this->billing_unit_price * $this->billing_quantity * (1 + $this->billing_percentage);
        }
        $unitCost = $this->getProcurementUnitFinalCost();
        
        return $unitCost + $this->getPercentageValue($unitCost);
    }
    private function getPercentageValue($finalCost)
    {
        if ($this->billing_percentage) {
            return $this->billing_percentage * $finalCost;
        }
        return 0;
    }
    
    //Extras
    public function getProfitPrice()
    {
        return $this->getBillingFinalPrice() - $this->getProcurementFinalCost();
    }
    
    
    public function deleteItem()
    {

        if ($this->has_bills != 0) {
            return false;
        }
        if ($this->procurement_status != 0) {
            return false;
        }

        if ($this->administration_fee == 1) {
            return false;
        }

        return $this->delete();
    }


  
}
