<?php

namespace App\Models\Accounting;

use App\Models\AircraftPermission;
use App\Models\Contracts\AbstractOperationService;
use App\Models\ItineraryProviderService;
use App\Models\ProviderService;
use App\Models\Scopes\OVFScope;
use App\Models\ServiceParent;
use Illuminate\Database\Eloquent\SoftDeletes;


class AccountingService extends AbstractOperationService
{
    use SoftDeletes;
    /*
  |--------------------------------------------------------------------------
  | GLOBAL VARIABLES
  |--------------------------------------------------------------------------
  */

    //protected $table = '';

    protected $fillable = [
        'notes', 'service_price', 'billing_percentage', 'billing_quote_price', 'billing_unit_price',
        'billing_final_price', 'bill_id', 'procurement_adm_fee', 'procurement_tax'
    ];

    protected $casts = [
        'itinerary_services' => 'array',
    ];

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function bill()
    {
        return $this->belongsTo(Bill::class, 'id', 'bill_id');
    }

    public function invoice()
    {
        return $this->belongsTo(Invoice::class, 'id', 'invoice_id');
    }

    public function serviceProvider()
    {
        return $this->belongsTo(ProviderService::class, 'id', 'service_provider_id');
    }

    public function servicesable()
    {
        return $this->morphTo()->withoutGlobalScope(OVFScope::class);
    }

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */


    public function getDescription()
    {
        if ($this->servicesable instanceof AdministrationFee) {
            return $this->description . ' (' . $this->servicesable->billing_percentage . ')';
        }
        return $this->description;
    }

    public function getUnitPrice()
    {
        return round($this->getBillingUnitPrice() * (1 + $this->billing_percentage), 3);
    }

    public function getBillingUnitPrice()
    {
        return round($this->billing_unit_price,3) ?? $this->procurement_cost;
    }

    public function getServiceFinalPrice()
    {
        if ($this->billing_unit_price > 0) {
            return $this->procurement_quantity * $this->billing_unit_price * (1 + $this->procurement_tax)
                * (1 + $this->procurement_adm_fee);
        }

        return round($this->billing_final_price, 3);
    }

    public function getServiceFinalCost()
    {
        $amount = $this->procurement_quantity * $this->procurement_cost * (1 + $this->procurement_tax);
        if(!$this->isPermit()) {
            $amount *=  (1 + $this->procurement_adm_fee);
        }
        return $amount;
    }


    public function isPermit()
    {
        return $this->servicesable instanceof AircraftPermission;
    }

    public function addBill($billId)
    {
        $this->servicesable->addBill($billId);
    }

    public function addInvoice()
    {
        if ($this->servicesable) {
            $this->servicesable->addInvoice();
        }
    }

    public function removeBilledFromService()
    {
        return $this->servicesable->removeBill();
    }

    public function deleteItem()
    {
        $service = $this->servicesable;
        try { //Prevent error with service Parent.
            $service->procurement_status = 0;
            $service->save();
        } catch (\Exception $e) {
        }


        $this->delete();
    }

    private function addBillToServiceParent()
    {
        $items = ItineraryProviderService::whereIn('id', $this->itinerary_services)->get();
        foreach ($items as $item) {
            $item->addBill($this->bill_id);
        }
    }

    public function isItineraryService()
    {
        return ($this->servicesable instanceof ItineraryProviderService || $this->servicesable instanceof ServiceParent);
    }

    public function isAdditional()
    {
        return ($this->servicesable instanceof Additional);
    }

    public function getInvoice()
    {
        $accountingService = $this;

        return Invoice::where('is_active', true)->whereHas('accountingServices',
            function ($query) use ($accountingService) {
                $query->where('servicesable_id', $accountingService->servicesable_id)
                    ->where('servicesable_type', $accountingService->servicesable_type);
            })->first();

    }

    public function getBill()
    {
        $accountingService = $this;

        return Bill::where('is_active', true)->whereHas('accountingServices',
            function ($query) use ($accountingService) {
                $query->where('servicesable_id', $accountingService->servicesable_id)
                    ->where('servicesable_type', $accountingService->servicesable_type);
            })->first();

    }




}
