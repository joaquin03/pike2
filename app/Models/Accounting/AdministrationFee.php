<?php

namespace App\Models\Accounting;

use App\Models\Contracts\AbstractOperationService;
use App\Models\Contracts\AccountingItem;
use App\Models\ItineraryProviderService;
use App\Models\ProviderService;
use App\Models\Scopes\OVFScope;
use phpDocumentor\Reflection\Types\Parent_;

class AdministrationFee extends AbstractOperationService
{
    /*
  |--------------------------------------------------------------------------
  | GLOBAL VARIABLES
  |--------------------------------------------------------------------------
  */
    
    
    protected $fillable = [
        'itinerary_id', 'operation_id', 'procurement_tax', 'billing_percentage', 'billing_unit_price', 'billing_final_price'
    ];

    protected $attributes = [
        'billing_percentage' => 0.12,
    ];
    
    
    public function getServiceFinalPrice()
    {
        return round ( $this->servicesable->getBillingFinalPrice(), 3);
    }
    
    public function getFinalCost()
    {
        return 0;
    }
    
    
    public function getProfitPrice()
    {
        return round ($this->getFinalPrice() - $this->getFinalCost(), 3 );
    }
    
    //TODO:: CHECK
    public function getFinalPrice()
    {
        return round ( ($this->getServiceFinalPrice()) * ( $this->billing_percentage), 3);
    }
    
    public function getUnitPrice(){
        return $this->getFinalPrice();
    }
    
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    
    public function servicesable()
    {
        return $this->morphTo()->withoutGlobalScope(OVFScope::class);
    }
    
    public function getServicesableId(){
        return $this->servicesable_id;
    }
  
    
    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function createAccountingService(AccountingItem $bill)
    {
        $administrationFee = $bill->administrationFees()->first();
        if ($administrationFee==null) {
            return Parent::createAccountingService($bill);
        }
        round($administrationFee->billing_unit_price += $this->getFinalPrice(),3);
        round($administrationFee->billing_final_price += $this->getFinalPrice(),3);
        $administrationFee->billing_percentage = $this->billing_percentage;
        $administrationFee->save();
        
    }
    
    public function getProcurementDescription()
    {
        return 'Administration Fee';
    }
    
    public function removeBill()
    {
    
    }
    
    
}
