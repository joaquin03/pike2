<?php

namespace App\Models\Accounting;

use App\Helpers\Garino\GarinoSFLFileBill;
use App\Helpers\Garino\GarinoSFLFileCreditNote;
use App\Models\Aircraft;
use App\Models\AircraftPermission;
use App\Models\Contracts\AbstractAccountableService;
use App\Models\Contracts\AbstractOperationServiceInterface;
use App\Models\Contracts\AccountableServiceInterface;
use App\Models\Contracts\AccountingItem;
use App\Models\Itinerary;
use App\Models\ItineraryProviderService;
use App\Models\Operation;
use App\Models\PermanentPermission;
use App\Models\Scopes\NotDebitNoteScope;
use App\Models\Scopes\OVFScope;
use App\Models\ServiceParent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Backpack\CRUD\CrudTrait;
use App\Models\Company;
use App\Models\User;
use Carbon\Carbon;
use App\Models\Scopes\BillScope;
use OwenIt\Auditing\Contracts\Auditable;

class Bill extends AccountingItem
{
    use SoftDeletes, CrudTrait;
    
    public static function boot()
    {
        parent::boot();

        static::addGlobalScope(new BillScope());
        static::addGlobalScope(new NotDebitNoteScope());
    }
    
    /*
   |--------------------------------------------------------------------------
   | GLOBAL VARIABLES
   |--------------------------------------------------------------------------
   */
    protected $table = 'bills';
    
    protected $dates = [
        'start_date', 'due_date', 'deleted_at', 'date'
    ];
    
    protected $with = ['payments', 'creditNotes'];
    
    protected $paymentMethods = [1 => 'Cash', 2 => 'Credit'];
    
    protected $fillable = [
        'bill_number', 'itinerary_id', 'operation_id', 'company_id', 'user_id', 'amount', 'type', 'date', 'due_date',
        'status', 'note', 'attachment', 'created_at', 'updated_at', 'is_active', 'dollar_change', 'bill_link',
        'payment_method', 'billing_company_id', 'ref_number'
    ];
    protected $appends = ['remaining_amount'];
    
    
    
    
    //protected $table = 'bills';
    //protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];
    
    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function getClientName()
    {
        return Company::findOrFail($this->billingCompany)->name;
    }
    
    public function getAttachments()
    {
        return $this->attachment;
    }
    
    public function getPaymentMethod()
    {
        return $this->paymentMethods[$this->payment_method] ?? "Cash";
    }
    
    public function isFromProcurement()
    {
        return $this->type == 'credit';
    }
    
    public function isFromBilling()
    {
        return $this->type == 'debit';
    }
    
    public function addAccountingServices($accountingServices, $admFeePercentage = 0, $itineraryId = null)
    {
        if ($itineraryId != null) {
            $this->addItineraryProviderServiceParent($itineraryId, $admFeePercentage);

            $this->addItineraryProviderService($itineraryId, $admFeePercentage);
        }
        
        $this->addAccountingItems($accountingServices['permits'] ?? [], new AircraftPermission());
        $this->addPermitAdministrationFeeAccountingItems(
            $accountingServices['permitsAdministrationFees'] ?? [], $admFeePercentage);
    }

    protected function addItineraryProviderServiceParent($itineraryId, $admFeePercentage = 0)
    {
        $itinerary = Itinerary::findOrFail($itineraryId);

        foreach ($itinerary->getBillingCheckedServicesParents as $service) {
            $service->createAccountingService($this);

            if ($service->adm_fee_checked == 1) {
                $admFeeAccountable = $service->createAdministrationFee($admFeePercentage);
                $admFeeAccountable->createAccountingService($this);
            }
        }
    }

    protected function addItineraryProviderService($itineraryId, $admFeePercentage = 0)
    {

        $itinerary = Itinerary::findOrFail($itineraryId);

        foreach ($itinerary->getBillingCheckedServices as $service) {
            $service->createAccountingService($this);
            
            if ($service->adm_fee_checked == 1) {
                $admFeeAccountable = $service->createAdministrationFee($admFeePercentage);
                $admFeeAccountable->createAccountingService($this);
            }
        }
    }
    
    
    private function addAccountingItems($itemIds, AccountableServiceInterface $class)
    {
        foreach ($itemIds as $id) {
            $item = $class::withoutGlobalScope(OVFScope::class)->find($id);
            $item->createAccountingService($this);
        }
    }
    
    private function addPermitAdministrationFeeAccountingItems($itemsIds, $admFeePercentage)
    {
        $admFeeItems = [];
        foreach ($itemsIds as $id) {
            $item = AircraftPermission::find($id);
            $admFeeItems = $item->createAdministrationFee($admFeePercentage);
            $admFeeItems->createAccountingService($this);
        }
    }
    
    public function addBillToAccountingService()
    {
        foreach ($this->accountingServices as $service)
        {
            $old = array_search($service['NomItem'], $this->accountingServices->toArray());
            $service->addBill($this->id);
        }
    }
    
    public function addInvoiceToAccountingService()
    {
        foreach ($this->accountingServices as $service) {
            $service->addInvoice();
        }
    }
    
    public function setCalculateAmount()
    {
        if ($this->isFromProcurement()) {
            $this->procurementAmount();
        } else {
            $this->billingAmount();
        }
        $this->save();
    }
    
    private function procurementAmount()
    {
        foreach ($this->accountingServices as $accountingService) {
            if ($accountingService->servicesable) {
                if ($accountingService->servicesable->parent == null) {
                    $this->amount += $accountingService->billing_final_price;
                }
            } else {
                $this->amount += $accountingService->billing_final_price;
            }
        }
    }
    
    private function billingAmount()
    {
        foreach ($this->accountingServices as $accountingService) {
            $this->amount += round($accountingService->billing_final_price, 2);
        }
    }
    
    public function deleteAccountingServices()
    {
        foreach ($this->accountingServices as $accountingService) {
            $accountingService->deleteItem();
        }
    }
    
    
    public function getStatus()
    {
        if ($this->status == 'invalidated') {
            return 'invalidated';
        }
        $canceled = '';
        if ($this->status == 'canceled') {
            $canceled = 'canceled /';
        }
        
        $balance = $this->getReminingPayment();
        
        if ($balance <= 0) {
            return $canceled.'paid';
        }
        if ($balance < $this->amount) {
            return $canceled.'partially_paid';
        }
        
        return $canceled.'not_paid';
    }
    
    public function deleteItem()
    {
        $this->deleteAccountingServices();
        return $this->delete();
        
    }
    
    public static function getLastItemOfTheDay()
    {
        $bill = Bill::lastBillOfTheDay()->whereNotNull('dollar_change')->first();
        return $bill != null ? $bill->dollar_change : null;
    }
    
    public function getDocumentoSFL()
    {
        $garinoGenerator = new GarinoSFLFileBill();
 
        return $garinoGenerator->generateEncode($this);
    }
    
    public function getTotalPaid()
    {
        $payments = $this->payments()->get()->sum('pivot.amount');
        $debitNotes = $this->creditNotes()->get()->sum('pivot.amount');
        
        return $payments + $debitNotes;
    }
    
    public function getReminingPayment()
    {
        return round($this->amount - $this->getTotalPaid(), 2);
    }
    
    public function cancelBill()
    {
        if ($this->status != 'Cancel') {
            foreach ($this->accountingServices as $accountingService) {
                $accountingService->removeBilledFromService();
            }
            $this->status = 'canceled';
            $this->save();
        }
    }
    public function invalidateBill()
    {
        if ($this->status != 'Invalidated') {
            foreach ($this->accountingServices as $accountingService) {
                $accountingService->removeBilledFromService();
            }
            $this->amount = 0;
            $this->status = 'invalidated';
            $this->save();
        }
    }
    
    //*** Report ****//
    public function getAccountableNumber()
    {
        return $this->bill_number;
    }
    public function getDebit()
    {
        return $this->amount;
    }

    public function getBalance()
    {
        if  ($this->balance === null) {
            throw new \Exception('Balance need to be setup.');
        }
        $this->balance -= $this->amount;
        return $this->balance;
    }

    public function showBalance()
    {
        return $this->balance;
    }


    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    
    
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
    
    public function company()
    {
        return $this->hasOne(Company::class, 'id', 'company_id');
    }
    
    public function billingCompany()
    {
        return $this->hasOne(Company::class, 'id', 'billing_company_id');
    }
    
    public function accountingServices()
    {
        return $this->hasMany(AccountingService::class, 'bill_id', 'id')->withoutGlobalScope(OVFScope::class);
    }
    
    public function additionals()
    {
        return $this->hasMany(Additional::class, 'bill_id', 'id')->withoutGlobalScope(OVFScope::class);
    }
    
    public function administrationFees()
    {
        return $this->hasOne(AccountingService::class, 'bill_id', 'id')
            ->where('servicesable_type', get_class(new AdministrationFee()));
    }
    
    public function permitsAdministrationFees()
    {
        return $this->hasOne(AccountingService::class, 'bill_id', 'id')
            ->where('servicesable_type', get_class(new AircraftPermission()));
    }
    
    public function permanentPermitsAdministrationFees()
    {
        return $this->hasOne(AccountingService::class, 'bill_id', 'id')
            ->where('servicesable_type', get_class(new PermanentPermission()));
    }
    
    public function operation()
    {
        return $this->hasOne(Operation::class, 'id', 'operation_id')->withTrashed();
    }
    
    public function aircraft()
    {
        return $this->hasOne(Aircraft::class, 'id', 'aircraft_id');
    }
    
    public function itinerary()
    {
        return $this->hasOne(Itinerary::class, 'id', 'itinerary_id');
    }
    
    public function getType($id)
    {
        return parent::$types[$id];
    }
    
    public static function getTotalCreditNoteAmount($bills)
    {
        $total = 0;
        foreach ($bills as $bill) {
            $total += $bill->pivot->amount;
        }
        return $total;
    }
    
    public function payments()
    {
        return $this->belongsToMany(Payment::class, 'bills_payments', 'bill_id', 'payment_id')
            ->withPivot('cfe_type', 'reason', 'amount', 'bill_type');
    }
    public function creditNotes()
    {
        return $this->belongsToMany(CreditNote::class, 'bills_payments', 'assigned_bill_id', 'payment_id')
            ->withPivot('cfe_type', 'reason', 'amount', 'bill_type');
    }

    public function getServicesString()
    {
        $services = collect();
        $companyName = $this->company ? $this->company->name : 'N/A';
        foreach ($this->accountingServices as $service) {
            $services->push(str_replace($companyName.',','', $service->description));
        }
        return implode(', ', $services->toArray());
    }
    
    
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    
    public function scopeLastBillOfTheDay($q)
    {
        return $q->where('created_at', '>=', Carbon::today())->latest();
    }
    
    public function scopeIsNotPaid(Builder $query)
    {
        return $query
            ->where('is_active', 1)
            ->where('status', '<>', 'paid');
    }
    
    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */
    public function getBillingDaysToPayAttribute()
    {
        return $this->billingCompany->billing_days_receptor ?? null;
    }
    
    public function getFltDate()
    {
        if($this->operation && $this->operation->start_date) {
            return Carbon::createFromFormat('Y-m-d H:i:s', $this->operation->start_date);
        }
        return null;
    }
    
    public function getRemainingAmountAttribute()
    {
        return $this->getReminingPayment();
    }
    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    public function setAttachmentAttribute($value)
    {
        $attribute_name = "attachment";
        $disk = "local";
        $destination_path = "bills";
        
        $this->uploadMultipleFilesToDisk($value, $attribute_name, $disk, $destination_path);
    }
    
    
    /*
    |--------------------------------------------------------------------------
    | HTML
    |--------------------------------------------------------------------------
    */
    
    
    
    
}
