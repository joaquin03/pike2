<?php

namespace App\Models\Accounting;

use App\Models\Aircraft;
use App\Models\AircraftPermission;
use App\Models\Contracts\AccountableServiceInterface;
use App\Models\Contracts\AccountingItem;
use App\Models\Itinerary;
use App\Models\ItineraryProviderService;
use App\Models\Operation;
use App\Models\PermanentPermission;
use App\Models\Scopes\OVFScope;
use App\Models\ServiceParent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Backpack\CRUD\CrudTrait;
use App\Models\Company;
use App\Models\User;
use App\Models\Scopes\InvoiceScope;
use Carbon\Carbon;



class Invoice extends AccountingItem
{
    use SoftDeletes, CrudTrait;
    protected $calculatedAmount = 0;

    public static function boot()
    {
        parent::boot();

        static::addGlobalScope(new InvoiceScope());

    }


    protected $table = 'bills';

    protected $dates = [
        'start_date', 'due_date',
    ];

    protected $fillable = [
        'bill_number', 'itinerary_id', 'operation_id', 'company_id', 'user_id', 'amount', 'type', 'date', 'due_date',
        'status', 'note', 'attachment', 'created_at', 'updated_at', 'is_active', 'dollar_change', 'start_date',
        'payment_method'
    ];

    protected $with = ['payments', 'creditNotes'];

    protected $appends = ['remaining_amount'];

    //protected $table = 'invoices';
    //protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function isFromProcurement()
    {
        return $this->type == 'credit';
    }

    public function isFromBilling()
    {
        return $this->type == 'debit';
    }


    public function addAccountingServices($accountingServices, $admFeePercentage = 0, $itineraryId = null)
    {
        $this->addAccountingItems($accountingServices['services'] ?? [], new ItineraryProviderService());

        $this->addAccountingItems($accountingServices['permits'] ?? [], new AircraftPermission());

    }


    private function addAccountingItems($itemIds, AccountableServiceInterface $class)
    {
        foreach ($itemIds as $id)
        {
            $item = $class::withoutGlobalScope(OVFScope::class)->find($id);
            $item->createAccountingService($this);
        }
    }


    public function addBillToAccountingService()
    {
        foreach ($this->accountingServices as $service){
            $service->addBill($this->id);
        }
    }

    public function addInvoiceToAccountingService()
    {
        foreach ($this->accountingServices as $service){
            $service->addInvoice();
        }
    }

    public function setCalculateAmount()
    {
        if ($this->isFromProcurement()) {
            $this->procurementAmount();
        } else {
            $this->invoiceAmount();
        }
        $this->save();
    }

    private function procurementAmount()
    {

        $amount = 0;
        foreach ($this->accountingServices as $accountingService) {
            if($accountingService->isItineraryService()){
                if($accountingService->servicesable->parent!=null){
                    $amount += $accountingService->getServiceFinalCost();
                }
            }
           else{
                $amount += $accountingService->getServiceFinalCost();
            }
        }
        $this->amount = round($amount,3);
    }

    private function invoiceAmount()
    {
        foreach ($this->accountingServices as $accountingService) {
            $this->amount += $accountingService->procurementAmount();
        }
        $this->amount = round($this->amount, 3);

    }

    public function deleteAccountingServices()
    {
        foreach($this->accountingServices as $accountingService){
            $accountingService->deleteItem();
        }
    }



    public function deleteItem()
    {
        $this->deleteAccountingServices();
        return $this->delete();

    }

    public static function getLastItemOfTheDay()
    {
        $invoice = Invoice::lastInvoiceOfTheDay()->whereNotNull('dollar_change')->first();
        return $invoice != null ? $invoice->dollar_change : null;
    }


    public static function getTotalCreditNoteAmount($bills)
    {
        $total=0;
        foreach ($bills as $bill){
            $total += $bill->pivot->amount;
        }
        return round($total,3);
    }

    public function validateIfExistSameInvoice()
    {
        $oldInvoice = $this->getSameInvoice();

        if (is_null($oldInvoice)) {
            return $this;
        }
        $this->is_active = 0;
        $this->save();

        $this->joinWithOldInvoice($oldInvoice);

        return $oldInvoice;
    }

    private function joinWithOldInvoice(Invoice $oldInvoice)
    {
        foreach ($this->accountingServices as $accountingService) {
            $accountingService->bill_id = $oldInvoice->id;
            $accountingService->save();
        }
        $oldInvoice->procurementAmount();
        $oldInvoice->save();
    }

    private function getSameInvoice()
    {
        return self::where('bill_number', $this->bill_number)->whereNotNull('bill_number')
            ->where('company_id', $this->company_id)->where('operation_id', $this->operation_id)->where('is_active', 1)
            ->where('id', '<>', $this->id)->first();
    }


    public function getType($id)
    {
        return  parent::$types[$id];
    }

    public function getServicesString()
    {
        $services = collect();
        $companyName = $this->company ? $this->company->name : 'N/A';
        foreach ($this->accountingServices as $service) {
            $services->push(str_replace($companyName.',','', $service->description));
        }
        return implode(', ', $services->toArray());
    }



    public function payments()
    {
        return $this->belongsToMany(Payment::class, 'bills_payments', 'bill_id', 'payment_id')
            ->withPivot('cfe_type', 'reason', 'amount', 'bill_type');
    }
    public function creditNotes()
    {
        return $this->belongsToMany(CreditNote::class, 'bills_payments', 'bill_id', 'payment_id')
            ->withPivot('cfe_type', 'reason', 'amount', 'bill_type');
    }


    public function getTotalPaid()
    {
        $payments = round($this->payments()->get()->sum('pivot.amount'),3);
        $debitNotes = round($this->creditNotes()->get()->sum('pivot.amount'),3);

        return round($payments + $debitNotes, 3);
    }

    public function getReminingPayment()
    {
        return round(round($this->amount,3) - $this->getTotalPaid(), 3);
    }

    public function getStatus(){

        $balance = $this->getReminingPayment();

        if ($balance <= 0) {
            return 'paid';
        }
        if ($balance < round($this->amount,3)) {
            return 'partially_paid';
        }

        return 'not_paid';
    }




    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function company()
    {
        return $this->hasOne(Company::class, 'id', 'company_id');
    }

    public function accountingServices()
    {
        return $this->hasMany(AccountingService::class, 'bill_id', 'id')->withoutGlobalScope(OVFScope::class);
    }

    public function additionals()
    {
        return $this->hasMany(Additional::class, 'bill_id', 'id')->withoutGlobalScope(OVFScope::class);
    }

    public function administrationFees()
    {
        return $this->hasOne(AccountingService::class, 'bill_id', 'id')
            ->where('servicesable_type', get_class(new AdministrationFee()));
    }

    public function permitsAdministrationFees()
    {
        return $this->hasOne(AccountingService::class, 'bill_id', 'id')
            ->where('servicesable_type', get_class(new AircraftPermission()));
    }

    public function permanentPermitsAdministrationFees()
    {
        return $this->hasOne(AccountingService::class, 'bill_id', 'id')
            ->where('servicesable_type', get_class(new PermanentPermission()));
    }

    public function operation()
    {
        return $this->hasOne(Operation::class, 'id', 'operation_id')->withTrashed();
    }

    public function aircraft()
    {
        return $this->hasOne(Aircraft::class, 'id', 'aircraft_id');
    }

    public function itinerary()
    {
        return $this->hasOne(Itinerary::class, 'id', 'itinerary_id');
    }


    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    public function scopeLastInvoiceOfTheDay($q)
    {
        return $q->where('created_at', '>=', Carbon::today())->latest();
    }

    public function scopeIsNotPaid(Builder $query)
    {
        return $query->where('status', '<>', 'paid');
    }


    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    public function getBillingDaysToPayAttribute()
    {
        return $this->company()->first()->invoice_days_receptor ?? 10;
    }


    public function getFltDate()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->operation->start_date ?? $this->operation->created_at);
    }

    public function getRemainingAmountAttribute()
    {
        return $this->getReminingPayment();
    }


    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    public function setAttachmentAttribute($value)
    {
        $attribute_name = "attachment";
        $disk = "local";
        $destination_path = "invoices";

        $this->uploadMultipleFilesToDisk($value, $attribute_name, $disk, $destination_path);
    }

    public function setDatetimeAttribute($value)
    {
        $this->attributes['due_date'] = \Date::parse($value);
    }

    /*
    |--------------------------------------------------------------------------
    | REPORT
    |--------------------------------------------------------------------------
    */
    public function getCredit()
    {
        if ($this->isFromProcurement()) {
            return '';
        }
        return $this->amount;
    }
    public function getDebit()
    {
        if ($this->isFromProcurement()) {
            return $this->amount;
        }
        return '';
    }

    public function getAccountableNumber()
    {
        return $this->bill_number;
    }


    public function getBalance()
    {
        if  ($this->balance === null) {
            throw new \Exception('Balance need to be setup.');
        }
        $this->balance -= $this->amount;
        return $this->balance;
    }

    public function showBalance()
    {
        return $this->balance;
    }

}
