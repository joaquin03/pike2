<?php
/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 4/11/19
 * Time: 11:19 AM
 */

namespace App\Models\Accounting;

use App\Models\Accounting\Bill;
use App\Models\Accounting\CreditNote;
use App\Models\Accounting\DebitNote;
use App\Models\Accounting\Payment;
use App\Models\Contracts\AccountingItem;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class ProviderStatementReport extends AccountingItem
{
    protected $filters = [];
    protected $request;

    public function getItems(Request $request)
    {
        $this->request = $request;

        $items = new Collection();
        $items = $items->merge($this->bills());
        $items = $items->merge($this->payments());
        $items = $items->merge($this->creditNote());
        //$items = $items->merge($this->debitNote());

        $items = $items->sortBy('date');

        return $items;
    }


    private function bills()
    {
        return $this->setFilters(Invoice::isActive())->get();
    }

    private function payments()
    {
        return $this->setFilters(Payment::where('type', 'debit'))->get();
    }

    private function creditNote()
    {
        return $this->setFilters(CreditNote::where('type', 'debit'))->get();
    }




    private function setFilters($query) :Builder
    {
        $query = $this->dateFilter($query);
        $query = $this->companyFilter($query);
        $query = $this->typeFilter($query);


        return $query;
    }

    private function typeFilter($query) :Builder
    {
        if ($this->request->has('types') && $this->request->get('types') != "null") {
            $className = (new \ReflectionClass($query->getModel()))->getShortName();
            $types = json_decode($this->request->get('types'))?? [];

            if (! in_array($className, $types)) {
                $query->where('id', false);
            }
        }
        return $query;
    }

    private function dateFilter($query) :Builder
    {
        if ($this->request->has('date')) {
            $date = json_decode( $this->request->get('date'));

            $query->where('date', '>=', Carbon::createFromFormat('d-m-Y', $date->from));
            $query->where('date', '<=', Carbon::createFromFormat('d-m-Y', $date->to));
        }

        return $query;
    }

    private function companyFilter($query) :Builder
    {

        if ($this->request->has('company_id') && $this->request->get('company_id') != "null") {
            $query->whereIn('company_id', json_decode($this->request->get('company_id')) ?? []);
        }

        return $query;
    }

    public function setBalance($value, $request)
    {
        $this->balance = $value;
    }

    public function getBalance(){
        return 0;
    }


}