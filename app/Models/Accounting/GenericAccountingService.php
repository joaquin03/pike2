<?php
/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 4/8/19
 * Time: 12:10 PM
 */

namespace App\Models\Accounting;


use App\Models\Contracts\AbstractOperationService;
use App\Models\Contracts\AccountableServiceInterface;
use App\Models\Contracts\AccountingItem;
use App\Models\Traits\AccountableServiceTrait;
use Illuminate\Database\Eloquent\Model;

class GenericAccountingService extends Model implements AccountableServiceInterface
{
    
    use AccountableServiceTrait;
    
    protected $table = 'generic_accounting_service';
    
    
    public function getFinalCostQuotation()
    {
        return 0;
    }
    /// Procurement
    public function getProcurementDescription()
    {
        return $this->name;
    }
    public function getProcurementQuantity()
    {
        return $this->quantity;
    }
    public function getProcurementCost()
    {
        return 0;
    }
    public function getProcurementAdmFee()
    {
        return 0;
    }
    public function getProcurementTax()
    {
        return 0;
    }
    public function getProcurementUnitFinalCost()
    {
        return 0;
    }
    public function getProcurementFinalCost()
    {
        return 0;
    }
    
    /// Billing
    public function getBillingDescription()
    {
        return $this->name;
    }
    public function getBillingQuantity()
    {
        return $this->quantity;
    }
    public function getBillingPercentage()
    {
        return 0;
    }
    public function getBillingUnitPrice()
    {
        return $this->amount;
    }
    public function getBillingFinalPrice()
    {
        return round($this->quantity * $this->amount, 2);
    }
    
    public function getProfitPrice()
    {
        return $this->getBillingFinalPrice();
    }
    
    // Helpers
    public function addBill($billId)
    {
    }
    
    public function addInvoice()
    {
    }
    
    public function isContainedInStatement($type)
    {
    }
    
    public function removeBill()
    {
    }
}