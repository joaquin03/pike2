<?php
namespace App\Models\Accounting;

use App\Models\Client;
use App\Models\Contracts\CompanyStatementInterface;
use App\Models\Scopes\BillScope;
use App\Models\Scopes\NotDebitNoteScope;
use Illuminate\Support\Collection;

class CompanyStatement extends Client implements CompanyStatementInterface
{
    /*
   |--------------------------------------------------------------------------
   | GLOBAL VARIABLES
   |--------------------------------------------------------------------------
   *

   /*
   |--------------------------------------------------------------------------
   | FUNCTIONS
   |--------------------------------------------------------------------------
   */
    public function getAccountingItems()
    {
        $items = new Collection();
        foreach ($this->bills as $bill) {
            $items->push($bill);
        }
        foreach ($this->payments as $payment) {
            $items->push($payment);
        }
        foreach ($this->creditNotes as $payment) {
            $items->push($payment);
        }
        foreach ($this->debitNote as $debitNote) {
            $items->push($debitNote);
        }

        $items = $items->sortBy('date');
        return $items;
    }
    
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function bills()
    {
        return $this->hasMany(Bill::class, 'billing_company_id', 'id')
            ->isActive()->where('type', 'debit');
    }
    
    public function payments()
    {
        return $this->hasMany(Payment::class, 'company_id', 'id')->where('type', 'credit');
    }
    
    public function creditNotes()
    {
        return $this->hasMany(CreditNote::class, 'company_id', 'id')->where('type', 'credit');
    }
    
    public function debitNote()
    {
        return $this->hasMany(DebitNote::class, 'billing_company_id', 'id')->withoutGlobalScope(new NotDebitNoteScope());
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    
    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */
    
    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}