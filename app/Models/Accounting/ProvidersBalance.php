<?php
/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 4/11/19
 * Time: 11:19 AM
 */

namespace App\Models\Accounting;


use App\Models\Accounting\Bill;
use App\Models\Accounting\CreditNote;
use App\Models\Accounting\DebitNote;
use App\Models\Accounting\Payment;
use App\Models\Company;
use App\Models\Contracts\AccountingItem;
use App\Models\Provider;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class ProvidersBalance extends AccountingItem
{
    protected $filters = [];
    protected $request;

    public function getItems(Request $request)
    {
        $this->request = $request;
        $items = new Collection();
        $items = $items->merge($this->providers());
        $items = $items->sortBy('date');
        $items = $this->setBalance($items, $request);

        return $items;
    }


    private function providers()
    {
        return $this->setFilters(Provider::where('deleted_at', null))->get();
    }


    private function setFilters($query): Builder
    {
        $query = $this->companyFilter($query);

        return $query;
    }


    private function companyFilter($query): Builder
    {
        if ($this->request->has('company_id') && $this->request->get('company_id') != "null") {
            $query->where('id', $this->request->get('company_id'));
        }
        return $query;
    }


    public function setBalance($items, $request)
    {
        foreach ($items as $provider) {
            $provider->setBalanceToDate($request->date);

        }

        return $items;
    }

    public function getBalance()
    {
        return null;
    }

}