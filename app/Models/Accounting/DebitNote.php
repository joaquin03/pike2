<?php
/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 4/8/19
 * Time: 10:12 AM
 */

namespace App\Models\Accounting;


use App\Helpers\Garino\GarinoSFLFileDebitNote;
use App\Helpers\GarinoGenerator;
use App\Models\Contracts\AccountableServiceInterface;
use App\Models\Contracts\AccountingItem;
use App\Models\ItineraryProviderService;
use App\Models\Scopes\BillScope;
use App\Models\Scopes\DebitNoteScope;
use App\Models\Scopes\NotDebitNoteScope;
use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class DebitNote extends Bill
{
    use SoftDeletes, CrudTrait;

    protected $fillable = [
        'bill_number', 'itinerary_id', 'operation_id', 'company_id', 'user_id', 'amount', 'type', 'date', 'due_date',
        'status', 'note', 'attachment', 'created_at', 'updated_at', 'is_active', 'dollar_change', 'bill_link',
        'payment_method', 'billing_company_id', 'ref_number', 'is_debit_note'
    ];

    public static function boot()
    {
        static::addGlobalScope(new DebitNoteScope());

        static::created(function($model){
            $model->bill_number ='Pike-DN-'.str_pad($model->id, 6, 0, STR_PAD_LEFT);
            $model->save();
        });
    }


    public function addAccountingServices($items, $admFeePercentage = 0, $itineraryId = NULL)
    {
        $this->addItineraryProviderServiceParent($this->itinerary_id, $admFeePercentage);

        $this->addItineraryProviderService($this->itinerary_id, $admFeePercentage);
    }


    public function getDocumentoSFL()
    {
        $garinoGenerator = new GarinoSFLFileDebitNote();

        return $garinoGenerator->generateEncode($this);
    }

    protected static function storeDebitNote($request)
    {
        $debitNote = DebitNote::withoutGlobalScopes()->find($request->get('bill_id'));
        if (!$debitNote){
            $debitNote = DebitNote::findOrFail($request->get('bill_id'));
        }
        $debitNote->is_debit_note = 1;

        $data = GarinoGenerator::generate($debitNote);
        //devuelve el string del rechazo


        $request->request->set('is_active', 1);
        // your additional operations before save here

        $debitNote->addBillToAccountingService();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry

        $debitNote->bill_number = 'DN-'.$data['bill_number'];
        $debitNote->bill_link = $data['url'];


        $debitNote->save();

        return $debitNote;
    }

}