<?php

namespace App\Models\Accounting;

use App\Models\Contracts\AccountingItem;
use App\Models\CreditCard;
use App\Models\Scopes\NotDebitNoteScope;
use App\Models\Scopes\PaymentScope;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Backpack\CRUD\CrudTrait;

use App\Models\Company;

class Payment extends AccountingItem
{
    use CrudTrait, SoftDeletes;
    /*
   |--------------------------------------------------------------------------
   | GLOBAL VARIABLES
   |--------------------------------------------------------------------------
   */
    protected $table = 'payments';

    protected $casts = [
        'attachment' => 'array'
    ];

    protected $fillable = [
        'credit_note_number', 'company_id', 'payment_number', 'user_id', 'amount', 'type', 'date', 'note', 'attachment',
        'dollar_change', 'bill_link', 'created_at', 'updated_at', 'payment', 'is_credit_note', 'payment_method',
        'amount', 'is_over_payment'
    ];

    public $reasons = [
        "Refund", "Discount", "Price error", "Price increase", "Price decrease", "Customer short payment"
    ];

    protected $dates = ['deleted_at'];


    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new PaymentScope);
    }


    public function deleteItem()
    {
        $this->bills()->detach();
        return $this->delete();
    }

    public function getDocumentoSFL()
    {
        return;
    }

    public function getDescription()
    {
        // if($this->payment_method == 'offset') {
        //     return 'O. Payment';
        // }
        if ($this->is_credit_note) {
            return 'CreditNote';
        }
        return 'Payment';
    }


    public function addBills($bills, $type)
    {
        $amount = 0;
        foreach ($bills as $bill) {
            $this->bills()->attach($bill['bill_id'],
                [
                    'cfe_type' => $bill['cfe_type'] ?? '',
                    'reason' => $bill['reason'] ?? '',
                    'amount' => $bill['amount'],
                    'bill_type' => $type
                ]
            );

            $amount += $bill['amount'];
        }
        if ($amount != 0) {
            $this->amount = $amount;
        }
        $this->save();
    }

    public function removeBills($bills)
    {
        foreach ($bills as $bill) {
            $this->bills()->detach($bill['bill_id']);
            $this->amount -= $bill['amount'];
        }
    }


    public function totalBills()
    {
        $total = 0;
        foreach ($this->invoices as $bill) {
            $total += $bill->pivot->amount;
        }

        foreach ($this->billsAndDebitNotes as $bill) {
            if ($bill->pivot->assigned_bill_id != -1) {
                $total += $bill->pivot->amount;
            }
        }

        return $total;
    }

    public function setAmount()
    {
        if ($this->totalBills() != 0) {
            $this->amount = $this->totalBills();
        }
    }


    public function getStatus()
    {
        $billsAmount = $this->totalBills();
        $balance = $this->amount - $billsAmount;

        if ($balance > 0) {
            return 'extra';
        }
        return 'paid';

    }

    public function setStatus()
    {
        $this->status = $this->getStatus();
    }


    //***  REPORT  ***//

    public function getAccountableNumber()
    {
        return $this->payment_number;
    }

    public function getCredit()
    {
        return $this->amount;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function bills()
    {
        return $this->belongsToMany(Bill::class, 'bills_payments', 'payment_id', 'bill_id')
            ->withPivot('cfe_type', 'reason', 'amount', 'bill_type', 'assigned_bill_id');
    }

    public function billsAndDebitNotes()
    {
        return $this->belongsToMany(Bill::class, 'bills_payments', 'payment_id', 'bill_id')
            ->withPivot('cfe_type', 'reason', 'amount', 'bill_type', 'assigned_bill_id')->withoutGlobalScope(new NotDebitNoteScope());
    }

    public function invoices()
    {
        return $this->belongsToMany(Invoice::class, 'bills_payments', 'payment_id', 'bill_id')
            ->withPivot('cfe_type', 'reason', 'amount', 'bill_type');
    }

    public function company()
    {
        return $this->hasOne(Company::class, 'id', 'company_id');
    }


    public function billingCompany()
    {
        return $this->company();
    }

    public function getBalance()
    {
        if  ($this->balance === null) {
            throw new \Exception('Balance need to be setup. '.$this->id);
        }
        $this->balance += $this->amount;
        return $this->balance;
    }

    public function showBalance()
    {
        return $this->balance;
    }



    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */


    /*
    |--------------------------------------------------------------------------
    | METHODS
    |--------------------------------------------------------------------------
    */

    public static function getPaymentMethods()
    {

        $paymentTypes = ['cash' => 'CASH', 'santander' => 'SANTANDER', 'bandes' => 'BANDES', 'banistmo' => 'BANISTMO', 'offset' => 'OFFSET'];

        foreach (CreditCard::all()->pluck('number') as $creditCard) {

            if (!isset($paymentTypes[$creditCard])) {
                $paymentTypes[$creditCard] = $creditCard;
            }
        }
        return $paymentTypes;
    }


    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */
    public function getObservationsAttribute()
    {
        $observationsText = '';
        $bills = $this->billsAndDebitNotes;
        $invoices = false;
        if ($bills->isEmpty()) {
            $bills = $this->invoices;
            $invoices = true;
        }
        foreach ($bills as $bill) {
            if ($invoices) {
                $originalBill = Invoice::findOrFail($bill->pivot->bill_id);
            }
            else{
                if ($bill->is_debit_note){
                    $originalBill = DebitNote::findOrFail($bill->pivot->bill_id);
                }
                else{
                    $originalBill = Bill    ::findOrFail($bill->pivot->bill_id);
                }
            }
            $assignedBill = null;

            $observationsText = $observationsText . $originalBill->bill_number;

            if ($bill->pivot->assigned_bill_id === -1) {
                $observationsText = $observationsText . ' pending assignment';
            } else {
                if ($bill->pivot->assigned_bill_id) {

                    if ($bill->is_debit_note){
                        $assignedBill = DebitNote::findOrFail($bill->pivot->assigned_bill_id);
                    }
                    else{
                        $assignedBill = Bill::findOrFail($bill->pivot->assigned_bill_id);
                    }

                    if ($originalBill != $assignedBill) {
                        $observationsText = $observationsText . '  assigned to ' . $assignedBill->bill_number;
                    }
                }
            }

            $observationsText = $observationsText . ' /';
        }
        $observationsText = substr($observationsText, 0, -1);

        if ($this->note) {
            if ($bills) {
                $observationsText = $observationsText . '. ' . $this->note;
            } else {
                $observationsText = $this->note;
            }
        }
        return $observationsText;
    }

    public function getPaymentMethodAttribute()
    {
        if (is_numeric($this->attributes['payment_method']) && $this->attributes['payment_method'] >= 0 && $this->attributes['payment_method'] < 10) {
            return CreditCard::findOrFail($this->attributes['payment_method'])->number;
        }
        return $this->attributes['payment_method'];
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

    public function setAttachmentAttribute($value)
    {
        $attribute_name = "attachment";
        $disk = "local";
        $destination_path = "bills";

        $this->uploadMultipleFilesToDisk($value, $attribute_name, $disk, $destination_path);

    }

}
