<?php
namespace App\Models\Accounting;

use App\Models\Client;
use App\Models\Contracts\CompanyStatementInterface;
use App\Models\Provider;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProviderStatement extends Provider implements CompanyStatementInterface
{
    use SoftDeletes;
    
    public function getAccountingItems()
    {
        $items = new Collection();
        foreach ($this->bills as $bill) {
            $items->push($bill);
        }
        foreach ($this->payments as $payment) {
            $items->push($payment);
        }
        foreach ($this->creditNotes as $payment) {
            $items->push($payment);
        }
        return $items;
    }
   
    public function bills()
    {
        return $this->hasMany(Invoice::class, 'company_id', 'id')->where('type', 'credit')->isActive();
    }
    
    public function payments()
    {
        return $this->hasMany(Payment::class, 'company_id', 'id')->where('type', 'debit');
    }
    
    public function creditNotes()
    {
        return $this->hasMany(CreditNote::class, 'company_id', 'id')->where('type', 'debit');
    }
   
}