<?php

namespace App\Models\Accounting;

use App\Helpers\GarinoGenerator;
use App\Helpers\Garino\GarinoSFLFileCreditNote;
use App\Models\Scopes\CreditNoteScope;
use App\Models\Scopes\PaymentScope;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use App\Models\Company;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;
use mysql_xdevapi\Collection;
use RuntimeException;

class CreditNote extends Payment
{
    use SoftDeletes, CrudTrait;

    protected $dates = ['deleted_at'];

    protected static function boot()
    {
        static::addGlobalScope(new CreditNoteScope);
    }
    /*
   |--------------------------------------------------------------------------
   | GLOBAL VARIABLES
   |--------------------------------------------------------------------------
   */

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function generateGarino($bills)
    {
        try {
            $data = GarinoGenerator::generate($this);
        } catch (\Exception $exception) {
            Bugsnag::notifyException(new RuntimeException($exception->getMessage()));
            $this->removeBills($bills);
            $this->delete();
            throw $exception;
        }

        $this->payment_number = $data['bill_number'];
        $this->bill_link = $data['url'];
        $this->save();
    }


    public function removeInvoices($bills)
    {
        foreach ($bills as $bill) {
            $this->invoices()->detach($bill['id']);
            $this->amount -= $bill['amount'];
        }
    }

    public function getDocumentoSFL()
    {
        $fileGenerator = new GarinoSFLFileCreditNote();
        //dd($fileGenerator->generate($this));
        return $fileGenerator->generateEncode($this);
    }

    public function deleteItem()
    {
        $this->removeBills($this->bills);
        $this->removeInvoices($this->invoices);
        return $this->delete();
    }


    public function reAssignBills($oldBills, $discounts, $assignBills)
    {
        $assignedBill = null;
        $bill = null;
        for ($i = 0; $i < count($oldBills); $i++) {
            $originalBill = $oldBills[$i];
            if ($discounts && array_key_exists($i, $discounts)) {
                $assignedBill = $originalBill;
            }
            else if ($assignBills[$i] != -1) {
                $assignedBill = $assignBills[$i];
            }

            //pivot de la bill actual
            $actualBill = $this->billsAndDebitNotes()->where('payment_id', $this->id)->where('bill_id', $originalBill)->get();
            if ($actualBill && $actualBill instanceof \Illuminate\Support\Collection && !$actualBill->isEmpty()) {
                //get the Pivot
                $bill = $actualBill[0]->pivot;
            }
            if ($bill) {
                $bill->assigned_bill_id = $assignedBill;
                $this->updateStatus($this->bills);
                $this->bills()->save($bill);
            }

        }
    }

    public function updateStatus($bills)
    {
        foreach ($bills as $bill) {
            $bill->status = $bill->getStatus();
            $bill->save();
        }
    }

    public function getBalance()
    {
        if ($this->balance === null) {
            throw new \Exception('Balance need to be setup.');
        }
        $this->balance += $this->amount;
        return $this->balance;
    }


    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */


    public function company()
    {
        return $this->hasOne(Company::class, 'id', 'company_id');
    }

    public function billingCompany()
    {
        return $this->hasOne(Company::class, 'id', 'company_id');
    }

    public function reAssignedBills()
    {
        return $this->belongsToMany(Bill::class, 'bills_payments', 'payment_id', 'bill_id')
            ->withPivot('cfe_type', 'reason', 'amount', 'bill_type', 'assigned_bill_id')
            ->where('bill_id', '!=', 'assigned_bill_id');
    }




    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */


    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

}
