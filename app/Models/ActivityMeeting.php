<?php

namespace App\Models;

use App\Models\Contracts\ActivityEvent;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class ActivityMeeting extends ActivityEvent
{
    use CrudTrait;
    
    /*
   |--------------------------------------------------------------------------
   | GLOBAL VARIABLES
   |--------------------------------------------------------------------------
   */
    
    protected $table = 'meetings';
    protected $fillable = [
        'name',
        'start_date',
        'start_time',
        'end_date',
        'end_time',
        'summary',
        'location'
    ];
    
    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function getTitle()
    {
        return "Meeting with " . $this->name;
    }
    
    public function getDescription()
    {
        return "";
    }
    
    public function getIcon()
    {
        return 'fa fa-users';
    }
    
    public function getColor()
    {
        return 'bg-red';
    }
    
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    
    
    public function allContacts()
    {
        return $this->belongsTo(Contact::class);
        
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    
    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */
    
    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    public function setStartDateAttribute($value)
    {
        $this->attributes['start_date'] = \Date::parse($value);
    }
    
    public function setEndDateAttribute($value)
    {
        $this->attributes['end_date'] = \Date::parse($value);
    }
}
