<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FlightPlan extends Model
{
    
    protected $fillable = [
        'flight_type', 'speed', 'level', 'route', 'icao_route', 'eet','provider_id'
    ];
    
    public static $flightLevels = [
        'S' => 'S', 'N' => 'N', 'G' => 'G', 'M' => 'M', 'X' => 'X'
    ];
    
    
    public function itinerary()
    {
        return $this->belongsTo(Itinerary::class, 'id', 'itinerary_id');
    }
    
    public function transformer()
    {
        return $this->toArray();
    }

    public function provider()
    {
        return $this->belongsTo(Provider::class);
    }
    
    
}
