<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class AircraftDefaultServicePrice extends Model
{
    use CrudTrait;

     /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    
    //protected $table = 'aircraft_default_service_prices';
    
    //protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = ['from','to','value', 'unit'];
    // protected $hidden = [];
    // protected $dates = [];
    
    public $table = 'aircraft_default_service_prices';
    
    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function getValue($aircraftWight)
    {
        $defaultValue = AircraftDefaultServicePrice::where('from', '>=', $aircraftWight)
                ->where('to', '<=', $aircraftWight)->first();
        if ($defaultValue != null) {
            return $defaultValue->value;
        }
        return 0;
    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
