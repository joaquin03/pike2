<?php

namespace App\Models;

use App\Models\Accounting\Bill;
use App\Models\Accounting\CreditNote;
use App\Models\Accounting\DebitNote;
use App\Models\Accounting\Invoice;
use App\Models\Accounting\Payment;
use App\Models\Contracts\ActivityEvent;
use App\Models\Scopes\ValidateCompanyUserRoleScope;
use App\Models\Scopes\ValidateContactUserRoleScope;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use App\Models\Utilities\Country;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;
use Venturecraft\Revisionable\RevisionableTrait;
use App\Models\Traits\CustomRevisionableTrait;

class Company extends Model implements Auditable
{
    use CrudTrait, RevisionableTrait, CustomRevisionableTrait, SoftDeletes;
    use \OwenIt\Auditing\Auditable;
    public $balance = 0;

    /*
   |--------------------------------------------------------------------------
   | GLOBAL VARIABLES
   |--------------------------------------------------------------------------
   */

    protected $table = 'companies';
    protected $fillable = [
        'name', 'country', 'website', 'business', 'payment_methods', 'is_client', 'is_operator', 'is_provider',
        'emails', 'phones', 'address', 'image', 'rut_cuit_vat', 'rut_cuit_vat_description', 'doc_type_receptor',
        'country_code_receptor', 'document_receptor', 'business_name_receptor', 'address_receptor', 'city_receptor',
        'state_receptor', 'postal_code_receptor', 'billing_days_receptor', 'tags', 'invoice_days_receptor',
        'billing_email', 'adm_fee', 'is_country_entity', 'creator_user_id', 'type_of_client', 'has_special_features',
        'special_features', 'branch_company_id', 'procurement_bank_info', 'billing_adm_fee', 'is_ww_provider',
        'zip_code', 'city',  'sales_manager_id', 'US_fuel_client_type',
    ];

    public static $paymentMethods = ['credit card', 'wire transfer', 'cash', 'prepaid'];
    public static $typeOfClient = ['' => '-', 'Bad' => 'Bad Payer - No Credit', 'Medium' => 'Check Before Give Credit', 'Good' => 'Good Payer'];
    public static $taxId = ['RUT', 'CUIT', 'Nro Fiscal'];

    protected $casts = [
        'payment_methods' => 'array',
        'emails' => 'array',
        'phones' => 'array',
        'address' => 'array',
    ];

    public static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ValidateCompanyUserRoleScope());

    }

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function getCountry()
    {
        return Country::getName($this->country);
    }

    public function lastActivityDate()
    {
        if (isset($this->updated_at)) {
            return $this->updated_at->format('d/m/Y H:i');
        }
    }

    public function getType()
    {
        $classname = strtolower(class_basename($this));

        if ($classname != 'company') {
            return $classname;
        }
        if ($this->is_client) {
            return 'client';
        }
        if ($this->is_operator) {
            return 'operator';
        }
        if ($this->is_provider) {
            return 'provider';
        }
        return false;
    }


    /* Activity */
    public function createActivity($event, $visibility = null, $request)
    {
        if ($event instanceof ActivityFile) {
            $request['date'] = Carbon::now('UTC');
        }

        $activity = new Activity([
            'visibility' => $visibility ?? 'public',
            'user_id' => \Auth::user()->id,
            'company_id' => $this->id,
            'date' => $request['date']
        ]);
        $activity->event()->associate($event);
        $activity->save();
        return $activity;
    }

    public function updateActivity($id, $eventType)
    {

    }


    public function hasQuotations()
    {
        return $this->quotations()->count() > 0;
    }

    public function hasActivitiesSpecialFeatures()
    {
        return $this->activitiesSpecialFeatures()->count() > 0 && $this->has_special_features == 1;
    }
    
    
    public function getAuditableDescription()
    {
        return $this->name;
    }

    public function getBalanceToDate($date)
    {
        return 0;
    }

    public function getPositiveAmountToDate($date)
    {
        return 0;
    }

    public function getNegativeAmountToDate($date)
    {
        return 0;
    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function contacts()
    {
        return $this->hasMany(Contact::class, 'company_id', 'id');
    }

    public function activities()
    {
        return $this->hasMany(Activity::class, 'company_id')->where('event_type', '<>', ActivityFile::class);
    }

    public function activitiesFile()
    {
        return $this->hasMany(Activity::class, 'company_id')->where('event_type', ActivityFile::class);
    }

    public function activitiesSpecialFeatures()
    {
        return $this->hasMany(Activity::class, 'company_id')->where('event_type', ActivitySpecialFeatures::class);
    }

    public function competitors()
    {
        return $this->belongsToMany(Company::class, 'competitors','client_id', 'competitor_id');
    }

    public function competitorsable()
    {
        return $this->belongsToMany(Company::class, 'competitors','competitor_id', 'client_id');

    }

    public function activityNotes()
    {
        return $this->hasMany(Activity::class, 'company_id')
            ->where('event_type', ActivityNote::class);
    }


    public function latestActivity()
    {
        return $this->hasOne(Activity::class, 'company_id')->latest();
    }

    public function showLink()
    {
        $url = route('crud.company.show', ['company' => $this->id]);
        return '<a class="btn btn-xs btn-info" href="' . $url . '" data-toggle="tooltip"><i class="fa fa-info"></i></a>';
    }

    public function editLink()
    {
        $url = route('crud.company.show', ['company' => $this->id]);
        return '<a class="btn btn-xs btn-default" href="' . $url . '#edit" data-toggle="tooltip"><i class="fa fa-pencil-square-o"></i>Edit</a>';
    }

    public function tags()
    {
        return $this->belongsToMany(CompanyTag::class, 'company_company_tags');
    }

    public function airports()
    {
        return $this->belongsToMany(Airport::class, 'provider_airports', 'provider_id', 'airport_icao')
            ->orderBy('provider_airports.score', 'ASC');
    }

    //Change to Client
    public function clientOperations()
    {
        return $this->hasMany(Operation::class, 'aircraft_client_id', 'id')
            ->with('aircraft')->orderBy('start_date', 'DESC');
    }

    public function quotations()
    {
        return $this->hasMany(Quotation::class, 'aircraft_client_id', 'id')
            ->with('aircraft')->orderBy('start_date', 'DESC');
    }

    public function branchCompany()
    {
        return $this->belongsTo(Company::class,  'branch_company_id', 'id');
    }

    public function salesManager()
    {
        return $this->hasOne(SalesManager::class, 'id', 'sales_manager_id');
    }

    /*------------- Providers Balance Report -------------*/

    public function providerInvoices()
    {
        return $this->hasMany(Invoice::class, 'company_id', 'id')->where('type', 'credit');
    }

    public function providerPayments()
    {
        return $this->hasMany(Payment::class, 'company_id', 'id')->where('type', 'debit');
    }

    public function providerCreditNotes()
    {
        return $this->hasMany(CreditNote::class, 'company_id', 'id')->where('type', 'debit');
    }

    /*------------- Clients Balance Report -------------*/

    public function clientBills()
    {
        return $this->hasMany(Bill::class, 'billing_company_id', 'id')->where('type', 'debit')
            ->where('is_active', true);
    }

    public function clientPayments()
    {
        return $this->hasMany(Payment::class, 'company_id', 'id')->where('type', 'credit');
    }

    public function clientCreditNotes()
    {
        return $this->hasMany(CreditNote::class, 'company_id', 'id')->where('type', 'credit');
    }

    public function clientDebitNotes()
    {
        return $this->hasMany(DebitNote::class, 'company_id', 'id');
    }

    public function prices()
    {
        return $this->hasMany(ProviderPrices::class);
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    
    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */
    //Name used for revisions
    public function identifiableName()
    {
        return $this->name;
    }
    
    public function getBusinessNameReceptorAttribute($businessName)
    {
        if ($businessName == '') {
            return $this->name;
        }
        return $businessName;
    }
    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    public function setImageAttribute($value)
    {
        $attribute_name = "image";
        $disk = "public";
        $destination_path = "companies";
        
        $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path);
    }
    
}
