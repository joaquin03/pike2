<?php

namespace App\Models;

use App\Models\Scopes\SalesManagerScope;
use App\Models\Utilities\Country;
use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;


class SalesManager extends Model
{
    use CrudTrait;

    protected $table = 'users';

    protected $fillable = [
        'name', 'email', 'password', 'countries', 'comments'
    ];
    protected $casts = [
        'countries' => 'array'
    ];

    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(new SalesManagerScope());

    }

    public function getCountries()
    {
        $aux = [];

        foreach ($this->countries as $country) {
            array_push($aux, Country::getName($country));
        }
        return implode($aux, ', ');
    }

    public function getCountriesKeys()
    {
        $aux = [];
        foreach ($this->countries as $country) {
            array_push($aux, Country::getCode($country));
        }
        return $aux;
    }

    public function userIsSalesManager()
    {
        return !empty($this->getCountriesKeys());
    }
}

