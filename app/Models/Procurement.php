<?php

namespace App\Models;


use App\Models\Accounting\Additional;
use App\Models\Accounting\Bill;
use App\Models\Accounting\Invoice;
use App\Models\Scopes\OperationScope;
use App\Models\Scopes\OVFScope;
use App\Models\Scopes\ProcurementScopes;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class Procurement extends Operation
{
    use SoftDeletes;
    
    public static function boot()
    {
        parent::boot();
    
        static::addGlobalScope(new OperationScope());
        static::addGlobalScope(new ProcurementScopes());
    }
    
    public function getProvidersPermits()
    {
        $permits = $this->aircraftPermission;
        $providers = Provider::whereIn('id', $permits->pluck('provider_id'))->get();
        
        return $this->transformProviderPermits($providers, $permits);
    }
    
    private function transformProviderPermits($providers, $permits)
    {
        $data = [];
        foreach ($providers as $index => $provider) {
            $data[$index]["id"] = $provider->id;
            $data[$index]["name"] = $provider->name;
            $data[$index]["country"] = $provider->country;
            foreach ($permits->where('provider_id', $provider->id) as $permit) {
                $data[$index]["permits"][] = $permit->toArray();
            }
            
        }
        return $data;
    }
    
    public function getProvidersPermanentPermits()
    {
        $permits = $this->permanentPermissions;
        $providers = Provider::whereIn('id', $permits->pluck('provider_id'))->get();
        
        return $this->transformProviderPermits($providers, $permits);
    }
    
    public function getTotal()
    {
        $service = $this->getServiceTotal();
        $aircraft_permissions = $this->getPermitsTotal();
        $permanent_permissions = $this->getPermanentPermitsTotal();
        $permitsAdditionals = $this->getAdditionals($this->permitsAdditionals());
        $serviceAdditionals = $this->getAdditionals($this->servicesAdditionals());
        $permanentPermitAdditionals = $this->getAdditionals($this->permanentPermitsAdditionals());
        $additionals = $serviceAdditionals + $permitsAdditionals + $permanentPermitAdditionals;

        return [
            'service'               => $service,
            'aircraft_permissions'  => $aircraft_permissions,
            'permanent_permissions' => $permanent_permissions,
            'additionals'           => $additionals,
            'total'                 => round($service,3) + round($aircraft_permissions,3) + round($permanent_permissions,3) + round($additionals,3)
        ];
    }
    
    public function getServiceTotal()
    {
        $itinerariesIds = $this->itineraries()->pluck('id');
        $services = ItineraryProviderService::whereNotNull('parent_id')
            ->whereIn('itinerary_id', $itinerariesIds)->get();
        return (double)$this->itinerariesProviderServicesFinalCosts($services);
    }
    
    public function getPermitsTotal()
    {
        return $this->aircraftPermissionFinalCosts();
    }
    
    public function getPermanentPermitsTotal()
    {
        return $this->permanentPermissionFinalCosts();
    }

    public function getAdditionals($additionals)
    {
        $items = $additionals->get();
        $finalCost = 0;

        foreach ($items as $item) {
            $finalCost += (($item->billing_unit_price)* $item->procurement_quantity * (1 + $item->tax));
        }
        return $finalCost;
    }
    
    public static function scopeSortByState(Builder $query)
    {
        return $query->orderByRaw('FIELD(procurement_status,"IN PROCESS","CANCELED", "COMPLETED") ASC')
            ->orderByRaw('FIELD(state,"COMPLETED","IN PROCESS", "CANCELED", "DELETED") ASC')
            ->orderByRaw('DATEDIFF(NOW(), start_date) ASC');

    }
    
    public function createInvoice($accountingServices = [], $providerId, $itineraryId = null, $type = 'credit', $note = null,
        $admFeePercentage = 0)
    {
        $invoice = new Invoice();
        $company = Company::find($providerId);
        //$bill->user_id = Auth::user()->id;
        $invoice->user_id = 1;
        $invoice->aircraft_id = $this->aircraft_id;
        $invoice->itinerary_id = $itineraryId;
        $invoice->company_id = $providerId;
        $invoice->billing_company_id = $invoice->company_id;
        $invoice->operation_id = $this->id;
        $invoice->date = Carbon::now('UTC');
        $invoice->due_date = Carbon::now()->addDays($invoice->billing_days_to_pay);

        $invoice->type = $type;
        $invoice->note = $note;
        $invoice->save();
        
        $invoice->addAccountingServices($accountingServices, $admFeePercentage, $itineraryId);
        $invoice->setCalculateAmount();

        return $invoice;
    }
    
    public function aircraftPermission()
    {
        return $this->hasMany(AircraftPermission::class, 'operation_id', 'id')
            ->withoutGlobalScope(OVFScope::class);
    }
    
    public function aircraftPermissionFinalCosts()
    {
        $finalCost = 0;
        $permissions = $this->hasMany(AircraftPermission::class, 'operation_id', 'id')
            ->withoutGlobalScope(OVFScope::class)->get();
        foreach ($permissions as $permission) {
            $finalCost += (($permission->cost) * (1 + $permission->tax));
        }
        return $finalCost;
    }
    
    public function permanentPermissions()
    {
        return $this->belongsToMany(PermanentPermission::class, 'operation_permanent_permission',
            'operation_id',
            'permanent_permission_id'
        
        )->withPivot('aircraft_pm_id', 'created_at', 'updated_at')
            ->withoutGlobalScope(OVFScope::class);
    }
    
    public function permanentPermissionFinalCosts()
    {
        $finalCost = 0;
        $permissions = $this->belongsToMany(PermanentPermission::class, 'operation_permanent_permission',
            'operation_id',
            'permanent_permission_id'
        
        )->withPivot('aircraft_pm_id', 'created_at', 'updated_at')
            ->withoutGlobalScope(OVFScope::class)->get();
        foreach ($permissions as $permission) {
            $finalCost += (($permission->cost) * (1 + $permission->tax));
        }
        return $finalCost;
    }
    
    public function permitsAdditionals()
    {
        return $this->hasMany(Additional::class, 'operation_id', 'id')
            ->withoutGlobalScope(OVFScope::class)
            ->where('servicesable_type', get_class(new AircraftPermission()));
    }

    public function permanentPermitsAdditionals()
    {
        return $this->hasMany(Additional::class, 'operation_id', 'id')
            ->withoutGlobalScope(OVFScope::class)
            ->where('servicesable_type', get_class(new PermanentPermission()));
    }


}
