<?php

namespace App\Models;

use App\Helpers\Fuel\AEGFuelLoader;
use App\Helpers\Fuel\EpicFuelLoader;
use App\Helpers\Fuel\FuelLoader;
use App\Helpers\Fuel\JetexFuelLoader;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class FuelPrices extends Model
{
    protected $table = 'fuel_prices';
    
    protected $fillable = [
        'icao', 'provider', 'min_quantity', 'price', 'date_effective', 'date_valid', 'notes', 'bat_number'
    ];
    
    public static $providers = [
        'AEG'       => AEGFuelLoader::class,
        'Jetex'     => JetexFuelLoader::class,
        'Epic Fuel'  => EpicFuelLoader::class,
    ];
    
    public $dates = ['updated_at'];
    
    public function airport()
    {
        return $this->belongsTo(Airport::class, 'icao', 'icao');
    }
    
    public static function getProviders()
    {
        return array_keys(self::$providers);
    }
    
    public static function getLastBatNumber($provider)
    {
        $last = self::where('provider', $provider)->orderBy('updated_at', 'desc')->first();
        if ($last == null) {
            return null;
        }
        return $last->bat_number;
    }
    
    public static function getLoaderClass($key) :FuelLoader
    {
        return new self::$providers[$key];
    }
    
    public static function getProvidersItems()
    {
        $ret = [];
        foreach (self::$providers as $provider => $class) {
            $ret[] = self::where('provider', $provider)
                        ->orderBy('updated_at', 'desc')->first();
        }

        return $ret;
        
    }
    
    public static function getIcaosItems()
    {
        return self::groupBy('icao')->select('icao');
    }
    
    public static function getLastBatNumberByProvider()
    {
        $ret = [];
        foreach (self::$providers as $key => $provider) {
            $ret[$key] = self::getLastBatNumber($key);
        }

        return $ret;
    }
    
    
    /*** SCOPES ***/
    public function scopeWhereIcao(Builder $query, $icao)
    {
        return $query->where('icao', $icao);
    }
    
    public function scopeWhereGallons(Builder $query, $gallons)
    {
        return $query->where('min_quantity', '>', $gallons)
                    ->where('max_quantity', '<', $gallons);
    }
    
    public function scopeOrderByPrice(Builder $query)
    {
        return $query->orderBy('price', 'ASC');
    }
    
    public function scopeLastBat(Builder $query)
    {
        $providers = self::getLastBatNumberByProvider();
        return $query->where(function($q) use ($providers){
            foreach ($providers as $provider=>$bat) {
                $q->orWhere(function($q2) use($provider, $bat) {
                    $q2->where('provider', $provider)->where('bat_number', $bat);
                });
            }
        });
    }
    
}
