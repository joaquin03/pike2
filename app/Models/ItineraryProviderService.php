<?php

namespace App\Models;

use App\Models\Accounting\AccountingService;
use App\Models\Accounting\Additional;
use App\Models\Accounting\AdministrationFee;
use App\Models\Accounting\Bill;
use App\Models\Contracts\AbstractAccountableService;
use App\Models\Contracts\AbstractOperationService;
use App\Models\Contracts\AccountableServiceInterface;
use App\Models\Traits\AccountableServiceTrait;
use Backpack\CRUD\CrudTrait;
use App\Models\Traits\CustomRevisionableTrait;
use App\Models\Traits\ItineraryProviderServiceRevisionableTrait;
use Config;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use OwenIt\Auditing\Contracts\Auditable;

class ItineraryProviderService extends Model implements AccountableServiceInterface, Auditable
{
    use CrudTrait, ItineraryProviderServiceRevisionableTrait, CustomRevisionableTrait;
    use \OwenIt\Auditing\Auditable;

    use AccountableServiceTrait {
        addBill as traitAddBill;
        removeBill as traitRemoveBill;
    }

    protected $fillable = [
        'itinerary_id', 'service_id', 'provider_id', 'operation_status', 'procurement_cost', 'procurement_quantity',
        'procurement_adm_fee', 'procurement_tax', 'procurement_status', 'billing_percentage', 'billing_quote_price',
        'billing_unit_price', 'billing_price', 'billing_final_price', 'has_bills', 'administration_fee', 'is_additional',
        'service_origin', 'billing_quantity', 'service_unit', 'billing_checked', 'adm_fee_checked', 'operation_quantity'
    ];

    protected $table = 'itinerary_provider_services';

    protected $statusVal = [
        1 => 'Done',
        2 => 'Requested',
        3 => 'Other'
    ];

    protected $revisionEnabled = true;

    protected $keepRevisionOf = array(
        'service_id', 'provider_id', 'operation_status'
    );

    protected $revisionFormattedFieldNames = array(
        'service' => 'Service',
        'provider' => 'Provider',
        'operation_status' => 'Status',
    );


    /*************
     * Relations *
     *************/

    public function providerService()
    {
        return $this->hasOne(ProviderService::class, 'id', 'service_provider_id');
    }

    public function itinerary()
    {
        return $this->hasOne(Itinerary::class, 'id', 'itinerary_id');
    }

    public function service()
    {
        return $this->hasOne(Service::class, 'id', 'service_id')->withTrashed();
    }

    public function provider()
    {
        return $this->hasOne(Provider::class, 'id', 'provider_id')->withTrashed();
    }

    public function administrationFee()
    {
        return $this->hasOne(AdministrationFee::class, 'id', 'servicesable_id');
    }

    public function children()
    {
        return $this->hasMany(ItineraryProviderService::class, 'parent_id', 'id');
    }

    public function parent()
    {
        return $this->hasOne(ItineraryProviderService::class, 'id', 'parent_id');
    }

    public function additionals()
    {
        return $this->hasMany(Additional::class, 'servicesable_id', 'id')
            ->where('servicesable_type', get_class(new ItineraryProviderService()));
    }

    /***************
     * Accountable
     ****************/
    //Procurement
    public function getProcurementDescription()
    {
        $description = $this->getProvider()->name . ", " . $this->getServiceName();
        if ($this->isAdditional()) {
            $description = $description . ' Additional';
        }
        return $description;
    }

    public function getProcurementQuantity()
    {
        if ($this->parent_id == null) {
            return 1;
        }
        return $this->procurement_quantity;
    }

    public function getProcurementCost()
    {
        return $this->procurement_cost;
    }

    public function getProcurementAdmFee()
    {
        return $this->procurement_adm_fee;
    }

    public function getProcurementTax()
    {
        return $this->procurement_tax;
    }

    public function getProcurementUnitFinalCost()
    {
        if ($this->parent_id == null) {
            return $this->getProcurementFinalUnitParent();
        }
        return $this->getProcurementFinalUnitChild();
    }

    public function getProcurementUnitFinalCostWithoutAdmFee()
    {
        if ($this->parent_id == null) {
            return $this->getProcurementFinalUnitParent();
        }
        return $this->getProcurementFinalUnitChildWithoutAdmFee();
    }

    public function getProcurementFinalCost()
    {
        return round($this->getProcurementUnitFinalCost() * $this->getProcurementQuantity(), 3);
    }
    public function getProcurementFinalCostWithoutAdmFee()
    {
        return round($this->getProcurementUnitFinalCostWithoutAdmFee() * $this->getProcurementQuantity(), 3);
    }


    private function getProcurementFinalUnitChild()
    {
        return $this->procurement_cost * (1 + ($this->procurement_adm_fee)) * (1 + $this->procurement_tax);
    }

    private function getProcurementFinalUnitChildWithoutAdmFee()
    {
        return $this->procurement_cost * (1 + $this->procurement_tax);
    }
    private function getProcurementFinalUnitParent()
    {
        $total = 0;
        foreach ($this->children as $child) {
            $total += $child->getProcurementFinalCost();
        }
        return $total;
    }

    //Billing
    public function getBillingDescription()
    {
        $description = $this->getService()->name;
        if ($this->isAdditional()) {
            $description .= ' Additional';
        }

        return $description;
    }

    public function getBillingQuantity()
    {
        return $this->billing_quantity ?? 1;
    }

    public function getBillingPercentage()
    {
        return $this->billing_percentage;
    }

    public function getBillingUnitPrice()
    {
        if (is_null($this->parent_id)) {
            return round($this->getBillingFinalUnitPriceParent(), 3);
        }

        return $this->getBillingFinalUnitPriceChild();
    }

    public function getBillingUnitPriceWithoutAdmFee()
    {
        if (is_null($this->parent_id)) {
            return round($this->getBillingFinalUnitPriceParent(), 3);
        }
        return round($this->getBillingFinalUnitPriceChildWithoutAdmFee(), 3);
    }

    public function getBillingFinalPrice()
    {
        return round($this->getBillingUnitPrice() * $this->getBillingQuantity(),3);
    }

    public function getQuotationFinalPrice()
    {
        return $this->getBillingUnitPrice() * $this->procurement_quantity * ( 1 + $this->administration_fee);
    }

    public function getBillingFinalPriceWithoutAdmFee()
    {
        return $this->getBillingUnitPriceWithoutAdmFee() * $this->getBillingQuantity();
    }


    private function getBillingFinalUnitPriceParent()
    {
        $total = 0;
        foreach ($this->children as $child) {
            $total += $child->getBillingFinalPrice();
        }
        return $total;
    }

    private function getBillingFinalUnitPriceChild()
    {
        $unitCost = $this->billing_unit_price;

        if ($this->billing_unit_price === null) {
            $unitCost = $this->getProcurementUnitFinalCost();
        }

        return $unitCost + $this->getPercentageValue($unitCost);
    }

    private function getBillingFinalUnitPriceChildWithoutAdmFee()
    {
        $unitCost = $this->billing_unit_price;

        if ($this->billing_unit_price === null) {
            $unitCost = $this->getProcurementUnitFinalCost();
        }

        return $unitCost + $this->getPercentageValue($unitCost);
    }

    private function getPercentageValue($finalCost)
    {
        if ($this->billing_percentage) {
            return $this->billing_percentage * $finalCost;
        }
        return 0;
    }

    //Extras
    public function getProfitPrice()
    {
        return round($this->getBillingFinalPrice() - $this->getProcurementFinalCost(), 3);
    }

    public function addBill($billId)
    {
        if ($this->parent_id == null) {
            foreach ($this->children as $child) {
                $child->addBill($billId);
            }
        }
        $this->traitAddBill($billId);
    }

    public function removeBill()
    {
        if ($this->parent_id == null) {
            foreach ($this->children as $child) {
                $child->removeBill();
            }
        }
        $this->traitRemoveBill();
    }

    //Quotation
    public function getFinalCostQuotationWithoutAdmFee()
    {
        if ($this->billing_unit_price != 0) {
            $finalCost = $this->billing_unit_price * $this->procurement_quantity;
        } else {
            $finalCost = $this->getBillingFinalPriceWithoutAdmFee();

        }

        return round($finalCost, 2);
    }



    public function getFinalCostQuotation()
    {
        $finalCost = 0 ;
        if (!$this->isAdditional()) {
            if ($this->billing_unit_price != 0) {
                if ($this->procurement_quantity != 0) {
                    $finalCost = $this->billing_unit_price * $this->procurement_quantity * (1 + $this->administration_fee);
                }
            } else {
                $finalCost = $this->getQuotationFinalPrice();
            }
            return round($finalCost, 2);
        }
        return 0;
    }


    public function getAdministrationFee()
    {
        return $this->getFinalCostQuotation() - $this->getFinalCostQuotationWithoutAdmFee();
    }


    /*************
     * Getters *
     *************/

    public function getProvider()
    {
        return $this->provider ?? null;
    }

    public function getService()
    {
        return $this->service ?? '';
    }

    public function getParentServiceName()
    {
        if ($this->parent) {
            return $this->parent->service->name;
        }
        return '';
    }

    public function getAdditionals()
    {
        return $this->hasMany(Additional::class, 'itinerary_id', 'id')
            ->where('servicesable_type', get_class(new ItineraryProviderService()))->get();
    }

    public function getCategoryOfServices()
    {
        return ServiceCategory::whereIn('id', $this->serviceCategoriesQuery()->pluck('services.category_id'))->get();
    }

    private function serviceCategoriesQuery()
    {
        return ServiceCategory::join('services', 'category_services.id', 'services.category_id')
            ->join('services_providers', 'services.id', 'services_providers.service_id')
            ->join('itinerary_provider_services', 'services_providers.id',
                'itinerary_provider_services.service_provider_id')
            ->where('itinerary_provider_services.itinerary_id', $this->id);

    }

    private function getServiceName()
    {
        if ($this->getParentServiceName() == $this->getService()->name) {
            return $this->getParentServiceName();
        } else {
            return $this->getParentServiceName() . " - " . $this->getService()->name;
        }

    }

    public function isAdditional()
    {
        return $this->is_additional === 1;
    }

    public function isFromPike()
    {
        return $this->provider_id == Config::get('constants.pikeProviderId');
    }

    public function getOperationQuantity()
    {
        if ($this->operation_quantity != null) {
            return " (O.Qty " . $this->operation_quantity . ")";
        }
    }


    /*************
     * ACTIONS *
     *************/


    public function createAdditional(Request $request)
    {

        if ($this->existAdditional()) {
            \Alert::warning('Additional already exists.')->flash();
        }

        $additional = new ItineraryProviderService();
        $additional->fill($this->toArray());
        $additional->fill($request->all());

        $itinerary = $additional->itinerary;
        $itinerary->addService($additional->toArray());
        return $additional;
    }

    private function existAdditional(): bool
    {
        return ItineraryProviderService::where('service_id', $this->service_id)
            ->where('itinerary_id', $this->itinerary_id)
            ->where('provider_id', $this->provider_id)
            ->where('is_additional', 1)->exists();
    }

    public function deleteItem()
    {
        if ($this->canBeDeleted()) {
            ItineraryServiceParents::removeItem($this);
            return $this->delete();
        }
        return true;
    }

    private function canBeDeleted()
    {
        if ($this->procurement_status == 1) {
            return false;
        }
        if ($this->has_bills != 0) {
            return false;
        }
        if ($this->administratinoFee != null) {
            return false;
        }
        if (count($this->additionals) > 0) {
            return false;
        }
        if (count($this->children) > 0) {
            return false;
        }
        return true;
    }


    public function updateService(Request $request)
    {
        $this->fill($request->all());
        return $this->save();
    }

    //In billing only the quantity of Pike Service can be edited.
    private function canEditQuantity($request)
    {
        return $request->get('billing-type', false) == 'billing' && !$this->isFromPike();
    }

    public function getBillAccountingService()
    {
        return AccountingService::where('servicesable_id', $this->id)
            ->where('servicesable_type', ItineraryProviderService::class)
            ->join('bills', function ($join) {
                $join->on('accounting_services.bill_id', '=', 'bills.id')->where('bills.is_active', true)
                    ->where('bills.type', 'debit');
            });
    }


    public function setDefaultUnitFromParam($unit)
    {
        $this->service_unit = $unit;
    }

    public function setDefaultUnit()
    {
        if ($this->service->isCoordinationFee()) {
            $this->setDefaultUnitFromParam('Per Operation');
        }
    }


    /*
  |--------------------------------------------------------------------------
  | MUTATORS
  |--------------------------------------------------------------------------
  */

    public function getBillingQuantityAttribute($val)
    {
        if ($val === null) {
            return $this->procurement_quantity;
        }
        if ($this->parent_id == null) {
            return 1;
        }
        return $val;
    }

    public function getBillingCheckedAttribute($val)
    {
        if ($this->has_bills) {
            return 0;
        }
        return $val;
    }

    public function getAdmFeeCheckedAttribute($val)
    {
        if ($this->has_bills) {
            return 0;
        }
        return $val;
    }

    public function setAdmFeeCheckedAttribute($val)
    {
        $this->attributes['adm_fee_checked'] = 0;

        if ($this->original['adm_fee_checked'] == 1 && $val == 1 && $this->has_bills == null) {
            $this->attributes['adm_fee_checked'] = 1;
            $this->attributes['billing_checked'] = 1;
        }

        if ($this->original['adm_fee_checked'] == 0 && $val == 1 && $this->has_bills == null) {
            $this->attributes['adm_fee_checked'] = 1;
            $this->attributes['billing_checked'] = 1;
        }

    }

    public function setBillingCheckedAttribute($val)
    {
        if ($this->has_bills != null) {
            return;
        }
        if ($this->original['billing_checked'] == 1 && $val == 0) { //Un check
            $this->attributes['adm_fee_checked'] = 0;
            $this->attributes['billing_checked'] = 0;
        }
        // Checked or Checked Adm Fee
        if (($this->original['billing_checked'] == 0 && $val == 1) || $this->attributes['adm_fee_checked'] == 1) {
            $this->attributes['billing_checked'] = 1;

            $this->onChildCheckBillingUnCheckParent();
        }
    }


    private function onChildCheckBillingUnCheckParent()
    {
        if ($this->parent_id != null) {
            $parent = $this->parent;
            $parent->updateBillingCheckItineraryServiceParent();
        }
    }

    //only works with parent Services.
    public function updateBillingCheckItineraryServiceParent()
    {
        if ($this->parent_id == null) {
            $itineraryServiceParent = ItineraryServiceParents::findItem($this);
            $itineraryServiceParent->billing_checked = $this->billing_checked;
            $itineraryServiceParent->adm_fee_checked = $this->adm_fee_checked;
            $itineraryServiceParent->save();
        }
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    public function scopeServiceOperation(Builder $query)
    {
        return $query->whereIn('service_origin', ['Operation'])->orWhereNull('service_origin');
    }

    public function scopeServiceQuotation(Builder $query)
    {
        return $query->whereIn('service_origin', ['Quotation'])->orWhereNull('service_origin');
    }

    public function scopeServiceProcurement(Builder $query)
    {
        return $query->whereIn('service_origin', ['Operation', 'Procurement'])->orWhereNull('service_origin');
    }


}