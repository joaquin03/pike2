<?php

namespace App\Models;

use App\Models\Contracts\ActivityEvent;
use App\Models\Scopes\ActivityEventScope;
use Backpack\CRUD\CrudTrait;
use Carbon\Carbon;

class ActivityEmail extends ActivityEvent
{
    use CrudTrait;
    
    protected static function boot()
    {
        parent::boot();
        
        static::addGlobalScope(new ActivityEventScope);
    }
    
    /*
   |--------------------------------------------------------------------------
   | GLOBAL VARIABLES
   |--------------------------------------------------------------------------
   */
    
    protected $table = 'emails';
    protected $fillable = [
        'subject',
        'to',
        'from',
        'cc',
        'cco',
        'proposal',
        'body'
    ];
    
    protected $dates = ['date'];
    
    
    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function getTitle()
    {
        return $this->subject;
    }
    
    public function getDescription()
    {
        return "From: " . $this->from . ", To: " . $this->to;
    }
    
    public function getIcon()
    {
        return 'fa fa-envelope';
    }
    
    public function getColor()
    {
        return 'bg-blue';
    }
    
    public function getProposal()
    {
        return str_replace("_", " ", $this->proposal) ?? " ";
    }
    
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

  
    
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    
    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */
    
    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    public function setDateAttribute($value)
    {
        $date = Carbon::createFromFormat('Y-m-d', $value);
        
        $this->attributes['date'] = $date;
        $this->setActivityDate($date);
    }
}
