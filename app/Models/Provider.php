<?php

namespace App\Models;

use App\Models\Accounting\Additional;
use App\Models\Scopes\ProviderScope;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Provider extends Company
{
    use SoftDeletes;
    public $balance = 0;

    public static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ProviderScope());

    }

    /*
   |--------------------------------------------------------------------------
   | GLOBAL VARIABLES
   |--------------------------------------------------------------------------
   *

   /*
   |--------------------------------------------------------------------------
   | FUNCTIONS
   |--------------------------------------------------------------------------
   */
    public function showLink()
    {
        $url = route('crud.provider.show', ['provider' => $this->id]);
        return '<a class="btn btn-xs btn-info" href="' . $url . '" data-toggle="tooltip"><i class="fa fa-info"></i></a>';
    }

    public function handlingService()
    {
        return $this->services()->where('services_providers.service_id', config('constants.handlingService'))->first();
    }


    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function services()
    {
        return $this->belongsToMany(Service::class, 'services_providers', 'provider_id', 'service_id')
            ->withPivot(['price', 'comments'])->orderBy('name');
    }

    public function parentServices()
    {
        return $this->services()->whereNull('services.service_id')->with('subServices');
    }

    public function airports()
    {
        return $this->belongsToMany(Airport::class, 'provider_airports', 'provider_id', 'airport_icao')
            ->orderBy('provider_airports.score', 'ASC');
    }

    public function airportProvider()
    {
        return $this->hasMany(ProviderAirport::class, 'provider_id', 'icao');
    }

    public function providerServices()
    {
        return $this->hasMany(ProviderService::class, 'provider_id', 'id')->with('service')->whereHas('service');
    }

    public function serviceParents()
    {
        return $this->belongsToMany(ServiceParent::class, 'services_providers', 'provider_id', 'service_id')
            ->withPivot(['price', 'comments'])->orderBy('name');
    }

    public function serviceChild()
    {
        return $this->belongsToMany(ServiceChild::class, 'services_providers', 'provider_id', 'service_id');
    }

    public function serviceChildForQuotation()
    {
        return $this->belongsToMany(ServiceChild::class, 'services_providers', 'provider_id', 'service_id')
            ->where('show_in_quotations', '=', 1);
    }

    public function operationProviderServices()
    {
        return $this->hasMany(ProviderService::class, 'provider_id', 'id')->with('serviceOperation')->whereHas('serviceOperation')->extraData();
    }

    public function procurementProviderServices()
    {
        return $this->hasMany(ProviderService::class, 'provider_id', 'id')->with('serviceProcurement')->extraData();
    }

    public function quotationProviderServices()
    {
        return $this->hasMany(ProviderService::class, 'provider_id', 'id')->with('serviceQuotation')->whereHas('serviceQuotation')->extraData();
    }

    public function addProviderToAirport(Airport $airport)
    {
        return $this->airports()->syncWithoutDetaching([$airport->icao]);

    }

    public function permanentPermissions()
    {
        return $this->hasMany(PermanentPermission::class, 'provider_id', 'id');
    }


    public function deleteProviderOfAirport(Airport $airport)
    {
        return $this->airports()->detach($airport->icao);
    }

    private function findProviderAirport(Airport $airport)
    {
        return $this->airportProvider()->where('airport_icao', $airport->icao)->first();
    }

    public function getPermitsAdditionals()
    {
        return $this->hasMany(Additional::class, 'operation_id', 'id')
            ->where('servicesable_type', get_class(new AircraftPermission()))->get();
    }

    public function getPermanentPermitsAdditionals()
    {
        return $this->hasMany(Additional::class, 'operation_id', 'id')
            ->where('servicesable_type', get_class(new PermanentPermission()))->get();
    }

    public function identifiableName()
    {
        return $this->name;
    }

    public function setBalanceToDate($date)
    {
        $date = Carbon::createFromFormat('d-m-Y', $date ?? Carbon::now()->format('d-m-Y'));

        $this->balance = round($this->getPositiveAmountToDate($date) - $this->getNegativeAmountToDate($date), 2);
    }

    public function showBalance()
    {
        return $this->balance;
    }

    public function getNegativeAmountToDate($date)
    {
        return $this->providerPayments()->where('date', '<', $date)->sum('amount')
            + $this->providerCreditNotes()->where('date', '<', $date)->sum('amount');
    }

    public function getPositiveAmountToDate($date)
    {
//        if($this->id == 1091){
//
//            dd($this->providerInvoices()->whereNotNull('start_date')->where('start_date', '<', $date)->pluck('date', 'amount') );
//        }

        return $this->providerInvoices()->whereNotNull('start_date')->where('start_date', '<', $date)->sum('amount')
            + $this->providerInvoices()->whereNull('start_date')->where('date', '<', $date)->sum('amount');
    }


    /*
    |--------------------------------------------------------------------------
    | SCOPESsale
    |--------------------------------------------------------------------------
    */
    public function scopeIsCountryEntity(Builder $query)
    {
        return $query->where('is_country_entity', 1);
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}