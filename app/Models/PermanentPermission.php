<?php

namespace App\Models;

use App\Http\Requests\StoreOVFPermission;
use App\Models\Accounting\Additional;
use App\Models\Accounting\AdministrationFee;
use App\Models\Contracts\AbstractOperationService;
use App\Models\Scopes\OVFScope;
use App\Models\Utilities\Country;
use Faker\Provider\DateTime;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Spatie\DbDumper\DbDumper;

class PermanentPermission extends AbstractOperationService
{
    use CrudTrait;
    
    /*
   |--------------------------------------------------------------------------
   | GLOBAL VARIABLES
   |--------------------------------------------------------------------------
   */
    
    protected $table = 'permanent_permissions';
    
    protected $fillable = [
        'type', 'code', 'start_at', 'expire_at', 'country', 'provider_id', 'file', 'file_name', 'file_notes',
        'address', 'cost', 'tax', 'billing_percentage', 'billing_quote_price', 'billing_unit_price', 'billing_price',
        'procurement_status', 'administration_fee', 'has_bills'
    ];
    
    protected $casts = [
        'photos' => 'array'
    ];
    
    public static $types = ['LANDING' => 'LND','LANDING DIPLOMATIC' => 'LND DP', 'OVER FLIGHT' => 'OVF',
        'OVERFLIGHT DIPLOMATIC' => 'OVF DP', 'DEPARTURE' => 'DPT', 'TRANSIT' => 'TR'];
    
    
    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(new OVFScope);
    }
    
    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    
    public function additionals()
    {
        return $this->hasMany(Additional::class, 'itinerary_id', 'id')
            ->where('servicesable_type', get_class(new PermanentPermission()));
    }
    
    public function getType()
    {
        $type = self::$types[strtoupper($this->type)] ?? '';
        if($type!=''){
            return $type;
        }
        return $this->type;
    }
    
    public function getCountry()
    {
        return Country::getName($this->country);
    }
    
    
    public function getStatusAttribute()
    {
        $now = Carbon::now();
        $start_at = Carbon::parse($this->attributes['start_at']);
        $expire_at = Carbon::parse($this->attributes['expire_at']);
        
        if ($now < $start_at) {
            return 'pending';
        }
        
        if ($now > $expire_at) {
            return 'expired';
        }
        
        return 'active';
    }
    
    
    
    public static function getAboutToExpire($daysToExpire, $status=0)
    {
        
        $mytime = Carbon::now()->subDays($daysToExpire);
        $products = PermanentPermission::whereDate('expire_at', '>=', $mytime)
            ->where('flag_notified',$status)->get();
        return $products;
        
        
    }
    
    public function getProcurementDescription()
    {
        return 'Permanent Permit - '.$this->code.' - '.$this->type;
    }
    
    public function getTax(){
        return $this->tax;
    }
    public function getCost(){
        return $this->cost;
    }
    
    public function createAdministrationFee($admFeePercentage, $operationId)
    {
        $older = AdministrationFee::where('servicesable_id', $this->id)
            ->where('operation_id', $operationId)->first();

        $administrationFee = new AdministrationFee();
        $administrationFee->servicesable()->associate($this);
        $this->administration_fee = 1;
        $administrationFee->billing_percentage = $admFeePercentage;

        if ($older==null) {
            $administrationFee->operation_id = $operationId;
            $administrationFee->servicesable_id = $this->id;
            $administrationFee->save();
            return $administrationFee;
        }
        return $older;
    }
    
    public function createAdditional(Request $request, $operation_id)
    {
        $additional = Additional::where('servicesable_id', $this->id)
            ->where('servicesable_type', get_class($this))
                ->where('operation_id',$operation_id)->first();
    
        if ($additional == null) {
            $additional = new Additional();
            $additional->servicesable()->associate($this);
            $additional->fill($request->all());
    
            $additional->operation_id = $operation_id;
            $additional->provider_id = $this->provider_id;
            $additional->servicesable_id = $this->id;
            $additional->save();
        }
        return $additional;
    }
    
    public function getFinalPrice()
    {
        return  $this->cost * (1 + ($this->tax));
    }
    
    public function getFinalCost()
    {
        return $this->cost * (1 + ($this->tax));
    }
    
    public function getUrlFile()
    {
        return $this->file ? url('/storage/'.$this->file) : '';
    }

    
    public function setAdministrationFee($value)
    {
        $this->attributes['administration_fee'] = $value;
        $this->save();
    }
    
    
    public function createOVFPermanentPermit(StoreOVFPermission $request, $operation)
    {
        $permanentPermission = new PermanentPermission();
        $permanentPermission->fill($this->toArray());
        $permanentPermission->fill($request->all());
        $permanentPermission->type = 'OVF-' . $this->type;
        $permanentPermission->has_bills = 0;
        $permanentPermission->administration_fee = 0;
        $permanentPermission->is_ovf = 1;
        $permanentPermission->save();
        $permanentPermission->aircrafts()->attach($operation->aircraft_id);
    
        return $permanentPermission;
    }
    
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    
    public function aircrafts()
    {
        return $this->belongsToMany(Aircraft::class)->withTimestamps();
    }
    
    public function provider()
    {
        return $this->belongsTo(Provider::class);
    }
    
    public function deleteItem($operationId)
    {
        if ($this->has_bills!=null && $this->has_bills != 0) {
            return false;
        }
        if ($this->administration_fee == 1) {
            return false;
        }
        if($this->additionals != null){
            return false;
        }
        $operation = Operation::findOrFail($operationId);
        return $operation->permanentPermissions()->detach($this->id);
    }
        
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */
    
    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    public function setFileAttribute($value)
    {
        $attribute_name = "file";
        $disk = "public";
        $destination_path = "permanent_permissions";
        
        $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path);
    }

//    public function setAdministrationFeeAttribute($value)
//    {
//        if($value==1 && $this->administration_fee == 0){
//            $this->attributes['administration_fee'] = $value;
//        }
//    }
    
}
