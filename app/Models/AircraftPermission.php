<?php

namespace App\Models;


use App\Http\Requests\StoreItineraryPermission;
use App\Models\Accounting\Additional;
use App\Models\Accounting\AdministrationFee;
use App\Models\Contracts\AbstractAccountableService;
use App\Models\Contracts\AbstractOperationService;
use App\Models\Contracts\AccountableServiceInterface;
use App\Models\Scopes\OVFScope;
use App\Models\Traits\AccountableServiceTrait;
use Backpack\CRUD\CrudTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;


class AircraftPermission extends Model implements AccountableServiceInterface, Auditable
{
    use CrudTrait, AccountableServiceTrait, SoftDeletes;
    use \OwenIt\Auditing\Auditable;
    
    protected $fillable = [
        'type', 'code', 'country', 'provider_id', 'status', 'icao_from', 'icao_to', 'expiration_date', 'tax',
        'operation_status', 'cost', 'procurement_status', 'billing_percentage', 'billing_unit_price',
        'billing_price', 'billing_final_price', 'has_bills', 'administration_fee', 'file',
        'country_distance', 'quotation_status', 'is_from_quotation', 'is_additional_quotation'
    ];
    
    public static $types = ['LANDING' => 'LND', 'OVER FLIGHT' => 'OVF', 'DEPARTURE' => 'DPT', 'TRANSIT' => 'TR'];
    
    protected $dates = ['expiration_date'];
    
    /*
   |--------------------------------------------------------------------------
   | RELATIONS
   |--------------------------------------------------------------------------
   */
    
    
    public function provider()
    {
        return $this->belongsTo(Company::class, 'provider_id', 'id');
    }
    
    public function additionals()
    {
        return $this->hasMany(Additional::class, 'servicesable_id', 'id')
                ->where('servicesable_type', get_class(new AircraftPermission()));
    }
    
    
    //
    
    public function setExpirationDateAttribute($value)
    {
        if ($value instanceof Carbon) {
            $this->attributes['expiration_date'] = Carbon::parse($value);
            return;
        }
        if ($value != null) {
            $this->attributes['expiration_date'] = Carbon::createFromFormat('d/m/Y', $value);
        }
    }
    
    public function getExpirationDateAttribute()
    {
        
        if ($this->attributes['expiration_date'] != null) {
            return Carbon::parse($this->attributes['expiration_date']);
        }
        return null;
    }
    
    public static function createItem($request, Operation $operation)
    {
        $permission = new AircraftPermission;
        $permission->fill($request);
        $permission->status = 'Requested';
        $permission->quotation_status = 'Requested';
        $permission->operation_id = $operation->id;
        
        $attribute_name = "file";
        $disk = "public";
        $destination_path = "aircraft-permission";
        
        $permission->uploadFileToDisk('file', $attribute_name, $disk, $destination_path);
        
        if ($permission->save()) {
            
            return $permission;
        }
        return false;
    }
    
    public function getUrlFile()
    {
        return $this->file ? url('/storage/' . $this->file) : '';
    }
    
    public function getType()
    {
        $type = self::$types[strtoupper($this->type)] ?? '';
        if ($type != '') {
            return $type;
        }
        return $this->type;
    }
    
    
    public function transformer()
    {
        return $this->toArray();
    }
    
    public function cancel()
    {
        $this->canceled_at = Carbon::now();
        return $this->save();
    }
    
    
    
    
    
    
    public function createAdministrationFee($admFeePercentage)
    {
        $administrationFee = $this->getAdministrationFee();
    
        if ($administrationFee == null) {
            $administrationFee = new AdministrationFee();
            $administrationFee->servicesable()->associate($this);
            $administrationFee->operation_id = $this->operation_id;
        }
        $administrationFee->billing_percentage = $admFeePercentage;
    
        $administrationFee->save();
    
        return $administrationFee;
    }

    public function getAdministrationFee()
    {
        return AdministrationFee::where('servicesable_id', $this->id)
            ->where('operation_id', $this->operation_id)->first();
    }
    
    public function setAdministrationFee($value)
    {
        $this->attributes['administration_fee'] = $value;
        $this->save();
    }
    
    public function deleteItem()
    {
        if ($this->procurement_status == 1) {
            return false;
        }
        if ($this->has_bills != 0) {
            return false;
        }
        if ($this->administration_fee == 1) {
            return false;
        }
        if (count($this->additionals) > 0) {
            return false;
        }
        return $this->delete();
    }
    
    public function createAdditional(Request $request)
    {
        $additional = Additional::where('servicesable_id', $this->id)
            ->where('servicesable_type', get_class($this))
            ->where('operation_id', $this->operation_id)->first();
        
        if ($additional == null) {
            $additional = new Additional();
            $additional->servicesable()->associate($this);
            $additional->fill($request->all());
            
            $additional->operation_id = $this->operation_id;
            $additional->provider_id = $this->provider_id;
            $additional->servicesable_id = $this->id;
            $additional->save();
        }
        return $additional;
    }
    
    public function addBill($billId)
    {
        $this->has_bills = $billId;
        //$this->procurement_status = $billId;
        $this->save();
    }
    
    public function removeBill()
    {
        $this->has_bills = null;
        $this->save();
    }
    
    
    
    /***************
     * Accountable
     ****************/
    //Procurement
    public function getProcurementDescription()
    {
        return 'Permit - ' . $this->code . ' - ' . $this->type;
    }
    
    public function getProcurementQuantity()
    {
        return 1;
    }
    public function getProcurementCost()
    {
        return $this->cost;
    }
    public function getProcurementAdmFee()
    {
        return $this->administration_fee;
    }
    public function getProcurementTax()
    {
        return $this->tax;
    }
    public function getProcurementUnitFinalCost()
    {
        try {
            return round(($this->cost) * (1 + ($this->tax)), 3);
        } catch (\Exception $e) {
            return 0;
        }
    }
    public function getProcurementFinalCost()
    {
        return round($this->getProcurementUnitFinalCost() * $this->getProcurementQuantity(), 3);
    }
    
    
    //Billing
    public function getBillingDescription()
    {
        return $this->getProcurementDescription();
    }
    public function getBillingQuantity()
    {
        return 1;
    }
    public function getBillingPercentage()
    {
        return $this->billing_percentage;
    }
    
    public function getBillingUnitPrice()
    {
        $finalCost = $this->getProcurementFinalCost();
    
        if ($this->billing_unit_price != 0) {
            $finalCost = $this->billing_unit_price;
        }
    
        return round($finalCost + $this->getPercentageValue($finalCost), 2);
    }
    
    public function getBillingFinalPrice()
    {
        return $this->getBillingUnitPrice() * $this->getBillingQuantity();
    }
    
    private function getPercentageValue($finalCost)
    {
        if ($this->billing_percentage) {
            return $this->billing_percentage * $finalCost;
        }
        return 0;
    }
    
    //Extras
    public function getProfitPrice()
    {
        return $this->getBillingFinalPrice() - $this->getProcurementFinalCost();
    }
    
    //Quotation
    public function getFinalCostQuotation()
    {
        return $this->getBillingUnitPrice();
    }

    public function getQuotationFinalPrice(){
        return $this->getBillingFinalPrice();
    }
    
    
   
   /*
  |--------------------------------------------------------------------------
  | MUTATORS
  |--------------------------------------------------------------------------
  */
    public function setAdministrationFeeAttribute($value)
    {
        if ($value == 1 && $this->administration_fee == 0) {
            $this->attributes['administration_fee'] = $value;
        }
    }
    
    
    
    
}
