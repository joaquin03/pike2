<?php

namespace App\Models;

use App\Models\Scopes\ServiceParentScope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Config;

class Service extends Model
{
    use CrudTrait, SoftDeletes;


    /*
   |--------------------------------------------------------------------------
   | GLOBAL VARIABLES
   |--------------------------------------------------------------------------
   */
    protected $fillable = [
        'name', 'description', 'service_id', 'show_in_quotations'
    ];
    public $table = 'services';

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function addServiceToProvider(Provider $provider, $data = [])
    {
        $providerService = $this->findServiceProvider($provider);

        if (is_null($providerService)) {
            $providerService = new ProviderService();
            $providerService->service()->associate($this);
            $providerService->provider()->associate($provider);

            $providerService->price = $data['price'] ?? null;
            $providerService->comments = $data['comments'] ?? null;
            return $providerService->save();
        }
        return false;
    }

    public function deleteServiceOfProvider(Provider $provider)
    {
        $providerService = $this->findServiceProvider($provider);

        return $providerService->pivot->delete();
    }

    private function findServiceProvider(Provider $provider)
    {
        return $this->providers()->where('provider_id', $provider->id)->first();
    }

    public function identifiableName()
    {
        return $this->name;
    }


    public function isCoordinationFee()
    {
        return $this->id === Config::get('constants.coordination_fee');
    }
        


    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function providers()
    {
        return $this->belongsToMany(Provider::class, 'services_providers', 'service_id', 'provider_id')
            ->withPivot('price', 'comments')
            ->withTimestamps();
    }

    public function category()
    {
        return $this->hasOne(ServiceCategory::class, 'id', 'category_id');
    }

    public function subServices()
    {
        return $this->hasMany(Service::class, 'service_id', 'id')->orderBy('name');
    }

    public function parent()
    {
        return $this->hasOne(ServiceParent::class, 'id', 'service_id');
    }


    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    public function scopeIsParent(Builder $q)
    {
        return $q->whereNull('service_id')->with('subServices');
    }
    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
