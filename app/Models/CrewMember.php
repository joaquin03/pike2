<?php

namespace App\Models;

use App\Models\Utilities\Country;
use App\Models\Utilities\CrewMembers;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Illuminate\Support\Collection;

class CrewMember extends Model
{
    use CrudTrait;

    /*
   |--------------------------------------------------------------------------
   | GLOBAL VARIABLES
   |--------------------------------------------------------------------------
   */

    protected $table = 'crew_members';

    protected $fillable = [
        'name',
        'country',
        'roles',
        'actions',
        'passport',
        'expire_date',
        'mail',
        'operator_id',
        'medical_certificate',
        'birthdate',
        'licence_number',
        'licence_number_expire_at',
        'licence',
        'medical_certificate_expire_date',
        'passport_number'

    ];
    protected $appends = ['roles'];
    protected $casts = ['roles' => 'array', 'photos' => 'array'];


    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function getCountry()
    {
        return Country::getName($this->country);
    }


    public function getRoles()
    {
        $roles = new Collection();

        if ($this->is_captain) {
            $roles->push('is_captain');
        }

        if ($this->is_first_official) {
            $roles->push('is_first_official');
        }

        if ($this->is_auxiliary) {
            $roles->push('is_auxiliary');
        }

        return $roles;
    }

    public function getRolesString()
    {
        $roles = new Collection();

        if ($this->is_captain) {
            $roles->push('Captain');
        }

        if ($this->is_first_official) {
            $roles->push('First Official');
        }

        if ($this->is_auxiliary) {
            $roles->push('Auxiliary');
        }

        return implode(", ", $roles->toArray());
    }


    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function operator()
    {
        return $this->belongsTo(Operator::class);
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    public function scopeIsCaptain(Builder $builder)
    {
        return $builder->where('is_captain', 1);
    }

    public function scopeIsFirstOfficial(Builder $builder)
    {
        return $builder->where('is_first_official', 1);
    }

    public function scopeIsAuxiliary(Builder $builder)
    {
        return $builder->where('is_auxiliary', 1);
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    public function getRolesAttribute()
    {
        return $this->getRoles()->toArray();
    }


    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */


    public function setRolesAttribute($roles)
    {
        $this->is_captain = false;
        $this->is_first_official = false;
        $this->is_auxiliary = false;

        if (in_array("is_captain", $roles)) {
            $this->is_captain = true;
        }
        if (in_array("is_first_official", $roles)) {
            $this->is_first_official = true;
        }
        if (in_array("is_auxiliary", $roles)) {
            $this->is_auxiliary = true;
        }
    }

    public function setPassportAttribute($value)
    {
        $this->setCustomAttr("passport", "crew_members_documents", $value);
    }

    public function setLicenceAttribute($value)
    {
        $this->setCustomAttr("licence", "crew_members_documents", $value);
    }

    public function setMedicalCertificateAttribute($value)
    {
        $this->setCustomAttr("medical_certificate", "crew_members_documents", $value);
    }

    private function setCustomAttr($attribute_name, $folder, $value)
    {
        $disk = "public";
        $destination_path = '';
        $file_name = '';

        if ($value) {

            $destination_path = $folder.'/'.md5($value->getClientOriginalName() . time());
            $file_name = $value->getClientOriginalName();
        }
        $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path, $file_name);
    }

    public function uploadFileToDisk($value, $attribute_name, $disk, $destination_path, $file_name)
    {
        $request = \Request::instance();

        // if a new file is uploaded, delete the file from the disk
        if ($request->hasFile($attribute_name) &&
            $this->{$attribute_name} &&
            $this->{$attribute_name} != null) {
            \Storage::disk($disk)->delete($this->{$attribute_name});
            $this->attributes[$attribute_name] = null;
        }

        // if the file input is empty, delete the file from the disk
        if (is_null($value) && $this->{$attribute_name} != null) {
            \Storage::disk($disk)->delete($this->{$attribute_name});
            $this->attributes[$attribute_name] = null;
        }

        // if a new file is uploaded, store it on disk and its filename in the database
        if ($request->hasFile($attribute_name) && $request->file($attribute_name)->isValid()) {
            // 1. Generate a new file name
            $file = $request->file($attribute_name);
            $new_file_name = $file_name;

            // 2. Move the new file to the correct path
            $file_path = $file->storeAs($destination_path, $new_file_name, $disk);

            // 3. Save the complete path to the database
            $this->attributes[$attribute_name] = $file_path;
        }
    }


    //Name used for revisions
    public function identifiableName()
    {
        return $this->name;
    }
}
