<?php

namespace App\Models;

use App\Models\Scopes\ServiceChildScope;
use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceChild extends Service
{
    use CrudTrait, SoftDeletes;
    
    public static function boot()
    {
        static::bootTraits();
        static::addGlobalScope(new ServiceChildScope());
    }
    
     /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $fillable = [
        'name', 'description', 'is_for_procurement', 'services_id'
    ];
    
    protected $attributes = [
        'is_for_procurement' => 1,
    ];
    
    public function serviceParent()
    {
        return $this->belongsTo(Service::class, 'id', 'service_id');
    }


}
