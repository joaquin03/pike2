<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class QuotationExtended extends Model
{
    protected $table = 'quotation_extended';
    
    protected $fillable = [
        'quotation_id', 'contact_id', 'sales_manager_id', 'payment', 'payment_method', 'pax_in_out',
        'quotation_exportable_file', 'fbo_id', 'type_of_flight', 'type_of_vip_assistance_flight',
    ];
    
    public function quotation()
    {
        return $this->belongsTo(Quotation::class, 'quotation_id', 'id');
    }
    
    public function contact()
    {
        return $this->belongsTo(Contact::class, 'contact_id', 'id');
    }

    public function fbo()
    {
        return $this->belongsTo(Provider::class, 'fbo_id', 'id');
    }
    
    
}
