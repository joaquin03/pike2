<?php

namespace App\Models;

use App\Models\Utilities\Country;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Excel;


class Airport extends Model
{
    use CrudTrait;
    
    /*
   |--------------------------------------------------------------------------
   | GLOBAL VARIABLES
   |--------------------------------------------------------------------------
   */
    
    protected $table = 'airports';
    
    protected $fillable = [
        'icao', 'name', 'descriptions', 'country', 'iata', 'OPR_HS_H24', 'OPR_HS_TO', 'OPR_HS_FROM',
        'H24_OR', 'RWY1', 'lengthwidth1ft', 'lengthwidth1m', 'PCN1', 'RWY2', 'lengthwidth2ft',
        'lengthwidth2m', 'PCN2',
    ];
    
    //protected $table = 'airports';
    protected $primaryKey = 'icao';
    protected $keyType = 'string';
    
    // public $timestamps = false;
    // protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];
    
    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function getCountry()
    {
        return Country::getName(strtolower($this->country));
    }
    
    
    public static function uploadExcel($file)
    {
        
        Excel::selectSheetsByIndex(0)->load($file, function ($sheet) {
            $sheet->each(function ($row) {
                
                $airportIcaos = Airport::find($row['icao']);
                if (is_null($airportIcaos)) {
                    
                    $airport = new Airport();
                    $airport->icao = $row->icao;
                    $airport->name = $row->name;
                    $airport->country = $row->country;
                    $airport->descriptions = $row->description;
                    $airport->save();
                }
            });
        });
    }
    
    public function showLink()
    {
        
        $url = route('crud.airport.show', ['airport' => $this->icao]);
        
        return '<a class="btn btn-xs btn-info" href="' . $url . '" data-toggle="tooltip"><i class="fa fa-info"></i> Show</a>';
    }
    
    public static function providersByAirportsIcao($airport)
    {
        $filteredProviders = [];
        $providers = Provider::all();
        foreach ($providers as $provider) {
            if (!in_array($provider, $filteredProviders)) {
                if (strtoupper($airport->country) == strtoupper($provider->country)) {
                    array_push($filteredProviders, $provider);
                }
                else if ($provider->is_ww_provider == 1) {
                    array_push($filteredProviders, $provider);
                }
            }
        }
        return $filteredProviders;
    }
    
    public static function providersByOperationIcaos($airports)
    {
        $filteredProviders = [];
        $providers = Provider::all();
        foreach ($airports as $airport) {
            foreach ($providers as $provider) {
                if (!in_array($provider, $filteredProviders)) {
                    if (strtoupper($airport->country) == strtoupper($provider->country)) {
                        array_push($filteredProviders, $provider);
                    }
                    else if ($provider->is_ww_provider == 1) {
                        array_push($filteredProviders, $provider);
                    }
                }
            }
        }
        return $filteredProviders;
    }
    
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function providers()
    {
        return $this->belongsToMany(Provider::class, 'provider_airports', 'airport_icao', 'provider_id')
            ->withPivot('score')->orderBy('score')
            ->withTimestamps();
    }


    
    public function providerAirport()
    {
        return $this->hasMany(ProviderAirport::class, 'airport_icao', 'icao')->orderBy('score');
    }
    
    public function itineraries()
    {
        return $this->belongsTo(Itinerary::class, 'icao', 'airport_icao_string');
    }
    
    
    public function addAirportToProvider(Provider $provider)
    {
        return $this->providers()->syncWithoutDetaching([$provider->id]);
        
    }
    
    public function deleteAirportOfProvider(Provider $provider)
    {
        return $this->providers()->detach($provider->id);
    }
    
    private function findAirportProvider(Provider $provider)
    {
        return $this->providerAirport()->where('provider_id', $provider->id)->first();
    }
    
    public function airportProviders()
    {
        return $this->hasMany(ProviderAirport::class, 'airport_icao', 'icao')->with('provider')->whereHas('provider');
    }
    
    public function canDelete(){
        return $this->itineraries===null;
    }
    
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    
    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */
    
    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
   
    */
}
