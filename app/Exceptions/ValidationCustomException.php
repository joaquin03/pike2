<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Support\Arr;


class ValidationCustomException extends Exception
{
    /**
     * The validator instance.
     *
     * @var \Illuminate\Contracts\Validation\Validator
     */
    public $validator;
    
    /**
     * The recommended response to send to the client.
     *
     * @var \Symfony\Component\HttpFoundation\Response|null
     */
    public $response;
    
    /**
     * The status code to use for the response.
     *
     * @var int
     */
    public $status = 422;
    
    /**
     * The name of the error bag.
     *
     * @var string
     */
    public $errorBag;
    
    /**
     * The path the client should be redirected to.
     *
     * @var string
     */
    public $redirectTo;
    

    public function __construct($errors, $response = null)
    {
        parent::__construct('The given data was invalid.');
    
        $this->errorBag = [];
        
        if (is_array($errors)){
            $this->withMessages($errors);
        } else {
            $this->withError($errors);
        }
        
        $this->response = $response;
    }
    
    /**
     * Create a new validation exception from a plain array of messages.
     *
     * @param  array  $messages
     * @return static
     */
    private function withMessages(array $messages)
    {
        foreach ($messages as $key => $value) {
            foreach (Arr::wrap($value) as $message) {
                $this->errorBag[$key] = $message;
            }
        }
    }
    
    private function withError($errorMessage)
    {
        $this->errorBag['Errors'] = $errorMessage;
    }
    
    /**
     * Get all of the validation error messages.
     *
     * @return array
     */
    public function errors()
    {
        return $this->validator->errors()->messages();
    }
    
    /**
     * Set the HTTP status code to be used for the response.
     *
     * @param  int  $status
     * @return $this
     */
    public function status($status)
    {
        $this->status = $status;
        
        return $this;
    }
    
    /**
     * Set the error bag on the exception.
     *
     * @param  string  $errorBag
     * @return $this
     */
    public function errorBag($errorBag)
    {
        $this->errorBag = $errorBag;
        
        return $this;
    }
    
    /**
     * Set the URL to redirect to on a validation error.
     *
     * @param  string  $url
     * @return $this
     */
    public function redirectTo($url)
    {
        $this->redirectTo = $url;
        
        return $this;
    }
    
    /**
     * Get the underlying response instance.
     *
     * @return \Symfony\Component\HttpFoundation\Response|null
     */
    public function getResponse()
    {
        return $this->response;
    }
}