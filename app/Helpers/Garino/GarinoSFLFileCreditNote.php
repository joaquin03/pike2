<?php
/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 11/16/18
 * Time: 2:49 PM
 */

namespace App\Helpers\Garino;


use App\Models\Accounting\AccountingService;
use App\Models\Accounting\Bill;

class GarinoSFLFileCreditNote extends BaseGarinoSFLFile
{
    
    protected function getEFactData($debitNote)
    {
        return [
            'Encabezado' => $this->getEncabezado($debitNote),
            'Detalle' => $this->getDetail($debitNote),
            'Referencias' => $this->getReferencia($debitNote)
        ];
    }
    
    
    //Base
    protected function getTotales($debitNote)
    {
        return [
            'TpoMoneda' => 'USD',
            'TpoCambio' => $debitNote->dollar_change,
            'MntNoGrv' => $this->formatNumber($debitNote->amount), //Total Monto - No Gravado
            'MntExpoyAsim' => '', //Total Monto - Exportación y asimiladas
            'MntImpuestoPerc' => 0, //Total Monto - Impuesto percibido
            'MntIVAenSusp' => 0, //Total Monto - IVA en suspenso
            'MntNetoIvaTasaMin' => 0, //Total Monto Neto - IVA Tasa mínima
            'MntNetoIVATasaBasica' => 0, //Total Monto Neto - IVA Tasa básica
            'MntNetoIVAOtra' => 0, //Total Monto Neto – IVA otra tasa
            'IVATasaMin' => 10,
            'IVATasaBasica' => 22,
            'MntIVATasaMin' => 0,
            'MntIVATasaBasica' => 0,
            'MntIVAOtra' => 0,
            'MntTotal' => $this->formatNumber($debitNote->amount),
            'MntTotRetenido' => '', //Total Monto Retenido/Percibido
            'TotCreditosFisc' => '',
            'CantLinDet' => $this->getItemQuantity($debitNote), //Líneas
            'MontoNF' => 0, //Monto no Facturable
            'MntPagar' => $this->formatNumber($debitNote->amount),
        ];
    }
    
    protected function getIdDoc($debitNote)
    {
        return [
            'TipoCFE' => '102',
            'Serie' => '', //Garino
            'Nro' => '', //Garino
            'FchEmis' => $debitNote->created_at != null ? $this->transformDate($debitNote->created_at->format('Y/m/d')) : '',
            'PeriodoDesde' => $debitNote->start_date != null ? $this->transformDate($debitNote->start_date->format('Y/m/d')) : '',
            'PeriodoHasta' => $debitNote->due_date != null ? $this->transformDate($debitNote->due_date->format('Y/m/d')) : '',
            'MntBruto' => 1,
            'FmaPago' => 2, // 1 => contado, 2=>Credito
            'FchVenc' => $debitNote->due_date != null ? $this->transformDate($debitNote->due_date->format('Y/m/d')) : '',
            'IndTipoTraslado' => 1,
            'ClauVenta' => '',
            'ModVenta' => '',
            'ViaTransp' => 2, //Vía de transporte
            'InfoAdicional' => '', //OPCIONAL
        ];
        
    }
    
    protected function getItemQuantity($debitNote)
    {
        return count($debitNote->billsAndDebitNotes);
    }
    
    protected function getAccountingService(AccountingService $service, $index)
    {
        return [
            'NroLinDet' => $index + 1, //Nº de línea o Nº Secuencial, +1 porque no puede empezar en 0
            'CodItem' => $service->id,
            'IndFact' => 1, //Indicador de facturación
            'NomItem' => $this->cleanText($service->name),
            'DscItem' => $this->cleanText($service->description),
            'Cantidad' => $this->formatNumber($service->procurement_quantity),
            'UniMed' => 'N/A',  //Unidad de medida
            'PrecioUnitario' => $this->getUnitPrice($service),
            'MontoItem' => $this->formatNumber($service->billing_final_price),
        ];
    }
    
    
    //Credit Note modifications
    protected function getDetail($debitNote)
    {
        $items = [];
        foreach ($debitNote->billsAndDebitNotes as $index => $bill) {
            $items[] = $this->getBillsDetails($bill->pivot, $index);
        }
        
        return $items;
    }
    
    private function getBillsDetails($pivot, $index) //pasar pivot
    {
        $bill =  Bill::find($pivot->bill_id);
        
        return [
            'NroLinDet' => $index + 1, //Nº de línea o Nº Secuencial, +1 porque no puede empezar en 0
            'CodItem' => $pivot->bill_id,
            'IndFact' => 1, //Indicador de facturación
            'NomItem' => "Bill " . $bill->bill_number . " - " . $pivot->reason ,
            'Cantidad' => 1,
            'UniMed' => 'N/A',  //Unidad de medida
            'PrecioUnitario' => $this->formatNumber($pivot->amount),
            'MontoItem' => $this->formatNumber($pivot->amount),
        ];
    }
    
    protected function getReferencia($debitNote)
    {
        $referencias = [];
        foreach ($debitNote->billsAndDebitNotes as $index => $bill) {
            $referencias[] = $this->getReferencias($bill, $index);
        }
        return $referencias;
    }
    
    protected function getReferencias($bill, $index)
    {
        
        return [
            'NroLinRef' => $index + 1, //Nº de línea o Nº Secuencial, +1 porque no puede empezar en 0
            'IndGlobal' => 0,
            'TpoDocRef' => $bill->pivot->cfe_type,
            'SerieCFERef' => $this->getSerieRef($bill->id), //TODO: Revisar esto!
            'NroCFERef' => $this->getNumeroRef($bill->id),
            'RazonRef' => $bill->pivot->reason,
            'FechaCFEref' => $this->transformDate(date('Y/m/d'))
        ];
    }
    
    private function getSerieRef($billId)
    {
        $bill = Bill::findOrFail($billId);
        
        return $this->validateBillNumber($bill->bill_number, 0);
    }
    
    private function getNumeroRef($billId)
    {
        $bill = Bill::findOrFail($billId);
  
        return $this->validateBillNumber($bill->bill_number, 1);
    }
    
    private function validateBillNumber($billNumber, $position){
        
        if ($billNumber) {
            $billSections = explode('-', $billNumber);
            if(count($billSections)!=2){
                return '';
            }
            if ($billSections[$position] == 'Pike') {
                return  ''; //fix problem with integration old data
            }
            return $billSections[$position];
        }
        return '';
    }
}