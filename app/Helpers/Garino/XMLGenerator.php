<?php

namespace App\Helpers\Garino;


class XMLGenerator
{
    public function create($data, $rootNodeName, $xml = null, $parent = null)
    {
        // turn off compatibility mode as simple xml throws a wobbly if you don't.
        if (ini_get('zend.ze1_compatibility_mode') == 1) {
            ini_set('zend.ze1_compatibility_mode', 0);
        }
        
        if ($xml == null) {
            $xml = simplexml_load_string("<?xml version='1.0' encoding='utf-8'?><$rootNodeName />");
        }
        
        $xml = $this->transformLines($data, $rootNodeName, $xml, $parent);
        
        // pass back as string. or simple xml object if you want!
        return $xml->asXML();
    }
    
    
    
    private function transformLines($data, $rootNodeName, $xml, $parent = null)
    {
        // loop through the data passed in.
        foreach ($data as $key => $value) {
            // no numeric keys in our xml please!
            if (is_numeric($key)) {
                // make string key...
                $key = $parent;
            }
            if ($key === "Detalle") {
                $parent = 'Item';
            }
            if ($key === "Referencias") {
                $parent = 'Referencia';
            }
            
            if ($key == null) {
                $key = 'Item';
            }
            
            // replace anything not alpha numeric
            $key = preg_replace('/[^a-zA-Z0-9]/', '', $key);
    
            // if there is another array found recrusively call this function
            if (is_array($value)) {
                $node = $xml->addChild($key);
                // recrusive call.
                $this->create($value, $rootNodeName, $node, $parent);
            } else {
                // add single node.
                $value = htmlentities($value);
                $xml->addChild($key, $value);
            }
        }
        
        return $xml;
    }
    
}