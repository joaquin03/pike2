<?php
/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 11/16/18
 * Time: 11:29 AM
 */

namespace App\Helpers\Garino;


use App\Models\Accounting\AccountingService;
use App\Models\Utilities\Country;

abstract class BaseGarinoSFLFile
{
    
    protected $array = [];
    protected $xmlGenerator;

    public function __construct()
    {
        $this->xmlGenerator = new XMLGenerator();
    }
    
    public function generateEncode($bill)
    {
        return base64_encode($this->generate($bill));
    }
    
    public function generate($bill)
    {
        $xmlHeader = "<?xml version='1.0' encoding='UTF-8'?>" .
            "<CFE_Empresas_Type xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>" .
            "</CFE_Empresas_Type>";
        
        
        $xml = new \SimpleXMLElement($xmlHeader);
        
        $array = [
            'CFE' => [
                'eFact' => $this->getEFactData($bill)
            ],
            'Adenda' => $this->getAdenda($bill)
        ];
        
        $xmlInfo = $this->xmlGenerator->create($array, "CFE version='1.0' xmlns='http://cfe.dgi.gub.uy'", $xml);

        return $xmlInfo;
    }
    
    protected function getEFactData($bill)
    {
        return [
            'Encabezado' => $this->getEncabezado($bill),
            'Detalle' => $this->getDetail($bill),
        ];
    }
    
    abstract protected function getTotales($bill);
    
    abstract protected function getIdDoc($bill);
    
    abstract protected function getAccountingService(AccountingService $service, $index);
    
    abstract protected function getItemQuantity($bill);
    
    
    protected function getEncabezado($bill)
    {
        return [
            'idDoc' => $this->getIdDoc($bill),
            'Emisor' => $this->getEmisor(),
            'Receptor' => $this->getReceptor($bill),
            'Totales' => $this->getTotales($bill),
        ];
    }
    
    protected function getEmisor()
    {
        return config('emisorInfo');
    }
    
    protected function getReceptor($bill)
    {
        return [
            'TipoDocRecep' => 4, // $bill->company->doc_type_receptor,  5 es psaporte para todos los paises
            'CodPaisRecep' => $bill->company->country, //PASAR A CODIGO - EJ UY
            'DocRecep' => $bill->billingCompany->document_receptor, //RUT o doc extranjero
            'RznSocRecep' => $this->cleanText($bill->billingCompany->business_name_receptor),
            'DirRecep' => $this->cleanText($bill->billingCompany->address_receptor),
            'CiudadRecep' => $this->cleanText($bill->billingCompany->city_receptor),
            'DeptoRecep' => $this->cleanText($bill->billingCompany->state_receptor),
            'PaisRecep' => Country::getName($bill->billingCompany->country) ?? '',
            'InfoAdicional' => '',
            //   'CP' => $bill->company->postal_code_receptor, solo para uruguay
            'CompraID' => $bill->id,
        ];
    }
    
   
    
    public function formatNumber($number)
    {
        $number = round($number, 2) ;
        return str_replace('.', ',', $number); // cambio .s por ,s
    }
    
    
    public function getAdenda($bill)
    {
        $refNumber = '';
        if ($bill->ref_number != '') {
            $refNumber = 'Ref. Number:'.$bill->ref_number.' @@ @@';
        }
        
        return [
            'Texto' => $refNumber.'**Transfers USD (Dolar) | Banco Santander S.A.!! @@ @@ **Beneficiary Bank:!! BANK SANTANDER S.A. Address: Cerrito 449. CP11000 Montevideo, Uruguay. Swift : BSCHUYMM @@ **Benefeciary Name:!! Pike Aviation S.A. Address: Dr. Alejandro Schroeder 6475 of 101 Montevideo, Uruguay Account: 5100338698 @@ **Intermediary Bank:!! WELLS FARGO Swift code :  PNBPUS3NNYC ABA :  #026005092 @@ @@  Please contact:billing@pike-aviation.com with any inquiries or question within 10 days from the invoice date. After that period we consider that it has been accepted with nothing to claim.',
            'TipoInterno' => '',
            'SerieInterna' => '',
            'NroInterno' => '',
        ];
    }


    abstract protected function getDetail($bill);
    
    
    
    protected function cleanText($text)
    { //quita caracteres especiales de XML
        $a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ', 'Ά', 'ά', 'Έ', 'έ', 'Ό', 'ό', 'Ώ', 'ώ', 'Ί', 'ί', 'ϊ', 'ΐ', 'Ύ', 'ύ', 'ϋ', 'ΰ', 'Ή', 'ή');
        $b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o', 'Α', 'α', 'Ε', 'ε', 'Ο', 'ο', 'Ω', 'ω', 'Ι', 'ι', 'ι', 'ι', 'Υ', 'υ', 'υ', 'υ', 'Η', 'η');
        $text = str_replace($a, $b, $text);
        $text = str_replace("&", " and ", $text);
        //para ampliar los caracteres a reemplazar agregar lineas de este tipo:
        //$s = str_replace("caracter-que-queremos-cambiar","caracter-por-el-cual-lo-vamos-a-cambiar",$s);
        return $text;
    }
    
    
    
    protected function getUnitPrice($service)
    {
        return $this->formatNumber($service->procurement_quantity != 0 ? round($service->billing_final_price /
            $service->procurement_quantity, 3) : 0);
    }
    
    
    protected function transformDate($date)
    {
        return str_replace('/', '', $date);
    }
    
    protected function getMeasurement($service) {
        $measurement = 'N/A';
        if (strtolower($service->description) === 'fuel') {
            $measurement = 'USG';
        }
        return $measurement;
    }
    
}