<?php


namespace App\Helpers\Garino;


use App\Models\Accounting\AccountingService;

class GarinoSFLFileBill extends BaseGarinoSFLFile
{
    
    protected function getTotales($bill)
    {
        return [
            'TpoMoneda' => 'USD',
            'TpoCambio' => $bill->dollar_change,
            'MntNoGrv' => $this->formatNumber($bill->amount), //Total Monto - No Gravado
            'MntExpoyAsim' => '', //Total Monto - Exportación y asimiladas
            'MntImpuestoPerc' => 0, //Total Monto - Impuesto percibido
            'MntIVAenSusp' => 0, //Total Monto - IVA en suspenso
            'MntNetoIvaTasaMin' => 0, //Total Monto Neto - IVA Tasa mínima
            'MntNetoIVATasaBasica' => 0, //Total Monto Neto - IVA Tasa básica
            'MntNetoIVAOtra' => 0, //Total Monto Neto – IVA otra tasa
            'IVATasaMin' => 10,
            'IVATasaBasica' => 22,
            'MntIVATasaMin' => 0,
            'MntIVATasaBasica' => 0,
            'MntIVAOtra' => 0,
            'MntTotal' => $this->formatNumber($bill->amount),
            'MntTotRetenido' => '', //Total Monto Retenido/Percibido
            'TotCreditosFisc' => '',
            'CantLinDet' => $this->getItemQuantity($bill), //Líneas
            'MontoNF' => 0, //Monto no Facturable
            'MntPagar' => $this->formatNumber($bill->amount),
        ];
    }
    
    protected function getIdDoc($bill)
    {
        return [
            'TipoCFE' => '101',
            'Serie' => '', //Garino
            'Nro' => '', //Garino
            'FchEmis' => $bill->date != null ? $this->transformDate($bill->date->format('Y/m/d')) : '',
            'PeriodoDesde' => $bill->date != null ? $this->transformDate($bill->date->format('Y/m/d')) : '',
            'PeriodoHasta' => $bill->due_date != null ? $this->transformDate($bill->due_date->format('Y/m/d')) : '',
            'MntBruto' => 1,
            'FmaPago' => $bill->payment_method, // 1 => contado, 2=>Credito
            'FchVenc' => $bill->due_date != null ? $this->transformDate($bill->due_date->format('Y/m/d')) : '',
            'IndTipoTraslado' => 1,
            'ClauVenta' => '',
            'ModVenta' => '',
            'ViaTransp' => 2, //Vía de transporte
            'InfoAdicional' => '', //OPCIONAL
        ];
    }
    
    protected function getAccountingService(AccountingService $service, $index)
    {
        return [
            'NroLinDet' => $index + 1, //Nº de línea o Nº Secuencial, +1 porque no puede empezar en 0
            'CodItem' => $service->id,
            'IndFact' => 1, //Indicador de facturación
            'NomItem' => $this->cleanText($service->name),
            'DscItem' => $this->cleanText($service->description),
            'Cantidad' => $this->formatNumber($service->procurement_quantity),
            'UniMed' => $this->getMeasurement($service),  //Unidad de medida
            'PrecioUnitario' => $this->getUnitPrice($service),
            'MontoItem' => $this->formatNumber($service->billing_final_price),
        ];
    }
    
    protected function getDetail($bill)
    {
        //$items = $this->mergeSameAccountingItems($bill);
        return $this->parseDetailsForGarino($bill->accountingServices);
    }
    
    protected function getItemQuantity($bill)
    {
        return count($bill->accountingServices);
    }
    
    private function mergeSameAccountingItems($bill)
    {
        $items = collect();
        foreach ($bill->accountingServices as $index => $accountingService) {
            
            $auxItem = $items->where('description', $accountingService->description)->first();
            if ($auxItem == null) {
                $items->offsetSet($accountingService->id, $accountingService);
            } else {
                $actual = $items[$auxItem->id];
                if ($actual->billing_unit_price == $accountingService->billing_unit_price) {
                    $items[$auxItem->id]->procurement_quantity += $accountingService->procurement_quantity;
                    $items[$auxItem->id]->billing_final_price += $accountingService->billing_final_price;
                }
            }
        }
        return $items;
    }
    
    protected function parseDetailsForGarino($accountingServices)
    {
        $items = [];
        $position = 1;
        foreach ($accountingServices as $index => $service) {
            $accountingService = $this->getAccountingService($service, $position);
            $items[] = $accountingService;
            $position++;
        }
        return $items;
    }
    
    
    
    
    
}