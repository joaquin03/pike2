<?php

namespace App\Helpers;


use App\Models\Accounting\Bill;
use App\Models\Accounting\Invoice;
use App\Models\Itinerary;
use App\Models\ItineraryProviderService;
use App\Models\Operation;
use App\Models\Utilities\Country;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportsGenerator
{
    
    protected $query;
    protected $request;
    
    
    public function __construct()
    {
        $this->query = ItineraryProviderService::query();
    }
    
    public function reportOperation(Request $request)
    {
        $this->request = $request;
        $this->query->select('itinerary_provider_services.*',
            DB::raw('REPLACE(operations.code, "PN", "") as code_number'))
            ->where('service_origin', 'Operation')
            ->with('service')->with('provider')
            
            ->with('itinerary')->with('itinerary.airport')->with('itinerary.operation')
            ->with('itinerary.operation.aircraft');
        
        $this->joinItinerary();
        $this->joinAirport();
        $this->joinOperation();
        $this->filters();
        
        if ($request->get('group_services')) {
            return $this->groupByOperationServicesProviders();
        }
        return $this->query->get()->unique();
    }
    
    
    
    
    public function reportProcurement(Request $request)
    {
        $this->request = $request;
        $this->query
            ->whereIn('service_origin', ['Operation', 'Procurement'])->orWhereNull('service_origin')
            ->with('service')->with('provider')->with('itinerary')->with('itinerary.operation');
        $this->joinItinerary();
        $this->joinAirport();
        $this->joinOperation();
        $this->filters();
        
        return $this->groupByOperationTotal();
    }
    
    private function joinItinerary()
    {
        $this->query->leftJoin('itineraries', function ($join) {
            $join->on('itinerary_provider_services.itinerary_id', 'itineraries.id');
        });
    }
    
    private function joinAirport()
    {
        $this->query->leftJoin('airports', function ($join) {
            $join->on('itineraries.airport_icao_string', 'airports.icao');
        });
    }
    
    private function joinOperation()
    {
        $this->query->leftJoin('operations', function ($join) {
            $join->select('operations.*', DB::raw('REPLACE(code, "PN", "") as code_number'));
            $join->on('itineraries.operation_id', 'operations.id');
        });
    }
    
    private function filters()
    {
        if ($this->request->get('icao', false)) {
            $this->query->whereIn('airports.icao', $this->request->get('icao'));
        }
        if ($this->request->get('country', false)) {
            $this->query->whereIn('airports.country', $this->request->get('country'));
        }
        
        if ($this->request->get('provider_id', false)) {
            $this->query->whereIn('itinerary_provider_services.provider_id', $this->request->get('provider_id'));
        }
        
        if ($this->request->get('service_id', false)) {
            $itineraries = Itinerary::whereIn('type', ['ETA', 'ATA'])
                ->whereNull('deleted_at')->get()->pluck('id');
            
            $this->query->whereIn('itinerary_provider_services.itinerary_id', $itineraries);
            //solo los atd y etd por tema de duplicados de handling
            
            $this->query->whereIn('itinerary_provider_services.service_id', $this->request->get('service_id'));
            
        }
        if ($this->request->get('aircraft_id', false)) {
            $this->query->whereIn('operations.aircraft_id', $this->request->get('aircraft_id'));
        }
        
        
        if ($this->request->get('pn_start', false)) {
            $pnStart = $this->removeStringFromNumber($this->request->get('pn_start'));
            $this->query->whereRaw('REPLACE(operations.code, "PN", "") >=' . $pnStart);
        }
        if ($this->request->get('pn_end', false)) {
            $pnEnd = $this->removeStringFromNumber($this->request->get('pn_end'));
            $this->query->whereRaw('REPLACE(operations.code, "PN", "") <=' . $pnEnd);
        }
        
        if ($this->request->get('date_start', false)) {
            $this->query->where('operations.start_date', '>=',
                Carbon::createFromFormat('d-m-Y', $this->request->get('date_start')));
        }
        if ($this->request->get('date_end', false)) {
            $this->query->where('operations.start_date', '<=',
                Carbon::createFromFormat('d-m-Y', $this->request->get('date_end')));
        }
    
        $this->query->whereNotIn('operations.state', ['Canceled', 'Deleted']);
        $this->query->where('itinerary_provider_services.parent_id', null);
        
    }
    
    private function removeStringFromNumber($string)
    {
        return preg_replace('~\D~', '', $string);
    }
    
    
    //Generate Report
    private function groupByOperationTotal()
    {
        $operationsG = $this->query->get()->groupBy('itinerary.operation.id');
        $operations = Operation::whereIn('id', $operationsG->keys())->with('aircraft')->get()->keyBy('id');
        
        foreach ($operationsG as $id => $itineraryProviderServiceList) {
            $providers = collect();
            $services = collect();
            $icao = collect();
            $countries = collect();
            $quantity = 0;
            
            foreach ($itineraryProviderServiceList as $item) {
                $operations[$id]->total = $item->getProcurementFinalCost();
                $icao->push($item->itinerary->airport->icao);
                $providers->push($item->provider->name);
                $services->push($item->getService()->name ?? '');
                $quantity = $quantity + $item->procurement_quantity;
                $countries->push(Country::getName($item->itinerary->airport->country));
            }
            $operations[$id]->service_names = implode(', ', $services->unique()->toArray());
            $operations[$id]->provider_names = implode(', ', $providers->unique()->toArray());
            $operations[$id]->icao_names = implode(', ', $icao->unique()->toArray());
            $operations[$id]->countries = implode(', ', $countries->unique()->toArray());
            $operations[$id]->procurement_quantity = $quantity;
        }
        return $operations;
    }
    
    //Operation
    private function groupByOperationServicesProviders()
    {
        $itineraryOperationGroup = $this->query->get()->groupBy('itinerary.operation_id');
        
        $list = collect();
        foreach ($itineraryOperationGroup as $itineraryProviderServiceList) {
            $providers = collect();
            $services = collect();
            $icao = collect();
            
            foreach ($itineraryProviderServiceList as $item) {
                $icao->push($item->itinerary->airport->icao);
                $providers->push($item->provider->name);
                $services->push($item->service->name ?? 'No service name');
            }
            
            $itineraryProviderServiceList[0]->service_names = implode(', ', $services->unique()->toArray());
            $itineraryProviderServiceList[0]->provider_names = implode(', ', $providers->unique()->toArray());
            $itineraryProviderServiceList[0]->icao_names = implode(', ', $icao->unique()->toArray());
            $list->push($itineraryProviderServiceList[0]);
        }
        
        return $list;
    }
    
    
    //Provider Statments
    
    public function reportInvoice(Request $request)
    {
        $this->request = $request;
        $this->query = Invoice::query()->where('is_active', 1);
        
        $this->filterInvoice();
        
        return $this->query->get();
        
    }
    
    private function filterInvoice()
    {
        if ($this->request->get('date_start', false)) {
            $this->query->where('due_date', '>=', $this->transformDate($this->request->get('date_start')));
        }
        if ($this->request->get('date_end', false)) {
            $this->query->where('due_date', '<=', $this->transformDate($this->request->get('date_end')));
        }
        if ($this->request->get('provider_id', false)) {
            $this->query->whereIn('company_id', $this->request->get('provider_id'));
        }
        if ($this->request->get('status', false)) {
            $this->query->whereIn('status', $this->request->get('status'));
        }
    }

    //Client statements

    public function reportBill(Request $request)
    {
        $this->request = $request;
        $this->query = Bill::query()->where('is_active', 1);

        $this->filterBill();

        return $this->query->get();

    }

    private function filterBill()
    {
        if ($this->request->get('date_start', false)) {
            $this->query->where('due_date', '>=', $this->transformDate($this->request->get('date_start')));
        }
        if ($this->request->get('date_end', false)) {
            $this->query->where('due_date', '<=', $this->transformDate($this->request->get('date_end')));
        }
        if ($this->request->get('client_id', false)) {
            $this->query->whereIn('company_id', $this->request->get('client_id'));
        }
        if ($this->request->get('status', false)) {
            $this->query->whereIn('status', $this->request->get('status'));
        }
    }
    
    private function transformDate($stringDate)
    {
        return Carbon::createFromFormat('d/m/Y', $stringDate);
    }
    
    
}