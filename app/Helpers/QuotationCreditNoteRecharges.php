<?php


namespace App\Helpers;

use App\Models\Quotation;
use Illuminate\Http\UploadedFile;
use App\Models\QuotationCreditNoteCharges;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Facades\Excel;

class QuotationCreditNoteRecharges
{
    protected $row;

    public function load(UploadedFile $file)
    {
        ini_set('memory_limit', '-1');
        Excel::load($file, function ($reader){
            foreach ($reader->all() as $index => $row) {
                $this->row = $row;
                $this->createRow();
            }
        });
    }

    private function createRow()
    {
        if ($this->row['to']) {
            $range = new QuotationCreditNoteCharges();
            $range->from = $this->row['from'];
            $range->to = $this->row['to'];
            $range->value = $this->row['value'];
            $range->save();
        }
    }


}