<?php

namespace App\Helpers;


use App\Models\Accounting\AccountingService;
use Illuminate\Http\Request;
use App\Models\Bill;
use App\Models\Utilities\Country;

abstract class GarinoSFLFileCreditNote extends GarinoSFLFile
{
    private static $array = [];
    
    public static function generate($debitNote)
    {
        $xmlstr = "<?xml version='1.0' encoding='UTF-8'?>" .
            "<CFE_Empresas_Type xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>" .
            "</CFE_Empresas_Type>";
        
        
        $xml = new \SimpleXMLElement($xmlstr);
        
        $array = [
            'CFE' => [
                'eFact' => [
                    'Encabezado' => self::getEncabezado($debitNote),
                    'Detalle' => self::getDetail($debitNote),
                    'Referencias' => self::getReferencia($debitNote)
                ]
            ],
            'Adenda' => self::getAdenda()
        ];
        $xmlInfo = self::toXml($array, "CFE version='1.0' xmlns='http://cfe.dgi.gub.uy'", $xml);
        
        return base64_encode($xmlInfo);
    }
    
    private static function getEncabezado($debitNote)
    {
        return [
            'idDoc' => self::getIdDoc($debitNote),
            'Emisor' => parent::getEmisor(),
            'Receptor' => parent::getReceptor($debitNote),
            'Totales' => self::getTotales($debitNote),
        ];
    }
    
    
    
    private static function getTotales($debitNote)
    {
        return [
            'TpoMoneda' => 'USD',
            'TpoCambio' => $debitNote->dollar_change,
            'MntNoGrv' => parent::formatNumber($debitNote->amount), //Total Monto - No Gravado
            'MntExpoyAsim' => '', //Total Monto - Exportación y asimiladas
            'MntImpuestoPerc' => 0, //Total Monto - Impuesto percibido
            'MntIVAenSusp' => 0, //Total Monto - IVA en suspenso
            'MntNetoIvaTasaMin' => 0, //Total Monto Neto - IVA Tasa mínima
            'MntNetoIVATasaBasica' => 0, //Total Monto Neto - IVA Tasa básica
            'MntNetoIVAOtra' => 0, //Total Monto Neto – IVA otra tasa
            'IVATasaMin' => 10,
            'IVATasaBasica' => 22,
            'MntIVATasaMin' => 0,
            'MntIVATasaBasica' => 0,
            'MntIVAOtra' => 0,
            'MntTotal' => parent::formatNumber($debitNote->amount),
            'MntTotRetenido' => '', //Total Monto Retenido/Percibido
            'TotCreditosFisc' => '',
            'CantLinDet' => self::getItemQuantity($debitNote), //Líneas
            'MontoNF' => 0, //Monto no Facturable
            'MntPagar' => parent::formatNumber($debitNote->amount),
        ];
    }
    
    public static function getReferencia($debitNote)
    {
        $referencias = [];
        foreach ($debitNote->billsAndDebitNotes as $index => $bill) {
            $referencias[] = self::getReferencias($bill, $index);
        }
        return $referencias;
    }
    
    public static function getReferencias($bill, $index)
    {
        
        return [
            'NroLinRef' => $index + 1, //Nº de línea o Nº Secuencial, +1 porque no puede empezar en 0
            'IndGlobal' => 0,
            'TpoDocRef' => $bill->pivot->cfe_type,
            'SerieCFERef' => self::getSerieRef($bill->id),
            'NroCFERef' => self::getNumeroRef($bill->id),
            'RazonRef' => $bill->pivot->reason,
            'FechaCFEref' => self::transformDate(date('Y/m/d'))
        ];
    }
    
    public static function getSerieRef($billId){
        $billNumber = '';
        if(isset(\App\Models\Accounting\Bill::findOrFail($billId)->bill_number)) {
            $billNumber = \App\Models\Accounting\Bill::findOrFail($billId)->bill_number;
        }
        return self::validateBillNumber($billNumber, 0);
    }
    
    public static function getNumeroRef($billId){
        $billNumber = '';
        if(isset(\App\Models\Accounting\Bill::findOrFail($billId)->bill_number)) {
            $billNumber = \App\Models\Accounting\Bill::findOrFail($billId)->bill_number;
        }
        return self::validateBillNumber($billNumber, 1);
    }
    
    public static function validateBillNumber($billNumber, $position){
        if($billNumber) {
            if( count(explode('-', $billNumber))!=2){
                return "Bill number malformed";
            }
            return explode('-',$billNumber)[$position];
        }
        return "No bill number";
    }
    
    private static function getIdDoc($debitNote)
    {
        
        return [
            'TipoCFE' => '102',
            'Serie' => '', //Garino
            'Nro' => '', //Garino
            'FchEmis' => $debitNote->created_at != null ? self::transformDate($debitNote->created_at->format('Y/m/d')) : '',
            'PeriodoDesde' => $debitNote->start_date != null ? self::transformDate($debitNote->start_date->format('Y/m/d')) : '',
            'PeriodoHasta' => $debitNote->due_date != null ? self::transformDate($debitNote->due_date->format('Y/m/d')) : '',
            'MntBruto' => 1,
            'FmaPago' => 2, // 1 => contado, 2=>Credito
            'FchVenc' => $debitNote->due_date != null ? self::transformDate($debitNote->due_date->format('Y/m/d')) : '',
            'IndTipoTraslado' => 1,
            'ClauVenta' => '',
            'ModVenta' => '',
            'ViaTransp' => 2, //Vía de transporte
            'InfoAdicional' => '', //OPCIONAL
        
        ];
        
    }
    
    private static function cleanText($array)
    { //quita caracteres especiales de XML
        return str_replace('&', 'and', $array);
    }
    
    public static function toXml($data, $rootNodeName, $xml = null, $type = '')
    {
        // turn off compatibility mode as simple xml throws a wobbly if you don't.
        if (ini_get('zend.ze1_compatibility_mode') == 1) {
            ini_set('zend.ze1_compatibility_mode', 0);
        }
        
        if ($xml == null) {
            $xml = simplexml_load_string("<?xml version='1.0' encoding='utf-8'?><$rootNodeName />");
        }
        
        // loop through the data passed in.
        foreach ($data as $key => $value) {
            // no numeric keys in our xml please!
            
            if ($key === "Detalle") {
                $type = 'Item';
            }
            if ($key === "Referencias") {
                $type = 'Referencia';
            }
            
            if (is_numeric($key)) {
                // make string key...
                $key = $type . (string)$key;
            }
            
            // replace anything not alpha numeric
            $key = preg_replace('/[^a-z]/i', '', $key);
            
            // if there is another array found recrusively call this function
            if (is_array($value)) {
                $node = $xml->addChild($key);
                // recrusive call.
                self::toXml($value, $rootNodeName, $node, $type);
            } else {
                // add single node.
                $value = htmlentities($value);
                $xml->addChild($key, $value);
            }
            
        }
        // pass back as string. or simple xml object if you want!
        return $xml->asXML();
    }
    
    private static function getDetail($debitNote)
    {
        $items = [];
        foreach ($debitNote->billsAndDebitNotes as $index => $bill) {
            $items[] = self::getBillsDetails($bill->pivot, $index);
        }
        
        return $items;
    }
    
    private static function getBillsDetails($pivot, $index) //pasar pivot
    {
        return [
            'NroLinDet' => $index + 1, //Nº de línea o Nº Secuencial, +1 porque no puede empezar en 0
            'CodItem' => $pivot->bill_id,
            'IndFact' => 1, //Indicador de facturación
            'NomItem' => "Bill " . \App\Models\Accounting\Bill::find($pivot->bill_id)->bill_number . " - " . $pivot->reason ,
            'Cantidad' => 1,
            'UniMed' => 'USD',  //Unidad de medida
            'PrecioUnitario' => parent::formatNumber($pivot->amount),
            'MontoItem' => parent::formatNumber($pivot->amount),
        ];
    }
    
    private static function getItemQuantity($debitNote)
    {
        return count($debitNote->billsAndDebitNotes);
    }
    
    private static function transformDate($date)
    {
        return str_replace('/', '', $date);
    }
    
    
}