<?php

namespace App\Helpers;


use App\Models\Accounting\AccountingService;
use Illuminate\Http\Request;
use App\Models\Bill;
use App\Models\Utilities\Country;

class GarinoSFLFileBill extends GarinoSFLFile
{
    protected $array = [];
    
    
    protected function getIdDoc($bill)
    {
        return [
            'TipoCFE' => '101',
            'Serie' => '', //Garino
            'Nro' => '', //Garino
            'FchEmis' => $bill->start_date != null ? self::transformDate($bill->start_date->format('Y/m/d')) : '',
            'PeriodoDesde' => $bill->start_date != null ? self::transformDate($bill->start_date->format('Y/m/d')) : '',
            'PeriodoHasta' => $bill->due_date != null ? self::transformDate($bill->due_date->format('Y/m/d')) : '',
            'MntBruto' => 1,
            'FmaPago' => $bill->payment_method, // 1 => contado, 2=>Credito
            'FchVenc' => $bill->due_date != null ? self::transformDate($bill->due_date->format('Y/m/d')) : '',
            'IndTipoTraslado' => 1,
            'ClauVenta' => '',
            'ModVenta' => '',
            'ViaTransp' => 2, //Vía de transporte
            'InfoAdicional' => '', //OPCIONAL
        
        ];
        
    }
    
    protected function getAccountingService(AccountingService $service, $index)
    {
        return [
            'NroLinDet' => $index + 1, //Nº de línea o Nº Secuencial, +1 porque no puede empezar en 0
            'CodItem' => $service->id,
            'IndFact' => 1, //Indicador de facturación
            'NomItem' => self::cleanText($service->description),
            'DscItem' => '',
            'Cantidad' => $service->procurement_quantity,
            'UniMed' => self::getMeasurement($service),  //Unidad de medida
            'PrecioUnitario' => self::getUnitPrice($service),
            'MontoItem' => round($service->getServiceFinalPrice(),3),
        ];
    }
    
    
    public function toXml($data, $rootNodeName, $xml = null)
    {
        // turn off compatibility mode as simple xml throws a wobbly if you don't.
        if (ini_get('zend.ze1_compatibility_mode') == 1) {
            ini_set('zend.ze1_compatibility_mode', 0);
        }
        
        if ($xml == null) {
            $xml = simplexml_load_string("<?xml version='1.0' encoding='utf-8'?><$rootNodeName />");
        }
        
        // loop through the data passed in.
        foreach ($data as $key => $value) {
            // no numeric keys in our xml please!
            if (is_numeric($key)) {
                // make string key...
                $key = "Item";
            }
            
            // replace anything not alpha numeric
            $key = preg_replace('/[^a-zA-Z0-9]/', '', $key);
    
            // if there is another array found recrusively call this function
            if (is_array($value)) {
                $node = $xml->addChild($key);
                // recrusive call.
                self::toXml($value, $rootNodeName, $node);
            } else {
                // add single node.
                $value = htmlentities($value);
                $xml->addChild($key, $value);
            }
            
        }
        
    
        // pass back as string. or simple xml object if you want!
        return $xml->asXML();
    }
    
    protected function getDetail($bill)
    {
        $items = collect();
        foreach ($bill->accountingServices as $index => $accountingService) {
            
            $auxItem = $items->where('description', $accountingService->description)->first();
            if ($auxItem == null) {
                $items->offsetSet($accountingService->id, $accountingService);
            } else {
                $actual = $items[$auxItem->id];
                if ($actual->billing_unit_price == $accountingService->billing_unit_price) {
                    $items[$auxItem->id]->procurement_quantity += $accountingService->procurement_quantity;
                }
            }
        }
        return self::parseDetailsForGarino($items);
    }
    
    public function parseDetailsForGarino($accountingServices)
    {
        $items = [];
        $position = 1;
        foreach ($accountingServices as $index => $service) {
            $accountingService = self::getAccountingService($service, $position);
            $items[] = $accountingService;
            $position++;
        }
        return $items;
    }
    
    protected function getItemQuantity($bill)
    {
        return count($bill->accountingServices);
    }
    
    protected function getUnitPrice($service) {
        return $service->billing_unit_price ?? $service->procurement_cost;
    }
    
    protected function getFinalPrice($service) {
        return  $service->getServiceFinalPrice();
    }
    
    
    
    protected function transformDate($date) {
        return str_replace('/', '', $date);
    }
    
    protected function getMeasurement($service) {
        $measurement = 'N/A';
        if (strtolower($service->description) === 'fuel') {
            $measurement = 'USG';
        }
        return $measurement;
    }
    
}