<?php

namespace App\Helpers;


use App\Exceptions\ValidationCustomException;
use App\Models\Bill;
use Spatie\ArrayToXml\ArrayToXml;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use RuntimeException;
use Config;


class GarinoGenerator
{
    
    private static $garinoHeader = [
        'Content-Type' => 'text/xml; charset=UTF-8',
        'Accept-Encoding' => 'gzip,deflate',
        'SOAPAction' => ' "http://tempuri.org/IOnlineSpocCloudService/SendOnLine"',
        'Content-Length' => '3737',
        'Host' => 'testportal.transactionalnetwork.com:445',
        'User-Agent' => 'Apache-HttpClient/4.5.2 (Java/1.8.0_162)'
    ];
    
    public static function generate($bill)
    {
        if (config('billingInfo.garino_disabled')) {

            return ['url' => 'http://pike-aviation.com',
                'bill_number' => 'Pike-'.str_pad($bill->id, 6, 0, STR_PAD_LEFT)];
        }

        self::billValidations($bill);

        $request = self::generateRequest($bill);

        $garinoResponse = self::sendRequest($request);

        $garinoResponse = self::decodeResponse($garinoResponse);

        self::validateResponse($garinoResponse);

        return self::createResponse($garinoResponse);
    }
    
    //Steps
    private static function billValidations($bill)
    {
        self::validateDolarChange($bill);
        self::validateMaxAmount($bill);
        self::validateLengthAddress($bill);
    }
    
    private static function generateRequest($bill)
    {
        $xml = self::generateXML($bill);
    
        return new Request('POST', env('GARINO_URL'), self::$garinoHeader, $xml);
    }
    
    private static function sendRequest($request)
    {
        $client = new Client();
        try {
            $response = $client->send($request); 
        }
        catch (\GuzzleHttp\Exception\ConnectException $e){
            Bugsnag::notifyException(new RuntimeException("Error while connecting with GARINO Servers"));
            throw new ValidationCustomException("Error while connecting to billing service provider, please try again later.");
        }
        catch (\Exception $e) {
            Bugsnag::notifyException('Garino bill generate error: '.new RuntimeException($e));
            throw new ValidationCustomException('Unknown error: '.(string)get_class($e));
        }
        
        return $response;
    }
    
    private static function decodeResponse($response)
    {
    
        $xmlResponse = str_replace("s:", '', $response->getBody()->getContents()); // para que quede con formato correcto
    
        $response = base64_decode(simplexml_load_string($xmlResponse)->Body->SendOnLineResponse->respuesta);
    
        $response = explode('||', $response);
        
        return $response;
    }
    
    private static function validateResponse($response)
    {
        if ($response[4] === "Anulado") {
            Bugsnag::notifyException(new RuntimeException("Error, ticket validation failed by DGI."));
            throw new ValidationCustomException("Error, ticket validation failed by DGI.");
        }
        if ($response[4] === "Rechazado" || $response[4] === "Descartado") {
            $errorString = "Desctiption: " .$response[18] . ", " . $response[19];
            Bugsnag::notifyException(new RuntimeException($errorString));
            throw new ValidationCustomException($errorString);
        }
    }
    
    private static function createResponse($garinoResponse)
    {
        $url = $garinoResponse[8];
        $bill_number = $garinoResponse[6] . "-" . $garinoResponse[7];
        
        
        return ['url' => $url, 'bill_number' => $bill_number];
    }
    
    //Helpers
    private static function generateXML($bill)
    {
        $array = [
            'soapenv:Header' => [],
            'soapenv:Body' => [
                'tem:SendOnLine' => [
                    'tem:rut' => config('billingInfo.RUCEmisor'),
                    'tem:sucursal' => config('billingInfo.Sucursal'),
                    'tem:nroTerminal' => config('billingInfo.NroTerminal'),
                    'tem:documentoSFL' => $bill->getDocumentoSFL(),
                    
                    'tem:username' => config('billingInfo.user.username'),
                    'tem:password' => config('billingInfo.user.password'),
                ],
            ],
        ];
        $xmlInfo = ArrayToXml::convert($array);
        $xmlInfo = str_replace("/root", "/soapenv:Envelope", $xmlInfo);
        $xmlInfo = str_replace("root",
            "soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:tem='http://tempuri.org/'",
            $xmlInfo);
        
        return $xmlInfo;
    }
    
    private static function getCompanyDocNumber($bill){
        return $bill->billingCompany->document_receptor;
    }
    
    private static function validateDolarChange($bill)
    {
        if ($bill->dollar_change == 0) {
            throw new ValidationCustomException("Error, the dollar change can not be zero.");
        }
    }
    
    private static function validateMaxAmount($bill)
    {
        $maxAmount = ((Config::get('constants.unidadIndexada') * 10000) - 500) / $bill->dollar_change;
    
        if( ($bill->amount > $maxAmount ) && self::getCompanyDocNumber($bill) === null ){
            throw new ValidationCustomException(
                "Error, the amount is over 10.000 UI. To perform this operation,
                the company needs to have document number. ");
        }
    }
    
    private static function validateLengthAddress($bill)
    {
        if (strlen($bill->billingCompany->address_receptor) > 70) {
            throw new ValidationCustomException("Error, the company address is too long. DGI only accepts 70 chars,
            this address has ".strlen($bill->billingCompany->address_receptor));
        }
    }
    
    
}