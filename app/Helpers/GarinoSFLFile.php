<?php

namespace App\Helpers;


use App\Models\Accounting\AccountingService;
use App\Models\Accounting\Bill;
use Illuminate\Http\Request;
use App\Models\Utilities\Country;

abstract class GarinoSFLFile
{
    private static $array = [];
    
    /**
     * OperationSearch constructor.
     */
    public static function generate($bill)
    {
        $xmlstr = "<?xml version='1.0' encoding='UTF-8'?>" .
            "<CFE_Empresas_Type xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>" .
            "</CFE_Empresas_Type>";
        
        
        $xml = new \SimpleXMLElement($xmlstr);
        
        $array = [
            'CFE' => [
                'eFact' => [
                    'Encabezado' => self::getEncabezado($bill),
                    'Detalle' => self::getDetail($bill),
                ]
            ],
            'Adenda' => self::getAdenda($bill)
        ];
        
        $xmlInfo = self::toXml($array, "CFE version='1.0' xmlns='http://cfe.dgi.gub.uy'", $xml);
        
        return $xmlInfo;
    }
    
    public static function toXml($data, $rootNodeName, $xml = null)
    {
        // turn off compatibility mode as simple xml throws a wobbly if you don't.
        if (ini_get('zend.ze1_compatibility_mode') == 1) {
            ini_set('zend.ze1_compatibility_mode', 0);
        }
        
        if ($xml == null) {
            $xml = simplexml_load_string("<?xml version='1.0' encoding='utf-8'?><$rootNodeName />");
        }
        
        // loop through the data passed in.
        foreach ($data as $key => $value) {
            // no numeric keys in our xml please!
            if (is_numeric($key)) {
                // make string key...
                $key = "Item" . (string)$key;
            }
            
            // replace anything not alpha numeric
            $key = preg_replace('/[^a-zA-Z0-9]/', '', $key);
            
            // if there is another array found recrusively call this function
            if (is_array($value)) {
                $node = $xml->addChild($key);
                // recrusive call.
                self::toXml($value, $rootNodeName, $node);
            } else {
                // add single node.
                $value = htmlentities($value);
                $xml->addChild($key, $value);
            }
            
        }
        // pass back as string. or simple xml object if you want!
        return $xml->asXML();
    }
    
    private static function getEncabezado($bill)
    {
        return [
            'idDoc' => self::getIdDoc($bill),
            'Emisor' => self::getEmisor(),
            'Receptor' => self::getReceptor($bill),
            'Totales' => self::getTotales($bill),
        ];
    }
    
    protected static function getEmisor()
    {
        return config('emisorInfo');
    }
    
    public static function getReceptor($bill)
    {
        return [
            'TipoDocRecep' => 4, // $bill->company->doc_type_receptor,  5 es psaporte para todos los paises
            'CodPaisRecep' => $bill->company->country, //PASAR A CODIGO - EJ UY
            'DocRecep' => $bill->company->document_receptor, //RUT o doc extranjero
            'RznSocRecep' => self::cleanText($bill->company->business_name_receptor),
            'DirRecep' => self::cleanText($bill->company->address_receptor),
            'CiudadRecep' => self::cleanText($bill->company->city_receptor),
            'DeptoRecep' => self::cleanText($bill->company->state_receptor),
            'PaisRecep' => Country::getName($bill->company->country) ?? '',
            'InfoAdicional' => '',
            //   'CP' => $bill->company->postal_code_receptor, solo para uruguay
            'CompraID' => $bill->id,
        ];
    }
    
    private static function getTotales($bill)
    {
        return [
            'TpoMoneda' => 'USD',
            'TpoCambio' => $bill->dollar_change ?? Bill::getLastItemOfTheDay(),
            'MntNoGrv' => self::formatNumber($bill->amount), //Total Monto - No Gravado
            'MntExpoyAsim' => '', //Total Monto - Exportación y asimiladas
            'MntImpuestoPerc' => 0, //Total Monto - Impuesto percibido
            'MntIVAenSusp' => 0, //Total Monto - IVA en suspenso
            'MntNetoIvaTasaMin' => 0, //Total Monto Neto - IVA Tasa mínima
            'MntNetoIVATasaBasica' => 0, //Total Monto Neto - IVA Tasa básica
            'MntNetoIVAOtra' => 0, //Total Monto Neto – IVA otra tasa
            'IVATasaMin' => 10,
            'IVATasaBasica' => 22,
            'MntIVATasaMin' => 0,
            'MntIVATasaBasica' => 0,
            'MntIVAOtra' => 0,
            'MntTotal' => self::formatNumber($bill->amount),
            'MntTotRetenido' => '', //Total Monto Retenido/Percibido
            'TotCreditosFisc' => '',
            'CantLinDet' => self::getItemQuantity($bill), //Líneas
            'MontoNF' => 0, //Monto no Facturable
            'MntPagar' => self::formatNumber($bill->amount),
        ];
    }
    
    public static function formatNumber($number)
    {
        $number = str_replace('.', ',', $number); // cambio .s por ,s
        return number_format((float)$number, 2, ',', ''); //devuelvo con 2 cifras despuesde la ,
    }
    
    public static function getReferencia()
    {
        return;
    }
    
    
    public static function getAdenda()
    {
        return [
            'Texto' => '**Transfers USD (Dolar) | Banco Santander S.A.!! @@ @@ **Beneficiary Bank:!! BANK SANTANDER S.A. Address: Cerrito 449. CP11000 Montevideo, Uruguay. Swift : BSCHUYMM @@ **Benefeciary Name:!! Pike Aviation S.A. Address: Dr. Alejandro Schroeder 6475 of 101 Montevideo, Uruguay Account: 5100338698 @@ **Intermediary Bank:!! WELLS FARGO Swift code :  PNBPUS3NNYC ABA :  #026005092 @@ @@  Please contact:billing@pike-aviation.com with any inquiries or question within 10 days from the invoice date. After that period we consider that it has been accepted with nothing to claim.',
            'TipoInterno' => '',
            'SerieInterna' => '',
            'NroInterno' => '',
        ];
    }
    
    private static function getIdDoc($bill)
    {
        
        return [
            'TipoCFE' => '102',
            'Serie' => '', //Garino
            'Nro' => '', //Garino
            'FchEmis' => $bill->created_at != null ? self::transformDate($bill->created_at->format('Y/m/d')) : '',
            'PeriodoDesde' => $bill->start_date != null ? self::transformDate($bill->start_date->format('Y/m/d')) : '',
            'PeriodoHasta' => $bill->due_date != null ? self::transformDate($bill->due_date->format('Y/m/d')) : '',
            'MntBruto' => 1,
            'FmaPago' => 2, // 1 => contado, 2=>Credito
            'FchVenc' => $bill->due_date != null ? self::transformDate($bill->due_date->format('Y/m/d')) : '',
            'IndTipoTraslado' => '',
            'ClauVenta' => '',
            'ModVenta' => '',
            'ViaTransp' => '', //Vía de transporte
            'InfoAdicional' => '', //OPCIONAL
        
        ];
        
    }
    
    private static function getAccountingService(AccountingService $service, $index)
    {
        return [
            'NroLinDet' => $index + 1, //Nº de línea o Nº Secuencial, +1 porque no puede empezar en 0
            'CodItem' => $service->id,
            'IndFact' => 1, //Indicador de facturación
            'NomItem' => self::cleanText($service->name),
            'DscItem' => self::cleanText($service->description),
            'Cantidad' => self::formatNumber($service->procurement_quantity),
            'UniMed' => 'USD',  //Unidad de medida
            'PrecioUnitario' => self::getUnitPrice($service),
            'MontoItem' => round($service->billing_final_price,3),
        ];
    }
    
    private static function cleanText($s)
    { //quita caracteres especiales de XML
        $a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ', 'Ά', 'ά', 'Έ', 'έ', 'Ό', 'ό', 'Ώ', 'ώ', 'Ί', 'ί', 'ϊ', 'ΐ', 'Ύ', 'ύ', 'ϋ', 'ΰ', 'Ή', 'ή');
        $b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o', 'Α', 'α', 'Ε', 'ε', 'Ο', 'ο', 'Ω', 'ω', 'Ι', 'ι', 'ι', 'ι', 'Υ', 'υ', 'υ', 'υ', 'Η', 'η');
        $s = str_replace($a, $b, $s);
        $s = str_replace("&", " and ", $s);
        //para ampliar los caracteres a reemplazar agregar lineas de este tipo:
        //$s = str_replace("caracter-que-queremos-cambiar","caracter-por-el-cual-lo-vamos-a-cambiar",$s);
        return $s;
        
    }
    
    private static function getDetail($bill)
    {
        $items = [];
        foreach ($bill->accountingServices as $index => $service) {
            $items[] = self::getAccountingService($service, $index);
        }
        
        return $items;
    }
    
    private static function getItemQuantity($bill)
    {
        return count($bill->accountingServices);
    }
    
    private static function getUnitPrice($service)
    {
        return self::formatNumber($service->procurement_quantity != 0 ? round($service->billing_final_price /
            $service->procurement_quantity, 3) : 0);
    }
    
    
    private static function transformDate($date)
    {
        return str_replace('/', '', $date);
    }
    
    
}