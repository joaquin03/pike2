<?php
/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 3/13/19
 * Time: 12:37 PM
 */

namespace App\Helpers\Fuel;


use App\Models\Airport;
use App\Models\FuelPrices;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Facades\Excel;


abstract class FuelLoader
{
    protected $provider;
    protected $row;
    protected $batNumber;
    
    public function load(UploadedFile $file)
    {
        ini_set('memory_limit', '-1');
        $this->batNumber = FuelPrices::getLastBatNumber($this->getProvider())+1;
        
        Excel::load($file, function ($reader){
            foreach ($reader->all() as $index => $row) {
                $this->row = $row;
                if ($this->getIcao() == null) {
                    break;
                }
                $this->createRow();
            }
        });
    }
    
    private function createRow()
    {
        $fuelPrice = new FuelPrices();
        $fuelPrice->icao            = $this->getIcao();
        $fuelPrice->provider        = $this->getProvider();
        $fuelPrice->min_quantity    = $this->getMinQuantity();
        $fuelPrice->max_quantity    = $this->getMaxQuantity();
        $fuelPrice->price           = $this->getPrice();
    
        $fuelPrice->date_effective  = $this->getDateEffective();
        $fuelPrice->date_valid      = $this->getDateValid();
        $fuelPrice->notes           = $this->getNotes();
        $fuelPrice->fuel_provider   = $this->getFuelProvider();
    
        $fuelPrice->bat_number      = $this->batNumber;
        $fuelPrice->save();
    }
    
    abstract public function getProvider();
    
    abstract public function getIcao();
    
    abstract public function getMinQuantity();
    abstract public function getMaxQuantity();
    abstract public function getPrice();
    abstract public function getDateEffective();
    abstract public function getDateValid();
    abstract public function getNotes();
    
    abstract public function getFuelProvider();
    
}