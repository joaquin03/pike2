<?php
/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 3/14/19
 * Time: 11:57 AM
 */

namespace App\Helpers\Fuel;


class AEGFuelLoader extends FuelLoader
{
    
    public function getProvider()
    {
        return "AEG";
    }
    
    public function getIcao()
    {
        return $this->row['icao'];
    }
    
    public function getMinQuantity()
    {
        $data = explode('-', $this->row['gallons1']);
        if (array_key_exists(0, $data)){
            return $data[0];
        }
        return 0;
    }
    public function getMaxQuantity()
    {
        $data = explode('-', $this->row['gallons1']);
        if (array_key_exists(1, $data)){
            return $data[1];
        }
        return 0;
    }
    
    public function getPrice()
    {
        return is_numeric($this->row['total_price1'])? $this->row['total_price1'] : null;
    }
    
    public function getDateEffective()
    {
        return "";
    }
    public function getDateValid()
    {
        return $this->row['updated'];
    }
    public function getNotes()
    {
        return $this->row['notes'];
    }
    
    public function getFuelProvider()
    {
        return $this->row['fueler'];
    }
}