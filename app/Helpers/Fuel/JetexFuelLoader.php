<?php
/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 3/14/19
 * Time: 11:57 AM
 */

namespace App\Helpers\Fuel;


class JetexFuelLoader extends FuelLoader
{
    
    public function getProvider()
    {
        return "Jetex";
    }
    
    public function getIcao()
    {
        return $this->row['icao'];
    }
    
    public function getMinQuantity()
    {
        $data = explode('-', str_replace("'", '', $this->row['qnty']) );
        return $data[0];
    }
    public function getMaxQuantity()
    {
        $data = explode('-', str_replace("'", '', $this->row['qnty']) );
        return $data[1];
    }
    
    public function getPrice()
    {
        return is_numeric($this->row['cost'])? $this->row['cost'] : null;
    }
    
    public function getDateEffective()
    {
        return str_replace("'", '', $this->row['effectivedate']);
    }
    public function getDateValid()
    {
        return str_replace("'", '', $this->row['validdate']);
    }
    public function getNotes()
    {
        return $this->row['notes_remarks'];
    }
    
    public function getFuelProvider()
    {
        return $this->row['intoplane'];
    }
}