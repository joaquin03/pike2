<?php
/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 3/18/19
 * Time: 2:14 AM
 */

namespace App\Helpers\Fuel;


class EpicFuelLoader extends FuelLoader
{
    
    public function getProvider()
    {
        return "Epic Fuel";
    }
    
    public function getIcao()
    {
        return $this->row['icao'];
    }
    
    public function getMinQuantity()
    {
        return $this->row['min_gal'];
    }
    public function getMaxQuantity()
    {
        return 99999;
    }
    
    public function getPrice()
    {
        return $this->row['pricegal'];
    }
    
    public function getDateEffective()
    {
        return $this->row['eff_date'];
    }
    public function getDateValid()
    {
        return "";
    }
    public function getNotes()
    {
        return "Fuel tipe:".$this->row['fuel_type'];
    }
    
    public function getFuelProvider()
    {
        return $this->row['fbo_name'];
    }
    
}