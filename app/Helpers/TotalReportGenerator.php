<?php

namespace App\Helpers;


use App\Models\Accounting\Bill;
use App\Models\Accounting\Invoice;
use Carbon\Carbon;

class TotalReportGenerator
{
    
    public function expenses($request)
    {
        $query = Invoice::query()->where('is_active', 1);
    
        $query = $this->dateFilter($query, $request);
    
        return round($query->sum('amount'), 3);
    }
    
    public function profits($request)
    {
        $query = Bill::query()->where('is_active', 1);
    
        $query = $this->dateFilter($query, $request);
    
        return round($query->sum('amount'), 3);
    }
   
    
    public function expensesByMonth($request)
    {
        $query = Invoice::query()->where('is_active', 1);
    
        $query->select(\DB::raw('DATE_FORMAT(due_date, \'%m/%Y\') as month_date, ROUND(SUM(amount), 3) as total'));
        
        $query = $this->dateFilter($query, $request);
        
        $query->groupBy('month_date');
        $query->orderBy('month_date');
    
        return $query->pluck('total', 'month_date');
    }
    
    public function profitsByMonth($request)
    {
        $query = Bill::query()->where('is_active', 1);
    
        $query->select(\DB::raw('DATE_FORMAT(due_date, \'%m/%Y\') as month_date, ROUND(SUM(amount), 3) as total'));
    
        $query = $this->dateFilter($query, $request);
    
        $query->groupBy('month_date');
        $query->orderBy('month_date');
        $this->monthBetween($request);
        return $query->pluck('total', 'month_date');
    }
    
    public function totalsByMoth($request)
    {
        $expenses = $this->expensesByMonth($request);
        $profits = $this->profitsByMonth($request);
        $month = $this->monthBetween($request);
    
        $totalMonth['expenses'] = [];
        $totalMonth['profits'] = [];
        
        foreach ($month as $date => $monthName) {
            $totalMonth['expenses'][] = $expenses[$date] ?? 0;
            $totalMonth['profits'][] = $profits[$date] ?? 0;
            $totalMonth['months'][] = $monthName;
            $totalMonth['total'][] = ($profits[$date]?? 0) - ($expenses[$date]?? 0);
        }
        
        return $totalMonth;
    }
    
    private function monthBetween($request)
    {
        $start = $this->transformDate($request->date_start);
        $end = $this->transformDate($request->date_end);
        $count = $start->diffInMonths($end);
        $month = collect();
        for ($i=0; $i<= $count; $i++) {
            $month[$start->format('m').'/'.$start->year] = $start->format('F');
            $start->addMonth();
        }
        return $month;
    }
    
    private function dateFilter($query, $request)
    {
        if ($request->get('date_start', false)) {
            $query->where('due_date', '>=', $this->transformDate($request->date_start));
        }
        if ($request->get('date_end', false)) {
            $query->where('due_date', '<=', $this->transformDate($request->date_end));
        }
        return $query;
    }
    
    private function transformDate($stringDate)
    {
        return Carbon::createFromFormat('d/m/Y', $stringDate);
    }
    
}