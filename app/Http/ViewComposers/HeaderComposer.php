<?php
/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 11/28/16
 * Time: 3:24 PM
 */

namespace App\Http\ViewComposers;

use App\Models\ActivityReminder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class HeaderComposer
{

	protected $reminder;


	public function __construct(ActivityReminder $reminder)
	{
		$this->reminder = $reminder;
	}

	/**
	 * Bind data to the view.
	 *
	 * @param  View  $view
	 * @return void
	 */
	public function compose(View $view)
	{
		$user = Auth::user();
		if ($user) {
            $reminders = $user->getThisWeekReminders();
        } else {
		    $reminders=new Collection();
        }
        
        $view->with('dashboardReminders', $reminders);
	}
}