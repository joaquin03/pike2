<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckPermissionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        
        //if (!$this->validateMainModule($request)) {
        //    return redirect('/admin/dashboard')
        //        ->withErrors( 'Error! User management not allowed. Contact your admin for permissions.');
        //}
    
        return $next($request);
    }
    
    private function getMianModule($request)
    {
        $action = explode('.', $request->route()->getName());
        return $action[1] ?? '';
    }
    
    private function validateMainModule($request)
    {
        return true;
        $user = Auth::user();
      
        $mainAction = $this->getMianModule($request);
        //$user->allowedTo($mainAction);
        if ($mainAction != '') {
            return true;
            //return $user->hasPermissionTo($mainAction) || $user->hasRole($mainAction);
        }
        return true;
    }
    
}
