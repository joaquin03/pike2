<?php
namespace App\Http\Transformers\Accounting;
use App\Http\Transformers\AbstractTransformer;
use App\Models\Accounting\AccountingService;

class AccountingServicesTransformer extends AbstractTransformer
{
    protected $amount;
    public function __construct($amount)
    {
        $this->amount = $amount;
    }
    
    public function transform(AccountingService $item)
    {
        return [
            'id'                    => $item->id,
            'description'           => $item->getDescription(),
            'procurement_cost'      => $item->procurement_cost,
            'procurement_quantity'  => $item->procurement_quantity,
            'procurement_tax'       => $item->procurement_tax,
            'procurement_adm_fee'   => $item->procurement_adm_fee,
            'procurement_unit_final_cost' => round( $item->procurement_unit_final_cost, 3),
            'procurement_final_cost'      => round( $item->procurement_final_cost,3),
            
            'billing_quantity'      => $item->billing_quantity,
            'billing_percentage'    => $item->billing_percentage,
            'billing_unit_price'    => $item->billing_unit_price,
            'billing_final_price'   => $item->billing_final_price,
            'servicesable_type'     => $item->servicesable_type
        ];
    }
    
    protected function changeItemsList($data)
    {
        $lastLine = [
            'id'                    => '',
            'description'           => 'TOTAL',
            'procurement_cost'      => '',
            'procurement_quantity'  => '',
            'procurement_tax'       => '',
            'procurement_adm_fee'   => '',
            'procurement_unit_final_cost' => '',
            'procurement_final_cost'=> round($this->amount,3),
            'billing_quantity'      => '',
            'billing_percentage'    => '',
            'billing_unit_price'    => '',
            'billing_final_price'   => $this->amount,
            'servicesable_type'     => '',

        ];
        $data['data'][] = $lastLine;
        return $data;
    }
    
   
    
}