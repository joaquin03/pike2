<?php

namespace App\Http\Transformers\Accounting;


use App\Http\Transformers\AbstractTransformer;
use App\Models\Accounting\CompanyStatement;
use App\Models\Contracts\CompanyStatementInterface;
use Illuminate\Support\Facades\DB;

class CompanyStatementTransformer extends AbstractTransformer
{
    protected $type;
  
    
    public function __construct($type = 'client')
    {
        $this->type = $type;

    }

    public function transform(CompanyStatementInterface $company)
    {
        return [
            'company'               => $company->toArray(),
            'accountingStatement'   => $this->includeAccountingStatement($company->getAccountingItems()),
            'type'                  => $this->type,
        ];
    }
    
    private function includeAccountingStatement($items)
    {
        $transformer = new AccountingItemsTransformer();
        $items = $transformer->transformItems($items);
        $items = collect($items['data'])->sortBy('ref_number')->sortBy('order_date')->toArray();
        
        return ['data'=> $this->setBalance($items)];
    }
    
    private function setBalance($items)
    {
        $balance = 0;
        foreach ($items as $index => $item) {
            $balance += (double) $item['debit'] - (double) $item['credit'];
            $items[$index]['balance'] = round($balance, 3);
        }
        return $items;
        
    }
    
    
}