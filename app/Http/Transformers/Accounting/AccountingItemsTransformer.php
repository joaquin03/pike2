<?php
/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 3/22/18
 * Time: 1:54 PM
 */

namespace App\Http\Transformers\Accounting;


use App\Http\Transformers\AbstractTransformer;
use App\Models\Accounting\Bill;
use App\Models\Accounting\DebitNote;
use App\Models\Aircraft;
use App\Models\Accounting\Invoice;
use App\Models\Accounting\Payment;
use App\Models\Contracts\AccountingItem;

class AccountingItemsTransformer extends AbstractTransformer
{
    protected $balance = 0;
    
    public function transform(AccountingItem $item)
    {
        if ($item instanceof DebitNote) {

            return $this->transformDebitNote($item);
        }
        
        if ($item instanceof Bill || $item instanceof Invoice) {
            return $this->transformBill($item);
        }
        if ($item instanceof Payment) {
            return $this->transformPayment($item);
        }
    }
    
    private function transformDebitNote($debitNote)
    {
        
        
        $this->updateBalance($debitNote->amount);
        return [
            'id' => $debitNote->id,
            'date' => $debitNote->start_date ? $debitNote->start_date->format('d-m-Y') : $debitNote->date->format('d-m-Y'),
            'type' => $debitNote->getDescription(),
            'ref_number' => $debitNote->bill_number,
            'flt_date' => '',
            'status' => $debitNote->getStatus(),
            'aircraft' => Aircraft::find($debitNote->aircraft_id)->registration ?? '',
            'observations' => $debitNote->note,
            'debit' => round($debitNote->amount,2),
            'credit' => '',
            'balance' => round($this->balance,2),
            'icao' => $debitNote->itinerary->airport_icao_string ?? '',
            'operation_id' => '',
            'pike_number' => '',
            'due_date' => ($debitNote->due_date != null ? $debitNote->due_date->format('d-m-Y') : ''),
            'operator' => '',
            'link'     => $debitNote->bill_link,
            'files'    => $debitNote->attachment ?? '',
            'html_status' => $this->transformHtmlStatus($debitNote),
            'order_date'  => $debitNote->start_date ? $debitNote->start_date->timestamp : $debitNote->date->timestamp,
        ];
    }
    
    private function transformBill($bill)
    {

        $this->updateBalance($bill->amount);

        return [
            'id' => $bill->id,
            'date' => $bill->start_date ? $bill->start_date->format('d-m-Y') : $bill->date->format('d-m-Y'),
            'type' => $bill->getDescription(),
            'ref_number' => $bill->bill_number,
            'flt_date' => $bill->getFltDate() ? $bill->getFltDate()->format('d-m-Y') : '',
            'status' => $bill->getStatus(),
            'aircraft' => Aircraft::find($bill->aircraft_id)->registration ?? '',
            'observations' => $bill->note,
            'debit' => round($bill->amount,2),
            'credit' => '',
            'balance' => round($this->balance,2),
            'icao' => $bill->itinerary->airport_icao_string ?? '',
            'operation_id' =>  $bill->operation->id ?? '',
            'pike_number' => $bill->operation ?  $bill->operation->code . ($bill->itinerary != null ? "_" . $bill->itinerary->code : "") : '',
            'due_date' => ($bill->due_date != null ? $bill->due_date->format('d-m-Y') : ''),
            'operator' => $bill->operation ? $bill->operation->getOperatorName() : '',
            'link'     => $bill->bill_link,
            'files'    => $bill->attachment ?? '',
            'html_status' => $this->transformHtmlStatus($bill),
            'order_date'  => $bill->start_date ? $bill->start_date->timestamp : $bill->date->timestamp,
        ];
    }
    
    private function transformPayment(Payment $payment)
    {
        $this->updateBalance(-$payment->amount);
        return [
            'id' => $payment->id,
            'date' => $payment->date->format('d-m-Y'),
            'type' => $payment->getDescription(),
            'payment_method' => $payment->payment_method,
            'is_over_payment' => $payment->is_over_payment,
            'ref_number' => $payment->payment_number,
            'flt_date' => '',
            'aircraft' => '',
            'observations' => $payment->observations,
            'debit' => '',
            'credit' => round($payment->amount,2),
            'balance' => round($this->balance,2),
            'icao' => '',
            'pike_number' => '',
            'due_date' => '',
            'operator' => '',
            'link'     => $payment->bill_link,
            'file'     => $payment->attachment ?? '',
            'order_date'  => $payment->date->timestamp,
            'html_status' => $this->transformHtmlStatusPayment($payment),

        ];
    }
    
    private function updateBalance($amount)
    {
        $this->balance = round($this->balance + $amount, 3);
    }
    
    private function transformHtmlStatus($bill)
    {
        if ($bill->status == 'paid'){
            return '<small class="label p1 white bg-green" style="position: absolute; color:white"> PAID </small>';
        }
        if ($bill->status == 'partially_paid'){
            return '<small class="label p1 white bg-yellow" style="position: absolute; color:white"> PART. PAID </small>';
        }
        if ($bill->status == 'canceled'){
            return '<small class="label p1 white bg-red" style="position: absolute; color:white"> CANCELED </small>';
        }
        return '<small class="label p1 white bg-gray" style="position: absolute; color:white"> NOT PAID </small>';
    
    }

    private function transformHtmlStatusPayment($payment)
    {
        if ($payment->status == 'paid'){
            return '<small class="label p1 white bg-green" style="position: absolute; color:white"> SETTLED </small>';
        }

        if ($payment->status == 'extra'){
            return '<small class="label p1 white bg-blue" style="position: absolute; color:white"> EXTRA </small>';
        }
    }
    
}