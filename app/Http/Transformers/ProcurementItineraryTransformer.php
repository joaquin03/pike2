<?php
/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 1/22/18
 * Time: 6:48 PM
 */

namespace App\Http\Transformers;


use App\Models\Itinerary;

class ProcurementItineraryTransformer extends AbstractTransformer
{
    public function transform(Itinerary $item)
    {
        return [
            'code'                      => $item->code,
            'type'                      => $item->type,
            'airport_icao_string'       => $item->airport_icao_string,
            'procurement_fuel_status'   => $item->getProcurementFuelStatus(),
            'procurement_service_status'=> $item->getProcurementServicesStatus(),
            'billing_service_status'    => $item->billing_service_status,
            'billing_fuel_status'       => $item->billing_fuel_status,
            'is_active'                 => $item->is_active,
        ];
    }
    
    
}