<?php
/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 1/22/18
 * Time: 6:48 PM
 */

namespace App\Http\Transformers\Screen;


use App\Http\Transformers\AbstractTransformer;
use App\Models\Itinerary;

class OperationScreenItineraryTransformer extends AbstractTransformer
{

    protected $defaultIncludes = [
        'services'
    ];

    public function transform(Itinerary $item)
    {
        return [
            'code'                      => $item->code,
            'type'                      => $item->type,
            'airport_icao_string'       => $item->airport_icao_string,
            'procurement_fuel_status'   => $item->getProcurementFuelStatus(),
            'procurement_service_status'=> $item->getProcurementServicesStatus(),
            'is_active'                 => $item->is_active,
            'date'                      => $item->screenDate()  ??  ' TBA',
            'time'                      => $item->time ? $item->time .' UTC' : ' TBA',
        ];
    }

    public function includeServices(Itinerary $itinerary)
    {
        if($itinerary->itineraryProviderServices->isNotEmpty()){
//            dd($itinerary->itineraryProviderServices);
        }
        return $this->collection($itinerary->serviceParents, new ItineraryServiceTransformer());
    }
    
//    private function includeServices(ServiceCategory $category)
//    {
//        $transformer = new ProviderServicesTransformer();
//        return $transformer->transformItems($category->getServicesOfItinerary($this->itinerary));
//    }
}
