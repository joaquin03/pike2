<?php
/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 1/21/18
 * Time: 10:18 PM
 */

namespace App\Http\Transformers\Screen;

use App\Http\Transformers\AbstractTransformer;
use App\Models\Itinerary;
use App\Models\Operation;
use App\Models\Procurement;
use Illuminate\Support\Collection;

class OperationScreenTransformer extends AbstractTransformer
{
    protected $defaultIncludes = [
        'itineraries'
    ];

    public function transform(Operation $item)
    {
        return [
            'id'                        => $item->id,
            'code'                      => $item->code,
            'date'                      => $item->getCreatedAt(),
            'in_charge_user'            => $item->user->name,
            'aircraft_operator'         => $item->aircraftOperator ? $item->aircraftOperator->name : '',
            'aircraft_client'           => $item->aircraftClient ? $item->aircraftClient->name : '',
            'aircraft'                  => $item->aircraft ? $item->aircraft->registration : '',
            'aircraft_type'             => $item->aircraft ? $item->aircraft->type->name : '',
            'route'                     => $item->operationAirports(),
            'procurement_permits_status'=> $item->getProcurementPermitsStatus(),
            'billing_permits_status'    => $item->getBillingPermitsStatus(),
            'operation_status'          => $item->getHtmlStatus($item->status),
            'procurement_status'        => $item->getHtmlProcurementStatus($item->procurement_status),
            'billing_status'            => $item->getHtmlStatus($item->billing_status),


        ];
    }


    public function includeItineraries(Operation $procurement)
    {
        return $this->collection($procurement->activeItineraries, new OperationScreenItineraryTransformer());
    }




}