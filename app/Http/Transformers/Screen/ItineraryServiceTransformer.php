<?php
/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 2/2/18
 * Time: 3:54 PM
 */

namespace App\Http\Transformers\Screen;


use App\Http\Transformers\AbstractTransformer;
use App\Models\ItineraryProviderService;
use App\Models\ProviderService;
use App\Models\Service;

class ItineraryServiceTransformer extends AbstractTransformer
{
    public function transform(Service $service)
    {
        return [
            'id'        => $service->id,
            'name'   => $service->name,
        ];
    }
}