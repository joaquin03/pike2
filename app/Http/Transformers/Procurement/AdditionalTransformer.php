<?php

namespace App\Http\Transformers\Procurement;

use App\Http\Transformers\AbstractTransformer;
use App\Models\Accounting\Additional;
use App\Models\Itinerary;

class AdditionalTransformer extends AbstractTransformer
{
   
    
    public function transform(Additional $additional)
    {
        return [
            'id'                => $additional->id,
            'service'           => $additional->servicesable != null? $additional->servicesable->getProcurementDescription() : '',
            'tax'               => $additional->tax,
            'amount'            => $additional->billing_unit_price,
            'price'             => $additional->getFinalPrice(),
            'provider_id'       => $additional->provider_id,
            'has_bills'         => $additional->has_bills,
            'procurement_status'=> $additional->procurement_status,
            
        ];
    }
    
    protected function changeItemsList($data)
    {
        $totalItem = $this->totalLine($data);
        $lastLine = [
            'id'                => null,
            'service'           => 'TOTAL',
            'tax'               => null,
            'amount'            => null,
            'price'             => $totalItem['price'],
            'provider_id'       => null,
            'has_bills'         => null,
            'procurement_status'=> null


        ];
        $data['data'][] = $lastLine;
        return $data;
    }
    
    private function totalLine($items)
    {
        $totalItem = [
            'price'             => 0,
        ];
        
        foreach ($items['data'] as $item) {
            $totalItem['price']     += $item['price'];
           }
        
        return $totalItem;
    }
    
}