<?php

namespace App\Http\Transformers\Procurement;

use App\Http\Transformers\AbstractTransformer;
use App\Models\Itinerary;
use App\Models\Procurement;
use App\Models\Provider;

class ItineraryServicesAdditionalsTransformer extends AbstractTransformer
{
    
    
    public function transform(Itinerary $itinerary)
    {
        $additionals = $itinerary->getAdditionals();
        return $this->transformData($itinerary, $additionals);
    }
    
    protected function transformData($itinerary, $additionals)
    {
        $additionalsTransformer = new AdditionalTransformer();
        $data = [];
        $index = 0;
        $data[$index]["id"] = $itinerary['id'];
        $data[$index]["name"] = $itinerary['name'];
        $data[$index]["country"] = $itinerary['country'];
        $data[$index]["additionals"] = [];
        
        if ($additionals != null) {
            foreach ($additionals->where('itinerary_id', $itinerary['id']) as $add) {
                $data[$index]["additionals"][] = $additionalsTransformer->transform($add);
            }
        }
        
        $data[$index]["additionals"][] = $this->calculateTotalAdditionals($data[$index]["additionals"]);
        
        return $data;
    }
    
    
    protected function calculateTotalAdditionals($items)
    {
        $totalPrice = 0;
        foreach ($items as $item) {
            $totalPrice += $item['price'];
        }
        return [
            'id' => null,
            'service' => 'Total',
            'tax' => null,
            'amount' => null,
            'price' => $totalPrice,
            'has_bills' => null,
        ];
    }
    
}