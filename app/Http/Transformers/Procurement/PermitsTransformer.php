<?php
/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 1/21/18
 * Time: 10:18 PM
 */

namespace App\Http\Transformers\Procurement;

use App\Http\Transformers\AbstractTransformer;
use App\Models\AircraftPermission;
use App\Models\Itinerary;
use App\Models\Procurement;
use Illuminate\Support\Collection;

class PermitsTransformer extends AbstractTransformer
{

    function transform($item)
    {

        
        $data = [
            'id'                => $item->id,
            'code'              => $item->code,
            'type'              => $item->getType(),
            'country'           => $item->provider->getCountry(),
            'icao_from'         => $item->icao_from,
            'icao_to'           => $item->icao_to,
            'country_distance'  => $item->country_distance,
            'expiration_date'   => $item->expiration_date,
            'operation_status'  => $item->status,
            'procurement_tax'   => $item->tax,
            'procurement_cost'  => $item->cost,
            'final_cost'        => $item->getProcurementFinalCost(),
    
            'status'            => $item->status,
            'quotation_status'  => $item->quotation_status,
            'file'              => $item->getUrlFile(),
            'invoice'           => $item->invoice,
            'procurement_status'=> $item->procurement_status,
            'provider_name'     => $item->provider->name,
    
            'profit'                => $item->getProfitPrice(),
            'billing_percentage'    => $item->billing_percentage,
            'billing_unit_price'    => $item->billing_unit_price,
            'billing_quote_price'   => $item->billing_quote_price,
            'amount'                => $item->getBillingFinalPrice(),
            'final_price'           => $item->getBillingFinalPrice(),
            'billing_status'        => $item->billing_status,
            'has_bills'             => $item->has_bills,
            'administration_fee'    => $item->administration_fee,
            'is_additional_quotation'=> $item->is_additional_quotation,


        ];
        if ($item->expiration_date != null) {
            $data['expiration_date'] = $item->expiration_date->format('d/m/Y');
        }
        return $data;
    }

}