<?php

namespace App\Http\Transformers\Procurement;

use App\Http\Transformers\AbstractTransformer;
use App\Models\ProviderService;

class ProviderServicesTransformer extends AbstractTransformer
{
    public function transform(ProviderService $serviceProvider)
    {
        return [
            'id'                    => $serviceProvider->pivot->id,
            'service_provider_id'   => $serviceProvider->id,
            'service_id'            => $serviceProvider->service->id,
            'service'               => $serviceProvider->service->name,
            'operation_status'      => $serviceProvider->pivot->operation_status,
            'quantity'              => $serviceProvider->pivot->procurement_quantity,
            'procurement_adm_fee'   => $serviceProvider->pivot->procurement_adm_fee,
            'procurement_tax'       => $serviceProvider->pivot->procurement_tax,
            'procurement_cost'      => $serviceProvider->pivot->procurement_cost,
            'final_cost'            => $serviceProvider->getProcurementFinalCost(),
            'procurement_status'    => $serviceProvider->pivot->procurement_status,
            'file'                  => $serviceProvider->pivot->file,
            'provider_name'         => $serviceProvider->provider->name,

            //Billing
            'billing_quote_price'        => $serviceProvider->pivot->billing_quote_price,
            'billing_percentage'    => $serviceProvider->pivot->billing_percentage,
            'billing_unit_price'    => $serviceProvider->pivot->billing_unit_price,
            'profit'            => $serviceProvider->getProfitPrice(),
            'price'             => $serviceProvider->getBillingFinalPrice(),
            'billing_status'    => $serviceProvider->pivot->billing_status,
            'administration_fee'=> $serviceProvider->pivot->administration_fee,
            'has_bills'         => $serviceProvider->pivot->has_bills,
        ];
    }
    
    protected function changeItemsList($data)
    {
        $totalItem = $this->totalLine($data);
        $lastLine = [
            'id'                => null,
            'service_provider_id'=>null,
            'service_id'        => null,
            'service'           => 'TOTAL',
            'operation_status'  => null,
            'quantity'          => null,
            'procurement_adm_fee'=> null,
            'procurement_tax'   => null,
            'procurement_cost'  => null,
            'final_cost'        => $totalItem['final_cost'],
            'procurement_status'=> ($totalItem['procurement_status'] ? 'Done' : ''),
            'file'   => null,
            
            //Billing
            'billing_percentage'        => null,
            'billing_quote_price'        => null,
            'amount'            => null,
            'profit'            => $totalItem['profit'],
            'price'             => $totalItem['price'],
            'billing_status'    => ($totalItem['procurement_status'] ? 'Done' : ''),
            'administration_fee'=> null,

        ];
        $data['data'][] = $lastLine;
        return $data;
    }
    
    private function totalLine($items)
    {
        $totalItem = [
            'final_cost'        => 0,
            'procurement_status'=> true,
            'profit'            => 0,
            'price'             => 0,
            'billing_status'    => 0,
        ];
        
        foreach ($items['data'] as $item) {
            $totalItem['final_cost'] += $item['final_cost'];
            $totalItem['procurement_status'] = $totalItem['procurement_status']&&($item['procurement_status'] == 'Done');
            $totalItem['profit']    += $item['profit'];
            $totalItem['price']     += $item['price'];
            $totalItem['billing_status'] = $totalItem['billing_status']&&($item['billing_status'] == 'Done');
        }
        
        return $totalItem;
    }
    
}