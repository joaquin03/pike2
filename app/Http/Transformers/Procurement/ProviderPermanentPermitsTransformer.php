<?php
/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 2/5/18
 * Time: 12:54 PM
 */

namespace App\Http\Transformers\Procurement;


use App\Http\Transformers\AbstractTransformer;
use App\Models\Procurement;
use App\Models\Provider;

class ProviderPermanentPermitsTransformer extends ProviderPermitsTransformer
{
    public function transform(Procurement $item)
    {
        $permits = $item->permanentPermissions;
        $providers = Provider::whereIn('id', $permits->pluck('provider_id'))->get();
        return $this->transformData($providers, $permits);
    }
}