<?php
/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 2/4/18
 * Time: 1:01 PM
 */

namespace App\Http\Transformers\Procurement;


use App\Http\Transformers\AbstractTransformer;
use App\Models\AircraftPermission;
use App\Models\Procurement;
use App\Models\Provider;
use App\Models\Scopes\OVFScope;

class   ProviderPermitsTransformer extends AbstractTransformer
{
    public function transform(Procurement $item)
    {
        $permits = $item->aircraftPermission;
        $providers = Provider::whereIn('id', $permits->pluck('provider_id'))->get();
        return $this->transformData($providers, $permits);
    }
    
    protected function transformData($providers, $permits)
    {
        $permitsTransformer = new PermitsTransformer();
        $data = [];
        foreach ($providers as $index => $provider) {
            $data[$index]["id"] = $provider->id;
            $data[$index]["name"] = $provider->name;
            $data[$index]["country"] = $provider->country;
            $data[$index]["permits"] = [];

            foreach ($permits->where('provider_id', $provider->id) as $permit) {
                $data[$index]["permits"][] = $permitsTransformer->transform($permit);
            }

            $data[$index]["permits"][] = $this->calculateTotalPermits($data[$index]["permits"]);
        }
        return $data;
    }
    
    
    protected function calculateTotalPermits($items)
    {
        $totalCost = 0;
        $totalFinalPrice = 0;
        $totalFinalCost = 0;
        $procurementStatus = true;
        foreach ($items as $item) {
            $totalCost += $item['procurement_cost']  ;
            $totalFinalCost += $item['final_cost'] ;
            $totalFinalPrice += $item['final_price'] ;
            $procurementStatus = $procurementStatus&&($item['procurement_status'] == 'Done');
        }
        return [
            'id'    => null,
            'code'  => 'Total',
            'type'  => null,
            'country'   => null,
            'icao_from' => null,
            'icao_to'   => null,
            'expiration_date'   => null,
            'operation_status'  => null,
            'procurement_tax'   => null,
            'procurement_cost' => $totalCost,
            'final_cost'   => $totalFinalCost,
            'final_price'   => $totalFinalPrice,
            'status'        => null,
            'file'          => null,
            'procurement_status' => $procurementStatus ? 'Done' : ''
        ];
    }
    
    
}