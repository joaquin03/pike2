<?php
/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 1/22/18
 * Time: 6:48 PM
 */

namespace App\Http\Transformers\Procurement;


use App\Http\Transformers\AbstractTransformer;
use App\Http\Transformers\ProviderServiceTransformer;
use App\Models\Itinerary;
use App\Models\Provider;
use Illuminate\Support\Collection;

class ItineraryProvidersTransformer extends AbstractTransformer
{
    protected $itinerary;
    
    public function __construct(Itinerary $itinerary)
    {
        $this->itinerary = $itinerary;
    }
    
    public function transform(Provider $provider)
    {
        $services = $this->includeServices($provider);
        $providerServices = $this->includeProviderServices($provider);
        $filterProviderServices = $this->filterProviderServices($providerServices, $services);
        $bill = $this->itinerary->getServicesBill($provider);
        
        return [
            'id'                => $provider->id,
            'name'              => $provider->name,
            'services'          => $services,
            'provider_services' => $filterProviderServices,
            'bill_id'           => $bill->id ?? '',
        ];
    }
    
    private function includeServices(Provider $provider)
    {
        $transformer = new ProviderServicesTransformer();
        return $transformer->transformItems($this->itinerary->getServicesOfProvider($provider->id));
    }
    
    private function includeProviderServices(Provider $provider)
    {
        $transformer = new ProviderServiceTransformer();
        return $transformer->transformItems($provider->providerServices);
        
    }
    
    private function filterProviderServices ($services, $usedServices)
    {
        $usedServices = new Collection($usedServices['data']);
        $services = new Collection($services['data']);
        
        return $services->whereNotIn('id', $usedServices->pluck('service_provider_id')->toArray())->toArray();
    }
    
}