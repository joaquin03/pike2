<?php

namespace App\Http\Transformers\Procurement;

use App\Http\Transformers\AbstractTransformer;
use App\Models\Itinerary;
use App\Models\Procurement;
use App\Models\Provider;

class ItineraryServiceAdditionalsTransformer extends AbstractTransformer
{
    
    
    public function transform(Itinerary $item)
    {
        $additionals = $item->serviceAdditionals;
        $providers = $item->providerServices()->get();
        return $this->transformData($providers, $additionals);
    }
    
    protected function transformData($providers, $additionals)
    {
        
        $additionalsTransformer = new AdditionalTransformer();
        $data = [];
        foreach ($providers as $index => $provider) {
            $data[$index]["id"] = $provider['id'];
            $data[$index]["name"] = $provider['name'];
            $data[$index]["country"] = $provider['country'];
            $data[$index]["additionals"] = [];
            
            foreach ($additionals->where('provider_id', $provider['id']) as $additional) {
                $data[$index]["additionals"][] = $additionalsTransformer->transform($additional);
            }
            
            $data[$index]["additionals"][] = $this->calculateTotalAdditionals($data[$index]["additionals"]);
            
        }
        return $data;
    }
    
    
    protected function calculateTotalAdditionals($items)
    {
        $totalPrice = 0;
        foreach ($items as $item) {
            $totalPrice += $item['price'];
        }
        return [
            'id' => null,
            'service' => 'Total',
            'tax' => null,
            'amount' => null,
            'price' => $totalPrice,
            'has_bills' => null,
        ];
    }
    
}