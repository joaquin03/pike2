<?php

namespace App\Http\Transformers\Quotation;

use App\Http\Transformers\AbstractTransformer;
use App\Http\Transformers\Billing\ItineraryServiceParentTransformer;
use App\Http\Transformers\Billing\QuotationItineraryServiceParentTransformer;
use App\Http\Transformers\ServiceTransformerMin;
use App\Http\Transformers\ProviderServiceTransformer;
use App\Models\Itinerary;
use App\Models\Provider;
use App\Models\Service;
use Illuminate\Support\Collection;

class QuotationProvidersItineraryServicesTransformer extends AbstractTransformer
{
    protected $itinerary;
    
    public function __construct(Itinerary $itinerary)
    {
        $this->itinerary = $itinerary;
    }
    
    public function transform(Provider $provider)
    {
        $providerServices = $this->includeProviderServices($provider);
    
        $itineraryProviderServices = $this->includeServices($provider);
        
        
        return [
            'id'                => $provider->id,
            'name'              => $provider->name,
            'services'          => $itineraryProviderServices,
            'provider_services' => $providerServices,
            'bill_id'           => '',
            'final_price'        => $this->itinerary->getProviderServiceFinalPrice($provider),
            'adm_fee'            =>$provider->adm_fee ?? 0.12,
        ];
    }
    
    private function includeServices(Provider $provider)
    {
        $transformer = new QuotationItineraryServiceParentTransformer($this->itinerary);
        $items = $transformer->transformItems($this->itinerary->serviceParents);
       
        return $items;
        
    }
    
    
    private function includeProviderServices(Provider $provider)
    {
        $transformer = new ServiceTransformerMin();


        return $transformer->transformItems($provider->serviceChild);
        
    }
    
    private function filterProviderServices ($services, $usedServices)
    {
        $usedServices = new Collection($usedServices['data']);
        $services = new Collection($services['data']);

        return $services->whereNotIn('id', $usedServices->pluck('service_provider_id')->toArray())->toArray();
    }
    
}