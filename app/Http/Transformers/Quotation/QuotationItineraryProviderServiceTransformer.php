<?php

namespace App\Http\Transformers\Quotation;

use App\Http\Transformers\ItineraryProviderServiceTransformer;
use App\Http\Transformers\Procurement\ProviderServicesTransformer;
use App\Models\ItineraryProviderService;


class QuotationItineraryProviderServiceTransformer extends ItineraryProviderServiceTransformer
{
    public function transform(ItineraryProviderService $itineraryPService)
    {

        $parent_name = null;
        if ($itineraryPService->parent_id == null) {
            $parent_name = $itineraryPService->service->name;
        }
        return [
            'id' => $itineraryPService->id,
            'service_provider_id' => $itineraryPService->id,
            'service_id' => $itineraryPService->service->id,
            'service' => $itineraryPService->isFromPike() ? $itineraryPService->service->name . ' (P)' : $itineraryPService->service->name,
            'parent_name' => $parent_name,
            'provider_name' => $itineraryPService->provider->name ?? '',
            'operation_status' => $itineraryPService->operation_status,
            'service_unit' => $itineraryPService->service_unit,
            'operation_quantity' => $itineraryPService->operation_quantity ?? 1,

            //Quotation
            'quotation_final_cost' => $itineraryPService->getQuotationFinalPrice(),

            //procurement
            'procurement_quantity' => $itineraryPService->getProcurementQuantity(),
            'procurement_adm_fee' => $itineraryPService->getProcurementAdmFee(),
            'procurement_tax' => $itineraryPService->getProcurementTax(),
            'procurement_cost' => $itineraryPService->getProcurementCost(),
            'procurement_status' => $itineraryPService->procurement_status,
            'procurement_status_text' => ($itineraryPService->procurement_status ? 'Invoiced' : 'Not Inv.'),
            'final_cost' => $itineraryPService->getProcurementFinalCost(),
            'is_additional' => $itineraryPService->is_additional,


            //Billing
            'billing_quantity' => $itineraryPService->getBillingQuantity(),
            'billing_percentage' => $itineraryPService->getBillingPercentage(),
            'billing_unit_price' => $itineraryPService->billing_unit_price,
            'profit' => $itineraryPService->getProfitPrice(),
            'price' => $itineraryPService->getQuotationFinalPrice(),
            'billing_status' => $itineraryPService->billing_status,
            'administration_fee' => $itineraryPService->administration_fee,
            'has_bills' => $itineraryPService->has_bills ?? 0,
            'billing_checked' => ($itineraryPService->billing_checked ? 1 : 0),
            'adm_fee_checked' => ($itineraryPService->adm_fee_checked ? 1 : 0),

            'children' => $itineraryPService->children,
        ];
    }

    protected function changeItemsList($data)
    {
        $totalItem = $this->totalLine($data);
        $lastLine = [
            'id' => null,
            'service_provider_id' => null,
            'service_id' => null,
            'service' => 'TOTAL',
            'operation_status' => null,
            'quantity' => null,
            'procurement_tax' => null,
            'procurement_adm_fee' => null,
            'procurement_cost' => null,
            'final_cost' => $totalItem['final_cost'],
            'procurement_status' => ($totalItem['procurement_status'] ? 'Done' : ''),
            'file' => null,

            //Billing
            'billing_percentage' => null,
            'billing_quote_price' => null,
            'billing_unit_price' => null,
            'profit' => round($totalItem['profit'], 2),
            'price' => round($totalItem['price'], 2),
            'billing_status' => ($totalItem['procurement_status'] ? 'Done' : ''),
            'administration_fee' => null,
            'has_bills' => null,


        ];
        $data['data'][] = $lastLine;
        return $data;
    }

    private function totalLine($items)
    {
        $totalItem = [
            'final_cost' => 0,
            'procurement_status' => true,
            'profit' => 0,
            'price' => 0,
            'billing_status' => 0,
        ];

        foreach ($items['data'] as $item) {
            if ($item['parent_name'] == null) {
                $totalItem['final_cost'] += $item['final_cost'];
                $totalItem['procurement_status'] = $totalItem['procurement_status'] && ($item['procurement_status'] == 'Done');
                $totalItem['profit'] += $item['profit'];
                $totalItem['price'] += $item['price'];
                $totalItem['billing_status'] = $totalItem['billing_status'] && ($item['billing_status'] == 'Done');
            }
        }

        return $totalItem;
    }

    public function changeItemsListBilling($data)
    {
        $totalItem = $this->totalLineBilling($data);
        $lastLine = [
            'id' => null,
            'service_provider_id' => null,
            'service_id' => null,
            'parent_name' => 'TOTAL',
            'operation_status' => null,
            'quantity' => null,
            'procurement_tax' => null,
            'procurement_cost' => null,
            'final_cost' => round($totalItem['final_cost'], 2),
            'procurement_status' => ($totalItem['procurement_status'] ? 'Done' : ''),
            'file' => null,

            //Billing
            'billing_percentage' => null,
            'billing_quote_price' => null,
            'billing_unit_price' => null,
            'profit' => round($totalItem['profit'], 2),
            'price' => round($totalItem['price'], 2),
            'quotation_final_cost' => round($totalItem['quotation_final_cost'], 2),
            'billing_status' => ($totalItem['procurement_status'] ? 'Done' : ''),
            'administration_fee' => null,
            'has_bills' => null,

        ];
        return $lastLine;
    }

    private function totalLineBilling($items)
    {
        $totalItem = [
            'final_cost' => 0,
            'procurement_status' => true,
            'profit' => 0,
            'price' => 0,
            'billing_status' => 0,
            'quotation_final_cost' => 0,
        ];

        foreach ($items as $item) {
            if (array_key_exists('parent_name', $item)) {
                if ($item['parent_name'] == null) {
                    $totalItem['final_cost'] += $item['final_cost'];
                    $totalItem['procurement_status'] = $totalItem['procurement_status'] && ($item['procurement_status'] == 'Done');
                    $totalItem['profit'] += $item['profit'];
                    $totalItem['price'] += $item['price'];
                    $totalItem['billing_status'] = $totalItem['billing_status'] && ($item['billing_status'] == 'Done');
                    $totalItem['quotation_final_cost'] += $item['quotation_final_cost'];
                }
            }
        }

        return $totalItem;
    }

}