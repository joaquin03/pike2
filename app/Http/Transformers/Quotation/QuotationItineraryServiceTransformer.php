<?php

namespace App\Http\Transformers\Quotation;

use App\Http\Transformers\AbstractTransformer;
use App\Models\ItineraryProviderService;



class QuotationItineraryServiceTransformer extends AbstractTransformer
{
    
    public function transform(ItineraryProviderService $itineraryPService)
    {
        $parent_name = null;
        if ($itineraryPService->parent_id != null) {
            $parent_name = $itineraryPService->service->name;
        }
        return [
            
            'id'                    => $itineraryPService->id,
            'parent_name'           => $parent_name,
            'service_id'            => $itineraryPService->service->id,
            'service'               => $itineraryPService->service->name,
            'provider_name'         => $itineraryPService->provider->name,
            'operation_status'      => $itineraryPService->operation_status,

            //Quotation
            'quotation_final_cost'  => $itineraryPService->getFinalCostQuotation(),

            //Procurement
            'procurement_quantity'  => $itineraryPService->getProcurementQuantity(),
            'procurement_cost'      => $itineraryPService->getProcurementCost(),
            'procurement_tax'       => $itineraryPService->getProcurementTax(),
            'procurement_adm_fee'   => $itineraryPService->getProcurementAdmFee(),
            'procurement_status'    => $itineraryPService->procurement_status,
            'final_cost'            => $itineraryPService->getProcurementFinalCost(),
            'is_additional'         => $itineraryPService->is_additional,
            
            //Billing
            'administration_fee'    => $itineraryPService->administration_fee,
            'billing_percentage'    => $itineraryPService->getBillingPercentage(),
            'billing_unit_price'    => $itineraryPService->billing_unit_price,
        ];
        
    }

    public function getTotalLine($itinerary, $provider)
    {
        return [
            'id'                    => null,
            'parent_name'           => 'TOTAL',
            'service_id'            => null,
            'service'               => null,
            'provider_name'         => null,
            'operation_status'      => null,

            //procurement
            'procurement_quantity'  => null,
            'procurement_tax'       => null,
            'procurement_cost'      => null,
            'procurement_status'    => null,
            'final_cost'            => round($itinerary->getQuotationProviderServiceFinalPrice($provider),3),
        ];
    
    }
    
    public function transformItems($parentServices)
    {
        $list = collect();
        foreach ($parentServices as $service) {
            $list->push($this->transformServiceInItineraryService($service));
            $subServices =  $service->getItineraryChildServices($this->itineraryId);
            
            foreach ($subServices as $item) {
                $item = $this->itineraryServiceTransformer->transform($item);
                $list->push($item);
            }
        }
        return [
            'data' => $list->toArray()
        ];
    }
    
}