<?php
namespace App\Http\Transformers\Reports;

use App\Http\Transformers\AbstractTransformer;
use App\Models\Accounting\Invoice;
use Carbon\Carbon;

class InvoiceReportsTransformer extends AbstractTransformer
{
    protected $total = 0;
    protected $paid = 0;
    protected $sub_total = 0;
    
    public function transform(Invoice $invoice)
    {
        $amount = round($invoice->amount, 3);
        $this->total = $this->total + $amount;
        $this->paid = $this->paid + $invoice->getTotalPaid();
        $this->sub_total = $this->sub_total + $invoice->getReminingPayment();
        
        return [
            'id'                    => $invoice->id,
            'bill_number'           => str_replace('_', ' ',$invoice->bill_number),
            'code'                  => $invoice->operation ? $invoice->operation->code : 'N/A',
            'provider'              => $invoice->company ? $invoice->company->name : 'N/A',
            'date'                  => $invoice->start_date ? $invoice->start_date->format('d/m/Y') : $invoice->date->format('d/m/Y') ,
            'due_date'              => $invoice->due_date->format('d/m/Y'),
            'aircraft'              => $invoice->aircraft->registration ?? '',
            'status'                => $invoice->status,
            'service_names'         => $invoice->getServicesString(),
            'total'                 => $amount,
            'paid'                  => $invoice->getTotalPaid(),
            'sub_total'             => $invoice->getReminingPayment()
        ];
    }
    
    protected function changeItemsList($data)
    {
        $lastLine = [
            'id'                    => '',
            'bill_number'           => 'TOTAL',
            'code'                  => '',
            'provider'              => '',
            'date'                  => '',
            'due_date'              => '',
            'icao'                  => '',
            'aircraft'              => '',
            'status'                => '',
            'service_names'         => '',
            'total'                 => round($this->total, 2),
            'paid'                  => round($this->paid, 2),
            'sub_total'             => round($this->sub_total, 2),
        
        
        ];
        $data['data'][] = $lastLine;
        return $data;
    }
    
}