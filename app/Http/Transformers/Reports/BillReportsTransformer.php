<?php
namespace App\Http\Transformers\Reports;

use App\Http\Transformers\AbstractTransformer;
use App\Models\Accounting\Bill;
use Carbon\Carbon;

class BillReportsTransformer extends AbstractTransformer
{
    protected $total = 0;
    protected $paid = 0;
    protected $sub_total = 0;
    
    public function transform(Bill $bill)
    {
        $amount = round($bill->amount, 3);
        $this->total = $this->total + $amount;
        $this->paid = $this->paid + $bill->getTotalPaid();
        $this->sub_total = $this->sub_total + $bill->getReminingPayment();
        
        return [
            'id'                    => $bill->id,
            'bill_number'           => str_replace('_', ' ',$bill->bill_number),
            'code'                  => $bill->operation ? $bill->operation->code : 'N/A',
            'client'                => $bill->company ? $bill->company->name : 'N/A',
            'date'                  => $bill->date->format('d/m/Y'),
            'due_date'              => $bill->due_date->format('d/m/Y'),
            'aircraft'              => $bill->aircraft->registration ?? '',
            'status'                => $bill->status,
            'service_names'         => $bill->getServicesString(),
            'total'                 => $amount,
            'paid'                  => $bill->getTotalPaid(),
            'sub_total'             => $bill->getReminingPayment()
        ];
    }
    
    protected function changeItemsList($data)
    {
        $lastLine = [
            'id'                    => '',
            'bill_number'           => 'TOTAL',
            'code'                  => '',
            'client'                => '',
            'date'                  => '',
            'due_date'              => '',
            'icao'                  => '',
            'aircraft'              => '',
            'status'                => '',
            'service_names'         => '',
            'total'                 => round($this->total, 2),
            'paid'                  => round($this->paid, 2),
            'sub_total'             => round($this->sub_total, 2),
        
        
        ];
        $data['data'][] = $lastLine;
        return $data;
    }
    
}