<?php
namespace App\Http\Transformers\Reports;

use App\Http\Transformers\AbstractTransformer;
use Carbon\Carbon;

class OperationsReportsTransformer extends AbstractTransformer
{
    public function transform($itineraryPService)
    {
//        $date = Carbon::createFromTimeString($itineraryPService->itinerary->operation->start_date)->format('d-m-Y');
        $date = null;
        if($itineraryPService->itinerary->operation->start_date) {
            $date = Carbon::createFromFormat('Y-m-d H:i:s', $itineraryPService->itinerary->operation->start_date)->format('d-m-Y');
        }
        return [
            'id'                    => $itineraryPService->id,
            'code'                  => $itineraryPService->itinerary->operation->code,
            'date'                  => $date,
            'aircraft'              => $itineraryPService->itinerary->operation->aircraft ? $itineraryPService->itinerary->operation->aircraft->registration : '',
            'country'               => \App\Models\Utilities\Country::getName($itineraryPService->itinerary->airport->country),
            'icao_names'            => $itineraryPService->icao_names ?? $itineraryPService->itinerary->airport_icao_string,
            'provider_name'         => $itineraryPService->provider_names ?? $itineraryPService->provider->name ?? '',
            'service_names'          => $itineraryPService->service_names ?? $itineraryPService->service->name ?? '',
            'service_status'          => $itineraryPService->operation_status,
        ];
    }
}