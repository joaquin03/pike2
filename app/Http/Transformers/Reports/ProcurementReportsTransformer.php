<?php
namespace App\Http\Transformers\Reports;

use App\Http\Transformers\AbstractTransformer;
use Carbon\Carbon;

class ProcurementReportsTransformer extends AbstractTransformer
{
    protected $total = 0;
    
    public function transform($operation)
    {
//       dd($operation->start_date);
//        $date = Carbon::createFromTimeString($operation->start_date)->format('d-m-Y');
        $date = null;
        if($operation->start_date) {
            $date = Carbon::createFromFormat('Y-m-d H:i:s', $operation->start_date)->toDateTimeString();
        }
        $this->total = $this->total + (double) $operation->total;
        return [
            'id'            => $operation->id,
            'code'          => $operation->code,
            'date'          => $date,
            'aircraft'      => $operation->aircraft->registration,
            'country'       => $operation->country,
            'icao_names'    => $operation->icao_names,
            'provider_name' => $operation->provider_names,
            'service_names' => $operation->service_names,
            'quantity'      => $operation->procurement_quantity,
            'total'         => $operation->total,
        ];
    }
    
    protected function changeItemsList($data)
    {
        $lastLine = [
            'id'            => '',
            'code'          => 'TOTAL',
            'date'          => '',
            'aircraft'      => '',
            'country'       => '',
            'icao_names'    => '',
            'provider_name' => '',
            'service_names' => '',
            'quantity'      => '',
            'total'         => $this->total,
        
        
        ];
        $data['data'][] = $lastLine;
        return $data;
    }
    
}