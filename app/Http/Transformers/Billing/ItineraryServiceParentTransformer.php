<?php
/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 3/7/18
 * Time: 12:55 PM
 */

namespace App\Http\Transformers\Billing;


use App\Http\Transformers\AbstractTransformer;
use App\Http\Transformers\ItineraryProviderServiceTransformer;
use App\Http\Transformers\Procurement\ProviderServicesTransformer;
use App\Models\Itinerary;
use App\Models\ItineraryServiceParents;
use App\Models\ServiceCategory;
use App\Models\ServiceParent;

class ItineraryServiceParentTransformer  extends AbstractTransformer
{
    protected $itineraryServiceTransformer;
    protected $itineraryId;
    
    public function __construct(Itinerary $itinerary)
    {
        $this->itineraryId = $itinerary->id;
        $this->itineraryServiceTransformer = new ItineraryProviderServiceTransformer();
    }
    
    public function transformItems($itineraryServiceParents)
    {
        $list = collect();
        foreach ($itineraryServiceParents as $itineraryServiceParent) {
            $parentService = $this->transform($itineraryServiceParent);
            
            $list->push($parentService);
            
            $subServices =  $itineraryServiceParent->getItineraryProviderServiceChild();
            
            foreach ($subServices as $item) {
                $item = $this->itineraryServiceTransformer->transform($item);
                $list->push($item);
            }
        }
        $list->push($this->itineraryServiceTransformer->changeItemsListBilling($list));

        return [
            'data' => $list->toArray()
        ];
    }
    
    
    public function transform(ItineraryServiceParents $serviceParent)
    {
        return [
            'id'                    => $serviceParent->id,
            'service_provider_id'   => $serviceParent->id,
            'service_id'            => $serviceParent->service->id,
            'service'               => '' . ($serviceParent->isFromPike() ?  ' (P)' : ''),
            'parent_name'           => $serviceParent->service->name,
            'provider_name'         => $serviceParent->provider->name ?? '',
            'operation_status'      => null,
            'service_unit'          => null,
        
            //Quotation
            'quotation_final_cost'       => $serviceParent->getFinalCostQuotation(),
        
            //Operation
            'operation_quantity'        => null,
            //procurement
            'procurement_quantity'       => $serviceParent->getProcurementQuantity(),
            'procurement_adm_fee'        => $serviceParent->getProcurementAdmFee(),
            'procurement_tax'            => $serviceParent->getProcurementTax(),
            'procurement_cost'           => $serviceParent->getProcurementCost(),
            'procurement_status'         => null,
            'procurement_status_text'    => null,
            'final_cost'                 => $serviceParent->getProcurementFinalCost(),
            'is_additional'              => null,
        
        
            //Billing
            'billing_quantity'           => $serviceParent->getBillingQuantity(),
            'billing_percentage'         => $serviceParent->getBillingPercentage(),
            'billing_unit_price'         => null,
            'profit'                     => $serviceParent->getProfitPrice(),
            'price'                      => $serviceParent->getBillingFinalPrice(),
            'billing_status'             => null,
            'administration_fee'         => null,
            'has_bills'                  => $serviceParent->has_bills ?? 0,
            'billing_checked'            => $serviceParent->billing_checked ? 1 : 0,
            'adm_fee_checked'            => $serviceParent->adm_fee_checked ? 1 : 0,
        
            'children'                   => null,
        ];
    }
    
}