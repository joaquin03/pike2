<?php
/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 3/7/18
 * Time: 12:55 PM
 */

namespace App\Http\Transformers\Billing;


use App\Http\Transformers\AbstractTransformer;
use App\Http\Transformers\Procurement\ProviderServicesTransformer;
use App\Models\Itinerary;
use App\Models\ItineraryProviderService;
use App\Models\ServiceCategory;

class ItineraryBillingTransformer  extends AbstractTransformer
{
    
    
    public function transform(ItineraryProviderService $serviceProvider)
    {
        $services = $this->includeServices($category);
        
        return [
            'id' => $category->id,
            'name' => $category->name,
            'services' => $services,
        ];
    }
    
    private function includeServices(ServiceCategory $category)
    {
        $transformer = new ProviderServicesTransformer();
        return $transformer->transformItems($category->getServicesOfItinerary($this->itinerary));
    }
    
}