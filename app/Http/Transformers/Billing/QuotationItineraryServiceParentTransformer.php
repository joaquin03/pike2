<?php
/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 3/7/18
 * Time: 12:55 PM
 */

namespace App\Http\Transformers\Billing;


use App\Http\Transformers\ItineraryProviderServiceTransformer;
use App\Http\Transformers\Procurement\ProviderServicesTransformer;
use App\Models\Itinerary;
use App\Models\ServiceCategory;
use App\Models\ServiceParent;

class QuotationItineraryServiceParentTransformer  extends \App\Http\Transformers\Quotation\ItineraryServiceParentTransformer
{

    public function transformItems($parentServices)
    {
        $list = collect();
        foreach ($parentServices as $service) {
            $subServices =  $service->getItineraryChildServices($this->itineraryId);
            if(!$subServices->isEmpty()) {
                $list->push($this->transformServiceInItineraryService($service));
            }
            foreach ($subServices as $item) {
                $item = $this->itineraryServiceTransformer->transform($item);
                $list->push($item);
            }

        }
        $list->push($this->itineraryServiceTransformer->changeItemsListBilling($list));

        return [
            'data' => $list->toArray()
        ];
    }

    public function transformAdditionalsItems($parentServices)
    {
        $list = collect();
        foreach ($parentServices as $service) {
            $subServices =  $service->getItineraryChildServicesAdditionals($this->itineraryId);
            if(!$subServices->isEmpty()){
                $list->push($this->transformServiceInItineraryService($service));
            }
            foreach ($subServices as $item) {
                $item = $this->itineraryServiceTransformer->transform($item);
                $list->push($item);
            }

        }
        $list->push($this->itineraryServiceTransformer->changeItemsListBilling($list));

        return [
            'data' => $list->toArray()
        ];
    }

    protected  function transformServiceInItineraryService(ServiceParent $serviceParent)
    {

       return [
           'id'                    => $serviceParent->id,
           'service_provider_id'   => null,
           'service_id'            => $serviceParent->id,
           'service'               => $serviceParent->name,
           'parent_name'           => $serviceParent->name,
           'provider_name'         => null,
           'operation_status'      => null,
           'service_unit'          => null,


           //procurement
           'procurement_quantity'  => $serviceParent->procurement_quantity,
           'procurement_tax'       => null,
           'procurement_cost'      => null,
           'procurement_status'    => null,
           'final_cost'            => null,

           //Billing
           'billing_quote_price'   => null,
           'billing_percentage'    => null,
           'billing_unit_price'    => null,
           'profit'                => $serviceParent->getBillingProfit($this->itineraryId),
           'quotation_final_cost'  => $serviceParent->getQuotationFinalPrice($this->itineraryId),
           'price'                 => $serviceParent->getQuotationFinalPrice($this->itineraryId),
           'billing_status'        => null,
           'administration_fee'    => $serviceParent->getHasAdmFee($this->itineraryId) ? false : '',
           'has_bills'             => $serviceParent->getHasBill($this->itineraryId) ? 1 : 0,

           'children'              => null,
       ];
    }

}