<?php

namespace App\Http\Transformers\Billing;

use App\Http\Transformers\AbstractTransformer;
use App\Models\Accounting\AdministrationFee;
use App\Models\Itinerary;

class AdministrationFeeTransformer extends AbstractTransformer
{


    public function transform(AdministrationFee $administrationFee)
    {
        return [
            'id'                => $administrationFee->id,
            'service'           => $administrationFee->servicesable->getProcurementDescription(),
            'service_price'     => $administrationFee->getServiceFinalPrice(),
            'percentage'        => $administrationFee->billing_percentage,
            'billing_unit_price'            => $administrationFee->billing_unit_price,
            'profit'            => $administrationFee->getProfitPrice(),
            'price'             => $administrationFee->getFinalPrice(),
            'has_bills'         => $administrationFee->has_bills,

        ];
    }

    protected function changeItemsList($data)
    {
        $totalItem = $this->totalLine($data);
        $lastLine = [
            'id'                => null,
            'service'           => 'TOTAL',
            'service_price'     => null,
            'percentage'        => null,
            'amount'            => null,
            'profit'            => $totalItem['profit'],
            'price'             => $totalItem['price'],


        ];
        $data['data'][] = $lastLine;
        return $data;
    }

    private function totalLine($items)
    {
        $totalItem = [
            'profit'            => 0,
            'price'             => 0,
        ];

        foreach ($items['data'] as $item) {
            $totalItem['profit']    += $item['profit'];
            $totalItem['price']     += $item['price'];
           }

        return $totalItem;
    }

}
