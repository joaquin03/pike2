<?php

namespace App\Http\Transformers;

use App\Http\Transformers\Serializer\CustomSerializer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use League\Fractal;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;



abstract class  AbstractTransformer extends Fractal\TransformerAbstract
{
    public function transformItems($items)
    {
        if ($items == []) {
            return [];
        }
        if ($items instanceof LengthAwarePaginator)
        {
            return $this->transformPaginator($items);
        }
        
        if ($items instanceof Collection) {
            $resource = new Fractal\Resource\Collection($items, $this);
            return $this->transformItemsORCollections($resource);
        }
        else if ($items instanceof Model) {
            $resource = new Fractal\Resource\Item($items, $this);
            return $this->transformItemsORCollections($resource);
        }
        
    }
    
    protected function changeItemsList($data)
    {
        return $data;
    }
    
    private function transformPaginator(LengthAwarePaginator $paginator)
    {
        $resource = new Fractal\Resource\Collection($paginator->getCollection(), $this);
        $resource->setPaginator(new IlluminatePaginatorAdapter($paginator));
        return $this->transformItemsORCollections($resource);
    }
    
    private function transformItemsORCollections($resource)
    {
        $manager = new Manager();
        $manager->setSerializer(new Fractal\Serializer\ArraySerializer());
        $data = $manager->createData($resource)->toArray();
    
        return $this->changeItemsList($data);
    }
}