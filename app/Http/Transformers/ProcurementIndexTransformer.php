<?php
/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 1/21/18
 * Time: 10:18 PM
 */

namespace App\Http\Transformers;

use App\Models\Itinerary;
use App\Models\Procurement;
use Illuminate\Support\Collection;

class ProcurementIndexTransformer extends AbstractTransformer
{
    protected $defaultIncludes = [
        'itineraries'
    ];
    
    public function transform(Procurement $item)
    {
        return [
            'id'                        => $item->id,
            'code'                      => $item->code,
            'date'                      => $item->getCreatedAt(),
            'in_charge_user'            => $item->user->name,
            'aircraft_operator'         => $item->aircraftOperator->name ?? '',
            'aircraft_client'           => $item->aircraftClient->name ?? '',
            'aircraft'                  => $item->aircraft->registration ?? '',
            'route'                     => $item->operationAirports(),
            'procurement_permits_status'=> $item->getProcurementPermitsStatus(),
            'billing_permits_status'    => $item->getBillingPermitsStatus(),
            'operation_status'          => $item->getHtmlStatus($item->status),
            'procurement_status'        => $item->getHtmlProcurementStatus($item->procurement_status),
            'billing_status'            => $item->getHtmlStatus($item->billing_status),


        ];
    }
    
    
    public function includeItineraries(Procurement $procurement)
    {
        return $this->collection($procurement->itineraryByAirport, new ProcurementItineraryTransformer());
    }
    
    
}