<?php
/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 2/2/18
 * Time: 3:54 PM
 */

namespace App\Http\Transformers;


use App\Models\ItineraryProviderService;
use App\Models\ProviderService;
use App\Models\Service;

class ServiceTransformerMin extends AbstractTransformer
{
    public function transform(Service $service)
    {
        $name = $service->name;
        if ($service->parent != null) {
            $name = $service->parent->name. " - ".$name;
        }
        return [
            'id'        => $service->id,
            'service'   => $name,
        ];
    }
}