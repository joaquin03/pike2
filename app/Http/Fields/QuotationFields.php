<?php
namespace App\Http\Fields;

class QuotationFields extends AbstractFields
{
    
    protected $class = 'quotation';
    
    public function getFormFields()
    {
        return [];
    }

    public function getColumnsFields()
    {
        return [
            ['name' => 'created_at', 'label'=>'Date', 'type'=>'model_function','function_name'=> 'getCreatedAt'],
            
            ['name' => 'code', 'label' => 'ID', 'type' => 'link_edit', 'link' => url('admin/'.$this->class.'/')],

            [
                'name' => 'quotation_sales_manager_id',
                'label' => 'In Charge',
                'type' => "quotation_sales_manager",
                'entity'=>'aircraftClient',
                'attribute'=>'name',
                'model' => 'App\Models\aircraftClient'
            ],
//
//            ['name' => 'quotation_sales_manager_id', 'label'=>'In Charge', 'entity'=>'user', 'type' => "select",
//                'attribute'=>'name','model' => 'App\Models\User' ],
            
            ['name' => 'aircraft_type_id', 'label'=>'Aircraft Type', 'entity'=>'aircraftType', 'type' => "operationAircraftType",
                'attribute'=>'name','model' => 'App\Models\AircraftType' ],
            [
                'name' => 'routes',
                'label' => 'Route',
                'type' => "model_function",
                'function_name' => 'operationAirports'
            ],
            [
                'name' => 'quotation_services_type',
                'label' => 'Service Type',
                'type' => 'text',
                'wrapperAttributes'=>['class'=>'form-group col-md-6']
            ],
            ['name' => 'aircraft_client_id', 'label'=>'A. Client', 'entity'=>'aircraftClient', 'type' => "select",
                'attribute'=>'name','model' => 'App\Models\Company'
            ],
            [
                'name' => 'country',
                'label' => 'Client Country',
                'type' => "quotation_country",
                'entity'=>'aircraftClient',
                'attribute'=>'name',
                'model' => 'App\Models\Company'
            ],
            [
                'name' => 'state',
                'label' => "Status",
                'type' => "quotation_status",
                'function_name' => 'getStatusAttribute'
            ],
        ];
    }

}
