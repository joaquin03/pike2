<?php
namespace App\Http\Fields;

use App\Models\Company;
use App\Models\Utilities\Country;

/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 8/1/17
 * Time: 4:46 PM
 */
class OperatorFields extends CompanyFields
{
    public function getColumnsFields()
    {
        return [
            ['name' => 'name', 'type'=> 'link_item', 'link'=>url('admin/operator/')],
            ['name' => 'country', 'type' => 'model_function', 'function_name' =>'getCountry'],
            ['name' => 'business'], ['name'=>'updated_at', 'label'=>'Latest activity', 'type' => 'model_function', 'function_name' =>'lastActivityDate'],
        ];
    }
}