<?php
namespace App\Http\Fields;
use App\Models\Accounting\Bill;
use Carbon\Carbon;

/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 8/1/17
 * Time: 4:46 PM
 */
class BillsProcurementFields extends AbstractFields
{
    public function getFormFields()
    {
        return [
            [
                'name' => 'bill_number',
                'label' => 'Bill Number',
                'type' => 'text',
                'wrapperAttributes'=>['class'=>'form-group col-md-3']
            ],
            [
                'name' => 'amount',
                'label' => 'Amount',
                'type' => 'number',
                'prefix' => 'USD',
                'attributes' => ['disabled' => 'disabled'],
                'wrapperAttributes'=>['class'=>'form-group col-md-3']
            ],
            [ // select_from_array
                'name' => 'status',
                'label' => "Status",
                'type' => 'select_from_array',
                'options' => ['not_paid' => 'Not Paid', 'partially_paid' => 'Partially Paid', 'paid' => 'Paid'],
                'allows_null' => false,
                'default' => 'paid',
                'wrapperAttributes'=>['class'=>'form-group col-md-3', 'disabled' => 'disabled']
            ],
            [   // Upload
                'name' => 'attachment',
                'label' => 'Attached Files',
                'type' => 'upload_multiple',
                'upload' => true,
                'disk' => 'local', // if you store files in the /public folder, please ommit this; if you store them in /storage or S3, please specify it;
                'wrapperAttributes'=>['class'=>'form-group col-md-3']
            ],
            [ // select_from_array
                'name' => 'payment_method',
                'label' => "Payment method",
                'type' => 'bill_select_from_array',
                'options' => [2 => 'Credit', 1 => 'Cash' ],
                'allows_null' => false,
                'default' => 2,
                'wrapperAttributes'=>['class'=>'form-group col-md-3']
            ],
            [   // date_picker
                'name' => 'start_date',
                'add_days_field' => 'billing_days_to_pay',
                'type' => 'date_picker_days_to_pay',
                'label' => 'Bill Date',
                // optional:
                'datetime_picker_options' => [
                    'format' => 'DD/MM/YYYY',
                    'language' => 'es',
                ],
                'default' => Carbon::now()->format('Y-m-d'),
                'wrapper_child_id' => 'due_date',
                'wrapperAttributes'=>['class'=>'form-group col-md-3', 'id'=>'start_date']
            ],
            [
                'name' => 'billing_days_to_pay',
                'label' => 'Billing days',
                'type' => 'text',
                'wrapperAttributes'=>['class'=>'form-group col-md-3'],
                'attributes' => ['disabled' => 'disabled']
            ],
            [   // date_picker
                'name' => 'due_date',
                'type' => 'date_picker_custom',
                'label' => 'Due Date',
                'datetime_picker_options' => [
                    'format' => 'DD/MM/YYYY',
                    'language' => 'es'
                ],
                'wrapperAttributes'=>['class'=>'form-group col-md-3', 'id'=>'due_date']
            ],
            [
                'name' => 'note',
                'label' => 'Notes',
                'type' => 'textarea',
                'wrapperAttributes'=>['class'=>'form-group col-md-12']
            ],
            [
                'name' => 'accounting_note',
                'label' => 'Services',
                'type' => 'procurement_accounting_services',
                'wrapperAttributes'=>['class'=>'form-group col-md-3']

            ],

        ];
    }
    public function getColumnsFields()
    {
        return [
            ['name' => 'bill_number', 'label' => 'Number', 'type' => 'link_edit', 'link' => url('admin\bills')],
            [
                'name' => 'company_id',
                'label' => 'Company',
                'entity' => 'company',
                'type' => "select",
                'attribute' => 'name',
                'model' => 'App\Models\Company'
            ],
            [
                'name' => 'amount', // The db column name
                'label' => "Amount", // Table column heading
                'type' => "number",
                // 'prefix' => "$",
                // 'suffix' => " EUR",
                // 'decimals' => 2,
            ],
            [
                'name' => "date", // The db column name
                'label' => "Bill Date", // Table column heading
                'type' => "date"
            ],
            [
                'name' => "due_date", // The db column name
                'label' => "Due Date", // Table column heading
                'type' => "date"
            ],
            [
                'name' => "status", // The db column name
                'label' => "Status", // Table column heading
                'type' => "text"
            ],
        ];
    }




}