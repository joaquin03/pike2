<?php
namespace App\Http\Fields;

use App\Models\Accounting\Bill;
use App\Models\Utilities\Country;
use Carbon\Carbon;

class SalesManagerFields extends AbstractFields
{
    
    public function getFormFields()
    {
        return [
            [
                'name' => 'name',
                'label' => 'Name *',
                'type' => 'text',
                'wrapperAttributes'=> ['class'=>'form-group col-md-6', 'required'=>true],
            ],
            [ // select_from_array
                'label' => 'Country *',
                'name' => 'countries',
                'type' => 'select2_from_array',
                'options' => Country::all(),
                'allows_multiple'=>true,
                'allows_null' => false,
                'wrapperAttributes' => ['class' => 'form-group col-md-6']
            ],
            [
                'name' => 'comments',
                'label' => 'Comments *',
                'type' => 'text',
                'wrapperAttributes'=> ['class'=>'form-group col-md-12'],
            ],
        ];
    }
    
    public function getColumnsFields()
    {
        return [
            ['name' => 'name', 'type' => 'text'],
            ['name' => 'countries', 'type' => 'model_function', 'function_name' =>'getCountries'],
        
        ];
    }
    
    
}
