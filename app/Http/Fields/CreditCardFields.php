<?php
namespace App\Http\Fields;

use App\Models\Accounting\Bill;
use Carbon\Carbon;

class CreditCardFields extends AbstractFields
{
    public function getFormFields()
    {
        return [
            [
                'name' => 'number',
                'label' => 'Credit Card Number',
                'type' => 'text',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'attributes' => ["step" => "any", "required" => "required"],
            ],
            [
                'name' => 'type',
                'type' => 'hidden',
                'label' => 'Type',
            
            ],
        ];
    }
    
    public function getColumnsFields()
    {
        return [
            [
                'name' => 'number',
                'label' => 'Credit Card Number',
                'type' => 'text',
                'wrapperAttributes' => ['class' => 'form-group col-md-6']
            ]
        
        ];
    }
    
    
}