<?php
namespace App\Http\Fields;
/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 8/1/17
 * Time: 4:46 PM
 */
class AircraftFields extends AbstractFields
{
    public function getFormFields()
    {
        return [
            [
                'name' => 'registration',
                'label'=> 'Registration',
                'type' => 'text',
                'wrapperAttributes'=>['class'=>'form-group col-md-4']
            ],
            [
                'name' => 'client_id',
                'label' => "Client",
                'type' => "select2",
                'entity' => 'type',
                'attribute' => "name",
                'model' => 'App\Models\Client',
                'wrapperAttributes'=>['class'=>'form-group col-md-4']
            ],[
                'name' => 'operator_id',
                'label' => "Operator",
                'type' => "select2",
                'entity' => 'type',
                'attribute' => "name",
                'model' => 'App\Models\Operator',
                'wrapperAttributes'=>['class'=>'form-group col-md-4']
            ],[
                'name' => 'aircraft_type_id',
                'label' => "Aircraft Type",
                'type' => "select2",
                'entity' => 'type',
                'attribute' => "name",
                'model' => 'App\Models\AircraftType',
                'wrapperAttributes'=>['class'=>'form-group col-md-3']
            ],[   // CustomHTML
                'name' => 'separator1',
                'type' => 'custom_html',
                'value' => '<h3 class="box-title with-border"></h3>'
            ],[
                'name' => 'equipment',
                'type' => 'text',
                'wrapperAttributes'=>['class'=>'form-group col-md-3']
            ],
            [
                'name' => 'pbn',
                'type' => 'text',
                'wrapperAttributes'=>['class'=>'form-group col-md-3']
            ],
            [
                'name' => 'nav',
                'type' => 'text',
                'wrapperAttributes'=>['class'=>'form-group col-md-3']
            ],
            [
                'name' => 'color',
                'type' => 'text',
                'wrapperAttributes'=>['class'=>'form-group col-md-3']
            ],[   // CustomHTML
                'name' => 'separator2',
                'type' => 'custom_html',
                'value' => '<h3 class="box-title with-border"></h3>'
            ], [
                'name' => 'dinghies_number',
                'label'=> 'Dinghies Number',
                'type' => 'number',
                'wrapperAttributes'=>['class'=>'form-group col-md-6']
            ],[
                'name' => 'dinghies_capacity',
                'label'=> 'Dinghies Capacity',
                'type' => 'number',
                'wrapperAttributes'=>['class'=>'form-group col-md-6']
            ],[
                'name' => 'dinghies_cover',
                'label'=> 'Dinghies Cover',
                'type' => 'text',
                'wrapperAttributes'=>['class'=>'form-group col-md-6']
            ],[
                'name' => 'dinghies_colour',
                'label'=> 'Dinghies Colour',
                'type' => 'text',
                'wrapperAttributes'=>['class'=>'form-group col-md-6']
            ],[   // Upload
                'name' => 'documents',
                'label' => 'Documents',
                'type' => 'upload_multiple_with_original_names',
                'disk' => 'public',
                'upload' => true,
            ],
        ];
    }
    
    public function getColumnsFields()
    {
        return [
            ['name' => 'registration', 'label' =>'Registration', 'type'=> 'link_edit', 'link'=>url('admin/aircraft/')]
            , ['name' => 'client_id', 'label' =>'Client', 'type'=> 'select',
                'entity' => 'client', 'attribute' => 'name', 'model'=> 'App\Models\Client'],
            ['name' => 'operator_id', 'label' =>'Operator', 'type'=> 'select',
                'entity' => 'operator', 'attribute' => 'name', 'model'=> 'App\Models\Operator'],
            ['name' => 'aircraft_type_id',
            'label' => "Aircraft Type",
            'type' => "select",
            'entity' => 'type',
            'attribute' => "name",
            'model' => 'App\Models\AircraftType',]
        ];
    }
    
}
