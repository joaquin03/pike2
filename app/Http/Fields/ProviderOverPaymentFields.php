<?php
namespace App\Http\Fields;

use Carbon\Carbon;

/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 8/1/17
 * Time: 4:46 PM
 */
class ProviderOverPaymentFields extends ProviderPaymentFields
{
    public function getFormFields()
    {
        return [
            [
                'name' => 'is_over_payment',
                'type'  => 'hidden',
                'value' => '1'
            ],
            [
                'name' => 'payment_number',
                'label' => 'Payment Number',
                'type' => 'text',
                'wrapperAttributes'=>['class'=>'form-group col-md-6']
                ],
            [   // date_picker
                'name' => 'date',
                'type' => 'datetime_picker',
                'label' => 'Payment Date',
                // optional:
                'datetime_picker_options' => [
                    'format' => 'DD/MM/YYYY',
                    'language' => 'es'
                ],
                'wrapperAttributes'=>['class'=>'form-group col-md-6'],
                'default' => Carbon::now(),
                'attributes' => ["required" => "required"],
            ],
            [
                'name' => 'amount',
                'label' => 'Amount',
                'type' => 'number',
                'prefix' => 'USD',
                'attributes' => ["step" => "any"],
                'wrapperAttributes'=>['class'=>'form-group col-md-3']
            ],
            [ // select_from_array
                'name' => 'payment_method',
                'label' => "Payment Method",
                'type' => 'credit_card_select',
                'options' => [  'wire transfer' =>  ['cash' => 'CASH', 'santander' => 'SANTANDER', 'bandes' => 'BANDES', 'banistmo' => 'BANISTMO'],
                    'offset'        =>  ['offset'   => 'Offset'],
                    
                ],
                
                'allows_null' => false,
                'wrapperAttributes'=>['class'=>'form-group col-md-3']
            ],
            [   // Upload
                'name' => 'attachment',
                'label' => 'Attached Files',
                'type' => 'upload_multiple',
                'upload' => true,
                'disk' => 'local', // if you store files in the /public folder, please ommit this; if you store them in /storage or S3, please specify it;
                'wrapperAttributes'=>['class'=>'form-group col-md-6']
            ],
            [
                'name' => 'note',
                'label' => 'Notes',
                'type' => 'textarea',
            ],
            [
                'label' => 'Payment',
                'name' => 'invoices',
                'type' => 'bill_over_payments_info',
                'entity' => 'invoices', // the method that defines the relationship in your Model
                'attribute' => 'bill_number', // foreign key attribute that is shown to user
                'model' => 'App\Models\Accounting\Invoice', // foreign key model
                'pivot' => true, // on create&update, do you need to add/delete pivot table entries?
                'wrapperAttributes'=>['class'=>'form-group col-md-6'],
                'options' => ['type' => 'payment']
            ]

        ];
    }

    
}