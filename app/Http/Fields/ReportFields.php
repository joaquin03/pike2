<?php

namespace App\Http\Fields;

use App\Models\Company;
use App\Models\Operator;
use App\Models\Utilities\Country;


class ReportFields extends AbstractFields
{
    public function getFormFields()
    {
        return [
        
            ['name' => 'date', 'label' => 'Date', 'type' => 'date_picker'],
            [
                'name' => 'place',
                'label' => 'Place',
                'type' => 'text',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
            ],
            [
                'name' => 'objectives',
                'label' => 'Objectives',
                'type' => 'textarea',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
            ],
        
            [
                'label' => "Participants",
                'type' => "activity_user",
                'name' => 'participants',
                'entity' => 'contacts',
                'model' => 'App\Models\User',
                'attribute' => "name",
                'pivot' => true,
        
            ],
            [
                'name' => 'background_info',
                'label' => 'Background Information',
                'type' => 'textarea',
            ],
            [
                'label' => "Tags",
                'type' => 'select2',
                'name' => 'tags',
                'entity' => 'tags',
                'attribute' => 'name',
                'model' => "App\Models\CompanyTag",
                'pivot' => true,
            ],
        ];
    }
    
    public function getColumnsFields()
    {
        return [
            
            ['name' => 'date', 'label' => 'Date', 'type' => 'datetime'],
            [
                'name' => 'place',
                'label' => 'Place',
                'type' => 'text',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
            ],
            [
                'name' => 'objectives',
                'label' => 'Objectives',
                'type' => 'text',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
            ],
    
            [
                'label' => "Participants",
                'type' => "activity_user",
                'name' => 'participants',
                'entity' => 'user',
                'model' => 'App\Models\User',
                'attribute' => "name",
                'pivot' => true,
    
            ]
        ];
    }
}