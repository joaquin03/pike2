<?php

namespace App\Http\Fields;

use App\Models\Company;
use App\Models\Operator;
use App\Models\Utilities\Country;


class CrewMemberFields extends AbstractFields
{
    public function getFormFields()
    {
        return [
            [   // CustomHTML
                'name' => 'separator1',
                'type' => 'custom_html',
                'value' => '<h3 class="box-title with-border">General</h3>'
            ],
            [
                'name' => 'name',
                'label' => 'Name',
                'type' => 'text',
                'wrapperAttributes' => ['class' => 'form-group col-md-6']
            ],
            [ // select_from_array
                'name' => 'country',
                'type' => 'select2_from_array',
                'options' => Country::all(),
                'allows_null' => false,
                'wrapperAttributes' => ['class' => 'form-group col-md-6']
                // 'allows_multiple' => true, // OPTIONAL; needs you to cast this to array in your model;
            ],
            [
                'name' => 'roles',
                'label' => 'Roles',
                'type' => 'select2_from_array',
                'options' => [
                    "is_captain" => "Captain",
                    "is_first_official" => "First Official",
                    "is_auxiliary" => "Auxiliary"
                ],
                'allows_null' => true,
                'allows_multiple' => true,
                'wrapperAttributes' => ['class' => 'form-group col-md-6']
            ],
            [
                'name' => 'mail',
                'label' => 'Mail',
                'type' => 'email',
                'wrapperAttributes' => ['class' => 'form-group col-md-6']
            ],
            [
                'name' => 'operator_id',
                'label' => "Operator",
                'type' => "select2",
                'entity' => 'operator',
                'attribute' => "name",
                'model' => 'App\Models\Operator',
                'wrapperAttributes' => ['class' => 'form-group col-md-6']
            ],
            [   // DateTime
                'name' => 'birthdate',
                'label' => 'Birthdate',
                'type' => 'datetime_picker',
                // optional:
                'datetime_picker_options' => [
                    'format' => 'DD/MM/YYYY',
                    'language' => 'es'
                ],
                'wrapperAttributes' => ['class' => 'form-group col-md-6']
            ],
            [   // Upload
                'name' => 'medical_certificate',
                'label' => 'Medical certificate',
                'type' => 'upload_with_original_names',
                'upload' => true,
                'disk' => 'public',

                'wrapperAttributes' => ['class' => 'form-group col-md-6']
            ],
            [   // DateTime
                'name' => 'medical_certificate_expire_date',
                'label' => 'Expire date',
                'type' => 'datetime_picker',
                // optional:
                'datetime_picker_options' => [
                    'format' => 'DD/MM/YYYY',
                    'language' => 'es'
                ],
                'wrapperAttributes' => ['class' => 'form-group col-md-6']
            ],
            [   // CustomHTML
                'name' => 'separator2',
                'type' => 'custom_html',
                'value' => '<h3 class="box-title with-border">Passport</h3>',
            ],
            [
                'name' => 'passport_number',
                'label' => 'Passport number',
                'type' => 'text',
                'wrapperAttributes' => ['class' => 'form-group col-md-6']
            ],
            [   // Upload
                'name' => 'passport',
                'label' => 'Upload',
                'type' => 'upload_with_original_names',
                'upload' => true,
                'disk' => 'public',

                'wrapperAttributes' => ['class' => 'form-group col-md-6']
            ],
            [   // DateTime
                'name' => 'expire_date',
                'label' => 'Expire date',
                'type' => 'datetime_picker',
                // optional:
                'datetime_picker_options' => [
                    'format' => 'DD/MM/YYYY',
                    'language' => 'es'
                ],
                'wrapperAttributes' => ['class' => 'form-group col-md-6']
            ],
            [   // CustomHTML
                'name' => 'separator3',
                'type' => 'custom_html',
                'value' => '<h3 class="box-title with-border">Licence </h3>',
            ],
            
            [
                'name' => 'licence_number',
                'label' => 'Licence number',
                'type' => 'number',
                'wrapperAttributes' => ['class' => 'form-group col-md-6']
            ],
            [   // Upload
                'name' => 'licence',
                'label' => 'Upload',
                'type' => 'upload_with_original_names',
                'upload' => true,
                'disk' => 'public',

                'wrapperAttributes' => ['class' => 'form-group col-md-6']
            ],
            [   // DateTime
                'name' => 'licence_number_expire_at',
                'label' => 'Expire date',
                'type' => 'datetime_picker',
                // optional:
                'datetime_picker_options' => [
                    'format' => 'DD/MM/YYYY',
                    'language' => 'es'
                ],
                'wrapperAttributes' => ['class' => 'form-group col-md-6']
            ],
        
        ];
    }
    
    public function getColumnsFields()
    {
        return [
            ['name' => 'id'],
            ['name' => 'name', 'label' => 'Name', 'type' => 'link_edit', 'link' => url('admin/crew-member/')],
            ['name' => 'roles', 'label' => "Roles", 'type' => 'model_function', 'function_name' => 'getRolesString'],
            ['name' => 'country', 'type' => 'model_function', 'function_name' => 'getCountry'],
            [
                'name' => 'operator_id',
                'label' => "Operator",
                'type' => "select",
                'entity' => 'operator',
                'attribute' => "name",
                'model' => 'App\Models\Operator',
            ],
        
        ];
    }
}