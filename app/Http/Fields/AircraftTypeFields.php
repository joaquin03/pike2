<?php

namespace App\Http\Fields;

/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 8/1/17
 * Time: 4:46 PM
 */
class AircraftTypeFields extends AbstractFields
{
    public function getFormFields()
    {
        return [
            [   // CustomHTML
                'name' => 'separator1',
                'type' => 'custom_html',
                'value' => '<h3 class="box-title with-border">General</h3>',
            ],
            [
                'name' => 'manufacturer',
                'type' => 'text',
                'wrapperAttributes' => ['class' => 'form-group col-md-3'],
            ],
            [
                'name' => 'model',
                'type' => 'text',
                'wrapperAttributes' => ['class' => 'form-group col-md-3'],
            ],
            [
                'name' => 'code',
                'type' => 'text',
                'wrapperAttributes' => ['class' => 'form-group col-md-3'],
            ],[
                'name' => 'MTOW',
                'type' => 'number',
                'wrapperAttributes' => ['class' => 'form-group col-md-3'],
            ],
            [   // CustomHTML
                'name' => 'separator2',
                'type' => 'custom_html',
                'value' => '<h3 class="box-title with-border">Seats</h3>'
            ],
            [
                'name' => 'crew_seats',
                'type' => 'number',
                'wrapperAttributes' => ['class' => 'form-group col-md-4'],
            ],
            [
                'name' => 'min_seats',
                'type' => 'number',
                'wrapperAttributes' => ['class' => 'form-group col-md-4'],
            ],
            [
                'name' => 'max_seats',
                'type' => 'number',
                'wrapperAttributes' => ['class' => 'form-group col-md-4'],
            ],
            [   // CustomHTML
                'name' => 'separator3',
                'type' => 'custom_html',
                'value' => '<h3 class="box-title with-border">Services required</h3>'
            ],
            [
                'name' => 'needs_stairs',
                'type' => 'enum',
                'label' => 'Stairs',
                'wrapperAttributes' => ['class' => 'form-group col-lg-3'],
            ],
            [
                'name' => 'needs_toilette',
                'type' => 'enum',
                'label' => 'Toilette',
                'wrapperAttributes' => ['class' => 'form-group col-lg-3'],
            ],
            [
                'name' => 'needs_bus',
                'type' => 'enum',
                'label' => 'Bus',
                'wrapperAttributes' => ['class' => 'form-group col-lg-3'],
            ],
            [
                'name' => 'needs_conveyor_belt',
                'type' => 'enum',
                'label' => 'Conveyor belt',
                'wrapperAttributes' => ['class' => 'form-group col-lg-3'],
            ],
            [
                'name' => 'needs_chocks',
                'type' => 'enum',
                'label' => 'Chocks',
                'wrapperAttributes' => ['class' => 'form-group col-lg-3'],
            ],
            [
                'name' => 'needs_asu',
                'type' => 'enum',
                'label' => 'Asu',
                'wrapperAttributes' => ['class' => 'form-group col-lg-3'],
            ],
            [
                'name' => 'needs_gpu',
                'type' => 'enum',
                'label' => 'Gpu',
                'wrapperAttributes' => ['class' => 'form-group col-lg-3'],
            ],
            [   // CustomHTML
                'name' => 'separator4',
                'type' => 'custom_html',
                'value' => '<h3 class="box-title with-border">Others</h3>'
            ],[
                'name' => 'wake_turbulence_cat',
                'label' => 'WAKE TURBULENCE CAT',
                'type' => 'select2_from_array',
                'options' => [
                    "L" => "L",
                    "M" => "M",
                    "H" => "H",
                ],
                'allows_null' => true,
                'allows_multiple' => false,
                'wrapperAttributes' => ['class' => 'form-group col-md-2']
            ],[
                'name' => 'MTOW_Type',
                'label' => 'MTOW Type',
                'type' => 'enum',
                'wrapperAttributes' => ['class' => 'form-group col-md-2'],
                'attributes' => ["required" => "required"],
            ],
            [
                'name' => 'characteristic',
                'type' => 'textarea'
            ],
        ];
    }
    
    public function getColumnsFields()
    {
        return [
            ['name' => 'id', 'label' =>'Id', 'type'=> 'link_edit', 'link'=>url('admin/aircraft-type/')],
            'manufacturer',
            'model',
            'code'
        ];
    }
}