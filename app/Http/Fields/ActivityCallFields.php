<?php
namespace App\Http\Fields;

class ActivityCallFields extends AbstractFields
{
    public function getFormFields()
    {
        return [
            [   // CustomHTML
                'name' => 'separator1',
                'type' => 'custom_html',
                'value' => '<h3 class="box-title with-border">Call Information</h3>'
            ],
            ['name' => 'name', 'label' => 'Name'],
            [   // DateTime
                'name' => 'start_date',
                'label' => 'Start',
                'type' => 'datetime_picker',
                // optional:
                'datetime_picker_options' => [
                    'format' => 'DD/MM/YYYY HH:mm',
                    'language' => 'es'
                ],'wrapperAttributes'=>['class'=>'form-group col-md-6']
            ],[   // DateTime
                'name' => 'end_date',
                'label' => 'End',
                'type' => 'datetime_picker',
                // optional:
                'datetime_picker_options' => [
                    'format' => 'DD/MM/YYYY HH:mm',
                    'language' => 'es'
                ],'wrapperAttributes'=>['class'=>'form-group col-md-6']
            ],['name' => 'summary', 'label' =>'Summary', 'type' => 'textarea'],
            [   // CustomHTML
                'name' => 'separator2',
                'type' => 'custom_html',
                'value' => '<h3 class="box-title with-border">Activity Information</h3>'
            ],
            //TODO: CONTACTS
            [
                'label' => "Contacts",
                'type' => "activity_contact",
                'name' => 'contacts',
                'entity' => 'contacts',
                'model' => 'App\Models\Contact',
                'attribute' => "name",
                'pivot' => true,

            ],
        ];
    }

    public function getColumnsFields()
    {
        return [
            ['name' => 'name', 'label' => 'Name'],
            ['name' => 'start_date', 'label' => 'Start', 'type'=>'datetime_picker', 'wrapperAttributes'=>['class'=>'form-group col-md-6']],
            ['name' => 'end_date', 'label' => 'End', 'type'=>'datetime_picker', 'wrapperAttributes'=>['class'=>'form-group col-md-6']],
            ['name' => 'summary', 'label' =>'Summary', 'type' => 'textarea'],
           
        ];
    }
    
}