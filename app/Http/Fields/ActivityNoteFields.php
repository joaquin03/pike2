<?php
namespace App\Http\Fields;

use Carbon\Carbon;

class ActivityNoteFields extends AbstractFields
{
    public function getFormFields()
    {
        return [
            [   // CustomHTML
                'name' => 'separator1',
                'type' => 'custom_html',
                'value' => '<h3 class="box-title with-border">Note Information</h3>'
            ],
            [
                'name' => 'date',
                'label' => 'Date',
                'type' => 'date_picker_custom',
                'datetime_picker_options' => [
                    'format' => 'DD/MM/YYYY',
                    'language' => 'es'
                ],
            ],
            ['name' => 'title', 'label' => 'Title'],
            ['name' => 'body', 'label' => 'Body', 'type'=>'textarea'],
            
            //TODO: CONTACTS
            [   // CustomHTML
                'name' => 'separator2',
                'type' => 'custom_html',
                'value' => '<h3 class="box-title with-border">Activity Information</h3>'
            ],
            [
                'label' => "Contacts",
                'type' => "activity_contact",
                'name' => 'contacts',
                'entity' => 'contacts',
                'model' => 'App\Models\Contact',
                'attribute' => "name",
                'pivot' => true,
            ],
            [
                'label' => "Tags",
                'type' => 'select2_multiple',
                'name' => 'tags',
                'entity' => 'tags',
                'attribute' => 'name',
                'model' => "App\Models\CompanyTag",
                'pivot' => true,
            ],
        ];
    }

    public function getColumnsFields()
    {
        return [
            ['name' => 'title', 'label' => 'Title'],
            ['name' => 'body', 'label' => 'Body', 'type'=>'textarea'],
        ];
    }
    
}