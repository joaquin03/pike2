<?php
namespace App\Http\Fields;
use App\Models\Accounting\Bill;
use Carbon\Carbon;

/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 8/1/17
 * Time: 4:46 PM
 */
class ProvidersBalanceFields extends AbstractFields
{
    public function getFormFields()
    {
    
    
    }
    public function getColumnsFields()
    {
    
        return [
            ['name' => 'name', 'type'=> 'link_item', 'link'=>url('admin/provider-statement/')],
            ['name' => 'country', 'type' => 'model_function', 'function_name' =>'getCountry'],
            ['name' => 'business'],
            ['name' => 'amount', 'label' => "Balance", 'type' => "provider_status", 'function_name' => 'showBalance'],
        ];
    }




}