<?php
namespace App\Http\Fields;

use App\Models\Accounting\Bill;
use Carbon\Carbon;

/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 8/1/17
 * Time: 4:46 PM
 */
class ProviderCreditNoteFields extends AbstractFields
{
    public function getFormFields()
    {
        return [
            [
                'name' => 'payment_number',
                'label' => 'Credit Note Number',
                'type' => 'text ',
                'wrapperAttributes'=>['class'=>'form-group col-md-4'],
            ],
            [   // date_picker
                'name' => 'date',
                'type' => 'datetime_picker',
                'label' => 'Date',
                // optional:
                'datetime_picker_options' => [
                    'format' => 'DD/MM/YYYY HH:mm',
                    'language' => 'es'
                ],
                'default' => Carbon::now(),
                'attributes' => ["required" => "required"],
                'wrapperAttributes'=>['class'=>'form-group col-md-4']
            ],
            [
                'name' => 'amount',
                'label' => 'Amount',
                'type' => 'number',
                'prefix' => 'USD',
                'attributes' => ["step" => "any", 'disabled' => 'disabled'],
                'wrapperAttributes'=>['class'=>'form-group col-md-4'],
            ],
            [
                'name' => 'bill_link',
                'label' => 'Bill Link',
                'type' => 'bill_url',
                'wrapperAttributes'=>['class'=>'form-group col-md-6'],
            ],
            [
                'name' => 'note',
                'label' => 'Notes',
                'type' => 'textarea',
            ],
            [
                'label' => 'Credit Note',
                'name' => 'invoices',
                'type' => 'payments_info',
                'entity' => 'invoices', // the method that defines the relationship in your Model
                'attribute' => 'bill_number', // foreign key attribute that is shown to user
                'model' => 'App\Models\Accounting\Invoice', // foreign key model
                'pivot' => true, // on create&update, do you need to add/delete pivot table entries?
                'wrapperAttributes'=>['class'=>'form-group col-md-6'],
                'options' => ['type' => 'credit-note', 'text' => '']

            ]
            
            
        ];
    }
    public function getColumnsFields()
    {
        return [
            [
                'name' => 'payment_number',
                'label' => 'Credit Note Number',
                'type' => 'link_edit',
                'link' => url('admin\credit-notes'),
                'wrapperAttributes'=>['class'=>'form-group col-md-6']
            ],
            [
                'name' => 'amount',
                'label' => "Amount",
                'type' => "number",
            ],
            [
                'name' => "date",
                'label' => "Date",
                'type' => "date"
            ],
        ];
    }




}