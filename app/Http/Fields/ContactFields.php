<?php

namespace App\Http\Fields;

use App\Models\Company;
use App\Models\Utilities\Country;
use Illuminate\Support\Facades\Request;

class ContactFields extends AbstractFields
{
    public function getFormFields()
    {
        return [
            [
                'name' => 'name',
                'label' => 'Name',
                'type' => 'text',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
            ],
            [
                'name' => 'surname',
                'label' => 'Surname',
                'type' => 'text',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
            ],
            [
                'name' => 'role',
                'label' => 'Role',
                'type' => 'text',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
            ],
            [
                'name' => 'company_id',
                'label' => 'Company',
                'type' => 'company_info',
                'entity' => 'company',
                'attribute' => "name",
                'model' => 'App\Models\Company',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'default'   => Request::has('company_id')? Request::get('company_id'):null, // default value
            ],
    
            [
                'name' => 'description',
                'label' => 'Description',
                'type' => 'textarea',
    
            ],
            [ // Table
                'name' => 'emails',
                'label' => 'Emails',
                'type' => 'company_table',
                'entity_singular' => 'Email',
                'columns' => [
                    'comment' => 'Comment',
                    'email' => 'Email',
                ],
                'options' => ['Labor email', 'Personal email'],
                'min' => 0,
                'store_as_json' => true
            ],
            [ // Table
                'name' => 'phones',
                'label' => 'Phones',
                'type' => 'company_table',
                'entity_singular' => 'phone',
                'columns' => [
                    'comment' => 'Comment',
                    'phone' => 'Phone',
                ],
                'options' => ['Main Office', 'Personal', 'Cell', 'Fax'],
                'min' => 0,
                'store_as_json' => true,
            ],
            [ // Table
                'name' => 'address',
                'label' => 'Address',
                'type' => 'table',
                'entity_singular' => 'address',
                'columns' => [
                    'address' => 'Address',
                    'city' => 'City',
                    'zip_code' => 'Zip code',
                    'country' => 'Country',
                ],
                'min' => 0,
                'store_as_json' => true
            ],
        ];
    }

    public function getColumnsFields()
    {
        return [
            ['name' => 'name', 'type' => 'link_edit', 'link' => url('admin/contact/')],
            ['name' => 'surname'],
            ['name' => 'role'],
            [
                'name' => 'company_id',
                'label' => "Company",
                'type' => "select",
                'entity' => 'company',
                'attribute' => "name",
                'model' => 'App\Models\Company',
            ],
            ['name' => 'description'],
        ];
    }
}