<?php
/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 1/11/18
 * Time: 11:21 AM
 */

namespace App\Http\Fields;


class BillingFields extends ProcurementFields
{
    protected $class = 'billing';
}