<?php

namespace App\Http\Fields;

/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 8/1/17
 * Time: 4:46 PM
 */
class HandlingServiceQuotationFields extends AbstractFields
{
    public function getFormFields()
    {
        return [
            ['name' => 'tons_from', 'type' => 'number', 'wrapperAttributes' => ['class' => 'form-group col-md-4']],
            ['name' => 'tons_to', 'type' => 'number', 'wrapperAttributes' => ['class' => 'form-group col-md-4']],
            ['name' => 'price', 'type' => 'number', 'wrapperAttributes' => ['class' => 'form-group col-md-4']],
            
        
        
        ];
    }
    
    public function getColumnsFields()
    {
        return [
            'tons_from',
            'tons_to',
            'start_date',
            'price',
        ];
    }
    
}