<?php

namespace App\Http\Fields;

use App\Models\Company;
use App\Models\Utilities\Country;


class PermanentPermissionsFields extends AbstractFields
{
    public function getFormFields()
    {
        return [
            [   // CustomHTML
                'name' => 'separator1',
                'type' => 'custom_html',
                'value' => '<h3 class="box-title with-border">General</h3>'
            ],
            [
                'name' => 'file_name',
                'label' => 'Name',
                'type' => 'text',
                'wrapperAttributes' => ['class' => 'form-group col-md-6']
            ],
            [ // select_from_array
                'name' => 'type',
                'type' => 'select_from_array',
                'options' => [
                    'landing' => 'Landing',
                    'overflight' => 'Overflight',
                    'landing diplomatic' => 'Landing diplomatic',
                    'overflight diplomatic' => 'Overflight diplomatic'
                ],
                'allows_null' => false,
                'wrapperAttributes' => ['class' => 'form-group col-md-6']
            ],
            [
                'name' => 'code',
                'label' => 'Code',
                'type' => 'text',
                'wrapperAttributes' => ['class' => 'form-group col-md-6']
            ],
            [   // DateTime
                'name' => 'start_at',
                'label' => 'Start at',
                'type' => 'datetime_picker',
                // optional:
                'datetime_picker_options' => [
                    'format' => 'DD/MM/YYYY HH:mm',
                    'language' => 'es'
                ],
                'wrapperAttributes' => ['class' => 'form-group col-md-6']
            ],
            [   // DateTime
                'name' => 'expire_at',
                'label' => 'Expire at',
                'type' => 'datetime_picker',
                // optional:
                'datetime_picker_options' => [
                    'format' => 'DD/MM/YYYY HH:mm',
                    'language' => 'es'
                ],
                'wrapperAttributes' => ['class' => 'form-group col-md-6']
            ],
            [ // select_from_array
                'name' => 'country',
                'type' => 'select2_from_array',
                'options' => Country::all(),
                'allows_null' => false,
                'wrapperAttributes' => ['class' => 'form-group col-md-6']
                // 'allows_multiple' => true, // OPTIONAL; needs you to cast this to array in your model;
            ],
            [
                'name' => 'provider_id',
                'label' => "Provider",
                'type' => "select2",
                'entity' => 'provider',
                'attribute' => "name",
                'model' => 'App\Models\Provider',
                'wrapperAttributes' => ['class' => 'form-group col-md-6']
            ],
            [   // CustomHTML
                'name' => 'separator2',
                'type' => 'custom_html',
                'value' => '<h3 class="box-title with-border">Aircrafts</h3>',
                'wrapperAttributes' => ['class' => 'form-group col-md-6']
            ],
            [       // Select2Multiple = n-n relationship (with pivot table)
                'label' => "Aircrafts",
                'type' => 'select2_multiple',
                'name' => 'aircrafts', // the method that defines the relationship in your Model
                'entity' => 'aircrafts', // the method that defines the relationship in your Model
                'attribute' => 'registration', // foreign key attribute that is shown to user
                'model' => "App\Models\Aircraft", // foreign key model
                'pivot' => true, // on create&update, do you need to add/delete pivot table entries?,
            ],
            [   // CustomHTML
                'name' => 'separator3',
                'type' => 'custom_html',
                'value' => '<h3 class="box-title with-border">File</h3>',
                'wrapperAttributes' => ['class' => 'form-group col-md-6']
            ],
            [   // Upload
                'name' => 'file',
                'label' => 'File',
                'type' => 'upload',
                'upload' => true,
                'disk' => 'public'
                // if you store files in the /public folder, please ommit this; if you store them in /storage or S3, please specify it;
            ],
            [
                'name' => 'file_name',
                'label' => 'Name',
                'type' => 'text'
            ],
            [
                'name' => 'file_notes',
                'label' => 'Notes',
                'type' => 'textarea'
            ],
        
        
        ];
    }
    
    public function getColumnsFields()
    {
        return [

            ['name' => 'code', 'label' =>'Code', 'type'=> 'link_edit', 'link'=>url('admin/permanent-permission/')],
            [
                'name'      => 'file_name',
                'label'     => 'Name',
            ],
            [
                'name'  => 'type',
                'label' => "Type",
                'type'  => "permanent_permissions_type",
            ],
            ['name' => 'country', 'type' => 'model_function', 'function_name' => 'getCountry'],
            [
                'name'      => 'provider_id',
                'label'     => "Provider",
                'type'      => "select",
                'entity'    => 'provider',
                'attribute' => "name",
                'model'     => 'App\Models\Provider',
            ],
            [
                'label'     => "Aircrafts",
                'type'      => 'select_multiple',
                'name'      => 'aircrafts',
                'entity'    => 'aircrafts',
                'attribute' => 'registration',
                'model'     => "App\Models\Aircraft",
                'pivot'     => true,
            ],
            [
                'name'          => 'status',
                'label'         => "Status",
                'type'          => "permanent_permissions_status",
                'function_name' => 'getStatusAttribute'
            ],
        
        
        ];
    }
}