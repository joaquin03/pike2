<?php
namespace App\Http\Fields;

use App\Models\Accounting\Bill;
use Carbon\Carbon;

class ClientPaymentFields extends AbstractFields
{
    public function getFormFields()
    {
        return [
            [
                'name' => 'payment_number',
                'label' => 'Payment Number',
                'type' => 'text',
                'attributes' => ["required" => "required"],
                'wrapperAttributes'=>['class'=>'form-group col-md-6']
            ],
            [
                'name' => 'amount',
                'label' => 'Amount',
                'type' => 'number',
                'prefix' => 'USD',
                'attributes' => ["step" => "any", 'disabled'=>'disabled'],
                'wrapperAttributes'=>['class'=>'form-group col-md-3']
            ],
            [ // select_from_array
                'name' => 'payment_method',
                'label' => "Payment Method",
                'type' => 'select_from_array',
                'options' => [ 'wire transfer' => 'Wire Transfer', 'credit card' => 'Credit Card', 'cash' => 'Cash' ,
            ],
//                public static $paymentMethods = ['credit card', 'wire transfer', 'cash', 'prepaid'];
                'allows_null' => false,
                'wrapperAttributes'=>['class'=>'form-group col-md-3']
            ],
            [   // date_picker
                'name' => 'date',
                'type' => 'datetime_picker',
                'label' => 'Payment Date',
                // optional:
                'datetime_picker_options' => [
                    'format' => 'DD/MM/YYYY',
                    'language' => 'es'
                ],
                'default' => Carbon::now(),
                'attributes' => ["required" => "required"],
                'wrapperAttributes'=>['class'=>'form-group col-md-6']
            ],
            
            [   // Upload
                'name' => 'attachment',
                'label' => 'Attached Files',
                'type' => 'upload_multiple',
                'upload' => true,
                'disk' => 'local', // if you store files in the /public folder, please ommit this; if you store them in /storage or S3, please specify it;
                'wrapperAttributes'=>['class'=>'form-group col-md-6']
            ],
            [
                'name' => 'note',
                'label' => 'Notes',
                'type' => 'textarea',
            ],
            [
                'name' => 'billsAndDebitNotes',
                'type' => 'bill_payments_info',
                'entity' => 'bills', // the method that defines the relationship in your Model
                'attribute' => 'bill_number', // foreign key attribute that is shown to user
                'model' => 'App\Models\Accounting\Bill', // foreign key model
                'pivot' => true, // on create&update, do you need to add/delete pivot table entries?
                'wrapperAttributes'=>['class'=>'form-group col-md-6'],
                'options' => ['type' => 'payment', 'text' => 'When there is not any bills, the Amount is equal to the set up global amount, otherwise the Amount will be overwrited by the sum of all the bill amounts']
            ]

        ];
    }


    public function getColumnsFields()
    {
        return [
            ['name' => 'company_id', 'label' =>'Company', 'type'=> 'select',
                'entity' => 'company', 'attribute' => 'name', 'model'=> 'App\Models\Company'],
            [
                'name' => 'payment_number',
                'label' => 'Payment Number',
                'type' => 'link_edit',
                'link' => url('admin\payments'),
                'wrapperAttributes'=>['class'=>'form-group col-md-6']
            ],
            [
                'name' => 'company_id',
                'label' => 'Company',
                'entity' => 'company',
                'type' => "select",
                'attribute' => 'name',
                'model' => 'App\Models\Company'
            ],
            [
                'name' => 'amount',
                'label' => "Amount",
                'type' => "number",
            ],
            [
                'name' => "date",
                'label' => "Payment Date",
                'type' => "date"
            ],
            [
                'name' => "type", // The db column name
                'label' => "Type", // Table column heading
                'type' => "text"
            ],
        ];
    }




}