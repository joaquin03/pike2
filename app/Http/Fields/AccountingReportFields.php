<?php
namespace App\Http\Fields;
use App\Models\Accounting\Bill;
use Carbon\Carbon;

/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 8/1/17
 * Time: 4:46 PM
 */
class AccountingReportFields extends AbstractFields
{
    public function getFormFields()
    {
    
    
    }
    public function getColumnsFields()
    {
    
        return [
            ['name' => 'date', 'label'=>'Date', 'type'=>'date'],
            ['name' => 'due_date', 'label'=>'Due Date', 'type'=>'date'],
            ['name' => 'type', 'label' => "Type", 'type' => "model_function", 'function_name'=> 'getDescription'],
            ['name' => 'code', 'label' => 'Code', 'type' => 'model_function', 'function_name' => 'getAccountableNumber'],
            ['name' => 'company_id', 'label'=>'Client', 'entity'=>'company', 'type' => "select", 'attribute'=>'name',
                'model' => 'App\Models\Company'],
            ['name' => 'credit', 'label' => 'Credit', 'type' => 'model_function', 'function_name' => 'getCredit'],
            ['name' => 'debit', 'label' => 'Debit', 'type' => 'model_function', 'function_name' => 'getDebit'],

            
        ];
    }




}