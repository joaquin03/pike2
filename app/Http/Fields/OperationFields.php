<?php

namespace App\Http\Fields;

use App\Models\Company;
use App\Models\Operator;
use App\Models\Utilities\Country;


class OperationFields extends AbstractFields
{
    public function getFormFields()
    {
        return [];
    }
    
    public function getColumnsFields()
    {
        return [
            
            ['name' => 'code', 'label' => 'ID', 'type' => 'link_edit', 'link' => url('admin/operation/')],
            ['name' => 'start_date', 'label' => 'Date', 'type' => 'date'],
            [
                'name' => 'aircraft_id',
                'label' => 'Aircraft',
                'entity' => 'aircraft',
                'type' => "select",
                'attribute' => 'registration',
                'model' => 'App\Models\Aircraft'
            ],
            [
                'name' => 'routes',
                'label' => 'Route',
                'type' => "model_function",
                'function_name' => 'operationAirports'
            ],
            [
                'name' => 'in_charge_user_id',
                'label' => 'In Charge',
                'entity' => 'user',
                'type' => "select",
                'attribute' => 'name',
                'model' => 'App\Models\User'
            ],
//            ['name' => 'aircraft_operator_id', 'label'=>'A. Operator', 'entity'=>'aircraftOperator', 'type' => "select",
//                'attribute'=>'name','model' => 'App\Models\Company' ],
            [
                'name' => 'aircraft_operator',
                'label' => 'A. Operator',
                'type' => "operations_operator",
                'function_name' => 'getOperatorName'
            ],
            
            [
                'name' => 'aircraft_client_id',
                'label' => 'A. Client',
                'entity' => 'aircraftClient',
                'type' => "select",
                'attribute' => 'name',
                'model' => 'App\Models\Company'
            ],
            [
                'name' => 'state',
                'label' => "Status",
                'type' => "operations_status",
                'function_name' => 'getStatusAttribute'
            ],
            [
                'name' => 'delete',
                'label' => "",
                'type' => "model_function",
                'function_name' => 'logicalDeletion'
            ],

        
        ];
    }
}