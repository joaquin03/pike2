<?php
namespace App\Http\Fields;
use App\Models\Accounting\Bill;
use Carbon\Carbon;

/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 8/1/17
 * Time: 4:46 PM
 */
class BillsFields extends AbstractFields
{
    public function getFormFields()
    {
        return [
            [   // Custom Field
                'name' => 'billing_company_id',
                'label' => '',
                'type' => 'billClientName',
            ],
            [
                'name' => 'bill_info',
                'label' => 'Bill Info Link',
                'type' => 'bill_info_url',
                'wrapperAttributes'=>['class'=>'form-group col-md-6'],
            ],

            [
                'name' => 'bill_number',
                'label' => 'Bill Number',
                'type' => 'bill_text',
                'wrapperAttributes'=>['class'=>'form-group col-md-6'],
                'attributes' => ['disabled' => 'disabled']
            ],
            [
                'name' => 'amount',
                'label' => 'Amount',
                'type' => 'bill_number',
                'prefix' => 'USD',
                'wrapperAttributes'=>['class'=>'form-group col-md-3'],
                'attributes' => ['disabled' => 'disabled']
            ],
            [
                'name' => 'dollar_change',
                'label' => 'Dollar Exchange Rate UYU',
                'type' => 'bill_number',
                'attributes' => ["step" => "any", "required" => "required"],
                'wrapperAttributes'=>['class'=>'form-group col-md-3'],
                'default' => Bill::getLastItemOfTheDay(),
            ],
            [ // select_from_array
                'name' => 'payment_method',
                'label' => "Payment method",
                'type' => 'bill_select_from_array',
                'options' => [2 => 'Credit', 1 => 'Cash'],
                'allows_null' => false,
                'default' => 'paid',
                'wrapperAttributes'=>['class'=>'form-group col-md-3', 'id'=>'payment_method']
            ],
            [   // date_picker
                'name' => 'date',
                'add_days_field' => 'billing_days_to_pay',
                'type' => 'date_picker_days_to_pay',
                'label' => 'Bill Date',
                // optional:
                'datetime_picker_options' => [
                    'format' => 'DD/MM/YYYY',
                    'language' => 'es'
                ],
                'default' => Carbon::now()->format('Y-m-d'),
                'wrapper_child_id' => 'due_date',
                'wrapperAttributes'=>['class'=>'form-group col-md-3', 'id'=>'start_date']
            ],
            [
                'name' => 'billing_days_to_pay',
                'label' => 'Billing days',
                'type' => 'text',
                'wrapperAttributes'=>['class'=>'form-group col-md-3'],
                'attributes' => ['disabled' => 'disabled']
            ],
            
            [   // date_picker
                'name' => 'due_date',
                'type' => 'date_picker_custom',
                'label' => 'Due Date',
                'datetime_picker_options' => [
                    'format' => 'DD/MM/YYYY',
                    'language' => 'es'
                ],
                'wrapperAttributes'=>['class'=>'form-group col-md-3']
            ],
            [ // select_from_array
                'name' => 'status',
                'label' => "Status",
                'type' => 'disabled_select_from_array',
                'options' => [
                    'not_paid' => 'Not Paid', 'partially_paid' => 'Partially Paid', 'paid' => 'Paid',
                    'canceled' => 'Canceled', 'invalidated' => 'Invalidated',
                ],
                'allows_null' => false,
                'wrapperAttributes'=>['class'=>'form-group col-md-6', 'disabled' => 'disabled']
            ],
            [
                'name' => 'note',
                'label' => 'Notes',
                'type' => 'bill_textarea',
                'wrapperAttributes'=>['class'=>'form-group col-md-6']
            ],
    
            [
                'name' => 'bill_link',
                'label' => 'Bill Link',
                'type' => 'bill_url',
                'wrapperAttributes'=>['class'=>'form-group col-md-6'],
            ],
            [
                'name' => 'accounting_note',
                'label' => 'Notes',
                'type' => 'bill_accounting_services',
                'wrapperAttributes'=>['class'=>'form-group col-md-6']
            ],
            [
                'name' => 'billing_cancel',
                'label' => 'Notes',
                'type' => 'bill_cancel_button',
                'wrapperAttributes'=>[]
            ],

        ];
    }
    public function getColumnsFields()
    {
        
        return [
            ['name' => 'bill_number', 'label' => 'Number', 'type' => 'link_show', 'link' => url('admin\bills')],
            [
                'name' => 'billing_company_id',
                'label' => 'Company',
                'entity' => 'billingCompany',
                'type' => "select",
                'attribute' => 'name',
                'model' => 'App\Models\Company'
            ],
            [
                'name' => 'amount', // The db column name
                'label' => "Amount", // Table column heading
                'type' => "number",
                // 'prefix' => "$",
                // 'suffix' => " EUR",
                // 'decimals' => 2,
            ],
            [
                'name' => "date", // The db column name
                'label' => "Bill Date", // Table column heading
                'type' => "date"
            ],
            [
                'name' => "due_date", // The db column name
                'label' => "Due Date", // Table column heading
                'type' => "date"
            ],
            [
                'name' => 'operation_id',
                'label' => 'Operation',
                'entity' => 'operation',
                'type' => "select_with_link_edit",
                'route' => 'crud.billing.edit',
                'attribute' => 'code',
                'model' => 'App\Models\Operation'
            ],
            [
                'name' => "status", // The db column name
                'label' => "Status", // Table column heading
                'type' => "text"
            ],
            
        ];
    }




}