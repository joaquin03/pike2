<?php

namespace App\Http\Fields;

class CompanyTagFields extends AbstractFields
{
    public function getFormFields()
    {
        return [
            [
                'name' => 'name',
                'label' => 'Name',
                'type' => 'text',
            ],
            [
                'name' => 'description',
                'label' => 'Description',
                'type' => 'textarea',
            ],
           
        ];
    }
    
    public function getColumnsFields()
    {
        return [
            ['name' => 'name', 'label' => 'Name', 'type' => 'link_edit', 'link' => url('admin/company-tag')],
            [
                'name' => 'description',
                'label' => 'Description',
                'type' => 'textarea',
            ]
        ];
    }
    
}