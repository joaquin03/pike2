<?php
namespace App\Http\Fields;

class ActivityReminderFields extends AbstractFields
{
    public function getFormFields()
    {
        return [
            [   // CustomHTML
                'name' => 'separator1',
                'type' => 'custom_html',
                'value' => '<h3 class="box-title with-border">Reminder Information</h3>'
            ],
            ['name' => 'title', 'label' => 'Title', 'wrapperAttributes'=>['class'=>'form-group col-md-6']],
            [   // DateTime
                'name' => 'due_date',
                'label' => 'Due Date',
                'type' => 'datetime_picker',
                // optional:
                'datetime_picker_options' => [
                    'format' => 'DD/MM/YYYY HH:mm',
                    'language' => 'es'
                ],'wrapperAttributes'=>['class'=>'form-group col-md-4']
            ],
            [
                'name' => 'visibility', // The db column name
                'label' => "Is Finished", // Table column heading
                'type' => 'checkbox'
                ,'wrapperAttributes'=>['class'=>'form-group col-md-2']
            ],
            ['name' => 'description', 'label' => 'Description', 'type'=>'textarea'],
            [
                'name' => 'assign_to_user_id',
                'label' => "Assigned to",
                'type' => "select2",
                'entity' => 'users',
                'attribute' => "name",
                'model' => 'App\Models\User',
                'wrapperAttributes'=>['class'=>'form-group col-md-6']
            ],
            //TODO: CONTACTS
            [   // CustomHTML
                'name' => 'separator2',
                'type' => 'custom_html',
                'value' => '<h3 class="box-title with-border">Activity Information</h3>'
            ],
            [
                'label' => "Contacts",
                'type' => "activity_contact",
                'name' => 'contacts',
                'entity' => 'contacts',
                'model' => 'App\Models\Contact',
                'attribute' => "name",
                'pivot' => true,

            ],

        ];
    }

    public function getColumnsFields()
    {
        return [
            ['name' => 'title', 'label' => 'Title'],
            ['name' => 'body', 'label' => 'Body', 'type'=>'textarea'],
        ];
    }
    
}