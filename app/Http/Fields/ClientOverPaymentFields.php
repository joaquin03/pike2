<?php
namespace App\Http\Fields;

use App\Models\Accounting\Bill;
use Carbon\Carbon;

class ClientOverPaymentFields extends ClientPaymentFields
{
    public function getFormFields()
    {
        return [
            [ // select_from_array
                'name' => 'is_over_payment',
                'type'  => 'hidden',
                'value' => '1'
            ],
            [
                'name' => 'payment_number',
                'label' => 'Payment Number',
                'type' => 'text',
                'attributes' => ["required" => "required"],
                'wrapperAttributes'=>['class'=>'form-group col-md-6']
            ],
            [
                'name' => 'amount',
                'label' => 'Amount',
                'type' => 'number',
                'prefix' => 'USD',
                'attributes' => ["step" => "any"],
                'wrapperAttributes'=>['class'=>'form-group col-md-6']
            ],
            [ // select_from_array
                'name' => 'payment_method',
                'label' => "Payment Method",
                'type' => 'select_from_array',
                'options' => ['wire transfer' => 'Wire Transfer', 'credit card' => 'Credit Card', 'cash' => 'Cash']
            ],
            [   // date_picker
                'name' => 'date',
                'type' => 'datetime_picker',
                'label' => 'Payment Date',
                // optional:
                'datetime_picker_options' => [
                    'format' => 'DD/MM/YYYY',
                    'language' => 'es'
                ],
                'default' => Carbon::now(),
                'attributes' => ["required" => "required"],
                'wrapperAttributes'=>['class'=>'form-group col-md-6']
            ],
            
            [   // Upload
                'name' => 'attachment',
                'label' => 'Attached Files',
                'type' => 'upload_multiple',
                'upload' => true,
                'disk' => 'local', // if you store files in the /public folder, please ommit this; if you store them in /storage or S3, please specify it;
                'wrapperAttributes'=>['class'=>'form-group col-md-6']
            ],
            [
                'name' => 'note',
                'label' => 'Notes',
                'type' => 'textarea',
            ],
            [
                'name' => 'bills',
                'type' => 'bill_over_payments_info',
                'entity' => 'bills', // the method that defines the relationship in your Model
                'attribute' => 'bill_number', // foreign key attribute that is shown to user
                'model' => 'App\Models\Accounting\Bill', // foreign key model
                'pivot' => true, // on create&update, do you need to add/delete pivot table entries?
                'wrapperAttributes'=>['class'=>'form-group col-md-6'],
                'options' => ['type' => 'payment']
            ]

        ];
    }

    
}