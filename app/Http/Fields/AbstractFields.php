<?php
namespace App\Http\Fields;
/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 8/1/17
 * Time: 4:43 PM
 */
abstract class AbstractFields
{
    
    abstract public function getFormFields();
    
    
}