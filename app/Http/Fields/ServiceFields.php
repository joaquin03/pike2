<?php

namespace App\Http\Fields;

class ServiceFields extends AbstractFields
{
    public function getFormFields()
    {
        return [
            [
                'name' => 'name',
                'label' => 'Name',
                'type' => 'text',
            ],
            [
                'name' => 'description',
                'label' => 'Description',
                'type' => 'textarea',
            ],
            [       // Select2Multiple = n-n relationship (with pivot table)
                'label' => "Providers",
                'type' => 'select2_multiple',
                'name' => 'providers', // the method that defines the relationship in your Model
                'entity' => 'providers', // the method that defines the relationship in your Model
                'attribute' => 'name', // foreign key attribute that is shown to user
                'model' => 'App\Models\Provider', // foreign key model
                'pivot' => true, // on create&update, do you need to add/delete pivot table entries?,
            ],
            
        ];
    }
    
    public function getColumnsFields($type='ServiceParent')
    {
        return [
            ['name' => 'name', 'label' => 'Name', 'type' => 'link_edit', 'link' => url('admin/'.$type)],
            [
                'name' => 'description',
                'label' => 'Description',
                'type' => 'textarea',
            ],
          
        ];
    }
    
}