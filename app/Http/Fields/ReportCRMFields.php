<?php
namespace App\Http\Fields;

class ReportCRMFields extends AbstractFields
{
    
    public function getFormFields()
    {
       return  [];
    }
    
    public function getColumnsFields()
    {
        return [
            [
                'name' => 'user_id',
                'label' => 'User',
                'entity' => 'user',
                'type' => "select",
                'attribute' => 'name',
                'model' => 'App\Models\User'
            ],
            ['name' => 'event', 'type'=> 'text'],
            [
                'name' =>'auditable_type',
                'label'=> 'Element',
                'type' => 'model_function', 'function_name' =>'getElementString'
            ],
            [
                'name' =>'info',
                'label'=> 'Info',
                'type' => "model_function", 'function_name' => 'getElementValue',
            ],
            [
                'name' =>'created_at',
                'label'=> 'Date',
                'type' => "datetime",
                'format' => 'l j F Y H:i'
            ],
        ];
    }
    
}