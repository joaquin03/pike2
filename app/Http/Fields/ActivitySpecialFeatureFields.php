<?php
namespace App\Http\Fields;

class ActivitySpecialFeatureFields extends AbstractFields
{
    public function getFormFields()
    {
        return [
            [   // CustomHTML
                'name' => 'separator1',
                'type' => 'custom_html',
                'value' => '<h3 class="box-title with-border">Special Feature Information</h3>'
            ],
            ['name' => 'name', 'label' => 'Name',  'wrapperAttributes'=>['class'=>'form-group col-md-4']],
            [
                'name' => 'user_id',
                'label' => "User",
                'type' => "select2",
                'entity' => 'users',
                'attribute' => "name",
                'model' => 'App\Models\User',
                'wrapperAttributes'=>['class'=>'form-group col-md-4']
            ],
            ['name' => 'notes', 'label' => 'Notes', 'type'=>'high-textarea'],
            [
                'label' => "Contacts",
                'type' => "activity_contact",
                'name' => 'contacts',
                'entity' => 'contacts',
                'model' => 'App\Models\Contact',
                'attribute' => "name",
                'pivot' => true,

            ],


        ];
    }

    public function getColumnsFields()
    {
        return [
        ];
    }
    
}