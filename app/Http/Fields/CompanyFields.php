<?php
namespace App\Http\Fields;

use App\Models\Company;
use App\Models\Utilities\Country;
use League\Flysystem\Config;

/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 8/1/17
 * Time: 4:46 PM
 */
class CompanyFields extends AbstractFields
{
    public function getFormFields()
    {
        return array_merge(
            $this->basicFields(),
            $this->advanceGeneral(), $this->advanceProcurement(), $this->advanceBilling(), $this->advanceQuotation());

    }

    private function basicFields()
    {
        return [
            [
                'name' => 'is_client',
                'label'=> 'Client',
                'type' => 'checkbox',
                'wrapperAttributes'=>['class'=>'form-group col-md-4', 'style'=>'text-align: center'],
                'tab' => 'Basic'
            ],
            [
                'name' => 'is_operator',
                'label'=> 'Operator',
                'type' => 'checkbox',
                'wrapperAttributes'=>['class'=>'form-group col-md-4', 'style'=>'text-align: center'],
                'tab' => 'Basic'
            ],
            [
                'name' => 'is_provider',
                'label'=> 'Provider',
                'type' => 'checkbox',
                'wrapperAttributes'=>['class'=>'form-group col-md-4', 'style'=>'text-align: center;'],
                'tab' => 'Basic'
            ],
            [
                'name' => 'name',
                'label' => 'Name *',
                'type' => 'text',
                'wrapperAttributes'=> ['class'=>'form-group col-md-6'],
                'tab' => 'Basic'
            ],
            [
                'name' => 'website',
                'type' => 'text',
                'wrapperAttributes'=> ['class'=>'form-group col-md-6'],
                'tab' => 'Basic'
            ],
            [
                'label' => 'Country *',
                'name' => 'country',
                'type' => 'select2_from_array',
                'options' => Country::all(),
                'allows_null' => false,
                'wrapperAttributes'=> ['class'=>'form-group col-md-4'],
                'tab' => 'Basic'
            ],
            [
                'label' => 'Company Branch',
                'name' => 'branch_company_id',
                'type' => 'select2_from_array',
                'options' => config('constants.branch_companies'),
                'allows_null' => false,
                'wrapperAttributes'=>['class'=>'form-group col-md-4'],
                'tab' => 'Basic'
            ],
            [   // Upload
                'name' => 'image',
                'type' => 'upload',
                'upload' => true,
                'wrapperAttributes'=>['class'=>'form-group col-md-4'],
                'disk' => 'public',
                'tab' => 'Basic'
            ],

            [
                'name' => 'address_receptor',
                'label' => 'Main Address (Billing)',
                'type' => 'text',
                'wrapperAttributes'=> ['class'=>'form-group col-md-4'],
                'tab' => 'Basic'
            ],
            [
                'name' => 'zip_code',
                'label' => 'Main Zip Code (Billing)',
                'type' => 'text',
                'wrapperAttributes'=> ['class'=>'form-group col-md-4'],
                'tab' => 'Basic'
            ],
            [
                'name' => 'city',
                'label' => 'Main City (Billing)',
                'type' => 'text',
                'wrapperAttributes'=> ['class'=>'form-group col-md-4'],
                'tab' => 'Basic'
            ],


            [   // CustomHTML
                'name' => 'basic_separator',
                'type' => 'custom_html',
                'value' => '<hr class="col-md-12">',
                'tab' => 'Basic',
            ],

            [ // Table
                'name' => 'emails',
                'label' => 'Emails',
                'type' => 'company_table',
                'entity_singular' => 'Email',
                'columns' => [
                    'comment' => 'Comment',
                    'email' => 'Email',
                ],
                'options' => ['Labor email', 'Personal email'],
                'min' => 0,
                'store_as_json' => true,
                'wrapperAttributes'=>['class'=>'form-group col-md-6'],
                'tab' => 'Basic',
            ],
            [ // Table
                'name' => 'phones',
                'label' => 'Phones',
                'type' => 'company_table',
                'entity_singular' => 'phone',
                'columns' => [
                    'comment' => 'Comment',
                    'phone' => 'Phone',
                ],
                'options' => ['Main Office', 'Personal', 'Cell', 'Fax'],
                'min' => 0,
                'store_as_json' => true,
                'wrapperAttributes'=>['class'=>'form-group col-md-6'],
                'tab' => 'Basic'
            ],
            [ // Table
                'name' => 'address',
                'label' => 'Address',
                'type' => 'company_address_table',
                'entity_singular' => 'address',
                'columns' => [
                    'address' => 'Address',
                    'city' => 'City',
                    'zip_code' => 'Zip code',
                    'country' => 'Country',
                ],
                'min' => 0,
                'store_as_json' => true,
                'tab' => 'Basic'
            ],
        ];
    }

    private function advanceGeneral()
    {
        return [

            [
                'name' => 'has_special_features',
                'label'=> 'Has Special Features',
                'type' => 'checkbox',
                'wrapperAttributes'=>['class'=>'form-group col-md-4'],
                'tab' => 'Advance'
            ],
            [
                'name' => 'is_ww_provider',
                'label'=> 'Is WW Provider ',
                'type' => 'checkbox',
                'wrapperAttributes'=>['class'=>'form-group col-md-4'],
                'tab' => 'Advance'
            ],
            [
                'name' => 'is_country_entity',
                'label'=> 'Is Permit Entity ',
                'type' => 'checkbox',
                'wrapperAttributes'=>['class'=>'form-group col-md-4'],
                'tab' => 'Advance'
            ],

            [ // select_from_array
                'label' => 'Type Of Client',
                'name' => 'type_of_client',
                'type' => 'select2_from_array',
                'options' => Company::$typeOfClient,
                'allows_null' => false,
                'wrapperAttributes'=>['class'=>'form-group col-md-4'],
                'tab' => 'Advance'
            ],
            [
                'label' => "Tags",
                'type' => 'select2_multiple',
                'name' => 'tags',
                'entity' => 'tags',
                'attribute' => 'name',
                'model' => "App\Models\CompanyTag",
                'wrapperAttributes'=>['class'=>'form-group col-md-4'],
                'pivot' => true,
                'tab' => 'Advance'
            ],
            [
                'name' => 'business',
                'type' => 'text',
                'wrapperAttributes'=>['class'=>'form-group col-md-4'],
                'tab' => 'Advance'
            ],
            [   // SelectMultiple = n-n relationship (with pivot table)
                'label' => "Airports",
                'type' => 'select2_multiple',
                'name' => 'airports', // the method that defines the relationship in your Model
                'entity' => 'airports', // the method that defines the relationship in your Model
                'attribute' => 'icao', // foreign key attribute that is shown to user
                'model' => "App\Models\Airport", // foreign key model
                'pivot' => true,
                'tab' => 'Advance',
                'wrapperAttributes'=>['class'=>'form-group col-md-12'],
            ],
        ];
    }

    private function advanceProcurement()
    {
        return [
            [   // CustomHTML
                'name' => 'h3_Procurement',
                'type' => 'custom_html',
                'value' => '<hr class="col-md-12"><h3 class="row col-md-12">Procurement</h3>',
                'tab' => 'Advance',
            ],

            [
                'name' => 'invoice_days_receptor',
                'label' => 'Days to pay',
                'type' => 'number',
                'wrapperAttributes'=>['class'=>'form-group col-md-4'],
                'tab' => 'Advance'
            ],
            [
                'name' => 'payment_methods',
                'label' => "Payment Methods",
                'type' => 'select2_from_array',
                'options' => Company::$paymentMethods,
                'allows_null' => true,
                'allows_multiple' => true,
                'wrapperAttributes'=>['class'=>'form-group col-md-4'],
                'tab' => 'Advance'
            ],
            [
                'name' => 'adm_fee',
                'label' => 'Administration Fee %',
                'type' => 'text',
                'wrapperAttributes'=>['class'=>'form-group col-md-4'],
                'tab' => 'Advance'
            ],
            [
                'name' => 'procurement_bank_info',
                'label' => 'Bank Info',
                'type' => 'textarea',
                'wrapperAttributes'=>['class'=>'form-group col-md-12'],
                'tab' => 'Advance'
            ],

        ];
    }

    private function advanceBilling()
    {
        return [
            [   // CustomHTML
                'name' => 'h3_Billing',
                'type' => 'custom_html',
                'value' => '<hr class="col-md-12"><h3 class="row col-md-12">Billing</h3>',
                'tab' => 'Advance',
            ],

            [ // select_from_array
                'name' => 'rut_cuit_vat',
                'label' => "Tax id",
                'type' => 'select2_from_array',
                'options' => Company::$taxId,
                'allows_null' => true,
                'wrapperAttributes'=>['class'=>'form-group col-md-4'],
                'tab' => 'Advance'
            ],
            [
                'name' => 'rut_cuit_vat_description',
                'label' => 'Value',
                'type' => 'text',
                'wrapperAttributes'=>['class'=>'form-group col-md-4'],
                'tab' => 'Advance'
            ],

            [
                'name' => 'billing_days_receptor',
                'label' => 'Days to pay',
                'type' => 'number',
                'wrapperAttributes'=>['class'=>'form-group col-md-4'],
                'tab' => 'Advance'
            ],
            [
                'name' => 'billing_adm_fee',
                'label' => 'Administration Fee %',
                'type' => 'number',
                'wrapperAttributes'=>['class'=>'form-group col-md-4'],
                'tab' => 'Advance'
            ],

            [   // CustomHTML
                'name' => 'separator_billing',
                'type' => 'custom_html',
                'value' => '<hr class="col-md-12">',
                'tab' => 'Advance',
            ],
        ];
    }


    private function advanceQuotation()
    {
        return [
            [   // CustomHTML
                'name' => 'h3_Quotation',
                'type' => 'custom_html',
                'value' => '<hr class="col-md-12"><h3 class="row col-md-12">Quotation</h3>',
                'tab' => 'Advance',
            ],
            [   // CustomHTML
                'label' => "Sales Manager",
                'name' => 'sales_manager',
                'entity' => 'salesManager',
                'type' => 'sales_manager_select',
                'model' => "App\Models\SalesManager",
                'wrapperAttributes'=>['class'=>'form-group col-md-4'],
                'tab' => 'Advance',
            ],
            [   // CustomHTML
                'label' => "Fuel Client Type",
                'name' => 'US_fuel_client_type',
                'type' => 'select_from_array',
                'allows_null' => true,
                'options' => ['client_a'=>"Client A", 'client_b' => "Client B", 'client_c' => "Client C"],
                'wrapperAttributes'=>['class'=>'form-group col-md-4'],
                'tab' => 'Advance',
            ],
            
            [   // CustomHTML
                'name' => 'separator_quotation',
                'type' => 'custom_html',
                'value' => '<hr class="col-md-12">',
                'tab' => 'Advance',
            ],
        
        ];
    }


    public function getColumnsFields()
    {
        return [
            ['name' => 'name', 'type'=> 'link_item', 'link'=>url('admin/company/')],
            ['name' => 'country', 'type' => 'model_function', 'function_name' =>'getCountry'],
            ['name' => 'business'],
            ['label' => "Tags", 'type' => "select_multiple", 'name' => 'tags', 'entity' => 'tags',
                'attribute' => "name", 'model' => "App\Models\CompanyTag",
            ],
            ['name'=>'updated_at', 'label'=>'Latest activity', 'type' => 'model_function', 'function_name' =>'lastActivityDate'],
        ];
    }
}
