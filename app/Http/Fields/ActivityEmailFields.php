<?php
namespace App\Http\Fields;
/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 8/1/17
 * Time: 4:46 PM
 */
class ActivityEmailFields extends AbstractFields
{
    public function getFormFields()
    {
        return [
            ['name' => 'from', 'type' => 'email', 'wrapperAttributes'=>['class'=>'form-group col-md-6']],
            ['name' => 'to', 'type' => 'email', 'wrapperAttributes'=>['class'=>'form-group col-md-6']],
            ['name' => 'cc', 'label'=> 'CC', 'type' => 'email', 'wrapperAttributes'=>['class'=>'form-group col-md-6']],
            ['name' => 'cco', 'label'=> 'CCO', 'type' => 'email' ,'wrapperAttributes'=>['class'=>'form-group col-md-6']],
            ['name' => 'date', 'type'=>'date_picker', 'wrapperAttributes'=>['class'=>'form-group col-md-6']],
            [
                'name' => 'proposal',
                'type' => 'select2_from_array',
                'options'=> ['waiting_response'=>'Waiting Response','accepted'=>'Accepted', 'declined'=>'Declined'],
                'wrapperAttributes'=>['class'=>'form-group col-md-6'],
            ],
            ['name' => 'subject', 'type' => 'text'],
            ['name' => 'body', 'type' => 'textarea'],
            [
                'label' => "Contacts",
                'type' => "activity_contact",
                'name' => 'contacts',
                'entity' => 'contacts',
                'model' => 'App\Models\Contact',
                'attribute' => "name",
                'pivot' => true,

            ],
        ];
    }
    
    
    public function getColumnsFields()
    {
        return [
            'from', 'to', 'cc', 'cco',
            [
                'name' => 'proposal',
                'type' => 'select2_from_array',
                'options'=> ['waiting_response'=>'Waiting Response','accepted'=>'Accepted', 'declined'=>'Declined']
            ],'subject'
        ];
    }
    
}