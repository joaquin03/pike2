<?php
namespace App\Http\Fields;

use App\Models\Company;
use App\Models\Utilities\Country;

/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 8/1/17
 * Time: 4:46 PM
 */
class CompanyBillingInfoFields extends AbstractFields
{
    public function getFormFields()
    {
        return [
            [
                'name' =>   'name',
                'label' => 'Name',
                'type' => 'text',
                'attributes' => ['disabled' => 'disabled'],
                'wrapperAttributes'=>['class'=>'form-group col-md-6']
            ],
            [
                'name' =>   'document_receptor',
                'label' => 'Document',
                'type' => 'text',
                'wrapperAttributes'=>['class'=>'form-group col-md-6']
            ],
            [   // Select
                'name' => 'doc_type_receptor',
                'label' => "Document Type",
                'type' => 'select_from_array',
                'options' => ['4' => 'Otros', '2' => 'RUC ', '3' => 'C.I' , '5' => 'Pasaporte',
                    '6' => 'DNI(Arg, Br, Ch, Par)', '7' => 'NIFE'], // the method that defines the relationship in your Model
                'allows_null' => false,
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
            ],[ // select_from_array
                'name' => 'country',
                'type' => 'select2_from_array',
                'options' => Country::all(),
                'allows_null' => false,
                'wrapperAttributes'=>['class'=>'form-group col-md-6']
                // 'allows_multiple' => true, // OPTIONAL; needs you to cast this to array in your model;
            ],[
                'name' =>   'business_name_receptor',
                'label' => 'Business Name',
                'type' => 'text',
                'wrapperAttributes'=>['class'=>'form-group col-md-6']
            ],[
                'name' => 'address_receptor',
                'label' => 'Address',
                'type' => 'text',
                'wrapperAttributes'=>['class'=>'form-group col-md-6']
            ],
            [
                'name' => 'city_receptor',
                'label' => 'City',
                'type' => 'text',
                'wrapperAttributes'=>['class'=>'form-group col-md-6']
            ],[
                'name' => 'state_receptor',
                'label' => 'State',
                'type' => 'text',
                'wrapperAttributes'=>['class'=>'form-group col-md-6']
            ],[
                'name' => 'postal_code_receptor',
                'label' => 'Postal Code',
                'type' => 'text',
                'wrapperAttributes'=>['class'=>'form-group col-md-6']
            ],
            [
                'name' => 'billing_days_receptor',
                'label' => 'Days to pay',
                'type' => 'number',
                'wrapperAttributes'=>['class'=>'form-group col-md-6']
            ],
            [
                'name' => 'billing_email',
                'label' => 'Email',
                'type' => 'text',
                'wrapperAttributes'=>['class'=>'form-group col-md-6']
    
            ],

        ];
    }
    
    public function getColumnsFields()
    {
        return [
            ['name' => 'name', 'type'=> 'link_edit', 'link'=>url('admin/company-billing-info/')],
            ['name' => 'country', 'type' => 'model_function', 'function_name' =>'getCountry'],
            ['name' => 'business'],
            ['name' => 'billing_email', 'label'=>'Email'],
            [
                'name' => 'doc_type_receptor', 'label' => "Doc. Type", 'type' => 'select_from_array',
                'options' => ['4' => 'Otros', '2' => 'RUC ', '3' => 'C.I' , '5' => 'Pasaporte',
                    '6' => 'DNI(Arg, Br, Ch, Par)', '7' => 'NIFE'], // the method that defines the relationship in your Model]
            ],
            ['name' =>   'document_receptor', 'label' => 'Document', 'type'=> 'text'],
            ['name' => 'address_receptor', 'label' => 'Address', 'type'=> 'text'],
            
        ];
    }
}