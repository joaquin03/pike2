<?php
namespace App\Http\Fields;

class ActivityFileFields extends AbstractFields
{
    public function getFormFields()
    {
        return [
            [   // CustomHTML
                'name' => 'separator1',
                'type' => 'custom_html',
                'value' => '<h3 class="box-title with-border">File Information</h3>'
            ],
            ['name' => 'name', 'label' => 'Name',  'wrapperAttributes'=>['class'=>'form-group col-md-4']],
            [
                'name' => 'user_id',
                'label' => "User",
                'type' => "select2",
                'entity' => 'users',
                'attribute' => "name",
                'model' => 'App\Models\User',
                'wrapperAttributes'=>['class'=>'form-group col-md-4']
            ],
            [   // Upload
                
                'name' => 'files',
                'label' => 'File',
                'type' => 'upload_multiple',
                'upload' => true,
                'disk' => 'public', // if you store files in the /public folder, please ommit this; if you store them in /storage or S3, please specify it;
                'wrapperAttributes'=>['class'=>'form-group col-md-4']
            ],
            ['name' => 'notes', 'label' => 'Notes', 'type'=>'textarea'],
            [
                'label' => "Contacts",
                'type' => "activity_contact",
                'name' => 'contacts',
                'entity' => 'contacts',
                'model' => 'App\Models\Contact',
                'attribute' => "name",
                'pivot' => true,

            ],


        ];
    }

    public function getColumnsFields()
    {
        return [
        ];
    }
    
}