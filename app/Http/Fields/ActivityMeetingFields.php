<?php

namespace App\Http\Fields;

/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 8/1/17
 * Time: 4:46 PM
 */
class ActivityMeetingFields extends AbstractFields
{
    public function getFormFields()
    {
        return [
            ['name' => 'name', 'type' => 'text', 'wrapperAttributes' => ['class' => 'form-group col-md-6']],
            ['name' => 'location', 'type' => 'text', 'wrapperAttributes' => ['class' => 'form-group col-md-6']],
            [
                'name' => 'start_date',
                'label' => 'Start',
                'type' => 'datetime_picker',
                'wrapperAttributes' => ['class' => 'form-group col-md-6']
            ],
            [
                'name' => 'end_date',
                'label' => 'End',
                'type' => 'datetime_picker',
                'wrapperAttributes' => ['class' => 'form-group col-md-6']
            ],
            ['name' => 'summary', 'type' => 'textarea'],
            [
                'label' => "Contacts",
                'type' => "activity_contact",
                'name' => 'contacts',
                'entity' => 'contacts',
                'model' => 'App\Models\Contact',
                'attribute' => "name",
                'pivot' => true
            ],
        
        
        ];
    }
    
    public function getColumnsFields()
    {
        return [
            'name',
            'location',
            'start_date',
            'end_date',
            
        ];
    }
    
}