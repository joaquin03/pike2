<?php
namespace App\Http\Fields;

class ProcurementFields extends AbstractFields
{
    
    protected $class = 'procurement';
    
    public function getFormFields()
    {
        return [];
    }

    public function getColumnsFields()
    {
        return [
            ['name' => 'created_at', 'label'=>'Date', 'type'=>'model_function','function_name'=> 'getCreatedAt'],
            ['name' => 'code', 'label' => 'ID', 'type' => 'link_edit', 'link' => url('admin/'.$this->class.'/')],
            ['name' => 'in_charge_user_id', 'label'=>'In Charge', 'entity'=>'user', 'type' => "select",
                'attribute'=>'name','model' => 'App\Models\User' ],
                ['name' => 'aircraft_id', 'label'=>'Aircraft', 'entity'=>'aircraft', 'type' => "select",
                'attribute'=>'registration','model' => 'App\Models\Aircraft' ],
            ['name' => 'aircraft_operator_id', 'label'=>'A. Operator', 'entity'=>'aircraftOperator', 'type' => "select",
                'attribute'=>'name','model' => 'App\Models\Company' ],
            ['name' => 'aircraft_client_id', 'label'=>'A. Client', 'entity'=>'aircraftClient', 'type' => "select",
                'attribute'=>'name','model' => 'App\Models\Company' ],
            [
                'name' => 'state',
                'label' => "Status",
                'type' => "operations_status",
                'function_name' => 'getStatusAttribute'
            ],
        ];
    }

}