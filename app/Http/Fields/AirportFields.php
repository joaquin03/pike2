<?php

namespace App\Http\Fields;

use App\Models\Utilities\Country;

class AirportFields extends AbstractFields
{
    public function getFormFields()
    {
        return [
            [   // CustomHTML
                'name' => 'separator1',
                'type' => 'custom_html',
                'value' => '<h3 class="box-title with-border">General</h3>'
            ],
            [
                'name' => 'icao',
                'label' => 'Icao',
                'type' => 'text',
                'wrapperAttributes' => ['class' => 'form-group col-md-2'],
            ],
            [
                'name' => 'iata',
                'label' => 'Iata',
                'type' => 'text',
                'wrapperAttributes' => ['class' => 'form-group col-md-2'],
            ],
            [
                'name' => 'name',
                'label' => 'Name',
                'type' => 'text',
                'wrapperAttributes' => ['class' => 'form-group col-md-4'],
            ],
            [ // select_from_array
                'name' => 'country',
                'type' => 'select2_from_array',
                'options' => Country::all(),
                'allows_null' => false,
                'wrapperAttributes' => ['class' => 'form-group col-md-2'],
            
            ],
            [
                'name' => 'descriptions',
                'label' => 'Descriptions',
                'type' => 'select2_from_array',
                'options' => [
                    "intl" => "INTL",
                    "dom" => "DOM",
                ],
                'allows_null' => true,
                'allows_multiple' => false,
                'wrapperAttributes' => ['class' => 'form-group col-md-2']
            ],
            [   // CustomHTML
                'name' => 'separator2',
                'type' => 'custom_html',
                'value' => '<h3 class="box-title with-border">...</h3>'
            ],
            [
                'name' => 'OPR_HS_H24',
                'label' => 'OPR HS (H24)',
                'type' => 'select2_from_array',
                'options' => [
                    "0" => "0",
                    "H24" => "H24",
                    "CLOSED**" => "CLOSED**",
                ],
                'allows_null' => true,
                'allows_multiple' => false,
                'wrapperAttributes' => ['class' => 'form-group col-md-3']
            ]
            ,[   // CustomHTML
                'name' => 'separator5',
                'type' => 'custom_html',
                'value' => '<div style="margin-top: 30px;font-weight: bold" >OPR HS</div>',
                'wrapperAttributes' => ['class' => 'form-group col-lg-1'],
            ],
            [
                'name' => 'OPR_HS_FROM',
                'label' => ' ',
                'type' => 'text',
                'wrapperAttributes' => ['class' => 'form-group col-md-2', 'style'=>'margin-top:5px'],
            ],
            [   // CustomHTML
                'name' => 'separator6',
                'type' => 'custom_html',
                'value' => '<div style="margin: 30px; font-weight: bold" >to</div>',
                'wrapperAttributes' => ['class' => 'form-group col-md-1'],
            ],
            [
                'name' => 'OPR_HS_TO',
                'label' => ' ',
                'type' => 'text',
                'wrapperAttributes' => ['class' => 'form-group col-md-2', 'style'=>'margin-top:5px'],
            ],
            [
                'name' => 'H24_OR',
                'label' => 'H24 (OR)',
                'type' => 'text',
                'wrapperAttributes' => ['class' => 'form-group col-md-2'],
            ],
            [   // CustomHTML
                'name' => 'separator3',
                'type' => 'custom_html',
                'value' => '<h3 class="box-title with-border">...</h3>'
            ],
            [
                'name' => 'RWY1',
                'label' => 'RWY1',
                'type' => 'text',
                'wrapperAttributes' => ['class' => 'form-group col-md-3'],
            ],
            [
                'name' => 'lengthwidth1ft',
                'label' => 'length/width1(ft)',
                'type' => 'text',
                'wrapperAttributes' => ['class' => 'form-group col-md-3'],
            ],
            [
                'name' => 'lengthwidth1m',
                'label' => 'length/width1(m)',
                'type' => 'text',
                'wrapperAttributes' => ['class' => 'form-group col-md-3'],
            ],
            [
                'name' => 'PCN1',
                'label' => 'PCN1',
                'type' => 'text',
                'wrapperAttributes' => ['class' => 'form-group col-md-3'],
            ],
            [   // CustomHTML
                'name' => 'separator4',
                'type' => 'custom_html',
                'value' => '<h3 class="box-title with-border">...</h3>'
            ],
            [
                'name' => 'RWY2',
                'label' => 'RWY2',
                'type' => 'text',
                'wrapperAttributes' => ['class' => 'form-group col-md-3'],
            ],
            [
                'name' => 'lengthwidth2ft',
                'label' => 'length/width2(ft)',
                'type' => 'text',
                'wrapperAttributes' => ['class' => 'form-group col-md-3'],
            ],
            [
                'name' => 'lengthwidth2m',
                'label' => 'length/width2(m)',
                'type' => 'text',
                'wrapperAttributes' => ['class' => 'form-group col-md-3'],
            ],
            [
                'name' => 'PCN2',
                'label' => 'PCN2',
                'type' => 'text',
                'wrapperAttributes' => ['class' => 'form-group col-md-3'],
            ],
        ];
    }
    
    public function getColumnsFields()
    {
        return [
            ['name' => 'icao', 'label' => 'Icao', 'type' => 'link_icao', 'link' => url('admin/airport/')],
            'name',
            ['name' => 'descriptions', 'label' => 'Description'],
            ['name' => 'country', 'type' => 'model_function', 'function_name' => 'getCountry']
        ];
    }
    
}