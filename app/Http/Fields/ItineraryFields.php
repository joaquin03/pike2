<?php
namespace App\Http\Fields;

class ItineraryFields extends AbstractFields
{
    public function getFormFields()
    {
        return [
            [
                'name' => 'airport_icao_string',
                'label' => "Airport",
                'type' => "select2",
                'entity' => 'airport',
                'attribute' => "icao",
                'model' => 'App\Models\Airport',
                'wrapperAttributes'=>['class'=>'form-group col-md-6']
            ], [
                'name' => 'type',
                'type' => 'select2_from_array',
                'options' => ['Arrive'=>'Arrive', 'Departure'=>'Departure'],
                'allows_null' => false,
                'wrapperAttributes'=>['class'=>'form-group col-md-6']
            ],[   // DateTime
                'name' => 'estimated',
                'label' => 'Estimated',
                'type' => 'datetime_picker',
                'wrapperAttributes'=>['class'=>'form-group col-md-6']
            ],[   // DateTime
                'name' => 'actual',
                'label' => 'Actual',
                'type' => 'datetime_picker',
                'wrapperAttributes'=>['class'=>'form-group col-md-6']
            ], [
                'name' => 'operator_id',
                'label' => "Handler",
                'type' => "select2",
                'entity' => 'handler',
                'attribute' => "name",
                'model' => 'App\Models\Company',
                'wrapperAttributes'=>['class'=>'form-group col-md-6']
            ], [
                'name' => 'fuel_provider_id',
                'label' => "Fuel Provider",
                'type' => "select2",
                'entity' => 'fuelProvider',
                'attribute' => "name",
                'model' => 'App\Models\Company',
                'wrapperAttributes'=>['class'=>'form-group col-md-6']
            ], [
                'name'=>'fuel_gallons',
                'label'=> 'Fuel Gallons',
                'type' => 'number',
                'wrapperAttributes'=>['class'=>'form-group col-md-6']
            ], [
                'name' => 'fuel_price',
                'label' => 'Fuel Price',
                'type' => 'number',
                'wrapperAttributes'=>['class'=>'form-group col-md-6']
            ]
        ];
    }

    public function getColumnsFields()
    {
        return [
            
        ];
    }
    
}