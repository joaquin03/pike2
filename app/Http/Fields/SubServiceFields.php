<?php

namespace App\Http\Fields;

class SubServiceFields extends AbstractFields
{
    public function getFormFields()
    {
        return [
            [
                'name' => 'name',
                'label' => 'Name',
                'type' => 'text',
            ],
            [
                'name' => 'description',
                'label' => 'Description',
                'type' => 'textarea',
            ],
            [
                'name' => 'service_id',
                'label' => "Parent service",
                'type' => "select2",
                'entity' => 'parent',
                'attribute' => "name",
                'model' => 'App\Models\ServiceParent',
                'wrapperAttributes'=>['class'=>'form-group col-md-4']
            ],
            [
                'name'        => 'show_in_quotations', // the name of the db column
                'label'       => 'Show in Quotations', // the input label
                'type'        => 'radio',
                'options'     => [
                    0 => "No",
                    1 => "Yes",
                ],
                'wrapperAttributes'=>['class'=>'form-group col-md-4'],

                // optional
                'inline'      => true, // show the radios all on the same line?
            ],
            [       // Select2Multiple = n-n relationship (with pivot table)
                'label' => "Providers",
                'type' => 'select2_multiple',
                'name' => 'providers', // the method that defines the relationship in your Model
                'entity' => 'providers', // the method that defines the relationship in your Model
                'attribute' => 'name', // foreign key attribute that is shown to user
                'model' => 'App\Models\Provider', // foreign key model
                'pivot' => true, // on create&update, do you need to add/delete pivot table entries?,
            ],

        ];
    }
    
    public function getColumnsFields($type='service')
    {
        return [
            ['name' => 'name', 'label' => 'Name', 'type' => 'link_edit', 'link' => url('admin/'.$type)],
            [
                'name' => 'description',
                'label' => 'Description',
                'type' => 'textarea',
            ],
            [
                'name' => 'service_id',
                'label' => "Parent service",
                'type' => "select",
                'entity' => 'parent',
                'attribute' => "name",
                'model' => 'App\Models\Service'
            ],

        ];
    }
    
}