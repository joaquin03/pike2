<?php

namespace App\Http\Fields;

class PikeServiceFields extends AbstractFields
{
    public function getFormFields()
    {
        return [
            [
                'name' => 'name',
                'label' => 'Name',
                'type' => 'text',
            ],
            [
                'name' => 'description',
                'label' => 'Description',
                'type' => 'textarea',
            ],
            [
                'name' => 'service_id',
                'label' => "Parent service",
                'type' => "select2",
                'entity' => 'parent',
                'attribute' => "name",
                'model' => 'App\Models\ServiceParent',
                'wrapperAttributes'=>['class'=>'form-group col-md-4']
            ],

        ];
    }

    public function getColumnsFields($type='service')
    {
        return [
            ['name' => 'name', 'label' => 'Name', 'type' => 'link_edit', 'link' => url('admin/'.$type)],
            [
                'name' => 'description',
                'label' => 'Description',
                'type' => 'textarea',
            ],
            [
                'name' => 'service_id',
                'label' => "Parent service",
                'type' => "select",
                'entity' => 'parent',
                'attribute' => "name",
                'model' => 'App\Models\ServiceParent',
                'wrapperAttributes'=>['class'=>'form-group col-md-4']
            ]
        ];
    }

}