<?php
namespace App\Http\Fields;
use App\Models\Accounting\Bill;

class InvoiceFields extends AbstractFields
{
    public function getFormFields()
    {
        return [
            [
                'name' => 'bill_number',
                'label' => 'Bill Number',
                'type' => 'bill_text',
                'wrapperAttributes'=>['class'=>'form-group col-md-6']
            ],
            [
                'name' => 'amount',
                'label' => 'Amount',
                'type' => 'bill_number',
                'prefix' => 'USD',
                'wrapperAttributes'=>['class'=>'form-group col-md-3'],
                'attributes' => ['disabled' => 'disabled']
            ],
            [
                'name' => 'dollar_change',
                'label' => 'Dollar Exchange Rate UYU',
                'type' => 'bill_number',
                'attributes' => ["step" => "any"],
                'wrapperAttributes'=>['class'=>'form-group col-md-3'],
                'default' => Bill::getLastItemOfTheDay(),
            ],
            [   // date_picker
                'name' => 'start_date',
                'type' => 'bill_datetime_picker',
                'label' => 'Bill Date',
                // optional:
                'datetime_picker_options' => [
                    'format' => 'DD/MM/YYYY',
                    'language' => 'es'
                ],
                'wrapperAttributes'=>['class'=>'form-group col-md-6']
            ],
            [   // date_picker
                'name' => 'due_date',
                'type' => 'bill_datetime_picker',
                'label' => 'Due Date',
                'datetime_picker_options' => [
                    'format' => 'DD/MM/YYYY',
                    'language' => 'es'
                ],
                'wrapperAttributes'=>['class'=>'form-group col-md-6']
            ],
            [ // select_from_array
                'name' => 'status',
                'label' => "Status",
                'type' => 'bill_select_from_array',
                'options' => ['not_paid' => 'Not Paid', 'partially_paid' => 'Partially Paid', 'paid' => 'Paid'],
                'allows_null' => false,
                'default' => 'paid',
                'wrapperAttributes'=>['class'=>'form-group col-md-6']
            ],
            [
                'name' => 'note',
                'label' => 'Notes',
                'type' => 'bill_textarea',
                'wrapperAttributes'=>['class'=>'form-group col-md-6']
            ],
            [
                'name' => 'accounting_note',
                'label' => 'Notes',
                'type' => 'procurement_accounting_services',
                'wrapperAttributes'=>['class'=>'form-group col-md-6']
            ],

        ];
    }
    public function getColumnsFields()
    {
        return [
            ['name' => 'bill_number', 'label' => 'Number', 'type' => 'link_edit', 'link' => url('admin\bills')],
            [
                'name' => 'company_id',
                'label' => 'Company',
                'entity' => 'company',
                'type' => "select",
                'attribute' => 'name',
                'model' => 'App\Models\Company'
            ],
            [
                'name' => 'amount', // The db column name
                'label' => "Amount", // Table column heading
                'type' => "number",
                // 'prefix' => "$",
                // 'suffix' => " EUR",
                // 'decimals' => 2,
            ],
            [
                'name' => "date", // The db column name
                'label' => "Bill Date", // Table column heading
                'type' => "date"
            ],
            [
                'name' => "due_date", // The db column name
                'label' => "Due Date", // Table column heading
                'type' => "date"
            ],
            [
                'name' => "status", // The db column name
                'label' => "Status", // Table column heading
                'type' => "text"
            ],
        ];
    }




}