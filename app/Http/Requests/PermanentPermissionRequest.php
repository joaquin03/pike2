<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\Utilities\Country;

class PermanentPermissionRequest extends \Backpack\CRUD\app\Http\Requests\CrudRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type'        => ['required', 'in:landing,overflight,landing diplomatic,overflight diplomatic'],
            'code'        => ['required'],
            'file_name'   => ['required'],
            'start_at'    => ['required', 'date'],
            'expire_at'   => ['required', 'date'],
            'country'     => ['required', 'in:' . implode(',', array_values(Country::getCodes()))],
            'provider_id' => ['required', 'exists:companies,id'],
            'aircrafts'   => ['required'],
//            'aircrafts.*' => ['required', 'exists:aircrafts,id'],
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //
        ];
    }
}
