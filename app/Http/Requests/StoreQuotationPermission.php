<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreQuotationPermission extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code'                => '',
            'permission_type'     => '',
            'all_providers_id'    => '',
            
            'icao_from'=> '',
            'icao_to'    => '',
            'icao_file'     => '',
        ];
    }
}
