<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreItineraryService extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'service_id'          => 'required',
            'provider_id'         => 'required',
            'operation_status'    => '',
            
            'procurement_quantity'=> '',
            'procurement_cost'    => '',
            'procurement_tax'     => '',
        ];
    }
}
