<?php

namespace App\Http\Requests;

use Backpack\CRUD\app\Http\Requests\CrudRequest;
use Illuminate\Validation\Rule;

class StoreCompanyTagRequest extends CrudRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $companyTagsId = $this->get('id', false) ?? null;
        
        return [
            'name' => ['required', Rule::unique('company_tags')->ignore($companyTagsId)],
        ];
    }
}
