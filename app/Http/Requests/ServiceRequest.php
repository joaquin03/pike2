<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

class ServiceRequest extends \Backpack\CRUD\app\Http\Requests\CrudRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return \Auth::check();
    }
    
    /**
     * Get the validation rules that apply to the request.
     *s
     * @return array
     */
    public function rules()
    {
        $data = $this->request->all();
        
        return [
            'name' => [
                'required',
                Rule::unique('services', 'name')->where('is_for_procurement', 0)
                    ->ignore($data['id'] ?? null)->where(function ($query) {
                        return $query->where('deleted_at', null);
                    })
            
            
            ],
        ];
    }
    
    
    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }
    
    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //
        ];
    }
}
