<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class QuotationRequest extends OperationRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return \Auth::check();
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code'                   => '',
            'in_charge_user_id'      => 'required',
            'state'                  => 'required',
            'notes'                  => '',
            
            'aircraft_id'            => '',
            'aircraft_operator_id'   => '',
            'aircraft_client_id'     => '',
        
        
        ];
    }
    
    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }
    
    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //
        ];
    }
}
