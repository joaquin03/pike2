<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CompanyBillingInfoRequest extends \Backpack\CRUD\app\Http\Requests\CrudRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    
    public function authorize()
    {
        // only allow updates if the user is logged in
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    
    //->has('user_id') ? '' : 'required|max:255',
    
    public function rules()
    {
        
        return [
            'document_receptor'         =>  'required_unless:doc_type_receptor,4',
            'country_code_receptor'     =>  'required_unless:doc_type_receptor,4',
            'business_name_receptor'    =>  '',
            'address_receptor'          =>  '',
            'city_receptor'             =>  '',
            'state_receptor'            =>  '',
            'postal_code_receptor'      =>  'required_unless:doc_type_receptor,4',
            'billing_days_receptor'     =>  '',
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //
        ];
    }
}
