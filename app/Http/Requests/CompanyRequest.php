<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\Utilities\Country;
use Illuminate\Validation\Rule;

class CompanyRequest extends \Backpack\CRUD\app\Http\Requests\CrudRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     *
     */
    public function rules()
    {
        $id = $this->request->get('id', false);
        $name = $this->request->get('name', false);
    
        return [
            'name'            =>  ['required', 'string',  'unique:companies,name,'.$id ],
            'country'         =>  ['required', 'in:' . implode(',', Country::getCodes())],
            'business'        =>  [],
            'payment_methods' =>  ['array'],
            'company_type'    =>  ['array'],
        ];
    }
    
    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //
        ];
    }
}
