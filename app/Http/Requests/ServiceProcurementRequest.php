<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

class ServiceProcurementRequest extends ServiceRequest {
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $data = $this->request->all();
    
        return [
            'name' => ['required', Rule::unique('services', 'name')
                ->where('is_for_procurement', 1)
                ->ignore($data['id'] ?? null)->where(function ($query) {
                    return $query->where('deleted_at', null);
                })
            ],
//            'service_id' => 'required',
        ];
    }
    
}
