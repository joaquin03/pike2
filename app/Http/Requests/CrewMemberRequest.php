<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\CrewMember;
use App\Models\Utilities\Country;

class CrewMemberRequest extends \Backpack\CRUD\app\Http\Requests\CrudRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'            =>  ['required', 'string'],/*['required', self::uniqueRule('companies', 'name', $name, $id)],*/
            'country'         =>  ['required', 'in:' . implode(',', Country::getCodes())],
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //
        ];
    }
}
