<?php
/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 3/27/18
 * Time: 1:37 PM
 */

namespace App\Http\Controllers\Admin\Contracts;

use App\Events\PaymentDeleted;
use App\Events\PaymentDone;
use App\Http\Requests\PaymentRequest as StoreRequest;
use App\Http\Requests\PaymentRequest as UpdateRequest;
use Illuminate\Http\Request;
use App\Http\Requests\PaymentRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Illuminate\Support\Facades\Redirect;

abstract class AccountingCrudController extends CrudController
{
    protected $type = '';
    protected $routeCompanyStatement = '';
    
    public function index()
    {
        $this->crud->hasAccessOrFail('list');
    
        return Redirect::to(route('crud.client-statement.show', ['client_id'=>$this->getLastParameter()]));
    }
    
    public function store(StoreRequest $request, $clientId)
    {

        $request->merge(['user_id'      => \Auth::user()->id]);
        $request->merge(['type'         => $this->type]);
        $request->merge(['company_id'   => $clientId]);


        parent::storeCrud($request);
        return Redirect::to(route('crud.'.$this->routeCompanyStatement.'.show', ['client_id'=>$clientId]));
    }
    
    public function update(UpdateRequest $request)
    {
        $redirect_location = parent::updateCrud($request);
        
        return Redirect::to(route('crud.'.$this->routeCompanyStatement.'.show', ['client_id'=>$this->crud->companyId]));
    }
    
    public function edit($id)
    {
        return parent::edit($this->getLastParameter());
    }
    
    public function destroy($id)
    {
        return parent::destroy($this->getLastParameter());
    }
    
    protected function getLastParameter()
    {
        $params = \Route::current()->parameters();
        $lastparam =  array_values($params)[count($params)-1];
        return $lastparam;
    }
    
    protected function addBill(Request $request, $clientId)
    {
        return;
    }
    
    
}