<?php

namespace App\Http\Controllers\Admin\Contracts;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ActivityEmailRequest as StoreRequest;
use App\Http\Requests\ActivityEmailRequest as UpdateRequest;
use App\Models\ActivityReminder;
use App\Models\Company;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Requests\CrudRequest;
use Carbon\Carbon;

abstract class ActivityCrudController extends CrudController
{
    protected $storeRequest;
    protected $updateRequest;
    
    
    public function __construct()
    {
        parent::__construct();
    }
    
    public function activitySetUp($fields, $request)
    {
        $this->crud->companyId = \Route::current()->parameter('company_id');
        $this->crud->allowAccess('show');
        //filter of company
        $this->crud->addClause('join', 'activities', function ($query) {
            $query->on($this->crud->model->getTable() . '.id', 'activities.event_id')
                ->where('activities.event_type', get_class($this->crud->model))
                ->where('activities.company_id', '=', $this->crud->companyId);
        });
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        
        $this->crud->addFields($fields->getFormFields(), 'both');
        $this->crud->addColumns($fields->getColumnsFields());
        
        $this->storeRequest = $request;
        $this->updateRequest = $request;
        $this->crud->setCreateView('activity.create');
        $this->crud->setEditView('activity.edit');
    }
    
    
    public function store($company_id)
    {
        $this->validate(\Request::instance(), $this->storeRequest->rules());
        
        $company = Company::findOrFail($company_id);
        
        $response = parent::storeCrud();
        
        $activityClass = $this->crud->entry;
        
        if(!array_key_exists('date', \Request::instance()->all())){
            \Request::instance()['date'] = $this->crud->entry->start_date ?? Carbon::now();
        }
        if ($activityClass instanceof ActivityReminder){
            \Request::instance()['date'] = $this->crud->entry->due_date;
        }
        $company->createActivity($activityClass , \Request::instance()->get('visibility'), \Request::instance()->all());
        
        
        return redirect()->route('crud.company.show', ['company' => $company]);
    }
    
    public function update()
    {
        $this->validate(\Request::instance(), $this->updateRequest->rules());
       
        $response = parent::updateCrud();
        
        return redirect()->route('crud.company.show', ['company' => $this->getCompanyId()]);
    }
    
    public function getActivityId()
    {
        return array_values(\Request::instance()->route()->parameters)[1];
    }
    
    private function getCompanyId()
    {
        return array_values(\Request::instance()->route()->parameters)[0];
    }
    
    public function edit($id)
    {
        $company = Company::findOrFail($this->getCompanyId());
        
        return parent::edit($this->getActivityId()); // TODO: Change the autogenerated stub
    }
    
    public function destroy($id)
    {
        $company = Company::findOrFail($this->getCompanyId());
        
        return parent::destroy($this->getActivityId());
        
    }
    
    
}
