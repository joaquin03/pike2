<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Contracts\ActivityCrudController;
use App\Http\Fields\ActivityFileFields;
use App\Http\Fields\ActivitySpecialFeatureFields;
use App\Http\Requests\ActivityFileRequest;
use App\Http\Requests\ActivitySpecialFeatureRequest;
use App\Models\Company;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ActivityFileRequest as StoreRequest;
use App\Http\Requests\ActivityFileRequest as UpdateRequest;

class ActivitySpecialFeatureCrudController extends ActivityCrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\ActivitySpecialFeatures');
        $this->crud->setEntityNameStrings('Activity Special Feature', 'Activity Special Features');
        $this->activitySetUp(new ActivitySpecialFeatureFields(), new ActivitySpecialFeatureRequest());
    
        $this->crud->setRoute("admin/company/".$this->crud->companyId.'/activity-special-features');
    }
    
}
