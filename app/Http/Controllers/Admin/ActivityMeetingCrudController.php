<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Contracts\ActivityCrudController;
use App\Http\Fields\ActivityMeetingFields;
use App\Http\Requests\ActivityMeetingRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ActivityMeetingRequest as StoreRequest;
use App\Http\Requests\ActivityMeetingRequest as UpdateRequest;

class ActivityMeetingCrudController extends ActivityCrudController
{
    public function setup()
    {
        
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\ActivityMeeting');
        $this->crud->setEntityNameStrings('Activity Meeting', 'activity Meetings');
        
        $this->activitySetUp(new ActivityMeetingFields(), new ActivityMeetingRequest());
        $this->crud->setRoute("admin/company/" . $this->crud->companyId . "/activity-meeting");
        
    }
    
}
