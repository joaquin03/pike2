<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Fields\ItineraryFields;
use App\Http\Requests\StoreFlightPlanRequest;
use App\Http\Requests\StoreItineraryCrewMember;
use App\Http\Requests\StoreItineraryPermission;
use App\Http\Requests\StoreItineraryService;
use App\Http\Transformers\ItineraryProviderServiceTransformer;
use App\Http\Transformers\Procurement\ItineraryServicesAdditionalsTransformer;
use App\Http\Transformers\Procurement\ProviderServicesTransformer;
use App\Http\Transformers\Procurement\ProvidersItineraryServicesTransformer;
use App\Models\Accounting\AccountingService;
use App\Models\Accounting\Additional;
use App\Models\Airport;
use App\Models\Itinerary;
use App\Models\ItineraryProviderService;
use App\Models\Operation;
use App\Models\ProviderService;
use App\Models\Quotation;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use App\Http\Transformers\Procurement\ItineraryProvidersTransformer;


// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ItineraryRequest as StoreRequest;
use App\Http\Requests\ItineraryRequest as UpdateRequest;
use Illuminate\Http\Request;


class ItineraryCrudController extends CrudController
{
    public function setup()
    {
        
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */

        $this->crud->setModel('App\Models\Itinerary');
        $this->crud->setEntityNameStrings('itinerary', 'itineraries');
        
        $this->crud->operationId = \Route::current()->parameter('operation_id');
        
        //filter of company
        $this->crud->addClause('join', 'operations', function ($query) {
            $query->on($this->crud->model->getTable() . '.operation_id', 'operations.id');
        });
        $this->crud->setRoute("admin/operation/" . $this->crud->operationId . '/itinerary');

        $this->crud->allowAccess('revisions');
        $this->crud->with('revisionHistory');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $fields = new ItineraryFields();
        $this->crud->addFields($fields->getFormFields(), 'both');
        $this->crud->addColumns($fields->getColumnsFields());
        
        $this->crud->enableDetailsRow();



        $this->crud->allowAccess('details_row');
        $this->crud->setCreateView('operation.modals.itineraryCreate');
        $this->crud->setEditView('operation.modals.itineraryCreate');
    }

    public function getType(){

    }
    
    public function show($itinerary_id)
    {
        $itinerary = Itinerary::findOrFail($itinerary_id);
        
        $this->data['operation'] = Operation::withoutGlobalScopes()->findOrFail($itinerary->operation_id);
        $this->data['providers'] = Airport::providersByAirportsIcao($itinerary->airport);
        $providers = $this->data['providers'];
        
        $transformer = new ItineraryProviderServiceTransformer();
        $providerServices = $itinerary->itineraryProviderServicesParent()->get();
        $providerServices = $transformer->transformItems($providerServices);

        return view('operation.modals.itineraryShow', compact('itinerary','providers', 'providerServices'));
    }
    
    public function create()
    {
        
        $this->crud->hasAccessOrFail('create');
        
        // prepare the fields you need to show
        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getCreateFields();
        $this->data['title'] = trans('backpack::crud.add') . ' ' . $this->crud->entity_name;
        $this->data['itinerary'] = new Itinerary();
        
        $this->data['creating'] = true;
        
        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getCreateView(), $this->data);
    }
    
    public function createQuotationItinerary()
    {
        $this->crud->hasAccessOrFail('create');
        
        // prepare the fields you need to show
        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getCreateFields();
        $this->data['title'] = trans('backpack::crud.add') . ' ' . $this->crud->entity_name;
        $this->data['itinerary'] = new Itinerary();
        
        $this->data['creating'] = true;
        
        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view('quotation.modals.itineraryCreate', $this->data);
    }
    
    public function edit($id)
    {
        $this->crud->hasAccessOrFail('update');
        
        // get the info for that entry
        $this->data['entry'] = $this->crud->getEntry($id);
        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getUpdateFields($id);
        $this->data['title'] = trans('backpack::crud.edit') . ' ' . $this->crud->entity_name;
        $this->data['itinerary'] = $this->data['entry'];
        $this->data['creating'] = false;
        $this->data['id'] = $id;

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getEditView(), $this->data);
    
    }

    public function editQuotation($id)
    {
        $this->crud->hasAccessOrFail('update');

        // get the info for that entry
        $this->data['entry'] = $this->crud->getEntry($id);
        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getUpdateFields($id);
        $this->data['title'] = trans('backpack::crud.edit') . ' ' . $this->crud->entity_name;
        $this->data['itinerary'] = $this->data['entry'];
        $this->data['creating'] = false;
        $this->data['id'] = $id;

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view('quotation.modals.itineraryCreate', $this->data);
    }



    public function delete(Request $request, $operation_id, $itinerary_id)
    {
        $itinerary = Itinerary::findOrFail($itinerary_id);

        if(!$itinerary->itineraryProviderServices()->exists()){
            $itinerary->delete();
            \Alert::success('Itinerary deleted.')->flash();
        }else{
            \Alert::error('You must remove the associated services to delete the itinerary.')->flash();
        }

        return $this->redirectOperationItinerary($operation_id, $itinerary_id);
    }


    public function deleteQuotation(Request $request, $operation_id, $itinerary_id)
    {
        $itinerary = Itinerary::findOrFail($itinerary_id);

        if(!$itinerary->itineraryProviderServices()->exists()){
            $itinerary->delete();
            \Alert::success('Itinerary deleted.')->flash();
        }else{
            \Alert::error('You must remove the associated services to delete the itinerary.')->flash();
        }

        return $this->redirectQuotationItinerary($operation_id, $itinerary_id);
    }

    public function cancel(Request $request, $operation_id, $itinerary_id)
    {
        $itinerary = Itinerary::findOrFail($itinerary_id);
        $itinerary->is_canceled = true;
        $itinerary->save();
        $operation = $itinerary->operation;

        $operation->resetStartDate();
        
        \Alert::success('Itinerary canceled.')->flash();

        return $this->redirectOperationItinerary($operation_id, $itinerary_id);
    }

    public function cancelQuotation(Request $request, $operation_id, $itinerary_id)
    {
        $itinerary = Itinerary::findOrFail($itinerary_id);
        $itinerary->is_canceled = true;
        $itinerary->save();
        $operation = $itinerary->quotation;

        $operation->resetStartDate();

        \Alert::success('Itinerary canceled.')->flash();

        return $this->redirectQuotationItinerary($operation_id, $itinerary_id);
    }
    
    private function redirectOperationItinerary($operation_id, $itinerary_id)
    {
//        $type = explode("/", $_SERVER['REQUEST_URI'])[2];
        return redirect()->to(
            route('crud.operation.edit', ['operation_id' => $operation_id]) . "#itinerary-" . $itinerary_id);
    }
    
    private function redirectQuotationItinerary($operation_id, $itinerary_id)
    {
        return redirect()->to(
            route('crud.quotation.edit', ['operation_id' => $operation_id]) . "#itinerary-" . $itinerary_id);
    }
    
    public function store(Request $request, $operation_id)
    {
        
        $operation = Operation::withoutGlobalScopes()->findOrFail($operation_id);
        foreach($request->get('itinerary') as $itinerary) {
            $itinerary = $operation->createItinerary($itinerary);
        }
    
        if ($itinerary) {
            \Alert::success('Itinerary added');
        } else {
            \Alert::error('Itinerary not added');
        }
        return $this->redirectOperationItinerary($operation->id, $itinerary->id);
    }
    
    public function storeQuotationItinerary(Request $request, $operation_id)
    {
        $quotation = Quotation::withoutGlobalScopes()->findOrFail($operation_id);
        foreach($request->get('itinerary') as $itinerary) {
            $itinerary = $quotation->createItinerary($itinerary);
        }
        
        if ($itinerary) {
            \Alert::success('Itinerary added');
        } else {
            \Alert::error('Itinerary not added');
        }
        return $this->redirectQuotationItinerary($quotation->id, $itinerary->id);
    }
    
    public function update(StoreRequest $request, $itinerary_id)
    {
        $itinerary = Itinerary::findOrFail($itinerary_id);
        $itinerary->fill($request["itinerary"][0]);
        
        if ($itinerary->save()) {
            \Alert::success('Itinerary updated')->flash();
        } else {
            \Alert::error('Itinerary not updated')->flash();
        }

        return $this->redirectOperationItinerary($itinerary->operation_id, $itinerary_id);
    }

    public function updateQuotationItinerary(StoreRequest $request, $itinerary_id)
    {
        $itinerary = Itinerary::findOrFail($itinerary_id);
        $itinerary->fill($request["itinerary"][0]);

        if ($itinerary->save()) {
            \Alert::success('Itinerary updated')->flash();
        } else {
            \Alert::error('Itinerary not updated')->flash();
        }

        return $this->redirectQuotationItinerary($itinerary->operation_id, $itinerary_id);
    }

    
    public function updateProcurementServiceStatus(StoreRequest $request, $itinerary_id)
    {
        $itinerary = Itinerary::findOrFail($itinerary_id);
        $itinerary->fill($request->all());
        
        if ($itinerary->save()) {
            \Alert::success('Itinerary Status updated')->flash();
        } else {
            \Alert::error('Itinerary Status not updated')->flash();
        }
        
        return redirect()->to(
            route('crud.procurement.edit', $itinerary->operation->id));
    }
    
    public function updateBillingServiceStatus(StoreRequest $request, $itinerary_id)
    {
        $itinerary = Itinerary::findOrFail($itinerary_id);
        $itinerary->fill($request->all());
        
        if ($itinerary->save()) {
            \Alert::success('Itinerary Status updated')->flash();
        } else {
            \Alert::error('Itinerary Status not updated')->flash();
        }
    
        return redirect()->to(
            route('crud.billing.edit', $itinerary->operation->id));
    }
    
    /*Services */
    public function createItineraryService($itinerary_id)
    {
        $itinerary = Itinerary::findOrFail($itinerary_id);
        $this->data['operation'] = Operation::findOrFail($itinerary->operation_id);
        $this->data['providers'] =  Airport::providersByAirportsIcao($itinerary->airport);
        $providers = $this->data['providers'];

        return view('operation.modals.services', compact('itinerary', 'providers'));
    }
    
    public function storeItineraryService(StoreItineraryService $request, $itinerary_id)
    {
        $itinerary = Itinerary::findOrFail($itinerary_id);

        if($itinerary->existsProviderServices($request->service_provider_id)){
            return response()->json(['message' => 'Service already exists'], 422);
        }

        $itinerary->addService($request->all());

        $transformer = new ProviderServicesTransformer();
        $providerServices = $itinerary->providerServices()->get();
        $providerServices = $transformer->transformItems($providerServices);
        return response()->json($providerServices, 200);
    }



    public function updateServiceStatus($itinerary_id,$service_id, $status)
    {
        $itinerary = Itinerary::findOrFail($itinerary_id);
        $providerServiceUpdated = $itinerary->updateProviderServiceStatus($service_id, $status);
        return $providerServiceUpdated;

    }

    public function deleteItineraryService($itinerary_id, $itinerary_provider_id)
    {
        $itinerary = Itinerary::findOrFail($itinerary_id);

        if(!empty($itinerary_provider_id))
        {
            $itinerary->providerServices()->wherePivot('id',$itinerary_provider_id)->detach();
            $itinerary->setProcurementStatus(true);
            \Alert::success('Service deleted')->flash();
        } else{
            \Alert::error('Service not deleted')->flash();
        }

        return $this->redirectOperationItinerary($itinerary->operation_id, $itinerary_id);
    }
    
    public function createCrewMember($itinerary_id)
    {
        $itinerary = Itinerary::findOrFail($itinerary_id);
        
        return view('operation.modals.crew-member', compact('itinerary'));
    }
    
    public function storeCrewMember(StoreItineraryCrewMember $request, $itinerary_id)
    {
        $itinerary = Itinerary::findOrFail($itinerary_id);
        
        $itinerary->addCrewMembers($request->all());
        
        return $this->redirectOperationItinerary($itinerary->operation_id, $itinerary_id);
    }
    
    
    public function createFlightPlan($itinerary_id)
    {
        $itinerary = Itinerary::findOrFail($itinerary_id);
        $flightPlan = $itinerary->flightPlan;
        return view('operation.modals.flight-plan', compact('itinerary', 'flightPlan'));
    }
    
    public function storeFlightPlan(StoreFlightPlanRequest $request, $itinerary_id)
    {
        $itinerary = Itinerary::findOrFail($itinerary_id);
        
        $itinerary->editFlightPlan($request->all());
        
        return $this->redirectOperationItinerary($itinerary->operation_id, $itinerary_id);
    }

    public function getStatementFromService($itineraryProviderServiceId)
    {
        $accountingService = AccountingService::where('servicesable_id', $itineraryProviderServiceId)
            ->where('servicesable_type', get_class(new ItineraryProviderService()))->first();

        return redirect()->route('crud.invoices.edit', ['id' => $accountingService->bill_id]);
    }
    
    public function getStatementFromAdditional($additionalId)
    {
        $accountingService = AccountingService::where('servicesable_id', $additionalId)
            ->where('servicesable_type', get_class(new Additional()))->first();
        
        return redirect()->route('crud.invoices.edit', ['id' => $accountingService->bill_id]);
    }

    public function listRevisions($id)
    {
        $this->crud->hasAccessOrFail('revisions');

        // get the info for that entry
        $this->data['entry'] = $this->crud->getEntry($id);
        $this->data['crud'] = $this->crud;
        $this->data['title'] = ucfirst($this->crud->entity_name).' '.trans('backpack::crud.revisions');
        $this->data['id'] = $id;
        $this->data['revisions'] =  $this->data['entry']->getRevisions(new Itinerary());
        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view('revision.revisions', $this->data);
    }
    
    
}
