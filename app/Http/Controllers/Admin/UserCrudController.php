<?php

namespace App\Http\Controllers\Admin;




// VALIDATION: change the requests to match your own file names if you need form validation

use App\Models\Utilities\Country;
use Backpack\PermissionManager\app\Http\Controllers\UserCrudController as UserBaseController;
use Illuminate\Support\Facades\Auth;

class UserCrudController extends UserBaseController
{
    
    public function setup()
    {
        parent::setup();
        
        // Fields
        $this->crud->addFields([
            [
                'label' => "Available Companies",
                'type'  => "select2_multiple",
                'name'  => 'availableCompanies',
                'entity'    => 'availableCompanies',
                'model'     => 'App\Models\Company',
                'attribute' => "name",
                'pivot'     => true,
            ],

        ]);

    }
    
    public function index(){
        if(!Auth::user()->can('User management')){
            return redirect('/admin/dashboard')->withErrors( 'Error! User management not allowed. Contact your admin for permissions.');
        }
        $this->crud->hasAccessOrFail('list');
    
        $this->data['crud'] = $this->crud;
        $this->data['title'] = ucfirst($this->crud->entity_name_plural);
    
        // get all entries if AJAX is not enabled
        if (! $this->data['crud']->ajaxTable()) {
            $this->data['entries'] = $this->data['crud']->getEntries();
        }
    
        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getListView(), $this->data);
    }
    
    
    
    
}
