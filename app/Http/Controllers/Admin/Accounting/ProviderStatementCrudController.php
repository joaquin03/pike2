<?php
/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 3/22/18
 * Time: 1:18 PM
 */

namespace App\Http\Controllers\Admin\Accounting;


use App\Events\PaymentDeleted;
use App\Http\Transformers\Accounting\CompanyStatementTransformer;
use App\Models\Accounting\CreditNote;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Illuminate\Http\Request;
use App\Models\Accounting\Invoice;
use App\Models\Accounting\Payment;
use Excel;


class ProviderStatementCrudController extends CompanyStatementCrudController
{
    protected $routeCompanyStatement = '/provider-statement';
    protected $companyType = 'providers';
    
    public function setup()
    {
        parent::setup();
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Accounting\ProviderStatement');
        $this->crud->setEntityNameStrings('Provider Statement', 'Provider Statements');
        
        $this->crud->setShowView('accounting.show');
        $this->crud->allowAccess('show');
    
        $this->crud->addClause('orderBy', 'name');
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        //$this->crud->addColumns($this->addColumns());
    }
    
    public function delete(Request $request, $statementId)
    {
        if ($request->type == 'Invoice') {
            $item = Invoice::findOrFail($statementId);
        } else {
            if ($request->type == 'CreditNote') {
                $item = CreditNote::findOrFail($statementId);
                
            } else {
                $item = Payment::findOrFail($statementId);
            }
        }
        $invoices = $item->invoices;
        $bills = $item->bills;
        
        $company = $item->company;
        $deleted = $item->deleteItem();
        
        if ($deleted) {
            
            if ($request->type == 'Payment' || $request->type == 'CreditNote' ) {
                event(new PaymentDeleted($item));
            }
            return redirect()->route('crud.provider-statement.show', ['id' => $company->id]);
        } else {
            \Alert::error('Item not deleted')->flash();
        }
    }
    
    
    public function show($id)
    {
        $this->crud->hasAccessOrFail('show');
        
        // get the info for that entry
        $company = $this->crud->getEntry($id);
        $transformer = new CompanyStatementTransformer($this->companyType);
        $data = $transformer->transform($company);
        
        return view('accounting.providerStatementShow', $data);
    }
    
    public function exportTable($id)
    {
        $this->crud->hasAccessOrFail('show');
        
        $company = $this->crud->getEntry($id);
        $transformer = new CompanyStatementTransformer($this->companyType);
        $data = $transformer->transform($company);
        
        Excel::create('Provider ' . $this->crud->getEntry($id)->name, function ($excel) use ($data) {
            
            $excel->sheet('New sheet', function ($sheet) use ($data) {
                
                
                $sheet->loadView('accounting.exportableTables.providerStatement')->with($data);
                $sheet->setOrientation('landscape');
                
            });
            
        })->export('xls');
//        return view('accounting.exportableTables.providerStatement', $data);
    
    }
    
}
