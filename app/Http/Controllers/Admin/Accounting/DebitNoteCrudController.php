<?php

namespace App\Http\Controllers\Admin\Accounting;

use App\Helpers\GarinoGenerator;
use App\Http\Fields\AbstractFields;
use App\Http\Fields\DebitNoteFields;
use App\Http\Transformers\Accounting\AccountingServicesTransformer;
use App\Models\Accounting\DebitNote;
use App\Models\Scopes\BillScope;
use App\Models\Scopes\NotDebitNoteScope;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use App\Models\Accounting\Bill;
use App\Http\Fields\BillsFields;
use Illuminate\Support\Facades\Auth;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\BillRequest as StoreRequest;
use App\Http\Requests\BillRequest as UpdateRequest;
use Illuminate\Support\Facades\Redirect;

class DebitNoteCrudController extends CrudController
{
    
    protected $type = 'debit';
    protected $routeCompanyStatement = 'client-statement';
    
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Accounting\DebitNote');
        
        $this->crud->companyId = \Route::current()->parameter('client_id');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/clients/' . $this->crud->companyId . '/debit-notes');
        
        $this->crud->setEntityNameStrings('Debit Note', 'Debit Notes');
        $this->crud->backroute = config('backpack.base.route_prefix') . '/client-statement/' . $this->crud->companyId;
    
    
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        
        $this->setFields();
        
        $this->crud->removeButton('delete');
        $this->crud->removeButton('edit');
        $this->crud->boxSizeClass = 'col-md-12';
    }
    
    protected function setFields()
    {
        $fields = new DebitNoteFields();
        $this->crud->addFields($fields->getFormFields(), 'both');
        $this->crud->addColumns($fields->getColumnsFields());
    }
    
    public function create()
    {
        $this->crud->hasAccessOrFail('create');
    
        // prepare the fields you need to show
        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getCreateFields();
        $this->data['title'] = trans('backpack::crud.add').' '.$this->crud->entity_name;
        
        
        
        $this->data['entry'] = new DebitNote();
        $this->data['entry']->billing_company_id = $this->crud->companyId;
        
        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getCreateView(), $this->data);
    }
    
    public function store(StoreRequest $request)
    {
        $request->request->add(['user_id' => Auth::id()]);
        $request->request->add(['type' => $this->type]);
        $request->request->add(['company_id' =>  $this->crud->companyId]);
        $request->request->add(['billing_company_id' =>  $this->crud->companyId]);
        $request->request->add(['is_debit_note' =>  1]);
        
        
        //setear nro de bill
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);

        $this->crud->entry->addAccountingItems($request->get('services'));
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return Redirect::to(route('crud.'.$this->routeCompanyStatement.'.show', ['client_id'=>$this->crud->entry->billing_company_id]));
    }

    public function update(UpdateRequest $request)
    {
        $bill = DebitNote::findOrFail($request->get('id'));
        $bill->fill($request->all());
       
        $data = GarinoGenerator::generate($bill);
    
        //devuelve el string del rechazo
     
        $request->request->set('is_active', 1);

        // your additional operations before save here
        

        $bill->addBillToAccountingService();
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry

        $bill->bill_number = $data['bill_number'];
        $bill->bill_link = $data['url'];
    
        $bill->save();
        
        
        return Redirect::to(route('crud.'.$this->routeCompanyStatement.'.show', ['client_id'=>$this->crud->entry->billing_company_id]));
    }
    
    public function edit($id)
    {
        $this->crud->hasAccessOrFail('update');

        // get the info for that entry
        $this->data['entry'] = $this->crud->getEntry($id);
        if($this->data['entry']->is_active) {
            $this->crud->backroute = config('backpack.base.route_prefix') . '/client-statement/' .
                $this->data['entry']->company_id;
        } else {
            $this->crud->backroute = config('backpack.base.route_prefix') . '/billing/' .
                $this->data['entry']->operation_id .'/edit#itineraryId='. $this->crud->request->itineraryId;
        }

        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getUpdateFields($id);
        $this->data['title'] = trans('backpack::crud.edit').' '.$this->crud->entity_name;
        $this->data['id'] = $id;
        $transformer = new AccountingServicesTransformer($this->data['entry']->amount);
        
        $this->data['accountingServices'] = $transformer->transformItems($this->data['entry']->accountingServices);
        

        return view('vendor.backpack.crud.edit', $this->data);
    }
    
    public function showItem($clientId, $id)
    {
        $this->data['bill'] = DebitNote::withoutGlobalScope(new NotDebitNoteScope())->findOrFail($id);

        return view('accounting.debitNote.show', $this->data);
    }
    
    public function amount($bill_id)
    {
        $bill = DebitNote::findOrFail($bill_id);
        return round($bill->getReminingPayment(),2);
    }
    
    public function index()
    {
        $this->crud->hasAccessOrFail('list');
        
        $this->data['crud'] = $this->crud;
        $this->data['title'] = ucfirst($this->crud->entity_name_plural);
        
        // get all entries if AJAX is not enabled
        if (! $this->data['crud']->ajaxTable()) {
            $this->data['entries'] = $this->data['crud']->getEntries()->where('is_active', 1)->sortByDesc('date');
        }
        
        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getListView(), $this->data);
    }
    
    public function invalidate($id)
    {
       $bill = DebitNote::findOrFail($id);
       
       $bill->invalidateBill();
    
       \Alert::success('Debit Note invalidated')->flash();
    
       return redirect()->to(route('crud.'.$this->routeCompanyStatement.'.show', ['client_id'=>$bill->company_id]));
    }
    
    public function cancel($id)
    {
        $bill = DebitNote::findOrFail($id);
        
        $bill->cancelBill();
        
        \Alert::success('Debit Note canceled')->flash();
        
        return redirect()->to(route('crud.'.$this->routeCompanyStatement.'.show', ['client_id'=>$bill->company_id]));
    }
}
