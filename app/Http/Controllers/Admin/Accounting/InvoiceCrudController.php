<?php

namespace App\Http\Controllers\Admin\Accounting;

use App\Http\Fields\BillsProcurementFields;
use App\Http\Fields\InvoiceFields;
use App\Http\Transformers\Accounting\AccountingServicesTransformer;
use App\Models\Accounting\Invoice;
use App\Models\Aircraft;
use App\Models\Airport;
use App\Models\Provider;
use App\Models\Service;
use App\Models\ServiceParent;
use App\Models\Utilities\Country;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\InvoiceRequest as StoreRequest;
use App\Http\Requests\InvoiceRequest as UpdateRequest;
use Illuminate\Support\Facades\Redirect;

class InvoiceCrudController extends CrudController
{
    protected $type = 'credit';
    protected $routeCompanyStatement = 'provider-statement';
    
    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Accounting\Invoice');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/invoices');
        $this->crud->setEntityNameStrings('Invoice', 'Invoices');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->setFields();
    }
    
    protected function setFields()
    {
        $fields = new BillsProcurementFields();
        $this->crud->addFields($fields->getFormFields(), 'both');
        $this->crud->addColumns($fields->getColumnsFields());
    }

    public function store(StoreRequest $request)
    {
    
        $request->request->add(['user_id', Auth::id()]);
        $request->request->add(['type', $this->type]);
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return Redirect::to(route('crud.'.$this->routeCompanyStatement.'.show', ['client_id'=>$this->crud->entry->company_id]));
    
    }

    public function update(UpdateRequest $request)
    {
        $request->request->set('is_active', 1);
        
        $invoice = Invoice::findOrFail($request->get('id'));
        $invoice->fill($request->all());
        
        $invoice->addInvoiceToAccountingService();
        $invoice->amount = round ($invoice->amount, 2);
        $oldInvoice = $invoice->validateIfExistSameInvoice();
    
        if ($oldInvoice->id == $invoice->id) {
            $redirect_location = parent::updateCrud($request);
        }
        $invoice->save();
    
        return Redirect::to(route('crud.'.$this->routeCompanyStatement.'.show', ['client_id'=>$oldInvoice->company_id]));
    }
    
    public function edit($id)
    {

        $this->crud->hasAccessOrFail('update');

        // get the info for that entry
        $this->data['entry'] = $this->crud->getEntry($id);


        if($this->data['entry']->is_active) {
            $this->crud->backroute = config('backpack.base.route_prefix') . '/provider-statement/' .
                $this->data['entry']->company_id;
        } else {
            $this->crud->backroute = config('backpack.base.route_prefix') . '/procurement/' .
                $this->data['entry']->operation_id .'/edit/';
        }

        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getUpdateFields($id);
        $this->data['title'] = trans('backpack::crud.edit').' '.$this->crud->entity_name;
        $this->data['id'] = $id;
            $transformer = new AccountingServicesTransformer($this->data['entry']->amount);

            $this->data['accountingServices'] = $transformer->transformItems($this->data['entry']->accountingServices);

            return view('vendor.backpack.crud.views.edit', $this->data);
    }

    public function show($id)
    {
        $this->data['invoice'] = Invoice::find($id);


        return view('accounting.modals.invoice', $this->data);
    }
    
    public function report()
    {
        $aircraft = Aircraft::all();
        $countries = Country::all();
        $airports = Airport::all();
        $providers = Provider::all();
        $services = Service::all();
        
        return view('reports.invoice', compact('aircraft', 'countries', 'airports', 'providers', 'services'));
    }
    
    public function amount($invoice_id)
    {
        $invoice = Invoice::findOrFail($invoice_id);
        return round($invoice->getReminingPayment(),3);
    }
}
