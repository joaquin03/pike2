<?php

namespace App\Http\Controllers\Admin\Accounting;

use App\Helpers\GarinoGenerator;
use App\Http\Fields\AbstractFields;
use App\Http\Transformers\Accounting\AccountingServicesTransformer;
use App\Models\Accounting\DebitNote;
use App\Models\Aircraft;
use App\Models\Airport;
use App\Models\Client;
use App\Models\Scopes\BillScope;
use App\Models\Scopes\NotDebitNoteScope;
use App\Models\Service;
use App\Models\Utilities\Country;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use App\Models\Accounting\Bill;
use App\Http\Fields\BillsFields;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\BillRequest as StoreRequest;
use App\Http\Requests\BillRequest as UpdateRequest;
use Illuminate\Support\Facades\Redirect;
use Mockery\Matcher\Not;

class BillCrudController extends CrudController
{
    
    protected $type = 'debit';
    protected $routeCompanyStatement = 'client-statement';
    
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Accounting\Bill');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/bills');
        $this->crud->setEntityNameStrings('Bill', 'Bills');


        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        
        $this->setFields();
        
        $this->crud->removeButton('delete');
        $this->crud->removeButton('edit');
        $this->crud->boxSizeClass = 'col-md-12';
    }
    
    protected function setFields()
    {
        $fields = new BillsFields();
        $this->crud->addFields($fields->getFormFields(), 'both');
        $this->crud->addColumns($fields->getColumnsFields());
    }
    
    public function store(StoreRequest $request)
    {
        $request->request->add(['user_id', Auth::id()]);
        $request->request->add(['type', $this->type]);
        
        //setear nro de bill
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return Redirect::to(route('crud.'.$this->routeCompanyStatement.'.show', ['client_id'=>$this->crud->entry->billing_company_id]));
    }

    public function updateDebitNoteData($id, UpdateRequest $request)
    {

        $date = Carbon::createFromFormat("d/m/yy", $request->date);
        $due_date = Carbon::createFromFormat("d/m/yy", $request->due_date);

        $bill = Bill::findOrFail($id);

        $bill->date = $date;
        $bill->due_date = $due_date;
        $bill->dollar_change = $request->dollar_change;

        $bill->save();

    }

    public function update(UpdateRequest $request)
    {
        $bill = Bill::findOrFail($request->get('id'));
        $bill->fill($request->all());
       
        $data = GarinoGenerator::generate($bill);
    
        //devuelve el string del rechazo
     
        $request->request->set('is_active', 1);

        // your additional operations before save here
        

        $bill->addBillToAccountingService();
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
     
        
        $bill->bill_number = $data['bill_number'];
        $bill->bill_link = $data['url'];
    
        $bill->save();
        
        
        return Redirect::to(route('crud.'.$this->routeCompanyStatement.'.show', ['client_id'=>$this->crud->entry->billing_company_id]));
    }
    
    public function edit($id)
    {
        $this->crud->hasAccessOrFail('update');

        // get the info for that entry
        $this->data['entry'] = $this->crud->getEntry($id);
        if($this->data['entry']->is_active) {
            $this->crud->backroute = config('backpack.base.route_prefix') . '/client-statement/' .
                $this->data['entry']->company_id;
        } else {
            $this->crud->backroute = config('backpack.base.route_prefix') . '/billing/' .
                $this->data['entry']->operation_id .'/edit#itineraryId='. $this->crud->request->itineraryId;
        }

        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getUpdateFields($id);
        $this->data['title'] = trans('backpack::crud.edit').' '.$this->crud->entity_name;
        $this->data['id'] = $id;
        $transformer = new AccountingServicesTransformer($this->data['entry']->amount);
        
        $this->data['accountingServices'] = $transformer->transformItems($this->data['entry']->accountingServices);
        

        return view('vendor.backpack.crud.edit', $this->data);
    }
    
    public function show($id)
    {
        $this->data['bill'] = Bill::find($id);
        return view('accounting.billShow', $this->data);
    }
    
    public function amount($bill_id)
    {
        $bill = Bill::withoutGlobalScope(new NotDebitNoteScope())->findOrFail($bill_id);

        return round($bill->getReminingPayment(),2);
    }
    
    public function index()
    {
        $this->crud->hasAccessOrFail('list');
        
        $this->data['crud'] = $this->crud;
        $this->data['title'] = ucfirst($this->crud->entity_name_plural);
        
        // get all entries if AJAX is not enabled
        if (! $this->data['crud']->ajaxTable()) {
            $this->data['entries'] = $this->data['crud']->getEntries()->where('is_active', 1)->sortByDesc('date');
        }
        
        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getListView(), $this->data);
    }
    
    public function invalidate($id)
    {
       $bill = Bill::findOrFail($id);
       
       $bill->invalidateBill();
    
       \Alert::success('Bill canceled')->flash();
    
       return redirect()->to(route('crud.'.$this->routeCompanyStatement.'.show', ['client_id'=>$bill->company_id]));
    }
    
    public function cancel($id)
    {
        $bill = Bill::findOrFail($id);
        
        $bill->cancelBill();
        
        \Alert::success('Bill canceled')->flash();
        
        return redirect()->to(route('crud.'.$this->routeCompanyStatement.'.show', ['client_id'=>$bill->company_id]));
    }

    public function debitNoteCreate($id)
    {
        $this->data['bill'] = Bill::find($id);
        return view('accounting.billDebitNoteShow', $this->data);
    }

    public function debitNoteStore(Request $request)
    {
        $debitNote = DebitNote::storeDebitNote($request);

        return Redirect::to(route('crud.'.$this->routeCompanyStatement.'.show', ['client_id'=>$debitNote->billing_company_id]));

    }


    public function report()
    {
        $aircraft = Aircraft::all();
        $countries = Country::all();
        $airports = Airport::all();
        $clients = Client::all();
        $services = Service::all();

        return view('reports.bill', compact('aircraft', 'countries', 'airports', 'clients', 'services'));
    }

}
