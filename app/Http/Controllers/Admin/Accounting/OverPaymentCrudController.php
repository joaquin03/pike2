<?php

namespace App\Http\Controllers\Admin\Accounting;

use App\Events\PaymentDeleted;
use App\Http\Controllers\Admin\Contracts\AccountingCrudController;
use App\Http\Controllers\Admin\Traits\NestedRouteBadParameterBug;
use App\Http\Fields\ClientOverPaymentFields;
use App\Http\Fields\ClientPaymentFields;
use App\Http\Requests\PaymentRequest as StoreRequest;
use App\Http\Requests\PaymentRequest as UpdateRequest;
use App\Models\Accounting\CreditNote;
use App\Models\Accounting\Payment;
use Illuminate\Support\Facades\Redirect;
use App\Events\PaymentDone;


class OverPaymentCrudController extends PaymentCrudController
{
    use NestedRouteBadParameterBug;
    
    protected $type = 'credit';
    protected $routeCompanyStatement = 'client-statement';
    
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        
        $this->crud->setModel('App\Models\Accounting\Payment');
        $this->crud->setEntityNameStrings('Over payment', 'Over payments');
        
        $this->crud->companyId = \Route::current()->parameter('client_id');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/clients/' . $this->crud->companyId . '/over-payments');
        $this->crud->addClause('where', 'company_id', '=', $this->crud->companyId);
        $this->crud->addClause('where', 'is_over_payment', '=', '1');
        $this->crud->backroute = config('backpack.base.route_prefix') . '/client-statement/' . $this->crud->companyId;
        
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        
        $fields = new ClientOverPaymentFields();
        $this->crud->addFields($fields->getFormFields(), 'both');
        $this->crud->addColumns($fields->getColumnsFields());
    }
    
    public function store(StoreRequest $request, $clientId)
    {
        $request->merge(['user_id' => \Auth::user()->id]);
        $request->merge(['type' => $this->type]);
        $request->merge(['company_id' => $clientId]);
        
        parent::storeCrud($request);
        
        $payment = $this->data['entry'];
        $payment->setStatus();

        $payment->save();
        
        event(new PaymentDone($payment));
        
        
        return Redirect::to(route('crud.' . $this->routeCompanyStatement . '.show', ['client_id' => $clientId]));
    }
    
    
    public function update(UpdateRequest $request)
    {
        $payment = Payment::findOrFail($request->route('id'));
        $request->merge(['user_id' => \Auth::user()->id]);
        $request->merge(['type' => $this->type]);
    
        event(new PaymentDeleted($payment));
        
        parent::updateCrud($request);
        
        $payment = $this->data['entry'];
        $payment->setStatus();

        $payment->save();
        
        event(new PaymentDone($payment));
        
        return Redirect::to(route('crud.' . $this->routeCompanyStatement . '.show',
            ['client_id' => $this->crud->companyId]));
    }
}
