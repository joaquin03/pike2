<?php
/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 3/22/18
 * Time: 1:18 PM
 */

namespace App\Http\Controllers\Admin\Accounting;


use App\Http\Fields\AccountingReportFields;
use App\Http\Fields\BillingFields;
use App\Http\Transformers\Accounting\CompanyStatementTransformer;
use App\Models\Accounting\CompanyStatementReport;
use App\Models\Client;
use Backpack\CRUD\app\Http\Controllers\CrudController;


use App\Models\Accounting\CreditNote;
use Illuminate\Http\Request;
use App\Models\Accounting\Bill;
use App\Models\Accounting\Payment;
use Excel;

class CompanyStatementReportCrudController extends CrudController
{
    
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Accounting\CompanyStatementReport');
        $this->crud->setRoute(config('backpack.base.route_prefix').'/client-statement-report');
        $this->crud->setEntityNameStrings('client Statement', 'clients Statements');
    
        $this->crud->addClause('orderBy', 'name');
        
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
    
        $fields = new AccountingReportFields();
        $this->crud->setDefaultPageLength(20000);
        $this->crud->removeAllButtons();
        $this->crud->addFields($fields->getFormFields(), 'both');
        $this->crud->addColumns($fields->getColumnsFields());
    
        $this->crud->enableExportButtons();
        
        $this->crud->removeAllButtons();
    
        $this->setFiters();
    }
    
    private function setFiters()
    {
        $this->crud->addFilter([ // daterange filter
            'type' => 'date_range',
            'name' => 'date',
            'label'=> 'Date',
        ],
            false,
            function($value) {

            }
        );


        $this->crud->addFilter([ // daterange filter
            'type' => 'date_range',
            'name' => 'due_date',
            'label'=> 'Due Date range',
        ],
            false,
            function($value) {
            
            }
        );
        
        $this->crud->addFilter([ // dropdown filter
            'name' => 'company_id',
            'type' => 'select2_multiple',
            'label' => 'Client'
        ], function () {
            return Client::all()->pluck('name', 'id')->toArray();
        }, function ($values) { // if the filter is active
        
        });
        $this->crud->addFilter([ // dropdown filter
            'name' => 'types',
            'type' => 'select2_multiple',
            'label' => 'Type'
        ], function () {
            return ['Bill'=> 'Bill', 'Payment' => 'Payment', 'CreditNote' => 'Credit Note', 'DebitNote'=> 'Debit Note'];
        }, function ($values) { // if the filter is active
        
        });
        
    }


    public function report(Request $request)
    {
        $reprot = new CompanyStatementReport();
        $this->crud->hasAccessOrFail('list');
        
        $this->data['crud'] = $this->crud;
        $this->data['title'] = ucfirst($this->crud->entity_name_plural);
        
        // get all entries if AJAX is not enabled
        if (! $this->data['crud']->ajaxTable()) {
            $this->data['entries'] = $reprot->getItems($request);
        }
        
        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getListView(), $this->data);
    }
    
   
    
}