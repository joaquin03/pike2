<?php

namespace App\Http\Controllers\Admin\Accounting\Providers;

use App\Http\Controllers\Admin\Accounting\PaymentCrudController;
use App\Http\Fields\ProviderPaymentFields;
use App\Http\Requests\PaymentRequest as StoreRequest;
use App\Http\Requests\PaymentRequest as UpdateRequest;
use App\Models\Accounting\Payment;
use App\Models\Provider;
use Illuminate\Support\Facades\Redirect;



class PaymentProviderCrudController extends PaymentCrudController
{
    protected $type = 'debit';
    protected $routeCompanyStatement = 'provider-statement';
    
    
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        
        $this->crud->setModel('App\Models\Accounting\Payment');
        $this->crud->setEntityNameStrings('payment', 'payments');
        
        $this->crud->companyId = \Route::current()->parameter('client_id');
        //$this->crud->setRoute(config('backpack.base.route_prefix') .'/clients/'.$this->crud->companyId. '/payments');
        $this->crud->addClause('where', 'company_id', '=', $this->crud->companyId);
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
    
        $this->crud->setRoute(config('backpack.base.route_prefix') .'/providers/'.$this->crud->companyId.'/payments');
        $this->crud->backroute = config('backpack.base.route_prefix') . '/provider-statement/' . $this->crud->companyId;


        $fields = new ProviderPaymentFields();
        $this->crud->addFields($fields->getFormFields(), 'both');
        $this->crud->addColumns($fields->getColumnsFields());
    }
    
}
