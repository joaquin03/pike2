<?php

namespace App\Http\Controllers\Admin\Accounting\Providers;

use App\Events\PaymentDeleted;
use App\Events\PaymentDone;
use App\Http\Controllers\Admin\Contracts\AccountingCrudController;
use App\Http\Controllers\Admin\Traits\NestedRouteBadParameterBug;
use App\Http\Fields\CreditNoteFields;
use App\Http\Requests\PaymentRequest as UpdateRequest;
use App\Http\Requests\PaymentRequest as StoreRequest;


// VALIDATION: change the requests to match your own file names if you need form validation

use App\Http\Fields\ProviderCreditNoteFields;
use App\Models\Accounting\CreditNote;
use Illuminate\Support\Facades\Redirect;

class ProviderCreditNoteCrudController extends AccountingCrudController
{
    
    use NestedRouteBadParameterBug;
    
    protected $type = 'debit';
    protected $routeCompanyStatement = 'provider-statement';
    
    protected function setFields()
    {
        $fields = new ProviderCreditNoteFields();
        $this->crud->addFields($fields->getFormFields(), 'both');
        $this->crud->addColumns($fields->getColumnsFields());
    }
    
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Accounting\CreditNote');
        //$this->crud->setRoute(config('backpack.base.route_prefix') . '/credit-notes');
        $this->crud->setEntityNameStrings('credit note', 'Credit notes');
        
        $this->crud->clientId = \Route::current()->parameter('client_id');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/providers/' . $this->crud->clientId . '/credit-notes');
        $this->crud->addClause('where', 'client_id', '=', $this->crud->clientId);
        $this->crud->backroute = config('backpack.base.route_prefix') . '/provider-statement/' . $this->crud->clientId;
        
        $this->setFields();
        
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        
    }
    
    public function update(UpdateRequest $request)
    {
        $debitNote = CreditNote::findOrFail($request->route('id'));
        $request->merge(['user_id' => \Auth::user()->id]);
        $request->merge(['type' => $this->type]);
        
        event(new PaymentDeleted($debitNote));
        
        
        parent::updateCrud($request);
        
        $debitNote->setAmount();
        event(new PaymentDone($debitNote));
        
        if ($debitNote->save()) {
            event(new PaymentDone($debitNote));
        }
        
        return Redirect::to(route('crud.' . $this->routeCompanyStatement . '.show',
            ['client_id' => $this->crud->clientId]));
    }
    
    public function store(StoreRequest $request, $clientId)
    {
        //Agarrar credit note actual a crear
        $request->merge(['user_id' => \Auth::user()->id]);
        $request->merge(['type' => $this->type]);
        $request->merge(['company_id' => $clientId]);
        
        parent::storeCrud($request);
        $debitNote = $this->data['entry'];
        
        $debitNote->is_credit_note = 1;
        $debitNote->setAmount();
        
        $debitNote->save();
        event(new PaymentDone($debitNote));
        
        
        return Redirect::to(route('crud.' . $this->routeCompanyStatement . '.show', ['client_id' => $clientId]));
    }
    
    public function destroy($id)
    {
        $debitNote = CreditNote::findOrFail($id);
        event(new PaymentDeleted($debitNote));
        
        return parent::destroy($this->getLastParameter());
    }
    
}
