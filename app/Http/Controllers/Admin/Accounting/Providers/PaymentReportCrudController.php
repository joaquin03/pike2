<?php
/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 4/5/19
 * Time: 3:10 PM
 */

namespace App\Http\Controllers\Admin\Accounting\Providers;


use App\Http\Fields\ProviderPaymentFields;
use App\Models\Accounting\Payment;
use App\Models\Provider;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Carbon\Carbon;

class PaymentReportCrudController extends CrudController
{
    protected $type = 'debit';
    protected $routeCompanyStatement = 'provider-statement';
    
    
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        
        $this->crud->setModel('App\Models\Accounting\Payment');
        $this->crud->setEntityNameStrings('payment', 'payments');
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        
        $this->crud->setRoute(config('backpack.base.route_prefix') .'/providers-payments/report');
        //$this->crud->backroute = config('backpack.base.route_prefix') . '/provider-statement/' . $this->crud->companyId;
        
        
        $fields = new ProviderPaymentFields();
        $this->crud->setDefaultPageLength(200);
        $this->crud->removeAllButtons();
        $this->crud->addFields($fields->getFormFields(), 'both');
        $this->crud->addColumns($fields->getColumnsFields());
        $this->crud->addClause('where', 'type', 'debit');
        $this->crud->enableExportButtons();
        
        $this->setFiters();
    }
    
    public function index()
    {
        $this->crud->hasAccessOrFail('list');
        
        $this->data['crud'] = $this->crud;
        $this->data['title'] = ucfirst($this->crud->entity_name_plural);
        
        // get all entries if AJAX is not enabled
        if (! $this->data['crud']->ajaxTable()) {
            $this->data['entries'] = $this->data['crud']->getEntries();
            $this->data['entries_total'] = $this->total($this->data['entries']);
        }
        
        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view('payment.list', $this->data);
    }
    
    
    private function setFiters()
    {
        $this->crud->addFilter([ // dropdown filter
            'name' => 'payment_method',
            'type' => 'select2_multiple',
            'label' => 'Payments Methods'
        ], function () {
            return Payment::getPaymentMethods();
        }, function ($values) { // if the filter is active
            $vals = json_decode($values);
            if (count($vals) > 0) {
                $this->crud->addClause('whereIn', 'payment_method', $vals);
            }
        });
        
        $this->crud->addFilter([ // daterange filter
            'type' => 'date_range',
            'name' => 'date',
            'label'=> 'Date range',
        ],
            false,
            function($value) { // if the filter is active, apply these constraints
                $dates = json_decode($value);
                $this->crud->addClause('where', 'date', '>=', Carbon::createFromFormat('d-m-Y', $dates->from));
                $this->crud->addClause('where', 'date', '<=', Carbon::createFromFormat('d-m-Y', $dates->to));
            }
        );
        
        $this->crud->addFilter([ // dropdown filter
            'name' => 'aircraft_client_id',
            'type' => 'select2_multiple',
            'label' => 'Providers'
        ], function () {
            return Provider::all()->pluck('name', 'id')->toArray();
        }, function ($values) { // if the filter is active
            $vals = json_decode($values);
            if (count($vals) > 0) {
                $this->crud->addClause('whereIn', 'company_id', $vals);
            }
        });
        
    }
    
    private function total($collection)
    {
        $total = 0;
        foreach ($collection as $item) {
            $total += $item->amount;
        }
        
        return $total;
    }
}