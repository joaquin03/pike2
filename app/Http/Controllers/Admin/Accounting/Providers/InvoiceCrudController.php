<?php

namespace App\Http\Controllers\Admin\Accounting\Providers;

use App\Http\Controllers\Admin\Accounting\BillCrudController;
use App\Http\Fields\BillsProcurementFields;
use App\Http\Transformers\Accounting\AccountingServicesTransformer;
use App\Models\Accounting\Invoice;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use App\Http\Fields\BillsFields;
use Illuminate\Support\Facades\Auth;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\InvoiceRequest as StoreRequest;
use App\Http\Requests\InvoiceRequest as UpdateRequest;
use Illuminate\Support\Facades\Redirect;

class InvoiceCrudController extends CrudController
{
    
    protected $type = 'credit';
    protected $routeCompanyStatement = 'provider-statement';
    
    public function setup()
    {

        dd("DONT USE THIS CLASS");
        $this->crud->setModel('App\Models\Accounting\Invoice');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/invoices');
        $this->crud->setEntityNameStrings('Invoice', 'Invoice');

        $this->setFields();

        $this->crud->setRoute(config('backpack.base.route_prefix') . '/bills-provider');
        $this->crud->boxSizeClass = 'col-md-12';


    }
    
    protected function setFields()
    {
        $fields = new BillsProcurementFields();
        $this->crud->addFields($fields->getFormFields(), 'both');
        $this->crud->addColumns($fields->getColumnsFields());
    }

    public function store(StoreRequest $request)
    {

        $request->request->add(['user_id', Auth::id()]);
        $request->request->add(['type', $this->type]);
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return Redirect::to(route('crud.'.$this->routeCompanyStatement.'.show', ['client_id'=>$this->crud->entry->company_id]));
    }
    
    public function update(UpdateRequest $request)
    {
        $request->request->set('is_active', 1);
        
        // your additional operations before save here
        $invoice = Invoice::findOrFail($request->get('id'));
        $invoice->addInvoiceToAccountingService();
        
        $oldInvoice = $invoice->validateIfExistSameInvoice();
        
        if ($oldInvoice->id == $invoice->id) {
            $redirect_location = parent::updateCrud($request);
        }
        
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        
        return Redirect::to(route('crud.'.$this->routeCompanyStatement.'.show', ['client_id'=>$oldInvoice->company_id]));
    }

    public function edit($id)
    {

        $this->crud->hasAccessOrFail('update');

        // get the info for that entry
        $this->data['entry'] = $this->crud->getEntry($id);
        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getUpdateFields($id);
        $this->data['title'] = trans('backpack::crud.edit').' '.$this->crud->entity_name;
        $this->data['id'] = $id;
        $transformer = new AccountingServicesTransformer($this->data['entry']->amount);

        $this->data['accountingServices'] = $transformer->transformItems($this->data['entry']->accountingServices);

        return view('vendor.backpack.crud.views.edit', $this->data);
    }
    
   
}
