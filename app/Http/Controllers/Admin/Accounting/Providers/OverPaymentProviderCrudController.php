<?php

namespace App\Http\Controllers\Admin\Accounting\Providers;

use App\Http\Controllers\Admin\Accounting\OverPaymentCrudController;
use App\Http\Fields\ProviderOverPaymentFields;



class OverPaymentProviderCrudController extends OverPaymentCrudController
{
    protected $type = 'debit';
    protected $routeCompanyStatement = 'provider-statement';
    
    
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        
        $this->crud->setModel('App\Models\Accounting\Payment');
        $this->crud->setEntityNameStrings('Over Payment', 'Over Payments');
        
        $this->crud->companyId = \Route::current()->parameter('client_id');
        $this->crud->addClause('where', 'is_over_payment', '=', '1');
        //$this->crud->setRoute(config('backpack.base.route_prefix') .'/clients/'.$this->crud->companyId. '/payments');
        $this->crud->addClause('where', 'company_id', '=', $this->crud->companyId);
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
    
        $this->crud->setRoute(config('backpack.base.route_prefix') .'/providers/'.$this->crud->companyId.'/over-payments');
        $this->crud->backroute = config('backpack.base.route_prefix') . '/provider-statement/' . $this->crud->companyId;


        $fields = new ProviderOverPaymentFields();
        $this->crud->addFields($fields->getFormFields(), 'both');
        $this->crud->addColumns($fields->getColumnsFields());
    }


    
}
