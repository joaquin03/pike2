<?php

namespace App\Http\Controllers\Admin\Accounting;

use App\Events\PaymentDone;
use App\Exceptions\ValidationException;
use App\Helpers\GarinoGenerator;
use App\Http\Controllers\Admin\Contracts\AccountingCrudController;
use App\Http\Controllers\Admin\Traits\NestedRouteBadParameterBug;
use App\Http\Fields\CreditNoteFields;
use App\Http\Requests\PaymentRequest as StoreRequest;

// VALIDATION: change the requests to match your own file names if you need form validation

use App\Models\Accounting\Bill;
use App\Models\Accounting\CreditNote;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;


class CreditNoteCrudController extends AccountingCrudController
{
    
    use NestedRouteBadParameterBug;
    
    protected $type = 'credit';
    protected $routeCompanyStatement = 'client-statement';
    protected $clientId = 'client-statement';
    
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Accounting\CreditNote');
        //$this->crud->setRoute(config('backpack.base.route_prefix') . '/credit-notes');
        $this->crud->setEntityNameStrings('credit note', 'Credit notes');
        
        $this->crud->companyId = \Route::current()->parameter('client_id');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/clients/' . $this->crud->companyId . '/credit-notes');
        $this->crud->addClause('where', 'company_id', '=', $this->crud->companyId);
        $this->crud->backroute = config('backpack.base.route_prefix') . '/client-statement/' . $this->crud->companyId;
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $fields = new CreditNoteFields();
        $this->crud->addFields($fields->getFormFields(), 'both');
        $this->crud->addColumns($fields->getColumnsFields());
    }
    
    public function store(StoreRequest $request, $clientId)
    {
        //Agarrar credit note actual a crear
        $request->merge(['user_id'      => \Auth::user()->id]);
        $request->merge(['type'         => $this->type]);
        $request->merge(['company_id'   => $clientId]);
        
        parent::storeCrud($request);
        $debitNote = $this->data['entry'];
        $debitNote->is_credit_note = 1;

        $bills = $request->get('bills');

        $this->setDefaultAssignedBills($debitNote);

        if (!$bills){
            return Redirect::back()->withErrors('Error, can not create credit note without any bill.');
        }

        $debitNote->setAmount();
        $data = $debitNote->generateGarino($bills);


        if(is_string($data)) //devuelve el string del rechazo
        {
            return Redirect::back()->withErrors($data);
        }

        event(new PaymentDone($debitNote));
    
        $debitNote->save();

        return \Redirect::to($this->crud->route.'/'.$this->crud->entry->getKey().'/show');
    }
    
    public function show($id)
    {
        $debitNoteId = \Route::current()->parameter('credit_note_id');
        $this->data['creditNote'] = $this->crud->getEntry($debitNoteId);
        $this->data['clientId'] = $id;
        $this->data['id'] = $debitNoteId;
    
        return view('accounting.creditNoteShow', $this->data);
    }

    public function setDefaultAssignedbills($debitNote)
    {
        foreach ($debitNote->billsAndDebitNotes as $bill){
            $bill->pivot->assigned_bill_id = $bill->pivot->bill_id;
            $bill->pivot->save();
        }
    }

    public function updateAssignedBill($id, Request $request)
    {
        $oldBills = $request['old-bills'];
        $discounts = $request['discount'];
        $assignBills = $request['assign-bills'];

        $debitNote = CreditNote::findOrFail($id);
        $debitNote->reAssignBills($oldBills, $discounts, $assignBills);

        return Redirect::to(route('crud.'.$this->routeCompanyStatement.'.show', ['client_id'=>$debitNote->billingCompany->id]));
    }


    
}
