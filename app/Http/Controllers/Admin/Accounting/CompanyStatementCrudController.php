<?php
/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 3/22/18
 * Time: 1:18 PM
 */

namespace App\Http\Controllers\Admin\Accounting;


use App\Http\Transformers\Accounting\CompanyStatementTransformer;
use Backpack\CRUD\app\Http\Controllers\CrudController;


use App\Models\Accounting\CreditNote;
use Illuminate\Http\Request;
use App\Models\Accounting\Bill;
use App\Models\Accounting\Payment;
use Excel;

class CompanyStatementCrudController extends CrudController
{
    
    protected $routeCompanyStatement = '/client-statement';
    protected $companyType = 'clients';
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Accounting\CompanyStatement');
        $this->crud->setRoute(config('backpack.base.route_prefix') . $this->routeCompanyStatement);
        $this->crud->setEntityNameStrings('client Statement', 'clients Statements');
    
        $this->crud->addClause('orderBy', 'name');
        
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
      
        $this->crud->setShowView('accounting.show');
        $this->crud->allowAccess('show');
        $this->crud->denyAccess(['create', 'edit', 'delete']);
        
        $this->crud->addColumns($this->addColumns());
        
    }
    
    protected function addColumns()
    {
        
        return [
            ['name' => 'name', 'type'=> 'link_item', 'link'=> url('/admin'.$this->routeCompanyStatement)],
            ['name' => 'country', 'type' => 'model_function', 'function_name' =>'getCountry'],
            ['name' => 'business'],
            ['name' => 'updated_at', 'label'=>'Latest activity', 'type' => 'model_function', 'function_name' =>'lastActivityDate'],
        ];
    }
    
    
    public function show($id)
    {
        $this->crud->addClause('where', 'is_active','=','1');
        
        $this->crud->hasAccessOrFail('show');
        
        // get the info for that entry
        $company = $this->crud->getEntry($id);
        $transformer = new CompanyStatementTransformer($this->companyType);

        $data = $transformer->transform($company);
        return view('accounting.statementShow', $data);
    }

    public function exportTable($id)
    {
        $this->crud->hasAccessOrFail('show');

        $company = $this->crud->getEntry($id);
        $transformer = new CompanyStatementTransformer($this->companyType);
        $data = $transformer->transform($company);

        Excel::create('Client '.$this->crud->getEntry($id)->name, function($excel) use ($data) {
            $excel->sheet('New sheet', function($sheet) use ($data) {
                $sheet->loadView('accounting.exportableTables.clientStatement')->with($data);
                $sheet->setOrientation('landscape');
            });
        })->export('xls');
//        return view('accounting.exportableTables.clientStatement', $data);

    }

}
