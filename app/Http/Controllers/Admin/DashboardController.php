<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ActivityReminder;
use Auth;

class DashboardController extends Controller
{
    protected $reminder;
    
    
    public function __construct(ActivityReminder $reminder)
    {
        $this->reminder = $reminder;
    }
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allReminders = $this->reminder->thisWeek()->get();
        $contactReminders = [];
        
        foreach ($allReminders as $reminder) {
            if (Auth::user()->id == $reminder->assign_to_user_id) {
                array_push($contactReminders, $reminder);
            }
        }
        
        return view('dashboard', compact('contactReminders'));
    }
    
}
