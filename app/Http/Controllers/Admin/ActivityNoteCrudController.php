<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Contracts\ActivityCrudController;
use App\Http\Fields\ActivityNoteFields;
use App\Http\Requests\ActivityNoteRequest;


class ActivityNoteCrudController extends ActivityCrudController
{
    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\ActivityNote');
        $this->crud->setEntityNameStrings('Activity Note', 'Activity Notes');
    
        
        $this->activitySetUp(new ActivityNoteFields(), new ActivityNoteRequest());
        $this->crud->setRoute("admin/company/".$this->crud->companyId."/activity-note");
        
    }
}
