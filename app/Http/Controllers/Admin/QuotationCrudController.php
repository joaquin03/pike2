<?php

namespace App\Http\Controllers\Admin;

use App\Http\Fields\QuotationFields;
use App\Http\Requests\GenericOperationRequest;
use App\Http\Requests\QuotationRequest;
use App\Http\Requests\StoreOperationPermission;
use App\Http\Requests\UpdateOperationPermission;
use App\Http\Transformers\Procurement\PermitsTransformer;
use App\Http\Transformers\Quotation\ProvidersItineraryServicesTransformer;
use App\Http\Transformers\Quotation\QuotationProvidersItineraryServicesTransformer;
use App\Models\Airport;
use App\Models\Itinerary;
use App\Models\Operation;
use App\Models\Quotation;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class QuotationCrudController extends OperationCrudController
{
    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Quotation');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/quotation');
        $this->crud->setEntityNameStrings('quotation', 'quotations');


        $fields = new QuotationFields();
        $this->crud->addFields($fields->getFormFields(), 'both');
        $this->crud->addColumns($fields->getColumnsFields());
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->removeButton('update');
        $this->crud->removeButton('delete');


    }

    public function index()
    {
//        if(!Auth::user()->can('Quotation')){
//            return redirect('/admin/dashboard')->withErrors( 'Error! Quotation not allowed. Contact your admin for permissions.');
//        }
        $sorted = Quotation::sortByPNDescending();

        $status = $this->crud->request->has('status') ? $this->crud->request->all()['status'] : '';
        if ($status != '') {
            foreach ($sorted as $operation) {
                if ($operation['attributes']['state'] != $status) {
                    $key = array_search($operation, $sorted);
                    array_pull($sorted, $key);
                }
            }
        }
        $this->crud->hasAccessOrFail('list');
        $this->data['crud'] = $this->crud;
        $this->data['title'] = ucfirst($this->crud->entity_name_plural);
        $this->data['entries'] = $sorted;

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getListView(), $this->data);
    }

    public function create()
    {
        $this->data['crud'] = $this->crud;
        $this->data['operation'] = new Quotation();
        $this->data['creating'] = true;


        return view('quotation.create', $this->data);
    }

    public function store(GenericOperationRequest $request)
    {

        $quotation = new Quotation();
        if (auth()->user()->can('update', $quotation)) {

            $quotation->creator_user_id = $request->user()->id;

            $quotation->in_charge_user_id = $quotation->aircraftClient->salesManager->id ?? env('QUOTATION_DEPARTMENT');

            $quotation->fill($request->all());

            //Sales Manager asignado, de lo contrario, 'Quotations'

            $quotation->is_quotation = 1;

            $quotation->save();

            return redirect()->route('crud.quotation.edit', ['quotation_id' => $quotation->id]);
        }
        return redirect()->back()->withErrors('You don´t have the permissions to edit this quotation.');
    }

    public function edit($id)
    {
        $this->data['crud'] = $this->crud;
        $this->data['operation'] = Quotation::withoutGlobalScopes()->findOrFail($id);
        $this->data['airports'] = $this->data['operation']->itineraryAirports();
        $this->data['providers'] = Airport::providersByOperationIcaos($this->data['airports']);
        $this->data['total'] = $this->data['operation']->getTotal();

        $permits = Operation::withoutGlobalScopes()->findOrFail($id)->aircraftPermission()->where('is_ovf', 0)->get();
        $permitsTransformer = new PermitsTransformer();
        $this->data['permits'] = $permitsTransformer->transformItems($permits);

        return view('quotation.edit', $this->data);
    }

    public function showItinerary($itineraryId)
    {
        $itinerary = Itinerary::findOrFail($itineraryId);

        $transformer = new QuotationProvidersItineraryServicesTransformer($itinerary);

        $providers = $transformer->transformItems($itinerary->getProviderOfServiceChilds());

        return view('quotation.modals.itineraryShow', compact('itinerary', 'providers'));
    }

    public function update(Request $request, $id)
    {
        $quotation = Quotation::withoutGlobalScopes()->findOrFail($id);
        $quotation->in_charge_user_id = $quotation->aircraftClient->salesManager->id ?? env('QUOTATION_DEPARTMENT');
        $quotation->fill($request->all());
        $quotation->setOperationCustomStartDate(true);

        $quotation->save();

        return redirect()->route('crud.quotation.edit', ['operation_id' => $quotation->id]);
    }

    public function createOperationFromQuotation(Request $request)
    {
        $quotation = Quotation::findOrFail($request->quotation_id);
        $operation_to_assign = $request->operation_id;

        if (!$operation_to_assign) {
            $operation_to_assign = $quotation->createOperation();
        } else {
            $quotation->assignOperation($operation_to_assign);
        }

        return redirect()->route('crud.operation.edit', ['operation_id' => $operation_to_assign]);
    }

    public function integrateWithGoogleSheet($itineraryId, Request $request)
    {

        $itinerary = Itinerary::findOrFail($itineraryId);
        if (in_array($itinerary->getCountryCode(), config('constants.allowed_quotation_countries'))) {
            return view('quotation.sheetIntegration.' . $itinerary->getCountryCode(),
                compact('itinerary', 'request'));
        }
        return redirect('/admin/quotation/' . $itinerary->operation_id . '/edit')
            ->withErrors('Error! Country not available to generate services.');
    }

    public function uploadRouteMap(Request $request)
    {
        $quotation = Quotation::findOrFail($request->quotation_id);

        return view('quotation.exportable.quotation', compact('quotation'));
    }

    public function saveQuotationDocument($quotation_id, Request $request)
    {

        $quotation = Quotation::findOrFail($quotation_id);
        $document = $request->htmlToSave;
        $filePath = $quotation->saveDocument($document);


        return redirect('/admin/quotation/' . $quotation_id . '/edit');
    }

    public function showQuotationDocument($quotation_id)
    {
        $quotation = Quotation::findOrFail($quotation_id);
        $url = url('/') . '/' . $quotation->extraData->quotation_exportable_file;

        $document = readfile($url);
//        $document = readfile("http://$_SERVER[HTTP_HOST]/".$quotation->extraData->quotation_exportable_file);

        return view('quotation.exportable.showDocument', compact('document'));

    }

    /*--------------------------------------------------------*/
    /*------------------    PERMISSIONS    -------------------*/
    /*--------------------------------------------------------*/

    /*Permissions */
    public function createPermission($operation_id, $is_additional = null)
    {
        $operation = Quotation::withoutGlobalScopes()->findOrFail($operation_id);
        $airports = $operation->itineraryAirports();
        $this->data['providers'] = Airport::providersByOperationIcaos($airports);
        $providers = $this->data['providers'];

        return view('quotation.modals.permission', compact('operation', 'providers', 'airports'));
    }

    public function storePermission(StoreOperationPermission $request, $operation_id)
    {
        $operation = Quotation::withoutGlobalScopes()->findOrFail($operation_id);

        $operation->addPermission($request);

        $operationTransformer = new PermitsTransformer();
        $permits = $operationTransformer->transformItems($operation->aircraftPermission);
        return response()->json([
            'message' => 'Permit added',
            'data' => ['permissions' => $permits],
            201
        ]);
    }

    public function updateOperationPermission(UpdateOperationPermission $request, $operation_id)
    {
        $operation = Operation::withoutGlobalScopes()->findOrFail($operation_id);
        $operation->updateOperationPermission($request->all());

        $operationTransformer = new PermitsTransformer();
        $permits = $operationTransformer->transformItems($operation->aircraftPermission);

        return response()->json([
            'message' => 'Permit edited',
            'data' => ['permissions' => $permits],
            201

        ]);
    }

    public function deletePermission($operation_id, $permission_id)
    {
        AircraftPermission::findOrFail($permission_id)->delete();
        $operationTransformer = new PermitsTransformer();
        $operation = Operation::withoutGlobalScopes()->findOrFail($operation_id);

        $permits = $operationTransformer->transformItems($operation->aircraftPermission);

        return response()->json([
            'message' => 'Permit edited',
            'data' => ['permissions' => $permits],
            201

        ]);
    }


    /*--------------------------------------------------------*/
    /*------------------  END PERMISSIONS  -------------------*/
    /*--------------------------------------------------------*/


}
