<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\FuelPrices;
use App\Models\FuelUSA;
use App\Models\Itinerary;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Illuminate\Http\Request;

class FuelPriceController extends CrudController
{
    public function setup()
    {
        
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\FuelPrices');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/fuel-price');
        $this->crud->setEntityNameStrings('Fuel Price', 'Fuel Price');
        
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
    }
    
    
    public function index2(Request $request)
    {
        $this->crud->hasAccessOrFail('list');
    
        $this->data['crud'] = $this->crud;
        $this->data['title'] = ucfirst($this->crud->entity_name_plural);
    
        $this->data['providerValues'] = [];
    
        if ($request->has('icao')) {
            $this->data['providerValues'] = FuelPrices::whereIcao($request->get('icao'))
                ->lastBat()->orderByPrice()->get();
        }
        $this->data['icao'] = $request->get('icao', '');
        
        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view('fuelPrices.load', $this->data);
    }
    
    public function uploadExcel(Request $request)
    {
        $this->validate($request, [
            'fuel_airports' => 'required',
            'provider'      => 'required'
        ]);
        
        $loader = FuelPrices::getLoaderClass($request->get('provider'));

        $loader->load($request->file('fuel_airports'));
        
        \Alert::success('File Uploaded')->flash();
        return redirect()->back();
    }
    
    public function index3(Request $request)
    {
        $this->crud->hasAccessOrFail('list');
        
        $this->data['crud'] = $this->crud;
        $this->data['title'] = ucfirst($this->crud->entity_name_plural);
        
        $this->data['providerValues'] = [];
        
        if ($request->has('icao')) {
            $this->data['providerValues'] = FuelPrices::whereIcao($request->get('icao'))
                ->lastBat()->orderByPrice()->get();
        }
        $this->data['icao'] = $request->get('icao', '');
        
        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view('fuelPrices.show', $this->data);
    }
    
    
    public function showFuelItinerary($itinerary)
    {
        $itinerary = Itinerary::findOrFail($itinerary);
        
        $providers =  FuelPrices::whereIcao($itinerary->airport_icao_string)
                            ->lastBat()->orderByPrice()->get();
        
        $client = $itinerary->operation->aircraftClient;
        
        return view('fuelPrices.modals.FuelPrice', compact('providers', 'client'));
    }
    
}
