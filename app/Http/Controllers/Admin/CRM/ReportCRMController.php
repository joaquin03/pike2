<?php

namespace App\Http\Controllers\Admin\CRM;

use App\Http\Fields\ReportCRMFields;
use App\Models\User;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Carbon\Carbon;
use OwenIt\Auditing\Models\Audit;


class ReportCRMController extends CrudController
{
    public function setup()
    {
        
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Audit');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/report-crm');
        $this->crud->setEntityNameStrings('report', 'Reports CRM');
    
        $this->crud->addClause('inCRM');
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
    
        $this->crud->allowAccess('details_row');
        $this->crud->enableDetailsRow();
        $this->crud->setDetailsRowView('CRM.details_row_report');
    
        $this->crud->enableExportButtons();
        
        $fields = new ReportCRMFields();
        $this->crud->addFields($fields->getFormFields(), 'both');
        $this->crud->addColumns($fields->getColumnsFields());
        
        $this->crud->removeAllButtonsFromStack('line');
        
        $this->crud->enableExportButtons();
        
        $this->setFiters();
        
    }
    
    private function setFiters()
    {
        $this->crud->addFilter([ // dropdown filter
            'name' => 'auditable_type',
            'type' => 'select2_multiple',
            'label' => 'Elements'
        ], function () {
            return [
                'App\Models\Company' => 'Company',
                'App\Models\ActivityEmail' => 'Emails',
                'App\Models\ActivityCall' => 'Call',
                'App\Models\ActivityFile' => 'File',
                'App\Models\ActivityMeeting' => 'Meetings',
                'App\Models\ActivityNote' => 'Notes',
                'App\Models\ActivitySpecialFeatures' => 'Special Features',
                'App\Models\ActivityReminder' => 'Reminders',
            ];
        }, function ($values) { // if the filter is active
            $vals = json_decode($values);
            if (count($vals) > 0) {
                $this->crud->addClause('whereIn', 'auditable_type', $vals);
            }
        });
        $this->crud->addFilter([ // dropdown filter
            'name' => 'event',
            'type' => 'select2_multiple',
            'label' => 'Action'
        ], function () {
            return [
                'created' => 'Created',
                'updated' => 'Updated',
                'deleted' => 'Deleted',
            ];
        }, function ($values) { // if the filter is active
            $vals = json_decode($values);
            if (count($vals) > 0) {
                $this->crud->addClause('whereIn', 'event', $vals);
            }
        });
    
        $this->crud->addFilter([ // dropdown filter
            'name' => 'user_id',
            'type' => 'select2_multiple',
            'label' => 'Users'
        ], function () {
            return User::all()->pluck('name', 'id')->toArray();
        }, function ($values) { // if the filter is active
            $vals = json_decode($values);
            if (count($vals) > 0) {
                $this->crud->addClause('whereIn', 'user_id', $vals);
            }
        });
    
        $this->crud->addFilter([ // daterange filter
            'type' => 'date_range',
            'name' => 'from_to',
            'label'=> 'Date range',
        ],
            false,
            function($value) { // if the filter is active, apply these constraints
                $dates = json_decode($value);
                $this->crud->addClause('where', 'created_at', '>=', Carbon::createFromFormat('d-m-Y', $dates->from));
                $this->crud->addClause('where', 'created_at', '<=', Carbon::createFromFormat('d-m-Y', $dates->to));
            });
    }
    
   
}
