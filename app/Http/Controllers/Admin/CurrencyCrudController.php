<?php

namespace App\Http\Controllers\Admin;

use App\Models\Currency;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\CurrencyRequest as StoreRequest;
use App\Http\Requests\CurrencyRequest as UpdateRequest;

class CurrencyCrudController extends CrudController
{
    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Currency');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/currency');
        $this->crud->setEntityNameStrings('Currency', 'Currencies');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */


        $this->crud->setColumns(['currency']);
        $this->crud->addField([
            'name' => 'currency',
            'type' => 'text',
            'label' => "Currency"
        ]);
    }

    public function store(StoreRequest $request)
    {
        $currency = new Currency();
        $currency->currency = $request->currency;
        $currency->save();

        return ['id' => $currency->id, 'name' => $currency->currency ];
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
