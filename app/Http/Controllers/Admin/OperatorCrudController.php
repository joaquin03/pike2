<?php

namespace App\Http\Controllers\Admin;

use App\Http\Fields\CompanyFields;
use App\Http\Fields\OperatorFields;
use App\Models\Operator;

class OperatorCrudController extends CompanyCrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Operator');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/operator');
        $this->crud->setEntityNameStrings('operator', 'operators');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->removeButton('create');
    
        $fields = new OperatorFields();
        $this->crud->addFields($fields->getFormFields(), 'both');
        $this->crud->addColumns($fields->getColumnsFields());
    
        $this->crud->removeButton('update');
        $this->crud->removeButton('revisions');
    
        $this->crud->addButtonFromModelFunction('line', 'edit_button', 'editLink', 'beginning');
        $this->crud->addButtonFromModelFunction('line', 'info_button', 'showLink', 'beginning');
    }
    
    
    
}
