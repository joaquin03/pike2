<?php

namespace App\Http\Controllers\Admin;

use App\Http\Fields\ServiceFields;
use App\Http\Requests\AddPriceToProviderRequest;
use App\Http\Requests\AddServiceToProviderRequest;

use App\Models\Provider;
use App\Models\Service;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ServiceRequest as StoreRequest;
use App\Http\Requests\ServiceRequest as UpdateRequest;

class ServiceCrudController extends CrudController
{
    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Service');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/service');
        $this->crud->setEntityNameStrings('service', 'services');
        
        $fields = new ServiceFields();
        $this->crud->addFields($fields->getFormFields(), 'both');
        $this->crud->addColumns($fields->getColumnsFields());
        $this->crud->enableExportButtons();

        $this->crud->addClause('where', 'name', '!=', 'Default');
    
    }
    
    public function addServiceToProvider(AddServiceToProviderRequest $request, $company_id)
    {
        $provider = Provider::findOrFail($company_id);
        $service = Service::findOrFail($request->get('service_id'));

        $ret = $service->addServiceToProvider($provider, $request->all());
        if ($ret){
            \Alert::success('Service added')->flash();
            redirect()->back()->with('success', ['Service added']);
        } else {
            \Alert::error('Service already exits')->flash();
            redirect()->back()->with('error', ['Service already exits']);
        }
        
        return back();
    }
    
    public function deleteServiceToProvider($company_id, $service_id)
    {
        $provider = Provider::findOrFail($company_id);
        $service = Service::findOrFail($service_id);
        
        $service->deleteServiceOfProvider($provider);
        \Alert::success('Service deleted')->flash();
        return back();
    }


    
}