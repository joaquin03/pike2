<?php

namespace App\Http\Controllers\Admin;

use App\Http\Fields\CompanyExtraFields;
use App\Http\Fields\CompanyFields;
use App\Http\Fields\ProviderFields;
use App\Models\Company;
use App\Models\Provider;
use App\Models\User;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use \App\Events\RegisterProvider;


// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\CompanyRequest as StoreRequest;
use App\Http\Requests\CompanyRequest as UpdateRequest;
use Illuminate\Support\Facades\Auth;

class CompanyCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Company');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/company');
        $this->crud->setEntityNameStrings('company', 'companies');


        $this->crud->allowAccess('revisions');
        $this->crud->with('revisionHistory');

        //$this->crud->enableDetailsRow();
        $this->crud->allowAccess('details_row');

        $this->crud->enableExportButtons();

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $fields = new CompanyFields();
        $this->crud->addFields($fields->getFormFields(), 'both');
        $this->crud->addColumns($fields->getColumnsFields());
        $this->crud->removeButton('update');
        $this->crud->addButtonFromModelFunction('line', 'info_button', 'editLink', 'beginning');

    }

    protected function companySetUp()
    {
        $fields = new ProviderFields();
        $this->crud->addFields($fields->getFormFields(), 'both');
        $this->crud->addColumns($fields->getColumnsFields());

        $this->crud->removeButton('update');

        $this->crud->addButtonFromModelFunction('line', 'edit_button', 'editLink', 'beginning');
        $this->crud->addButtonFromModelFunction('line', 'info_button', 'showLink', 'beginning');

    }

    public function show($id)
    {
        //$this->crud->hasAccessOrFail('show');

        // get the info for that entry
        $this->data['company'] = $this->crud->getEntry($id);
        $this->data['activities'] = $this->data['company']->activities()->withOutSpecialFeatures()->publicOrMine()->orderByDate()->get();
        $this->data['activitiesFile'] = $this->data['company']->activitiesFile()->publicOrMine()->orderByDate()->get();
        $this->data['activitiesSpecialFeatures'] = $this->data['company']->activitiesSpecialFeatures()->publicOrMine()->orderByDate()->get();
        $this->data['contacts'] = $this->data['company']->contacts;

        $this->crud->hasAccessOrFail('update');

        // get the info for that entry
        $this->data['entry'] = $this->crud->getEntry($id);
        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getUpdateFields($id);
        $this->data['title'] = trans('backpack::crud.edit') . ' ' . $this->crud->entity_name;

        $this->data['id'] = $id;

        return view('company.show', $this->data);
    }

    public function info($id)
    {
        $company = Company::findOrFail($id);
        return response()->json($company);

    }

    public function edit($id)
    {
        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view('company.edit', $this->data);
    }

    public function store(StoreRequest $request)
    {
        $request->request->add(['creator_user_id' => Auth::user()->id]);
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        if ($this->crud->entry->is_provider) {
            event(new RegisterProvider(Provider::findOrFail($this->crud->entry->id)));
        }
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        if ($this->crud->entry->is_provider) {
            event(new RegisterProvider(Provider::findOrFail($this->crud->entry->id)));
        }

        return $this->show($request->id);
    }

    public function listRevisions($id)
    {

        $this->crud->hasAccessOrFail('revisions');

        // get the info for that entry
        $this->data['entry'] = $this->crud->getEntry($id);
        $this->data['crud'] = $this->crud;
        $this->data['title'] = ucfirst($this->crud->entity_name) . ' ' . trans('backpack::crud.revisions');
        $this->data['id'] = $id;
        $this->data['revisions'] = $this->data['entry']->getRevisions(new Company());
        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view('revision.revisions', $this->data);
    }


    public function performSaveAction($itemId = null)
    {
        $saveAction = \Request::input('save_action', config('backpack.crud.default_save_action', 'save_and_back'));
        $itemId = $itemId ? $itemId : \Request::input('id');

        switch ($saveAction) {
            case 'save_and_new':
                $redirectUrl = $this->crud->route . '/create';
                break;
            case 'save_and_edit':
                $redirectUrl = $this->crud->route . '/' . $itemId . '#edit';
                if (\Request::has('locale')) {
                    $redirectUrl .= '?locale=' . \Request::input('locale');
                }
                break;
            case 'save_and_back':
            default:
                $redirectUrl = $this->crud->route;
                break;
        }

        return \Redirect::to($redirectUrl);
    }

    public function alertSpecialFeatures($company_id)
    {

        $company = Company::findOrFail($company_id);
        return response()->json([
            'special_features' => (isset($company)) ? $company->has_special_features : null,
        ], 200);
    }

    public function addCompetitor($company_id, $competitor_id)
    {
        $company = Company::findOrFail($company_id);
        $company->competitors()->attach($competitor_id);
        $competitor = Company::findOrFail($competitor_id);

        return ['id' => $competitor_id, 'name' => $competitor->name, 'country' => $competitor->getCountry(), 'business' => $competitor->business];
    }

    public function deleteCompetitor($company_id, $competitor_id)
    {
        $company = Company::findOrFail($company_id);
        $company->competitors()->detach($competitor_id);
        $company->competitorsable()->detach($competitor_id);
    }
}


