<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ReportsGenerator;
use App\Http\Fields\ProcurementFields;
use App\Http\Transformers\Procurement\AdditionalTransformer;
use App\Http\Transformers\Procurement\ItineraryProvidersTransformer;
use App\Http\Transformers\Procurement\ItineraryServiceAdditionalsTransformer;
use App\Http\Transformers\Procurement\ItineraryServicesAdditionalsTransformer;
use App\Http\Transformers\Procurement\ProviderPermanentPermissionAdditionalsTransformer;
use App\Http\Transformers\Procurement\ProviderPermissionAdditionalsTransformer;
use App\Http\Transformers\Procurement\ProviderAdditionalTransformer;
use App\Http\Transformers\Procurement\ProviderPermanentPermitsTransformer;
use App\Http\Transformers\Procurement\ProviderPermitsTransformer;
use App\Http\Transformers\Procurement\ProvidersItineraryServicesTransformer;
use App\Http\Transformers\ProcurementIndexTransformer;
use App\Http\Transformers\Screen\OperationScreenTransformer;
use App\Models\Accounting\AccountingService;
use App\Models\Aircraft;
use App\Models\Airport;
use App\Models\Itinerary;
use App\Models\Procurement;
// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ProcurementRequest as StoreRequest;
use App\Http\Requests\ProcurementRequest as UpdateRequest;
use App\Models\Provider;
use App\Models\ServiceParent;
use App\Models\Utilities\Country;
use App\Searcher\OperationSearch;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProcurementCrudController extends OperationCrudController
{
    
    /**
     * Paginate the given query.
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     *
     * @throws \InvalidArgumentException
     */
    public function setup()
    {
        
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Procurement');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/procurement');
        $this->crud->setEntityNameStrings('procurement', 'procurements');
        
        $this->crud->removeButton('delete');
        $this->crud->removeButton('create');
    
    
        $fields = new ProcurementFields();
        $this->crud->addFields($fields->getFormFields(), 'both');
        $this->crud->addColumns($fields->getColumnsFields());
        
        
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */

        $this->crud->getListView('procurement.index');
        $this->crud->setEditView('procurement.edit');
    }
    
    public function index()
    {
        if(!Auth::user()->can('Procurement')){
            return redirect('/admin/dashboard')->withErrors( 'Error! Procurement not allowed. Contact your admin for permissions.');
        }

        $sorted  = Procurement::sortByState()->paginate(50);


        $this->crud->hasAccessOrFail('list');
        $this->data['crud'] = $this->crud;
        $this->data['title'] = ucfirst($this->crud->entity_name_plural);

        $transformer = new ProcurementIndexTransformer();
        $this->data['items'] = $transformer->transformItems($sorted);


        return view('procurement.index',  $this->data);
    }



    
    public function edit($id)
    {
        //Additionals
        $providerPermissionsAdditionalsTransformer = new ProviderPermissionAdditionalsTransformer();
        $providerPermanentPermissionsAdditionalsTransformer = new ProviderPermanentPermissionAdditionalsTransformer();
 
    
        $this->data['crud'] = $this->crud;
        $this->data['operation'] = Procurement::findOrFail($id);
        $this->data['airports'] = $this->data['operation']->itineraryAirports();
        $this->data['providers'] =  Airport::providersByOperationIcaos($this->data['airports']);
        
        $providerPermitsTransformer = new ProviderPermitsTransformer();
        $this->data['providerPermits'] = $providerPermitsTransformer->transform($this->data['operation']);
        
        $providerPermanentPermitsTransformer = new ProviderPermanentPermitsTransformer();
        $this->data['providerPermanentPermits'] = $providerPermanentPermitsTransformer->transform($this->data['operation']);
        
        //Additionals
        $this->data['permissionsAdditionals'] = $providerPermissionsAdditionalsTransformer->transform($this->data['operation']);
        $this->data['permanentPermissionsAdditionals'] = $providerPermanentPermissionsAdditionalsTransformer->transform($this->data['operation']);
        
        
        $this->data['total'] =  $this->data['operation']->getTotal();
        return view('procurement.edit', $this->data);
    }
    
    public function update(Request $request, $id)
    {
        $procurement = Procurement::findOrFail($id);
        $procurement->fill($request->all());
        $procurement->save();
        
        return redirect()->route('crud.procurement.edit', ['operation_id' => $procurement->id]);
    }
    
    
    public function getTotalCost($id)
    {
        $procurement = Procurement::find($id);

        $total = $procurement->getTotal();
        return view('procurement.modals.total-cost', compact('total',$total));
    }


    public function showItinerary($itineraryId)
    {
        $itinerary = Itinerary::findOrFail($itineraryId);

        $transformer = new ProvidersItineraryServicesTransformer($itinerary);
        
        $providers = $transformer->transformItems($itinerary->getProviderOfServices());
        
        $itineraryServicesAdditionalsTransformer = new ItineraryServicesAdditionalsTransformer();
        $servicesAdditionals = $itineraryServicesAdditionalsTransformer->transform($itinerary);

        return view('procurement.modals.itineraryShow', compact('itinerary', 'providers', 'servicesAdditionals'));
    }
    
    

    public function filter(Request $request)
    {
        $searcher = new OperationSearch(new Procurement());
        
        $transformer = new ProcurementIndexTransformer();
        $this->data['crud'] = $this->crud;
        $this->data['items'] = $transformer->transformItems($searcher->apply($request));
        
        return $this->data;
    }

    
    
    public function report()
    {
        $aircraft = Aircraft::all();
        $countries = Country::all();
        $airports = Airport::all();
        $providers = Provider::all();
        $services = ServiceParent::all();
        
        
        return view('reports.procurementReport', compact('aircraft', 'countries', 'airports', 'providers', 'services'));
    }
    

}
