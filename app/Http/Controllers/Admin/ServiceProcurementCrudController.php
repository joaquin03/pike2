<?php

namespace App\Http\Controllers\Admin;

use App\Http\Fields\ServiceFields;

use App\Http\Fields\SubServiceFields;
use App\Models\Service;
use \App\Events\RegisterProcurementService;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ServiceProcurementRequest as StoreRequest;
use App\Http\Requests\ServiceProcurementRequest as UpdateRequest;

class ServiceProcurementCrudController extends ServiceCrudController
{
    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Service');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/service-procurement');
        $this->crud->setEntityNameStrings('service pocurement', 'services procurement');
        $this->crud->enableExportButtons();

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        
        $fields = new SubServiceFields();
        $this->crud->addFields($fields->getFormFields(), 'both');
        $this->crud->addColumns($fields->getColumnsFields('service-procurement'));

    }

    public function store(StoreRequest $request)
    {

        // your additional operations before save here
        $redirect_location = parent::storeCrud();
        $service = Service::findOrFail($this->crud->entry->id);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        event(new RegisterProcurementService($service));
        return $redirect_location;
    }
    
    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
    
    
    
}