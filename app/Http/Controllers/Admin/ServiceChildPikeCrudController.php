<?php

namespace App\Http\Controllers\Admin;

use App\Http\Fields\PikeServiceFields;
use App\Http\Requests\AddServiceToProviderRequest;

use App\Models\Provider;
use App\Models\Service;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use App\Models\ServiceChildPike;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ServiceProcurementRequest as StoreRequest;
use App\Http\Requests\ServiceProcurementRequest as UpdateRequest;

class ServiceChildPikeCrudController extends CrudController
{
    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\ServiceChildPike');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/service-pike');
        $this->crud->setEntityNameStrings('service', 'services');

        $fields = new PikeServiceFields();
        $this->crud->addFields($fields->getFormFields(), 'both');
        $this->crud->addColumns($fields->getColumnsFields('service-pike'));
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $request->request->add(['is_from_pike' => 1]);
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }






}