<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PusherController extends Controller
{
    protected $pusher;

    public function __construct() {
        $this->pusher = new \Pusher(\Config::get('pusher.auth_key'), \Config::get('pusher.secret'), \Config::get('pusher.app_id'));
    }


    public function postAuth(Request $request)
    {
        //We see if the user is logged in our laravel application.
        if (\Auth::check()) {
            $user =  \Auth::user();
            $presence_data = array('name' => $user->name);

            $response = $this->pusher->presence_auth($request->get('channel_name'), $request->get('socket_id'), $user->id, $presence_data);

            return response()->json(json_decode($response, true), 200);

        }
        else {
            return Response::make('Forbidden',403);
        }
    }
}
