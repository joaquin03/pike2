<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Contracts\ActivityCrudController;
use App\Http\Fields\ActivityEmailFields;
// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ActivityEmailRequest;

class ActivityEmailCrudController extends ActivityCrudController
{
    
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\ActivityEmail');
        
        $this->crud->setEntityNameStrings('Activity Email', 'Activity Emails');
        
        
        $this->activitySetUp(new ActivityEmailFields(), new ActivityEmailRequest());
        
        
        $this->crud->setRoute("admin/company/" . $this->crud->companyId . '/activity-email');
        
    }
    
    
}
