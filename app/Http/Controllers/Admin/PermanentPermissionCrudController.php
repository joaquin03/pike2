<?php

namespace App\Http\Controllers\Admin;

use App\Http\Fields\PermanentPermissionsFields;
use App\Models\Operation;
use App\Models\PermanentPermission;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\PermanentPermissionRequest as StoreRequest;
use App\Http\Requests\PermanentPermissionRequest as UpdateRequest;
use App\Models\Provider;
use Illuminate\Http\Request;

class PermanentPermissionCrudController extends CrudController
{
    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\PermanentPermission');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/permanent-permission');
        $this->crud->setEntityNameStrings('permanent permission', 'permanent permissions');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */

        $fields = new PermanentPermissionsFields();
        $this->crud->addFields($fields->getFormFields(), 'both');
        $this->crud->addColumns($fields->getColumnsFields());
        
    }
    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }



    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
