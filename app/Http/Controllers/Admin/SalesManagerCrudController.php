<?php

namespace App\Http\Controllers\Admin;

use App\Http\Fields\CompanyTagFields;
use App\Http\Fields\SalesManagerFields;
use Backpack\CRUD\app\Http\Controllers\CrudController;
// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\StoreCompanyTagRequest as StoreRequest;
use App\Http\Requests\StoreCompanyTagRequest as UpdateRequest;

class SalesManagerCrudController extends CrudController
{
    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\User');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/sales-manager');
        $this->crud->setEntityNameStrings('company event', 'Sales Managers');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */

        $fields = new SalesManagerFields();
        $this->crud->addFields($fields->getFormFields(), 'both');
        $this->crud->addColumns($fields->getColumnsFields());

        $this->crud->allowAccess(['create', 'list', 'update', 'reorder', 'edit']);
        $this->crud->denyAccess(['create', 'delete']);
    }



    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
