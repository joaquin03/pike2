<?php

namespace App\Http\Controllers\Admin;

use App\Http\Fields\CrewMemberFields;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\CrewMemberRequest as StoreRequest;
use App\Http\Requests\CrewMemberRequest as UpdateRequest;

class CrewMemberCrudController extends CrudController
{
    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\CrewMember');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/crew-member');
        $this->crud->setEntityNameStrings('Crew member', 'crew members');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
    
        $fields = new CrewMemberFields();
        $this->crud->addFields($fields->getFormFields(), 'both');
        $this->crud->addColumns($fields->getColumnsFields());
        
    }
    
    public function show($id)
    {
        //$this->crud->hasAccessOrFail('show');
        
        // get the info for that entry
        $crewMember = $this->crud->getEntry($id);
        return view('crewMember.show', compact('crewMember'));
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
    
    
   

}
