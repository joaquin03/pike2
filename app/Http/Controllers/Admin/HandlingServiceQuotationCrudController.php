<?php

namespace App\Http\Controllers\Admin;

use App\Http\Fields\CrewMemberFields;
use App\Http\Fields\HandlingServiceQuotationFields;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\HandlingServiceQuotationRequest as StoreRequest;
use App\Http\Requests\HandlingServiceQuotationRequest as UpdateRequest;

class HandlingServiceQuotationCrudController extends CrudController
{
    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\HandlingServiceQuotation');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/handling-service');
        $this->crud->setEntityNameStrings('Handling Service', 'Handling Service');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
    
        $fields = new HandlingServiceQuotationFields();
        $this->crud->addFields($fields->getFormFields(), 'both');
        $this->crud->addColumns($fields->getColumnsFields());
        
    }
    
    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
    
    
   

}
