<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\TotalReportGenerator;
use App\Http\Fields\ReportFields;
use App\Models\Report;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ReportRequest as StoreRequest;
use App\Http\Requests\ReportRequest as UpdateRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\UnauthorizedException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class ReportCrudController extends CrudController
{
    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Report');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/report');
        $this->crud->setEntityNameStrings('report', 'reports');
        $this->crud->companyId = \Route::current()->parameter('company_id');
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $fields = new ReportFields();
        $this->crud->addFields($fields->getFormFields(), 'both');
        $this->crud->addColumns($fields->getColumnsFields());
        
        if(!Auth::user()->can('CRM-Advance')){
            return redirect('/admin/dashboard')
                ->withErrors( 'Error! Procurement not allowed. Contact your admin for permissions.');
        }
    
    }

    public function index(){
        return $this->create();
    }
    public function store(StoreRequest $request)
    {
        $report= new Report();
        $data = $request->all();
        $report->date=$data['date'];
        $report->place=$data['place'];
        $report->objectives=$data['objectives'];
        $report->participants=$report->getParticipantsNames($data['participants']);
        $report->background_info=$data['background_info'];
        $report->tag = $request['tags'];
        $report->tag_name = $report->getTagName($request['tags']);
    
    
        return view('reports.show', compact('report'));
    
    }

    public function update(UpdateRequest $request)
    {
        $report= new Report();
        $data = $request->all();
        $report->date=$data['date'];
        $report->place=$data['place'];
        $report->objectives=$data['objectives'];
        $report->participants=$report->getParticipantsNames($data['participants']);
        $report->background_info=$data['background_info'];
        $report->tag = $request['tags'];
    
        $report->tag_name = $report->getTagName($request['tags']);
    
    
        return view('reports.show', compact('report'));
    }
    
    public function totals(Request $request)
    {
        $request->date_start = $request->get('date_start', Carbon::now()->subMonth(3)->firstOfMonth()->format('d/m/Y'));
        $request->date_end = $request->get('date_end', Carbon::now()->lastOfMonth()->format('d/m/Y'));

        $report = new TotalReportGenerator();
        $expensesTotal = $report->expenses($request);
        $profitsTotal = $report->profits($request);
        $total = round($profitsTotal - $expensesTotal, 3);
    
        $totalByMonth = $report->totalsByMoth($request);
    
        return view('charts.totals', compact('expensesTotal', 'profitsTotal', 'total', 'totalByMonth', 'request'));
    }
}
