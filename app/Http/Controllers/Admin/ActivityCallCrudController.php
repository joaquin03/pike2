<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Contracts\ActivityCrudController;
use App\Http\Fields\ActivityCallFields;
use App\Http\Requests\ActivityCallRequest;


// VALIDATION: change the requests to match your own file names if you need form validation


class ActivityCallCrudController extends ActivityCrudController
{
    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\ActivityCall');
        $this->crud->setEntityNameStrings('Activity Call', 'Activity Calls');
    
    
        $this->activitySetUp(new ActivityCallFields(), new ActivityCallRequest());
        $this->crud->setRoute("admin/company/".$this->crud->companyId."/activity-call");
    }
}
