<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Contracts\ActivityCrudController;
use App\Http\Fields\ActivityReminderFields;
use App\Http\Requests\ActivityReminderRequest;
use App\Models\ActivityReminder;
use App\Models\Company;
use Prologue\Alerts\Facades\Alert;


class ActivityReminderCrudController extends ActivityCrudController
{
    
    public function setup()
    {
        
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\ActivityReminder');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/activity-reminder');
        $this->crud->setEntityNameStrings('Activity Reminder', 'Activity Reminders');
        
        $this->activitySetUp(new ActivityReminderFields(), new ActivityReminderRequest());
        $this->crud->setRoute("admin/company/" . $this->crud->companyId . "/activity-reminder");
    }
    
    
    public function finishReminder($companyid, ActivityReminder $reminder)
    {
        if ($reminder->isAuthorOrAssigned()) {
            $reminder->setIsFinish(['is_finish' => true]);
            $reminder->save();
            Alert::success('Reminder Completed');
        } else {
            Alert::error('Only the assigned or author user, cant set as finished');
        }
        
        return response()->redirectTo('/admin/dashboard');
    }
    
    public function edit($id)
    {
        
        $company = Company::findOrFail($id);
        
        return parent::edit($this->getActivityId());
    }
    
    public function delete($company_id, $reminder_id)
    {
        if (parent::destroy($reminder_id)) {
            Alert::success('Reminder Deleted')->flash();
        } else {
            Alert::error('Reminder not deleted')->flash();
        }
        
        return response()->redirectTo('/admin/dashboard');
    }
}
