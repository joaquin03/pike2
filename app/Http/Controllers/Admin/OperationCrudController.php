<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ReportsGenerator;
use App\Http\Fields\CrewMemberFields;
use App\Http\Fields\OperationFields;
use App\Http\Requests\GenericOperationRequest;
use App\Http\Requests\OperationRequest;
use App\Http\Requests\StoreOperationPermission;
use App\Http\Requests\UpdateOperationPermission;
use App\Http\Requests\StoreOperationPermanentPermission;
use App\Http\Transformers\Procurement\PermitsTransformer;
use App\Http\Transformers\Screen\OperationScreenTransformer;
use App\Models\Aircraft;
use App\Models\AircraftPermission;
use App\Models\Airport;
use App\Models\Operation;
use App\Models\Accounting\AccountingService;
use App\Models\PermanentPermission;
use App\Models\Provider;
use App\Models\ServiceParent;
use App\Models\Utilities\Country;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\OperationRequest as StoreRequest;
use App\Http\Requests\OperationRequest as UpdateRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class OperationCrudController extends CrudController
{
    public function setup()
    {
        /*getQuotationCreditNoteSurcharge
        |
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Operation');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/operation');
        $this->crud->setEntityNameStrings('operation', 'operations');

        $this->crud->allowAccess('revisions');
        $this->crud->with('revisionHistory');

        //$this->crud->enableDetailsRow();
        $this->crud->allowAccess('details_row');

        $this->crud->enableExportButtons();

        $this->crud->removeButton('update');
        $this->crud->removeButton('delete');
//        $this->crud->removeAllButtonsFromStack('line');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */

        $fields = new OperationFields();
        $this->crud->addFields($fields->getFormFields(), 'both');
        $this->crud->addColumns($fields->getColumnsFields());

        $this->crud->addFilter([ // dropdown filter
            'name' => 'status',
            'type' => 'select2',
            'label' => 'Status'
        ], function () {
            return [
                'In Process' => 'In Process',
                'Completed' => 'Completed',
                'Canceled' => 'Canceled',
                'Deleted' => 'Deleted',
            ];
        }, function ($value) { // if the filter is active
            $this->crud->addClause('where', 'state', $value);
        });

        $this->crud->setCreateView('operation.create');
        $this->crud->setEditView('operation.edit');
        $this->crud->removeButton('delete');

    }

    public function index()
    {
        if (!Auth::user()->can('Operation')) {
            return redirect('/admin/dashboard')->withErrors('Error! Operation not allowed. Contact your admin for permissions.');
        }
        $sorted = Operation::sortByStateAndDate();
        $status = $this->crud->request->has('status') ? $this->crud->request->all()['status'] : '';
        if ($status != '') {
            foreach ($sorted as $operation) {
                if ($operation['attributes']['state'] != $status) {
                    $key = array_search($operation, $sorted);
                    array_pull($sorted, $key);
                }
            }
        }
        $this->crud->hasAccessOrFail('list');
        $this->data['crud'] = $this->crud;
        $this->data['title'] = ucfirst($this->crud->entity_name_plural);
        $this->data['entries'] = $sorted;

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getListView(), $this->data);
    }


    public function create()
    {
        $this->data['crud'] = $this->crud;
        $this->data['operation'] = new Operation();
        $this->data['creating'] = true;


        return view('operation.create', $this->data);
    }

    public function listOperations()
    {
        $operations = Operation::sortByStateAndDate()->get();

        return $operations;
    }

    public function store(GenericOperationRequest $request)
    {
        if (!$this->validateOperationFields($request)) {
            return Redirect::back()->withErrors(['Please select all the Aircraft fields.']);
        }
        if (!$this->validateStartingDate($request)) {
            return Redirect::back()->withErrors(['Please select the Starting Date.']);
        }
        $operation = new Operation();
        $operation->creator_user_id = $request->user()->id;
        $operation->in_charge_user_id = $request->user()->id;
        $operation->fill($request->all());
        $operation->procurement_status = 'In Process';
        $operation->save();

        return redirect()->route('crud.operation.edit', ['operation_id' => $operation->id]);
    }

    public function validateOperationFields(GenericOperationRequest $request)
    {
        return $request->state && $request->aircraft_id && $request->aircraft_operator_id && $request->aircraft_client_id;
    }

    public function validateStartingDate(GenericOperationRequest $request)
    {
        return $request->start_date;
    }

    public function edit($id)
    {
        $this->data['crud'] = $this->crud;
        $this->data['operation'] = Operation::withoutGlobalScopes()->findOrFail($id);
        $this->data['airports'] = $this->data['operation']->itineraryAirports();
        $this->data['providers'] = Airport::providersByOperationIcaos($this->data['airports']);
        $this->data['creating'] = false;
        $permits = Operation::withoutGlobalScopes()->findOrFail($id)->aircraftPermission()->where('is_ovf', 0)->get();
        $permitsTransformer = new PermitsTransformer();
        $this->data['permits'] = $permitsTransformer->transformItems($permits);

        return view('operation.edit', $this->data);
    }

    public function cancel($id)
    {
        $operation = Operation::withoutGlobalScopes()->findOrFail($id);
        $operation->is_active = false;
        $operation->state = 'Canceled';
        $this->cancelServices($operation);
        $operation->save();

        return redirect()->back();
    }

    protected function cancelServices($operation)
    {
        foreach ($operation->itinerariesProviderServices()->get() as $ips) {
            $ips->operation_status = 'Canceled';
            $ips->save();
        }

    }

    public function update(Request $request, $id)
    {

        $operation = Operation::withoutGlobalScopes()->findOrFail($id);
        if (auth()->user()->can('update', $operation)) {

            $operation->fill($request->all());
            if ($request->state == 'Canceled') {
                $this->cancel($id);
            }

            $operation->save();

            return redirect()->route('crud.operation.edit', ['operation_id' => $operation->id]);
        }
        return redirect()->back()->withErrors('You don´t have the permissions to edit this operation.');

    }

    public function delete($id)
    {
        $operation = Operation::withoutGlobalScopes()->findOrFail($id);
        $operation->is_active = false;
        $operation->state = 'Deleted';
        $this->cancelServices($operation);
        $operation->save();

        return redirect()->back();
    }

    public function getTotalCost($operation_id)
    {
        $operation = Operation::findOrFail($operation_id);
        $total = $operation->getTotal();


        return view('operation.modals.total-cost', compact('total'));
    }

    /*Permissions */
    public function createPermission($operation_id, $is_additional = null)
    {
        $operation = Operation::withoutGlobalScopes()->findOrFail($operation_id);
        $airports = $operation->itineraryAirports();
        $this->data['providers'] = Airport::providersByOperationIcaos($airports);
        $providers = $this->data['providers'];

        return view('operation.modals.permission', compact('operation', 'providers', 'airports'));
    }

    public function storePermission(StoreOperationPermission $request, $operation_id)
    {
        $operation = Operation::withoutGlobalScopes()->findOrFail($operation_id);

        $operation->addPermission($request);

        $operationTransformer = new PermitsTransformer();
        $permits = $operationTransformer->transformItems($operation->aircraftPermission);
        return response()->json([
            'message' => 'Permit added',
            'data' => ['permissions' => $permits],
            201
        ]);
    }

    public function updateOperationPermission(UpdateOperationPermission $request, $operation_id)
    {
        $operation = Operation::withoutGlobalScopes()->findOrFail($operation_id);
        $operation->updateOperationPermission($request->all());

        $operationTransformer = new PermitsTransformer();
        $permits = $operationTransformer->transformItems($operation->aircraftPermission);

        return response()->json([
            'message' => 'Permit edited',
            'data' => ['permissions' => $permits],
            201

        ]);
    }

    public function deletePermission($operation_id, $permission_id)
    {
        AircraftPermission::findOrFail($permission_id)->delete();
        $operationTransformer = new PermitsTransformer();
        $operation = Operation::withoutGlobalScopes()->findOrFail($operation_id);

        $permits = $operationTransformer->transformItems($operation->aircraftPermission);

        return response()->json([
            'message' => 'Permit edited',
            'data' => ['permissions' => $permits],
            201

        ]);
    }

    public function showPermissionsQuotation($operation_id)
    {
        $operation = Operation::withoutGlobalScopes()->findOrFail($operation_id);
        $transformer = new PermitsTransformer();
        $data = $transformer->transformItems($operation->aircraftPermission);
        return response()->json($data, 200);
    }

    public function createPermanentPermission(Request $request, $operation_id)
    {
        $operation = Operation::withoutGlobalScopes()->findOrFail($operation_id);
        $aircraft = $operation->aircraft;
        $permanentPermissions = $aircraft->permanentPermissions();
        $operationPermanentPermissions = $operation->permanentPermissions()->pluck('permanent_permission_id')->toArray();

        $airports = $operation->itineraryAirports();

        return view('operation.modals.permanent-permission', compact('permanentPermissions',
            'operationPermanentPermissions', 'operation'));

    }

    public function storePermanentPermission(StoreOperationPermanentPermission $request, $operation_id)
    {
        $operation = Operation::withoutGlobalScopes()->findOrFail($operation_id);
        $permanentPermissions = [];
        if (isset(request()->all()['permanentPermissionId'])) {
            $permanentPermissions = request()->all()['permanentPermissionId'];
        }
        $operation->addPermanentPermissionList($permanentPermissions);

        return redirect()->route('crud.operation.edit', ['operation_id' => $operation->id]);
    }

    public function deletePermanentPermission(Request $request, $operation_id)
    {
        $operation = Operation::withoutGlobalScopes()->findOrFail($operation_id);

        if (!empty($request['operation_pm_id'])) {
            $operation->permanentPermissions()->wherePivot('id', $request['operation_pm_id'])->detach();
            \Alert::success('Permanent permission deleted')->flash();
        } else {
            \Alert::error('Permanent permission not deleted')->flash();
        }

        return redirect()->route('crud.operation.edit', ['operation_id' => $operation->id]);


    }

    public function getStatementFromPermission($permissionId)
    {
        $accountingService = AccountingService::where('servicesable_id', $permissionId)
            ->where('servicesable_type', get_class(new AircraftPermission()))->first();

        return redirect()->route('crud.invoices.edit', ['id' => $accountingService->bill_id]);

    }

    public function getStatementFromPermanentPermission($permissionId)
    {
        $accountingService = AccountingService::where('servicesable_id', $permissionId)
            ->where('servicesable_type', get_class(new PermanentPermission()))->first();

        return redirect()->route('crud.invoices.edit', ['id' => $accountingService->bill_id]);

    }

    public function listRevisions($id)
    {

        $this->crud->hasAccessOrFail('revisions');

        // get the info for that entry
        $this->data['entry'] = $this->crud->getEntry($id);
        $this->data['crud'] = $this->crud;
        $this->data['title'] = ucfirst($this->crud->entity_name) . ' ' . trans('backpack::crud.revisions');
        $this->data['id'] = $id;
        $this->data['revisions'] = $this->data['entry']->getRevisions(new Operation());
        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view('revision.revisions', $this->data);
    }

    public function report()
    {
        $aircraft = Aircraft::all();
        $countries = Country::all();
        $airports = Airport::all();
        $providers = Provider::all();
        $services = ServiceParent::all();


        return view('reports.operationReport', compact('aircraft', 'countries', 'airports', 'providers', 'services'));
    }


    public function screen(Request $request)
    {
        $sorted = Operation::where('state', 'In Process')->orderBy('start_date')->paginate($request->paginate);

        $this->crud->hasAccessOrFail('list');
        $this->data['crud'] = $this->crud;
        $this->data['title'] = ucfirst($this->crud->entity_name_plural);
        $this->data['page'] = $request->page;
        $this->data['paginate'] = $request->paginate;

        $transformer = new OperationScreenTransformer();
        $this->data['items'] = $transformer->transformItems($sorted);

        return view('operation.screen', $this->data);
    }

    public function resetStartDates()
    {
        foreach (Operation::withoutGlobalScopes()->get() as $operation) {
            $operation->setOldestStartDate();
        }
        return $this->screen();
    }


}
