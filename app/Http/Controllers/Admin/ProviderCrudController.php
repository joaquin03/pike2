<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\AddPriceToProviderRequest;
use App\Http\Requests\AddServiceToProviderRequest;
use App\Http\Requests\UpdateServiceToProviderRequest;
use App\Models\Airport;
use App\Models\Provider;

use App\Http\Requests\AirportRequest as StoreRequest;
use App\Http\Requests\AirportRequest as UpdateRequest;
use App\Models\ProviderPrices;
use App\Models\ProviderService;
use App\Models\Service;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ProviderCrudController extends CompanyCrudController
{
    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Provider');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/provider');
        $this->crud->setEntityNameStrings('provider', 'providers');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->removeButton('create');
        $this->companySetUp();
    }

    public function show($id)
    {
        //$this->crud->hasAccessOrFail('show');
        // get the info for that entry
        $company = $this->crud->getEntry($id);
        $activities = $company->activities()->publicOrMine()->orderByDate()->get();
        $contacts = $company->contacts;

        return view('provider.show', compact('company', 'activities', 'contacts'));
    }


    public function apiServiceList(Request $request, $provider_id)
    {
        $provider = Provider::findOrFail($provider_id);

        if ($request->get('is_for_procurement', false)) {
            $services = $provider->operationProviderServices;
        } else {
            $services = $provider->quotationProviderServices;
        }
        return response()->json($services);
    }


    public function addProviderToAirport(Request $request, $airport_icao)
    {
        try {
            $airport = Airport::findOrFail($airport_icao);

            $provider = Provider::findOrFail($request->get('provider_id'));


            $ret = $provider->addProviderToAirport($airport);
            \Alert::success('Provider added');
        } catch (\Exception $e) {
            \Log::error($e);
            \Alert::error('Provider added');
        }
        return back();
    }


    public function deleteProviderToAirport($airport_icao, $company_id)
    {
        $airport = Airport::findOrFail($airport_icao);
        $provider = Provider::findOrFail($company_id);

        $provider->deleteProviderOfAirport($airport);

        \Alert::success('Provider deleted');
        return back();
    }

    public function addPriceToProvider(AddPriceToProviderRequest $request, $provider_id)
    {
        $service = Service::findOrFail($request->service_id);
        $provider = Provider::findOrFail($provider_id);

        $price = new ProviderPrices();
        $price->provider_id = $provider_id;
        $price->service_id = $service->id;
        $price->currency_id = $request->currency_id;
        $price->service_date = Carbon::createFromFormat('d/m/Y', $request->service_date);
        $price->price = $request->price;
        $price->observation = $request->observation ?? '';
        $price->save();

        $provider->prices()->save($price);

        return ['price_id' => $price->id, 'date' => $price->date, 'service' => $service->name,
            'currency' => $price->currency->currency, 'price' => $price->price, 'observation' => $price->observation];

    }

    public function deletePriceToProvider($provider_id, $price_id)
    {
        $provider = Provider::findOrFail($provider_id);

        $provider->prices()->where('id', $price_id)->delete();


        return $price_id;
    }

    public function updatePriceToProvider(UpdateServiceToProviderRequest $request, $provider_id, $price_id)
    {
        $provider = Provider::findOrFail($provider_id);
        $price = ProviderPrices::findOrFail($price_id);
        $price->service_date = Carbon::createFromFormat('d/m/Y', $request->service_date);
        $price->price = $request->price;
        $price->observation = $request->observation ?? '';
        $price->save();


        return ['price_id' => $price->id, 'date' => $price->date, 'service' => $price->service->name,
            'currency' => $price->currency->currency, 'price' => $price->price, 'observation' => $price->observation];
    }


}
