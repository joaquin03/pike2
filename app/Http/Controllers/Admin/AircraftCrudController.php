<?php

namespace App\Http\Controllers\Admin;

use App\Http\Fields\AircraftFields;
use App\Models\AircraftType;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use App\Models\Aircraft;
use App\Models\Client;
use App\Models\Operator;


// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\AircraftRequest as StoreRequest;
use App\Http\Requests\AircraftRequest as UpdateRequest;

class AircraftCrudController extends CrudController
{
    public function setup()
    {
        
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Aircraft');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/aircraft');
        $this->crud->setEntityNameStrings('aircraft', 'aircraft');
        
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $fields = new AircraftFields();
        $this->crud->addFields($fields->getFormFields(), 'both');
        $this->crud->addColumns($fields->getColumnsFields());
        //$this->crud->setFromDb();
    }
    
    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
    
    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
    
    public function info($aircraft_registration)
    {
        if($aircraft_registration != null) {
            $aircraft = Aircraft::findOrFail($aircraft_registration);
            $client = Client::find($aircraft->client_id);
            $operator = Operator::find($aircraft->operator_id);
            $type = $aircraft->type;
        }
        return response()->json([
            'client_id' => (isset($client)) ? $client->id : null,
            'operator_id' => (isset($operator)) ? $operator->id : null,
            'mtow' => (isset($type)) ? $type->MTOW : null,
            'mtow_type' => (isset($type)) ? $type->MTOW_Type : null,
            'aircraft_type' => (isset($type)) ? $type->name : null,
        ], 200);
    }
    
    public function typeInfo($aircraft_type_id)
    {
        $type = AircraftType::findOrFail($aircraft_type_id);
        
        return response()->json([
            'mtow' => (isset($type)) ? $type->MTOW : null,
            'mtow_type' => (isset($type)) ? $type->MTOW_Type : null,
        ], 200);
    }
    
    public function destroy($id)
    {
        $aircraft = Aircraft::findOrFail($id);
        $this->crud->hasAccessOrFail('delete');
        
        if($aircraft->delete()!=null){
            return $this->crud->delete($id);
        }
        
        return back()->withInput()
            ->with('error_message','Error, aircraft contained in Operations.');
    }
    
    
    
}
