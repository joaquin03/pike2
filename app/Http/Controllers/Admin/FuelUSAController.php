<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\FuelUSA;
use App\Models\Itinerary;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Illuminate\Http\Request;

class FuelUSAController extends CrudController
{
    public function setup()
    {
        
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\FuelUSA');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/fuel-usa');
        $this->crud->setEntityNameStrings('Fuel USA', 'Fuel USA');
        
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
    }
    
    
    public function index()
    {
        $this->crud->hasAccessOrFail('list');
        
        $this->data['crud'] = $this->crud;
        $this->data['title'] = ucfirst($this->crud->entity_name_plural);
        
        $this->data['fuelData'] = FuelUSA::getLastFuelValues();
        $this->data['lastUpdate'] = FuelUSA::orderBy('updated_at', 'desc')->first();
        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view('fuelUSA.load', $this->data);
    }
    
    public function uploadExcel(Request $request)
    {
        $this->validate($request, [
            'fuel_airports' => 'required'
        ]);
        
        FuelUSA::loadExcel($request->file('fuel_airports'));
        \Alert::success('File Uploaded')->flash();
        return redirect()->back();
    }
    
    public function index2()
    {
        $this->crud->hasAccessOrFail('list');
        
        $this->data['crud'] = $this->crud;
        $this->data['title'] = ucfirst($this->crud->entity_name_plural);
        
        $this->data['fuelData'] = FuelUSA::getLastFuelValues();
        $this->data['lastUpdate'] = FuelUSA::orderBy('updated_at', 'desc')->first();
        
        return view('fuelUSA.show', $this->data);
    }
    
    public function showFuelItinerary($itinerary)
    {
        $itinerary = Itinerary::findOrFail($itinerary);
        $fuelData = FuelUSA::getFuelValuesForIcao($itinerary->airport_icao_string);
        
        if ($fuelData == []) {
            return redirect(route('fuel-prices.show', ['itinerary'=> $itinerary->id]));
        }
        
        $client = $itinerary->operation->aircraftClient;
        
        return view('fuelPrices.modals.FuelUsa', compact('fuelData', 'client'));
    }
    
}
