<?php

namespace App\Http\Controllers\Admin;

use App\Http\Fields\CreditCardFields;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\CreditCardRequest as StoreRequest;
use App\Http\Requests\CreditCardRequest as UpdateRequest;

class CreditCardCrudController extends CrudController
{
    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\CreditCard');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/credit-card');
        $this->crud->setEntityNameStrings('Credit Card', 'Credit Cards');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
    
    
        $fields = new CreditCardFields();
        $this->crud->addFields($fields->getFormFields(), 'both');
        $this->crud->addColumns($fields->getColumnsFields());
        $this->crud->allowAccess(['show', 'list', 'update', 'reorder', 'edit', 'delete']);
    
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
    
    
}
