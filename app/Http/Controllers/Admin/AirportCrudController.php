<?php

namespace App\Http\Controllers\Admin;

use App\Http\Fields\AirportFields;
use App\Models\Airport;
use App\Models\Provider;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\AirportRequest as StoreRequest;
use App\Http\Requests\AirportRequest as UpdateRequest;
use Illuminate\Http\Request;
use Prologue\Alerts\Facades\Alert;

class AirportCrudController extends CrudController
{
    public function setup()
    {
        
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Airport');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/airport');
        $this->crud->setEntityNameStrings('airport', 'airports');
        
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        
        $fields = new AirportFields();
        $this->crud->addFields($fields->getFormFields(), 'both');
        $this->crud->addColumns($fields->getColumnsFields());
        
        
        $this->crud->addButtonFromModelFunction('line', 'info_button', 'showLink', 'beginning');
        
    }
    
    public function show($icao)
    {
        //$this->crud->hasAccessOrFail('show');
        
        // get the info for that entry
        $airport = $this->crud->getEntry($icao);
        return view('airport.show', compact('airport'));
    }
    
    public function upload()
    {
        return view('airport.upload-excel');
    }
    
    public function uploadExcel(Request $request)
    {
        $file = $request->file('file');
        $quantity = airport::uploadExcel($file);
        
        $airports = Airport::all();
        \Alert::success('Airports successfully added');
        
        return redirect()->route('crud.airport.index', ['airport' => $airports]);
        
    }
    
    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
    
    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
    
    public function destroy($id)
    {
        $airport = Airport::findOrFail($id);

        $this->crud->hasAccessOrFail('delete');
        if($airport->canDelete()){
            
            return $this->crud->delete($id);
        }
        return back()->withInput()
            ->with('error_message','Error, airport contained in Itineraries.');
    }
    
    
    public function addAirportToProvider(Request $request, $company_id)
    {
        
        try {
            $airport = Airport::findOrFail($request->get('airport_icao'));
            
            $provider = Provider::findOrFail($company_id);
            
            
            $ret = $airport->addAirportToProvider($provider);
            \Alert::success('Airport added');
        } catch (\Exception $e) {
            \Log::error($e);
            \Alert::error('Airport added');
        }

        return redirect('/admin/provider/'. $company_id .'#airports');
    }
    
    public function deleteAirportToProvider($company_id, $airport_icao)
    {
        $airport = Airport::findOrFail($airport_icao);
        $provider = Provider::findOrFail($company_id);
        
        $airport->deleteAirportOfProvider($provider);
        
        \Alert::success('Airport deleted');
        return back();
    }
    
    public function handlers($airport_icao)
    {
        $airport = Airport::findOrFail($airport_icao);
        return response()->json($airport->airportProviders);
    
    }
}
