<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Contracts\ActivityCrudController;
use App\Http\Fields\ActivityFileFields;
use App\Http\Requests\ActivityFileRequest;
use App\Models\Company;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ActivityFileRequest as StoreRequest;
use App\Http\Requests\ActivityFileRequest as UpdateRequest;

class ActivityFileCrudController extends ActivityCrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\ActivityFile');
        $this->crud->setEntityNameStrings('Activity Files', 'Activity Files');
        $this->activitySetUp(new ActivityFileFields(), new ActivityFileRequest());
    
        $this->crud->setRoute("admin/company/".$this->crud->companyId.'/activity-file');
    }
    
}
