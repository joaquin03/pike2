<?php

namespace App\Http\Controllers\Admin;


use App\Http\Fields\BillingFields;
use App\Http\Transformers\Billing\AdministrationFeeTransformer;
use App\Http\Transformers\ItineraryProviderServiceTransformer;
use App\Http\Transformers\Procurement\ItineraryProvidersTransformer;
use App\Http\Transformers\Billing\ItineraryServiceParentTransformer;
use App\Http\Transformers\Procurement\ProviderPermanentPermitsTransformer;
use App\Http\Transformers\Procurement\ProviderPermitsTransformer;
use App\Http\Transformers\ProcurementIndexTransformer;
use App\Models\Accounting\AccountingService;
use App\Models\Airport;
use App\Models\Billing;
use App\Models\Client;
use App\Models\Itinerary;
use App\Models\ItineraryProviderService;
use App\Models\Provider;
use App\Models\Service;
use App\Models\ServiceChild;
use App\Models\ServiceChildPike;
use App\Searcher\OperationSearch;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Config;

class BillingCrudController extends ProcurementCrudController
{
    public function setup()
    {
        
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Billing');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/billing');
        $this->crud->setEntityNameStrings('billing', 'billings');
        
        $this->crud->removeButton('delete');
        $this->crud->removeButton('create');
    
    
        $fields = new BillingFields();
        $this->crud->addFields($fields->getFormFields(), 'both');
        $this->crud->addColumns($fields->getColumnsFields());
        
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        
        $this->crud->getListView('billing.index');
        $this->crud->setEditView('billing.edit');
        if(!Auth::user()->can('Billing')){
            return redirect('/admin/dashboard')->withErrors( 'Error! Billing not allowed. Contact your admin for permissions.');
        }
    }
    
    public function index()
    {
        $sorted  = Billing::sortByState()->paginate(50);
        $transformer = new ProcurementIndexTransformer();
        $this->data['crud'] = $this->crud;
        $this->data['items'] = $transformer->transformItems($sorted);
        return view('billing.index',  $this->data);
    }
    
    
    
    public function edit($id)
    {
        $providerPermitsTransformer = new ProviderPermitsTransformer();
      
        $this->data['crud'] = $this->crud;
        $this->data['operation'] = Billing::findOrFail($id);
        $this->data['airports'] = $this->data['operation']->itineraryAirports();
        $this->data['providers'] =  Airport::providersByOperationIcaos($this->data['airports']);
    
        $this->data['providerPermits'] = $providerPermitsTransformer->transform($this->data['operation']);
     
        $this->data['total'] =  $this->data['operation']->getTotal();
    
       
        return view('billing.edit', $this->data);
    }
    
    public function update(Request $request, $id)
    {
        $billing = Billing::findOrFail($id);
        $billing->fill($request->all());
        $billing->save();
        
        return redirect()->route('crud.billing.edit', ['operation_id' => $billing->id]);
    }
    
    public function showItinerary($itineraryId)
    {
        $itinerary = Itinerary::findOrFail($itineraryId);
        $transformer = new ItineraryServiceParentTransformer($itinerary);
        $itineraryServices = $transformer->transformItems($itinerary->itineraryServiceParents);

        $provider = Provider::findOrFail(Config::get('constants.pikeProviderId')); //In order to add new services, with pike as a provider


        //Datos de administration fee asociada a itinerario
        $transformer = new AdministrationFeeTransformer();
        $administrationFees = $transformer->transformItems($itinerary->getAdministrationFees());
        
        return view('billing.modals.itineraryShow', compact('itinerary', 'itineraryServices',
            'administrationFees', 'provider'));
    }
    
    public function filter(Request $request)
    {
        $searcher = new OperationSearch(new Billing());
        
        $transformer = new ProcurementIndexTransformer();
        $this->data['crud'] = $this->crud;
        $this->data['items'] = $transformer->transformItems($searcher->apply($request));
        
        return $this->data;
    }
    
    
    public function showEditBillingCompany(Request $request, $id)
    {
        $billing = Billing::findOrFail($id);
        $clinets = Client::all();
        return view('billing.modals.changeBillingCompany')->with(['operation' => $billing, 'companies'=> $clinets]);
}
    
    public function editBillingCompany(Request $request, $id)
    {
        $billing = Billing::findOrFail($id);
        $billing->billing_company_id = $request->billing_company_id;
        $billing->save();
    
        return redirect()->route('crud.billing.edit', ['operation_id' => $billing->id]);
    }
    
    
    
    public function redirectToBill($itineraryProviderServiceId)
    {
        $service = ItineraryProviderService::findOrFail($itineraryProviderServiceId);
       
        if ($service->has_bills != null && $service->has_bills >1) {

            return redirect()->route('crud.clients.bills.show', ['id'=>$service->has_bills]);
        }
        \Alert::error('Bill not found')->flash();
        return redirect()->back();
    }
}
