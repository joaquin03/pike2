<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\QuotationCreditNoteRecharges;
use App\Models\Quotation;
use App\Models\QuotationCreditNoteCharges;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\QuotationCreditNoteChargesRequest as StoreRequest;
use App\Http\Requests\QuotationCreditNoteChargesRequest as UpdateRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;


class QuotationCreditNoteChargesCrudController extends CrudController
{
    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\QuotationCreditNoteCharges');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/quotation_credit_note_charges');
        $this->crud->setEntityNameStrings('quotation_credit_note_charges', 'quotation_credit_note_charges');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
    }

    public function uploadExcel(Request $request)
    {

        $quotation = new QuotationCreditNoteRecharges;
        $quotation->load($request->file('charges'));

        \Alert::success('Excel Updated')->flash();
        return redirect()->back();
    }


    public function index()
    {
        $this->crud->hasAccessOrFail('list');

        $this->data['crud'] = $this->crud;
        $this->data['title'] = ucfirst($this->crud->entity_name_plural);
        $this->data['lastUpdate']= null;
        $this->data['creditCardsData'] = null;

        $lastItem = QuotationCreditNoteCharges::orderBy('updated_at', 'desc')->first();

        if ($lastItem) {
            $this->data['lastUpdate'] = $lastItem;
            $this->data['creditCardsData'] = QuotationCreditNoteCharges::where('updated_at', $this->data['lastUpdate']->updated_at)->get();
        }

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view('quotation.creditNoteCharges', $this->data);
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
