<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreItineraryService;
use App\Http\Requests\StoreOVFPermission;
use App\Http\Transformers\Billing\AdministrationFeeTransformer;
use App\Http\Transformers\Billing\ItineraryServiceParentTransformer;
use App\Http\Transformers\Procurement\AdditionalTransformer;
use App\Http\Transformers\Procurement\ItineraryProvidersTransformer;
use App\Http\Transformers\Procurement\ItineraryServicesAdditionalsTransformer;
use App\Http\Transformers\Procurement\ProviderPermanentPermissionAdditionalsTransformer;
use App\Http\Transformers\Procurement\ProviderPermissionAdditionalsTransformer;
use App\Http\Transformers\Procurement\ProviderPermanentPermitsTransformer;
use App\Http\Transformers\Procurement\ProviderPermitsTransformer;
use App\Http\Transformers\Procurement\ProvidersItineraryServicesTransformer;
use App\Models\Accounting\Additional;
use App\Models\Accounting\AdministrationFee;
use App\Models\AircraftPermission;
use App\Models\Itinerary;
use App\Models\ItineraryProviderService;
use App\Models\Operation;
use App\Models\PermanentPermission;
use App\Models\Procurement;
use App\Models\Provider;
use App\Models\Scopes\OVFScope;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Storage;

class ProcurementController extends Controller
{
    public function itineraryProviders($itineraryId)
    {
        $itinerary = Itinerary::find($itineraryId);
        
        return response()->json($itinerary->getProviderOfServices());
    }
    
    public function itineraryProvidersServices($itineraryId, $providerId)
    {
        $itinerary = Itinerary::find($itineraryId);
        $serviceProvider = $this->transformServices($itinerary->getServicesOfProvider($providerId));
        return response()->json($serviceProvider);
    }
    
    public function addService(StoreItineraryServxice $request, $itineraryId)
    {
        $itinerary = Itinerary::findOrFail($itineraryId);
        
        if ($itinerary->existsProviderServices($request->service_provider_id)) {
            return response()->json(['message' => 'Service already exists', 422]);
            
        } else {
            $itinerary->addService($request->all());
            return response()->json(['message' => 'Service added', 201]);
        }
    }
    
    
    private function transformServices($servicesProvider)
    {
        $ret = [];
        foreach ($servicesProvider as $serviceProvider) {
            $ret[] = [
                'id' => $serviceProvider->id,
                'service' => $serviceProvider->service->name,
                'procurement_cost' => $serviceProvider->pivot->procurement_cost,
                'quantity' => $serviceProvider->pivot->procurement_quantity,
                'status' => $serviceProvider->pivot->status,
            ];
        }
        return $ret;
    }
    
    public function updateService(Request $request, $itineraryId)
    {
        $itinerary = Itinerary::findOrFail($itineraryId);
        
        $itineraryProviderService = ItineraryProviderService::where('id', $request->id)
            ->where('itinerary_id', $itineraryId)->first();
        $itineraryProviderService->fill($request->all());
        $itineraryProviderService->save();
        $itinerary = $itinerary->setProcurementStatus(true);
        
        return $this->providerItineraryResponse($itinerary, 'Itinerary Updated');
    }
    
    public function createOVFPermit(StoreOVFPermission $request, $operationId)
    {
        $operation = Operation::findOrFail($operationId);
        $basedPermission = AircraftPermission::findOrFail($request->permit_id);
        
        $aircraftPermission = new AircraftPermission();
        $aircraftPermission->fill($basedPermission->toArray());
        $aircraftPermission->fill($request->all());
        $aircraftPermission->code = $basedPermission->code . ' - OverFlight ';
        $aircraftPermission->type = $basedPermission->type;
        $aircraftPermission->is_ovf = 1;
        $aircraftPermission->administration_fee = 0;
        $aircraftPermission->procurement_status = 0;
        $aircraftPermission->operation_id = $basedPermission->operation_id;
        $aircraftPermission->save();
        
        return $this->providerPermitsResponse($operation, 'Permit Updated');
    }
    
    public function createOVFPermanentPermit(StoreOVFPermission $request, $operationId)
    {
        $operation = Operation::findOrFail($operationId);
        $basedPermission = PermanentPermission::findOrFail($request->permit_id);
        
        $permanentPermission = $basedPermission->createOVFPermanentPermit($request, $operation);
        
        //agrego el nuevo pp a la lista de pps
        $aircraft = $operation->aircraft;
        $permanentPermissions = $aircraft->permanentPermissions();
        $permanentPermissions->push($permanentPermission);
        
        $operation->addPermanentPermissionList($permanentPermissions->pluck('id'));
    
    
        return $this->providerPermanentPermitsResponse($operation, 'Permanent Permit Updated');
    }
    
    
    public function createServiceAdditional(Request $request)
    {
        $itineraryProviderService = ItineraryProviderService::find($request->iti_pro_ser_id);
        $additional = $itineraryProviderService->createAdditional($request);
        
        return $additional;
    }
    
    public function updateServiceAdditional(Request $request, $additionalId)
    {
        $additional = Additional::findOrFail($additionalId);

        if($additional->isContainedInStatement($request->get('billing-type'))) {
            return $this->servicesAdditionalsResponse($additional->servicesable->itinerary_id, 'Additional already contained in statement', 422);
        }

        $additional->fill($request->all());
        $additional->save();
        
        return $this->servicesAdditionalsResponse($additional->servicesable->itinerary_id, 'Additional Updated');
    }
    
    //StoreOVFPermission request has the same params as the Additionals
    public function createPermitAdditional(StoreOVFPermission $request, $permitId)
    {
        $permit = AircraftPermission::withoutGlobalScope(OVFScope::class)->findOrFail($permitId);
        $additional = $permit->createAdditional($request);
        $additional->administration_fee = 0;
        $additional->has_bills = 0;
        $additional->save();
        
        return $this->permitsAdditionalsResponse($additional->servicesable->operation_id, 'Additional Created');
    }
    
    //StoreOVFPermission request has the same params as the Additionals
    public function updatePermitAdditional(StoreOVFPermission $request, $additionalId)
    {
        $additional = Additional::findOrFail($additionalId);

        if($additional->isContainedInStatement($request->get('billing-type'))) {
            return $this->permitsAdditionalsResponse($additional->servicesable->operation_id, 'Additional already contained in statement', 422);
        }

        $additional->tax = $request->tax;
        $additional->billing_unit_price = $request->billing_unit_price;
        $additional->save();
        
        return $this->permitsAdditionalsResponse($additional->servicesable->operation_id, 'Additional Updated');
    }
    
    //StoreOVFPermission request has the same params as the Additionals
    public function createPermanentPermitAdditional(StoreOVFPermission $request, $permitId, $operationId)
    {
        $permit = PermanentPermission::withoutGlobalScope(OVFScope::class)->findOrFail($permitId);
        $additional = $permit->createAdditional($request, $operationId);
        $additional->administration_fee = 0;
        $additional->has_bills = 0;
        $additional->save();
        
        return $this->permanentPermitsAdditionalsResponse($additional, $operationId, 'Additional Created');
    }
    
    //StoreOVFPermission request has the same params as the Additionals
    public function updatePermanentPermitAdditional(StoreOVFPermission $request, $additionalId, $operationId)
    {
        $additional = Additional::findOrFail($additionalId);
        $additional->tax = $request->tax;
        $additional->billing_unit_price = $request->billing_unit_price;
        $additional->save();
        
        return $this->permanentPermitsAdditionalsResponse($additional, $operationId, 'Additional Updated');
    }

    public function deletePermitAdditional(Request $request, $additionalId)
    {
        $additional = Additional::findOrFail($additionalId);
        if ($additional->deleteItem() ) {
            return $this->permitsAdditionalsResponse($request->operation_id, 'Additional Deleted');
        }

        return response()->json([
            'message'   => 'Can not delete additional. Invoice attached',
        ], 422);
    }

    public function deletePermanentPermitAdditional(Request $request, $additionalId)
    {
        $additional = Additional::findOrFail($additionalId);

        if ($additional->deleteItem() ) {
            return $this->permanentPermitsAdditionalsResponse(null, $request->operation_id, 'Additional Deleted');
        }

        return response()->json([
            'message'   => 'Can not delete additional. Invoice attached',
        ], 422);

    }

    public function deleteServiceAdditional(Request $request, $additionalId)
    {
        $additional = Additional::findOrFail($additionalId);

        if ($additional->deleteItem()) {
            return $this->servicesAdditionalsResponse($request->itinerary_id, 'Additional Deleted');
        }

        return response()->json([
            'message'   => 'Can not delete additional. Invoice attached',
        ], 422);

    }


    
    public function providersPermits($procurementId)
    {
        $procurment = Procurment::find($procurementId);
        
        return response()->json($procurment->getProvidersPermits());
    }
    
    public function aircraftPermissionUpdate(Request $request, $id, $permitsId)
    {
        $procurement = Procurement::findOrFail($id);
        $permits = $procurement->aircraftPermission()->find($permitsId);
        if ($permits == null) {
            return response()->json(['error' => 'AircraftPermission not found'], 404);
        }

        $providerPermitsTransformer = new ProviderPermitsTransformer();
        if($permits->isContainedInStatement($request->get('billing-type'))) {

            return response()->json([
                'message' => 'AircraftPermission already contained in statement',
                'data' => $providerPermitsTransformer->transform($procurement)
            ], 422);
        }
        $permits->fill($request->all());
        $permits->save();
        
        $providerPermitsTransformer = new ProviderPermitsTransformer();

        return response()->json([
            'message' => 'Permit Updated',
            'data' => $providerPermitsTransformer->transform($procurement)
        ], 200);

    }
    
    public function permanentPermissionUpdate(Request $request, $id, $permitsId)
    {
        $procurement = Procurement::findOrFail($id);
        $permanentPermits = $procurement->permanentPermissions()->find($permitsId);

        if ($permanentPermits == null) {
            return response()->json(['error' => 'PermanentPermission not found'], 404);
        }

        $providerPermanentPermitsTransformer = new ProviderPermanentPermitsTransformer();

        if($permanentPermits->isContainedInStatement($request->get('billing-type'))) {
            return response()->json([
                'message' => 'AircraftPermission already contained in statement',
                'data' => $providerPermanentPermitsTransformer->transform($procurement)
            ], 422);
        }

        $permanentPermits->fill($request->all());

        if ($request->administration_fee == 1) {
            $permanentPermits->createAdministrationFee($request);
        }

        $permanentPermits->save();
        

        return response()->json([
            'message' => 'Permanent Permit Updated',
            'data' => $providerPermanentPermitsTransformer->transform($procurement)
        ], 200);
    }
    
    public function getTotal($id)
    {
        $procurment = Procurement::find($id);
        
        return response()->json($procurment->getTotal());
    }
    
    
    public function uploadBillingProvider(Request $request, $itineraryId, $providerId)
    {
        $itinerary = Itinerary::findOrFail($itineraryId);
        $providerServices = $itinerary->providerServices()->where('provider_id', $providerId);
        
        $path = $request->file('billing')->store('provider-services');
        
        $itinerary = $itinerary->setBillingStatus(true);
    
        if ($providerServices->update(['billing_file' => $path])) {
            return $this->providerItineraryResponse($itinerary, 'Billing Updated');
        };
        
        return response()->json([
            'message' => 'Error',
            421
        ]);
        
    }
    
    
    private function providerItineraryResponse($itinerary, $message = '')
    {
        $transformer = new ItineraryProvidersTransformer($itinerary);
        $providers = $transformer->transformItems($itinerary->getProviderOfServices());
        
        return response()->json([
            'message' => $message,
            'data' => ['providers' => $providers, 'total' => $itinerary->getTotal()],
            201
        ]);
    }
    
    private function providerPermitsResponse($operation, $message = '')
    {
        $procurement = Procurement::findOrFail($operation->id);
        $transformer = new ProviderPermitsTransformer();
        $providers = $transformer->transformItems($procurement);
        
        return response()->json([
            'message' => $message,
            'data' => ['providers' => $providers, 'total' => $operation->getTotal()],
            201
        ]);
    }
    
    private function providerPermanentPermitsResponse($operation, $message = '')
    {
        $procurement = Procurement::findOrFail($operation->id);
        $transformer = new ProviderPermanentPermitsTransformer();
        $providers = $transformer->transformItems($procurement);
        
        return response()->json([
            'message' => $message,
            'data' => ['providers' => $providers, 'total' => $operation->getTotal()],
            201
        ]);
    }
    

    private function servicesAdditionalsResponse($itinerary_id, $message = '', $code = 201)
    {
        $itinerary = Itinerary::findOrFail($itinerary_id);
    
        $transformer = new ItineraryServicesAdditionalsTransformer();
        $additionals = $transformer->transformItems($itinerary);

        return response()->json([
            'message' => $message,
            'data' => ['additionals' => $additionals, 'total' => 0],
            $code
        ]);
    }
    
    private function permitsAdditionalsResponse($operation_id, $message = '', $code = 201)
    {
        $procurement = Procurement::findOrFail($operation_id);
        $transformer = new ProviderPermissionAdditionalsTransformer();
        $providers = $transformer->transformItems($procurement);
//        $categoriesTotals = $itinerary->procurementTransformServiceCategoriesTotals();
        
        return response()->json([
            'message' => $message,
            'data' => ['providers' => $providers, 'total' => 0],
            $code
        ]);
    }
    
    private function permanentPermitsAdditionalsResponse($additional, $operationId, $message = '')
    {
        $procurement = Procurement::findOrFail($operationId);
        $transformer = new ProviderPermanentPermissionAdditionalsTransformer();
        $providers = $transformer->transformItems($procurement);
//        $categoriesTotals = $itinerary->procurementTransformServiceCategoriesTotals();
        
        return response()->json([
            'message' => $message,
            'data' => ['providers' => $providers, 'total' => 0],
            201
        ]);
    }
    
    
    public function showAdministrationFee($itineraryId)
    {
        $itinerary = Itinerary::findOrFail($itineraryId);
        $transformer = new AdministrationFeeTransformer($itinerary);
        $administrationFees = $transformer->transformItems($itinerary->getAdministrationFees());
//      $categoriesTotals = $itinerary->procurementTransformServiceCategoriesTotals();
        
        
        return response()->json([
            'message' => '',
            'data' => ['administrationFees' => $administrationFees, 'total' => 0],
            201
        ]);
    }
    
    public function showPermitsAdministrationFee($operationId)
    {
        $operation = Operation::findOrFail($operationId);
        $transformer = new AdministrationFeeTransformer();
        $administrationFees = $transformer->transformItems($operation->permitsAdministrationFees()->get());
//      $categoriesTotals = $itinerary->procurementTransformServiceCategoriesTotals();
        
        return response()->json([
            'message' => '',
            'data' => ['administrationFees' => $administrationFees, 'total' => 0],
            201
        ]);
    }
    
    public function showPermitAdditionals($providerId)
    {
        $provider = Provider::findOrFail($providerId);
        $transformer = new AdditionalTransformer();
        $additionals = $transformer->transformItems($provider->permitsAdditionals()->get());
//      $categoriesTotals = $itinerary->procurementTransformServiceCategoriesTotals();
        
        return response()->json([
            'message' => '',
            'data' => ['additionals' => $additionals, 'total' => 0],
            201
        ]);
    }
    
    public function showPermanentPermitAdditionals($providerId)
    {
        $provider = Provider::findOrFail($providerId);
        $transformer = new AdditionalTransformer();
        $additionals = $transformer->transformItems($provider->getPermanentPermitsAdditionals());
//      $categoriesTotals = $itinerary->procurementTransformServiceCategoriesTotals();
        
        return response()->json([
            'message' => '',
            'data' => ['additionals' => $additionals, 'total' => 0],
            201
        ]);
    }
    
    public function showPermanentPermitsAdministrationFee($operationId)
    {
        $operation = Operation::findOrFail($operationId);
        $transformer = new AdministrationFeeTransformer();
        $administrationFees = $transformer->transformItems($operation->permanentPermitsAdministrationFees()->get());
//      $categoriesTotals = $itinerary->procurementTransformServiceCategoriesTotals();
        
        return response()->json([
            'message' => '',
            'data' => ['administrationFees' => $administrationFees, 'total' => 0],
            201
        ]);
    }
    
    public function upload(Request $request, $operationId)
    {
        if ($request->hasFile('file')) {
            $filePath = Storage::putFile('aircraft-permission', $request->file('file'));
            $operation = Operation::find($operationId);
            $permissions = $operation->aircraftPermission()->where('provider_id', $request->provider_id)->get();
            foreach ($permissions as $permission) {
                $permission->invoice = $filePath;
                $permission->save();
            }
        }
        return back();
        
    }
    
    public function getUpload(Request $request)
    {
        return view(Route::current());
    }
    
    public function addInvoice(Request $request, $operationId)
    {
        $procurement = Procurement::find($operationId);
        $accountingServices['services'] = $request->get('services', []);
        $accountingServices['serviceParent'] = $request->get('serviceParent', []);
        $accountingServices['permits'] = $request->get('permits', []);
        $accountingServices['permanentPermits'] = $request->get('permanentPermits', []);
        $accountingServices['administrationFees'] = $request->get('administrationFees', []);
        $accountingServices['permitsAdministrationFees'] = $request->get('permitsAdministrationFees', []);

        $bill = $procurement->createInvoice(
            $accountingServices, $request->get('provider_id'), $request->get('itinerary_id'), 'credit', $request->get('note'));

        return response()->json($bill, 201);
    }
    
    public function addPermanentPermitsInvoice(Request $request, $operationId)
    {
        $procurement = Procurement::find($operationId);
        
        $accountingServices['services'] = $request->get('services', []);
        $accountingServices['permits'] = $request->get('permits', []);
        $accountingServices['permanentPermits'] = $request->get('permanentPermits', []);
        $accountingServices['administrationFees'] = $request->get('administrationFees', []);
        $accountingServices['permitsAdministrationFees'] = $request->get('permitsAdministrationFees', []);

        $bill = $procurement->createInvoice(
            $accountingServices, $request->get('provider_id'), $request->get('itinerary_id'), 'credit', $request->get('note'));
        return response()->json($bill, 201);
    }
    
    public function addAdditionalInvoice(Request $request, $operationId)
    {
        
        $itinerary = Itinerary::findOrFail($request->itinerary_id);
        $procurement = Procurement::find($itinerary->operation_id);
    
        $accountingServices['additionals'] = $request->get('service', []);
        
        $bill = $procurement->createInvoice(
                $accountingServices, $request->get('provider_id'), $request->get('itinerary_id'), 'credit', $request->get('note'));
        return response()->json($bill, 201);
    }
    
    public function addPermitAdditionalInvoice(Request $request, $operationId)
    {
        $additional = Additional::findOrFail($request->service[0]);
        $procurement = Procurement::find($additional->operation_id);
    
        $accountingServices['additionals'] = $request->get('service', []);
        
        $bill = $procurement->createInvoice(
            $accountingServices, $request->get('provider_id')[0], null, 'credit', $request->get('note'));
        return response()->json($bill, 201);
    }
    
    
    public function deleteOperationPermit($id, $permitId)
    {
        $permit = AircraftPermission::withoutGlobalScope(OVFScope::class)->findOrFail($permitId);
        $procurement = Procurement::findOrFail($id);
    
        if ($permit->deleteItem()) {
            $transformer = new ProviderPermitsTransformer();
            $permits = $transformer->transform($procurement);
        
            return response()->json([
                'message' => 'Permit deleted',
                'data' => ['providers' => $permits],
                201
            ]);
        }
        return response()->json([
            'message' => 'Error. Permit contained in Invoice, Bill or contains Additionals',
            'data' => false,
            422
        ]);
    }
    
    public function deleteOperationPermanentPermit($id, $permitId)
    {
        $permit = PermanentPermission::withoutGlobalScope(OVFScope::class)->findOrFail($permitId);
        $procurement = Procurement::findOrFail($id);
        
        
        if ($permit->deleteItem($procurement->id)) {
            $transformer = new ProviderPermanentPermitsTransformer();
            $permanentPermits = $transformer->transform($procurement);
            
            return response()->json([
                'message' => 'Permanent Permit deleted',
                'data' => ['providers' => $permanentPermits],
                201
            ]);
        }
        return response()->json([
            'message' => 'Error. Permit contained in Invoice, Bill or contains Additionals',
            'data' => false,
            422
        ]);
    }
}