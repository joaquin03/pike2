<?php
/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 3/28/18
 * Time: 2:34 PM
 */

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Models\Airport;
use App\Models\ProviderAirport;
use Illuminate\Http\Request;

class AirportController extends Controller
{
    public function updateProvider(Request $request,  $airportIcao)
    {
        $providerAirport = ProviderAirport::where(
            ['airport_icao'=>$airportIcao, 'provider_id'=>$request->get('provider_id')])->first();
    
        $providerAirport->score = $request->get('score', 0);
        $providerAirport->save();
        return response()->json([], 200);
    }
}