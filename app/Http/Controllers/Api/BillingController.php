<?php

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Http\Transformers\ProcurementIndexTransformer;
use App\Models\Accounting\AdministrationFee;
use App\Models\Billing;
use App\Models\Itinerary;
use Illuminate\Http\Request;
use App\Searcher\OperationSearch;


class BillingController extends Controller
{
    
    public function getItineraryBillingInfo($billingId, $itineraryId)
    {
        $itinerary = Itinerary::find($itineraryId);
        
        return response()->json($itinerary->getBillingInfo());
    }
    
    public function updateBillingPercentage(Request $request, $billingId, $itineraryId)
    {
        $itinerary = Itinerary::find($itineraryId);
        $itinerary->updateServiceCategoryBillingPercentage(
            $request->get('category_id'), $request->get('billing_percentage'));
        
        return response()->json($itinerary->getBillingInfo());
    }
    
    public function getItineraryBillingTotal($billingId, $itineraryId)
    {
        $itinerary = Itinerary::find($itineraryId);
        return response()->json($itinerary->billingTotal());
    }
    
    public function updateItineraryBillingInfo(Request $request, $billingId, $itineraryId)
    {
        $itinerary = Itinerary::find($itineraryId);
        $itinerary->updateBillingInfo($request->all());
        
        $itinerary = $itinerary->setBillingStatus(true);
    
    
        return response()->json($itinerary->getBillingInfo());
    }



    
    public function createBill(Request $request, $billingId)
    {
        $billing = Billing::find($billingId);
        
        $accountingServices['permits'] = $request->get('permits', []);
        $accountingServices['permitsAdministrationFees'] = $request->get('permitsAdministrationFees', []);
        
        $bill = $billing->createBill(
            $accountingServices, $request->get('itinerary_id', null),
            $request->get('note'), $request->get('admFeePercentage')
        );

        return response()->json($bill, 201);
    }

    public function filter(Request $request)
    {
        $searcher = new OperationSearch(new Billing());

        $transformer = new ProcurementIndexTransformer();
        $this->data['crud'] = $this->crud;
        $this->data['items'] = $transformer->transformItems($searcher->apply($request));

        return $this->data;
    }
}