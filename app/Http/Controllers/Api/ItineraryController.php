<?php

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Http\Requests\StoreItineraryService;
use App\Http\Requests\StoreOVFPermission;
use App\Http\Transformers\Billing\ItineraryBillingTransformer;
use App\Http\Transformers\Billing\ItineraryServiceParentTransformer;
use App\Http\Transformers\Billing\QuotationItineraryServiceParentTransformer;
use App\Http\Transformers\ItineraryProviderServiceTransformer;
use App\Http\Transformers\Procurement\ItineraryProvidersTransformer;
use App\Http\Transformers\Procurement\ItineraryServicesAdditionalsTransformer;
use App\Models\Accounting\AdministrationFee;
use App\Http\Transformers\Procurement\ProvidersItineraryServicesTransformer;
use App\Models\Itinerary;
use App\Models\ItineraryProviderService;
use App\Models\ItineraryServiceParents;
use App\Models\ProviderService;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\Http\Requests\UpdateItineraryServicesRequest;
use App\Http\Requests\ItineraryRequest as StoreRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;


class ItineraryController extends Controller
{
    
    public function getTotal($itineraryId)
    {
        $itinerary = Itinerary::find($itineraryId);
        
        return response()->json(['total_services' => $itinerary->getTotal()], 200);
    }
    
    public function showServices(Request $request, $itineraryId)
    {
        $itinerary = Itinerary::findOrFail($itineraryId);

        if ($request->get('in_operation')) {
            return $this->showServicesOperation($itinerary);
        }
        if ($request->get('in_procurement')) {
            return $this->showServicesProcurement($itinerary);
        }
        if ($request->get('in_billing')) {
            return $this->showServicesBilling($itinerary);
        }
        if ($request->get('in_quotation')) {
            if($request->get('is_additional')){
                return $this->showServicesQuotationAdditionals($itinerary);
            }
            return $this->showServicesQuotation($itinerary);
        }
    }
    
    private function showServicesOperation($itinerary)
    {
        $transformer = new ItineraryProviderServiceTransformer();
        $data = $transformer->transformItems($itinerary->itineraryProviderServicesForOperations);
        return response()->json($data, 200);
    }
    
    private function showServicesProcurement($itinerary)
    {
        $transformer = new ProvidersItineraryServicesTransformer($itinerary);
        
        $providers = $transformer->transformItems($itinerary->getProviderOfServices());
        
        return response()->json($providers, 200);
    }
    
    private function showServicesQuotation($itinerary)
    {
        $transformer = new QuotationItineraryServiceParentTransformer($itinerary);
        $data = $transformer->transformItems($itinerary->serviceParents);
        return response()->json($data, 200);
    }
    
    private function showServicesQuotationAdditionals($itinerary)
    {
        $transformer = new QuotationItineraryServiceParentTransformer($itinerary);
        $data = $transformer->transformAdditionalsItems($itinerary->serviceParents);
        return response()->json($data, 200);
    }
    
    private function showServicesBilling($itinerary)
    {
        $transformer = new ItineraryServiceParentTransformer($itinerary);
        $itineraryServices = $transformer->transformItems($itinerary->itineraryServiceParents);
    
    
        return response()->json($itineraryServices, 200);
    }
    
    public function update(StoreRequest $request, $itinerary_id)
    {
        $itinerary = Itinerary::findOrFail($itinerary_id);
        $itinerary->fill($request["itinerary"][0]);
        $itinerary->save();

        return response()->json([], 200);
    }

    public function storeService(StoreItineraryService $request, $itineraryId)
    {
        $itinerary = Itinerary::findOrFail($itineraryId);

        if (! $itinerary->addService($request->all()) ){
            throw new HttpResponseException(new JsonResponse(['message'=>'Unexpected Error'], 422));
        };
       
        return response()->json([
            'message' => 'Service added'],
            201
        );
    }

    public function storeQuotationService(Request $request, $itineraryId)
    {

        $itinerary = Itinerary::findOrFail($itineraryId);
        $serviceProvidersData = collect($request->services)->keyBy('service_provider_id');

        $providerServices = ProviderService::whereIn('id', $serviceProvidersData->pluck('service_provider_id'))->get();

        foreach ($providerServices as $providerService) {

            if (! $itinerary->existsItineraryProviderServices($providerService->provider_id, $providerService->service_id)) {

                $itinerary->addService(['provider_id'=>$providerService->provider_id,
                    'service_id'=>$providerService->service_id, 'procurement_quantity'=>1,
                    'billing_unit_price'=>$serviceProvidersData[$providerService->id]['amount'],
                    'service_origin'=>$serviceProvidersData[$providerService->id]['service_origin']]);
            }
        }

    }
    
    public function addService(StoreItineraryService $request, $itineraryId)
    {
        $itinerary = Itinerary::findOrFail($itineraryId);
        if ($itinerary->existsProviderServices($request->service_provider_id)) {
            throw new HttpResponseException('Service already exists');
        }
        $itinerary->addService($request->all());
        $itinerary = Itinerary::findOrFail($itineraryId);
        
        $transformer = new ItineraryProvidersTransformer($itinerary);
        $providers = $transformer->transformItems($itinerary->getProviderOfServices());
        
        return response()->json([
            'message' => 'Service added',
            'data' => ['providers' => $providers, 'total' => $itinerary->getTotal()],
            201
        ]);
    }
    
    public function createServiceAdditional($itinerary_id, $request)
    {
        $itinerary = Itinerary::findOrFail($itinerary_id);
        $additional = $itinerary->createAdditional($request);
        $additional->administration_fee = 0;
        $additional->has_bills = 0;
        $additional->save();
        
        return $this->servicesAdditionalsResponse($additional, 'Additional Created');
        
        
    }
    
    public function updateService(UpdateItineraryServicesRequest $request, $itinerary_id, $itinerary_provider_id)
    {
        if ($request->service_parent) {
            $ret = $this->updateServiceParent($request, $itinerary_provider_id);
        } else {
            $ret = $this->updateServiceChild($request, $itinerary_provider_id);
        }
        if ($ret) {
            return response()->json(['message'   => 'Service updated'], 200);
        }
        return response()->json(['message' =>"Service not updated"], 422);
    
    }
    
    private function updateServiceParent($request, $parentId)
    {
        if ($request->service_parent) {
            $itineraryParentService = ItineraryServiceParents::findOrFail($parentId);
            if(!$itineraryParentService->isContainedInStatement($request->get('billing-type'))){
                
                $itineraryParentService->fill($request->all());
                return $itineraryParentService->save();
            }
            return false;
        }
    }
    
    private function updateServiceChild($request, $servicesId)
    {
        $itineraryProviderService = ItineraryProviderService::findOrFail($servicesId);
        if(!$itineraryProviderService->isContainedInStatement($request->get('billing-type'))){
            return $itineraryProviderService->updateService($request);
        }
        return false;
    }
    
    
    public function updateQuotationService(UpdateItineraryServicesRequest $request, $itinerary_id, $itinerary_provider_id)
    {
        $itineraryProviderService = ItineraryProviderService::findOrFail($itinerary_provider_id);
        if(!$itineraryProviderService->isContainedInStatement($request->get('billing-type'))){
            if ($itineraryProviderService->updateService($request)) {
                return response()->json(['message'   => 'Service updated'], 200);
            }
        }
        return response()->json(['message' =>"Service already contained in statement"], 422);
    }
    
    public function deleteItineraryService($itinerary_id, $itinerary_provider_id)
    {
        $itineraryProviderService = ItineraryProviderService::findOrFail($itinerary_provider_id);


        if ($itineraryProviderService->deleteItem() ) {
            return response()->json([
                'message'   => 'Service deleted',
            ], 201);
        }
    
        return response()->json([
            'message'   => 'Error. Service is contained in Invoice, Bill or contains Additionals',
        ], 422);
    }
    
   
    
   
    
    
}
