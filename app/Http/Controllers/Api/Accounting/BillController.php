<?php
/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 3/28/18
 * Time: 2:34 PM
 */

namespace App\Http\Controllers\Api\Accounting;
use App\Http\Transformers\Accounting\CompanyStatementTransformer;



use App\Http\Controllers\Controller;
use App\Models\Accounting\Payment;
use Illuminate\Http\Request;
use App\Models\Accounting\Bill;
use App\Models\Accounting\ProviderStatement;


class BillController extends Controller
{
    public function delete(Request $request,  $statementId)
    {
        if ($request->type == 'Bill') {
            $bill = Invoice::findOrFail($statementId);
            $deleted = $bill->deleteItem();
        } else {
            $payment = Payment::findOrFail($statementId);
            $deleted = $payment->deleteItem();
        }

        if ($deleted) {
            return response()->json(['message' => 'Service deleted'], 201);
        }
        return response()->json(['message'   => 'Error deleting Statement '], 422);
    }

    public function getStatementBills(Request $request, $statementId)
    {
        $companyStatement = ProviderStatement::findOrFail($statementId);
        $transformer = new CompanyStatementTransformer($companyStatement->companyType);
        $data = $transformer->transform($companyStatement);

        return response()->json(['data'=>$data], 200);
    }
}