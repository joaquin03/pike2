<?php
/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 3/28/18
 * Time: 2:34 PM
 */

namespace App\Http\Controllers\Api\Accounting;
use App\Http\Transformers\Accounting\CompanyStatementTransformer;



use App\Http\Controllers\Controller;
use App\Models\Accounting\Payment;
use Illuminate\Http\Request;
use App\Models\Accounting\Invoice;
use App\Models\Accounting\ProviderStatement;


class InvoiceController extends Controller
{
    public function delete(Request $request,  $statementId)
    {
        if ($request->type == 'Invoice') {
            $bill = Invoice::findOrFail($statementId);
            $deleted = $bill->deleteItem();
        } else {
            $payment = Payment::findOrFail($statementId);
            $deleted = $payment->deleteItem();
        }

        if ($deleted) {
            return response()->json(['message' => 'Service deleted'], 201);
        }
        return response()->json(['message'   => 'Error deleting Statement '], 422);
    }

}