<?php

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Http\Requests\StoreItineraryService;
use App\Http\Transformers\Procurement\ItineraryProvidersTransformer;
use App\Models\Accounting\AdministrationFee;
use App\Models\Itinerary;
use App\Models\ItineraryProviderService;
use App\Models\Operation;
use App\Models\AircraftPermission;
use App\Models\Service;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Http\Requests\UpdateItineraryServicesRequest;
use App\Http\Transformers\Procurement\ProviderServicesTransformer;
use App\Http\Requests\UpdateOperationPermission;
use App\Http\Transformers\Procurement\PermitsTransformer;



class QuotationController extends Controller
{
   
    public function updateOperationPermission(UpdateOperationPermission $request, $operation_id)
    {
        $operation = Operation::findOrFail($operation_id);
        $operation->updateOperationPermission($request->all());

        $operationTransformer = new PermitsTransformer();
        $permits = $operationTransformer->transformItems($operation->aircraftPermission);

        return response()->json([
            'message' => 'Permit edited',
            'data' => ['permissions' => $permits], 201

        ]);
    }

    public function deletePermission($operation_id, $permission_id)
    {
        AircraftPermission::findOrFail($permission_id)->delete();
        $operationTransformer = new PermitsTransformer();
        $operation = Operation::findOrFail($operation_id);

        $permits = $operationTransformer->transformItems($operation->aircraftPermission);

        return response()->json([
            'message' => 'Permit edited',
            'data' => ['permissions' => $permits], 201

        ]);
    }
    
}