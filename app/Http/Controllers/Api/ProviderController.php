<?php
/**
 * Created by PhpStorm.
 * User: joaquinanduano
 * Date: 5/10/18
 * Time: 12:34 AM
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Provider;
use App\Models\Service;
use Illuminate\Http\Request;
use App\Http\Requests\AddServiceToProviderRequest;

class ProviderController extends Controller
{
    public function showServices(Request $request, $provider_id)
    {
        $provider = Provider::findOrFail($provider_id);
        if ($request->get('is_for_procurement', false)) {
            $services = $provider->serviceParents;
        }
    else {
            $services = $provider->serviceChild;
        }

        return response()->json($services);
    }

    public function showServicesQuotation(Request $request, $provider_id)
    {
        $provider = Provider::findOrFail($provider_id);

        if ($request->get('is_for_procurement', false)) {
            $services = $provider->serviceParents;
        }
        else {
            $services = $provider->serviceChildForQuotation;
        }

        return response()->json($services);
    }

}