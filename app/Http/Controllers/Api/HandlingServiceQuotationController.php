<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Aircraft;
use App\Models\HandlingServiceQuotation;
use Illuminate\Http\Request;

class HandlingServiceQuotationController extends Controller
{
    
    public function index($aircraftId)
    {
        //$this->validate($request, ['aircraft_id' => 'required']);
        
        $aircraft = Aircraft::with('type')->findOrFail($aircraftId);

        $price = HandlingServiceQuotation::getPrice($aircraft->type->MTOW);
        
        return response()->json(['price'=>$price], 200);
    }
}
