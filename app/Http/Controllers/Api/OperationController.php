<?php

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Http\Requests\StoreItineraryService;
use App\Http\Transformers\Procurement\ItineraryProvidersTransformer;
use App\Models\Accounting\AdministrationFee;
use App\Models\Itinerary;
use App\Models\ItineraryProviderService;
use App\Models\Operation;
use App\Models\AircraftPermission;
use App\Models\Service;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Http\Requests\UpdateItineraryServicesRequest;
use App\Http\Transformers\Procurement\ProviderServicesTransformer;
use App\Http\Requests\UpdateOperationPermission;
use App\Http\Transformers\Procurement\PermitsTransformer;



class OperationController extends Controller
{
    
    public function createItineraryService()
    {
        $itinerary = Itinerary::findOrFail($itinerary_id);
    
        if($itinerary->existsProviderServices($request->service_provider_id)){
            return response()->json(['message' => 'Service already exists'], 422);
        }
    
        $itinerary->addService($request->all());
    
        $transformer = new ProviderServicesTransformer();
        $providerServices = $itinerary->providerServices()->get();
        $providerServices = $transformer->transformItems($providerServices);
        return response()->json($providerServices, 200);
    }

    public function updateOperationPermission(UpdateOperationPermission $request, $operation_id)
    {
        $operation = Operation::withoutGlobalScopes()->findOrFail($operation_id);
        $operation->updateOperationPermission($request->all());

        $operationTransformer = new PermitsTransformer();
        $permits = $operationTransformer->transformItems($operation->aircraftPermission);

        return response()->json([
            'message' => 'Permit edited',
            'data' => ['permissions' => $permits], 201

        ]);
    }

    public function deletePermission($operation_id, $permission_id)
    {
        AircraftPermission::findOrFail($permission_id)->delete();
        $operationTransformer = new PermitsTransformer();
        $operation = Operation::findOrFail($operation_id);

        $permits = $operationTransformer->transformItems($operation->aircraftPermission);

        return response()->json([
            'message' => 'Permit edited',
            'data' => ['permissions' => $permits], 201

        ]);
    }
    
}