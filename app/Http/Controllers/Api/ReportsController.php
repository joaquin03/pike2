<?php

namespace App\Http\Controllers\Api;

use App\Helpers\ReportsGenerator;
use App\Helpers\TotalReportGenerator;
use App\Http\Controllers\Controller;
use App\Http\Transformers\Reports\BillReportsTransformer;
use App\Http\Transformers\Reports\InvoiceReportsTransformer;
use App\Http\Transformers\Reports\OperationsReportsTransformer;
use App\Http\Transformers\Reports\ProcurementReportsTransformer;
use Illuminate\Http\Request;

class ReportsController extends Controller
{
    public function operations(Request $request)
    {
        $report = new ReportsGenerator($request);
        $data = $report->reportOperation($request);
        
        $transformer = new OperationsReportsTransformer();
    
        $collect = collect($transformer->transformItems($data)['data']);
        return datatables()->collection($collect)->toJson(true);
    }
    
    public function procurement(Request $request)
    {
        $report = new ReportsGenerator($request);
        $data = $report->reportProcurement($request);
    

        $transformer = new ProcurementReportsTransformer();
    
        $collect = collect($transformer->transformItems($data)['data']);
        return datatables()->collection($collect)->toJson(true);
    }
    
    public function invoice(Request $request)
    {
        $report = new ReportsGenerator($request);
        $data = $report->reportInvoice($request);
    
        $transformer = new InvoiceReportsTransformer();
    
        $collect = collect($transformer->transformItems($data)['data']);
        
        return datatables()->collection($collect)->toJson(true);
    }

    public function bill(Request $request)
    {
        $report = new ReportsGenerator($request);
        $data = $report->reportBill($request);

        $transformer = new BillReportsTransformer();

        $collect = collect($transformer->transformItems($data)['data']);

        return datatables()->collection($collect)->toJson(true);
    }
    
    
    public function total(Request $request)
    {
        $report = new TotalReportGenerator();
        $expensesTotal = $report->expenses($request);
        $profitsTotal = $report->profits($request);
        $total = round($profitsTotal - $expensesTotal, 3);
    
        
        
        return response()->json([
            'total'             => $total,
            'total_expenses'    => $expensesTotal,
            'total_profit'      => $profitsTotal,
        ]);
    }
    
    public function totalByMonth(Request $request)
    {
        $report = new TotalReportGenerator();
        $totalByMonth = $report->totalsByMoth($request);
    
    
        return response()->json($totalByMonth);
    }
    
    
    
}
