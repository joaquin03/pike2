<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['prefix'=>'procurement/{id}'], function() {
    Route::get('/total', ['as'=>'api.procurement.total', 'uses'=>'ProcurementController@getTotal']);
    
    Route::put('provider-permits', ['as'=>'api.procurement.providerPermits', 'uses'=>'ProcurementController@getProviderPermits']);
    
    Route::put('/aircraft-permission/{permissionId}', ['as'=>'api.procurement.aircraft-permission.update', 'uses'=>'ProcurementController@aircraftPermissionUpdate']);

    Route::put('/permanent-permission/{permissionId}', ['as'=>'api.procurement.permanent-permission.update', 'uses'=>'ProcurementController@permanentPermissionUpdate']);
    
    Route::post('/permission/upload',   ['as'=>'api.procurement.permission.upload', 'uses'=>   'ProcurementController@upload']);
    
    Route::post('add-invoice', ['as' => 'api.procurement.add-invoice', 'uses' => 'ProcurementController@addInvoice']);
    Route::post('/permanent-permit/add-invoice', ['as' => 'api.procurement..permanent-permit.add-invoice', 'uses' => 'ProcurementController@addPermanentPermitsInvoice']);
    
    Route::post('/additional/add-invoice', ['as' => 'api.procurement.add-additional-invoice', 'uses' => 'ProcurementController@addAdditionalInvoice']);
    Route::post('/permit/additional/add-invoice', ['as' => 'api.procurement.permit.add-additional-invoice', 'uses' => 'ProcurementController@addPermitAdditionalInvoice']);
    
    
    Route::delete('/service/{service_id}', ['as'=>'crud.procurement.service.delete', 'uses'=>'ProcurementController@deleteItineraryService']);
    Route::delete('/permit/{permit_id}', ['as'=>'crud.procurement.permit.delete', 'uses'=>'ProcurementController@deleteOperationPermit']);
    Route::delete('/permanentPermit/{permanentPermit_id}', ['as'=>'crud.procurement.permanentPermit.delete', 'uses'=>'ProcurementController@deleteOperationPermanentPermit']);

});

Route::group(['prefix'=>'operation/{operation_id}/permission'], function(){

    Route::get('/{permission_id}/delete', ['as'=>'crud.permission.delete', 'uses'=>'OperationController@deletePermission']);
    Route::post('/update', ['as'=>'crud.permission.update', 'uses'=>'OperationController@updateOperationPermission']);
    Route::put('/update', ['as' => 'crud.permission-status.update', 'uses' => 'OperationController@updatePermissionStatus']);
});

Route::group(['prefix'=>'quotation/{quotation_id}/permission'], function(){
    
    Route::get('/{permission_id}/delete', ['as'=>'crud.permission.delete', 'uses'=>'QuotationController@deletePermission']);
    Route::post('/update', ['as'=>'crud.quotation-permission.update', 'uses'=>'QuotationController@updateOperationPermission']);
    Route::put('/update', ['as' => 'crud.quotation-permission-status.update', 'uses' => 'QuotationController@updatePermissionStatus']);
});

Route::group(['prefix'=>'/itinerary/{id}'], function() {
    Route::put('', ['as'=>'crud.operation.itinerary.update', 'uses'=>'ItineraryController@update']);
    Route::group(['prefix'=>'/provider'], function() {
        Route::get('/', ['as'=>'api.procurement.itinerary.provider', 'uses'=>'ProcurementController@itineraryProviders']);
        
        Route::get('/{providerId}', ['as'=>'api.procurement.itinerary.provider.services', 'uses'=>'ProcurementController@itineraryProvidersServices']);
        Route::delete('{service_id}', ['as'=>'crud.service.itinerary.delete', 'uses'=>'ItineraryController@deleteItineraryService']);

    });

    Route::post('/generate-quotation-services', ['as'=>'api.itinerary.storeQuotationService', 'uses'=>'ItineraryController@storeQuotationService']);


    Route::group(['prefix'=>'/service'], function() {
    
        Route::get('', ['as'=>'api.itinerary.showServices', 'uses'=>'ItineraryController@showServices']);
        Route::post('', ['as'=>'api.itinerary.storeService', 'uses'=>'ItineraryController@storeService']);
        Route::put('{itinerary_provider_id}', ['as'=>'api.procurement.itinerary.updateService', 'uses'=>'ItineraryController@updateService']);
    
        Route::put('/quotation/{itinerary_provider_id}', ['as'=>'api.procurement.itinerary.updateQuotationService', 'uses'=>'ItineraryController@updateQuotationService']);
    
        //Route::post('', ['as'=>'api.procurement.itinerary.addServices', 'uses'=>'ItineraryController@storeService']);
    
        
//        Route::post('/service/update', ['as'=>'crud.service.itinerary.update', 'uses'=>'ItineraryCrudController@updateItineraryService']);

        Route::delete('{service_id}', ['as'=>'crud.service.itinerary.delete', 'uses'=>'ItineraryController@deleteItineraryService']);
  
        Route::put('category', ['as'=>'api.procurement.itinerary.updateCategoryService', 'uses'=>'ProcurementController@updateCategoryService']);
       
        Route::put('fee', ['as'=>'api.procurement.itinerary.updateAdministrationFee', 'uses'=>'ProcurementController@updateAdministrationFee']);
        Route::get('fee', ['as'=>'api.procurement.itinerary.showAdministrationFee', 'uses'=>'ProcurementController@showAdministrationFee']);
    
        Route::get('{service}/delete', ['as'=>'api.procurement.itinerary.service.delete', 'uses'=>'ProcurementController@updateService']);

        Route::put('/update', ['as'=>'crud.service.itinerary.update', 'uses'=>'ItineraryController@updateItineraryService']);

    });
    
   
    Route::put('/administration-fee', ['as'=>'api.itinerary.administration-fee', 'uses'=>'ItineraryController@updateAdministrationFee']);
    
    Route::get('/total', ['as'=>'api.itinerary.total', 'uses'=>'ItineraryController@getTotal']);
    
    Route::post('/provider-service/{providerId}/upload',   ['as'=>'api.itinerary.provider-service.upload', 'uses'=>   'ItineraryController@upload']);
    
});

Route::group(['prefix'=>'/administration_fee/{admFee_id}'], function() {
    Route::delete('/', ['as'=>'api.itinerary.administration-fee.delete', 'uses'=>'ProcurementController@deleteAdministrationFee']);
    Route::put('/permit/fee', ['as'=>'api.procurement.itinerary.updatePermitsAdministrationFee', 'uses'=>'ProcurementController@updatePermitsAdministrationFee']);
    Route::get('/permit/fee', ['as'=>'api.procurement.operation.showPermitsAdministrationFee', 'uses'=>'ProcurementController@showPermitsAdministrationFee']);
    
    Route::put('/permanent_permit/fee', ['as'=>'api.procurement.itinerary.updatePermanentPermitsAdministrationFee', 'uses'=>'ProcurementController@updatePermanentPermitsAdministrationFee']);
    Route::get('/permanent_permit/fee', ['as'=>'api.procurement.operation.showPermanentPermitsAdministrationFee', 'uses'=>'ProcurementController@showPermanentPermitsAdministrationFee']);
    
});


Route::group(['prefix'=>'/billing/{id}'], function() {
    Route::put('itinerary/{itineraryId}/percentage',
        ['as' => 'api.billing.itinerary.updatePercentage', 'uses' => 'BillingController@updateBillingPercentage']);
    Route::get('itinerary/{itineraryId}/info',
        ['as' => 'api.billing.itinerary.billingInfo', 'uses' => 'BillingController@getItineraryBillingInfo']);
    
    Route::put('itinerary/{itineraryId}/info',
        ['as' => 'api.billing.itinerary.billingInfo', 'uses' => 'BillingController@updateItineraryBillingInfo']);
    
    Route::post('create-bill', ['as' => 'api.billing.create-bill', 'uses' => 'BillingController@createBill']);

});

Route::get('billing/search', ['as' => 'api.billing.search', 'uses' => 'BillingController@search']);

Route::get('/permission/getUpload',          'ProcurementController@getUpload');

Route::put('/airport/{airport_id}/update-provider',          'AirportController@updateProvider');

Route::post('operation/{operation_id}/OVF-permit', ['as'=>'operation.permit.create', 'uses'=>'ProcurementController@createOVFPermit']);
Route::post('operation/{operation_id}/OVF-permanent-permit', ['as'=>'operation.permanent-permit.create', 'uses'=>'ProcurementController@createOVFPermanentPermit']);



Route::namespace('Accounting')->group(function () {
    Route::delete('invoice/{bill_id}', 'InvoiceController@delete');
    Route::get('provider-statement/{statement_id}', 'BillController@getStatementBills');

});


Route::group(['prefix'=>'/additional'], function() {
    //SERVICES
    Route::get('/service/',
        ['as' => 'api.permit.additional.service.show', 'uses' => 'ProcurementController@showServiceAdditionals']);
    Route::post('/service/{service_id}',
        ['as' => 'api.permit.additional.service.create', 'uses' => 'ProcurementController@createServiceAdditional']);
    Route::put('/service/{service_id}',
        ['as' => 'api.permit.additional.service.update', 'uses' => 'ProcurementController@updateServiceAdditional']);
    Route::delete('/service/{service_id}',
        ['as' => 'api.service.additional.permit.delete', 'uses' => 'ProcurementController@deleteServiceAdditional']);



    //PERMITS
    Route::get('/permit/',
        ['as' => 'api.permit.additional.permit.show', 'uses' => 'ProcurementController@showPermitAdditionals']);
    Route::post('/permit/{permit_id}',
        ['as' => 'api.permit.additional.permit.create', 'uses' => 'ProcurementController@createPermitAdditional']);
    Route::put('/permit/{permit_id}',
        ['as' => 'api.permit.additional.permit.update', 'uses' => 'ProcurementController@updatePermitAdditional']);
    Route::get('/permit/{permit_id}',
        ['as' => 'api.permit.additional.permit.delete', 'uses' => 'ProcurementController@deletePermitAdditional']);


    //PERMANENT PERMITS
    Route::get('/permanent-permit/{provider_id}',
        ['as' => 'api.permanent.permit.additional.permit.show', 'uses' => 'ProcurementController@showPermanentPermitAdditionals']);
    Route::post('/permanent-permit/{permit_id}/operation/{operation_id}',
        ['as' => 'api.permanent.permit.additional.permit.create', 'uses' => 'ProcurementController@createPermanentPermitAdditional']);
    Route::put('/permanent-permit/{permit_id}/operation/{operation_id}',
        ['as' => 'api.permanent.permit.additional.permit.update', 'uses' => 'ProcurementController@updatePermanentPermitAdditional']);
    Route::delete('/permanent-permit/{permit_id}',
            ['as' => 'api.permanent.permit.additional.permit.delete', 'uses' => 'ProcurementController@deletePermanentPermitAdditional']);
});


Route::group(['prefix'=>'/provider'], function() {
    Route::get('/{id}/service', 'ProviderController@showServices');

    Route::get('/{id}/quotation-service', 'ProviderController@showServicesQuotation');
});



Route::group(['prefix'=>'/reports'], function() {
    Route::get('/operation', 'ReportsController@operations');
    Route::get('/procurement', 'ReportsController@procurement');
    Route::get('/invoice', 'ReportsController@invoice');
    Route::get('/bill', 'ReportsController@bill');

    Route::get('/total', 'ReportsController@total');
    Route::get('/total-by-month', 'ReportsController@totalByMonth');
});

Route::group(['prefix'=>'/handling-service'], function() {
    Route::get('/aircraft/{id}/price', 'HandlingServiceQuotationController@index');
});