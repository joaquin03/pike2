<?php

CRUD::resource('aircraft-type',        'AircraftTypeCrudController');
CRUD::resource('aircraft',             'AircraftCrudController');
CRUD::resource('airport',              'AirportCrudController');
CRUD::resource('client',               'ClientCrudController');
CRUD::resource('crew-member',          'CrewMemberCrudController');
CRUD::resource('contact',              'ContactCrudController');
CRUD::resource('permanent-permission', 'PermanentPermissionCrudController');

CRUD::resource('user',                 'UserCrudController');
CRUD::resource('permission',           'PermissionCrudController');
CRUD::resource('role',                 'RoleCrudController');

CRUD::resource('company',              'CompanyCrudController');
CRUD::resource('operator',             'OperatorCrudController');
CRUD::resource('provider',             'ProviderCrudController');


CRUD::resource('service-operation',    'ServiceOperationCrudController');
CRUD::resource('service-procurement',  'ServiceProcurementCrudController');
CRUD::resource('service-category',     'ServiceCategoryCrudController');
CRUD::resource('aircraft-default-service-price', 'AircraftDefaultServicePriceCrudController');

CRUD::resource('sales-manager', 'SalesManagerCrudController');

CRUD::resource('report-crm', 'CRM\ReportCRMController');

Route::namespace('Accounting')->group(function () {
    CRUD::resource('bills', 'BillCrudController');
    CRUD::resource('client-statement', 'CompanyStatementCrudController');
    CRUD::resource('provider-statement', 'ProviderStatementCrudController');
    CRUD::resource('credit-notes', 'CreditNoteCrudController');
    
    Route::group(['namespace'=>'Providers'], function() {
        CRUD::resource('bills-provider', 'InvoiceCrudController');
    });
});

CRUD::resource('handling-service', 'HandlingServiceQuotationCrudController');