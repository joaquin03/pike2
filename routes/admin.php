<?php

use Illuminate\Foundation\Bus\DispatchesJobs;
use Vinkla\Pusher\Facades\Pusher;

Route::get('/', function () {
    return redirect()->to('admin/login');
});

Route::get('/pusher', function () {
    //Pusher::trigger('users', 'view-operation', ['user' => Auth::user()]);
    //Pusher::getSettings();

    return view('pusher');

});


Route::post('/pusher/auth', 'PusherController@postAuth');

Route::get('/pusher/pusher-auth', function() {
    return view('pusher-auth');

});


Route::get('/refactorOldAccountingServicesProcurementAmounts', function(){
    $invoices = \App\Models\Accounting\Invoice::all();

    foreach ($invoices as $invoice){
        foreach ($invoice->accountingServices as $accountingService) {
            $accountingService->procurement_unit_final_cost =  $accountingService->procurement_cost;
            $accountingService->procurement_final_cost = $accountingService->billing_final_price;
            $accountingService->save();
        }
    }
    return "OK";
});

//Function made because doing it all at once references the values and deletes them all
Route::get('/deleteOldAccountingServicesProcurementAmounts', function(){
    $invoices = \App\Models\Accounting\Invoice::all();

    foreach ($invoices as $invoice){
        foreach ($invoice->accountingServices as $accountingService) {
            $accountingService->procurement_cost = null;
            $accountingService->billing_final_price = null;
            $accountingService->save();
        }
    }
    return "OK";
});

//Transfer the old service parents info to the new ones
Route::get('/loadNewServiceParents', function(){
    $ipss = \App\Models\ItineraryProviderService::where('parent_id', null)->get();

    foreach ($ipss as $ips){
        \App\Models\ItineraryServiceParents::createItem($ips);
    }
    return "OK";
});



Route::get('/garino', function () {
    $bill = App\Models\Accounting\Bill::findOrFail(95);
    $helper = App\Helpers\GarinoSFLFile::generateXML($bill);
    return view('garino', compact('helper'))->withHeaders([
        'Content-Type' => 'text/xml'
    ]);
   // return $helper;
});

Route::get('/garino-generateXML', function () {
    $bill = App\Models\Accounting\Bill::findOrFail(95);
   // $helper = App\Helpers\GarinoGenerator::generateXML($bill);
    //return $helper;
});

    Route::get('/garino-generateXML/credit-note', function () {
    $debitNote = App\Models\Accounting\CreditNote::findOrFail(32);
    //$helper = App\Helpers\GarinoGenerator::generateXML($debitNote);
    //return $helper;
});

Route::get('/garino-generate', function () {
    $bill = App\Models\Accounting\Bill::findOrFail(301);
    $helper = App\Helpers\GarinoGenerator::generate($bill);
    return $helper;
});

Route::get('/garino-generate/credit-note', function () {
    $debitNote = App\Models\Accounting\CreditNote::findOrFail(30);
    $helper = App\Helpers\GarinoGenerator::generate($debitNote);
    return $helper;
});

Route::get('/admin', function () {
    if (\Auth::check()) {
        return redirect()->to('admin/dashboard');
    }
    return redirect()->to('admin/login');
});

Route::get('dashboard',                'DashboardController@index');

Route::get('/airport/upload',              'AirportCrudController@upload');
Route::post('/airport/upload',             'AirportCrudController@uploadExcel');
Route::get('/aircraft/info/{aircraft_registration}', ['as'   => 'crud.aircraft.info', 'uses' => 'AircraftCrudController@info']);
Route::get('/aircraft/type/info/{aircraft_type}', ['as'   => 'crud.aircraft.type.info', 'uses' => 'AircraftCrudController@typeInfo']);
Route::get('/airport/handlers/{airport_icao}', ['as'   => 'crud.airport.handlers', 'uses' => 'AirportCrudController@handlers']);
Route::get('/company/alert/special-features/{company_id}', ['as'   => 'crud.company.alert-special-features', 'uses' => 'CompanyCrudController@alertSpecialFeatures']);


CRUD::resource('aircraft-type',        'AircraftTypeCrudController');
CRUD::resource('aircraft',             'AircraftCrudController');
CRUD::resource('airport',              'AirportCrudController');
CRUD::resource('client',               'ClientCrudController');
CRUD::resource('crew-member',          'CrewMemberCrudController');
CRUD::resource('contact',              'ContactCrudController');
CRUD::resource('permanent-permission', 'PermanentPermissionCrudController');

CRUD::resource('user',                 'UserCrudController');
CRUD::resource('permission',           'PermissionCrudController');
CRUD::resource('role',                 'RoleCrudController');

CRUD::resource('company',              'CompanyCrudController');
CRUD::resource('company-billing-info', 'CompanyBillingInfoCrudController');
CRUD::resource('operator',             'OperatorCrudController');
CRUD::resource('provider',             'ProviderCrudController');
Route::get('dashboard',                'DashboardController@index');

CRUD::resource('service-operation',    'ServiceParentOperationCrudController');
CRUD::resource('service-procurement',  'ServiceProcurementCrudController');
CRUD::resource('service-category',     'ServiceCategoryCrudController');
CRUD::resource('aircraft-default-service-price', 'AircraftDefaultServicePriceCrudController');
CRUD::resource('service-pike', 'ServiceChildPikeCrudController');
CRUD::resource('credit-card', 'CreditCardCrudController');

CRUD::resource('company-tag', 'CompanyTagCrudController');

CRUD::resource('report', 'ReportCrudController');
CRUD::resource('currency', 'CurrencyCrudController');


Route::namespace('Accounting')->group(function () {
    CRUD::resource('bills', 'BillCrudController');
    CRUD::resource('invoices', 'InvoiceCrudController');
    
    Route::get('/providers-payments/report', ['as'=>'crud.provider-payments.index', 'uses'=>'Providers\PaymentReportCrudController@index']);
    
    Route::get('/invoice/{bill_id}/delete', 'ProviderStatementCrudController@delete');

    Route::get('/invoice/report', 'InvoiceCrudController@report');

    Route::get('/bill/report', 'BillCrudController@report');

    Route::group(['prefix' => 'bills'], function() {
        Route::get('/{id}/show', ['as'=>'crud.clients.bills.show', 'uses'=>'BillCrudController@show']);
        Route::get('/delete-all-bills', ['as'=>'crud.clients.bills.delete.all', 'uses'=>'BillCrudController@deleteAllbills']);
        Route::get('/amount/{bill_id}', ['as'   => 'crud.bill.amount', 'uses' => 'BillCrudController@amount']);

        Route::get('/{id}/cancel', ['as'=>'crud.clients.bills.cancel', 'uses'=>'BillCrudController@cancel']);
        Route::get('/{id}/invalidate', ['as'=>'crud.clients.bills.invalidate', 'uses'=>'BillCrudController@invalidate']);


        Route::get('/{id}/debit-note',  ['as'=>'crud.clients.bills.debit-note.create', 'uses'=>'BillCrudController@debitNoteCreate']);
        Route::post('/{id}/debit-note', ['as'=>'crud.clients.bills.debit-note.store', 'uses'=>'BillCrudController@debitNoteStore']);


        Route::post('/{id}/update-debit-note-data', ['as'=>'crud.clients.bills.dollar-change.update', 'uses'=>'BillCrudController@updateDebitNoteData']);

    });

    Route::get('invoices/{id}/show', ['as'=>'crud.providers.invoices.show', 'uses'=>'InvoiceCrudController@show']);
    Route::get('/invoice/amount/{invoice_id}', ['as'   => 'crud.invoice.amount', 'uses' => 'InvoiceCrudController@amount']);
    Route::get('provider-statement/{id}/export-table', 'ProviderStatementCrudController@exportTable');
    
    
    
   
    
    Route::group(['prefix' => '/clients/{client_id}'], function() {
        Route::get('payments', ['as'=>'crud.clients.payments.show', 'uses'=>'PaymentCrudController@show']);
        Route::get('payments', ['as'=>'crud.clients.payments.create', 'uses'=>'PaymentCrudController@create']);

        Route::post('payments', ['as'=>'crud.clients.payments.store', 'uses'=>'PaymentCrudController@store']);

        Route::get('payments/{id}', ['as'=>'crud.clients.payments.edit', 'uses'=>'PaymentCrudController@edit']);
        Route::put('payments/{id}', ['as'=>'crud.clients.payments.update', 'uses'=>'PaymentCrudController@update']);

        //Over Payments
        Route::get('over-payments/', ['as'=>'crud.clients.overPayments.create', 'uses'=>'OverPaymentCrudController@create']);
        Route::post('over-payments/', ['as'=>'crud.clients.overPayments.store', 'uses'=>'OverPaymentCrudController@store']);

        Route::get('over-payments/{id}', ['as'=>'crud.clients.overPayments.edit', 'uses'=>'OverPaymentCrudController@edit']);
        Route::put('over-payments/{id}', ['as'=>'crud.clients.overPayments.update', 'uses'=>'OverPaymentCrudController@update']);

        //credit note
        Route::get('credit-notes/{credit_note_id}/show', ['as'=>'crud.clients.credit-notes.show', 'uses'=>'CreditNoteCrudController@show']);
        Route::get('credit-notes', ['as'=>'crud.clients.credit-notes.create', 'uses'=>'CreditNoteCrudController@create']);

        Route::post('credit-notes', ['as'=>'crud.clients.credit-notes.store', 'uses'=>'CreditNoteCrudController@store']);

        Route::get('credit-notes/{id}', ['as'=>'crud.clients.credit-notes.edit', 'uses'=>'CreditNoteCrudController@edit']);
        Route::put('credit-notes/{id}', ['as'=>'crud.clients.credit-notes.update', 'uses'=>'CreditNoteCrudController@update']);


        //add bills to credit note
        Route::post('credit-note-bills', ['as'=>'crud.clients.credit-notes.bills.create', 'uses'=>'CreditNoteCrudController@addBill']);
        Route::get('credit-note-bills', ['as'=>'crud.clients.credit-notes.bills.edit', 'uses'=>'CreditNoteCrudController@editBill']);
    
        //Debit Notes
        Route::get('debit-notes', ['as'=>'crud.clients.debit-notes.edit', 'uses'=>'DebitNoteCrudController@create']);
        Route::post('debit-notes', ['as'=>'crud.clients.debit-notes.create', 'uses'=>'DebitNoteCrudController@store']);
    
        Route::get('debit-notes/{id}/show', ['as'=>'crud.clients.debit-notes.show', 'uses'=>'DebitNoteCrudController@showItem']);
        

    });

    Route::put('/credit-notes/assigned-bill/{id}', ['as'=>'crud.clients.credit-notes.update-assigned-bill', 'uses'=>'CreditNoteCrudController@updateAssignedBill']);


    Route::group(['prefix' => '/providers/{client_id}', 'namespace'=>'Providers'], function() {

        Route::get('payments', ['as'=>'crud.providers.payments.create', 'uses'=>'PaymentProviderCrudController@create']);

        Route::post('payments', ['as'=>'crud.providers.payments.store', 'uses'=>'PaymentProviderCrudController@store']);

        Route::get('payments/{id}', ['as'=>'crud.providers.payments.edit', 'uses'=>'PaymentProviderCrudController@edit']);
        Route::put('payments/{id}', ['as'=>'crud.providers.payments.update', 'uses'=>'PaymentProviderCrudController@update']);

        //Over Payments
        Route::get('over-payments/', ['as'=>'crud.providers.overPayments.create', 'uses'=>'OverPaymentProviderCrudController@create']);
        Route::post('over-payments/', ['as'=>'crud.providers.overPayments.store', 'uses'=>'OverPaymentProviderCrudController@store']);

        Route::get('over-payments/{id}', ['as'=>'crud.providers.overPayments.edit', 'uses'=>'OverPaymentProviderCrudController@edit']);
        Route::put('over-payments/{id}', ['as'=>'crud.providers.overPayments.update', 'uses'=>'OverPaymentProviderCrudController@update']);


        //credit note
        Route::get('credit-notes', ['as'=>'crud.providers.credit-notes.show', 'uses'=>'ProviderCreditNoteCrudController@show']);
        Route::get('credit-notes', ['as'=>'crud.providers.credit-notes.create', 'uses'=>'ProviderCreditNoteCrudController@create']);

        Route::post('credit-notes', ['as'=>'crud.providers.credit-notes.store', 'uses'=>'ProviderCreditNoteCrudController@store']);

        Route::get('credit-notes/{id}', ['as'=>'crud.providers.credit-notes.edit', 'uses'=>'ProviderCreditNoteCrudController@edit']);
        Route::put('credit-notes/{id}', ['as'=>'crud.providers.credit-notes.update', 'uses'=>'ProviderCreditNoteCrudController@update']);

        //add bills to credit note
        Route::post('credit-note-bills', ['as'=>'crud.providers.credit-notes.bills.create', 'uses'=>'ProviderCreditNoteCrudController@addBill']);
        Route::get('credit-note-bills', ['as'=>'crud.providers.credit-notes.bills.edit', 'uses'=>'ProviderCreditNoteCrudController@editBill']);

    });
    
    
    Route::get('/provider-statement-report',
        ['as'=>'crud.provider-statement.report', 'uses'=>'Providers\ProviderStatementReportCrudController@report']);

    Route::get('/providers-balance',
        ['as'=>'crud.providers.balance', 'uses'=>'Providers\ProvidersBalanceCrudController@report']);

    Route::get('/clients-balance',
        ['as'=>'crud.clients.balance', 'uses'=>'ClientsBalanceCrudController@report']);


    Route::get('/client-statement-report', ['as'=>'crud.client-statement.report', 'uses'=>'CompanyStatementReportCrudController@report']);

    Route::get('client-statement/{id}/export-table', 'CompanyStatementCrudController@exportTable');
    CRUD::resource('client-statement', 'CompanyStatementCrudController');
    CRUD::resource('provider-statement', 'ProviderStatementCrudController');
    Route::get('update-all-payments', ['as'=>'crud.providers.payments.update-all', 'uses'=>'PaymentCrudController@updateAllPaymentsStatus']);
    
    
    
    
});
Route::group(['prefix' => 'provider-statement'], function() {
    Route::get('/service/{itineraryServiceId}/', 'ItineraryCrudController@getStatementFromService');
    Route::get('/permission/{permissionId}/', 'OperationCrudController@getStatementFromPermission');
    Route::get('/permanent-permission/{permissionId}/', 'OperationCrudController@getStatementFromPermanentPermission');

    Route::get('/additional/{additionalId}/', 'ItineraryCrudController@getStatementFromAdditional');



});

Route::get('/provider-services-2', function(){
    $p = App\Models\Provider::all();

    $services = App\Models\Service::all();

    foreach ($p as $item) {
        foreach($services as $service){
            if(!$item->services->contains($service->id)){
                $item->services()->attach($service->id);
            }
        }
    }
    return "OK";
});








Route::group(['prefix' => 'company/{company_id}'], function() {

    CRUD::resource('activity-email',            'ActivityEmailCrudController');
    CRUD::resource('activity-meeting',          'ActivityMeetingCrudController');
    CRUD::resource('activity-call',             'ActivityCallCrudController');
    CRUD::resource('activity-note',             'ActivityNoteCrudController');
    CRUD::resource('activity-reminder',         'ActivityReminderCrudController');
    CRUD::resource('activity-file',             'ActivityFileCrudController');
    CRUD::resource('activity-special-features',  'ActivitySpecialFeatureCrudController');

    Route::get('/', ['as'=>'crud.company.show', 'uses'=>'CompanyCrudController@show']);

    Route::get('/info', ['as'=>'crud.company.info', 'uses'=>'CompanyCrudController@info']);
    Route::post('/edit', ['as'=>'crud.company.update', 'uses'=>'CompanyCrudController@update']);
    Route::post('/add-competitor/{competitor_id}', ['as'=>'crud.company.add-competitor', 'uses'=>'CompanyCrudController@addCompetitor']);
    Route::delete('/delete-competitor/{competitor_id}', ['as'=>'crud.company.delete-competitor', 'uses'=>'CompanyCrudController@deleteCompetitor']);


    Route::get('reminder/{reminder}/finish', [
        'as'   => 'crud.activityReminder.finish',
        'uses' => 'ActivityReminderCrudController@finishReminder'
    ]);
    Route::get('reminder/{reminder_id}', [
        'as' => 'crud.activityReminder.remove',
        'uses' => 'ActivityReminderCrudController@delete'
    ]);

    Route::get('reminder/{reminder_id}/edit', [
        'as' => 'crud.activityReminder.edit',
        'uses' => 'ActivityReminderCrudController@edit'
    ]);
});

Route::group(['prefix' => 'provider/{company_id}'], function() {
    Route::post('/services', [
        'as'   => 'crud.providers.services.add',
        'uses' => 'ServiceCrudController@addServiceToProvider'
    ]);

    Route::get('/services', [
        'as'=>'api.provider.services.list',
        'uses'=>'ProviderCrudController@apiServiceList'
    ]);

    Route::get('/services/{service_id}', [
        'as'   => 'crud.providers.services.remove',
        'uses' => 'ServiceCrudController@deleteServiceToProvider'
    ]);

    Route::post('/airport', [
        'as'   => 'crud.providers.airport.add',
        'uses' => 'AirportCrudController@addAirportToProvider'
    ]);

    Route::get('/airport/{airport_icao}', [
        'as'   => 'crud.providers.airport.remove',
        'uses' => 'AirportCrudController@deleteAirportToProvider'
    ]);



    Route::post('/price', [
        'as'   => 'crud.providers.price.add',
        'uses' => 'ProviderCrudController@addPriceToProvider'
    ]);

    Route::put('/price/{price_id}', [
        'as'   => 'crud.providers.price.update',
        'uses' => 'ProviderCrudController@updatePriceToProvider'
    ]);

    Route::delete('/price/{price_id}', [
        'as'   => 'crud.providers.price.delete',
        'uses' => 'ProviderCrudController@deletePriceToProvider'
    ]);
});

Route::group(['prefix' => 'airport/{airport_icao}'], function() {

    Route::get('/', ['as'=>'crud.airport.show', 'uses'=>'AirportCrudController@show']);

    Route::post('/provider', [
        'as'   => 'crud.airport.provider.add',
        'uses' => 'ProviderCrudController@addProviderToAirport'
    ]);

    Route::get('/provider/{provider_id}/delete', [
        'as'   => 'crud.airport.provider.remove',
        'uses' => 'ProviderCrudController@deleteProviderToAirport'
    ]);

});

Route::group(['prefix' => 'crew-member/{crew-member_id}'], function() {
    Route::get('/', ['as'=>'crud.crew-member.show', 'uses'=>'CrewMemberCrudController@show']);
});

Route::group(['prefix'=>'quotation'], function() {
    Route::get('/{quotation_id}/export', ['as'=>'crud.quotation.report', 'uses'=>'QuotationCrudController@export']);
    Route::get('/integrate-sheet/{itinerary_id}', ['as'=>'crud.quotation.integrate', 'uses'=>'QuotationCrudController@integrateWithGoogleSheet']);
    Route::post('/route-map-upload', ['as'=>'quotation.route-map.upload', 'uses'=>'QuotationCrudController@uploadRouteMap']);

    Route::post('/{quotation_id}/document-save', ['as'=>'quotation.document.save', 'uses'=>'QuotationCrudController@saveQuotationDocument']);
    Route::get('/{quotation_id}/document-show', ['as'=>'quotation.document.show', 'uses'=>'QuotationCrudController@showQuotationDocument']);


    Route::get('/report/search', ['as'=>'crud.quotation.list-report', 'uses'=>'QuotationReportCrudController@index']);

    Route::get('update-all-quotations', ['as' => 'crud.quotation.update-all-quotations', 'uses' => 'QuotationCrudController@updateAllQuotations']);
});




Route::group(['prefix'=>'operation'], function(){

    Route::get('/report', ['as'=>'crud.operation.report', 'uses'=>'OperationCrudController@report']);

    Route::get('/', ['as'=>'crud.operation.create', 'uses'=>'OperationCrudController@create']);
    Route::resource('/', 'OperationCrudController');
    Route::post('/', ['as'=>'crud.operation.store', 'uses'=>'OperationCrudController@store']);
    Route::get('/screen/', ['as'=>'crud.operation.screen', 'uses'=>'OperationCrudController@screen']);
    Route::get('/reset_start_dates', ['as'=>'crud.operation.reset.start.dates', 'uses'=>'OperationCrudController@resetStartDates']);


    Route::get('/{operation_id}/edit', ['as'=>'crud.operation.edit', 'uses'=>'OperationCrudController@edit']);
    Route::post('/{operation_id}/edit', ['as'=>'crud.operation.update', 'uses'=>'OperationCrudController@update']);
    Route::get('/{id}/delete/', ['as'=>'crud.operation.delete', 'uses'=>'OperationCrudController@delete']);
    Route::get('/{id}/cancel/', ['as'=>'crud.operation.cancel', 'uses'=>'OperationCrudController@cancel']);
    Route::get('/{operation_id}/total', ['as'=>'crud.operation.total', 'uses'=>'OperationCrudController@getTotalCost']);

    Route::get('/{id}/revisions', ['as' => 'crud.operation.listRevisions', 'uses' => 'OperationCrudController@listRevisions']);
    Route::post('/{id}/revisions/{revisionId}/restore', ['as' => 'crud.operation.restoreRevision','uses' => 'OperationCrudController@restoreRevision']);

    Route::group(['prefix'=>'{operation_id}/itinerary'], function(){
        Route::get('/create', ['as'=>'crud.operation.itinerary.create', 'uses'=>'ItineraryCrudController@create']);
        Route::post('/create', ['as'=>'crud.operation.itinerary.store', 'uses'=>'ItineraryCrudController@store']);

        Route::get('{itinerary_id}/delete', ['as'=>'crud.operation.itinerary.delete', 'uses'=>'ItineraryCrudController@delete']);
        Route::get('{itinerary_id}/cancel', ['as'=>'crud.operation.itinerary.cancel', 'uses'=>'ItineraryCrudController@cancel']);
    });

    Route::group(['prefix'=>'{operation_id}/quotation-itinerary'], function(){
        Route::get('/create', ['as'=>'crud.quotation.itinerary.create', 'uses'=>'ItineraryCrudController@createQuotationItinerary']);
        Route::post('/create', ['as'=>'crud.quotation.itinerary.store', 'uses'=>'ItineraryCrudController@storeQuotationItinerary']);


    });

    Route::group(['prefix'=>'{operation_id}/permission'], function(){
        Route::get('/create', ['as'=>'crud.permission.create', 'uses'=>'OperationCrudController@createPermission']);
        Route::post('/create', ['as'=>'crud.permission.store', 'uses'=>'OperationCrudController@storePermission']);
        Route::get('', ['as'=>'crud.ovf-permission.list', 'uses'=>'OperationCrudController@getOverFlightPermissions']);

        Route::get('/{permission_id}/delete', ['as'=>'crud.permission.delete', 'uses'=>'OperationCrudController@deletePermission']);
        Route::post('/update', ['as'=>'crud.permission.update', 'uses'=>'OperationCrudController@updateOperationPermission']);
        Route::put('/update', ['as' => 'crud.permission-status.update', 'uses' => 'OperationCrudController@updatePermissionStatus']);
    });

    Route::group(['prefix'=>'{operation_id}/quotation/permission'], function() {
        Route::get('', ['as'=>'crud.permission.show', 'uses'=>'OperationCrudController@showPermissionsQuotation']);

    });

        Route::group(['prefix'=>'{operation_id}/permanent-permission'], function(){
        Route::post('/store', ['as' => 'crud.operation.permanent-permission.store', 'uses' => 'OperationCrudController@storePermanentPermission']);
        Route::get('/create', ['as'=>'crud.operation.permanent-permission.create', 'uses'=>'OperationCrudController@createPermanentPermission']);
    });


    Route::get('/services', [
        'as'=>'api.provider.services.list',
        'uses'=>'ProviderCrudController@apiServiceList'
    ]);

});

Route::group(['prefix'=>'procurement'], function(){

    Route::get('/report', ['as'=>'crud.procurement.report', 'uses'=>'ProcurementCrudController@report']);


    Route::get('/search', ['as'=>'crud.procurement.filter', 'uses' => 'ProcurementCrudController@filter']);
    Route::get('/', ['as'=>'crud.procurement.index', 'uses'=>'ProcurementCrudController@index']);
    Route::get('/{id}/edit', ['as'=>'crud.procurement.edit', 'uses'=>'ProcurementCrudController@edit']);
    Route::post('/{id}/edit', ['as'=>'crud.procurement.update', 'uses'=>'ProcurementCrudController@update']);
    Route::get('{id}/total', ['as'=>'crud.procurement.total', 'uses'=>'ProcurementCrudController@getTotalCost']);


    Route::get('{id}/service/{serviceId}/', ['as'=>'crud.procurement.service.show', 'uses'=>'ProcurementCrudController@showSubServices']);
    Route::get('{id}/service/{serviceId}/create', ['as'=>'crud.procurement.service.create', 'uses'=>'ProcurementCrudController@createSubService']);
    Route::post('{id}/service/{serviceProviderId}/create', ['as'=>'crud.procurement.service.store', 'uses'=>'ProcurementCrudController@storeSubService']);
});

Route::group(['prefix'=>'quotation'], function(){

//    Route::get('/search', ['as'=>'crud.procurement.filter', 'uses' => 'ProcurementCrudController@filter']);
    Route::get('/', ['as'=>'crud.procurement.index', 'uses'=>'QuotationCrudController@index']);
    Route::get('/create', ['as'=>'crud.operation.create', 'uses'=>'QuotationCrudController@create']);
    Route::post('/', ['as'=>'crud.quotation.store', 'uses'=>'QuotationCrudController@store']);

    Route::post('/operation-create', ['as'=>'crud.quotation.operation.create', 'uses'=>'QuotationCrudController@createOperationFromQuotation']);

    Route::get('/{id}/edit', ['as'=>'crud.quotation.edit', 'uses'=>'QuotationCrudController@edit']);
    Route::post('/{id}/edit', ['as'=>'crud.quotation.update', 'uses'=>'QuotationCrudController@update']);


    Route::get('{id}/itinerary/{itinerary_id}/delete', ['as'=>'crud.quotation.itinerary.delete', 'uses'=>'ItineraryCrudController@deleteQuotation']);
    Route::get('{id}/itinerary/{itinerary_id}/cancel', ['as'=>'crud.quotation.itinerary.cancel', 'uses'=>'ItineraryCrudController@cancelQuotation']);

    Route::group(['prefix'=>'{operation_id}/permission'], function(){
        Route::get('/create', ['as'=>'crud.permission.create', 'uses'=>'OperationCrudController@createPermission']);
        Route::post('/create', ['as'=>'crud.permission.store', 'uses'=>'QuotationCrudController@storePermission']);
        Route::get('', ['as'=>'crud.ovf-permission.list', 'uses'=>'OperationCrudController@getOverFlightPermissions']);

        Route::get('/{permission_id}/delete', ['as'=>'crud.permission.delete', 'uses'=>'OperationCrudController@deletePermission']);
        Route::post('/update', ['as'=>'crud.permission.update', 'uses'=>'OperationCrudController@updateOperationPermission']);
        Route::put('/update', ['as' => 'crud.permission-status.update', 'uses' => 'OperationCrudController@updatePermissionStatus']);
    });

});


Route::group(['prefix'=>'/itinerary/{itinerary_id}'], function(){


    Route::get('/crew-member/create', ['as'=>'crud.crew-member.itinerary.create', 'uses'=>'ItineraryCrudController@createCrewMember']);
    Route::post('/crew-member/create', ['as'=>'crud.crew-member.itinerary.store', 'uses'=>'ItineraryCrudController@storeCrewMember']);

    Route::get('/flight-plan/create', ['as'=>'crud.flight-plan.itinerary.create', 'uses'=>'ItineraryCrudController@createFlightPlan']);
    Route::post('/flight-plan/create', ['as'=>'crud.flight-plan.itinerary.store', 'uses'=>'ItineraryCrudController@storeFlightPlan']);


    //
    Route::get('/edit', ['as'=>'crud.operation.itinerary.edit', 'uses'=>'ItineraryCrudController@edit']);
    Route::post('/edit', ['as'=>'crud.operation.itinerary.update', 'uses'=>'ItineraryCrudController@update']);

    Route::get('/edit-quotation', ['as'=>'crud.quotation.itinerary.edit', 'uses'=>'ItineraryCrudController@editQuotation']);
    Route::post('/edit-quotation', ['as'=>'crud.quotation.itinerary.update', 'uses'=>'ItineraryCrudController@updateQuotationItinerary']);


    Route::get('/', ['as'=>'crud.itinerary.show', 'uses'=>'ItineraryCrudController@show']);
//    Route::get('/quotation/', ['as'=>'crud.itinerary.quotation.show', 'uses'=>'ItineraryCrudController@showQuotationItinerary']);

    Route::get('/revisions', ['as' => 'crud.itinerary.listRevisions', 'uses' => 'ItineraryCrudController@listRevisions']);
    Route::post('/revisions/{revisionId}/restore', ['as' => 'crud.itinerary.restoreRevision','uses' => 'ItineraryCrudController@restoreRevision']);

});
Route::group(['prefix'=>'/itinerary/{itinerary_id}/service'], function(){
    Route::post('/create', ['as'=>'crud.service.itinerary.store', 'uses'=>'ItineraryCrudController@storeItineraryService']);
    Route::post('/update', ['as'=>'crud.service.itinerary.update', 'uses'=>'ItineraryCrudController@updateItineraryService']);

    Route::get('/create', ['as'=>'crud.service.itinerary.create', 'uses'=>'ItineraryCrudController@createItineraryService']);

    Route::get('/{service_id}/delete', ['as'=>'crud.service.itinerary.delete', 'uses'=>'ItineraryCrudController@deleteItineraryService']);
    Route::get('/{service_id}/{status}/update', ['as' => 'crud.itinerary-status.update', 'uses' => 'ItineraryCrudController@updateServiceStatus']);

});



Route::group(['prefix'=>'/itinerary-procurement/{id}'], function() {
    Route::get('/', ['as'=>'crud.procurement.itinerary.show', 'uses'=>'ProcurementCrudController@showItinerary']);
    Route::post('/edit', ['as'=>'crud.procurement.itinerary.update', 'uses'=>'ItineraryCrudController@updateProcurementServiceStatus']);
});

Route::group(['prefix'=>'/itinerary-quotation/{id}'], function() {
    Route::get('/', ['as'=>'crud.quotation.itinerary.show', 'uses'=>'QuotationCrudController@showItinerary']);
    Route::post('/edit', ['as'=>'crud.quotation.itinerary.update.services.status', 'uses'=>'QuotationCrudController@updateQuotationServiceStatus']);
});


Route::group(['prefix'=>'/itinerary-billing/{id}'], function() {
    Route::get('/', ['as'=>'crud.billing.itinerary.show', 'uses'=>'BillingCrudController@showItinerary']);
    Route::post('/edit', ['as'=>'crud.billing.itinerary.update', 'uses'=>'ItineraryCrudController@updateBillingServiceStatus']);
});

Route::group(['prefix'=>'billing'], function() {

    Route::get('/', ['as' => 'crud.billing.index', 'uses' => 'BillingCrudController@index']);
    Route::get('/{id}/edit', ['as' => 'crud.billing.edit', 'uses' => 'BillingCrudController@edit']);
    Route::post('/{id}/edit', ['as' => 'crud.billing.update', 'uses' => 'BillingCrudController@update']);
    Route::get('/search', ['as'=>'crud.billing.filter', 'uses' => 'BillingCrudController@filter']);


    Route::get('/{id}/edit-billing-company', ['as'=> 'crud.billing.showChangeBillingCompany', 'uses'=> 'BillingCrudController@showEditBillingCompany']);
    Route::post('/{id}/edit-billing-company', ['as'=> 'crud.billing.changeBillingCompany', 'uses'=> 'BillingCrudController@editBillingCompany']);

    Route::get('/provider-service-bill/{itineraryProviderServiceId}',['as'=>'crud.billing.find-bill', 'uses'=> 'BillingCrudController@redirectToBill'] );
});

Route::group(['prefix'=>'/administrationFee/{id}'], function() {
    Route::get('/', ['as'=>'crud.billing.administrationFee.show', 'uses'=>'BillingCrudController@showAdministrationFee']);

});


Route::group(['prefix'=>'/reports'], function() {
    Route::get('/total', ['as'=>'crud.reports.totals', 'uses'=>'ReportCrudController@totals']);
});



Route::get('fuel/itinerary/{itinerary}/', ['as'=>'fuel.itinerary.values', 'uses'=>'FuelUSAController@showFuelItinerary']);

Route::group(['prefix'=>'/fuel-usa'], function() {
    Route::get('/', ['as'=>'fuel-usa.load', 'uses'=>'FuelUSAController@index']);
    
    Route::get('/show', ['as'=>'fuel-usa.index', 'uses'=>'FuelUSAController@index2']);
    
    Route::post('/', ['as'=>'fuel-usa.upload', 'uses'=>'FuelUSAController@uploadExcel']);
    
});

Route::group(['prefix'=>'/fuel-price'], function() {
    
    Route::get('/show', ['as'=>'fuel-prices.index', 'uses'=>'FuelPriceController@index3']);
    
    Route::get('/', ['as'=>'fuel-prices.load', 'uses'=>'FuelPriceController@index2']);
    
    Route::post('/', ['as'=>'fuel-prices.upload', 'uses'=>'FuelPriceController@uploadExcel']);
    
    Route::get('/itinerary/{itinerary}', ['as' => 'fuel-prices.show', 'uses'=>'FuelPriceController@showFuelItinerary']);
});

Route::group(['prefix'=>'/quotations-credit-card-charges'], function() {

    Route::get('/', ['as'=>'credit-card-charges.list', 'uses'=>'QuotationCreditNoteChargesCrudController@index']);
    Route::post('/', ['as'=>'credit-card-charges.upload', 'uses'=>'QuotationCreditNoteChargesCrudController@uploadExcel']);

});

