<?php

return [
    'notificationUsers'      =>[1, 2],
    'handlingService'        => env('HANDLING_SERVICE', 15),
    'serviceCategoryFuel'    => 1,
    'defaultProviderService' => env('DEFAULT_PROVIDER_SERVICE', 1),
    'pikeProviderId'         => 67,
    'unidadIndexada'         => 3.90450,
    'operation_user_id'      => 44,
    'billing_user_id'        => [45, 31],
    'procurement_user_id'    => 32,
    'service_fuel_child'     => 79,
    'handling_services_id'   => 124,
    'basic_handling_service_id'   => 70,


    'branch_companies'      => [
        NULL => "No Branch",
        38 => "Pike Argentina",
        39 => "Pike Handling SRL",
        51 => "Pike Aviation S.A.",
        67 => "Pike Uruguay S.R.L.",
        204=> "Pike Aviation CORP",
        481=> "Pike Handling B.V.",
        ],

    'allowed_quotation_countries' => [
        'uy', 'ar' ,'bo', 'py'
    ],

    'credit_card_quotation_surcharge' => 0.08,

    // Quotations sheets info
    'ssID'          => env('QUOTATION_SHEETS_SS_ID','1k-8SxGcumZnESt2vYhwYQcZZr0cZpHVI_iM99PTvePA'),
    'apiKey'        => env('QUOTATION_SHEETS_API_KEY', 'AIzaSyBJIjRLC7T4QFv71B_GZ8rz3MiaS3dc40I'),
    'client_id'     => env('QUOTATION_SHEETS_CLIENT_ID', '901856530835-4ou6vlrpurkqq2461q7q06vljb8rke47.apps.googleusercontent.com'),
    'scope'         => env('QUOTATION_SHEETS_SCOPE', 'https://www.googleapis.com/auth/spreadsheets'),

    'coordination_fee' => 171,

    'tba_company_id' => 957,

];
