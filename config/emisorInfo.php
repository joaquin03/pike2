<?php

return [
    'RUCEmisor'         => '217034690015',
    'RznSoc'            => 'PIKE AVIATION S.A',
    'NomComercial'      => 'PIKE AVIATION',
    'GiroEmis'          => 'OPCIONAL',
    'Telefono'          => '+59899684326',
    'EmiSucursal'       => 'PIKE AVIATION',
    'CdgDGISucur'       => '0',
    'DomFiscal'         => 'Paraguay 2141 oficina 1010, Montevideo',
    'Ciudad'            => 'Montevideo',
    'InfoAdicionalEms'   => 'info@pike-aviation.com, https://www.pike-aviation.com/',
];


