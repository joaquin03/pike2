<?php

/*
 * This file is part of Laravel Pusher.
 *
 * (c) Vincent Klaiber <hello@vinkla.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

return [

    /*
    |--------------------------------------------------------------------------
    | Default Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the connections below you wish to use as
    | your default connection for all work. Of course, you may use many
    | connections at once using the manager class.
    |
    */

    'default' => 'main',

    /*
    |--------------------------------------------------------------------------
    | Pusher Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the connections setup for your application. Example
    | configuration has been included, but you may add as many connections as
    | you would like.
    |
    */

    'connections' => [

        'main' => [
            'auth_key' => env('PUSHER_APP_KEY', '908271e08d04d875675c'),
            'secret' => env('PUSHER_APP_SECRET', '23e91d0b36ef643d56c5'),
            'app_id' => env('PUSHER_APP_ID', '548374'),
            'host' => null,
            'port' => null,
            'timeout' => null,
            'options' => [
                'cluster' => env('PUSHER_CLUSTER', 'us2')
            ],
        ],

        'alternative' => [
            'auth_key' => '908271e08d04d875675c',
            'secret' => '23e91d0b36ef643d56c5',
            'app_id' => '548374',
            'host' => null,
            'port' => null,
            'timeout' => null,
            'options' => [
                'cluster' =>'us2'
            ],
        ],

    ],

];
